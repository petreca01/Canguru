fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

## Choose your installation method:

<table width="100%" >
<tr>
<th width="33%"><a href="http://brew.sh">Homebrew</a></td>
<th width="33%">Installer Script</td>
<th width="33%">Rubygems</td>
</tr>
<tr>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS or Linux with Ruby 2.0.0 or above</td>
</tr>
<tr>
<td width="33%"><code>brew cask install fastlane</code></td>
<td width="33%"><a href="https://download.fastlane.tools">Download the zip file</a>. Then double click on the <code>install</code> script (or run it in a terminal window).</td>
<td width="33%"><code>sudo gem install fastlane -NV</code></td>
</tr>
</table>
# Available Actions
### write_secret
```
fastlane write_secret
```


----

## Android
### android build
```
fastlane android build
```
Builds App and generates APK

This action does the following:



- perform 'setup'

- Compiles

- Uploads Staging APK to HockeyApp

- Uploads Production APK to HockeyApp

- Uploads Production APK to Play Store as Beta (pending)



Params:



- `deploy`: flag to force deploy to HockeyApp regardless of being a tag or not. Usage '$ fastlane android build deploy:true'
### android test
```
fastlane android test
```

### android setup
```
fastlane android setup
```
Setup everything needed to run/deploy

----

## iOS
### ios staging
```
fastlane ios staging
```
Builds Staging App

This action does the following:



- perform 'setup'

- Update version number with travis build number

- Compiles staging schema

- Uploads IPA to HockeyApp



Params:



- `deploy`: flag to force deploy to HockeyApp regardless of being a tag or not. Usage '$ fastlane ios staging deploy:true'
### ios production
```
fastlane ios production
```
Builds Production App

This action does the following:



- perform 'setup'

- Incremets build number from iTunesConnect last build

- Compiles production schema

- Uploads IPA to HockeyApp

- Resign IPA with AppStore certificates and provisionings

- Uploads resigned IPA to iTunesConnect



Params:



- `deploy`: flag to force deploy regardless of being a tag or not. Usage '$ fastlane ios production deploy:true'

- `should_increment_build_number`: flag to not increment app build number, default value is true. Usage '$ fastlane ios production should_increment_build_number:false'

- `should_hockeyapp`: flag to skip deploy to HockeyApp. Usage '$ fastlane ios production should_hockeyapp:false'

- `should_appstore`: flag to not resign '.ipa' with AppStore certificates and provisionings. Usage '$ fastlane ios production should_appstore:false'
### ios resign_build
```
fastlane ios resign_build
```
Builds AppStore App

This action does the following:



- Resign the production build with appstore provisiong and code signing identity

- Uploads IPA to iTunesConnect



Params:



- `deploy`: flag to force deploy to iTunesConnect regardless of being a tag or not. Usage '$ fastlane ios appstore deploy:true'
### ios test
```
fastlane ios test
```

### ios setup
```
fastlane ios setup
```
Setup everything needed to run/deploy



- Runs XCake if project is setup to use it

- Dowload and install Apple Certificates and provisioning profiles

- pod install
### ios keychain
```
fastlane ios keychain
```
Dowload and install Apple Certificates and provisioning profiles
### ios fast
```
fastlane ios fast
```
Simple version of 'setup' lane, without either cocoapods repo update or 'keychain' that is used to download and install certificates.
### ios xcode
```
fastlane ios xcode
```
Fix Xcode 'Fix Issue' button. check https://github.com/neonichu/FixCode#fixcode
### ios reprovision
```
fastlane ios reprovision
```
Renew ALL Apple provisioning profiles used by this app
### ios derived
```
fastlane ios derived
```

### ios travis_dedup
```
fastlane ios travis_dedup
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
