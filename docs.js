var docs = require('docs-images');
docs.generate({
  source_path: 'docs/diagrams-src',
  source_extension: 'txt',
  images_path:  'docs/assets'
}, function(err) {
  if (err) {
    console.log('errr generating docs: ' + err);
  } else {
    console.log('docs generated :)');
  }
});
