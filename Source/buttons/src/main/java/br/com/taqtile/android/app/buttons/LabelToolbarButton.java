package br.com.taqtile.android.app.buttons;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 04/07/17.
 */

public class LabelToolbarButton extends FrameLayout {

  private CustomTextView label;
  private Listener listener;
  private OnClickListener onClickListener;
  private FrameLayout root;

  public LabelToolbarButton(Context context) {
    super(context);
    init();
  }

  public LabelToolbarButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public LabelToolbarButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_label_toolbar_button, this, true);

    bindViews();
    setupButton();
  }

  private void bindViews() {
    label = (CustomTextView) findViewById(R.id.component_label_toolbar_button);
    root = (FrameLayout) findViewById(R.id.component_label_toolbar_button_root);
  }

  private void setupButton(){
    root.setOnClickListener(getOnClickListener());
  }

  private OnClickListener getOnClickListener(){
    onClickListener = v -> {
      if (listener != null){
        listener.onLabelClick();
      }
    };
    return onClickListener;
  }

  public void setEnabledButton(boolean isEnabled){
    if (isEnabled){
      enableButton();
    } else {
      disableButton();
    }
  }

  public void enableButton(){
    label.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
    root.setOnClickListener(onClickListener);
  }

  public void disableButton(){
    label.setTextColor(ContextCompat.getColor(getContext(), R.color.color_toolbar_disabled_button));
    root.setOnClickListener(null);
  }

  public void setText(String text){
    label.setText(text);
  }

  public interface Listener{
    void onLabelClick();
  }

  public void setListener(Listener listener){ this.listener = listener; }

}
