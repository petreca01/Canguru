package br.com.taqtile.android.app.buttons;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.PopupMenu;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import java.util.List;

/**
 * Created by taqtile on 18/04/17.
 */

public class OverflowButton extends FrameLayout {

  private AppCompatImageButton overflowButton;
  private List<String> menuList;
  private OverflowButtonListener listener;

  public OverflowButton(@NonNull Context context) {
    super(context);
    init();
  }

  public OverflowButton(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public OverflowButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_overflow_button, this, true);

    bindViews();
    setupListeners();
  }

  private void bindViews() {
    overflowButton = (AppCompatImageButton) findViewById(R.id.component_overflow_button_button);
  }

  private void setupListeners() {
    overflowButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {

        PopupMenu popupMenu = new PopupMenu(getContext(), overflowButton);

        for (String item : menuList) {
          popupMenu.getMenu().add(item);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
          @Override
          public boolean onMenuItemClick(MenuItem item) {
            if (listener != null) {
              listener.onItemSelected(item.getTitle().toString());
            }
            return false;
          }
        });
        popupMenu.show();
      }
    });
  }

  public void setIconColor(int color) {
    overflowButton.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_ATOP));
  }

  public void setMenuList(List<String> menuList) {
    this.menuList = menuList;
  }

  public interface OverflowButtonListener {
    void onItemSelected(String item);
  }

  public void onMenuItemSelected(OverflowButtonListener listener) {
    this.listener = listener;
  }
}
