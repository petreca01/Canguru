package br.com.taqtile.android.app.buttons;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 11/8/16.
 */

public class ProgressBarButton extends FrameLayout {

  //In milliseconds
  private final static int PROGRESS_BAR_LOADING_TIME = 15000;
  private final static int PROGRESS_BAR_FAST_LOADING_TIME = 500;

  private final static int INCREMENT_AMMOUNT = 100;

  private ProgressBar progressBar;
  private boolean isLoading;

  private Drawable defaultProgressBarDrawable;

  private Drawable loadingProgressBarDrawable;

  private CustomTextView buttonText;

  private String defaultButtonText;

  private String loadingButtonText;

  private FrameLayout root;

  private ProgressBarButtonButtonListener buttonListener;

  private ProgressBarButtonAnimationListener animationListener;

  private ObjectAnimator objectAnimator;

  private View clickableView;

  private Handler completeLoadingFastHandler;

  private int mLoadingTime = PROGRESS_BAR_LOADING_TIME;

  private int mFastLoadingTime = PROGRESS_BAR_FAST_LOADING_TIME;

  public ProgressBarButton(Context context) {
    super(context);
    init(null, 0);
  }

  public ProgressBarButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(attrs, 0);
  }

  public ProgressBarButton(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(attrs, defStyleAttr);
  }

  //Setup region

  private void init(AttributeSet attrs, int defStyleAttr) {

    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_progress_bar_button, this, true);

    bindViews();
    setupBarAnimation();
    getXmlResources(attrs);
    showDefaultState();
    setupCompleteLoadingFastHandler();
    isLoading = false;

  }

  private void getXmlResources(AttributeSet attrs) {
    TypedArray attributesFromXmlLayout = getContext().obtainStyledAttributes(attrs, R.styleable.ProgressBarButton);

    if (attributesFromXmlLayout == null) {
      return;
    }

    defaultProgressBarDrawable = attributesFromXmlLayout.getDrawable(
      R.styleable.ProgressBarButton_TQ_progress_bar_default_drawable);

    defaultButtonText = attributesFromXmlLayout.getString(
      R.styleable.ProgressBarButton_TQ_progress_bar_default_text);

    loadingProgressBarDrawable = attributesFromXmlLayout.getDrawable(
      R.styleable.ProgressBarButton_TQ_progress_bar_loading_drawable);

    loadingButtonText = attributesFromXmlLayout.getString(
      R.styleable.ProgressBarButton_TQ_progress_bar_loading_text);

    attributesFromXmlLayout.recycle();
  }

  private void bindViews() {
    progressBar = (ProgressBar) findViewById(R.id.component_progress_bar_progress_bar);
    buttonText = (CustomTextView) findViewById(R.id.component_progress_bar_button_text);
    root = (FrameLayout) findViewById(R.id.component_progress_bar_button_root);
    clickableView = (View) findViewById(R.id.component_progress_bar_button_clickable_view);
  }

  //end region

  //Animation region

  public void setupBarAnimation() {
    objectAnimator = new ObjectAnimator();
    objectAnimator.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animation) {
        if (animationListener != null) {
          animationListener.onLoadingStarted();
        }
      }

      @Override
      public void onAnimationEnd(Animator animation) {
        if (animationListener != null) {
          animationListener.onLoadingFinished();
        }
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        if (animationListener != null) {
          animationListener.onLoadingCanceled();
        }
      }

      @Override
      public void onAnimationRepeat(Animator animation) {

      }
    });

    clickableView.setOnClickListener(view -> {
      if (buttonListener != null) {
        buttonListener.onButtonClicked();
      }
    });
  }

  public void animateBar(int time) {
    objectAnimator.cancel();
    progressBar.setProgress(0);
    objectAnimator = ObjectAnimator.ofInt(progressBar, "progress",
      progressBar.getProgress(), INCREMENT_AMMOUNT).setDuration(time);

    objectAnimator.addUpdateListener(valueAnimator -> {
      int progress = (int) valueAnimator.getAnimatedValue();
      progressBar.setProgress(progress);
    });
    objectAnimator.start();
  }

  public void completeLoadingFast() {
    animateBar(mFastLoadingTime);
  }

  //end region

  //Progress bar region

  public void showLoadingState() {
    isLoading = true;
    disableClick();
    progressBar.setProgressDrawable(loadingProgressBarDrawable);
    if (loadingButtonText == null) {
      buttonText.setText(defaultButtonText);
    } else {
      buttonText.setText(loadingButtonText);
    }
    buttonText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
    animateBar(mLoadingTime);
  }

  public void showDefaultState() {
    isLoading = false;
    enableClick();
    this.completeLoadingFast();
    progressBar.setProgressDrawable(defaultProgressBarDrawable);
    buttonText.setText(defaultButtonText);
    buttonText.setTextColor(ContextCompat.getColor(getContext(), R.color.color_white));
  }

  public void enableClick(){
    clickableView.setEnabled(true);
    this.setEnabled(true);
  }

  public void disableClick(){
    clickableView.setEnabled(false);
    this.setEnabled(false);
  }

  private void setupCompleteLoadingFastHandler(){
    completeLoadingFastHandler = new Handler();
  }

  //Setters and getters region

  public void setLoadingTime(int loadingTime) {
    mLoadingTime = loadingTime;
  }

  public void setFastLoadingTime(int fastLoadingTime) {
    mFastLoadingTime = fastLoadingTime;
  }

  public void setButtonTextColor(int buttonTextColor) {
    buttonText.setTextColor(ContextCompat.getColor(getContext(), buttonTextColor));
  }

  public void setButtonDefaultText(String defaultText) {
    buttonText.setText(defaultText);
  }

  public void setButtonLoadingText(String loadingText) {
    buttonText.setText(loadingText);
  }

  public boolean isLoading(){
    return isLoading;
  }

  //end region

  //Listener region

  public interface ProgressBarButtonButtonListener {
    void onButtonClicked();
  }

  public interface ProgressBarButtonAnimationListener {
    void onLoadingStarted();

    void onLoadingFinished();

    void onLoadingCanceled();

    void onLoadingRepeat();
  }

  public void setButtonListener(ProgressBarButtonButtonListener progressBarButtonButtonListener) {
    buttonListener = progressBarButtonButtonListener;
  }

  public void setAnimationListener(ProgressBarButtonAnimationListener progressBarButtonAnimationListener) {
    animationListener = progressBarButtonAnimationListener;
  }

  //end region

}
