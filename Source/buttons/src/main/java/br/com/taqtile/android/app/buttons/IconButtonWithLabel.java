package br.com.taqtile.android.app.buttons;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 20/04/17.
 */

public class IconButtonWithLabel extends LinearLayout {

  private String id;
  private LinearLayout root;
  private ImageView activeIcon;
  private ImageView inactiveIcon;
  private CustomTextView label;
  private Drawable activeButtonDrawable;
  private Drawable inactiveButtonDrawable;
  private boolean isButtonActive;

  private Listener listener;

  public IconButtonWithLabel(Context context) {
    super(context);
    init(null);
  }

  public IconButtonWithLabel(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init(attrs);
  }

  public IconButtonWithLabel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(attrs);
  }

  private void init(AttributeSet attrs) {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_icon_button_with_label, this, true);

    bindViews(attrs);

    setupView();
    setupIcons();
    setupButtons();
  }

  private void bindViews(AttributeSet attrs) {
    TypedArray attributesFromXmlLayout = getContext().obtainStyledAttributes(attrs, R.styleable.IconButtonWithLabel);

    if (attributesFromXmlLayout == null) {
      return;
    }

    activeButtonDrawable = attributesFromXmlLayout.getDrawable(
      R.styleable.IconButtonWithLabel_TQ_icon_button_with_label_active_button_drawable);
    inactiveButtonDrawable = attributesFromXmlLayout.getDrawable(
      R.styleable.IconButtonWithLabel_TQ_icon_button_with_label_inactive_button_drawable);

    activeIcon = (ImageView) findViewById(R.id.component_icon_button_with_label_active_icon);
    inactiveIcon = (ImageView) findViewById(R.id.component_icon_button_with_label_inactive_icon);
    label = (CustomTextView) findViewById(R.id.component_icon_button_with_label_label);
    root = (LinearLayout) findViewById(R.id.component_icon_button_with_label_root);

    attributesFromXmlLayout.recycle();

  }

  private void setupButtons(){
    root.setOnClickListener(v -> {
      if (listener != null) {
        setEnabled(false);
        if (isButtonActive){
          onActiveButtonClick();
        } else {
          onInactiveButtonClick();
        }
      }
    });
  }

  public void setButtonActive(boolean isButtonActive) {
    this.isButtonActive = isButtonActive;
    updateIcon();
  }

  public boolean isButtonActive(){
    return isButtonActive;
  }

  public void onActiveButtonClick(){
    listener.onActiveButtonClick();
  }

  public void onInactiveButtonClick(){
    listener.onInactiveButtonClick();
  }

  private void setupIcons() {
    activeIcon.setImageDrawable(activeButtonDrawable);
    inactiveIcon.setImageDrawable(inactiveButtonDrawable);

  }

  private void setupView() {
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setOrientation(VERTICAL);
    setLayoutParams(layoutParams);
    setEnabled(true);
  }

  public void showActiveIcon() {
    activeIcon.setVisibility(VISIBLE);
  }

  public void showInactiveIcon() {
    inactiveIcon.setVisibility(VISIBLE);
  }

  public void hideActiveIcon() {
    activeIcon.setVisibility(GONE);
  }

  public void hideInactiveIcon() {
    inactiveIcon.setVisibility(GONE);
  }

  private void updateIcon() {
    setEnabled(true);
    if (isButtonActive) {
      showActiveIcon();
      hideInactiveIcon();
    } else {
      showInactiveIcon();
      hideActiveIcon();
    }
  }

  public void setViewId(String id){
    this.id = id;
  }

  public String getViewId(){
    return id;
  }

  public void setActiveIcon(Drawable icon){
    this.activeIcon.setImageDrawable(icon);
  }

  public void setInactiveIcon(Drawable icon){
    this.inactiveIcon.setImageDrawable(icon);
  }

  public void setLabel(String label){
    this.label.setText(label);
  }

  public interface Listener {
    void onActiveButtonClick();

    void onInactiveButtonClick();
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

}
