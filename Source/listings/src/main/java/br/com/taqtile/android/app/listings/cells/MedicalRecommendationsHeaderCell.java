package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsHeaderCell extends FrameLayout {
  public MedicalRecommendationsHeaderCell(@NonNull Context context) {
    super(context);
    init();
  }

  public MedicalRecommendationsHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public MedicalRecommendationsHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_medical_recommendations_header_cell, this, true);
  }
}
