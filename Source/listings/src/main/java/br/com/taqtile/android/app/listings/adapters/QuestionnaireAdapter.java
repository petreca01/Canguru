package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.questionnairecomponents.AnswerListener;
import br.com.taqtile.android.app.listings.questionnairecomponents.FreeTextQuestionnaireComponent;
import br.com.taqtile.android.app.listings.questionnairecomponents.IMCQuestionnaireComponent;
import br.com.taqtile.android.app.listings.questionnairecomponents.MultipleChoiceQuestionnaireComponent;
import br.com.taqtile.android.app.listings.questionnairecomponents.RangeQuestionnaireComponent;
import br.com.taqtile.android.app.listings.questionnairecomponents.SingleChoiceQuestionnaireComponent;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionType;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import xyz.santeri.wvp.WrappingViewPager;

/**
 * Created by taqtile on 19/05/17.
 */

public class QuestionnaireAdapter extends PagerAdapter {

  private int currentPosition = -1;

  private final static int VIEWPAGER_RESIZING_ANIMAITON_DURATION = 150;

  private final static int MULTIPLE_CHOICES = 0;
  private final static int SINGLE_CHOICE = 1;
  private final static int FREE_TEXT = 2;
  private final static int RANGE = 3;
  private final static int YES_OR_NO = 4;
  private final static int IMC = 5;
  private final static int LIMITED_TEXT = 6;

  private AnswerListener answerListener;
  private List<QuestionViewModel> questions;

  private boolean hasBeenPopulated;

  public QuestionnaireAdapter(AnswerListener answerListener) {
    this.questions = new ArrayList<>();
    this.answerListener = answerListener;
    hasBeenPopulated = false;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {
    return getQuestionComponent(container, position);
  }

  private View getQuestionComponent(ViewGroup container, int position) {
    View itemView = new View(container.getContext());

    int questionType = this.questions.get(position).getType();
    if (questionType == QuestionType.QUESTION_TYPE_MULTIPLE) {
      itemView = buildQuestion(MULTIPLE_CHOICES, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_SELECT) {
      itemView = buildQuestion(SINGLE_CHOICE, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_IMC) {
      itemView = buildQuestion(IMC, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_TEXT) {
      itemView = buildQuestion(FREE_TEXT, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_VARCHAR) {
      itemView = buildQuestion(LIMITED_TEXT, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_RANGE) {
      itemView = buildQuestion(RANGE, container.getContext(), position);
    } else if (questionType == QuestionType.QUESTION_TYPE_BOOLEAN) {
      itemView = buildQuestion(YES_OR_NO, container.getContext(), position);
    }
    container.addView(itemView);
    return itemView;
  }

  @Override
  public int getCount() {
    return questions.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  private View buildQuestion(int type, Context context, int position) {
    View question = new View(context);

    QuestionViewModel questionModel = this.questions.get(position);

    switch (type) {
      case MULTIPLE_CHOICES:
        question = new MultipleChoiceQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case SINGLE_CHOICE:
        question = new SingleChoiceQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case FREE_TEXT:
        question = new FreeTextQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case LIMITED_TEXT:
        question = new FreeTextQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case RANGE:
        question = new RangeQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case YES_OR_NO:
        question = new SingleChoiceQuestionnaireComponent(context, answerListener, questionModel);
        break;
      case IMC:
        question = new IMCQuestionnaireComponent(context, answerListener, questionModel);
        break;
      default:
        question = new MultipleChoiceQuestionnaireComponent(context, answerListener, questionModel);
        break;
    }
    return question;
  }

  public void setQuestions(List<QuestionViewModel> questions) {
    this.questions.clear();
    this.addQuestionViewModels(questions);
  }

  public void addQuestionViewModels(List<QuestionViewModel> currentQuestion) {
    this.questions.addAll(currentQuestion);
  }

  @Override
  public void setPrimaryItem(ViewGroup container, int position, Object object) {
    super.setPrimaryItem(container, position, object);

    if (!(container instanceof WrappingViewPager)) {
      return; // Do nothing if it's not a compatible ViewPager
    }

    if (position != currentPosition) { // If the position has changed, tell WrappingViewPager
      View view = (View) object;
      WrappingViewPager pager = (WrappingViewPager) container;
      if (view != null) {
        if (!hasBeenPopulated) {
          pager.setAnimationDuration(1);
          hasBeenPopulated = true;
        } else {
          pager.setAnimationDuration(VIEWPAGER_RESIZING_ANIMAITON_DURATION);
        }
        currentPosition = position;
        pager.onPageChanged(view);
      }
    }
  }

}
