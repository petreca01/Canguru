package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class QuestionViewModel implements
  QuestionMultipleType,
  QuestionSelectType,
  QuestionRangeType,
  QuestionTextType,
  QuestionVarcharType,
  QuestionBooleanType,
  QuestionImcType,
  Parcelable {

  public QuestionViewModel(int questionId, int type, String question, String description,
                           List<String> alternatives, List<String> answers,
                           Integer min, Integer max) {
    this.questionId = questionId;
    this.type = type;
    this.question = question;
    this.description = description;
    this.alternatives = alternatives;
    this.answers = answers;
    this.min = min;
    this.max = max;
  }

  public QuestionViewModel(int questionId, int type, String question, String description,
                           List<String> alternatives, List<String> answers) {
    this.questionId = questionId;
    this.type = type;
    this.question = question;
    this.description = description;
    this.alternatives = alternatives;
    this.answers = answers;
    this.min = 0;
    this.max = 0;
  }

  private int questionId;
  private int type;
  private String question;
  private String description;
  private List<String> answers;

  // Multiple
  // Select
  private List<String> alternatives;

  // Range
  private Integer min;
  private Integer max;

  @Override
  public String getQuestion() {
    return this.question;
  }

  @Override
  public int getType() {
    return this.type;
  }

  @Override
  public List<String> getAlternatives() {
    return this.alternatives;
  }

  @Override
  public String getAnswer() {
    if (this.answers != null && this.answers.size() > 0) {
      return this.answers.get(0);
    } else {
      return null;
    }
  }

  @Override
  public int getQuestionId() {
    return this.questionId;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public Integer getMin() {
    return this.min;
  }

  @Override
  public Integer getMax() {
    return this.max;
  }

  @Override
  public List<String> getAnswers() {
    return this.answers;
  }

  @Override
  public Boolean getAnswerValue() {
    return this.getAnswer().equals("1");
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.questionId);
    dest.writeInt(this.type);
    dest.writeString(this.question);
    dest.writeString(this.description);
    dest.writeStringList(this.answers);
    dest.writeStringList(this.alternatives);
    dest.writeValue(this.min);
    dest.writeValue(this.max);
  }

  protected QuestionViewModel(Parcel in) {
    this.questionId = in.readInt();
    this.type = in.readInt();
    this.question = in.readString();
    this.description = in.readString();
    this.answers = in.createStringArrayList();
    this.alternatives = in.createStringArrayList();
    this.min = (Integer) in.readValue(Integer.class.getClassLoader());
    this.max = (Integer) in.readValue(Integer.class.getClassLoader());
  }

  public static final Creator<QuestionViewModel> CREATOR = new Creator<QuestionViewModel>() {
    @Override
    public QuestionViewModel createFromParcel(Parcel source) {
      return new QuestionViewModel(source);
    }

    @Override
    public QuestionViewModel[] newArray(int size) {
      return new QuestionViewModel[size];
    }
  };
}
