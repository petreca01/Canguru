package br.com.taqtile.android.app.listings.grids;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import java.util.ArrayList;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.CarouselGridAdapter;
import br.com.taqtile.android.app.listings.adapters.CarouselGridLoadingAdapter;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ICarouselGridItemModel;
import br.com.taqtile.android.app.misc.utils.HorizontalSpaceItemDecoration;


/**
 * Created by taqtile on 1/22/16.
 */
public class CarouselGrid extends RecyclerView implements CarouselGridAdapter.CarouselGridAdapterListener, CarouselGridLoadingAdapter.CarouselGridLoadingAdapterListener {
    private Context mContext;
    private ArrayList<ICarouselGridItemModel> mArray;
    private CarouselGridListener mListener;

    public CarouselGrid(Context context) {
        super(context);
        initialSetup();
    }

    public CarouselGrid(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialSetup();
    }

    public CarouselGrid(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialSetup();
    }

    public void setData(ArrayList<ICarouselGridItemModel> data) {
        CarouselGridAdapter adapter = new CarouselGridAdapter(data);
        adapter.setListener(this);
        this.setAdapter(adapter);
    }

    public void setLoadingState() {
        CarouselGridLoadingAdapter adapter = new CarouselGridLoadingAdapter();
        adapter.setListener(this);
        this.setAdapter(adapter);
    }

    private void initialSetup() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        this.setLayoutManager(layoutManager);

        this.addItemDecoration(new HorizontalSpaceItemDecoration((int) getResources().getDimension(R.dimen.margin_small)));
    }

    public void setListener(CarouselGridListener mListener) {
        this.mListener = mListener;
    }

    //region ProductCarouselGridAdapterListener
    @Override
    public void onCarouselGridItemClick(int position, ICarouselGridItemModel model) {
        if (mListener != null) {
            mListener.onCarouselGridItemClick(this, position, model);
        }
    }

    @Override
    public Context getExternalContext() {
        return getContext();
    }
    //endregion

    public interface CarouselGridListener {
        void onCarouselGridItemClick(CarouselGrid productCarouselGrid, int position, ICarouselGridItemModel model);
    }
}
