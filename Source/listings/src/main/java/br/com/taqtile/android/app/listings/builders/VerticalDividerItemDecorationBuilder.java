package br.com.taqtile.android.app.listings.builders;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemSeparatorViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;

/**
 * Created by taqtile on 16/05/17.
 */

public class VerticalDividerItemDecorationBuilder {

  public static List<AgendaFeedViewModel> getListWithSeparators(List<AgendaFeedViewModel> listViewModels, AgendaItemSeparatorViewModel separatorViewModel) {
    
    ArrayList<AgendaFeedViewModel> listViewModelsWithSeparator = new ArrayList<>();
    for (int i = 0; i < listViewModels.size(); i++) {
      listViewModelsWithSeparator.add(listViewModels.get(i));
      listViewModelsWithSeparator.add(separatorViewModel);
    }
    return listViewModelsWithSeparator;
  }
}
