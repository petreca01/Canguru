package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/23/17.
 */

public class UltraSoundCell extends LinearLayout {

  private CustomTextView cellDate;
  private CustomTextView cellDoctorName;
  private CustomTextView cellGestationWeekDUM;
  private CustomTextView cellGestationWeekUltraSound;
  private CustomTextView cellFetalWeight;
  private CustomTextView cellPlacenta;
  private CustomTextView cellLiquid;
  private CustomTextView cellPBF;

  public UltraSoundCell(Context context) {
    super(context);
    init();
  }

  public UltraSoundCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public UltraSoundCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void setupAttrs(AttributeSet attrs) {
    // TODO: 5/23/17 ADD STYLES
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.ultra_sound_cell, this, true);

    bindView();
  }

  private void bindView() {
    cellDate = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_date);
    cellDoctorName = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_doctor_name);
    cellGestationWeekDUM = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_gestation_week_dum);
    cellGestationWeekUltraSound = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_gestation_week_ultra_sound);
    cellFetalWeight = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_fetal_weight);
    cellPlacenta = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_placenta);
    cellLiquid = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_liquid);
    cellPBF = (CustomTextView) findViewById(R.id.ultra_sound_cell_ultra_sound_pbf);
  }

  public void setCellDate(String cellName) {
    this.cellDate.setText(cellName);
  }

  public void setCellDoctorName(String cellDate) {
    this.cellDoctorName.setText(cellDate);
  }

  public void setCellGestationWeekDUM(String cellName) {
    this.cellGestationWeekDUM.setText(cellName);
  }

  public void setCellGestationWeekUltraSound(String cellDate) {
    this.cellGestationWeekUltraSound.setText(cellDate);
  }

  public void setCellFetalWeight(String cellName) {
    this.cellFetalWeight.setText(cellName);
  }

  public void setCellPlacenta(String cellDate) {
    this.cellPlacenta.setText(cellDate);
  }

  public void setCellLiquid(String cellName) {
    this.cellLiquid.setText(cellName);
  }

  public void setCellPBF(String cellDate) {
    this.cellPBF.setText(cellDate);
  }

}
