package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.adapters.viewholders.ChannelViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.ForumViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SymptomViewHolder;
import br.com.taqtile.android.app.listings.cells.ChannelCell;
import br.com.taqtile.android.app.listings.cells.ForumCell;
import br.com.taqtile.android.app.listings.cells.SimpleTextImageCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SymptomListingViewModel;

/**
 * Created by taqtile on 3/23/17.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<SearchListingViewModel> searchResults;

  private SearchAdapterListener searchAdapterListener;

  public SearchAdapter(List<SearchListingViewModel> searchResults) {
    this.searchResults = searchResults;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder viewHolder;
    switch (viewType) {
      case Constants.CHANNEL_LISTING_CELL:
        viewHolder = new ChannelViewHolder(new ChannelCell(parent.getContext()));
        break;
      case Constants.FORUM_LISTING_CELL:
        viewHolder = new ForumViewHolder(new ForumCell(parent.getContext()));
        break;
      case Constants.SYMPTOM_LISTING_CELL:
        viewHolder = new SymptomViewHolder(new SimpleTextImageCell(parent.getContext()));
        break;
      default:
        viewHolder = new SymptomViewHolder(new SimpleTextImageCell(parent.getContext()));
        break;
    }
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case Constants.CHANNEL_LISTING_CELL:
        setupChannelCell(holder, position);
        break;
      case Constants.FORUM_LISTING_CELL:
        setupForumCell(holder, position);
        break;
      case Constants.SYMPTOM_LISTING_CELL:
        setupSymptomCell(holder, position);
        break;
    }
  }

  //TODO move to a binder later
  private void setupSymptomCell(RecyclerView.ViewHolder holder, int position) {
    SimpleTextImageCell symptomCell = ((SymptomViewHolder) holder).getSymptomCell();
    SymptomListingViewModel symptomListingViewModel = (SymptomListingViewModel) searchResults.get(position);

    symptomCell.setIcon(br.com.taqtile.android.app.resources.R.drawable.ic_symptom);
    symptomCell.setCellId(symptomListingViewModel.getId());
    symptomCell.setHeader(symptomListingViewModel.getTopicName());
    symptomCell.setListener(id -> {
      if (searchAdapterListener != null) {
        searchAdapterListener.onSymptomCellClick(symptomListingViewModel.getId());
      }
    });
  }

  //TODO move to a binder later
  private void setupForumCell(RecyclerView.ViewHolder holder, int position) {
    ForumCell forumCell = ((ForumViewHolder) holder).getForumCell();
    ForumListingViewModel forumListingViewModel = (ForumListingViewModel) searchResults.get(position);

    forumCell.setLeftCounter(forumListingViewModel.getLikesCounter());
    forumCell.setRightCounter(forumListingViewModel.getCommentsCounter());
    forumCell.setHeader(forumListingViewModel.getTopicName());
    forumCell.setCellId(forumListingViewModel.getId());
    forumCell.setListener(id -> {
      if (searchAdapterListener != null) {
        searchAdapterListener.onForumCellClick(forumListingViewModel.getId());
      }
    });

  }

  //TODO move to a binder later
  private void setupChannelCell(RecyclerView.ViewHolder holder, int position) {
    ChannelCell channelCell = ((ChannelViewHolder) holder).getChannelCell();
    ChannelListingViewModel channelListingViewModel = (ChannelListingViewModel) searchResults.get(position);

    channelCell.setLeftCounter(channelListingViewModel.getPostsCounter());
    channelCell.setRightCounter(channelListingViewModel.getFollowersCounter());
    channelCell.setHeader(channelListingViewModel.getTopicName());
    channelCell.setInstitutionName(channelListingViewModel.getInstitutionName());
    channelCell.setCellId(channelListingViewModel.getId());
    channelCell.setListener(id -> {
      if (searchAdapterListener != null) {
        searchAdapterListener.onChannelCellClick(channelListingViewModel.getId());
      }
    });
  }

  @Override
  public int getItemCount() {
    return searchResults.size();
  }

  @Override
  public int getItemViewType(int position) {
    SearchListingViewModel searchListingViewModel = searchResults.get(position);
    if (searchListingViewModel instanceof ForumListingViewModel) {
      return Constants.FORUM_LISTING_CELL;
    } else if (searchListingViewModel instanceof ChannelListingViewModel) {
      return Constants.CHANNEL_LISTING_CELL;
    } else {
      return Constants.SYMPTOM_LISTING_CELL;
    }
  }

  public interface SearchAdapterListener {
    void onForumCellClick(String id);

    void onChannelCellClick(String id);

    void onSymptomCellClick(String id);
  }

  public void setListener(SearchAdapterListener searchAdapterListener) {
    this.searchAdapterListener = searchAdapterListener;
  }

}
