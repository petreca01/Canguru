package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.flexbox.FlexboxLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.general.CustomFlag;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/22/17.
 */

public class AntecedentsAccordion extends LinearLayout {

  private ImageView icon;
  private CustomTextView cardTitle;
  private CustomTextView age;
  private CustomTextView numberOfGestations;
  private CustomTextView childbirthTypes;
  private FlexboxLayout alertFlagsContainer;
  private FlexboxLayout riskFlagsContainer;
  private LinearLayout childCellContainer;
  private View childCellBottomLine;
  private View cellBottomLine;
  private AccordionSkeleton skeleton;
  private LinearLayout childCellContent;
  private AccordionPlaceholder noContentPlaceholder;

  private AntecedentsAccordion.Listener listener;
  private FrameLayout clickableView;
  private boolean closedCell;

  String antecedentsTextStyle;
  String iconAttr;
  boolean showBottomLineAttr;

  public AntecedentsAccordion(Context context) {
    super(context);
    init();
  }

  public AntecedentsAccordion(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public AntecedentsAccordion(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.accordion_antecedents_cell, this, true);

    bindView();
    setupView();

  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextCell);

    antecedentsTextStyle = a.getString(R.styleable.TextCell_leftText);
    iconAttr = a.getString(R.styleable.TextCell_rightText);
    showBottomLineAttr = a.getBoolean(R.styleable.TextCell_showBottomLine, true);

    a.recycle();
  }

  private void bindView() {
    cardTitle = (CustomTextView) findViewById(R.id.accordion_antecedents_cell_text);
    age = (CustomTextView) findViewById(R.id.accordion_antecedents_cell_age_text);
    numberOfGestations = (CustomTextView) findViewById(R.id.accordion_antecedents_cell_gestation_text);
    childbirthTypes = (CustomTextView) findViewById(R.id.accordion_antecedents_cell_childbirth_type_text);
    alertFlagsContainer = (FlexboxLayout) findViewById(R.id.accordion_antecedents_cell_alert_flag_container);
    icon = (ImageView) findViewById(R.id.accordion_antecedents_cell_icon);
    childCellContainer = (LinearLayout) findViewById(R.id.accordion_antecedents_cell_child_cell_content_layout);
    childCellBottomLine = findViewById(R.id.accordion_antecedents_child_cell_bottom_line);
    clickableView = (FrameLayout) findViewById(R.id.accordion_antecedents_cell_clickable_view);
    cellBottomLine = (View) findViewById(R.id.accordion_antecedents_cell_bottom_line);
    riskFlagsContainer = (FlexboxLayout) findViewById(R.id.accordion_antecedents_cell_risk_flag_container);
    skeleton = (AccordionSkeleton) findViewById(R.id.accordion_antecedents_cell_skeleton);
    childCellContent = (LinearLayout) findViewById(R.id.accordion_antecedents_child_cell_content);
    noContentPlaceholder = (AccordionPlaceholder) findViewById(R.id.accordion_antecedents_cell_no_content_placeholder);
  }

  private void setupView() {
    setupCustomTextViews(cardTitle, antecedentsTextStyle);
    childCellContainer.setVisibility(GONE);
    setChevronDown();

    if (!showBottomLineAttr) {
      childCellBottomLine.setVisibility(GONE);
    }

    clickableView.setOnClickListener(this::onIconClick);

    icon.setOnClickListener(this::onIconClick);
  }

  private void onIconClick(View view) {
    this.onDropdownClick();
  }

  private void onDropdownClick() {
    if (closedCell) {
      setChevronUp();
    } else {
      setChevronDown();
    }
  }

  private void onCellClick() {
    if (listener != null) {
      listener.onCellClick(getRootView());
    }
  }

  private void setChevronUp() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_up));
    childCellContainer.setVisibility(VISIBLE);
    toggleChevronState();
    onCellClick();
    cellBottomLine.setVisibility(GONE);
    childCellBottomLine.setVisibility(VISIBLE);
  }

  private void setChevronDown() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_down));
    childCellContainer.setVisibility(GONE);
    toggleChevronState();
    cellBottomLine.setVisibility(VISIBLE);
    childCellBottomLine.setVisibility(GONE);
  }

  private void toggleChevronState() {
    closedCell = !closedCell;
  }

  private void setupCustomTextViews(CustomTextView textView, String conditionTextStyle) {
    if (conditionTextStyle != null) {
      textView.setText(conditionTextStyle);
    }
  }

  public interface Listener {
    void onCellClick(View view);
  }

  public void setListener(AntecedentsAccordion.Listener listener) {
    this.listener = listener;
  }

  public void setAge(String cardTitle) {
    if (cardTitle != null && !cardTitle.isEmpty()) {
      this.age.setText(cardTitle);
    }
  }

  public void setNumberOfGestations(String cardTitle) {
    if (cardTitle != null && !cardTitle.isEmpty()) {
      this.numberOfGestations.setText(cardTitle);
    }
  }

  public void setChildbirthTypes(String cardTitle) {
    if (cardTitle != null && !cardTitle.isEmpty()) {
      this.childbirthTypes.setText(cardTitle);
    }
  }

  public void addAlertFlag(String flagText) {
    CustomFlag flag = new CustomFlag(getContext());
    flag.setFlagType(CustomFlag.FlagType.Neutral);
    flag.setFlagText(flagText);
    alertFlagsContainer.addView(flag);
  }

  public void addRiskFlag(String flagText) {
    CustomFlag flag = new CustomFlag(getContext());
    flag.setFlagType(CustomFlag.FlagType.Alert);
    flag.setFlagText(flagText);
    riskFlagsContainer.addView(flag);
  }

  public void setBottomLineVisibility(boolean shouldShowBottomLine) {
    childCellBottomLine.setVisibility(shouldShowBottomLine ? VISIBLE : GONE);
  }

  public void showSkeleton(){
    skeleton.setVisibility(VISIBLE);
    noContentPlaceholder.setVisibility(GONE);
    childCellContent.setVisibility(GONE);
  }

  public void showChildCellContent(){
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(GONE);
    childCellContent.setVisibility(VISIBLE);
  }

  public void showNoContentPlaceholder(){
    noContentPlaceholder.setImageAndText(getResources().getString(R.string.component_accordion_placeholder_no_content_text),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_cartaoprenatal_baby_mobile));
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(VISIBLE);
    childCellContent.setVisibility(GONE);
  }

  public void showErrorPlaceholder(){
    noContentPlaceholder.setImageAndText(getResources().getString(R.string.component_accordion_placeholder_error_text),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_placeholder_no_connection));
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(VISIBLE);
    childCellContent.setVisibility(GONE);
  }

}

