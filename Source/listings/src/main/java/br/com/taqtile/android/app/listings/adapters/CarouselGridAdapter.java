package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ICarouselGridItemModel;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 1/22/16.
 */
public class CarouselGridAdapter extends RecyclerView.Adapter<CarouselGridAdapter.ViewHolder> {
    private ArrayList<ICarouselGridItemModel> mArrayList;
    private CarouselGridAdapterListener mListener;

    public CarouselGridAdapter(ArrayList<ICarouselGridItemModel> items) {
        mArrayList = items;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.setData(mArrayList.get(position));
        holder.getClickable().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onCarouselGridItemClick(position, mArrayList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_carousel_grid_item, null);
        ViewHolder vH = new ViewHolder(v);
        return vH;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImage;

        CustomTextView mName;

        View mClickable;

        public ViewHolder(View view) {
            super(view);
            bindViews();
        }

        private void bindViews(){
            mImage = (ImageView) itemView.findViewById(R.id.component_product_carousel_grid_item_image);
            mName = (CustomTextView) itemView.findViewById(R.id.component_product_carousel_grid_item_name);
            mClickable = (View) itemView.findViewById(R.id.component_product_carousel_grid_item_clickable);
        }

        public void setData(ICarouselGridItemModel data) {
            if(data.getImageUrl() != null) {
                PicassoHelper.loadImageFromUrl(mImage, data.getImageUrl(), mImage.getContext());
            }
            mName.setText(data.getName());
        }

        public View getClickable() {
            return mClickable;
        }
    }

    public void setListener(CarouselGridAdapterListener mListener) {
        this.mListener = mListener;
    }

    public interface CarouselGridAdapterListener {
        void onCarouselGridItemClick(int position, ICarouselGridItemModel model);
    }
}
