package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/23/17.
 */

public class MedicalAppointmentCell extends LinearLayout {

  private CustomTextView cellName;
  private CustomTextView cellDate;
  private CustomTextView cellWeightHeight;
  private CustomTextView cellEdema;
  private CustomTextView cellArterialPressure;
  private CustomTextView cellUterineHeight;
  private CustomTextView cellFetalPresentation;
  private CustomTextView cellBcf;
  private CustomTextView cellFetalMovement;
  private CustomTextView cellUsingFerrousSulphate;
  private CustomTextView cellUsingFolicAcid;
  private CustomTextView cellTouch;
  private CustomTextViewWithHTML cellComplaint;

  public MedicalAppointmentCell(Context context) {
    super(context);
    init();
  }

  public MedicalAppointmentCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public MedicalAppointmentCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void setupAttrs(AttributeSet attrs) {
    // TODO: 5/23/17 ADD STYLES
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.medical_appointment_cell, this, true);

    bindView();
  }

  private void bindView() {
    cellName = (CustomTextView) findViewById(R.id.medical_appointment_cell_name);
    cellDate = (CustomTextView) findViewById(R.id.medical_appointment_cell_date);
    cellWeightHeight = (CustomTextView) findViewById(R.id.medical_appointment_cell_weight_height);
    cellEdema = (CustomTextView) findViewById(R.id.medical_appointment_cell_edema);
    cellArterialPressure = (CustomTextView) findViewById(R.id.medical_appointment_cell_arterial_pressure);
    cellUterineHeight = (CustomTextView) findViewById(R.id.medical_appointment_cell_uterine_height);
    cellFetalPresentation = (CustomTextView) findViewById(R.id.medical_appointment_cell_fetal_presentation);
    cellBcf = (CustomTextView) findViewById(R.id.medical_appointment_cell_bcf);
    cellFetalMovement = (CustomTextView) findViewById(R.id.medical_appointment_cell_fetal_movement);
    cellUsingFerrousSulphate = (CustomTextView) findViewById(R.id.medical_appointment_cell_using_ferrous_sulphate);
    cellUsingFolicAcid = (CustomTextView) findViewById(R.id.medical_appointment_cell_using_folic_acid);
    cellTouch = (CustomTextView) findViewById(R.id.medical_appointment_cell_touch);
    cellComplaint = (CustomTextViewWithHTML) findViewById(R.id.medical_appointment_cell_complaint);
  }

  public void setCellName(String cellName) {
    this.cellName.setText(cellName);
  }

  public void setCellDate(String cellDate) {
    this.cellDate.setText(cellDate);
  }

  public void setCellWeightHeight(String cellName) {
    this.cellWeightHeight.setText(cellName);
  }

  public void setCellEdema(String cellDate) {
    this.cellEdema.setText(cellDate);
  }

  public void setCellArterialPressure(String cellName) {
    this.cellArterialPressure.setText(cellName);
  }

  public void setCellUterineHeight(String cellDate) {
    this.cellUterineHeight.setText(cellDate);
  }

  public void setCellFetalPresentation(String cellName) {
    this.cellFetalPresentation.setText(cellName);
  }

  public void setCellBcf(String cellDate) {
    this.cellBcf.setText(cellDate);
  }

  public void setCellFetalMovement(String cellName) {
    this.cellFetalMovement.setText(cellName);
  }

  public void setCellUsingFerrousSulphate(String cellDate) {
    this.cellUsingFerrousSulphate.setText(cellDate);
  }

  public void setCellUsingFolicAcid(String cellName) {
    this.cellUsingFolicAcid.setText(cellName);
  }

  public void setCellTouch(String cellDate) {
    this.cellTouch.setText(cellDate);
  }

  public void setCellComplaint(String cellName) {
    this.cellComplaint.setTextWithHTML(cellName);
  }

}
