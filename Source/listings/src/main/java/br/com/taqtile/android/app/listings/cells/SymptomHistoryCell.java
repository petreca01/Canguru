package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomHistoryCell extends TextSubtextAndImageCell {
  public SymptomHistoryCell(@NonNull Context context) {
    super(context);
  }

  public SymptomHistoryCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public SymptomHistoryCell(@NonNull Context context, @Nullable AttributeSet attrs,
    @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override public boolean getSubheader() {
    return true;
  }

  @Override public int getImageId() {
    return R.drawable.ic_symptom;
  }

  @Override public boolean showBiggerImage() {
    return false;
  }

  @Override public void setSubheader(String subheader) {
    String dateFormatted = getContext().getString(R.string.component_symptoms_history_cell_date,
      DateFormatterHelper.formatDateWithoutHours(subheader));
    super.setSubheader(dateFormatted);
  }
}
