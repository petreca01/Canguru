package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareWithPartnerCell extends TextSubtextAndImageCell {
  public HealthcareWithPartnerCell(@NonNull Context context) {
    super(context);
  }

  public HealthcareWithPartnerCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public HealthcareWithPartnerCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  public boolean getSubheader() {
    return true;
  }

  @Override
  public int getImageId() {
    return R.drawable.ic_perfil_plano_de_saude_sintomas;
  }

  @Override
  public boolean showBiggerImage() {
    return true;
  }
}
