package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedCommunityViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 3/18/17.
 */

public class SocialFeedCommunityImplViewModel implements SocialFeedCommunityViewModel {

  private Integer postId;
  private Integer userId;
  private String iconUrl;
  private String posterName;
  private String date;
  private String postTitle;
  private String body;
  private int likes;
  private int commentsCount;
  private boolean isLikeActivated;
  private boolean isCertifiedProfessional;

  public SocialFeedCommunityImplViewModel(Integer postId, Integer userId, String iconUrl,
    String posterName, String date, String postTitle, String body, int likes,
    int commentsCount, boolean isLikeActivated,
    boolean isCertifiedProfessional) {
    this.postId = postId;
    this.userId = userId;
    this.iconUrl = iconUrl;
    this.posterName = posterName;
    this.date = date;
    this.postTitle = postTitle;
    this.body = body;
    this.likes = likes;
    this.commentsCount = commentsCount;
    this.isLikeActivated = isLikeActivated;
    this.isCertifiedProfessional = isCertifiedProfessional;
  }

  public SocialFeedCommunityImplViewModel(Integer postId, Integer userId, String iconUrl,
                                          String posterName, String date, String body, int likes, boolean isLikeActivated) {
    this.postId = postId;
    this.userId = userId;
    this.iconUrl = iconUrl;
    this.posterName = posterName;
    this.date = date;
    this.body = body;
    this.likes = likes;
    this.isLikeActivated = isLikeActivated;
  }

  @Override
  public void setPostId(Integer postId) {
    this.postId = postId;
  }

  @Override
  public Integer getPostId() {
    return postId;
  }

  @Override public void setProfileUrl(String iconUrl) {

    this.iconUrl = iconUrl;
  }

  @Override public String getProfileUrl() {
    return iconUrl;
  }

  @Override public void setPosterName(String posterName) {

    this.posterName = posterName;
  }

  @Override public String getPosterName() {
    return posterName;
  }

  @Override public void setDate(String date) {

    this.date = date;
  }

  @Override public String getDate() {
    return date;
  }

  @Override public long getDateInMilliseconds() {
    return DateFormatterHelper.getDateInMilliseconds(date);
  }

  @Override public void setPostTitle(String postTitle) {

    this.postTitle = postTitle;
  }

  @Override public String getPostTitle() {
    return postTitle;
  }

  @Override public void setPostBody(String body) {

    this.body = body;
  }

  @Override public String getPostBody() {
    return body;
  }

  @Override public void setLikesCount(int likes) {

    this.likes = likes;
  }

  @Override public int getLikesCount() {
    return likes;
  }

  @Override public void setCommentsCount(int commentsCount) {

    this.commentsCount = commentsCount;
  }

  @Override public int getCommentsCount() {
    return commentsCount;
  }

  @Override
  public void setLikeActivated(boolean isLikeActivated) {
    this.isLikeActivated = isLikeActivated;
  }

  @Override
  public boolean getLikeActivated() {
    return isLikeActivated;
  }

  @Override public void setCertifiedProfessional(boolean certifiedProfessional) {
    isCertifiedProfessional = certifiedProfessional;
  }

  @Override public boolean isCertifiedProfessional() {
    return isCertifiedProfessional;
  }

  public Integer getUserId() {
    return userId;
  }
}
