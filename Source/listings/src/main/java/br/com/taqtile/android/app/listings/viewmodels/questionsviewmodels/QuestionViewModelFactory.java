package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by felipesabino on 5/22/17.
 */

public class QuestionViewModelFactory {

  private static final String YES = "1";
  private static final String NO = "0";

  public static QuestionViewModel newBooleanType(int questionId, String question,
                                                 String description, String answer) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_BOOLEAN, question,
      description, getBooleanAlternatives(), getAnswersFromValue(answer));
  }

  public static QuestionViewModel newImcType(int questionId, String question, String description,
                                             String answer) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_IMC, question, description,
      null, getAnswersFromValue(answer));
  }

  public static QuestionViewModel newMultipleType(int questionId, String question,
                                                  String description, List<String> alternatives,
                                                  List<String> answers) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_MULTIPLE, question,
      description, alternatives, answers);
  }

  public static QuestionViewModel newRangeType(int questionId, String question, String description,
                                               String answer, Integer min, Integer max) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_RANGE, question,
      description, null, getAnswersFromValue(answer), min, max);
  }

  public static QuestionViewModel newSelectType(int questionId, String question, String description,
                                                List<String> alternatives, String answer) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_SELECT, question,
      description, alternatives, getAnswersFromValue(answer));
  }

  public static QuestionViewModel newVarcharType(int questionId, String question, String description,
                                              String answer) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_VARCHAR, question, description,
      null, getAnswersFromValue(answer));
  }

  public static QuestionViewModel newTextType(int questionId, String question, String description,
                                              String answer) {
    return new QuestionViewModel(questionId, QuestionType.QUESTION_TYPE_TEXT, question, description,
      null, getAnswersFromValue(answer));
  }

  private static List<String> getBooleanAlternatives() {
    List<String> booleanAlternatives = new ArrayList<>();
    booleanAlternatives.add(YES);
    booleanAlternatives.add(NO);
    return booleanAlternatives;
  }

  private static List<String> getAnswersFromValue(String answer) {

    if (answer != null) {
      return Collections.singletonList(answer);
    } else {
      return Collections.emptyList();
    }
  }
}
