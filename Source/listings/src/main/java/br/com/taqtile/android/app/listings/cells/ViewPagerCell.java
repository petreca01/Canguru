package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 11/4/16.
 */

public class ViewPagerCell extends FrameLayout {

  private ImageView image;
  private ImageView placeholder;

  public ViewPagerCell(Context context) {
    super(context);
    init(context);
  }

  public ViewPagerCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public ViewPagerCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.viewpager_cell, this, true);
    bindViews();
    setupView();
  }

  private void setupView() {
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_gray));
  }

  private void bindViews() {
    image = (ImageView) findViewById(R.id.viewpager_cell_image);

  }

  public void setImage(String imageUrl) {
    if (imageUrl != null) {
      Picasso.with(getContext().getApplicationContext())
        .load(imageUrl)
        .fit()
        .centerCrop()
        .into(image);
    }
  }

  public void showPlaceholder() {
    placeholder.setVisibility(VISIBLE);
  }

  public void hidePlaceholder() {
    placeholder.setVisibility(GONE);
  }

  public void setPlaceholder(Drawable drawable) {
    image.setImageDrawable(drawable);
  }

  public void setPlaceholder(String placeholderUrl) {
    if (placeholderUrl != null) {
      Picasso.with(getContext().getApplicationContext())
        .load(placeholderUrl)
        .fit()
        .centerCrop()
        .into(image);
    }
  }

}
