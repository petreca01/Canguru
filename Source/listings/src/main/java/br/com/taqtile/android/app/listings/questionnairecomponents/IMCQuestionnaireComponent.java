package br.com.taqtile.android.app.listings.questionnairecomponents;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.edittexts.HeightLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.WeightLabelEditTextCaption;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 22/05/17.
 */

public class IMCQuestionnaireComponent extends LinearLayout implements QuestionnaireResult {

  private DescriptionHeaderWithImage questionDescription;
  private String questionId;
  private List<String> answer;
  private WeightLabelEditTextCaption weightForm;
  private HeightLabelEditTextCaption heightForm;
  private CustomTextView imc;
  private TemplateButton calculateImcButton;
  private AnswerListener answerListener;

  public IMCQuestionnaireComponent(Context context) {
    super(context);
  }

  public IMCQuestionnaireComponent(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public IMCQuestionnaireComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public IMCQuestionnaireComponent(Context context, AnswerListener listener, QuestionViewModel questionViewModel) {
    super(context);
    this.questionId = String.valueOf(questionViewModel.getQuestionId());
    this.answerListener = listener;
    init(questionViewModel.getQuestion(), questionViewModel.getDescription(),
      questionViewModel.getAnswer());
  }

  private void init(String question, String questionDescription, String answer) {
    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
    View v = layoutInflater.inflate(R.layout.component_imc_questionnaire, this, true);

    bindViews();
    setupAnswer();
    setupButtons();
    setupListeners();
    this.questionDescription.setTitle(question);
    this.questionDescription.setDescription(questionDescription);
    setAnswerIfAlreadyAnswered(answer);
  }

  private void bindViews() {
    weightForm = (WeightLabelEditTextCaption) findViewById(R.id.component_imc_questionnaire_weight);
    heightForm = (HeightLabelEditTextCaption) findViewById(R.id.component_imc_questionnaire_height);
    imc = (CustomTextView) findViewById(R.id.component_imc_questionnaire_imc_result);
    calculateImcButton = (TemplateButton) findViewById(R.id.component_imc_questionnaire_calculate_imc);
    questionDescription = (DescriptionHeaderWithImage) findViewById(R.id.component_imc_questionnaire_question_description);
  }

  private void setupAnswer() {
    answer = new ArrayList<>();
  }

  private void setupButtons() {
    calculateImcButton.setText(getResources().getString(R.string.component_imc_questionnaire_calculate_button_text));
    calculateImcButton.setOnClickListener(v -> {
      if (answerListener != null) {
        imc.setText(calculateIMC());
        setIMC();
      }
    });
  }


  private void setupListeners() {
    weightForm.setOnFocusChangeListener((l, v) -> answer.clear());
    heightForm.setOnFocusChangeListener((l, v) -> answer.clear());
  }

  private String calculateIMC() {
    if (checkIfImcValuesAreValid()) {
      float weight = Integer.valueOf(weightForm.getText());
      float height = Integer.valueOf(heightForm.getText());
      return String.format(getContext().getString(R.string.format_float_2_digits),
        weight / (((height / 100) * (height / 100))));
    } else {
      return "";
    }
  }

  private boolean checkIfImcValuesAreValid() {
    return areFormsFilled() && areFormsTextsValid();
  }

  private boolean areFormsFilled() {
    return !weightForm.getText().isEmpty() && !heightForm.getText().isEmpty();
  }

  private boolean areFormsTextsValid() {
    int weight = Integer.valueOf(weightForm.getText());
    int height = Integer.valueOf(weightForm.getText());
    return weight >= 0 && height >= 0;
  }

  private void setAnswerIfAlreadyAnswered(String answer) {
    if (answer != null) {
      imc.setText(answer);
      setIMC();
      this.answer.clear();
      calculateImcButton.setText(R.string.component_imc_questionnaire_recalculate_button_text);
    }
  }

  @Override
  public List<String> getResult() {
    return answer;
  }

  private List<String> setIMC() {
    answer.clear();
    answer.add((String) this.imc.getText());
    answerListener.onAnswerSelected(this);
    return answer;
  }

  @Override
  public boolean isResultValid() {
    return checkIMCValidity();
  }

  private boolean checkIMCValidity() {
    return (!this.answer.isEmpty() && this.answer.size() == 1 && this.answer.get(0) != null &&
      !this.answer.get(0).isEmpty());
  }

  @Override
  public String getQuestionId() {
    return questionId;
  }

}
