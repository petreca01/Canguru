package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 13/07/17.
 */

public interface PaginationViewModel {
  void setTotalPosts(int totalPosts);
  int getTotalPosts();
}
