package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ForumCell;

/**
 * Created by taqtile on 3/24/17.
 */

public class ForumViewHolder extends RecyclerView.ViewHolder {
    ForumCell forumCell;

    public ForumViewHolder(View itemView) {
        super(itemView);
        itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.forumCell = (ForumCell) itemView;
    }

    public ForumCell getForumCell() {
        return forumCell;
    }
}
