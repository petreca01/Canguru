package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.ButtonViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SymptomHistoryCellViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SymptomsHeaderViewHolder;
import br.com.taqtile.android.app.listings.cells.ButtonCell;
import br.com.taqtile.android.app.listings.cells.SymptomHistoryCell;
import br.com.taqtile.android.app.listings.cells.SymptomsHeaderCell;

import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_HISTORY_FOOTER;
import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_HISTORY_HEADER;
import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_HISTORY_SYMPTOM;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomsHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<SymptomHistoryViewModel> symptomsList;
  private Context context;
  private SymptomsHistoryAdapterListener listener;

  public SymptomsHistoryAdapter(List<SymptomHistoryViewModel> symptomsList, Context context) {
    this.symptomsList = symptomsList;
    this.context = context;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == SYMPTOMS_HISTORY_SYMPTOM) {
      return new SymptomHistoryCellViewHolder(new SymptomHistoryCell(parent.getContext()));
    } else if (viewType == SYMPTOMS_HISTORY_FOOTER) {
      return new ButtonViewHolder(new ButtonCell(parent.getContext()));
    } else {
      return new SymptomsHeaderViewHolder(new SymptomsHeaderCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case SYMPTOMS_HISTORY_HEADER:
        setSymptomsHeader(holder);
        break;
      case SYMPTOMS_HISTORY_FOOTER:
        setSymptomFooter(holder);
        break;
      case SYMPTOMS_HISTORY_SYMPTOM:
        setSymptomCell(holder, position);
        break;
    }
  }

  private void setSymptomCell(RecyclerView.ViewHolder holder, int position) {
    SymptomHistoryViewModel symptomHistoryViewModel = getSymptom(position);

    SymptomHistoryCellViewHolder symptomHistoryCellViewHolder = (SymptomHistoryCellViewHolder) holder;
    symptomHistoryCellViewHolder.getSymptomHistoryCell().setHeader(symptomHistoryViewModel.getName());
    symptomHistoryCellViewHolder.getSymptomHistoryCell().setSubheader(symptomHistoryViewModel.getDate());
    symptomHistoryCellViewHolder.getSymptomHistoryCell().setBottomLineVisibility(position != getItemCount() - 2);
  }

  private void setSymptomsHeader(RecyclerView.ViewHolder holder) {
    SymptomsHeaderViewHolder symptomsHeaderViewHolder = (SymptomsHeaderViewHolder) holder;
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderTitle(context.getString(R.string.component_symptoms_history_header_title));
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderDescription(context.getString(R.string.component_symptoms_history_header_description));
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderImage(R.drawable.ic_gestacao_plan);
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setSectionHeaderTitle(context.getString(R.string.component_symptoms_history_header_section_title));
  }

  private void setSymptomFooter(RecyclerView.ViewHolder holder) {
    ButtonViewHolder buttonViewHolder = (ButtonViewHolder) holder;
    buttonViewHolder.getButtonCell().setButtonText(context.getString(R.string.component_symptoms_history_button_cell_text));
    buttonViewHolder.getButtonCell().setListener(this::onSendHistoryClicked);
  }

  private void onSendHistoryClicked(View view) {
    if (listener != null) {
      listener.onSendHistoryClicked(view);
    }
  }

  @Override
  public int getItemViewType(int position) {
    if (position == 0) {
      return SYMPTOMS_HISTORY_HEADER;
    } else if(position == symptomsList.size() + 1) {
      return SYMPTOMS_HISTORY_FOOTER;
    } else {
      return SYMPTOMS_HISTORY_SYMPTOM;
    }
  }

  @Override
  public int getItemCount() {
    return symptomsList.size() + 2;
  }

  public SymptomHistoryViewModel getSymptom(int position) {
    return symptomsList.get(position - 1);
  }

  public interface SymptomsHistoryAdapterListener {
    void onSendHistoryClicked(View view);
  }

  public void setListener(SymptomsHistoryAdapterListener listener) {
    this.listener = listener;
  }
}
