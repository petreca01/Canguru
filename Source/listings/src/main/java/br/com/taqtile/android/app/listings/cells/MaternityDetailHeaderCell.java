package br.com.taqtile.android.app.listings.cells;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.LinkedHashMap;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.components.BirthsProgressBarComponent;
import br.com.taqtile.android.app.listings.components.ProgressBarAndCaptionComponent;
import br.com.taqtile.android.app.listings.components.RatingViewComponent;
import br.com.taqtile.android.app.listings.support.helpers.StaticMapHelper;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 10/05/17.
 */

public class MaternityDetailHeaderCell extends RelativeLayout {
  // TODO: 5/11/17 THIS COMPONENT SHOULD BE SPLIT INTO SEVERAL COMPONENTS
  private static final int STATIC_MAP_HEIGHT = 216;

  private CircleImageView maternityLogo;
  private CustomTextView maternityName;
  private CustomTextView maternityAddress;
  private CustomTextView maternityPhoneNumber;
  private CustomTextView maternityOwner;
  private BirthsProgressBarComponent birthsProgressBarComponent;
  private CustomTextView birthsStatisticsSource;
  private ProgressBarAndCaptionComponent excellentRatingProgressBar;
  private ProgressBarAndCaptionComponent veryGoodRatingProgressBar;
  private ProgressBarAndCaptionComponent normalRatingProgressBar;
  private ProgressBarAndCaptionComponent badRatingProgressBar;
  private ProgressBarAndCaptionComponent terribleRatingProgressBar;
  private ImageView phoneIcon;
  private TemplateButton ratingMaternityButton;
  private RatingViewComponent ratingViewComponent;
  private ImageView maternityMap;
  private LinearLayout phoneContainer;
  private LinearLayout maternityInfoContainer;
  private ImageView maternityPicture;
  private LinearLayout additionalDetails;
  private LinearLayout additionalDetailsRootLayout;

  private MaternityDetailHeaderCellListener listener;

  public MaternityDetailHeaderCell(Context context) {
    super(context);
    init();
  }

  public MaternityDetailHeaderCell(Context context, MaternityDetailHeaderCellListener listener) {
    super(context);
    this.listener = listener;
    init();
  }

  public MaternityDetailHeaderCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public MaternityDetailHeaderCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_maternity_detail_header_view, this, true);

    bindView();

}

  private void bindView() {
    maternityLogo = (CircleImageView) findViewById(R.id.component_maternity_detail_logo);
    maternityName = (CustomTextView) findViewById(R.id.component_maternity_detail_name);
    maternityAddress = (CustomTextView) findViewById(R.id.component_maternity_detail_address);
    maternityPhoneNumber = (CustomTextView) findViewById(R.id.component_maternity_detail_phone_number);
    maternityOwner = (CustomTextView) findViewById(R.id.component_maternity_detail_owner_maternity);
    birthsProgressBarComponent = (BirthsProgressBarComponent) findViewById(R.id.component_maternity_detail_births_progress);
    birthsStatisticsSource = (CustomTextView) findViewById(R.id.component_maternity_detail_births_statistics_source);
    excellentRatingProgressBar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_detail_excellent_rating_progressbar);
    veryGoodRatingProgressBar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_detail_very_good_rating_progressbar);
    normalRatingProgressBar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_detail_normal_rating_progressbar);
    badRatingProgressBar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_detail_bad_rating_progressbar);
    terribleRatingProgressBar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_detail_terrible_rating_progressbar);
    ratingViewComponent = (RatingViewComponent) findViewById(R.id.component_maternity_detail_rating_component);
    ratingMaternityButton = (TemplateButton) findViewById(R.id.component_maternity_detail_rating_button);
    maternityMap = (ImageView) findViewById(R.id.component_maternity_detail_static_map);
    phoneIcon = (ImageView) findViewById(R.id.component_maternity_detail_header_view_phone_icon);
    phoneContainer = (LinearLayout) findViewById(R.id.component_maternity_detail_header_view_phone_icon_container);
    maternityInfoContainer = (LinearLayout) findViewById(R.id.component_maternity_detail_header_view_header_text_container);
    maternityPicture = (ImageView) findViewById(R.id.component_maternity_detail_header_view_maternity_picture);
    additionalDetails = (LinearLayout) findViewById(R.id.component_maternity_detail_header_view_additional_details);
    additionalDetailsRootLayout = (LinearLayout) findViewById(R.id.component_maternity_detail_header_view_additional_details_layout);
  }

  public void setupView(MaternityViewModel maternityViewModel) {
    if (maternityViewModel.getLogo() != null) {
      setLogo(maternityViewModel.getLogo());
    }
    PicassoHelper.loadImageFromUrl(
      maternityLogo,
      maternityViewModel.getLogo(),
      getContext(),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_maternidade_placeholder));
    setMaternityMap(maternityViewModel.getAddress());
    maternityName.setText(maternityViewModel.getName());
    setMaternityPhoneNumber(maternityViewModel.getPhone());
    setMaternityOwner();
    birthsProgressBarComponent.setNaturalBirthPercent(maternityViewModel.getCurrentPercentNormalBirth().intValue());
    birthsProgressBarComponent.setCesareanBirthPercent(maternityViewModel.getCurrentPercentCsections().intValue());
    birthsStatisticsSource.setText(String.format(getContext().getString(R.string.component_maternity_header_cell_source),
      maternityViewModel.getCurrentBirthsSource()));
    if (maternityViewModel.getTotalRates() > 0) {
      excellentRatingProgressBar.setProgress(getPercentage(maternityViewModel.getFiveRates(), maternityViewModel.getTotalRates()));
      veryGoodRatingProgressBar.setProgress(getPercentage(maternityViewModel.getFourRates(), maternityViewModel.getTotalRates()));
      normalRatingProgressBar.setProgress(getPercentage(maternityViewModel.getThreeRates(), maternityViewModel.getTotalRates()));
      badRatingProgressBar.setProgress(getPercentage(maternityViewModel.getTwoRates(), maternityViewModel.getTotalRates()));
      terribleRatingProgressBar.setProgress(getPercentage(maternityViewModel.getOneRates(), maternityViewModel.getTotalRates()));
    }
    if (maternityViewModel.getRate() != null) {
      ratingViewComponent.setRating(maternityViewModel.getRate().intValue(), maternityViewModel.getTotalRates());
    }

    setupListener();

    addAdditionalDetails(getAdditionalDetails(maternityViewModel));
    setMaternityPicture(maternityViewModel.getImage() != null ? maternityViewModel.getImage() : "");

  }

  private void setMaternityOwner() {

    //String htmlString="<u>Solicitar alteração de dados</u>";
    this.maternityOwner.setText(R.string.component_maternity_detail_owner_maternity);

  }

  private boolean setMaternityAddress(String maternityAddress) {
    if (maternityAddress == null || maternityAddress.isEmpty()) {
      this.maternityAddress.setVisibility(GONE);
      maternityMap.setVisibility(GONE);
      return false;
    }
    this.maternityAddress.setText(maternityAddress);
    return true;
  }

  private void setMaternityPhoneNumber(String maternityPhoneNumber) {
    if (maternityPhoneNumber == null || maternityPhoneNumber.isEmpty()) {
      this.phoneContainer.setVisibility(GONE);
      return;
    }
    this.maternityPhoneNumber.setText(maternityPhoneNumber);
  }

  private int getPercentage(Integer dividend, Integer divisor) {
    return (100 * dividend / divisor);
  }

  private void setMaternityMap(String maternityAddress) {
    if (!setMaternityAddress(maternityAddress)) {
      return;
    }
    StaticMapHelper mapHelper = new StaticMapHelper();
    Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    int width = (int) (outMetrics.widthPixels / getResources().getDisplayMetrics().density);
    String mapUrl = mapHelper.getMapUrl(maternityAddress,
      width,
      STATIC_MAP_HEIGHT);
    PicassoHelper.loadImageFromUrlAndChangeVisibility(
      maternityMap,
      mapUrl,
      getContext());
  }

  public void setMaternityPicture(String imageUrl){
    maternityPicture.setVisibility(VISIBLE);
    PicassoHelper.loadImageFromUrl(maternityPicture, imageUrl, getContext());
  }

  private void setLogo(String logo) {
    PicassoHelper.loadImageFromUrl(maternityLogo,
      logo,
      getContext(),
      R.drawable.ic_maternidade_placeholder);
  }

  private void setupListener() {
    ratingMaternityButton.setOnClickListener(v -> {
      if (listener != null) {
        listener.onRatingMaternityClicked();
      }
    });

    phoneContainer.setOnClickListener(v -> {
      if (listener != null) {
        listener.onMaternityPhoneNumberClicked();
      }
    });

    maternityMap.setOnClickListener(v -> {
      if (listener != null) {
        listener.onMaternityMapClicked();
      }
    });

    maternityOwner.setOnClickListener(v -> {
      if (listener != null) {
        listener.onMaternityOwnerClicked();
      }
    });
  }

  private void addAdditionalDetails(HashMap<String, String> additionalDetails){
    if (additionalDetails == null || additionalDetails.isEmpty()){
      additionalDetailsRootLayout.setVisibility(GONE);
      return;
    }
    additionalDetailsRootLayout.setVisibility(VISIBLE);
    this.additionalDetails.removeAllViews();
    for (HashMap.Entry<String, String> entry : additionalDetails.entrySet()){
      this.additionalDetails.addView(getAdditionalDetailsContainerWithHeaderAndDescription(entry.getKey(), entry.getValue()));
    }
  }

  private LinearLayout getAdditionalDetailsContainerWithHeaderAndDescription(String header, String description){
    LinearLayout headerAndDescriptionContainer = setupAdditionalDetailsContainer();
    addAdditionalDetailsHeaderAndDescriptionToContainer(headerAndDescriptionContainer, header, description);
    return headerAndDescriptionContainer;
  }

  private LinearLayout setupAdditionalDetailsContainer(){
    LinearLayout headerAndDescriptionContainer = new LinearLayout(getContext());
    headerAndDescriptionContainer.setOrientation(LinearLayout.VERTICAL);
    headerAndDescriptionContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    return headerAndDescriptionContainer;
  }

  private void addAdditionalDetailsHeaderAndDescriptionToContainer(LinearLayout container, String header, String description){
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_xx_small);
    container.addView(getAdditionalDetailHeader(header), layoutParams);
    container.addView(getAdditionalDetailDescription(description));
  }

  private CustomTextView getAdditionalDetailHeader(String header){
    CustomTextView headerView = new CustomTextView(new ContextThemeWrapper(getContext(), R.style.TextElement_DT), null, R.style.TextElement_DT);
    headerView.setText(header);
    return headerView;
  }

  private CustomTextView getAdditionalDetailDescription(String description){
    CustomTextView descriptionView = new CustomTextView(new ContextThemeWrapper(getContext(), R.style.TextElement_DD));
    descriptionView.setText(description);
    return descriptionView;
  }

  private LinkedHashMap<String, String> getAdditionalDetails(MaternityViewModel maternityViewModel){
    LinkedHashMap<String, String> map = new LinkedHashMap<>();
    if (isValid(maternityViewModel.getAbout())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_about),
        maternityViewModel.getAbout());
    }
    if (isValid(maternityViewModel.getBirthSchool())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_birth_school),
        maternityViewModel.getBirthSchool());
    }
    if (isValid(maternityViewModel.getVisitRules())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_visit_rules),
        maternityViewModel.getVisitRules());
    }
    if (isValid(maternityViewModel.getAvailableMidwife())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_midwife),
        maternityViewModel.getAvailableMidwife());
    }
    if (isValid(maternityViewModel.getBathBirth())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_ppp_room),
        maternityViewModel.getBathBirth());
    }
    if (isValid(maternityViewModel.getPppRoom())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_bath_room),
        maternityViewModel.getPppRoom());
    }
    if (isValid(maternityViewModel.getCrowdRoom())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_midwife),
        maternityViewModel.getCrowdRoom());
    }
    if (isValid(maternityViewModel.getAccreditations())) {
      map.put(getContext().getResources().getString(R.string.component_maternity_detail_accreditations)
        , maternityViewModel.getAccreditations());
    }

    return map;
  }

  private boolean isValid(String text) {
    return text != null && !text.isEmpty();
  }

  public interface MaternityDetailHeaderCellListener {
    void onRatingMaternityClicked();

    void onMaternityPhoneNumberClicked();

    void onMaternityOwnerClicked();

    void onMaternityMapClicked();


  }
}
