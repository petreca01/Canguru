package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.SkeletonLoadingGenerator;

/**
 * Created by taqtile on 4/28/17.
 */

public class PostPlaceholderCell extends FrameLayout implements SkeletonLoadingGenerator.SkeletonLoadingWithShimmer {

    private FrameLayout shimmerLayout;

  public static PostPlaceholderCell newInstance(Context context){
    return new PostPlaceholderCell(context);
  }

  public PostPlaceholderCell(Context context) {
    super(context);
    init();
  }

  public PostPlaceholderCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public PostPlaceholderCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_social_placeholder_card, this, true);

    bindViews();

    setupView();

  }

  private void bindViews(){
    shimmerLayout = (FrameLayout) findViewById(R.id.component_social_placeholder_card_shimmer_layout);
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    setLayoutParams(layoutParams);
  }

  @Override
  public View instantiate() {
    return PostPlaceholderCell.newInstance(getContext());
  }

}

