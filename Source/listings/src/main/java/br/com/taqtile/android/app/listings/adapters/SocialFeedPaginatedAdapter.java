package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.SocialFeedChannelCardViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SocialFeedCommunityCardViewHolder;
import br.com.taqtile.android.app.listings.cells.ChannelCard;
import br.com.taqtile.android.app.listings.cells.CommunityCard;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedChannelViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedCommunityViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;

import static br.com.taqtile.android.app.listings.Constants.CHANNEL_CELL;
import static br.com.taqtile.android.app.listings.Constants.COMMUNITY_CELL;

/**
 * Created by taqtile on 3/1/17.
 */

public class SocialFeedPaginatedAdapter extends RecyclerViewPaginatedAdapter {

  private CommunityCard.CommunityCardListener communityCardListener;
  private ChannelCard.ChannelCardListener channelCardListener;
  private List<SocialFeedViewModel> socialFeedList;

  public SocialFeedPaginatedAdapter(Context context, List<SocialFeedViewModel> socialFeedList,
    CommunityCard.CommunityCardListener communityCardListener,
    ChannelCard.ChannelCardListener channelCardListener) {
    super(context);
    this.communityCardListener = communityCardListener;
    this.channelCardListener = channelCardListener;
    this.socialFeedList = socialFeedList;
  }

  @Override public int getLoadingLayout() {
    return R.layout.component_social_placeholder_card;
  }

  @Override
  public RecyclerView.ViewHolder createAdditionalViewHolder(ViewGroup parent, int viewType) {
    if (viewType == COMMUNITY_CELL) {
      return new SocialFeedCommunityCardViewHolder(new CommunityCard(parent.getContext()));
    } else {
      return new SocialFeedChannelCardViewHolder(new ChannelCard(parent.getContext()));
    }
  }

  @Override public int getAdditionalViewTypes(int position) {
    SocialFeedViewModel socialFeedViewModel = socialFeedList.get(position);

    if (socialFeedViewModel instanceof SocialFeedChannelViewModel) {
      return CHANNEL_CELL;
    } else {
      return COMMUNITY_CELL;
    }
  }

  @Override public void bindAdditionalViewHolder(RecyclerView.ViewHolder holder, int position) {
    SocialFeedViewModel socialFeedViewModel = socialFeedList.get(position);

    if (socialFeedViewModel instanceof SocialFeedChannelViewModel) {
      ChannelCard channelCard = ((SocialFeedChannelCardViewHolder) holder).getChannelCard();
      setupChannelCard(channelCard, (SocialFeedChannelViewModel) socialFeedViewModel);
    } else if (socialFeedViewModel instanceof SocialFeedCommunityViewModel) {
      CommunityCard communityCard = ((SocialFeedCommunityCardViewHolder) holder).getCommunityCard();
      setupCommunityCard(communityCard, (SocialFeedCommunityViewModel) socialFeedViewModel,
        position);
    }
  }

  //TODO move setup methods to a binder
  private void setupCommunityCard(CommunityCard communityCard,
    SocialFeedCommunityViewModel socialFeedViewModel, int position) {
    setCommunityCardProperties(communityCard, socialFeedViewModel);
    setupCommunityCardListeners(communityCard);
  }

  private void setCommunityCardProperties(CommunityCard communityCard,
    SocialFeedCommunityViewModel socialFeedViewModel) {
    communityCard.setCommentsCounter(socialFeedViewModel.getCommentsCount());
    communityCard.setBody(socialFeedViewModel.getPostBody());
    communityCard.setHeading(socialFeedViewModel.getPostTitle());
    communityCard.setLikesCounter(socialFeedViewModel.getLikesCount());
    communityCard.setDate(socialFeedViewModel.getDate());
    communityCard.setProfileImage(socialFeedViewModel.getProfileUrl());
    communityCard.setProfileName(socialFeedViewModel.getPosterName());
    communityCard.setPostId(socialFeedViewModel.getPostId());
    communityCard.setUserId(socialFeedViewModel.getUserId());
    communityCard.setLiked(socialFeedViewModel.getLikeActivated());
  }

  private void setupCommunityCardListeners(CommunityCard communityCard) {
    communityCard.setListener(communityCardListener);
  }

  private void setupChannelCard(ChannelCard channelCard,
    SocialFeedChannelViewModel socialFeedViewModel) {
    setChannelCardProperties(channelCard, socialFeedViewModel);
    setupChannelCardListener(channelCard);
  }

  private void setChannelCardProperties(ChannelCard channelCard,
    SocialFeedChannelViewModel socialFeedViewModel) {
    channelCard.setCommentsCounter(socialFeedViewModel.getCommentsCount());
    channelCard.setBody(socialFeedViewModel.getPostBody());
    channelCard.setHeading(socialFeedViewModel.getPostTitle());
    channelCard.setLikesCounter(socialFeedViewModel.getLikesCount());
    channelCard.setDate(socialFeedViewModel.getDate(), socialFeedViewModel.getPosterName());
    channelCard.setProfileName(((SocialFeedChannelImplViewModel) socialFeedViewModel).getChannelName());
    channelCard.setImage(socialFeedViewModel.getImageUrl());
    channelCard.setPostId(socialFeedViewModel.getPostId());
    channelCard.setChannelId(socialFeedViewModel.getChannelId());
    channelCard.setLiked(socialFeedViewModel.getLikeActivated());
  }

  private void setupChannelCardListener(ChannelCard channelCard) {
    channelCard.setListener(channelCardListener);
  }

  public void modifySpecifCard(RecyclerView.ViewHolder holder, int position) {
    SocialFeedViewModel socialFeedViewModel = socialFeedList.get(position);

    if (socialFeedViewModel instanceof SocialFeedChannelViewModel) {
      ((SocialFeedChannelCardViewHolder) holder)
        .getChannelCard()
        .updateCard(socialFeedViewModel.getLikeActivated(),
          socialFeedViewModel.getLikesCount(),
          socialFeedViewModel.getCommentsCount());

    } else if (socialFeedViewModel instanceof SocialFeedCommunityViewModel) {
      ((SocialFeedCommunityCardViewHolder) holder)
        .getCommunityCard()
        .updateCard(socialFeedViewModel.getLikeActivated(),
                    socialFeedViewModel.getLikesCount(),
                    socialFeedViewModel.getCommentsCount());
    }
  }

  @Override public int getAdditionalItemCount() {
    return socialFeedList.size();
  }
}
