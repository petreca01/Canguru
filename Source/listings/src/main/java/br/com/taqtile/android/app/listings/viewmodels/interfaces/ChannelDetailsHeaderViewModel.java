package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 06/04/17.
 */

public interface ChannelDetailsHeaderViewModel extends ListingsViewModel {
  void setId(String id);
  String getId();
  void setImageUrl(String imageUrl);
  String getImageUrl();
  void setChannelName(String channelName);
  String getChannelName();
  void setChannelDescription(String channelDescription);
  String getChannelDescription();
  void setIsFollowing(boolean isFollowing);
  boolean isFollowing();
}
