package br.com.taqtile.android.app.listings.grids;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import java.util.ArrayList;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ICarouselGridItemModel;
import br.com.taqtile.android.app.misc.utils.AsyncRequestErrorPlaceholder;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;


/**
 * Created by taqtile on 1/22/16.
 */
public class CarouselGridWrapper extends FrameLayout {

    CarouselGrid mCarousel;

    AsyncRequestErrorPlaceholder mErrorPlaceholder;

    public CarouselGridWrapper(Context context) {
        super(context);
        initialSetup(context);
    }

    public CarouselGridWrapper(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialSetup(context);
    }

    public CarouselGridWrapper(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialSetup(context);
    }

    private  void bindViews(){
        mCarousel = (CarouselGrid) findViewById(R.id.component_carousel_wrapper_carousel);
        mErrorPlaceholder = (AsyncRequestErrorPlaceholder) findViewById(R.id.component_product_carousel_wrapper_error_placeholder);
    }

    public void setLoadingState() {
        FadeEffectHelper.fadeInView(mCarousel);
        mErrorPlaceholder.setVisibility(GONE);

        mCarousel.setLoadingState();
    }

    public void setData(ArrayList<ICarouselGridItemModel> data) {
        FadeEffectHelper.fadeInView(mCarousel);
        mErrorPlaceholder.setVisibility(GONE);

        mCarousel.setData(data);
    }

    public void setErrorState(OnClickListener tryAgainListener) {
        FadeEffectHelper.fadeInView(mErrorPlaceholder);
        mCarousel.setVisibility(GONE);

        mErrorPlaceholder.setListener(tryAgainListener);
    }

    public void setProductCarouselGridListener(CarouselGrid.CarouselGridListener listener) {
        mCarousel.setListener(listener);
    }

    private void initialSetup(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_carousel_wrapper, this, true);
        bindViews();
    }
}
