package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;

/**
 * Created by taqtile on 5/8/17.
 */

public class AgendaItemViewModel implements Parcelable, AgendaFeedViewModel {

  private Boolean loaded;
  private Integer id;
  private Boolean operatorHighlight;
  private Integer quarter;
  private String name;
  private String code;
  private Integer typeId;
  private String type;
  private String weekStart;
  private Integer weekEnd;
  private String dayEnd;
  private String description;
  private String descriptionImageUrl;
  private String completeDescription;
  private String observation;
  private Integer done;
  private String createdAt;
  private String updatedAt;
  private String markingDate;
  private String markingObservation;
  private Integer isAppointment;


  public AgendaItemViewModel() {
  }

  public AgendaItemViewModel(String weekStart) {
    this.weekStart = weekStart;
  }

  public AgendaItemViewModel(Integer id, Integer quarter,
                             String name, Integer typeId, String type, String weekStart,
                             Integer weekEnd, String dayEnd, String description, Integer done) {
    this.id = id;
    this.quarter = quarter;
    this.name = name;
    this.typeId = typeId;
    this.type = type;
    this.weekStart = weekStart;
    this.weekEnd = weekEnd;
    this.dayEnd = dayEnd;
    this.description = description;
    this.done = done;
  }

  public AgendaItemViewModel(Boolean loaded, Integer id, Boolean operatorHighlight, Integer quarter,
                             String name, String code, Integer typeId, String type, String weekStart,
                             Integer weekEnd, String dayEnd, String description,
                             String descriptionImageUrl, String completeDescription,
                             String observation, Integer done, String createdAt, String updatedAt,
                             String markingDate, String markingObservation, Integer isAppointment) {
    this.loaded = loaded;
    this.id = id;
    this.operatorHighlight = operatorHighlight;
    this.quarter = quarter;
    this.name = name;
    this.code = code;
    this.typeId = typeId;
    this.type = type;
    this.weekStart = weekStart;
    this.weekEnd = weekEnd;
    this.dayEnd = dayEnd;
    this.description = description;
    this.descriptionImageUrl = descriptionImageUrl;
    this.completeDescription = completeDescription;
    this.observation = observation;
    this.done = done;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.markingDate = markingDate;
    this.markingObservation = markingObservation;
    this.isAppointment = isAppointment;
  }

  public Boolean getLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public Boolean getOperatorHighlight() {
    return operatorHighlight;
  }

  public Integer getQuarter() {
    return quarter;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public Integer getTypeId() {
    return typeId;
  }

  public String getType() {
    return type;
  }

  public String getWeekStart() {
    return weekStart;
  }

  public Integer getWeekEnd() {
    return weekEnd;
  }

  public String getDayEnd() {
    return dayEnd;
  }

  public String getDescription() {
    return description;
  }

  public String getDescriptionImageUrl() {
    return descriptionImageUrl;
  }

  public String getCompleteDescription() {
    return completeDescription;
  }

  public String getObservation() {
    return observation;
  }

  public Integer getDone() {
    return done;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getMarkingDate() {
    return markingDate;
  }

  public String getMarkingObservation() {
    return markingObservation;
  }

  public Integer getIsAppointment() {
    return isAppointment;
  }

  public boolean thereIsFullDescription() {
    return (this.getCompleteDescription() != null && !this.getCompleteDescription().isEmpty());
  }

  public boolean thereIsDescription() {
    return (this.getDescription() != null && !this.getDescription().isEmpty());
  }

  public boolean isAppointmentAttended() {
    return this.getDone().equals(1);
  }

  public boolean isAppointmentScheduled() {
    return (this.getMarkingDate() != null && !this.getMarkingDate().isEmpty());
  }

  public boolean isAppointmentNotScheduled() {
    return (this.getMarkingDate() == null || this.getMarkingDate().isEmpty());
  }

  public void setDone(Integer done) {
    this.done = done;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.loaded);
    dest.writeValue(this.id);
    dest.writeValue(this.operatorHighlight);
    dest.writeValue(this.quarter);
    dest.writeString(this.name);
    dest.writeString(this.code);
    dest.writeValue(this.typeId);
    dest.writeString(this.type);
    dest.writeString(this.weekStart);
    dest.writeValue(this.weekEnd);
    dest.writeString(this.dayEnd);
    dest.writeString(this.description);
    dest.writeString(this.descriptionImageUrl);
    dest.writeString(this.completeDescription);
    dest.writeString(this.observation);
    dest.writeValue(this.done);
    dest.writeString(this.createdAt);
    dest.writeString(this.updatedAt);
    dest.writeString(this.markingDate);
    dest.writeString(this.markingObservation);
    dest.writeValue(this.isAppointment);
  }

  protected AgendaItemViewModel(Parcel in) {
    this.loaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    this.operatorHighlight = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.quarter = (Integer) in.readValue(Integer.class.getClassLoader());
    this.name = in.readString();
    this.code = in.readString();
    this.typeId = (Integer) in.readValue(Integer.class.getClassLoader());
    this.type = in.readString();
    this.weekStart = in.readString();
    this.weekEnd = (Integer) in.readValue(Integer.class.getClassLoader());
    this.dayEnd = in.readString();
    this.description = in.readString();
    this.descriptionImageUrl = in.readString();
    this.completeDescription = in.readString();
    this.observation = in.readString();
    this.done = (Integer) in.readValue(Integer.class.getClassLoader());
    this.createdAt = in.readString();
    this.updatedAt = in.readString();
    this.markingDate = in.readString();
    this.markingObservation = in.readString();
    this.isAppointment = (Integer) in.readValue(Integer.class.getClassLoader());
  }

  public static final Creator<AgendaItemViewModel> CREATOR = new Creator<AgendaItemViewModel>() {
    @Override
    public AgendaItemViewModel createFromParcel(Parcel source) {
      return new AgendaItemViewModel(source);
    }

    @Override
    public AgendaItemViewModel[] newArray(int size) {
      return new AgendaItemViewModel[size];
    }
  };
}
