package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.AccordionPlaceholder;
import br.com.taqtile.android.app.listings.cells.MaternityNoCommentPlaceholder;
import br.com.taqtile.android.app.listings.cells.TextWithLabelAndDotCell;

/**
 * Created by taqtile on 26/06/17.
 */

public class MaternityNoCommentPlaceholderViewHolder extends RecyclerView.ViewHolder {

  private AccordionPlaceholder maternityNoCommentPlaceholder;

  public MaternityNoCommentPlaceholderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.maternityNoCommentPlaceholder = (AccordionPlaceholder) itemView;
  }

  public AccordionPlaceholder getMaternityNoCommentPlaceholder(){
    return maternityNoCommentPlaceholder;
  }

}
