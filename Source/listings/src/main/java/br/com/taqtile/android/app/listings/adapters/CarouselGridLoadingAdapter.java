package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import br.com.taqtile.android.app.listings.R;


/**
 * Created by taqtile on 1/22/16.
 */
public class CarouselGridLoadingAdapter extends RecyclerView.Adapter<CarouselGridLoadingAdapter.ViewHolder> {

    private CarouselGridLoadingAdapterListener mListener;

    private final static int LOADING_GRID_DEFAULT_ITEM_NUMBER = 3;

    @Override
    public int getItemCount() {
        if(mListener != null) {
            final float itemWidth = mListener.getExternalContext().getResources().getDimension(R.dimen.component_product_carousel_grid_item_width);
            final float windowWidth = getWindowWidth();
            final int numberOfItemsToFillScreen = (int) (windowWidth / itemWidth) + 1;
            return numberOfItemsToFillScreen;
        } else {
            return LOADING_GRID_DEFAULT_ITEM_NUMBER;
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_carousel_grid_loading_item, null);
        ViewHolder vH = new ViewHolder(v);
        return vH;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }
    }

    // src: http://stackoverflow.com/a/1016941
    private float getWindowWidth() {
        if(mListener != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mListener.getExternalContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size.x;
        } else return 0;

    }

    public interface CarouselGridLoadingAdapterListener{
        Context getExternalContext();
    }

    public void setListener(CarouselGridLoadingAdapterListener carouselGridLoadingAdapterListener){ mListener = carouselGridLoadingAdapterListener;}

}
