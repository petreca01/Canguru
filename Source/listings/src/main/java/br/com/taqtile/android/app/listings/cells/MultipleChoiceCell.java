package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/19/17.
 */

public class MultipleChoiceCell extends LinearLayout {

  private AppCompatCheckBox check;
  private CustomTextView text;
  private boolean isSelected;

  private Listener listener;

  String textAttr;

  public MultipleChoiceCell(Context context) {
    super(context);
    init();
  }

  public MultipleChoiceCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public MultipleChoiceCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_multiple_choice_cell, this, true);

    bindView();
    setupView();
    setupButtons();
  }

  private void bindView() {
    check = (AppCompatCheckBox) findViewById(R.id.component_form_choice_cell_checkbox);
    text = (CustomTextView) findViewById(R.id.component_form_choice_cell_text);
  }

  private void setupButtons() {
    this.setOnClickListener(v -> {
      if (listener != null) {
        setIsSelected(!isSelected);
        listener.onCellClick((String) text.getText(), this);
      }
    });
  }

  private void toggleImageVisibility() {
    if (isSelected) {
      showCheck();
    } else {
      hideCheck();
    }
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MultipleChoiceCell);

    textAttr = a.getString(R.styleable.MultipleChoiceCell_textCell);

    a.recycle();
  }

  private void setupView() {

    if (textAttr != null) {
      text.setText(textAttr);
    }
    setIsSelected(false);
  }

  public void setIsSelected(boolean isSelected) {
    this.isSelected = isSelected;
    toggleImageVisibility();
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setText(String text) {
    this.text.setText(text);
  }

  public String getText() {
    return (String) text.getText();
  }

  public void showCheck() {
    check.setChecked(true);
  }

  public void hideCheck() {
    check.setChecked(false);
  }

  public interface Listener {
    void onCellClick(String choice, MultipleChoiceCell multipleChoiceCell);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

}
