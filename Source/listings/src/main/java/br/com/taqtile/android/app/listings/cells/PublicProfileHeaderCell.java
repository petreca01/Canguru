package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 25/04/17.
 */

public class PublicProfileHeaderCell extends FrameLayout {

  CustomTextView motherName;
  CustomTextView babyName;
  CustomTextView location;
  CustomTextView pregnancyAge;
  CustomTextView description;
  private FrameLayout descriptionLayout;
  private LinearLayout locationContainer;

  public PublicProfileHeaderCell(@NonNull Context context) {
    super(context);
    init();
  }

  public PublicProfileHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public PublicProfileHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_public_profile_header_cell, this, true);

    bindViews();
    setupView();
  }

  private void bindViews() {
    motherName = (CustomTextView) findViewById(R.id.component_public_profile_header_name);
    babyName = (CustomTextView) findViewById(R.id.component_public_profile_header_baby_name);
    pregnancyAge = (CustomTextView) findViewById(R.id.component_public_profile_header_pregnancy_age);
    locationContainer = (LinearLayout) findViewById(R.id.component_public_profile_header_location_container);
    location = (CustomTextView) findViewById(R.id.component_public_profile_header_location);
    description = (CustomTextView) findViewById(R.id.component_public_profile_header_description);
    descriptionLayout = (FrameLayout) findViewById(R.id.component_public_profile_header_description_layout);
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setLayoutParams(layoutParams);
  }

  public void setMotherName(String name) {
    motherName.setText(name);
  }

  public void setBabyName(String name) {
    if (isValidString(name)) {
      babyName.setText(String.format(
        getResources().getString(R.string.fragment_profile_menu_additional_image_mother_baby_format),
        name));
      babyName.setVisibility(View.VISIBLE);
    }
  }

  public void setPregnancyAge(Integer babyAgeWeek, Integer babyAgeDay) {
    String pregnancyWeek = "";
    String pregnancyDay = "";

    if (babyAgeWeek != null && babyAgeWeek > 0 ){

      pregnancyWeek = getResources().getQuantityString(R.plurals.babyAgeWeeks,
        babyAgeWeek, babyAgeWeek);
    }
    if (babyAgeDay != null && babyAgeDay > 0 ){

      pregnancyDay = getResources().getQuantityString(R.plurals.babyAgeDays,
        babyAgeDay, babyAgeDay);

      pregnancyDay = String.format(getResources().getString(R.string.fragment_profile_menu_additional_image_mother_pregnancy_age_days),pregnancyDay);
    }

    if (!pregnancyWeek.isEmpty() || !pregnancyDay.isEmpty()) {
      this.pregnancyAge.setText(String.format(
        getResources().getString(R.string.fragment_profile_menu_additional_image_mother_pregnancy_age_format),
        pregnancyWeek, pregnancyDay));
      this.pregnancyAge.setVisibility(View.VISIBLE);
    }
  }

  public void setLocation(String city, String state) {
    if (isValidString(city) && isValidString(state)) {
      location.setText(String.format(
        getResources().getString(R.string.fragment_profile_menu_additional_image_location_format),
        city, state));
      locationContainer.setVisibility(VISIBLE);
    }
  }

  public void hideLocation() {
    locationContainer.setVisibility(GONE);
  }

  public void setDescription(String description) {
    if (isValidString(description)) {
      this.description.setText(description);
      this.descriptionLayout.setVisibility(VISIBLE);
    }
  }

  private boolean isValidString(String name) {
    return name != null && !name.isEmpty();
  }

}
