package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 10/05/17.
 */

public class TextWithLabelAndDotCell extends LinearLayout {

  private int id;

  private CustomTextView header;
  private CustomTextView label;
  private SpannableStringBuilder imageInTextSpan;
  private Typeface regularTypeface;
  private Typeface mediumTypeface;
  private View separator;

  private OnClickListener clickListener;
  private Listener listener;

  private boolean showSeparatorAttr;

  public TextWithLabelAndDotCell(Context context) {
    super(context);
    init();
  }

  public TextWithLabelAndDotCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
    setupAttrs(attrs);
  }

  public TextWithLabelAndDotCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
    setupAttrs(attrs);
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_with_label_and_dot_cell, this, true);


    bindViews();
    setupView();
    setupTypefaces();
    setupButtons();
  }

  private void bindViews() {
    header = (CustomTextView) findViewById(R.id.component_text_with_label_and_dot_cell_header);
    label = (CustomTextView) findViewById(R.id.component_text_with_label_and_dot_cell_label);
    separator = (View) findViewById(R.id.component_text_with_label_and_dot_separator);
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextWithLabelAndDotCell);

    showSeparatorAttr = a.getBoolean(R.styleable.TextWithLabelAndDotCell_showSeparator, false);

    if (showSeparatorAttr) {
      separator.setVisibility(VISIBLE);
    } else {
      separator.setVisibility(GONE);
    }

    a.recycle();
  }

  private void setupView() {
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setOrientation(VERTICAL);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setLayoutParams(layoutParams);
  }

  public void showSeparator() {
    separator.setVisibility(VISIBLE);
  }

  public void hideSeparator() {
    separator.setVisibility(GONE);
  }

  private void setupButtons() {
    clickListener = v -> {
      if (listener != null) {
        listener.onViewClick(id);
      }
    };
    enableClick();
  }

  private void setupTypefaces() {
    mediumTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-Medium.ttf");
    regularTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-Regular.ttf");
    imageInTextSpan = new SpannableStringBuilder();
  }

  public void setHeader(String header, boolean isVisualized) {
    if (isVisualized) {
      showUnhighlightedStatus();
      this.header.setText(header);
    } else {
      showHighlightedStatus();
      setTextWithBadge(header);
    }
  }

  private void setTextWithBadge(String leftText) {
    int badgeWidth = (int) getResources().getDimension(R.dimen.notification_badge_width);
    int badgeHeight = (int) getResources().getDimension(R.dimen.notification_badge_height);
    Drawable badge = ContextCompat.getDrawable(getContext(), R.drawable.bkg_notification_badge);
    badge.setBounds(0, 0, badgeWidth, badgeHeight);
    //Add an extra space on the string to place the badge
    leftText += "  ";
    imageInTextSpan = new SpannableStringBuilder(leftText);
    imageInTextSpan.setSpan(new ImageSpan(badge, ImageSpan.ALIGN_BASELINE), imageInTextSpan.length() - 1, imageInTextSpan.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
    this.header.setText(imageInTextSpan, TextView.BufferType.SPANNABLE);
  }

  public void showHighlightedStatus() {
    highlightLeftText();
  }

  public void showUnhighlightedStatus() {
    unhighlightLeftText();
    imageInTextSpan.clearSpans();
  }

  public void setViewIndex(int id) {
    this.id = id;
  }

  private void highlightLeftText() {
    header.setTypeface(mediumTypeface);
  }

  private void unhighlightLeftText() {
    header.setTypeface(regularTypeface);
  }

  private void disableClick() {
    this.setOnClickListener(null);
  }

  private void enableClick() {
    this.setOnClickListener(clickListener);
  }

  public void setHeader(String header) {
    this.header.setText(header);
  }

  public void setLabel(String label) {
    this.label.setText(label);
  }

  public interface Listener {
    void onViewClick(int id);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

}
