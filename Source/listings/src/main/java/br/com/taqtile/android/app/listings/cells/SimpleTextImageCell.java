package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 3/24/17.
 */

public class SimpleTextImageCell extends TextAndImageCell {

    public SimpleTextImageCell(Context context) {
        super(context);
        init();
    }

    public SimpleTextImageCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SimpleTextImageCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        hideLeftCounter();
        hideRightCounter();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textLayout.getLayoutParams();
        layoutParams.addRule(Gravity.CENTER_VERTICAL);
        textLayout.setLayoutParams(layoutParams);
    }

    @Override
    protected boolean isInstitutionNameVisible() {
        return false;
    }

    @Override
    protected String getFormattedLeftCounterText(int leftCounter) {
        return "";
    }

    @Override
    protected String getFormattedRightCounterText(int rightCounter) {
        return "";
    }

    @Override
    protected Drawable getIcon() {
        return null;
    }
}
