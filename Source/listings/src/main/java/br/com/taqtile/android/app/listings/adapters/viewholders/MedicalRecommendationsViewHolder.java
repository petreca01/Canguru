package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.MedicalRecommendationsCell;

/**
 * Created by taqtile on 04/05/17.
 */

public class MedicalRecommendationsViewHolder extends RecyclerView.ViewHolder {

  MedicalRecommendationsCell medicalRecommendationsCell;

  public MedicalRecommendationsViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.medicalRecommendationsCell = (MedicalRecommendationsCell) itemView;
  }

  public MedicalRecommendationsCell getMedicalRecommendationsCell() {
    return medicalRecommendationsCell;
  }
}
