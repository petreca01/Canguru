package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.ConditionsCellViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SymptomsHeaderViewHolder;
import br.com.taqtile.android.app.listings.cells.ConditionCell;
import br.com.taqtile.android.app.listings.cells.SymptomsHeaderCell;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_GUIDE_HEADER;
import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_GUIDE_SYMPTOM;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomsGuideAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<SymptomViewModel> symptomsList;
  private SymptomsGuideAdapterListener listener;
  private Context context;

  public SymptomsGuideAdapter(List<SymptomViewModel> symptomsList, Context context) {
    this.symptomsList = symptomsList;
    this.context = context;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == SYMPTOMS_GUIDE_SYMPTOM) {
      return new ConditionsCellViewHolder(new ConditionCell(parent.getContext()));
    } else {
      return new SymptomsHeaderViewHolder(new SymptomsHeaderCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case SYMPTOMS_GUIDE_HEADER:
        setSymptomsHeader(holder);
        break;
      case SYMPTOMS_GUIDE_SYMPTOM:
        setSymptomCell(holder, position - 1);
        break;
    }
  }

  private void setSymptomCell(RecyclerView.ViewHolder holder, int position) {
    SymptomViewModel symptomViewModel = symptomsList.get(position);

    ConditionsCellViewHolder conditionsCellViewHolder = (ConditionsCellViewHolder) holder;
    ConditionCell conditionCell = conditionsCellViewHolder.getConditionCell();

    conditionCell.setConditionText(symptomViewModel.getName());
    conditionCell.setBottomLineVisibility(position != getItemCount());
    conditionCell.setListener((view, subitemPosition) ->
      this.symptomCellClick(view, symptomViewModel.getSymptom()));

    if (symptomViewModel.getChildren() != null && symptomViewModel.getChildren().size() > 0) {
      List<String> subSymptomsName = StreamSupport.stream(symptomViewModel.getChildren())
        .map(SubSymptomViewModel::getName)
        .collect(Collectors.toList());

      conditionCell.setSubConditionCellContainer(subSymptomsName);

      conditionCell.setSubItemListener((view, subitemPosition) ->
        this.symptomCellClick(view, symptomViewModel.getChildren().get(subitemPosition)));
      conditionCell.showIcon();
      conditionCell.setHasSubItems();
    }

  }

  private void symptomCellClick(View view, SubSymptomViewModel subSymptomViewModel) {
    if (listener != null) {
      listener.onSymptomCellClicked(subSymptomViewModel);
    }
  }

  private void setSymptomsHeader(RecyclerView.ViewHolder holder) {
    SymptomsHeaderViewHolder symptomsHeaderViewHolder = (SymptomsHeaderViewHolder) holder;
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderTitle(context.getString(R.string.component_symptoms_guide_header_title));
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderDescription(context.getString(R.string.component_symptoms_guide_header_description));
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setDescriptionHeaderImage(R.drawable.ic_gestacao_heart_filled);
    symptomsHeaderViewHolder.getSymptomsHeaderCell().setSectionHeaderTitle(context.getString(R.string.component_symptoms_guide_header_section_title));
  }

  @Override
  public int getItemViewType(int position) {
    // TODO: redo here
    switch (position) {
      case 0:
        return SYMPTOMS_GUIDE_HEADER;
      default:
        return SYMPTOMS_GUIDE_SYMPTOM;
    }
  }

  @Override
  public int getItemCount() {
    return symptomsList.size() + 1;
  }

  public interface SymptomsGuideAdapterListener {
    void onSymptomCellClicked(SubSymptomViewModel subSymptomViewModel);
  }

  public void setListener(SymptomsGuideAdapterListener listener) {
    this.listener = listener;
  }

  public void setSymptomsList(List<SymptomViewModel> symptomsList) {
    this.symptomsList = symptomsList;
  }
}
