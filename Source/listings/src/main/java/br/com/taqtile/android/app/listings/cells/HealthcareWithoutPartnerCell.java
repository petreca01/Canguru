package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareWithoutPartnerCell extends TextSubtextAndImageCell {

  public HealthcareWithoutPartnerCell(@NonNull Context context) {
    super(context);
    init();
  }

  public HealthcareWithoutPartnerCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public HealthcareWithoutPartnerCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){

  }

  @Override
  public boolean getSubheader() {
    return false;
  }

  @Override
  public int getImageId() {
    return R.drawable.ic_perfil_plano_de_saude_sintomas;
  }

  @Override
  public boolean showBiggerImage() {
    return true;
  }
}
