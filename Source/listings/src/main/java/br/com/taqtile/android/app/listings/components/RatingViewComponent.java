package br.com.taqtile.android.app.listings.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Arrays;
import java.util.Collection;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/9/17.
 */

public class RatingViewComponent extends LinearLayout {

  private ImageView firstStar;
  private ImageView secondStar;
  private ImageView thirdStar;
  private ImageView fourthStar;
  private ImageView fifthStar;
  private CustomTextView ratingNumber;
  private Collection<ImageView> stars;

  public RatingViewComponent(Context context) {
    super(context);
    init(context);
  }

  public RatingViewComponent(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public RatingViewComponent(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_rating_view, this, true);
    bindViews();
    stars = Arrays.asList(firstStar, secondStar, thirdStar, fourthStar, fifthStar);
  }

  private void bindViews() {
    firstStar = (ImageView) findViewById(R.id.component_rating_view_firstStar);
    secondStar = (ImageView) findViewById(R.id.component_rating_view_secondStar);
    thirdStar = (ImageView) findViewById(R.id.component_rating_view_thirdStar);
    fourthStar = (ImageView) findViewById(R.id.component_rating_view_fourthStar);
    fifthStar = (ImageView) findViewById(R.id.component_rating_view_fifthStar);
    ratingNumber = (CustomTextView) findViewById(R.id.component_rating_view_total_rating);
  }

  public void setRating(int rating, int totalRating) {
    int countDown = rating;
    for (ImageView star : stars) {
      if (countDown > 0) {
        star.setImageResource(R.drawable.ic_star_full);
      } else {
        star.setImageResource(R.drawable.ic_star_empty);
      }
      countDown -= 1;
    }
    ratingNumber.setText(
      String.format(
        getContext().getString(
          R.string.component_maternity_header_cell_total_rating),
        totalRating));
  }
}
