package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.PublicProfileHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SectionHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SocialFeedCommunityCardViewHolder;
import br.com.taqtile.android.app.listings.cells.CommunityCard;
import br.com.taqtile.android.app.listings.cells.PublicProfileHeaderCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedCommunityViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;

import static br.com.taqtile.android.app.listings.Constants.PUBLIC_PROFILE_HEADER;
import static br.com.taqtile.android.app.listings.Constants.PUBLIC_PROFILE_SECTION_SEPARATOR;
import static br.com.taqtile.android.app.listings.Constants.PUBLIC_PROFILE_SOCIAL_CARD;

/**
 * Created by taqtile on 24/04/17.
 */

public class PublicProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<SocialFeedViewModel> socialFeedViewModels;
  private ProfileDetailsViewModel profileDetailsViewModel;
  private CommunityCard.CommunityCardListener listener;

  public PublicProfileAdapter(List<SocialFeedViewModel> socialFeedViewModels,
                              ProfileDetailsViewModel profileDetailsViewModel) {
    this.socialFeedViewModels = socialFeedViewModels;
    this.profileDetailsViewModel = profileDetailsViewModel;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == PUBLIC_PROFILE_SOCIAL_CARD) {
      return new SocialFeedCommunityCardViewHolder(new CommunityCard(parent.getContext()));
    } else if (viewType == PUBLIC_PROFILE_HEADER) {
      return new PublicProfileHeaderViewHolder(new PublicProfileHeaderCell(parent.getContext()));
    } else {
      return new SectionHeaderViewHolder(new SectionHeader(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case PUBLIC_PROFILE_HEADER:
        setupPublicProfileHeader(holder);
        break;
      case PUBLIC_PROFILE_SOCIAL_CARD:
        setCommunityCardProperties(
          ((SocialFeedCommunityCardViewHolder) holder).getCommunityCard(),
          (SocialFeedCommunityViewModel) socialFeedViewModels.get(position - 2));
        break;
      case PUBLIC_PROFILE_SECTION_SEPARATOR:
        setupSectionHeader(holder);
        break;
    }
  }

  @Override
  public int getItemViewType(int position) {
    // TODO: redo here (do not use position)
    switch (position) {
      case 0:
        return PUBLIC_PROFILE_HEADER;
      case 1:
        return PUBLIC_PROFILE_SECTION_SEPARATOR;
      default:
        return PUBLIC_PROFILE_SOCIAL_CARD;
    }
  }

  private void setupPublicProfileHeader(RecyclerView.ViewHolder holder) {
    PublicProfileHeaderViewHolder publicProfileHeaderViewHolder = (PublicProfileHeaderViewHolder) holder;
    publicProfileHeaderViewHolder.getPublicProfileHeaderCell().setMotherName(profileDetailsViewModel.getName());
    publicProfileHeaderViewHolder.getPublicProfileHeaderCell().setBabyName(profileDetailsViewModel.getBabyName());
    publicProfileHeaderViewHolder.getPublicProfileHeaderCell().setPregnancyAge(profileDetailsViewModel.getPregnancyWeeks(), profileDetailsViewModel.getPregnancyDays());
    publicProfileHeaderViewHolder.getPublicProfileHeaderCell().setDescription(profileDetailsViewModel.getAbout());
    if (profileDetailsViewModel.isLocationPrivacyAgreed()) {
      publicProfileHeaderViewHolder.getPublicProfileHeaderCell()
        .setLocation(profileDetailsViewModel.getCity(), profileDetailsViewModel.getState());
    } else {
      publicProfileHeaderViewHolder.getPublicProfileHeaderCell().hideLocation();
    }
  }

  private void setCommunityCardProperties(CommunityCard communityCard,
                                          SocialFeedCommunityViewModel socialFeedViewModel) {
    communityCard.setCommentsCounter(socialFeedViewModel.getCommentsCount());
    communityCard.setBody(socialFeedViewModel.getPostBody());
    communityCard.setHeading(socialFeedViewModel.getPostTitle());
    communityCard.setLikesCounter(socialFeedViewModel.getLikesCount());
    communityCard.setDate(socialFeedViewModel.getDate());
    communityCard.setProfileImage(socialFeedViewModel.getProfileUrl());
    communityCard.setProfileName(socialFeedViewModel.getPosterName());
    communityCard.setPostId(socialFeedViewModel.getPostId());
    communityCard.setUserId(socialFeedViewModel.getUserId());
    communityCard.setLiked(socialFeedViewModel.getLikeActivated());
    if (listener != null) {
      communityCard.setListener(listener);
    }
  }

  private void setupSectionHeader(RecyclerView.ViewHolder holder) {
    SectionHeaderViewHolder sectionHeader = (SectionHeaderViewHolder) holder;
    Context context = ((SectionHeaderViewHolder) holder).getSectionHeader().getContext();
    sectionHeader.getSectionHeader().setText((context.getResources().getString(R.string.channel_details_section_header_text)));
  }

  public void setListener(CommunityCard.CommunityCardListener listener) {
    this.listener = listener;
  }

  @Override
  public int getItemCount() {
    return socialFeedViewModels.size() + 2;
  }

  public void setProfileDetailsViewModel(ProfileDetailsViewModel profileDetailsViewModel) {
    this.profileDetailsViewModel = profileDetailsViewModel;
  }
}
