package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import br.com.taqtile.android.app.listings.adapters.viewholders.MedicalRecommendationsHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.MedicalRecommendationsViewHolder;
import br.com.taqtile.android.app.listings.cells.MedicalRecommendationsCell;
import br.com.taqtile.android.app.listings.cells.MedicalRecommendationsHeaderCell;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import java.util.List;

import static br.com.taqtile.android.app.listings.Constants.MEDICAL_RECOMMENDATIONS_HEADER;
import static br.com.taqtile.android.app.listings.Constants.MEDICAL_RECOMMENDATIONS_RECOMMENDATION;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private List<MedicalRecommendationViewModel> medicalRecommendationsList;
  private MedicalRecommendationsCell.MedicalRecomendationsListener listener;

  public MedicalRecommendationsAdapter(
    List<MedicalRecommendationViewModel> medicalRecommendationsList) {
    this.medicalRecommendationsList = medicalRecommendationsList;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == MEDICAL_RECOMMENDATIONS_RECOMMENDATION) {
      return new MedicalRecommendationsViewHolder(
        new MedicalRecommendationsCell(parent.getContext()));
    } else {
      return new MedicalRecommendationsHeaderViewHolder(
        new MedicalRecommendationsHeaderCell(parent.getContext()));
    }
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case MEDICAL_RECOMMENDATIONS_HEADER:
        break;
      case MEDICAL_RECOMMENDATIONS_RECOMMENDATION:
        setMedicalRecommendationCell(holder, getMedicalRecommendationViewModel(position));
        break;
    }
  }

  private void setMedicalRecommendationCell(RecyclerView.ViewHolder holder,
    MedicalRecommendationViewModel medicalRecommendationViewModel) {
    MedicalRecommendationsViewHolder medicalRecommendationsViewHolder =
      (MedicalRecommendationsViewHolder) holder;

    medicalRecommendationsViewHolder.getMedicalRecommendationsCell()
      .setupWithMedicalRecommendationViewModel(medicalRecommendationViewModel, listener);
  }

  @Override public int getItemViewType(int position) {
    switch (position) {
      case 0:
        return MEDICAL_RECOMMENDATIONS_HEADER;
      default:
        return MEDICAL_RECOMMENDATIONS_RECOMMENDATION;
    }
  }

  // We've to count one more because of the header
  @Override public int getItemCount() {
    return medicalRecommendationsList.size() + 1;
  }

  // We'e to count one less because of the header
  private MedicalRecommendationViewModel getMedicalRecommendationViewModel(int position) {
    return medicalRecommendationsList.get(position - 1);
  }

  public void setListener(MedicalRecommendationsCell.MedicalRecomendationsListener listener) {
    this.listener = listener;
  }
}
