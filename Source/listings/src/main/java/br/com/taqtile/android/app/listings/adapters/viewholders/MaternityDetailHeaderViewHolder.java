package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import br.com.taqtile.android.app.listings.cells.MaternityDetailHeaderCell;

/**
 * Created by taqtile on 10/05/17.
 */

public class MaternityDetailHeaderViewHolder extends RecyclerView.ViewHolder {

  MaternityDetailHeaderCell maternityDetailHeaderCell;

  public MaternityDetailHeaderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.WRAP_CONTENT));
    maternityDetailHeaderCell = (MaternityDetailHeaderCell) itemView;
  }

  public MaternityDetailHeaderCell getMaternityDetailHeaderCell() {
    return maternityDetailHeaderCell;
  }
}
