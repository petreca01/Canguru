package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ButtonCell;

/**
 * Created by taqtile on 09/05/17.
 */

public class ButtonViewHolder extends RecyclerView.ViewHolder {

  ButtonCell buttonCell;

  public ButtonViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.buttonCell = (ButtonCell) itemView;
  }

  public ButtonCell getButtonCell(){
    return buttonCell;
  }
}
