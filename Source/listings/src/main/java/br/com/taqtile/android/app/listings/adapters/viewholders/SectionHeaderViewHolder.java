package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.SectionHeader;

/**
 * Created by taqtile on 06/04/17.
 */

public class SectionHeaderViewHolder extends RecyclerView.ViewHolder {
  SectionHeader sectionHeader;

  public SectionHeaderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.sectionHeader = (SectionHeader) itemView;
  }

  public SectionHeader getSectionHeader() {
    return sectionHeader;
  }
}
