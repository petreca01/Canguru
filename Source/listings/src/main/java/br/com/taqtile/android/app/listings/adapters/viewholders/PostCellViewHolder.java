package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.PostCell;

/**
 * Created by taqtile on 13/04/17.
 */

public class PostCellViewHolder extends RecyclerView.ViewHolder {

  PostCell postCell;

  public PostCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.postCell = (PostCell) itemView;
  }

  public PostCell getPostCell() {
    return postCell;
  }

}
