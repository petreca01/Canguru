package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.PopupMenu;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 17/04/17.
 */

public class CommentCell extends FrameLayout {

  private LinearLayout root;
  private FrameLayout iconLayout;
  private CircleImageView icon;
  private LinearLayout userNameLayout;
  private CustomTextView userName;
  private CustomTextView commentTime;
  private CustomTextView body;
  private CounterWithDrawable likes;
  private CustomTextView certifiedLabel;
  private AppCompatImageView certifiedBadge;
  private View bottomSeparator;

  private Integer commentId;
  private CommentCellListener listener;

  private boolean isCommentMine = false;

  public CommentCell(Context context) {
    super(context);
    init();
  }

  public CommentCell(Context context, CommentCellListener listener) {
    super(context);
    this.listener = listener;
    init();
  }

  public CommentCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CommentCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_comment_cell, this, true);

    bindViews();
    setupView();
    setupListeners();
  }

  private void bindViews() {
    root = (LinearLayout) findViewById(R.id.component_comment_cell_root);
    iconLayout = (FrameLayout) findViewById(R.id.component_comment_cell_icon_layout);
    icon = (CircleImageView) findViewById(R.id.component_comment_cell_icon);
    userNameLayout = (LinearLayout) findViewById(R.id.component_comment_cell_user_name_layout);
    userName = (CustomTextView) findViewById(R.id.component_comment_cell_user_name);
    commentTime = (CustomTextView) findViewById(R.id.component_comment_time);
    body = (CustomTextView) findViewById(R.id.component_comment_body);
    likes = (CounterWithDrawable) findViewById(R.id.component_comment_like);
    certifiedLabel = (CustomTextView) findViewById(R.id.component_comment_cell_certified_label);
    certifiedBadge = (AppCompatImageView) findViewById(R.id.component_comment_cell_certified_badge);
    bottomSeparator = findViewById(R.id.component_comment_bottom_separator);
  }

  public void setupView() {
    likes.setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_like));
    certifiedLabel.setVisibility(GONE);
    certifiedBadge.setVisibility(GONE);
  }

  private void setupListeners() {
    root.setOnLongClickListener(view -> {
      if (listener != null && isCommentMine) {
        PopupMenu popupMenu = new PopupMenu(getContext(), userName);
        popupMenu.getMenu().add(getResources().getString(R.string.comment_cell_delete_comment));
        popupMenu.setOnMenuItemClickListener(item -> {
          if (listener != null) listener.onCellLongClick(commentId);
          return false;
        });
        popupMenu.show();
      }
      return false;
    });
    likes.setListener((boolean isCounterActivated, CounterWithDrawable counterWithDrawable) -> {
      if (listener != null) {
        listener.onLikeClick(commentId, likes.isLiked());
      }
    });
    iconLayout.setOnClickListener(view -> profileRegionClick());
    userNameLayout.setOnClickListener(view -> profileRegionClick());
  }

  private void profileRegionClick() {
    if (listener != null) {
      listener.onProfileRegionClick(commentId);
    }
  }

  public void setIcon(String imageUrl) {
    if (imageUrl != null && !imageUrl.equals("")) {
      PicassoHelper.loadImageFromUrl(icon, imageUrl, getContext(),
        ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    } else {
      icon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    }
  }

  public void setUserName(String userName) {
    this.userName.setText(userName);
  }

  public void setUserCertified(boolean isUserCertified) {
    certifiedLabel.setVisibility(isUserCertified ? VISIBLE : GONE);
    certifiedBadge.setVisibility(isUserCertified ? VISIBLE : GONE);
  }

  //TODO change this later to the right format
  public void setDate(String date) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(date, getContext());
    DateFormat.getTimeFormat(getContext());
    this.commentTime.setText(formattedDate);
  }

  public void setCommentId(Integer commentId) {
    this.commentId = commentId;
  }

  public void setBody(String body) {
    this.body.setText(body);
  }

  public void setLikesCount(int count) {
    this.likes.setCounter(count);

    likes.setOnClickListener(view -> {
      if (listener != null) {
        listener.onLikeClick(commentId, likes.isLiked());
      }
    });
  }

  public void setLikeActivated(boolean activated) {
    this.likes.setLiked(activated);
  }

  public void setCommentMine(boolean isCommentMine) {
    this.isCommentMine = isCommentMine;
  }

  public void hideSeparator() {
    bottomSeparator.setVisibility(GONE);
  }

  public interface CommentCellListener {
    void onCellLongClick(Integer id);

    void onLikeClick(Integer id, boolean isLikeActivated);

    void onProfileRegionClick(Integer id);
  }

  public void setListener(CommentCellListener listener) {
    this.listener = listener;
  }

  public void hideLike() {
    likes.setVisibility(GONE);
  }
}
