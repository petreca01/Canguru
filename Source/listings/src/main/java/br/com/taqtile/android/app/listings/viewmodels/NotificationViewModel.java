package br.com.taqtile.android.app.listings.viewmodels;

/**
 * Created by taqtile on 5/9/17.
 */

public class NotificationViewModel {

  private Integer id;

  private String content;

  private String type;

  private String entity;

  private Integer entityId;

  private boolean visualized;

  private String createdAt;

  private String userPerformerName;

  public NotificationViewModel(Integer id, String content, String type, String entity,
                               Integer entityId, String createdAt, Boolean visualized,
                               String userPerformerName) {
    this.id = id;
    this.content = content;
    this.type = type;
    this.entity = entity;
    this.entityId = entityId;
    this.createdAt = createdAt;
    this.visualized = visualized;
    this.userPerformerName = userPerformerName;
  }

  public Integer getId() {
    return id;
  }

  public String getContent() {
    return content;
  }

  public String getType() {
    return type;
  }

  public String getEntity() {
    return entity;
  }

  public Integer getEntityId() {
    return entityId;
  }

  public boolean isVisualized() {
    return visualized;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUserPerformerName() {
    return userPerformerName;
  }
}
