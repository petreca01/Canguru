package br.com.taqtile.android.app.listings.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.CommentCellViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.MaternityDetailHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.MaternityNoCommentPlaceholderViewHolder;
import br.com.taqtile.android.app.listings.cells.AccordionPlaceholder;
import br.com.taqtile.android.app.listings.cells.CommentCell;
import br.com.taqtile.android.app.listings.cells.MaternityDetailHeaderCell;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityDetailsViewModel;

import static br.com.taqtile.android.app.listings.Constants.MATERNITY_DETAIL_COMMENTS;
import static br.com.taqtile.android.app.listings.Constants.MATERNITY_DETAIL_HEADER;
import static br.com.taqtile.android.app.listings.Constants.MATERNITY_DETAIL_NO_COMMENT_PLACEHOLDER;

/**
 * Created by taqtile on 10/05/17.
 */

public class MaternityReviewCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int HEADER_OFFSET = 1;
  private static final int NO_COMMENT_PLACEHOLDER_OFFSET = 1;
  private MaternityDetailsViewModel maternityDetails;
  private MaternityDetailHeaderCell.MaternityDetailHeaderCellListener maternityDetailHeaderCellListener;
  private CommentCell.CommentCellListener commentCellListener;
  private Integer userId;

  public MaternityReviewCommentsAdapter(MaternityDetailHeaderCell.MaternityDetailHeaderCellListener maternityDetailHeaderCellListener,
                                        CommentCell.CommentCellListener commentCellListener) {
    this.maternityDetailHeaderCellListener = maternityDetailHeaderCellListener;
    this.commentCellListener = commentCellListener;
    maternityDetails = new MaternityDetailsViewModel(null, new ArrayList<>());
  }

  @Override
  public int getItemViewType(int position) {
    // TODO: 5/11/17 redo to getByType of the viewModel and not byPosition
    if (position == 0) {
      return MATERNITY_DETAIL_HEADER;
    } else {
      if (maternityDetails.getComments().isEmpty()) {
        return MATERNITY_DETAIL_NO_COMMENT_PLACEHOLDER;
      } else {
        return MATERNITY_DETAIL_COMMENTS;
      }
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    switch (viewType) {
      case MATERNITY_DETAIL_HEADER:
        return new MaternityDetailHeaderViewHolder(
          new MaternityDetailHeaderCell(parent.getContext(), maternityDetailHeaderCellListener));
      case MATERNITY_DETAIL_COMMENTS:
        return new CommentCellViewHolder(new CommentCell(parent.getContext(), commentCellListener));
      case MATERNITY_DETAIL_NO_COMMENT_PLACEHOLDER:
        return new MaternityNoCommentPlaceholderViewHolder(new AccordionPlaceholder(parent.getContext()));
      default:
        return null;
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case MATERNITY_DETAIL_HEADER:
        setupMaternityDetailHeader(holder);
        break;
      case MATERNITY_DETAIL_COMMENTS:
        setupMaternityDetailComments(holder, position);
        break;
      case MATERNITY_DETAIL_NO_COMMENT_PLACEHOLDER:
        setupNoCommentsPlaceholder(holder);
        break;
    }
  }

  private void setupNoCommentsPlaceholder(RecyclerView.ViewHolder holder){
    AccordionPlaceholder maternityNoCommentPlaceholder = ((MaternityNoCommentPlaceholderViewHolder) holder).getMaternityNoCommentPlaceholder();
      maternityNoCommentPlaceholder.setImageAndText(maternityNoCommentPlaceholder.getResources().getString(R.string.component_maternity_no_comment_placeholder),
        ContextCompat.getDrawable(maternityNoCommentPlaceholder.getContext(), R.drawable.ic_cartaoprenatal_baby_mobile));
  }

  private void setupMaternityDetailHeader(RecyclerView.ViewHolder holder) {
    MaternityDetailHeaderViewHolder maternityDetailHeaderViewHolder =
      (MaternityDetailHeaderViewHolder) holder;
    if (maternityDetails.getMaternity() != null) {
      maternityDetailHeaderViewHolder.getMaternityDetailHeaderCell().setupView(maternityDetails.getMaternity());
    }
  }

  private void setupMaternityDetailComments(RecyclerView.ViewHolder holder, int position) {
    CommentCellViewHolder commentCellHolder = (CommentCellViewHolder) holder;
    commentCellHolder.getCommentCell().setUserName(getComment(position).getUsername());
    commentCellHolder.getCommentCell().setIcon(getComment(position).getUserPicture());
    commentCellHolder.getCommentCell().setBody(getComment(position).getComment());
    commentCellHolder.getCommentCell().setCommentId(getComment(position).getUserId());
    commentCellHolder.getCommentCell().setDate(getComment(position).getCreatedAt());
    commentCellHolder.getCommentCell().hideLike();
    commentCellHolder.getCommentCell().setCommentMine(isLoggedUserPost(getComment(position)));
    commentCellHolder.getCommentCell().setListener(commentCellListener);
  }

  private boolean isLoggedUserPost(MaternityCommentViewModel maternityCommentViewModel) {
    return userId.equals((maternityCommentViewModel).getUserId());
  }

  @Override
  public int getItemCount() {
    return HEADER_OFFSET + (maternityDetails.getComments().isEmpty() ?
      NO_COMMENT_PLACEHOLDER_OFFSET : maternityDetails.getComments().size());
  }

  private MaternityCommentViewModel getComment(int position) {
    return maternityDetails.getComments().get(position - HEADER_OFFSET);
  }

  public void setMaternityDetails(MaternityDetailsViewModel maternityDetails) {
    this.maternityDetails = maternityDetails;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }
}
