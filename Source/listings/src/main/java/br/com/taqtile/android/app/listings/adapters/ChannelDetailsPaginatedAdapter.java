package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.ChannelDetailsHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SectionHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SocialFeedChannelCardViewHolder;
import br.com.taqtile.android.app.listings.binders.ChannelDetailsBinder;
import br.com.taqtile.android.app.listings.cells.ChannelCard;
import br.com.taqtile.android.app.listings.cells.ChannelDescriptionCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelDetailsHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedChannelViewModel;

import static br.com.taqtile.android.app.listings.Constants.CHANNEL_DETAILS_CHANNEL_CARD;
import static br.com.taqtile.android.app.listings.Constants.CHANNEL_DETAILS_HEADER;
import static br.com.taqtile.android.app.listings.Constants.CHANNEL_DETAILS_SECTION_SEPARATOR;

/**
 * Created by taqtile on 4/5/17.
 */

public class ChannelDetailsPaginatedAdapter extends RecyclerViewPaginatedAdapter {

  private List<ListingsViewModel> channelDetailsModels;

  private ChannelCard.ChannelCardListener channelCardListener;
  private ChannelDescriptionCell.Listener channelHeaderListener;

  public ChannelDetailsPaginatedAdapter(Context context, List<ListingsViewModel> channelDetailsModels,
                                        ChannelCard.ChannelCardListener channelCardListener, ChannelDescriptionCell.Listener channelHeaderListener) {
    super(context);
    this.channelDetailsModels = channelDetailsModels;
    this.channelCardListener = channelCardListener;
    this.channelHeaderListener = channelHeaderListener;
  }

  @Override
  public int getLoadingLayout() {
    return R.layout.component_social_placeholder_card;
  }

  @Override
  public RecyclerView.ViewHolder createAdditionalViewHolder(ViewGroup parent, int viewType) {
    if (viewType == CHANNEL_DETAILS_CHANNEL_CARD) {
      return new SocialFeedChannelCardViewHolder(new ChannelCard(parent.getContext()));
    } else if (viewType == CHANNEL_DETAILS_HEADER) {
      return new ChannelDetailsHeaderViewHolder(new ChannelDescriptionCell(parent.getContext()), channelHeaderListener);
    } else {
      return new SectionHeaderViewHolder(new SectionHeader(parent.getContext()));
    }
  }

  @Override
  public void bindAdditionalViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case CHANNEL_DETAILS_HEADER:
        setupChannelDetailsHeader(holder, position);
        break;
      case CHANNEL_DETAILS_CHANNEL_CARD:
        setupChannelDetailsChannelCell(holder, position);
        break;
      case CHANNEL_DETAILS_SECTION_SEPARATOR:
        setupSectionHeader(holder);
        break;
    }
  }

  private void setupChannelDetailsHeader(RecyclerView.ViewHolder holder, int position) {
    ChannelDetailsHeaderViewHolder channelDetailsHeaderViewHolder = (ChannelDetailsHeaderViewHolder) holder;
    ChannelDetailsHeaderImplViewModel channelDetailsHeaderViewModel = (ChannelDetailsHeaderImplViewModel) channelDetailsModels.get(position);

    ChannelDetailsBinder.bindChannelDetailsHeaderViewModel(channelDetailsHeaderViewModel, channelDetailsHeaderViewHolder);

  }

  private void setupChannelDetailsChannelCell(RecyclerView.ViewHolder holder, int position) {
    SocialFeedChannelCardViewHolder channelDetailsChannelCardViewHolder = (SocialFeedChannelCardViewHolder) holder;
    SocialFeedChannelViewModel channelDetailsChannelCardViewModel = (SocialFeedChannelViewModel) channelDetailsModels.get(position);

    ChannelDetailsBinder.bindChannelCardViewModel(channelDetailsChannelCardViewModel, channelDetailsChannelCardViewHolder);

    setupChannelDetailsChannelCellListener(channelDetailsChannelCardViewHolder, channelDetailsChannelCardViewModel);

  }

  private void setupChannelDetailsChannelCellListener(SocialFeedChannelCardViewHolder socialFeedChannelCardViewHolder, SocialFeedChannelViewModel socialFeedChannelImplViewModel) {
    socialFeedChannelCardViewHolder.getChannelCard().setListener(channelCardListener);
  }

  private void setupSectionHeader(RecyclerView.ViewHolder holder) {
    SectionHeaderViewHolder sectionHeader = (SectionHeaderViewHolder) holder;
    Context context = ((SectionHeaderViewHolder) holder).getSectionHeader().getContext();
    sectionHeader.getSectionHeader().setText(context.getResources().getString(R.string.channel_details_section_header_text));
  }

  @Override
  public int getAdditionalViewTypes(int position) {
    ListingsViewModel viewModel = channelDetailsModels.get(position);

    if (viewModel instanceof SocialFeedChannelViewModel) {
      return CHANNEL_DETAILS_CHANNEL_CARD;
    } else if (viewModel instanceof ChannelDetailsHeaderViewModel) {
      return CHANNEL_DETAILS_HEADER;
    } else {
      return CHANNEL_DETAILS_SECTION_SEPARATOR;
    }
  }

  @Override
  public int getAdditionalItemCount() {
    return channelDetailsModels.size();
  }


  public void setChannelHeaderListener(ChannelDescriptionCell.Listener channelHeaderListener) {
    this.channelHeaderListener = channelHeaderListener;
  }

}
