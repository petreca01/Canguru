package br.com.taqtile.android.app.listings.builders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 08/05/17.
 */

public class HorizontalSelectorBuilder {

  private LinearLayout selectorContainer;
  private Integer defaultSelectedColor;
  private String defaultSelectedText;
  private Context context;

  public HorizontalSelectorBuilder(LinearLayout selectorContainer, Integer defaultSelectedColor, String defaultSelectedCell, Context context) {
    this.selectorContainer = selectorContainer;
    this.defaultSelectedColor = defaultSelectedColor;
    this.defaultSelectedText = defaultSelectedCell;
    this.context = context;
  }

  public HorizontalSelectorBuilder(LinearLayout selectorContainer, Context context) {
    this.selectorContainer = selectorContainer;
    this.context = context;
  }

  public void addSelectorCell(String selectorCellText, HorizontalSelectorCell.Listener listener,
                               int textActiveColor, int textInactiveColor, int backgroundColor) {
    HorizontalSelectorCell horizontalSelectorCell = new HorizontalSelectorCell(context);
    horizontalSelectorCell.setTextActiveColor(textActiveColor);
    horizontalSelectorCell.setTextInactiveColor(textInactiveColor);
    horizontalSelectorCell.setBackgroundActiveColor(backgroundColor);
    horizontalSelectorCell.setText(selectorCellText);
    horizontalSelectorCell.showUnselectedState();
    horizontalSelectorCell.setListener(listener);
    selectorContainer.addView(horizontalSelectorCell);
  }

  public HorizontalSelectorCell getCellToSelect(String cellNumber) {
    HorizontalSelectorCell horizontalSelectorCell = new HorizontalSelectorCell(context);
    for (int i = 0; i < selectorContainer.getChildCount(); i++) {
      HorizontalSelectorCell selectedCell = new HorizontalSelectorCell(context);
      selectedCell = (HorizontalSelectorCell) selectorContainer.getChildAt(i);
      if (selectedCell.getNumber().equals(cellNumber)) {
        horizontalSelectorCell = selectedCell;
      }
    }
    return horizontalSelectorCell;
  }

  public void unselectAllCells() {
    HorizontalSelectorCell horizontalSelectorCell = new HorizontalSelectorCell(context);
    for (int i = 0; i < selectorContainer.getChildCount(); i++) {
      HorizontalSelectorCell selectedCell = new HorizontalSelectorCell(context);
      selectedCell = (HorizontalSelectorCell) selectorContainer.getChildAt(i);
      selectedCell.showUnselectedState();
    }
  }

  public void setDefaultTextPaintedCell() {
    if (defaultSelectedColor != null) {
      HorizontalSelectorCell horizontalSelectorCell = getCellToSelect(defaultSelectedText);
      horizontalSelectorCell.setTextColor(defaultSelectedColor);
    }
  }

  public void setSelectedCell(String cellNumber) {
    unselectAllCells();
    setDefaultTextPaintedCell();
    HorizontalSelectorCell selectorCell = getCellToSelect(cellNumber);
    selectorCell.showSelectedState();
  }

  public void setSelectedCellWithoutUnselectingOtherCells(String cellNumber){
    setDefaultTextPaintedCell();
    HorizontalSelectorCell selectorCell = getCellToSelect(cellNumber);
    selectorCell.showSelectedState();
  }

  public void paintPreviousCells(String cellNumber){
    int cellIndex = selectorContainer.indexOfChild(getCellToSelect(cellNumber));
    for (int i = 0; i < cellIndex + 1;i++){
      setSelectedCellWithoutUnselectingOtherCells(((HorizontalSelectorCell) selectorContainer.getChildAt(i)).getNumber());
    }
  }

  public void clearSelector() {
    selectorContainer.removeAllViews();
  }

}
