package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelHeaderViewModel;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelHeaderImplViewModel implements ChannelHeaderViewModel {

  private String headerText;

  public ChannelHeaderImplViewModel(String headerText) {
    this.headerText = headerText;
  }

  @Override
  public void setId(String id) {

  }

  @Override
  public String getId() {
    return "";
  }

  @Override
  public void setTopicName(String topicName) {

  }

  @Override
  public String getTopicName() {
    return "";
  }

  @Override
  public void setSectionHeaderText(String headerText) {
    this.headerText = headerText;
  }

  @Override
  public String getSectionHeaderText() {
    return headerText;
  }
}
