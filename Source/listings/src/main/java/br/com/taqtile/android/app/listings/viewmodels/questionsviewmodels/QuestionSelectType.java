package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

import java.util.List;

/**
 * Created by felipesabino on 5/22/17.
 */
public interface QuestionSelectType extends QuestionType {
  List<String> getAlternatives();
  String getAnswer();
}
