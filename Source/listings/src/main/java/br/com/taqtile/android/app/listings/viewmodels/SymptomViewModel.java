package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by taqtile on 5/9/17.
 */

public class SymptomViewModel implements Parcelable {

  private SubSymptomViewModel symptom;
  private List<SubSymptomViewModel> children;

  public SymptomViewModel(SubSymptomViewModel symptom, List<SubSymptomViewModel> children) {
    this.symptom = symptom;
    this.children = children;
  }

  public Integer getId() {
    return symptom.getId();
  }

  public Integer getParentId() {
    return symptom.getParentId();
  }

  public String getName() {
    return symptom.getName();
  }

  public String getExplanation() {
    return symptom.getExplanation();
  }

  public String getHowToAvoid() {
    return symptom.getHowToAvoid();
  }

  public String getWhenToWorry() {
    return symptom.getWhenToWorry();
  }

  public List<SubSymptomViewModel> getChildren() {
    return children;
  }

  public SubSymptomViewModel getSymptom() {
    return symptom;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(this.symptom, flags);
    dest.writeTypedList(this.children);
  }

  protected SymptomViewModel(Parcel in) {
    this.symptom = in.readParcelable(SubSymptomViewModel.class.getClassLoader());
    this.children = in.createTypedArrayList(SubSymptomViewModel.CREATOR);
  }

  public static final Creator<SymptomViewModel> CREATOR = new Creator<SymptomViewModel>() {
    @Override
    public SymptomViewModel createFromParcel(Parcel source) {
      return new SymptomViewModel(source);
    }

    @Override
    public SymptomViewModel[] newArray(int size) {
      return new SymptomViewModel[size];
    }
  };
}
