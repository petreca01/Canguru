package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/18/17.
 */

public class ProfileHeaderCell extends LinearLayout {

  private CustomTextView name;
  private RelativeLayout root;
  private ImageView image;
  private TemplateButton seeProfile;
  private ProfileHeaderCellListener listener;

  public ProfileHeaderCell(Context context) {
    super(context);
    init();
  }

  public ProfileHeaderCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ProfileHeaderCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_profile_header_cell, this, true);

    bindViews();

    setupButtons();
  }

  private void bindViews() {
    name = (CustomTextView) findViewById(R.id.component_profile_header_cell_name);
    image = (ImageView) findViewById(R.id.component_profile_header_cell_image);
    root = (RelativeLayout) findViewById(R.id.component_profile_header_cell_root);
    seeProfile = (TemplateButton) findViewById(R.id.component_channel_card_posting_time);
  }

  private void setupButtons() {
    setOnClickListener(v -> {
      if (listener != null) {
        listener.onSeeProfileClicked();
      }
    });
    seeProfile.setOnClickListener(v -> {
      if (listener != null) {
        listener.onSeeProfileClicked();
      }
    });
  }

  public void setListener(ProfileHeaderCellListener listener) {
    this.listener = listener;
  }

  public void setProfileName(String name) {
    this.name.setText(name);
  }

  public void setProfileImage(String imageUrl) {
    if (!imageUrl.equals("")) {
      PicassoHelper.loadImageFromUrl(image, imageUrl, getContext(), ContextCompat.getDrawable(getContext(), R.drawable.ic_picture_placeholder));
    } else {
      image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    }
  }

  public interface ProfileHeaderCellListener {
    void onSeeProfileClicked();
  }

}
