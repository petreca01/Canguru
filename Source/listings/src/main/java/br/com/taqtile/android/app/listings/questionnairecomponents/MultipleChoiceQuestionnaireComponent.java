package br.com.taqtile.android.app.listings.questionnairecomponents;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.cells.MultipleChoiceCell;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;

/**
 * Created by taqtile on 19/05/17.
 */

public class MultipleChoiceQuestionnaireComponent extends LinearLayout implements QuestionnaireResult, MultipleChoiceCell.Listener {

  private String questionId;
  private DescriptionHeaderWithImage questionDescription;

  private LinearLayout choicesContainer;

  private List<String> choices;

  private List<String> answersList;

  private AnswerListener answerListener;

  public MultipleChoiceQuestionnaireComponent(Context context) {
    super(context);
  }

  public MultipleChoiceQuestionnaireComponent(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public MultipleChoiceQuestionnaireComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public MultipleChoiceQuestionnaireComponent(Context context, AnswerListener listener, QuestionViewModel questionModel) {
    super(context);
    this.answerListener = listener;
    this.questionId = String.valueOf(questionModel.getQuestionId());
    init(questionModel.getAlternatives(), questionModel.getQuestion(),
      questionModel.getDescription(), questionModel.getAnswers());
  }

  private void init(List<String> choices, String question, String questionDescription,
                    List<String> answers) {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_multiple_choice_questionnaire, this, true);
    bindViews();
    setupAnswersList();
    setAnswersIfAlreadyAswered(answers);
    buildChoices(choices, question, questionDescription);
  }

  private void bindViews() {
    choicesContainer = (LinearLayout) findViewById(R.id.component_multiple_choice_questionnaire_choices_container);
    questionDescription = (DescriptionHeaderWithImage) findViewById(R.id.component_multiple_choice_questionnaire_question_description);
  }

  private void setupAnswersList() {
    answersList = new ArrayList<>();
  }

  private void setAnswersIfAlreadyAswered(List<String> answers) {
    for (String answer : answers) {
      if (answer != null && !answer.isEmpty()) {
        addAnswer(answer);
      }
    }
  }

  private void buildChoices(List<String> choices, String question, String questionDescription) {
    this.choices = new ArrayList<>();
    this.choices.addAll(choices);
    this.questionDescription.setTitle(question);
    this.questionDescription.setDescription(questionDescription);
    if (this.choices != null && !this.choices.isEmpty()) {
      for (int i = 0; i < choices.size(); i++) {
        MultipleChoiceCell choice = new MultipleChoiceCell(getContext());
        choice.setText(choices.get(i));
        if (alreadyAnswered(i)) {
          choice.setIsSelected(true);
        }
        choice.setListener(this);
        choicesContainer.addView(choice);
      }
      answerListener.onAnswerSelected(this);
    }
  }

  private boolean alreadyAnswered(int i) {
    return this.answersList != null && !this.answersList.isEmpty() &&
      answersList.contains(choices.get(i));
  }


  @Override
  public List<String> getResult() {
    return getSelectedOption();
  }

  public List<String> getSelectedOption() {
    List<String> selectedChoice = new ArrayList<>();
    for (int i = 0; i < choicesContainer.getChildCount(); i++) {
      MultipleChoiceCell selectedCell = (MultipleChoiceCell) choicesContainer.getChildAt(i);
      if (selectedCell.isSelected()) {
        selectedChoice.add(selectedCell.getText());
      }
    }
    return selectedChoice;
  }

  @Override
  public boolean isResultValid() {
    if (validAnswerList()) {
      for (String response : this.answersList) {
        if (!choices.contains(response)) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String getQuestionId() {
    return this.questionId;
  }

  @Override
  public void onCellClick(String choice, MultipleChoiceCell multipleChoiceCell) {
    if (answerListener == null) {
      return;
    }
    if (isAlreadyAdded(choice)) {
      removeAnswer(choice);
    } else {
      addAnswer(choice);
    }
    answerListener.onAnswerSelected(this);
  }

  private void removeAnswer(String answer) {
    answersList.remove(answer);
  }

  private void addAnswer(String answer) {
    answersList.add(answer);
  }

  private boolean isAlreadyAdded(String choice) {
    return validAnswerList() && answersList.contains(choice);
  }

  private boolean validAnswerList() {
    return this.answersList != null && !this.answersList.isEmpty() &&
      this.answersList.size() <= this.choices.size();
  }

}
