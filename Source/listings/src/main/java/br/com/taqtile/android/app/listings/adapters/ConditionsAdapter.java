package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.ConditionsCellViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.ConditionsHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.HealthcareWithPartnerViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SectionHeaderViewHolder;
import br.com.taqtile.android.app.listings.cells.ConditionCell;
import br.com.taqtile.android.app.listings.cells.ConditionsHeaderCell;
import br.com.taqtile.android.app.listings.cells.HealthcareWithPartnerCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionSectionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionSectionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static br.com.taqtile.android.app.listings.Constants.CONDITION;
import static br.com.taqtile.android.app.listings.Constants.CONDITIONS_HEADER;
import static br.com.taqtile.android.app.listings.Constants.CONDITIONS_HISTORY;
import static br.com.taqtile.android.app.listings.Constants.CONDITIONS_HISTORY_SECTION_HEADER;
import static br.com.taqtile.android.app.listings.Constants.CONDITIONS_SECTION_HEADER;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<ListingsViewModel> symptomsList;
  private ConditionsAdapterListener listener;

  public ConditionsAdapter(List<ListingsViewModel> symptomsList) {
    this.symptomsList = symptomsList;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == CONDITIONS_HEADER) {
      return new ConditionsHeaderViewHolder(new ConditionsHeaderCell(parent.getContext()));
    } else if (viewType == CONDITIONS_HISTORY_SECTION_HEADER) {
      return new SectionHeaderViewHolder(new SectionHeader(parent.getContext()));
    } else if (viewType == CONDITIONS_HISTORY) {
      return new HealthcareWithPartnerViewHolder(
        new HealthcareWithPartnerCell(parent.getContext()));
    } else if (viewType == CONDITIONS_SECTION_HEADER) {
      return new SectionHeaderViewHolder(new SectionHeader(parent.getContext()));
    } else {
      return new ConditionsCellViewHolder(new ConditionCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case CONDITIONS_HEADER:
        setConditionsHeaderCell(holder);
        break;
      case CONDITIONS_HISTORY_SECTION_HEADER:
        setHistorySectionHeaderCell(holder);
        break;
      case CONDITIONS_HISTORY:
        setHistoryCell(holder, position);
        break;
      case CONDITIONS_SECTION_HEADER:
        setConditionSectionHeaderCell(holder);
        break;
      case CONDITION:
        setConditionCell(holder, position);
        break;
    }
  }

  private void setConditionsHeaderCell(RecyclerView.ViewHolder holder) {
    ConditionsHeaderCell conditionsHeaderCell =
      ((ConditionsHeaderViewHolder) holder).getConditionsHeaderCell();
  }

  private void setHistorySectionHeaderCell(RecyclerView.ViewHolder holder) {
    SectionHeader sectionHeader = ((SectionHeaderViewHolder) holder).getSectionHeader();
    Context context = sectionHeader.getContext();
    sectionHeader.setText(context.getResources().getString(R.string.component_conditions_header_history_section_title));
  }

  private void setHistoryCell(RecyclerView.ViewHolder holder, int position) {

    UserClinicalConditionViewModel historyCondition =
      (UserClinicalConditionViewModel) symptomsList.get(position);
    HealthcareWithPartnerCell healthcareWithPartnerCell =
      ((HealthcareWithPartnerViewHolder) holder).getHealthcareWithPartnerCell();
    healthcareWithPartnerCell.setHeader(historyCondition.getName());
    Context context = healthcareWithPartnerCell.getContext();
    healthcareWithPartnerCell.setSubheader(
      String.format(context.getResources().getString(R.string.fragment_conditions_report_date_cell),
        historyCondition.getDate()));
    healthcareWithPartnerCell.setImageFromDrawable(R.drawable.ic_gestacao_medical);
    healthcareWithPartnerCell.setListener(() -> {
      if (listener != null) {
        listener.onConditionHistoryCellClicked(historyCondition.getPregnancyConditionId());
      }
    });
  }

  private void setConditionSectionHeaderCell(RecyclerView.ViewHolder holder) {
    SectionHeader sectionHeader = ((SectionHeaderViewHolder) holder).getSectionHeader();
    Context context = sectionHeader.getContext();
    sectionHeader.setText(context.getResources().getString(R.string.component_conditions_header_section_title));
  }

  private void setConditionCell(RecyclerView.ViewHolder holder, int position) {
    ClinicalConditionViewModel condition = (ClinicalConditionViewModel) symptomsList.get(position);
    ClinicalConditionItemViewModel conditionInfo = condition.getClinicalConditionItemViewModel();
    ConditionsCellViewHolder textCellViewHolder = (ConditionsCellViewHolder) holder;
    ConditionCell conditionCell = textCellViewHolder.getConditionCell();
    conditionCell.setConditionId(conditionInfo.getId());
    conditionCell.setConditionText(conditionInfo.getName());
    conditionCell.setBottomLineVisibility(position != getItemCount());

    if (condition.getChildren() != null && !condition.getChildren().isEmpty()) {
      conditionCell.showIcon();
      conditionCell.setHasSubItems();
      conditionCell.setSubItemListener((view, subitemPosition) ->
        this.conditionCellClicked(condition.getChildren().get(subitemPosition)));
      conditionCell.setSubConditionCellContainer(
        StreamSupport.stream(condition.getChildren())
          .filter(Objects::nonNull)
          .map(ClinicalConditionItemViewModel::getName)
          .collect(Collectors.toList()));
    }

    conditionCell.setListener((view, subitemPosition) ->
      this.conditionCellClicked(condition.getClinicalConditionItemViewModel()));
  }

  private void conditionCellClicked(ClinicalConditionItemViewModel clinicalConditionItemViewModel) {
    if (listener != null) {
      listener.onConditionCellClicked(clinicalConditionItemViewModel);
    }
  }

  @Override
  public int getItemViewType(int position) {
    if (symptomsList.get(position) instanceof ClinicalConditionHeaderViewModel) {
      return CONDITIONS_HEADER;
    }
    if (symptomsList.get(position) instanceof UserClinicalConditionSectionHeaderViewModel) {
      return CONDITIONS_HISTORY_SECTION_HEADER;
    }
    if (symptomsList.get(position) instanceof UserClinicalConditionViewModel) {
      return CONDITIONS_HISTORY;
    }
    if (symptomsList.get(position) instanceof ClinicalConditionSectionHeaderViewModel) {
      return CONDITIONS_SECTION_HEADER;
    } else {
      return CONDITION;
    }
  }

  @Override
  public int getItemCount() {
    return symptomsList.size();
  }

  public interface ConditionsAdapterListener {
    void onConditionCellClicked(ClinicalConditionItemViewModel clinicalConditionItemViewModel);

    void onConditionHistoryCellClicked(int conditionId);

  }

  public void setListener(ConditionsAdapterListener listener) {
    this.listener = listener;
  }

}
