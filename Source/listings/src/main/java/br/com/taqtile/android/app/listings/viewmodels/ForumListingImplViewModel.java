package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;

/**
 * Created by taqtile on 3/22/17.
 */

public class ForumListingImplViewModel implements ForumListingViewModel {

  private String id;
  private String topicName;
  private int likesCounter;
  private int commentsCounter;
  private String description;
  private int totalPosts;
  private int contentId;
  private int contentParentId;

  public ForumListingImplViewModel(String id, String topicName, int likesCounter, int commentsCounter) {
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.commentsCounter = commentsCounter;
    this.description = "";
    totalPosts = 0;
  }

  public ForumListingImplViewModel(String id, String topicName, int likesCounter, int commentsCounter, String description) {
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.commentsCounter = commentsCounter;
    this.description = description;
    this.totalPosts = 0;
  }

  public ForumListingImplViewModel(String id, String topicName, int likesCounter, int commentsCounter, String description, int totalPosts, int contentId, int contentParentId) {
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.commentsCounter = commentsCounter;
    this.description = description;
    this.totalPosts = totalPosts;
    this.contentId = contentId;
    this.contentParentId = contentParentId;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setTopicName(String channelName) {

    this.topicName = channelName;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Override
  public void setLikesCounter(int likesCounter) {

    this.likesCounter = likesCounter;
  }

  @Override
  public int getLikesCounter() {
    return likesCounter;
  }

  @Override
  public void setCommentsCounter(int followersCounter) {
    commentsCounter = followersCounter;
  }

  @Override
  public int getCommentsCounter() {
    return commentsCounter;
  }

  @Override
  public void setDescription(String description) {

    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setContentId(int contentId) {
    this.contentId = contentId;
  }

  @Override
  public int getContentId() {
    return contentId;
  }

  @Override
  public void setContentParentId(int contentParentId) {
    this.contentParentId = contentParentId;
  }

  @Override
  public int getContentParentId() {
    return contentParentId;
  }


  @Override
  public void setTotalPosts(int totalPosts) {
    this.totalPosts = totalPosts;
  }

  @Override
  public int getTotalPosts() {
    return totalPosts;
  }
}
