package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/23/17.
 */

public interface SymptomListingViewModel extends SearchListingViewModel, PaginationViewModel {
  void setDescription(String description);

  String getDescription();

  void setContentId(int contentId);

  int getContentId();

  void setContentParentId(int contentParentId);

  int getContentParentId();

}
