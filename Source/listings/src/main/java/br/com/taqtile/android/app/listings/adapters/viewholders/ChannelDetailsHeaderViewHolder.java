package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ChannelDescriptionCell;

/**
 * Created by taqtile on 06/04/17.
 */

public class ChannelDetailsHeaderViewHolder extends RecyclerView.ViewHolder {

  ChannelDescriptionCell channelDescriptionCell;

  public ChannelDetailsHeaderViewHolder(View itemView, ChannelDescriptionCell.Listener listener) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.channelDescriptionCell = (ChannelDescriptionCell) itemView;
    this.channelDescriptionCell.setListener(listener);
  }

  public ChannelDescriptionCell getChannelDescriptionCell() {
    return channelDescriptionCell;
  }

  public void setListener(ChannelDescriptionCell.Listener listener) {
    channelDescriptionCell.setListener(listener);
  }
}
