package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 3/23/17.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
  View v;
  ProgressBar layout;

  public LoadingViewHolder(View itemView) {
    super(itemView);
    v = itemView;
    RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layout = (ProgressBar) itemView.findViewById(R.id.component_list_loading_progress_bar);
    itemView.setLayoutParams(layoutParams);
  }

  public ProgressBar getLayout() {
    return layout;
  }

}
