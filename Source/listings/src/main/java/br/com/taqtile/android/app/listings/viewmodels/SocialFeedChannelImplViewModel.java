package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedChannelViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 3/18/17.
 */

public class SocialFeedChannelImplViewModel implements SocialFeedChannelViewModel {

  private String videoUrl;
  private String imageUrl;
  private Integer postId;
  private Integer channelId;
  private String posterName;
  private String channelName;
  private String date;
  private String postTitle;
  private String body;
  private int likes;
  private int commentsCount;
  private boolean isLikeActivated;

  public SocialFeedChannelImplViewModel(String videoUrl, String imageUrl, Integer postId, Integer channelId,
                                        String posterName, String channelName, String date, String postTitle,
                                        String body, int likes, int commentsCount,
                                        boolean isLikeActivated) {
    this.videoUrl = videoUrl;
    this.imageUrl = imageUrl;
    this.postId = postId;
    this.channelId = channelId;
    this.posterName = posterName;
    this.channelName = channelName;
    this.date = date;
    this.postTitle = postTitle;
    this.body = body;
    this.likes = likes;
    this.commentsCount = commentsCount;
    this.isLikeActivated = isLikeActivated;
  }

  @Override public String getVideoUrl() {
    return videoUrl;
  }

  @Override public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  @Override public String getImageUrl() {
    return imageUrl;
  }

  @Override public void setPostId(Integer postId) {
    this.postId = postId;
  }

  @Override public Integer getPostId() {
    return postId;
  }

  @Override public void setPosterName(String posterName) {
    this.posterName = posterName;
  }

  @Override public String getPosterName() {
    return posterName;
  }

  @Override public void setDate(String date) {
    this.date = date;
  }

  @Override public String getDate() {
    return date;
  }

  @Override public long getDateInMilliseconds() {
    return DateFormatterHelper.getDateInMilliseconds(date);
  }

  @Override public void setPostTitle(String postTitle) {

    this.postTitle = postTitle;
  }

  @Override public String getPostTitle() {
    return postTitle;
  }

  @Override public void setPostBody(String body) {

    this.body = body;
  }

  @Override public String getPostBody() {
    return body;
  }

  @Override public void setLikesCount(int likes) {

    this.likes = likes;
  }

  @Override public int getLikesCount() {
    return likes;
  }

  @Override public void setCommentsCount(int commentsCount) {

    this.commentsCount = commentsCount;
  }

  @Override public int getCommentsCount() {
    return commentsCount;
  }

  @Override public void setLikeActivated(boolean isCounterActivated) {
    this.isLikeActivated = isCounterActivated;
  }

  @Override public boolean getLikeActivated() {
    return isLikeActivated;
  }

  @Override public boolean isCertifiedProfessional() {
    return false; // For the time being we don't have this data
  }

  @Override public void setCertifiedProfessional(boolean certifiedProfessional) {

  }

  public Integer getChannelId() {
    return channelId;
  }

  public String getChannelName() {
    return channelName;
  }

}
