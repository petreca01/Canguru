package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.HealthcareWithPartnerCell;

/**
 * Created by taqtile on 18/04/17.
 */

public class HealthcareWithPartnerViewHolder extends RecyclerView.ViewHolder {

  HealthcareWithPartnerCell healthcareWithPartnerCell;

  public HealthcareWithPartnerViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.healthcareWithPartnerCell = (HealthcareWithPartnerCell) itemView;
  }

  public HealthcareWithPartnerCell getHealthcareWithPartnerCell(){
    return healthcareWithPartnerCell;
  }

}
