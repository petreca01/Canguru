package br.com.taqtile.android.app.listings.viewmodels;

import android.util.Log;

/**
 * Created by taqtile on 4/25/17.
 */

public class HealthOperatorViewModel {

  private static final String TAG = "HealthOperatorViewModel";
  private Integer id;
  private String name;
  private String about;
  private String logo;
  private Boolean healthPlanPartner;
  private String documentType;

  public HealthOperatorViewModel() {
    this.id = null;
    this.name = null;
    this.about = null;
    this.logo = null;
    this.healthPlanPartner = null;
    this.documentType = null;
  }

  public HealthOperatorViewModel(Integer id, String name, String about,
                                 String logo, Boolean healthPlanPartner, String documentType) {
    this.id = id;
    this.name = name;
    this.about = about;
    this.logo = logo;
    this.healthPlanPartner = healthPlanPartner;
    this.documentType = documentType;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAbout() {
    return about;
  }

  public String getLogo() {
    return logo;
  }

  public Boolean getHealthPlanPartner() {
    return healthPlanPartner;
  }

  public String getDocumentType() {
    return documentType;
  }
}
