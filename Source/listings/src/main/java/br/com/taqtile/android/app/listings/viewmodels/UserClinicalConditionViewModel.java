package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;

/**
 * Created by taqtile on 5/14/17.
 */

public class UserClinicalConditionViewModel implements ListingsViewModel {
  private Integer pregnancyConditionId;
  private String name;
  private String date;
  private Integer gestationAgeWeeks;
  private Integer gestationAgeDays;
  private Integer risk;

  public UserClinicalConditionViewModel(Integer pregnancyConditionId, String name, String date,
                                        Integer gestationAgeWeeks, Integer gestationAgeDays,
                                        Integer risk) {
    this.pregnancyConditionId = pregnancyConditionId;
    this.name = name;
    this.date = date;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.gestationAgeDays = gestationAgeDays;
    this.risk = risk;
  }

  public Integer getPregnancyConditionId() {
    return pregnancyConditionId;
  }

  public String getName() {
    return name;
  }

  public String getDate() {
    return date;
  }

  public Integer getGestationAgeWeeks() {
    return gestationAgeWeeks;
  }

  public Integer getGestationAgeDays() {
    return gestationAgeDays;
  }

  public Integer getRisk() {
    return risk;
  }
}
