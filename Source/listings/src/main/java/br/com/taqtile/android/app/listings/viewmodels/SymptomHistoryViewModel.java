package br.com.taqtile.android.app.listings.viewmodels;

/**
 * Created by taqtile on 12/05/17.
 */

public class SymptomHistoryViewModel {
  private String date;
  private String name;

  public SymptomHistoryViewModel(String date, String name) {
    this.date = date;
    this.name = name;
  }

  public String getDate() {
    return date;
  }

  public String getName() {
    return name;
  }
}
