package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;

/**
 * Created by taqtile on 3/27/17.
 */

public class SocialPlaceholderCard extends CardView {

  private FrameLayout shimmerLayout;

  public SocialPlaceholderCard(Context context) {
    super(context);
    init();
  }

  public SocialPlaceholderCard(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public SocialPlaceholderCard(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_social_placeholder_card, this, true);

    setupView();

    bindViews();

  }

  private void bindViews() {
    shimmerLayout = (FrameLayout) findViewById(R.id.component_social_placeholder_card_shimmer_layout);
  }

  private void setupView() {
    CardView.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    setClipChildren(false);
    setClipToPadding(false);
    setLayoutParams(layoutParams);
    float cornerRadius = FloatToDpConverter.convertFromDpToFloat(getResources().getDimension(R.dimen.card_corner_radius), getContext());
    setRadius(cornerRadius);
  }

}
