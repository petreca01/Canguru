package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 29/05/17.
 */

public class AccordionPlaceholder extends LinearLayout {

  private ImageView image;
  private CustomTextView text;

  public AccordionPlaceholder(Context context) {
    super(context);
    init();
  }

  public AccordionPlaceholder(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public AccordionPlaceholder(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_accordion_placeholder, this, true);

    bindViews();
  }

  private void bindViews(){
    image = (ImageView) findViewById(R.id.component_accordion_placeholder_image);
    text = (CustomTextView) findViewById(R.id.component_accordion_placeholder_text);
  }

  public void setImageAndText(String text, Drawable image){
    this.text.setText(text);
    this.image.setImageDrawable(image);
  }

}
