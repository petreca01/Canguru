package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/10/17.
 */

public class ConditionCell extends LinearLayout {

  public CustomTextView conditionText;
  public ImageView icon;
  public LinearLayout subConditionCellContainer;
  public View bottomLine;

  private ConditionCell.Listener listener;
  private FrameLayout clickableView;
  private boolean closedCell;

  String conditionTextStyle;
  String iconAttr;
  boolean showBottomLineAttr;
  private Integer conditionId;
  private boolean hasSubItems;
  private ConditionCell.SubItemListener subItemListener;


  public ConditionCell(Context context) {
    super(context);
    init();
  }

  public ConditionCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public ConditionCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.condition_cell, this, true);

    bindView();
    setupView();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextCell);

    conditionTextStyle = a.getString(R.styleable.TextCell_leftText);
    iconAttr = a.getString(R.styleable.TextCell_rightText);
    showBottomLineAttr = a.getBoolean(R.styleable.TextCell_showBottomLine, true);

    a.recycle();
  }

  private void bindView() {
    conditionText = (CustomTextView) findViewById(R.id.condition_cell_text);
    icon = (ImageView) findViewById(R.id.condition_cell_icon);
    subConditionCellContainer = (LinearLayout) findViewById(R.id.condition_cell_child_cell);
    bottomLine = findViewById(R.id.condition_cell_bottom_line);
    clickableView = (FrameLayout) findViewById(R.id.condition_cell_clickable_view);

  }

  private void setupView() {
    setupCustomTextViews(conditionText, conditionTextStyle);
    subConditionCellContainer.setVisibility(VISIBLE);
    setChevronDown();

    if (!showBottomLineAttr) {
      bottomLine.setVisibility(GONE);
    }

    clickableView.setOnClickListener(this::onCellClick);

    icon.setOnClickListener(this::onIconClick);
  }

  private void onIconClick(View view) {
    if (listener != null) {
      this.onDropdownClick();
    }
  }

  private void onCellClick(View view) {
    if (hasSubItems) {
      onIconClick(view);
    } else {
      cellClick(view);
    }
  }

  private void cellClick(View view) {
    if (listener != null) {
      listener.onCellClick(view, conditionId);
    }
  }

  private void onDropdownClick() {
    if (closedCell) {
      setChevronUp();
    } else {
      setChevronDown();
    }
  }

  private void setChevronUp() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_up));
    subConditionCellContainer.setVisibility(VISIBLE);
    subConditionCellContainer.getChildAt(subConditionCellContainer.getChildCount() - 1).requestFocus();
    toggleChevronState();
  }

  private void setChevronDown() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_down));
    subConditionCellContainer.setVisibility(GONE);
    toggleChevronState();
  }

  private void toggleChevronState() {
    closedCell = !closedCell;
  }

  private void setupCustomTextViews(CustomTextView textView, String conditionTextStyle) {
    if (conditionTextStyle != null) {
      textView.setText(conditionTextStyle);
    } else {
      textView.setVisibility(GONE);
    }
  }

  public void setConditionId(Integer id) {
    this.conditionId = id;
  }

  public interface Listener {
    void onCellClick(View view, Integer id);
  }

  public interface SubItemListener {
    void onSubItemCellClick(View view, Integer subItemIndex);
  }

  public void setListener(ConditionCell.Listener listener) {
    this.listener = listener;
  }

  public void setSubItemListener(SubItemListener subItemListener) {
    this.subItemListener = subItemListener;
  }

  public void setConditionText(String conditionText) {
    if (conditionText != null && !conditionText.isEmpty()) {
      this.conditionText.setVisibility(VISIBLE);
      this.conditionText.setText(conditionText);
    }
  }

  public void showIcon() {
    icon.setVisibility(VISIBLE);
  }

  public void setHasSubItems() {
    this.hasSubItems = true;
  }

  public void setSubConditionCellContainer(List<String> conditionCell) {
    for (String text : conditionCell) {
      this.addSubconditionCell(text, conditionCell.size(), conditionCell.indexOf(text));
    }
  }

  private void addSubconditionCell(String text, int size, int position) {

    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
      LinearLayout.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    subConditionCellContainer.setLayoutParams(params);

    SubconditionCell subconditionCell = new SubconditionCell(getContext());
    subconditionCell.setBottomLineVisibility(position < size - 1);
    subconditionCell.setText(text);
    subconditionCell.setListener(v -> this.onSubItemCellClick(v, position));

    subConditionCellContainer.addView(subconditionCell);
  }

  private void onSubItemCellClick(View view, int subItemIndex) {
    if (subItemListener != null) {
      subItemListener.onSubItemCellClick(view, subItemIndex);
    }
  }

  public void setBottomLineVisibility(boolean shouldShowBottomLine) {
    bottomLine.setVisibility(shouldShowBottomLine ? VISIBLE : GONE);
  }

  public void hideBottomLine(){
    bottomLine.setVisibility(GONE);
  }

  public void showBottomLine(){
    bottomLine.setVisibility(VISIBLE);
  }
}
