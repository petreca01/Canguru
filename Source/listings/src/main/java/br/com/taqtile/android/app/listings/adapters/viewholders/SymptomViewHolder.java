package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.SimpleTextImageCell;

/**
 * Created by taqtile on 3/24/17.
 */

public class SymptomViewHolder extends RecyclerView.ViewHolder {
    SimpleTextImageCell symptomCell;

    public SymptomViewHolder(View itemView) {
        super(itemView);
        itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.symptomCell = (SimpleTextImageCell) itemView;
    }

    public SimpleTextImageCell getSymptomCell(){
        return symptomCell;
    }
}
