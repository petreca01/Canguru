package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 10/11/16.
 */

public class TextCell extends LinearLayout {

  public CustomTextView leftText;
  public CustomTextView rightText;
  public View bottomLine;
  private Listener listener;

  private FrameLayout clickableView;

  String leftTextAttr;
  String rightTextAttr;
  boolean showBottomLineAttr;

  private LinearLayout root;


  public TextCell(Context context) {
    super(context);
    init();
  }

  public TextCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public TextCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_cell, this, true);

    bindView();
    setupView();
    setupButtons();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextCell);

    leftTextAttr = a.getString(R.styleable.TextCell_leftText);
    rightTextAttr = a.getString(R.styleable.TextCell_rightText);
    showBottomLineAttr = a.getBoolean(R.styleable.TextCell_showBottomLine, true);
  }

  private void bindView() {
    leftText = (CustomTextView) findViewById(R.id.component_text_cell_left_text);
    rightText = (CustomTextView) findViewById(R.id.component_text_cell_right_text);
    bottomLine = findViewById(R.id.component_text_cell_bottom_line);
    clickableView = (FrameLayout) findViewById(R.id.component_text_cell_clickable_view);

    root = (LinearLayout) findViewById(R.id.component_text_cell_root);

  }

  private void setupButtons() {
    clickableView.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCellClick(view, this.leftText.getText());
      }
    });
  }

  private void setupView() {
    setupCustomTextViews(leftText, leftTextAttr);
    setupCustomTextViews(rightText, rightTextAttr);

    if (!showBottomLineAttr) {
      bottomLine.setVisibility(GONE);
    }
  }

  private void setupCustomTextViews(CustomTextView textView, String attr) {
    if (attr != null) {
      textView.setText(attr);
    }
  }

  public interface Listener {
    void onCellClick(View view, CharSequence text);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  public void setLeftText(String text) {
    if (text != null && !text.isEmpty()) {
      leftText.setVisibility(VISIBLE);
      leftText.setText(text);
    }
  }

  public void setRightText(String text) {
    if (text != null && !text.isEmpty()) {
      rightText.setVisibility(VISIBLE);
      rightText.setText(text);
    }
  }

  public void setBottomLineVisibility(boolean shouldShowBottomLine) {
    bottomLine.setVisibility(shouldShowBottomLine ? VISIBLE : GONE);
  }

}
