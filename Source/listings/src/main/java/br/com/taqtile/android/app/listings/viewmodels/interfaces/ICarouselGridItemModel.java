package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 1/22/16.
 */
public interface ICarouselGridItemModel {
    String getImageUrl();

    String getName();

    String getId();
}
