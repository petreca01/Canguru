package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.icu.text.DisplayContext;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 3/23/17.
 */

public abstract class TextAndImageCell extends FrameLayout {

  private String id;
  private int contentId;
  private int contentParentId;

  private ImageView icon;
  private LinearLayout root;
  protected LinearLayout textLayout;
  protected CustomTextView header;
  private CustomTextViewWithHTML description;
  private FrameLayout descriptionContainer;
  private CustomTextView leftCounterLayout;
  private CustomTextView rightCounterLayout;
  private CustomTextView institutionName;
  private RelativeLayout subheadingLayout;
  private Typeface semiboldTypeface;
  private Typeface regularTypeface;
  private TextAndImageCellListener listener;
  private int leftCounter;
  private int rightCounter;
  private View separator;

  private String headerAttr;
  private int imageAttr;
  private boolean showLineAttr = true;

  protected abstract boolean isInstitutionNameVisible();

  protected abstract String getFormattedLeftCounterText(int leftCounter);

  protected abstract String getFormattedRightCounterText(int rightCounter);

  protected abstract Drawable getIcon();

  public TextAndImageCell(Context context) {
    super(context);
    init();
  }

  public TextAndImageCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public TextAndImageCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_and_image_cell, this, true);

    bindViews();
    setupButtons();
    setupViews();
    setupTypeface();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextAndImageCell);

    headerAttr = a.getString(R.styleable.TextAndImageCell_headerCell);
    imageAttr = a.getResourceId(R.styleable.TextAndImageCell_imageCell, 0);
    showLineAttr = a.getBoolean(R.styleable.TextAndImageCell_showLineCell, true);

    a.recycle();
  }

  private void setupButtons() {
    root.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCellClick(getCellId());
      }
    });
    description.setOnClickListener(v -> {
      if (listener != null){
        listener.onCellClick(getCellId());
      }
    });
  }

  private void setupTypeface() {
    semiboldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-SemiBold.ttf");
    regularTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-Regular.ttf");
  }

  private void setupLayoutParams() {
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    setLayoutParams(layoutParams);
  }

  private void bindViews() {
    root = (LinearLayout) findViewById(R.id.component_text_and_image_cell_root);
    header = (CustomTextView) findViewById(R.id.component_text_and_image_cell_header);
    leftCounterLayout = (CustomTextView) findViewById(R.id.component_text_and_image_cell_likes);
    rightCounterLayout = (CustomTextView) findViewById(R.id.component_text_and_image_cell_comments);
    subheadingLayout = (RelativeLayout) findViewById(R.id.component_text_and_image_cell_subheading_layout);
    institutionName = (CustomTextView) findViewById(R.id.component_text_and_image_cell_institution_name);
    icon = (ImageView) findViewById(R.id.component_text_and_image_cell_icon);
    textLayout = (LinearLayout) findViewById(R.id.component_text_and_image_cell_text_layout);
    separator = findViewById(R.id.component_text_and_image_cell_separator);
    description = (CustomTextViewWithHTML) findViewById(R.id.component_text_and_image_cell_description);
    descriptionContainer = (FrameLayout) findViewById(R.id.fragment_component_text_and_image_cell_description_container);
  }

  private void setupViews() {
    if (headerAttr != null) {
      header.setText(headerAttr);
    }

    if (imageAttr != 0) {
      icon.setImageResource(imageAttr);
    } else {
      icon.setImageDrawable(getIcon());
    }

    if (!showLineAttr) {
      separator.setVisibility(GONE);
    }

    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setupLayoutParams();
    setSubheadingVisibility();
  }

  public void setSubheadingVisibility() {
    if (!isInstitutionNameVisible()) {
      subheadingLayout.setVisibility(GONE);
    }
  }

  public void setHeader(String header) {
    this.header.setText(header);
  }

  public void setInstitutionName(String institutionName) {
    this.institutionName.setVisibility(VISIBLE);
    this.institutionName.setText(institutionName);
  }

  public void setRightCounter(int rightCounter) {
    this.rightCounterLayout.setText(getFormattedRightCounterText(rightCounter));
  }

  public void setDescription(String description) {
    this.descriptionContainer.setVisibility(VISIBLE);
    this.description.setTextWithHTML(description);
  }

  public String getCellId() {
    return id;
  }

  public void setCellId(String id) {
    this.id = id;
  }

  public int getContentId() {
    return contentId;
  }

  public void setContentId(int contentId) {
    this.contentId = contentId;
  }

  public int getContentParentId() {
    return contentParentId;
  }

  public void setContentParentId(int contentParentId) {
    this.contentParentId = contentParentId;
  }

  public void setLeftCounter(int leftCounter) {
    this.leftCounterLayout.setText(getFormattedLeftCounterText(leftCounter));
  }

  public void highlightText() {
    header.setTypeface(semiboldTypeface);
  }

  public void unhighlightText() {
    header.setTypeface(regularTypeface);
  }

  protected void showLeftCounter() {
    leftCounterLayout.setVisibility(VISIBLE);
  }

  protected void showRightCounter() {
    rightCounterLayout.setVisibility(VISIBLE);
  }

  protected void hideLeftCounter() {
    leftCounterLayout.setVisibility(GONE);
  }

  protected void hideRightCounter() {
    rightCounterLayout.setVisibility(GONE);
  }

  public interface TextAndImageCellListener {
    void onCellClick(String id);
  }

  public void setListener(TextAndImageCellListener textAndImageCellListener) {
    listener = textAndImageCellListener;
  }

  public void setIcon(int image) {
    icon.setImageResource(image);
  }
}
