package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 04/05/17.
 */

public class HorizontalSelectorCell extends FrameLayout {

  private CustomTextView selectorText;
  private int textActiveColor;
  private int textInactiveColor;
  private int backgroundActiveColor;

  private String number;
  private boolean isSelected;

  private Listener listener;

  public HorizontalSelectorCell(@NonNull Context context) {
    super(context);
    init();
  }

  public HorizontalSelectorCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public HorizontalSelectorCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_week_selector_cell, this, true);

    bindViews();
    setupView();
    setupButtons();
  }

  private void bindViews(){
    selectorText = (CustomTextView) findViewById(R.id.component_week_selector_cell_text);
  }

  private void setupView(){
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setPadding((int) getResources().getDimension(R.dimen.margin_small), 0, 0, 0);
    setLayoutParams(layoutParams);

    isSelected = false;
  }

  private void setupButtons(){
    this.setOnClickListener(v -> {
      if (listener != null){
        listener.onSelectorClick(isSelected, number);
      }
    });
  }

  public void setText(String text){
    setNumber(text);
    this.selectorText.setText(text);
  }

  public void setBackgroundActiveColor(@ColorInt int color){
    backgroundActiveColor = color;
  }

  public void setTextActiveColor(@ColorInt int color){
    textActiveColor = color;
  }

  public void setTextInactiveColor(@ColorInt int color){
    textInactiveColor = color;
  }

  public void setTextColor(@ColorInt int color){
    selectorText.setTextColor(color);
  }

  public void showSelectedState(){
    isSelected = true;
    selectorText.setTextColor(textActiveColor);
    selectorText.getBackground().setColorFilter(backgroundActiveColor, PorterDuff.Mode.DST_ATOP);
  }

  public void showUnselectedState(){
    isSelected = false;
    selectorText.setTextColor(textInactiveColor);
    selectorText.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.DST_ATOP);
  }

  public void setNumber(String id){
    this.number = id;
  }

  public String getNumber(){
    return number;
  }

  public interface Listener{
    void onSelectorClick(boolean state, String number);
  }

  public void setListener(Listener listener){ this.listener = listener; }

  public View getSelectorTextView() {
   return selectorText;
  }

}
