package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 10/11/16.
 */

public class TextImageCell extends LinearLayout {
    public TextImageCell(Context context) {
        super(context);
        init();
    }

    public TextImageCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextImageCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.component_text_image_cell, this, true);

        setupView();
    }

    private void setupView(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
        setOrientation(HORIZONTAL);
        setLayoutParams(layoutParams);
    }
}
