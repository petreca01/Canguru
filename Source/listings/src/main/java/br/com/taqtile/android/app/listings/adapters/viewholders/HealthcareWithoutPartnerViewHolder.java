package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.taqtile.android.app.listings.cells.HealthcareWithoutPartnerCell;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareWithoutPartnerViewHolder extends RecyclerView.ViewHolder {

  HealthcareWithoutPartnerCell healthcareWithoutPartnerCell;

  public HealthcareWithoutPartnerViewHolder(View itemView) {
    super(itemView);
    this.healthcareWithoutPartnerCell = (HealthcareWithoutPartnerCell) itemView;
  }

  public HealthcareWithoutPartnerCell getHealthcareWithoutPartnerCell(){
    return healthcareWithoutPartnerCell;
  }

}
