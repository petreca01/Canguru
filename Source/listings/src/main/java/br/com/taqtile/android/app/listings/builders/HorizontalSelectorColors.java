package br.com.taqtile.android.app.listings.builders;

/**
 * Created by taqtile on 08/05/17.
 */

public class HorizontalSelectorColors {

  private int textActiveColor;
  private int textInactiveColor;
  private int backgroundSelectedColor;

  public HorizontalSelectorColors(int textActiveColor, int textInactiveColor, int backgroundSelectedColor) {
    this.textActiveColor = textActiveColor;
    this.textInactiveColor = textInactiveColor;
    this.backgroundSelectedColor = backgroundSelectedColor;
  }

  public int getTextActiveColor() {
    return textActiveColor;
  }

  public int getTextInactiveColor() {
    return textInactiveColor;
  }

  public int getBackgroundSelectedColor() {
    return backgroundSelectedColor;
  }
}
