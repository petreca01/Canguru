package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 3/15/17.
 */

public class HomeHeaderCell extends CardView {

    private CircleImageView image;
    private CustomTextView text;
    private HomeHeaderCellListener listener;
    private RelativeLayout content;

    public HomeHeaderCell(Context context) {
        super(context);
        init();
    }

    public HomeHeaderCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HomeHeaderCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.component_home_header_cell, this, true);

        setupView();
        bindViews();
        setupButton();
    }

    private void setupButton(){
        content.setOnClickListener(view -> {
            if (listener != null){
                listener.onHomeHeaderCellClick();
            }
        });
    }

    private void setupView(){
        setupLayoutParams();
        setupCornerRadius();

    }

    private void setupCornerRadius(){
        float cornerRadius = FloatToDpConverter.convertFromDpToFloat(getResources().getDimension(R.dimen.card_corner_radius), getContext());
        setRadius(cornerRadius);
    }

    private void setupLayoutParams(){
        FrameLayout.LayoutParams layoutParams =
          new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        setLayoutParams(layoutParams);
    }

    private void bindViews(){
        image = (CircleImageView) findViewById(R.id.component_home_header_cell_image);
        text = (CustomTextView) findViewById(R.id.component_home_header_cell_text);
        content = (RelativeLayout) findViewById(R.id.component_home_header_cell_content);
    }

    public void setImage(String imageUrl){
        PicassoHelper.loadImageFromUrl(image, imageUrl, getContext(),
          ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    }

    public interface HomeHeaderCellListener{
        void onHomeHeaderCellClick();
    }

    public void setListener(HomeHeaderCellListener homeHeaderCellListener){
      listener = homeHeaderCellListener; }

}
