package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/22/17.
 */

public class UltraSoundAccordion extends LinearLayout {

  private ImageView icon;
  private CustomTextView cardTitle;
  private LinearLayout childCellContent;
  private View childCellBottomLine;
  private View cellBottomLine;
  private FrameLayout childCellContainer;
  private AccordionSkeleton skeleton;
  private AccordionPlaceholder noContentPlaceholder;

  private UltraSoundAccordion.Listener listener;
  private FrameLayout clickableView;
  private boolean closedCell;

  String conditionTextStyle;
  String iconAttr;
  boolean showBottomLineAttr;

  public UltraSoundAccordion(Context context) {
    super(context);
    init();
  }

  public UltraSoundAccordion(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public UltraSoundAccordion(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.accordion_ultra_sound_cell, this, true);

    bindView();
    setupView();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextCell);

    conditionTextStyle = a.getString(R.styleable.TextCell_leftText);
    iconAttr = a.getString(R.styleable.TextCell_rightText);
    showBottomLineAttr = a.getBoolean(R.styleable.TextCell_showBottomLine, true);

    a.recycle();
  }

  private void bindView() {
    cardTitle = (CustomTextView) findViewById(R.id.accordion_ultra_sound_cell_text);
    icon = (ImageView) findViewById(R.id.accordion_ultra_sound_cell_icon);
    childCellContent = (LinearLayout) findViewById(R.id.accordion_ultra_sound_cell_child_cell);
    childCellBottomLine = findViewById(R.id.accordion_ultra_sound_child_cell_bottom_line);
    clickableView = (FrameLayout) findViewById(R.id.accordion_ultra_sound_cell_clickable_view);
    cellBottomLine = (View) findViewById(R.id.accordion_ultra_sound_cell_bottom_line);
    childCellContainer = (FrameLayout) findViewById(R.id.accordion_ultra_sound_child_cell_container);
    skeleton = (AccordionSkeleton) findViewById(R.id.accordion_ultra_sound_cell_skeleton);
    noContentPlaceholder = (AccordionPlaceholder) findViewById(R.id.accordion_ultra_sound_cell_no_content_placeholder);
  }

  private void setupView() {
    setupCustomTextViews(cardTitle, conditionTextStyle);
    childCellContent.setVisibility(VISIBLE);
    setChevronDown();

    if (!showBottomLineAttr) {
      childCellBottomLine.setVisibility(GONE);
    }

    clickableView.setOnClickListener(this::onIconClick);

    icon.setOnClickListener(this::onIconClick);
  }

  private void onIconClick(View view) {
    this.onDropdownClick();
  }

  private void onDropdownClick() {
    if (closedCell) {
      setChevronUp();
    } else {
      setChevronDown();
    }
  }

  private void setChevronUp() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_up));
    childCellContainer.setVisibility(VISIBLE);
    toggleChevronState();
    cellBottomLine.setVisibility(GONE);
    childCellBottomLine.setVisibility(VISIBLE);
  }

  private void setChevronDown() {
    icon.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.ic_chevron_down));
    childCellContainer.setVisibility(GONE);
    toggleChevronState();
    cellBottomLine.setVisibility(VISIBLE);
    childCellBottomLine.setVisibility(GONE);
  }

  private void toggleChevronState() {
    closedCell = !closedCell;
  }

  private void setupCustomTextViews(CustomTextView textView, String conditionTextStyle) {
    if (conditionTextStyle != null) {
      textView.setText(conditionTextStyle);
    }
  }

  public interface Listener {
    void onCellClick(View view);
  }

  public void setListener(UltraSoundAccordion.Listener listener) {
    this.listener = listener;
  }

  public void addChildCellText(String date, String doctorName, String gestationWeekDUM,
                               String gestationWeekUltraSound, String FetalWeight, String placenta,
                               String liquid, String pbf) {
    UltraSoundCell customTextView = new UltraSoundCell(getContext());

    if (date != null && !date.isEmpty()) {
      customTextView.setCellDate(date);
      customTextView.setCellDoctorName(doctorName);
      customTextView.setCellGestationWeekDUM(gestationWeekDUM);
      customTextView.setCellGestationWeekUltraSound(gestationWeekUltraSound);
      customTextView.setCellFetalWeight(FetalWeight);
      customTextView.setCellPlacenta(placenta);
      customTextView.setCellLiquid(liquid);
      customTextView.setCellPBF(pbf);
      childCellContent.addView(customTextView);
    }
  }

  public void setBottomLineVisibility(boolean shouldShowBottomLine) {
    childCellBottomLine.setVisibility(shouldShowBottomLine ? VISIBLE : GONE);
  }

  public void showSkeleton(){
    skeleton.setVisibility(VISIBLE);
    noContentPlaceholder.setVisibility(GONE);
    childCellContent.setVisibility(GONE);
  }

  public void showChildCellContent(){
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(GONE);
    childCellContent.setVisibility(VISIBLE);
  }

  public void showNoContentPlaceholder(){
    noContentPlaceholder.setImageAndText(getResources().getString(R.string.component_accordion_placeholder_no_content_text),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_cartaoprenatal_baby_mobile));
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(VISIBLE);
    childCellContent.setVisibility(GONE);
  }

  public void showErrorPlaceholder(){
    noContentPlaceholder.setImageAndText(getResources().getString(R.string.component_accordion_placeholder_error_text),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_placeholder_no_connection));
    skeleton.setVisibility(GONE);
    noContentPlaceholder.setVisibility(VISIBLE);
    childCellContent.setVisibility(GONE);
  }

}
