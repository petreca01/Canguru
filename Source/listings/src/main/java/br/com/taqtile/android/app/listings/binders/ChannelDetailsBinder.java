package br.com.taqtile.android.app.listings.binders;

import br.com.taqtile.android.app.listings.adapters.viewholders.ChannelDetailsHeaderViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SocialFeedChannelCardViewHolder;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedChannelViewModel;

/**
 * Created by taqtile on 06/04/17.
 */

public class ChannelDetailsBinder {

  public static void bindChannelDetailsHeaderViewModel(ChannelDetailsHeaderImplViewModel channelDetailsHeaderViewModel, ChannelDetailsHeaderViewHolder channelDetailsHeaderViewHolder) {
    channelDetailsHeaderViewHolder.getChannelDescriptionCell().setImage(channelDetailsHeaderViewModel.getImageUrl());
    channelDetailsHeaderViewHolder.getChannelDescriptionCell().setChannelDescription(channelDetailsHeaderViewModel.getChannelDescription());
    channelDetailsHeaderViewHolder.getChannelDescriptionCell().setChannelName(channelDetailsHeaderViewModel.getChannelName());
    channelDetailsHeaderViewHolder.getChannelDescriptionCell().toggleButtons(channelDetailsHeaderViewModel.isFollowing());
  }

  public static void bindChannelCardViewModel(SocialFeedChannelViewModel socialFeedChannelViewModel, SocialFeedChannelCardViewHolder socialFeedChannelCardViewHolder) {

    socialFeedChannelCardViewHolder.getChannelCard().setCommentsCounter(socialFeedChannelViewModel.getCommentsCount());
    socialFeedChannelCardViewHolder.getChannelCard().setBody(socialFeedChannelViewModel.getPostBody());
    socialFeedChannelCardViewHolder.getChannelCard().setHeading(socialFeedChannelViewModel.getPostTitle());
    socialFeedChannelCardViewHolder.getChannelCard().setLikesCounter(socialFeedChannelViewModel.getLikesCount());
    socialFeedChannelCardViewHolder.getChannelCard().setLiked(socialFeedChannelViewModel.getLikeActivated());
    socialFeedChannelCardViewHolder.getChannelCard().setDate(socialFeedChannelViewModel.getDate(), socialFeedChannelViewModel.getPosterName());
    socialFeedChannelCardViewHolder.getChannelCard().setProfileName(((SocialFeedChannelImplViewModel) socialFeedChannelViewModel).getChannelName());
    socialFeedChannelCardViewHolder.getChannelCard().setImage(socialFeedChannelViewModel.getImageUrl());
    socialFeedChannelCardViewHolder.getChannelCard().setPostId(socialFeedChannelViewModel.getPostId());
    socialFeedChannelCardViewHolder.getChannelCard().setChannelId(socialFeedChannelViewModel.getChannelId());
  }

}
