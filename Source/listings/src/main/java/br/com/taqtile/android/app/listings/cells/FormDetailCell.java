package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/20/17.
 */

public class FormDetailCell extends LinearLayout {

  private CustomTextView name;
  private CustomTextView value;

  String nameAttr;
  String valueAttr;

  public FormDetailCell(Context context) {
    super(context);
    init();
  }

  public FormDetailCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public FormDetailCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_form_detail_cell, this, true);

    bindViews();
    setupView();
  }

  private void bindViews(){
    name = (CustomTextView) findViewById(R.id.component_form_detail_cell_name);
    value = (CustomTextView) findViewById(R.id.component_form_detail_cell_value);
  }

  private void setupView(){
    if(nameAttr != null){
      name.setText(nameAttr);
    }

    if(valueAttr != null){
      value.setText(valueAttr);
    }
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FormDetailCell);

    nameAttr = a.getString(R.styleable.FormDetailCell_name);
    valueAttr = a.getString(R.styleable.FormDetailCell_value);

    a.recycle();
  }

  public void setupName(String name){
    this.name.setText(name);
  }

  public void setupValue(String value){
    this.value.setText(value);
  }

}
