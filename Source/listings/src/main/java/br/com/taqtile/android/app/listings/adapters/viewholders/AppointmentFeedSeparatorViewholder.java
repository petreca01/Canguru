package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.AppointmentFeedSeparator;

/**
 * Created by taqtile on 16/05/17.
 */

public class AppointmentFeedSeparatorViewholder extends RecyclerView.ViewHolder {

  private AppointmentFeedSeparator appointmentFeedSeparator;

  public AppointmentFeedSeparatorViewholder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.appointmentFeedSeparator = (AppointmentFeedSeparator) itemView;
  }

  public AppointmentFeedSeparator getAppointmentFeedSeparator(){
    return appointmentFeedSeparator;
  }
}
