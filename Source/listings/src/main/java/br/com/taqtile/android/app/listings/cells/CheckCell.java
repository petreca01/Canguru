package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Renato on 4/20/17.
 */

public class CheckCell extends TextImageCellCenter {

  private Boolean isChecked;

  public CheckCell(Context context) {
    super(context);
    init();
  }

  public CheckCell(Context context, boolean isChecked){
    super(context);
    init();
    setChecked(isChecked);
  }

  public CheckCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CheckCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    setChecked(false);
  }

  public void setChecked(boolean isChecked){
    this.isChecked = isChecked;
    if(isChecked){
      setIcon(br.com.taqtile.android.app.resources.R.drawable.ic_check_button);
    } else{
      setIcon(br.com.taqtile.android.app.resources.R.drawable.ic_check_button_disabled);
    }
  }

  private void enable(){
    setEnabled(true);
  }

  private void disable(){
    setEnabled(false);
  }

  public Boolean getChecked() {
    return isChecked;
  }
}
