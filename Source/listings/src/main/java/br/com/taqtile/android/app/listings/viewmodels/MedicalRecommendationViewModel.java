package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by taqtile on 09/05/17.
 */

public class MedicalRecommendationViewModel implements Parcelable {
  private String id;
  private String title;
  private String doctorName;
  private String date;
  private String content;
  private boolean read;

  public MedicalRecommendationViewModel() {
  }

  public MedicalRecommendationViewModel(String id, String title, String doctorName, String date,
                                        String content, boolean read) {
    this.id = id;
    this.title = title;
    this.doctorName = doctorName;
    this.date = date;
    this.content = content;
    this.read = read;
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getDoctorName() {
    return doctorName;
  }

  public String getDate() {
    return date;
  }

  public String getContent() {
    return content;
  }

  public boolean isRead() {
    return read;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeString(this.title);
    dest.writeString(this.doctorName);
    dest.writeString(this.date);
    dest.writeString(this.content);
    dest.writeByte(this.read ? (byte) 1 : (byte) 0);
  }

  protected MedicalRecommendationViewModel(Parcel in) {
    this.id = in.readString();
    this.title = in.readString();
    this.doctorName = in.readString();
    this.date = in.readString();
    this.content = in.readString();
    this.read = in.readByte() != 0;
  }

  public static final Parcelable.Creator<MedicalRecommendationViewModel> CREATOR =
    new Parcelable.Creator<MedicalRecommendationViewModel>() {
      @Override public MedicalRecommendationViewModel createFromParcel(Parcel source) {
        return new MedicalRecommendationViewModel(source);
      }

      @Override public MedicalRecommendationViewModel[] newArray(int size) {
        return new MedicalRecommendationViewModel[size];
      }
    };
}
