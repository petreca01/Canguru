package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 13/07/17.
 */

public class ChannelSearchPostCell extends TextAndImageCell {

  public ChannelSearchPostCell(Context context) {
    super(context);
  }

  public ChannelSearchPostCell(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ChannelSearchPostCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean isInstitutionNameVisible() {
    return true;
  }

  @Override
  protected String getFormattedLeftCounterText(int leftCounter) {
    String likesCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (leftCounter == 0) {
      likesCounterText = getResources().getString(R.string.component_search_cell_no_likes);
    } else {
      likesCounterText = getResources().getQuantityString(R.plurals.likesCounter, leftCounter, leftCounter);
    }
    return likesCounterText;
  }

  @Override
  protected String getFormattedRightCounterText(int rightCounter) {
    String commentsCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (rightCounter == 0) {
      commentsCounterText = getResources().getString(R.string.component_search_cell_no_comments);
    } else {
      commentsCounterText = getResources().getQuantityString(R.plurals.commentsCounter, rightCounter, rightCounter);
    }
    return commentsCounterText;
  }

  @Override
  protected Drawable getIcon() {
    return ContextCompat.getDrawable(getContext(), R.drawable.ic_channel_listing_placeholder);

  }
}
