package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 30/05/17.
 */

public class SingleChoiceCell extends LinearLayout {

  private AppCompatRadioButton radio;
  private CustomTextView text;
  private boolean isSelected;

  private SingleChoiceCell.Listener listener;

  String textAttr;

  public SingleChoiceCell(Context context) {
    super(context);
    init();
  }

  public SingleChoiceCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public SingleChoiceCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_single_choice_cell, this, true);

    bindView();
    setupView();
    setupButtons();
  }

  private void bindView() {
    radio = (AppCompatRadioButton) findViewById(R.id.component_single_choice_cell_radio);
    text = (CustomTextView) findViewById(R.id.component_single_choice_cell_text);
  }

  private void setupButtons() {
    this.setOnClickListener(v -> {
      if (listener != null) {
        setIsSelected(!isSelected);
        listener.onCellClick((String) text.getText(), this);
      }
    });
  }

  private void toggleImageVisibility() {
    if (isSelected) {
      selectRadio();
    } else {
      unselectRadio();
    }
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MultipleChoiceCell);

    textAttr = a.getString(R.styleable.MultipleChoiceCell_textCell);

    a.recycle();
  }

  private void setupView() {

    if (textAttr != null) {
      text.setText(textAttr);
    }
    setIsSelected(false);
  }

  public void setIsSelected(boolean isSelected) {
    this.isSelected = isSelected;
    toggleImageVisibility();
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setText(String text) {
    this.text.setText(text);
  }

  public String getText() {
    return (String) text.getText();
  }

  public void selectRadio() {
    radio.setChecked(true);
  }

  public void unselectRadio() {
    radio.setChecked(false);
  }

  public interface Listener {
    void onCellClick(String choice, SingleChoiceCell singleChoiceCell);
  }

  public void setListener(SingleChoiceCell.Listener listener) {
    this.listener = listener;
  }

}
