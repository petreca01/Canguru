package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.adapters.viewholders.ChannelViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SectionHeaderViewHolder;
import br.com.taqtile.android.app.listings.cells.ChannelCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.ChannelHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;

/**
 * Created by taqtile on 3/22/17.
 */

public class ChannelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<SearchListingViewModel> channelList;

  private ChannelAdapterListener channelAdapterListener;

  public ChannelAdapter(List<SearchListingViewModel> channelList) {
    this.channelList = channelList;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == Constants.CHANNEL_SECTION_HEADER) {
      return new SectionHeaderViewHolder(new SectionHeader(parent.getContext()));
    } else {
      return new ChannelViewHolder(new ChannelCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case Constants.CHANNEL_SECTION_HEADER:
        setupChannelSectionHeader((SectionHeaderViewHolder) holder, position);
        break;
      case Constants.CHANNEL_CELL:
        setupChannelCell(holder, position);
        break;
    }
  }

  //TODO move this to a binder
  private void setupChannelCell(RecyclerView.ViewHolder holder, int position) {
    ChannelCell channelCell = ((ChannelViewHolder) holder).getChannelCell();
    ChannelListingViewModel channelListingViewModel = (ChannelListingViewModel) channelList.get(position);

    channelCell.setLeftCounter(channelListingViewModel.getPostsCounter());
    channelCell.setRightCounter(channelListingViewModel.getFollowersCounter());
    channelCell.setHeader(channelListingViewModel.getTopicName());
    channelCell.setInstitutionName(channelListingViewModel.getInstitutionName());
    channelCell.setCellId(channelListingViewModel.getId());
    channelCell.setListener(id -> {
      if (channelAdapterListener != null) {
        channelAdapterListener.onChannelCellClick(channelListingViewModel.getId());
      }
    });
  }

  private void setupChannelSectionHeader(SectionHeaderViewHolder holder, int position) {
    ChannelHeaderImplViewModel channelHeader = (ChannelHeaderImplViewModel) channelList.get(position);
    holder.getSectionHeader().setText(channelHeader.getSectionHeaderText());
  }

  @Override
  public int getItemCount() {
    return channelList.size();
  }

  @Override
  public int getItemViewType(int position) {
    SearchListingViewModel channelViewModel = channelList.get(position);
    if (channelViewModel instanceof ChannelHeaderViewModel) {
      return Constants.CHANNEL_SECTION_HEADER;
    } else {
      return Constants.CHANNEL_CELL;
    }
  }

  public interface ChannelAdapterListener {
    void onChannelCellClick(String id);
  }

  public void setListener(ChannelAdapterListener channelAdapterListener) {
    this.channelAdapterListener = channelAdapterListener;
  }

}
