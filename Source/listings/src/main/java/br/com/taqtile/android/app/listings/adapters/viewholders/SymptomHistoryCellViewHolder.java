package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.SymptomHistoryCell;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomHistoryCellViewHolder extends RecyclerView.ViewHolder {
  SymptomHistoryCell symptomHistoryCell;

  public SymptomHistoryCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.symptomHistoryCell = (SymptomHistoryCell) itemView;
  }

  public SymptomHistoryCell getSymptomHistoryCell(){
    return symptomHistoryCell;
  }

}
