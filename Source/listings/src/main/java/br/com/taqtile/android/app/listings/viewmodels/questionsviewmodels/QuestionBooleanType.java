package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

/**
 * Created by felipesabino on 5/22/17.
 */
public interface QuestionBooleanType extends QuestionType {
  Boolean getAnswerValue();
}
