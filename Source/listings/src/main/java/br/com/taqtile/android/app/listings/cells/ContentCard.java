package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;


/**
 * Important observation: when using CardViews in API < 21, the card shadow is counted as view height and width.
 * That is, if the view has a width of 100, then the AVAILABLE space will be 100 - (card shadow width*2) (because of
 * shadows on left and right). Same applies to height.
 */

public class ContentCard extends CardView {

    public ContentCard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ContentCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ContentCard(Context context) {
        super(context);
        init();
    }

    private void init() {

        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.component_content_card, this, true);

        setupView();

    }

    private void setupView() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        setLayoutParams(layoutParams);

    }

    //end region


}
