package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;

/**
 * Created by taqtile on 3/27/17.
 */

public class BabyDevelopmentPlaceholder extends FrameLayout {

  public BabyDevelopmentPlaceholder(Context context) {
    super(context);
    init();
  }

  public BabyDevelopmentPlaceholder(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public BabyDevelopmentPlaceholder(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_baby_development_placeholder, this, true);

    setupView();

    bindViews();

  }

  private void bindViews() {
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    setClipChildren(false);
    setClipToPadding(false);
    setLayoutParams(layoutParams);

  }

}
