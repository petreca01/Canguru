package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.adapters.viewholders.AppointmentFeedCellViewholder;
import br.com.taqtile.android.app.listings.adapters.viewholders.AppointmentFeedSeparatorViewholder;
import br.com.taqtile.android.app.listings.cells.AppointmentFeedCell;
import br.com.taqtile.android.app.listings.cells.AppointmentFeedSeparator;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 05/05/17.
 */

public class AppointmentFeedAdapter extends RecyclerView.Adapter {

  private final static int CELL = 0;
  private final static int SEPARATOR = 1;

  private AppointmentFeedCell.Listener appointmentFeedCellListener;
  private List<AgendaFeedViewModel> agendaItems;

  public AppointmentFeedAdapter(AppointmentFeedCell.Listener appointmentFeedCellListener,
                                List<AgendaFeedViewModel> agendaItems) {
    this.appointmentFeedCellListener = appointmentFeedCellListener;
    this.agendaItems = agendaItems;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    switch (viewType){
      case CELL:
        return new AppointmentFeedCellViewholder(new AppointmentFeedCell(parent.getContext()));
      case SEPARATOR:
        return new AppointmentFeedSeparatorViewholder(new AppointmentFeedSeparator(parent.getContext()));
      default:
        return new AppointmentFeedCellViewholder(new AppointmentFeedCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)){
      case CELL:
        setupAppointmentFeedCell((AppointmentFeedCellViewholder) holder, position);
        break;
      case SEPARATOR:
        break;
    }
  }

  private void setupAppointmentFeedCell(AppointmentFeedCellViewholder viewHolder, int position) {
    AppointmentFeedCell appointmentFeedCell = viewHolder.getAppointmentFeedCell();
    AgendaItemViewModel item = (AgendaItemViewModel) agendaItems.get(position);

    appointmentFeedCell.setAppointmentName(item.getName());
    appointmentFeedCell.setCustomAppointment(item.getIsAppointment() != null &&
      item.getIsAppointment().equals(1));
    appointmentFeedCell.setCellState(cellState(item), DateFormatterHelper.formatDateWithHours(item.getMarkingDate(),
      appointmentFeedCell.getContext()));
    appointmentFeedCell.setViewId(item.getId());
    appointmentFeedCell.setListener(appointmentFeedCellListener);
  }

  private int cellState(AgendaItemViewModel item) {
    if (item.getDone() != null && item.getDone().equals(1)) {
      return AppointmentFeedCell.EXAM_DONE;
    } else if (item.getMarkingDate() != null && !item.getMarkingDate().isEmpty()) {
      return AppointmentFeedCell.EXAM_SCHEDULED;
    } else {
      return AppointmentFeedCell.EXAM_UNSCHEDULED;
    }
  }

  public void setAgendaItems(List<AgendaFeedViewModel> agendaItems) {
    this.agendaItems = agendaItems;
  }

  public void setNormalCellState(AppointmentFeedCell appointmentFeedCell) {
    setNormalCellState(appointmentFeedCell, true);
  }

  public void setNormalCellState(AppointmentFeedCell appointmentFeedCell, Boolean shouldUpdate) {
    appointmentFeedCell.setNormalCellState(shouldUpdate);
    notifyDataSetChanged();
  }

  public void enableClickableView(AppointmentFeedCell appointmentFeedCell) {
    appointmentFeedCell.enableClickableView();
  }

  public void fadeOutCell(AppointmentFeedCell appointmentFeedCell) {
    appointmentFeedCell.fadeOutCell();
  }

  @Override
  public int getItemCount() {
    return agendaItems.size();
  }

  @Override
  public int getItemViewType(int position) {
    AgendaFeedViewModel agendaItem = agendaItems.get(position);
    if (agendaItem instanceof AgendaItemViewModel){
      return CELL;
    } else {
      return SEPARATOR;
    }
  }
}
