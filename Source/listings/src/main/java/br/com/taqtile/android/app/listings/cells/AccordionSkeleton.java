package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 29/05/17.
 */

public class AccordionSkeleton extends LinearLayout {
  public AccordionSkeleton(Context context) {
    super(context);
    init();
  }

  public AccordionSkeleton(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public AccordionSkeleton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_accordion_skeleton, this, true);

    setupView();
  }

  private void setupView(){
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setLayoutParams(layoutParams);
    setOrientation(VERTICAL);
  }

}
