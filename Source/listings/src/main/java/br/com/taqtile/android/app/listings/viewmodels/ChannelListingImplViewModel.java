package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelListingViewModel;

/**
 * Created by taqtile on 3/23/17.
 */

public class ChannelListingImplViewModel implements ChannelListingViewModel {

  public ChannelListingImplViewModel(String institutionName, int postsCounter, int followersCounter, String id, String topicName) {
    this.institutionName = institutionName;
    this.postsCounter = postsCounter;
    this.followersCounter = followersCounter;
    this.id = id;
    this.topicName = topicName;
  }

  private String institutionName;
  private int postsCounter;
  private int followersCounter;
  private String id;
  private String topicName;

  @Override
  public void setInstitutionName(String institutionName) {

    this.institutionName = institutionName;
  }

  @Override
  public String getInstitutionName() {
    return institutionName;
  }

  @Override
  public void setPostsCounter(int postsCounter) {

    this.postsCounter = postsCounter;
  }

  @Override
  public int getPostsCounter() {
    return postsCounter;
  }

  @Override
  public void setFollowersCounter(int followersCounter) {

    this.followersCounter = followersCounter;
  }

  @Override
  public int getFollowersCounter() {
    return followersCounter;
  }

  @Override
  public void setId(String id) {

    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setTopicName(String topicName) {

    this.topicName = topicName;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }
}
