package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelDetailsHeaderViewModel;

/**
 * Created by taqtile on 06/04/17.
 */

public class ChannelDetailsHeaderImplViewModel implements ChannelDetailsHeaderViewModel {
  private String id;
  private String imageUrl;
  private String channelName;
  private String channelDescription;
  private boolean isFollowing;
  private int totalPosts;

  public ChannelDetailsHeaderImplViewModel(String id, String imageUrl, String channelName, String channelDescription, boolean isFollowing, int totalPosts) {
    this.id = id;
    this.imageUrl = imageUrl;
    this.channelName = channelName;
    this.channelDescription = channelDescription;
    this.isFollowing = isFollowing;
    this.totalPosts = totalPosts;
  }

  @Override
  public void setId(String id) {

    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setImageUrl(String imageUrl) {

    this.imageUrl = imageUrl;
  }

  @Override
  public String getImageUrl() {
    return imageUrl;
  }

  @Override
  public void setChannelName(String channelName) {

    this.channelName = channelName;
  }

  @Override
  public String getChannelName() {
    return channelName;
  }

  @Override
  public void setChannelDescription(String channelDescription) {

    this.channelDescription = channelDescription;
  }

  @Override
  public String getChannelDescription() {
    return channelDescription;
  }

  @Override
  public void setIsFollowing(boolean isFollowing) {

    this.isFollowing = isFollowing;
  }

  @Override
  public boolean isFollowing() {
    return isFollowing;
  }

  public int getTotalPosts() {
    return totalPosts;
  }

  public void setTotalPosts(int totalPosts) {
    this.totalPosts = totalPosts;
  }
}
