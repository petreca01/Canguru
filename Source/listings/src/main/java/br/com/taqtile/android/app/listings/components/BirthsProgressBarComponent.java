package br.com.taqtile.android.app.listings.components;

import android.content.Context;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 5/16/17.
 */

public class BirthsProgressBarComponent extends GridLayout {

  ProgressBarAndCaptionComponent naturalBirthProgressbar;
  ProgressBarAndCaptionComponent cesareanBirthProgressbar;

  public BirthsProgressBarComponent(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  public BirthsProgressBarComponent(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public BirthsProgressBarComponent(Context context) {
    super(context);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_births_progress_bar, this, true);
    bindViews();
  }

  private void bindViews() {
    naturalBirthProgressbar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_cell_natural_birth_progressbar);
    cesareanBirthProgressbar = (ProgressBarAndCaptionComponent) findViewById(R.id.component_maternity_cell_ceserean_birth_progressbar);
  }

  public void setNaturalBirthPercent(int percent) {
    naturalBirthProgressbar.setProgress(percent);
  }

  public void setCesareanBirthPercent(int percent) {
    cesareanBirthProgressbar.setProgress(percent);
  }
}

