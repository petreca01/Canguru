package br.com.taqtile.android.app.listings.support.helpers;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.misc.utils.RatioImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by taqtile on 01/06/17.
 */

public class YoutubeHelper {

  private YouTubePlayerFragment youtubeFragment;

  public YoutubeHelper(YouTubePlayerFragment youtubeFragment) {
    this.youtubeFragment = youtubeFragment;
  }

  public void initializeYoutube(String videoId, ImageView image, FrameLayout video) {
    this.youtubeFragment.initialize(Constants.GOOGLE_API_KEY, new YouTubePlayer.OnInitializedListener() {
      @Override
      public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(videoId);
        youTubePlayer.setShowFullscreenButton(false);
      }

      @Override
      public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        video.setVisibility(GONE);
        image.setVisibility(VISIBLE);
      }
    });
  }
}
