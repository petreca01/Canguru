package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 06/04/17.
 */

public class SectionHeader extends LinearLayout {

  private CustomTextView text;
  private CustomTextView link;
  private SectionHeaderListener sectionHeaderListener;
  private FrameLayout sectionHeaderLinkContainer;

  String textAttr;
  String linkAttr;

  public SectionHeader(Context context) {
    super(context);
    init();
  }

  public SectionHeader(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();

  }

  public SectionHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();

  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_section_header, this, true);

    bindViews();
    setupView();
    setupLinkButton();
  }

  private void setupView(){
    LinearLayout.LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setOrientation(VERTICAL);
    setLayoutParams(layoutParams);

    if(textAttr != null){
      text.setText(textAttr);
    }

    if(linkAttr != null){
      link.setText(linkAttr);
    }
  }

  private void bindViews(){
    text = (CustomTextView) findViewById(R.id.component_section_header_text);
    link = (CustomTextView) findViewById(R.id.component_section_header_link);
    sectionHeaderLinkContainer = (FrameLayout) findViewById(R.id.component_section_header_link_container);
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SectionHeader);

    textAttr = a.getString(R.styleable.SectionHeader_text);
    linkAttr = a.getString(R.styleable.SectionHeader_link);
    a.recycle();
  }

  private void setupLinkButton() {
    sectionHeaderLinkContainer.setOnClickListener(view -> {
      if (sectionHeaderListener != null){
        sectionHeaderListener.onLinkButtonClick();
      }
    });
    link.setOnClickListener(view -> {
      if (sectionHeaderListener != null){
        sectionHeaderListener.onLinkButtonClick();
      }
    });
  }
  public void setText(String text){
    this.text.setText(text);
  }

  public void setLink(String link) { this.link.setText(link); }

  public interface SectionHeaderListener {
    void onLinkButtonClick();
  }

  public void setSectionHeaderListener(SectionHeaderListener sectionHeaderListener) {
    this.sectionHeaderListener = sectionHeaderListener;
  }
}
