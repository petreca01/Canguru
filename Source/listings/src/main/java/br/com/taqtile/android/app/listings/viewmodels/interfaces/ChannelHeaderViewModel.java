package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 10/07/17.
 */

public interface ChannelHeaderViewModel extends SearchListingViewModel {
  void setSectionHeaderText(String headerText);
  String getSectionHeaderText();
}
