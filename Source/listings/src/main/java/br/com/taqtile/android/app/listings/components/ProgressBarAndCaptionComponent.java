package br.com.taqtile.android.app.listings.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/16/17.
 */

public class ProgressBarAndCaptionComponent extends LinearLayout {

  ProgressBar progressBar;
  CustomTextView caption;

  private Drawable progressDrawable;

  public ProgressBarAndCaptionComponent(Context context) {
    super(context);
    init(context);
  }

  public ProgressBarAndCaptionComponent(Context context, AttributeSet attrs) {
    super(context, attrs);
    getXmlResources(attrs);
    init(context);
  }

  public ProgressBarAndCaptionComponent(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    getXmlResources(attrs);
    init(context);
  }

  private void getXmlResources(AttributeSet attrs) {
    TypedArray ta = getResources().obtainAttributes(attrs, R.styleable.ProgressBarAndCaptionComponent);
    progressDrawable = ta.getDrawable(R.styleable.ProgressBarAndCaptionComponent_progressDrawable);
    if (progressDrawable == null) {
      progressDrawable = ContextCompat.getDrawable(getContext(), R.drawable.bkg_progressbar_color_success);
    }
    ta.recycle();
  }
  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    final View view = inflater.inflate(R.layout.component_progress_bar_and_caption, this, true);
    bindViews();
    this.setProgressDrawable(progressDrawable);
  }

  public void bindViews() {
    progressBar = (ProgressBar) findViewById(R.id.component_progress_bar_and_caption_progress_bar);
    caption = (CustomTextView) findViewById(R.id.component_progress_bar_and_caption_caption);
  }

  public void setProgress(int progress) {
    this.progressBar.setProgress(progress);
    this.caption.setText(getResources().getString(R.string.percent, progress));
  }

  public void setProgressDrawable(@DrawableRes int progressDrawable) {
    this.setProgressDrawable(ContextCompat.getDrawable(getContext(), progressDrawable));
  }

  public void setProgressDrawable(Drawable progressDrawable) {
    this.progressDrawable = progressDrawable;
    this.progressBar.setProgressDrawable(progressDrawable);
  }

}
