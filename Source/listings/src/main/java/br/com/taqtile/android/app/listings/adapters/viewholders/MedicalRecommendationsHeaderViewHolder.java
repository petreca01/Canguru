package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.MedicalRecommendationsHeaderCell;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsHeaderViewHolder extends RecyclerView.ViewHolder {

  MedicalRecommendationsHeaderCell medicalRecommendationsHeaderCell;

  public MedicalRecommendationsHeaderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.medicalRecommendationsHeaderCell = (MedicalRecommendationsHeaderCell) itemView;
  }

  public MedicalRecommendationsHeaderCell getMedicalRecommendationsHeaderCell() {
    return medicalRecommendationsHeaderCell;
  }
}
