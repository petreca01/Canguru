package br.com.taqtile.android.app.listings.questionnairecomponents;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.edittexts.CustomDropDown;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 22/05/17.
 */

public class RangeQuestionnaireComponent extends LinearLayout implements QuestionnaireResult, CustomDropDown.CustomDropDownListener {

  private List<String> rangeItems;
  private List<String> answer;

  private CustomDropDown dropdown;
  private String dropdownLabel;

  private DescriptionHeaderWithImage questionDescription;
  private String questionId;

  private AnswerListener answerListener;

  public RangeQuestionnaireComponent(Context context) {
    super(context);
  }

  public RangeQuestionnaireComponent(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public RangeQuestionnaireComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public RangeQuestionnaireComponent(Context context, AnswerListener listener, QuestionViewModel questionViewModel) {
    super(context);
    this.questionId = String.valueOf(questionViewModel.getQuestionId());
    this.answerListener = listener;
    init(getRange(questionViewModel.getMin(), questionViewModel.getMax())
      , questionViewModel.getQuestion(), questionViewModel.getQuestion(),
      questionViewModel.getDescription(), questionViewModel.getAnswer());
  }

  private List<String> getRange(Integer min, Integer max) {
    List<String> answerRange = new ArrayList<>();
    for (int i = min; i <= max; i++) {
      answerRange.add(String.valueOf(i));
    }
    return answerRange;
  }

  private void init(List<String> rangeItems, String dropdownLabel, String question,
                    String questionDescription, String answer) {
    LayoutInflater layoutInflater = LayoutInflater.from(getContext());
    View v = layoutInflater.inflate(R.layout.component_range_questionnaire, this, true);

    bindViews();
    setupAnswer();
    setupDropdown(rangeItems, dropdownLabel, question, questionDescription);
    setAnswerIfAlreadyAnswered(answer);
  }

  private void bindViews() {
    dropdown = (CustomDropDown) findViewById(R.id.component_range_questionnaire_dropdown);
    questionDescription = (DescriptionHeaderWithImage) findViewById(R.id.component_range_questionnaire_question_description);
  }

  private void setupAnswer() {
    answer = new ArrayList<>();
  }

  private void setupDropdown(List<String> rangeItems, String dropdownLabel, String question, String questionDescription) {
    this.rangeItems = rangeItems;
    this.dropdownLabel = dropdownLabel;
    this.questionDescription.setTitle(question);
    this.questionDescription.setDescription(questionDescription);
    dropdown.setListener(this);
    dropdown.setValues(rangeItems);
    dropdown.setLabel(dropdownLabel);
  }

  private void setAnswerIfAlreadyAnswered(String answer) {
    if (answer != null && !answer.isEmpty()) {
      this.answer.add(answer);
      dropdown.setSelectedValue(this.rangeItems.indexOf(answer));
    }
    answerListener.onAnswerSelected(this);
  }

  @Override
  public List<String> getResult() {
    return getSelectedRangeItem();
  }

  private List<String> getSelectedRangeItem() {
    answer.clear();
    answer.add(dropdown.getSelectedValue());
    return answer;
  }

  @Override
  public boolean isResultValid() {
    return validAnswerList() && this.rangeItems.contains(answer.get(0));
  }

  private boolean validAnswerList() {
    return this.answer != null && !this.answer.isEmpty() && this.answer.size() == 1;
  }

  @Override
  public String getQuestionId() {
    return questionId;
  }

  @Override
  public void onItemSelectedListener(int position) {
    if (answerListener != null) {
      answerListener.onAnswerSelected(this);
    }
  }

}
