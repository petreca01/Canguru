package br.com.taqtile.android.app.listings.viewmodels;

import java.util.List;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityDetailsViewModel {

  private MaternityViewModel maternity;
  private List<MaternityCommentViewModel> comments;

  public MaternityDetailsViewModel(MaternityViewModel maternity, List<MaternityCommentViewModel> comements) {
    this.maternity = maternity;
    this.comments = comements;
  }

  public MaternityViewModel getMaternity() {
    return maternity;
  }

  public List<MaternityCommentViewModel> getComments() {
    return comments;
  }

  public void setComments(List<MaternityCommentViewModel> comments) {
    this.comments = comments;
  }
}
