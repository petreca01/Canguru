package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 16/05/17.
 */

public class AppointmentFeedSeparator extends RelativeLayout {

  public AppointmentFeedSeparator(Context context) {
    super(context);
    init();
  }

  public AppointmentFeedSeparator(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public AppointmentFeedSeparator(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_appointment_feed_separator, this, true);

    setupView();
  }

  private void setupView(){
    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setLayoutParams(layoutParams);
  }

}
