package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by taqtile on 5/10/17.
 */

public class MaternityViewModel implements Parcelable {

  private Integer id;

  private String state;

  private String city;

  private String name;

  private String address;

  private String phone;

  private String logo;

  private String image;

  private Float rate;

  private Integer totalRates;

  private String showMap;

  private String currentBirthsSource;

  private Float currentPercentNormalBirth;

  private Float currentPercentCsections;

  private Integer oneRates;

  private Integer twoRates;

  private Integer threeRates;

  private Integer fourRates;

  private Integer fiveRates;

  private Integer myRate;

  private String about;

  private String birthSchool;

  private String visitRules;

  private String availableMidwife;

  private String bathBirth;

  private String pppRoom;

  private String crowdRoom;

  private String accreditations;

  public MaternityViewModel(Integer id, String state, String city, String name, String about,
                            String address, String phone, String logo, String image, Float rate,
                            Integer totalRates, String showMap, String currentBirthsSource,
                            Float currentPercentNormalBirth, Float currentPercentCsections,
                            Integer oneRates, Integer twoRates, Integer threeRates,
                            Integer fourRates, Integer fiveRates, Integer myRate,
                            String birthSchool, String visitRules, String availableMidwife,
                            String bathBirth, String pppRoom, String crowdRoom,
                            String accreditations) {
    this.id = id;
    this.state = state;
    this.city = city;
    this.name = name;
    this.about = about;
    this.address = address;
    this.phone = phone;
    this.logo = logo;
    this.image = image;
    this.rate = rate;
    this.totalRates = totalRates;
    this.showMap = showMap;
    this.currentBirthsSource = currentBirthsSource;
    this.currentPercentNormalBirth = currentPercentNormalBirth;
    this.currentPercentCsections = currentPercentCsections;
    this.oneRates = oneRates;
    this.twoRates = twoRates;
    this.threeRates = threeRates;
    this.fourRates = fourRates;
    this.fiveRates = fiveRates;
    this.myRate = myRate;
    this.birthSchool = birthSchool;
    this.visitRules = visitRules;
    this.availableMidwife = availableMidwife;
    this.bathBirth = bathBirth;
    this.pppRoom = pppRoom;
    this.crowdRoom = crowdRoom;
    this.accreditations = accreditations;

  }

  public MaternityViewModel(int i, String s, String s1, String s2, String s3, String s4, String s5,
                            String s6, Float aFloat, int i1, String s7, Float aFloat1, Float aFloat2,
                            int i2, int i3, int i4, int i5, int i6, int i7) {

  }

  public Integer getId() {
    return id;
  }

  public String getState() {
    return state;
  }

  public String getCity() {
    return city;
  }

  public String getName() {
    return name;
  }

  public String getAbout() {
    return about;
  }

  public String getAddress() {
    return address;
  }

  public String getPhone() {
    return phone;
  }

  public String getLogo() {
    return logo;
  }

  public String getImage() {
    return image;
  }

  public Float getRate() {
    return rate;
  }

  public Integer getTotalRates() {
    return totalRates;
  }

  public String getShowMap() {
    return showMap;
  }

  public String getCurrentBirthsSource() {
    return currentBirthsSource;
  }

  public Float getCurrentPercentNormalBirth() {
    return currentPercentNormalBirth;
  }

  public Float getCurrentPercentCsections() {
    return currentPercentCsections;
  }

  public Integer getOneRates() {
    return oneRates;
  }

  public Integer getTwoRates() {
    return twoRates;
  }

  public Integer getThreeRates() {
    return threeRates;
  }

  public Integer getFourRates() {
    return fourRates;
  }

  public Integer getFiveRates() {
    return fiveRates;
  }

  public Integer getMyRate() {
    return myRate;
  }

  public String getBirthSchool() {
    return birthSchool;
  }

  public String getVisitRules() {
    return visitRules;
  }

  public String getAvailableMidwife() {
    return availableMidwife;
  }

  public String getBathBirth() {
    return bathBirth;
  }

  public String getPppRoom() {
    return pppRoom;
  }

  public String getCrowdRoom() {
    return crowdRoom;
  }

  public String getAccreditations() {
    return accreditations;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.id);
    dest.writeString(this.state);
    dest.writeString(this.city);
    dest.writeString(this.name);
    dest.writeString(this.address);
    dest.writeString(this.phone);
    dest.writeString(this.logo);
    dest.writeString(this.image);
    dest.writeValue(this.rate);
    dest.writeValue(this.totalRates);
    dest.writeString(this.showMap);
    dest.writeString(this.currentBirthsSource);
    dest.writeValue(this.currentPercentNormalBirth);
    dest.writeValue(this.currentPercentCsections);
    dest.writeValue(this.oneRates);
    dest.writeValue(this.twoRates);
    dest.writeValue(this.threeRates);
    dest.writeValue(this.fourRates);
    dest.writeValue(this.fiveRates);
    dest.writeValue(this.myRate);
    dest.writeString(this.about);
    dest.writeString(this.birthSchool);
    dest.writeString(this.visitRules);
    dest.writeString(this.availableMidwife);
    dest.writeString(this.bathBirth);
    dest.writeString(this.pppRoom);
    dest.writeString(this.crowdRoom);
    dest.writeString(this.accreditations);
  }

  protected MaternityViewModel(Parcel in) {
    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    this.state = in.readString();
    this.city = in.readString();
    this.name = in.readString();
    this.address = in.readString();
    this.phone = in.readString();
    this.logo = in.readString();
    this.image = in.readString();
    this.rate = (Float) in.readValue(Float.class.getClassLoader());
    this.totalRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.showMap = in.readString();
    this.currentBirthsSource = in.readString();
    this.currentPercentNormalBirth = (Float) in.readValue(Float.class.getClassLoader());
    this.currentPercentCsections = (Float) in.readValue(Float.class.getClassLoader());
    this.oneRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.twoRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.threeRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.fourRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.fiveRates = (Integer) in.readValue(Integer.class.getClassLoader());
    this.myRate = (Integer) in.readValue(Integer.class.getClassLoader());
    this.about = in.readString();
    this.birthSchool = in.readString();
    this.visitRules = in.readString();
    this.availableMidwife = in.readString();
    this.bathBirth = in.readString();
    this.pppRoom = in.readString();
    this.crowdRoom = in.readString();
    this.accreditations = in.readString();
  }

  public static final Creator<MaternityViewModel> CREATOR = new Creator<MaternityViewModel>() {
    @Override
    public MaternityViewModel createFromParcel(Parcel source) {
      return new MaternityViewModel(source);
    }

    @Override
    public MaternityViewModel[] newArray(int size) {
      return new MaternityViewModel[size];
    }
  };
}
