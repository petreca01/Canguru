package br.com.taqtile.android.app.listings.questionnairecomponents;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.cells.SingleChoiceCell;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionType;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;

/**
 * Created by taqtile on 19/05/17.
 */

public class SingleChoiceQuestionnaireComponent extends LinearLayout implements
  QuestionnaireResult, SingleChoiceCell.Listener {

  private LinearLayout choicesContainer;

  private List<String> choices;
  private AnswerListener answerListener;

  private DescriptionHeaderWithImage questionDescription;
  private String questionId;
  private List<String> answerList;
  private QuestionViewModel questionViewModel;

  public SingleChoiceQuestionnaireComponent(Context context) {
    super(context);
  }

  public SingleChoiceQuestionnaireComponent(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public SingleChoiceQuestionnaireComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public SingleChoiceQuestionnaireComponent(Context context, AnswerListener listener, QuestionViewModel questionViewModel) {
    super(context);
    this.questionViewModel = questionViewModel;
    this.questionId = String.valueOf(questionViewModel.getQuestionId());
    this.answerListener = listener;
    init(questionViewModel.getAlternatives(), questionViewModel.getQuestion(),
      questionViewModel.getDescription(), questionViewModel.getAnswer());
  }

  private void init(List<String> choices, String question,
                    String questionDescription, String answer) {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_single_choice_questionnaire_component, this, true);
    bindViews();
    setupAnswersList();
    setAnswerIfAlreadyAnswered(answer);
    buildChoices(choices, question, questionDescription);
  }

  private void bindViews() {
    choicesContainer = (LinearLayout) findViewById(R.id.component_single_choice_questionnaire_choices_container);
    questionDescription = (DescriptionHeaderWithImage) findViewById(R.id.component_single_choice_questionnaire_component_question_description);
  }

  private void setupAnswersList() {
    answerList = new ArrayList<>();
  }

  private void setAnswerIfAlreadyAnswered(String answer) {
    if (answer != null && !answer.isEmpty()) {
      addAnswer(answer);
    }
  }

  private void buildChoices(List<String> choices, String question, String questionDescription) {
    this.choices = new ArrayList<>();
    this.choices.addAll(choices);
    this.questionDescription.setTitle(question);
    this.questionDescription.setDescription(questionDescription);
    String choiceText = "";
    if (this.choices != null && !this.choices.isEmpty()) {
      for (int i = 0; i < choices.size(); i++) {
        SingleChoiceCell choice = new SingleChoiceCell(getContext());
        choiceText = getChoiceText(choices.get(i));
        choice.setText(choiceText);
        if (alreadyAnswered(i)) {
          choice.setIsSelected(true);
        }
        choice.setListener(this);
        choicesContainer.addView(choice);
      }
      answerListener.onAnswerSelected(this);
    }
  }

  private boolean alreadyAnswered(int i) {
    return validAnswerList() && choices.get(i).equals(this.answerList.get(0));
  }

  @Override
  public List<String> getResult() {
    return this.answerList;
  }

  @Override
  public boolean isResultValid() {
    return validAnswerList() && this.choices.contains(answerList.get(0));
  }

  @Override
  public String getQuestionId() {
    return questionId;
  }

  @Override
  public void onCellClick(String choice, SingleChoiceCell singleChoiceCell) {
    if (answerListener == null) {
      return;
    }
    unselectCells(singleChoiceCell);
    String choiceValue = getChoiceValue(choice);
    if (isAlreadyChecked(choiceValue)) {
      removeAnswer(choiceValue);
    } else {
      addAnswer(choiceValue);
    }
    answerListener.onAnswerSelected(this);
  }

  private void unselectCells(SingleChoiceCell singleChoiceCell) {
    for (int i = 0; i < choicesContainer.getChildCount(); i++) {
      SingleChoiceCell cell = (SingleChoiceCell) choicesContainer.getChildAt(i);
      if (!singleChoiceCell.equals(cell)) {
        cell.setIsSelected(false);
      }
    }
  }

  private boolean isAlreadyChecked(String choice) {
    return validAnswerList() && answerList.contains(choice);
  }

  private boolean validAnswerList() {
    return this.answerList != null && !this.answerList.isEmpty() && this.answerList.size() == 1;
  }

  private void removeAnswer(String answer) {
    answerList.remove(answer);
  }

  private void addAnswer(String answer) {
    answerList.clear();
    answerList.add(answer);
  }

  private String getChoiceText(String choice){
    if (this.questionViewModel.getType() == QuestionType.QUESTION_TYPE_BOOLEAN){
      if (choice == "1") { // Sim
        return getContext().getString(
          R.string.component_imc_questionnaire_yes_option);
      } else {
        return getContext().getString(
          R.string.component_imc_questionnaire_no_option);
      }
    }
    else {
      return choice;
    }
  }
  private String getChoiceValue(String choice){
    if (this.questionViewModel.getType() == QuestionType.QUESTION_TYPE_BOOLEAN) {
      if (choice == getContext().getString(
        R.string.component_imc_questionnaire_yes_option)) {
        return "1"; // Sim
      } else {
        return "0"; // Não
      }
    }
    else {
      return choice;
    }
  }
}
