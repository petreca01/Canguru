package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/22/17.
 */

public interface SearchListingViewModel {
    void setId(String id);
    String getId();
    void setTopicName(String topicName);
    String getTopicName();

}
