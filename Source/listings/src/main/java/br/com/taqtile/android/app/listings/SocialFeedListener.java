package br.com.taqtile.android.app.listings;

/**
 * Created by taqtile on 3/21/17.
 */

public interface SocialFeedListener {
    void onProfileRegionClick(String id, int type);
    void onCardClick(String id, int type);
    void onLikeClick(boolean isCounterActivated, String id, String type);
    void onCommentsCounterClick(int id, int type);
}
