package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/18/17.
 */

public interface SocialFeedCommunityViewModel extends SocialFeedViewModel {
  void setProfileUrl(String iconUrl);
  String getProfileUrl();
  Integer getUserId();
}
