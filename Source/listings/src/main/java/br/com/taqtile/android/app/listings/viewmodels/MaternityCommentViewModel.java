package br.com.taqtile.android.app.listings.viewmodels;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityCommentViewModel {

  private Integer id;

  private Integer userId;

  private String comment;

  private String createdAt;

  private String username;

  private String userPicture;

  public MaternityCommentViewModel(Integer id, Integer userId, String comment, String createdAt, String username, String userPicture) {
    this.id = id;
    this.userId = userId;
    this.comment = comment;
    this.createdAt = createdAt;
    this.username = username;
    this.userPicture = userPicture;
  }

  public Integer getId() {
    return id;
  }

  public Integer getUserId() {
    return userId;
  }

  public String getComment() {
    return comment;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUsername() {
    return username;
  }

  public String getUserPicture() {
    return userPicture;
  }
}
