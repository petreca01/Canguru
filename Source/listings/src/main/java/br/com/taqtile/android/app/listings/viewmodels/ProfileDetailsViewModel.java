package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;

/**
 * Created by taqtile on 4/27/17.
 */

public class ProfileDetailsViewModel implements ListingsViewModel {

  private Integer id;

  private String userType;

  private String name;

  private String babyName;

  private String about;

  private String profilePicture;

  private String coverPicture;

  private Integer pregnancyWeeks;

  private Integer pregnancyDays;

  private String city;

  private String state;

  private boolean locationPrivacyAgreed;

  private boolean isFollowing;

  private String createdAt;

  private String updatedAt;

  public ProfileDetailsViewModel() {
  }

  public ProfileDetailsViewModel(Integer id, String name, String babyName, String about,
                                 String profilePicture, String coverPicture, Integer pregnancyWeeks,
                                 Integer pregnancyDays, String city, String state,
                                 boolean locationPrivacyAgreed, String createdAt, String updatedAt) {
    this.id = id;
    this.name = name;
    this.babyName = babyName;
    this.about = about;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.pregnancyWeeks = pregnancyWeeks;
    this.pregnancyDays = pregnancyDays;
    this.city = city;
    this.state = state;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public ProfileDetailsViewModel(Integer id, String userType, String name, String babyName,
                                 String about, String profilePicture, String coverPicture,
                                 Integer pregnancyWeeks, Integer pregnancyDays, boolean isFollowing,
                                 String city, String state, boolean locationPrivacyAgreed,
                                 String createdAt, String updatedAt) {
    this.id = id;
    this.userType = userType;
    this.name = name;
    this.babyName = babyName;
    this.about = about;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.pregnancyWeeks = pregnancyWeeks;
    this.pregnancyDays = pregnancyDays;
    this.isFollowing = isFollowing;
    this.city = city;
    this.state = state;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public Integer getId() {
    return id;
  }

  public String getUserType() {
    return userType;
  }

  public String getName() {
    return name;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getAbout() {
    return about;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public Integer getPregnancyWeeks() {
    return pregnancyWeeks;
  }

  public Integer getPregnancyDays() {
    return pregnancyDays;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public boolean isLocationPrivacyAgreed() {
    return locationPrivacyAgreed;
  }

  public boolean isFollowing() {
    return isFollowing;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

}
