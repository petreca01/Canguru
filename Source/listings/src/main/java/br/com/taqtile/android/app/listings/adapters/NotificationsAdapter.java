package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.adapters.viewholders.NotificationViewholder;
import br.com.taqtile.android.app.listings.cells.TextWithLabelAndDotCell;
import br.com.taqtile.android.app.listings.viewmodels.NotificationViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 15/05/17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter implements TextWithLabelAndDotCell.Listener {

  private List<NotificationViewModel> notificationViewModels;
  private NotificationsAdapter.Listener listener;

  public NotificationsAdapter(List<NotificationViewModel> notificationViewModels, NotificationsAdapter.Listener listener) {
    this.notificationViewModels = notificationViewModels;
    this.listener = listener;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new NotificationViewholder(new TextWithLabelAndDotCell(parent.getContext()));
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    setupNotification((NotificationViewholder) holder, notificationViewModels.get(position), position);
  }

  private void setupNotification(NotificationViewholder notificationViewholder, NotificationViewModel notificationViewModel, int position){
    TextWithLabelAndDotCell notification = notificationViewholder.getNotification();

    notification.setHeader(notificationViewModel.getContent(), notificationViewModel.isVisualized());
    notification.setLabel(DateFormatterHelper.formatDateWithHours(notificationViewModel.getCreatedAt(), notification.getContext()));
    if (position != notificationViewModels.size() - 2){
      notificationViewholder.getNotification().showSeparator();
    }
    notification.setListener(this);
    notification.setViewIndex(position);
  }

  @Override
  public int getItemCount() {
    return notificationViewModels.size();
  }

  @Override
  public void onViewClick(int id) {
    if (listener != null) {
      listener.onCellClick(notificationViewModels.get(id));
    }
  }

  public interface Listener {
    void onCellClick(NotificationViewModel notification);
  }
}
