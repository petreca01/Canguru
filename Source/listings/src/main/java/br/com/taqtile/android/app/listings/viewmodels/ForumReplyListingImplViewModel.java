package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumReplyListingViewModel;

/**
 * Created by taqtile on 18/07/17.
 */

public class ForumReplyListingImplViewModel implements ForumReplyListingViewModel {
  private int totalPosts;
  private String id;
  private String topicName;
  private int likesCounter;
  private int followersCounter;
  private String description;
  private int contentId;
  private int contentParentId;

  public ForumReplyListingImplViewModel(int totalPosts, String id, String topicName, int likesCounter, int followersCounter, String description, int contentId, int contentParentId) {
    this.totalPosts = totalPosts;
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.followersCounter = followersCounter;
    this.description = description;
    this.contentId = contentId;
    this.contentParentId = contentParentId;
  }

  @Override
  public void setTotalPosts(int totalPosts) {

    this.totalPosts = totalPosts;
  }

  @Override
  public int getTotalPosts() {
    return totalPosts;
  }

  @Override
  public void setId(String id) {

    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setTopicName(String topicName) {

    this.topicName = topicName;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Override
  public void setLikesCounter(int likesCounter) {

    this.likesCounter = likesCounter;
  }

  @Override
  public int getLikesCounter() {
    return likesCounter;
  }

  @Override
  public void setCommentsCounter(int followersCounter) {

    this.followersCounter = followersCounter;
  }

  @Override
  public int getCommentsCounter() {
    return followersCounter;
  }

  @Override
  public void setDescription(String description) {

    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setContentId(int contentId) {

    this.contentId = contentId;
  }

  @Override
  public int getContentId() {
    return contentId;
  }

  @Override
  public void setContentParentId(int contentParentId) {

    this.contentParentId = contentParentId;
  }

  @Override
  public int getContentParentId() {
    return contentParentId;
  }
}
