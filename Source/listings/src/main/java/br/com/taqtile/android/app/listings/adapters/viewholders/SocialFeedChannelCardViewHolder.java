package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.cells.ChannelCard;

/**
 * Created by taqtile on 3/20/17.
 */

public class SocialFeedChannelCardViewHolder extends RecyclerView.ViewHolder {

    ChannelCard socialFeedChannelCard;

    public SocialFeedChannelCardViewHolder(View itemView) {
        super(itemView);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = (int) itemView.getResources().getDimension(R.dimen.margin_medium);
        layoutParams.rightMargin = (int) itemView.getResources().getDimension(R.dimen.margin_medium);

        itemView.setLayoutParams(layoutParams);

        this.socialFeedChannelCard = (ChannelCard) itemView;
    }

    public ChannelCard getChannelCard() {
        return socialFeedChannelCard;
    }

}
