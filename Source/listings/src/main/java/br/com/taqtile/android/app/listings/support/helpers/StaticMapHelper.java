package br.com.taqtile.android.app.listings.support.helpers;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import br.com.taqtile.android.app.listings.Constants;

/**
 * Created by taqtile on 02/06/17.
 */

public class StaticMapHelper {

  private static final String PARAMETER_CENTER = "center";
  private static final String PARAMETER_SENSOR = "sensor";
  private static final String PARAMETER_SIZE = "size";
  private static final String PARAMETER_KEY = "key";
  private static final String PARAMETER_ZOOM = "zoom";
  public static final String PARAMETER_MARKERS = "markers";
  private String mKey;
  private String mBaseUrl;

  public StaticMapHelper() {
    this.mBaseUrl = Constants.MAPS_BASE_URL;
    this.mKey = Constants.GOOGLE_API_KEY;
  }

  public String getMapUrl(String address, int width, int height) {
    final Uri.Builder b = Uri.parse(mBaseUrl).buildUpon();
    b.appendQueryParameter(PARAMETER_KEY, mKey);
    b.appendQueryParameter(PARAMETER_SIZE, width + "x" + height);
    b.appendQueryParameter(PARAMETER_SENSOR, "false");
    b.appendQueryParameter(PARAMETER_ZOOM, "15");
    b.appendQueryParameter(PARAMETER_CENTER, address);
    b.appendQueryParameter(PARAMETER_MARKERS, address);
    return b.toString();
  }
}
