package br.com.taqtile.android.app.listings.questionnairecomponents;

import java.util.List;

/**
 * Created by taqtile on 23/05/17.
 */

public interface QuestionnaireResult {
  List<String> getResult();
  boolean isResultValid();
  String getQuestionId();
}
