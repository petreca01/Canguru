package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.PublicProfileHeaderCell;

/**
 * Created by taqtile on 25/04/17.
 */

public class PublicProfileHeaderViewHolder extends RecyclerView.ViewHolder {

  PublicProfileHeaderCell publicProfileHeaderCell;

  public PublicProfileHeaderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.publicProfileHeaderCell = (PublicProfileHeaderCell) itemView;
  }

  public PublicProfileHeaderCell getPublicProfileHeaderCell() {
    return publicProfileHeaderCell;
  }
}
