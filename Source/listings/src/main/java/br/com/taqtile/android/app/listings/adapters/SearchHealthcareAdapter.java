package br.com.taqtile.android.app.listings.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.adapters.viewholders.HealthcareWithPartnerViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.HealthcareWithoutPartnerViewHolder;
import br.com.taqtile.android.app.listings.cells.HealthcareWithPartnerCell;
import br.com.taqtile.android.app.listings.cells.HealthcareWithoutPartnerCell;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;

/**
 * Created by taqtile on 18/04/17.
 */

public class SearchHealthcareAdapter extends RecyclerView.Adapter {

  private List<HealthOperatorViewModel> healthOperatorViewModels;
  private SearchHealthcareAdapterListener channelAdapterListener;

  public SearchHealthcareAdapter(List<HealthOperatorViewModel> healthOperatorViewModels) {
    this.healthOperatorViewModels = healthOperatorViewModels;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == Constants.HEALTHCARE_PARTNER) {
      return new HealthcareWithPartnerViewHolder(new HealthcareWithPartnerCell(parent.getContext()));
    } else {
      return new HealthcareWithoutPartnerViewHolder(new HealthcareWithoutPartnerCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case Constants.HEALTHCARE_PARTNER:
        setupHealthcareWithPartnerCell((HealthcareWithPartnerViewHolder) holder, position);
        break;
      case Constants.HEALTHCARE_NOT_PARTNER:
        setupHealthcareWithoutPartnerCell((HealthcareWithoutPartnerViewHolder) holder, position);
        break;
    }
  }

  private void setupHealthcareWithPartnerCell(HealthcareWithPartnerViewHolder holder, int position) {
    HealthcareWithPartnerCell healthcareWithPartnerCell = holder.getHealthcareWithPartnerCell();
    HealthOperatorViewModel healthOperatorViewModel = healthOperatorViewModels.get(position);
    healthcareWithPartnerCell.setHeader(healthOperatorViewModel.getName());
    healthcareWithPartnerCell.setImage(healthOperatorViewModel.getLogo());
    healthcareWithPartnerCell.setListener(() -> {
      if (channelAdapterListener != null) {
        channelAdapterListener.onSearchHealthcareCellClick(position);
      }
    });
  }

  private void setupHealthcareWithoutPartnerCell(HealthcareWithoutPartnerViewHolder holder, int position) {
    HealthcareWithoutPartnerCell healthcareWithoutPartnerCell = holder.getHealthcareWithoutPartnerCell();
    HealthOperatorViewModel healthOperatorViewModel = healthOperatorViewModels.get(position);
    healthcareWithoutPartnerCell.setHeader(healthOperatorViewModel.getName());
    healthcareWithoutPartnerCell.setImage(healthOperatorViewModel.getLogo());
    healthcareWithoutPartnerCell.setListener(() -> {
      if (channelAdapterListener != null) {
        channelAdapterListener.onSearchHealthcareCellClick(position);
      }
    });
  }

  @Override
  public int getItemCount() {
    return healthOperatorViewModels.size();
  }

  @Override
  public int getItemViewType(int position) {
    if (healthOperatorViewModels.get(position).getHealthPlanPartner()) {
      return Constants.HEALTHCARE_PARTNER;
    } else {
      return Constants.HEALTHCARE_NOT_PARTNER;
    }
  }

  public interface SearchHealthcareAdapterListener {
    void onSearchHealthcareCellClick(int index);
  }

  public void setListener(
    SearchHealthcareAdapter.SearchHealthcareAdapterListener channelAdapterListener) {
    this.channelAdapterListener = channelAdapterListener;
  }

}
