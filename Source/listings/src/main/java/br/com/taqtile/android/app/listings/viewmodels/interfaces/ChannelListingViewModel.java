package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/23/17.
 */

public interface ChannelListingViewModel extends SearchListingViewModel {
  void setInstitutionName(String institutionName);

  String getInstitutionName();

  void setPostsCounter(int postsCounter);

  int getPostsCounter();

  void setFollowersCounter(int followersCounter);

  int getFollowersCounter();
}
