package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelSearchPostViewModel;

/**
 * Created by taqtile on 13/07/17.
 */

public class ChannelSearchPostImplViewModel implements ChannelSearchPostViewModel {

  private String id;
  private String topicName;
  private int likesCounter;
  private int followersCounter;
  private String description;
  private int totalPosts;
  private int contentId;
  private int contentInParentId;

  public ChannelSearchPostImplViewModel(String id, String topicName, int likesCounter, int followersCounter) {
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.followersCounter = followersCounter;
    this.description = "";
    totalPosts = 0;
  }

  public ChannelSearchPostImplViewModel(String id, String topicName, int likesCounter, int followersCounter, String description, int totalPosts, int contentId, int contentInParentId) {
    this.id = id;
    this.topicName = topicName;
    this.likesCounter = likesCounter;
    this.followersCounter = followersCounter;
    this.description = description;
    this.totalPosts= totalPosts;
    this.contentId = contentId;
    this.contentInParentId = contentInParentId;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Override
  public void setLikesCounter(int likesCounter) {

    this.likesCounter = likesCounter;
  }

  @Override
  public int getLikesCounter() {
    return likesCounter;
  }

  @Override
  public void setCommentsCounter(int followersCounter) {

    this.followersCounter = followersCounter;
  }

  @Override
  public int getCommentsCounter() {
    return followersCounter;
  }

  @Override
  public void setDescription(String description) {

    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setContentId(int contentId) {
    this.contentId = contentId;
  }

  @Override
  public int getContentId() {
    return contentId;
  }

  @Override
  public void setContentParentId(int contentParentId) {
    this.contentInParentId = contentParentId;
  }

  @Override
  public int getContentParentId() {
    return contentInParentId;
  }

  @Override
  public void setTotalPosts(int totalPosts) {
    this.totalPosts = totalPosts;
  }

  @Override
  public int getTotalPosts() {
    return totalPosts;
  }
}
