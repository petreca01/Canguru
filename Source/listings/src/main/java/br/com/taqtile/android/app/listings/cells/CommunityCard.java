package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

import static br.com.taqtile.android.app.listings.Constants.COMMUNITY_CELL;

/**
 * Created by taqtile on 3/16/17.
 */

public class CommunityCard extends CardView implements CounterWithDrawable.CounterWithDrawableListener {

  private RelativeLayout profileLayout;
  private CircleImageView profileImage;
  private CustomTextView profileName;
  private CustomTextView date;
  private CustomTextView heading;
  private CustomTextView body;
  private LinearLayout bodyLayout;
  private CounterWithDrawable likesCounter;
  private CustomTextView commentsCounter;
  private LinearLayout contentRoot;
  private RelativeLayout commentsCounterLayout;
  //Todo remove this variable after service provides a way to like and dislike
  private CustomTextView likesLabel;
  private int type;
  private Integer postId;
  private Integer userId;

  private CommunityCardListener listener;

  public CommunityCard(Context context) {
    super(context);
    init();
  }

  public CommunityCard(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CommunityCard(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_community_card, this, true);

    type = COMMUNITY_CELL;

    bindViews();

    setupView();

    setupButtons();

    setupLikeView();
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    setClipChildren(false);
    setClipToPadding(false);
    setLayoutParams(layoutParams);
    float cornerRadius = FloatToDpConverter.convertFromDpToFloat(getResources().getDimension(R.dimen.card_corner_radius), getContext());
    setRadius(cornerRadius);
  }

  private void setupLikeView() {
    likesCounter.setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_like));
    likesCounter.setListener(this);
  }

  private void bindViews() {
    profileLayout = (RelativeLayout) findViewById(R.id.component_community_card_profile_profile_layout);
    profileImage = (CircleImageView) findViewById(R.id.component_community_card_profile_image);
    profileName = (CustomTextView) findViewById(R.id.component_community_card_profile_name);
    date = (CustomTextView) findViewById(R.id.component_community_card_posting_time);
    heading = (CustomTextView) findViewById(R.id.component_community_card_body_heading);
    body = (CustomTextView) findViewById(R.id.component_community_card_body_body);
    bodyLayout = (LinearLayout) findViewById(R.id.component_social_placeholder_card_body_layout);
    likesCounter = (CounterWithDrawable) findViewById(R.id.component_social_placeholder_card_like);
    commentsCounter = (CustomTextView) findViewById(R.id.component_community_card_comments);
    contentRoot = (LinearLayout) findViewById(R.id.component_community_card_content_root);
    commentsCounterLayout = (RelativeLayout) findViewById(R.id.component_social_placeholder_card_comments_layout);
    likesLabel = (CustomTextView) findViewById(R.id.component_community_card_likes_label);
  }

  public void setProfileImage(String imageUrl) {
    if (imageUrl != null && !imageUrl.equals("")) {
      PicassoHelper.loadImageFromUrl(profileImage, imageUrl, getContext(), ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    } else {
      profileImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    }
  }

  private void setupButtons() {
    profileLayout.setOnClickListener(view -> {
      if (listener != null) {
        listener.onProfileRegionClick(userId, postId, type);
      }
    });
    contentRoot.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCardClick(postId, type);
      }
    });
    likesCounter.setOnClickListener(view -> {
      if (listener != null) {
        listener.onLikeClick(likesCounter.isLiked(), postId, type, likesCounter);
      }
    });
    commentsCounterLayout.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCommentsCounterClick(postId, type);
      }
    });
  }

  public void setProfileName(String profileName) {
    this.profileName.setText(profileName);
  }

  public void setDate(String date) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(date, getContext());
    DateFormat.getTimeFormat(getContext());
    this.date.setText(formattedDate);
  }

  public void setHeading(String heading) {
    this.heading.setText(heading);
  }

  public void setBody(String body) {
    this.body.setText(body);
  }

  public void setCommentsCounter(int commentsCounter) {
    this.commentsCounter.setText(getFormattedCounterText(commentsCounter));
  }

  private String getFormattedCounterText(int commentsCounter) {
    String commentsCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (commentsCounter == 0) {
      commentsCounterText = getResources().getString(R.string.component_community_card_no_comments);
    } else {
      commentsCounterText = getResources().getQuantityString(R.plurals.commentsCounter, commentsCounter, commentsCounter);
    }
    return commentsCounterText;
  }

  public void setPostId(Integer postId) { this.postId = postId; }

  public void setUserId(Integer userId) { this.userId = userId; }

  public void setLiked(boolean liked) {
    likesCounter.setLiked(liked);
  }

  public LinearLayout getContentRoot() {
    return contentRoot;
  }

  public RelativeLayout getProfileLayout() {
    return profileLayout;
  }

  public void modifyCommunityLikeCounter(boolean like) {
    likesCounter.setLiked(like);
  }

  public RelativeLayout getCommentsCounterLayout() {
    return commentsCounterLayout;
  }

  public Integer getViewId() {
    return postId;
  }


  @Override
  public void onCounterClick(boolean isCounterActivated, CounterWithDrawable counterWithDrawable) {
    if (listener != null) {
      listener.onLikeClick(isCounterActivated, postId, type, counterWithDrawable);
    }
  }

  public void setLikesCounter(int likesCounter) {
    this.likesCounter.setCounter(likesCounter);
    setupLikesLabel();
  }

  //Todo remove this variable after service provides a way to like and dislike
  public void setupLikesLabel() {
    int likesCount = likesCounter.getValueOfText();
    String likesLabelText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (likesCount == 0) {
      likesLabelText = getResources().getString(R.string.component_community_card_no_likes);
    } else {
      likesLabelText = getResources().getQuantityString(R.plurals.likesCounter, likesCount, likesCount);
    }
    likesLabel.setText(likesLabelText);
  }

  //TODO adapt this to reflect services later
  public interface CommunityCardListener {
    void onProfileRegionClick(Integer profileId, Integer postId, int type);

    void onCardClick(Integer id, int type);

    void onLikeClick(boolean isCounterActivated, Integer id, int type, CounterWithDrawable counterWithDrawable);

    void onCommentsCounterClick(Integer id, int type);
  }

  public void setListener(CommunityCardListener communityCardListener) {
    listener = communityCardListener;
  }

  public void updateCard(boolean liked, Integer likesCount, Integer commentCount){
    setLiked(liked);
    setLikesCounter(likesCount);
    setCommentsCounter(commentCount);
  }

}
