package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.cells.AppointmentFeedCell;

/**
 * Created by taqtile on 05/05/17.
 */

public class AppointmentFeedCellViewholder extends RecyclerView.ViewHolder {

  private AppointmentFeedCell appointmentFeedCell;

  public AppointmentFeedCellViewholder(View itemView) {
    super(itemView);
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.gravity = Gravity.RIGHT;
    itemView.setLayoutParams(layoutParams);

    this.appointmentFeedCell = (AppointmentFeedCell) itemView;
  }

  public AppointmentFeedCell getAppointmentFeedCell(){
    return appointmentFeedCell;
  }

}
