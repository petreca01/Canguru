package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;

/**
 * Created by taqtile on 5/14/17.
 */

public class ClinicalConditionItemViewModel implements ListingsViewModel, Parcelable {
  private Integer id;
  private Integer parentId;
  private String name;
  private String content;

  public ClinicalConditionItemViewModel(Integer id, Integer parentId, String name, String content) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
    this.content = content;
  }

  public Integer getId() {
    return id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public String getName() {
    return name;
  }

  public String getContent() {
    return content;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.id);
    dest.writeValue(this.parentId);
    dest.writeString(this.name);
    dest.writeString(this.content);
  }

  protected ClinicalConditionItemViewModel(Parcel in) {
    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    this.parentId = (Integer) in.readValue(Integer.class.getClassLoader());
    this.name = in.readString();
    this.content = in.readString();
  }

  public static final Creator<ClinicalConditionItemViewModel> CREATOR = new Creator<ClinicalConditionItemViewModel>() {
    @Override
    public ClinicalConditionItemViewModel createFromParcel(Parcel source) {
      return new ClinicalConditionItemViewModel(source);
    }

    @Override
    public ClinicalConditionItemViewModel[] newArray(int size) {
      return new ClinicalConditionItemViewModel[size];
    }
  };
}
