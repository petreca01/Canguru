package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.taqtile.android.app.listings.cells.CommunityCard;

/**
 * Created by taqtile on 3/20/17.
 */

public class SocialFeedCommunityCardViewHolder extends RecyclerView.ViewHolder {

    CommunityCard communityCard;

    public SocialFeedCommunityCardViewHolder(View itemView) {
        super(itemView);
        this.communityCard = (CommunityCard) itemView;
    }

    public CommunityCard getCommunityCard() {
        return communityCard;
    }
}
