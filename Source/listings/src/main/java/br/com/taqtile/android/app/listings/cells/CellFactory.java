package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;

/**
 * Created by taqtile on 27/04/17.
 */

public class CellFactory {

  public static ConnectedProfessionalsCell getConnectedProfessionalsCell(
    Context context, ConnectedProfessionalViewModel connectedProfessionalViewModel) {
    ConnectedProfessionalsCell connectedProfessionalsCell =
      new ConnectedProfessionalsCell(context);
    connectedProfessionalsCell.setupCell(connectedProfessionalViewModel);
    return connectedProfessionalsCell;
  }
}
