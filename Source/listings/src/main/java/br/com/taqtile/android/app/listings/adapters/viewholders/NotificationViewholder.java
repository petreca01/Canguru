package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.TextWithLabelAndDotCell;

/**
 * Created by taqtile on 15/05/17.
 */

public class NotificationViewholder extends RecyclerView.ViewHolder {

  TextWithLabelAndDotCell notification;

  public NotificationViewholder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.notification = (TextWithLabelAndDotCell) itemView;
  }

  public TextWithLabelAndDotCell getNotification(){
    return notification;
  }
}
