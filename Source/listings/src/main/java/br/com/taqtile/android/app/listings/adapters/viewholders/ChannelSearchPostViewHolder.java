package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ChannelCell;
import br.com.taqtile.android.app.listings.cells.ChannelSearchPostCell;

/**
 * Created by taqtile on 13/07/17.
 */

public class ChannelSearchPostViewHolder extends RecyclerView.ViewHolder {

  private ChannelSearchPostCell channelSearchPostCell;

  public ChannelSearchPostViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.channelSearchPostCell = (ChannelSearchPostCell) itemView;
  }

  public ChannelSearchPostCell getChannelCell() {
    return channelSearchPostCell;
  }

}
