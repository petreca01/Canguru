package br.com.taqtile.android.app.listings.cells;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.youtube.player.YouTubePlayerFragment;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.support.helpers.YoutubeHelper;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by taqtile on 13/04/17.
 */

public class PostCell extends FrameLayout implements CounterWithDrawable.CounterWithDrawableListener {

  private static final String YOUTUBE_REGEX_PATTERN = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|\u200C\u200B%2Fvideos%2F|embed%2\u200C\u200BF|youtu.be%2F|%2Fv%2\u200C\u200BF)[^#\\&\\?\\n]*";
  private LinearLayout profileLayout;
  private CircleImageView profileImage;
  private CustomTextView profileName;
  private CustomTextView date;
  private CustomTextView heading;
  private CustomTextViewWithHTML body;
  private LinearLayout bodyLayout;
  private CounterWithDrawable likesCounter;
  private CustomTextView commentsCounter;
  private LinearLayout contentRoot;
  private RelativeLayout commentsCounterLayout;
  private ImageView image;
  private CustomTextView likesLabel;
  private RelativeLayout likeLayout;
  private View bottomSeparator;
  private Integer id;
  private Integer profileId;
  private String videoUrl;
  private FrameLayout video;

  private OverflowButton overflowButton;

  private PostListener listener;

  public interface PostListener {
    void onProfileRegionClick(Integer profileId, Integer postId);

    void onCardClick(Integer postId);

    void onLikeClick(boolean isCounterActivated, Integer postId, CounterWithDrawable counterWithDrawable);

    void onCommentsCounterClick(Integer postId);

    void onOverflowMenuItemSelected(String item);

    void onBodyClick(Integer postId);
  }

  public PostCell(@NonNull Context context) {
    super(context);
    init();
  }

  public PostCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public PostCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_post, this, true);

    bindViews();
    setupView();
    setupButtons();
    setupLikeView();
  }

  private void bindViews() {
    profileLayout = (LinearLayout) findViewById(R.id.component_post_profile_profile_layout);
    profileImage = (CircleImageView) findViewById(R.id.component_post_profile_image);
    profileName = (CustomTextView) findViewById(R.id.component_post_profile_name);
    date = (CustomTextView) findViewById(R.id.component_post_posting_time);
    heading = (CustomTextView) findViewById(R.id.component_post_body_heading);
    body = (CustomTextViewWithHTML) findViewById(R.id.component_post_body_body);
    bodyLayout = (LinearLayout) findViewById(R.id.component_post_body_layout);
    likesCounter = (CounterWithDrawable) findViewById(R.id.component_post_like);
    commentsCounter = (CustomTextView) findViewById(R.id.component_post_comments);
    contentRoot = (LinearLayout) findViewById(R.id.component_post_content_root);
    commentsCounterLayout = (RelativeLayout) findViewById(R.id.component_post_comments_layout);
    image = (ImageView) findViewById(R.id.component_post_image);
    video = (FrameLayout) findViewById(R.id.component_post_video);
    likesLabel = (CustomTextView) findViewById(R.id.component_post_likes_label);
    likeLayout = (RelativeLayout) findViewById(R.id.component_post_like_layout);
    bottomSeparator = findViewById(R.id.component_post_bottom_separator);
    overflowButton = (OverflowButton) findViewById(R.id.componenent_post_overflow_button);
  }

  private void setupView() {
    image.setVisibility(GONE);
    likesLabel.setVisibility(GONE);
    overflowButton.setIconColor(R.color.color_gray_dark);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
  }

  private void setupButtons() {
    body.setOnClickListener(view -> {
      if (listener != null) {
        listener.onBodyClick(id);
      }
    });
    profileLayout.setOnClickListener(view -> {
      if (listener != null) {
        listener.onProfileRegionClick(profileId, id);
      }
    });
    contentRoot.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCardClick(id);
      }
    });
    commentsCounter.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCommentsCounterClick(id);
      }
    });
    likesCounter.setListener((boolean isCounterActivated, CounterWithDrawable counterWithDrawable) -> {
      if (listener != null) {
        listener.onLikeClick(isCounterActivated, id, counterWithDrawable);
      }
    });
    overflowButton.onMenuItemSelected(item -> {
      if (listener != null) {
        listener.onOverflowMenuItemSelected(item);
      }
    });
  }

  private void setupLikeView() {
    likesCounter.setIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_like));
    likesCounter.setListener(this);
  }

  @Override
  public void onCounterClick(boolean isCounterActivated, CounterWithDrawable counterWithDrawable) {
    if (listener != null) {
      listener.onLikeClick(isCounterActivated, id, counterWithDrawable);
    }
  }

  public void setProfileName(String profileName) {
    this.profileName.setText(profileName);
  }

  public void setProfileId(Integer profileId) {
    this.profileId = profileId;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setTextUnlimited() {
    body.setMaxLines(10000);
  }

  public void setOverflowMenuList(List<String> menuList) {
    if (menuList != null && !menuList.isEmpty()) {
      overflowButton.setVisibility(VISIBLE);
      overflowButton.setMenuList(menuList);
    }
  }

  //TODO change this later to the right format
  public void setDate(String date) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(date, getContext());
    DateFormat.getTimeFormat(getContext());
    this.date.setText(formattedDate);
  }

  public void setDate(String date, String posterName) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(date, getContext());
    DateFormat.getTimeFormat(getContext());
    this.date.setText(formatDateAndPoster(formattedDate, posterName));
  }

  private String formatDateAndPoster(String formattedDate, String posterName) {
    if (posterName == null || posterName.isEmpty()) {
      return formattedDate;
    } else {
      return getResources().getString(R.string.component_post_cell_date_poster_name_text,
        formattedDate, posterName);
    }
  }

  public void setProfileImage(String imageUrl) {
    if (!imageUrl.isEmpty()) {
      PicassoHelper.loadImageFromUrl(profileImage, imageUrl, getContext(),
        ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_placeholder));
    } else {
      profileImage.setImageDrawable(ContextCompat.getDrawable(getContext(),
        R.drawable.ic_profile_placeholder));
    }
  }

  public void setProfileImage(int drawableId) {
    profileImage.setImageDrawable(ContextCompat.getDrawable(getContext(), drawableId));
  }

  public void setHeading(String heading) {
    if (heading.isEmpty()) {
      this.heading.setVisibility(GONE);
    }
    this.heading.setText(heading);
  }

  public void setBody(String body) {
    this.body.setTextWithHTML(body);
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public void setVideo() {
    String sanitizedUrl = sanitizeUrl();
    if (isValidUrl(sanitizedUrl)) {
      final YouTubePlayerFragment youTubePlayerFragment = YouTubePlayerFragment.newInstance();
      ((Activity) this.getContext()).getFragmentManager().beginTransaction().replace(this.video.getId(), youTubePlayerFragment).commit();
      YoutubeHelper youtubeHelper = new YoutubeHelper(youTubePlayerFragment);
      youtubeHelper.initializeYoutube(sanitizedUrl, image, video);

      video.setVisibility(VISIBLE);
    } else {
      image.setVisibility(VISIBLE);
      video.setVisibility(GONE);
    }
  }

  private String sanitizeUrl() {
    if(this.videoUrl == null || this.videoUrl.isEmpty()) {
      return "";
    }
    String pattern = YOUTUBE_REGEX_PATTERN;

    Pattern compiledPattern = Pattern.compile(pattern);
    Matcher matcher = compiledPattern.matcher(this.videoUrl);

    if (matcher.find()) {
      return matcher.group();
    } else {
      return this.videoUrl;
    }
  }

  private boolean isValidUrl(String sanitizedUrl) {
    return sanitizedUrl != null && sanitizedUrl.replaceAll("\\s", "") != null &&
      sanitizedUrl.replaceAll("\\s", "").length() == 11;
  }

  public void setImage(String imageUrl) {
    if (imageUrl != null && !imageUrl.isEmpty()) {
      PicassoHelper.loadImageFromUrl(image, imageUrl, getContext());
      image.setVisibility(VISIBLE);
    } else {
      image.setVisibility(GONE);
    }
  }

  public void setCommentsCounter(int commentsCounter) {
    this.commentsCounter.setText(getFormattedCounterText(commentsCounter));
  }

  public void setLikesCounter(int likesCounter) {
    this.likesCounter.setCounter(likesCounter);
  }

  public void setLiked(boolean liked) {
    this.likesCounter.setLiked(liked);
  }

  public Integer getViewId() {
    return id;
  }

  private String getFormattedCounterText(int commentsCounter) {
    String commentsCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (commentsCounter == 0) {
      commentsCounterText = getResources().getString(R.string.component_community_card_no_comments);
    } else {
      commentsCounterText = getResources().getQuantityString(R.plurals.commentsCounter, commentsCounter, commentsCounter);
    }
    return commentsCounterText;
  }

  public void setupLikesLabel() {
    int likesCount = likesCounter.getValueOfText();
    String likesLabelText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (likesCount == 0) {
      likesLabelText = getResources().getString(R.string.component_community_card_no_likes);
    } else {
      likesLabelText = getResources().getQuantityString(R.plurals.likesCounter, likesCount, likesCount);
    }
    likesLabel.setText(likesLabelText);
    likesLabel.setVisibility(VISIBLE);
  }

  public void setSeparatorHidden(boolean hidden) {
    this.bottomSeparator.setVisibility(hidden ? GONE : VISIBLE);
  }

  public void setListener(PostListener postListener) {
    listener = postListener;
  }

  public RelativeLayout getCommentsCounterLayout() {
    return commentsCounterLayout;
  }

  public LinearLayout getProfileLayout() {
    return profileLayout;
  }

  public LinearLayout getContentRoot() {
    return contentRoot;
  }

  public CounterWithDrawable getLikesCounter() {
    return likesCounter;
  }

}
