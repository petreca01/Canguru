package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/18/17.
 */

public interface SocialFeedChannelViewModel extends SocialFeedViewModel {
    String getVideoUrl();
    void setImageUrl(String imageUrl);
    String getImageUrl();
    Integer getChannelId();
}
