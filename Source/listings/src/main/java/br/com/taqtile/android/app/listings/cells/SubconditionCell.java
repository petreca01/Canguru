package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/10/17.
 */

public class SubconditionCell extends LinearLayout {

  public CustomTextView subconditionText;
  public View bottomLine;
  private Listener listener;

  private FrameLayout clickableView;

  String subconditionTextAttr;
  boolean showBottomLineAttr;

  public SubconditionCell(Context context) {
    super(context);
    init();
  }

  public SubconditionCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public SubconditionCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.subcondition_cell, this, true);

    bindView();
    setupView();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextCell);

    subconditionTextAttr = a.getString(R.styleable.TextCell_leftText);
    showBottomLineAttr = a.getBoolean(R.styleable.TextCell_showBottomLine, true);

    a.recycle();
  }

  private void bindView() {
    subconditionText = (CustomTextView) findViewById(R.id.subcondition_cell_text);
    bottomLine = findViewById(R.id.subcondition_cell_bottom_line);
    clickableView = (FrameLayout) findViewById(R.id.subcondition_cell_clickable_view);
  }

  private void setupView() {
    setupCustomTextViews(subconditionText, subconditionTextAttr);

    if (!showBottomLineAttr) {
      bottomLine.setVisibility(GONE);
    }

    clickableView.setOnClickListener(this::onCellClick);
  }

  private void onCellClick(View view) {
    if (listener != null) {
      listener.onCellClick(view);
    }
  }

  private void setupCustomTextViews(CustomTextView textView, String attr) {
    if (attr != null) {
      textView.setText(attr);
    } else {
      textView.setVisibility(GONE);
    }
  }

  public interface Listener {
    void onCellClick(View view);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  public void setText(String text) {
    if (text != null && !text.isEmpty()) {
      this.subconditionText.setVisibility(VISIBLE);
      this.subconditionText.setText(text);
    }
  }

  public void setBottomLineVisibility(boolean shouldShowBottomLine) {
    bottomLine.setVisibility(shouldShowBottomLine ? VISIBLE : GONE);
  }

}
