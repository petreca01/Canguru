package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by taqtile on 26/04/17.
 */

public class ConnectedProfessionalViewModel implements Parcelable {

  private String name;
  private String authorizationDate;
  private Boolean isAuthorized;
  private String professionalId;

  public ConnectedProfessionalViewModel(String professionalId, String name,
    String authorizationDate, Boolean isAuthorized) {
    this.professionalId = professionalId;
    this.name = name;
    this.authorizationDate = authorizationDate;
    this.isAuthorized = isAuthorized;
  }

  public String getName() {
    return name;
  }

  public String getAuthorizationDate() {
    return authorizationDate;
  }

  public Boolean getAuthorized() {
    return isAuthorized;
  }

  public String getProfessionalId() {
    return professionalId;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.authorizationDate);
    dest.writeValue(this.isAuthorized);
    dest.writeString(this.professionalId);
  }

  protected ConnectedProfessionalViewModel(Parcel in) {
    this.name = in.readString();
    this.authorizationDate = in.readString();
    this.isAuthorized = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.professionalId = in.readString();
  }

  public static final Creator<ConnectedProfessionalViewModel> CREATOR =
    new Creator<ConnectedProfessionalViewModel>() {
      @Override public ConnectedProfessionalViewModel createFromParcel(Parcel source) {
        return new ConnectedProfessionalViewModel(source);
      }

      @Override public ConnectedProfessionalViewModel[] newArray(int size) {
        return new ConnectedProfessionalViewModel[size];
      }
    };
}
