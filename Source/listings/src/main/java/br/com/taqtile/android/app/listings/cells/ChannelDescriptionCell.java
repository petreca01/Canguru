package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 4/5/17.
 */

public class ChannelDescriptionCell extends FrameLayout {

  private CircleImageView image;
  private CustomTextView channelName;
  private TemplateButton followButton;
  private TemplateButton activeFollowButton;
  private CustomTextViewWithHTML channelDescription;
  private int type;

  private Listener listener;

  private boolean isButtonActive;

  public ChannelDescriptionCell(Context context) {
    super(context);
    init();
  }

  public ChannelDescriptionCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ChannelDescriptionCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_channel_description_cell, this, true);

    bindViews();

    setupView();

    setupButtons();

    toggleButtons(false);
  }

  private void setupButtons() {
    followButton.setOnClickListener(v -> {

      if (listener != null) {
        followButton.setEnabled(false);
        listener.onFollowButtonClick(type);
      }
    });
    activeFollowButton.setOnClickListener(v -> {

      if (listener != null) {
        activeFollowButton.setEnabled(false);
        listener.onActiveFollowButtonClick(type);
      }
    });
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setLayoutParams(layoutParams);
    setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
  }

  private void bindViews() {
    image = (CircleImageView) findViewById(R.id.component_channel_description_cell_image);
    channelName = (CustomTextView) findViewById(R.id.component_channel_description_cell_name);
    followButton = (TemplateButton) findViewById(R.id.component_channel_description_cell_follow_button);
    activeFollowButton = (TemplateButton) findViewById(R.id.component_channel_description_cell_following_button);
    channelDescription = (CustomTextViewWithHTML) findViewById(R.id.component_channel_description_cell_body);
  }

  public void toggleButtons(boolean isButtonActive) {
    setButtonActive(isButtonActive);
    if (this.isButtonActive) {
      followButton.setVisibility(GONE);
      activeFollowButton.setVisibility(VISIBLE);
      activeFollowButton.setEnabled(true);
    } else {
      activeFollowButton.setVisibility(GONE);
      followButton.setVisibility(VISIBLE);
      followButton.setEnabled(true);
    }
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getType() {
    return type;
  }

  private void setButtonActive(boolean isButtonActive) {
    this.isButtonActive = isButtonActive;
  }

  public boolean isButtonActive() {
    return isButtonActive;
  }

  public void setImage(String imageUrl) {
    PicassoHelper.loadImageFromUrl(image, imageUrl, getContext(), ContextCompat.getDrawable(getContext(), R.drawable.ic_channel_placeholder_large));
  }

  public void setChannelName(String channelName) {
    this.channelName.setText(channelName);
  }

  public void setChannelDescription(String channelDescription) {
    if (channelDescription != null) {
      this.channelDescription.setTextWithHTML(channelDescription);
    } else {
      this.channelDescription.setVisibility(GONE);
    }
  }

  public TemplateButton getFollowButton() {
    return followButton;
  }

  public TemplateButton getActiveFollowButton() {
    return activeFollowButton;
  }

  public interface Listener {
    void onFollowButtonClick(int type);

    void onActiveFollowButtonClick(int type);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }
}
