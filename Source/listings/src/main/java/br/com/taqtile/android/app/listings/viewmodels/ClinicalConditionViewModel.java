package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;

/**
 * Created by taqtile on 5/18/17.
 */

public class ClinicalConditionViewModel implements ListingsViewModel, Parcelable {

  private ClinicalConditionItemViewModel clinicalConditionItemViewModel;
  private List<ClinicalConditionItemViewModel> children;

  public ClinicalConditionViewModel(ClinicalConditionItemViewModel clinicalConditionItemViewModel,
                                    List<ClinicalConditionItemViewModel> children) {
    this.clinicalConditionItemViewModel = clinicalConditionItemViewModel;
    this.children = children;
  }

  public ClinicalConditionItemViewModel getClinicalConditionItemViewModel() {
    return clinicalConditionItemViewModel;
  }

  public List<ClinicalConditionItemViewModel> getChildren() {
    return children;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(this.clinicalConditionItemViewModel, flags);
    dest.writeList(this.children);
  }

  protected ClinicalConditionViewModel(Parcel in) {
    this.clinicalConditionItemViewModel = in.readParcelable(ClinicalConditionItemViewModel.class.getClassLoader());
    this.children = new ArrayList<ClinicalConditionItemViewModel>();
    in.readList(this.children, ClinicalConditionItemViewModel.class.getClassLoader());
  }

  public static final Creator<ClinicalConditionViewModel> CREATOR = new Creator<ClinicalConditionViewModel>() {
    @Override
    public ClinicalConditionViewModel createFromParcel(Parcel source) {
      return new ClinicalConditionViewModel(source);
    }

    @Override
    public ClinicalConditionViewModel[] newArray(int size) {
      return new ClinicalConditionViewModel[size];
    }
  };
}
