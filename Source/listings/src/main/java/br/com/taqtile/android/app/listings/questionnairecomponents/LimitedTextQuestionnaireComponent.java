package br.com.taqtile.android.app.listings.questionnairecomponents;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.edittexts.FreeTextLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.GenericTextLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.LimitedTextLabelEditTextCaption;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 19/05/17.
 */

public class LimitedTextQuestionnaireComponent extends LinearLayout implements QuestionnaireResult, TextWatcher {

  private LimitedTextLabelEditTextCaption textForm;
  private DescriptionHeaderWithImage questionDescription;
  private List<String> answer;
  private String questionId;
  private AnswerListener answerListener;

  public LimitedTextQuestionnaireComponent(Context context) {
    super(context);
  }

  public LimitedTextQuestionnaireComponent(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public LimitedTextQuestionnaireComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public LimitedTextQuestionnaireComponent(Context context, AnswerListener answerListener, QuestionViewModel questionViewModel) {
    super(context);
    this.answerListener = answerListener;
    this.questionId = String.valueOf(questionViewModel.getQuestionId());
    init(questionViewModel.getQuestion(), questionViewModel.getDescription(),
      questionViewModel.getAnswer());
  }

  private void init(String question, String questionDescription, String answer) {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_limited_text_questionnaire, this, true);

    bindViews();
    setupLimitedTextQuestionnaire(question, questionDescription);
    setupAnswer();
    setupForm();
    setAnswerIfAlreadyAnswered(answer);
  }

  private void bindViews() {
    textForm = (LimitedTextLabelEditTextCaption) findViewById(R.id.component_limited_text_questionnaire_form);
    questionDescription = (DescriptionHeaderWithImage) findViewById(R.id.component_limited_text_questionnaire_question_description);
  }

  private void setupLimitedTextQuestionnaire(String question, String questionDescription) {
    textForm.setInputPlaceholder(getResources().getString(R.string.component_free_text_questionnaire_form_input_placeholder));
    this.questionDescription.setTitle(question);
    this.questionDescription.setDescription(questionDescription);
  }

  private void setupAnswer() {
    answer = new ArrayList<>();
  }

  private void setupForm() {
    textForm.setTextChangedListener(this);
  }

  private void setAnswerIfAlreadyAnswered(String answer) {
    if (answer != null) {
      this.answer.add(answer);
      textForm.setText(answer);
    }
  }

  @Override
  public List<String> getResult() {
    return getText();
  }

  private List<String> getText() {
    return answer;
  }

  @Override
  public boolean isResultValid() {
    return true;
  }

  @Override
  public String getQuestionId() {
    return questionId;
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    if (answerListener != null) {
      answer.clear();
      answer.add(textForm.getText());
      answerListener.onAnswerSelected(this);
    }
  }

  @Override
  public void afterTextChanged(Editable s) {

  }

}

