package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.RatioImageView;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 18/04/17.
 */

public abstract class TextSubtextAndImageCell extends FrameLayout {

  protected RatioImageView ratioImage;
  private ImageView image;
  private CustomTextView header;
  private CustomTextView subheader;
  protected LinearLayout root;
  private  View separator;
  private RelativeLayout subheaderContainer;

  String partnerAttr;
  String headerAttr;
  String imageUrlAttr;
  int imageAttr;
  boolean showLineAttr = true;

  private HealthcareCellListener listener;

  public TextSubtextAndImageCell(@NonNull Context context) {
    super(context);
    init();
  }

  public TextSubtextAndImageCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public TextSubtextAndImageCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  public abstract boolean getSubheader();
  public abstract int getImageId();
  public abstract boolean showBiggerImage();

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_subtext_and_image_cell, this, true);

    bindViews();
    setupView();
    setupButtons();
    hideIfHasSubheader();
  }

  private void bindViews() {
    root = (LinearLayout) findViewById(R.id.component_text_subtext_and_image_cell_root);
    ratioImage = (RatioImageView) findViewById(R.id.component_text_subtext_and_image_cell_ratio_image);
    image = (ImageView) findViewById(R.id.component_text_subtext_and_image_cell_image);
    header = (CustomTextView) findViewById(R.id.component_text_subtext_and_image_cell_header);
    subheader = (CustomTextView) findViewById(R.id.component_text_subtext_and_image_cell_subheader);
    separator = findViewById(R.id.component_text_subtext_and_image_cell_separator);
    subheaderContainer = (RelativeLayout) findViewById(R.id.component_text_subtext_and_image_cell_subheading_layout);
  }

  private void setupView(){
    if(imageUrlAttr != null && imageAttr != 0) {
      ratioImage.setImageResource(imageAttr);
    } else {
      setImage(imageUrlAttr);
    }

    if(headerAttr != null){
      header.setText(headerAttr);
    }

    if(partnerAttr != null){
      subheader.setText(partnerAttr);
    }

    if(!showLineAttr){
      separator.setVisibility(GONE);
    }
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextSubtextAndImageCell);

    headerAttr = a.getString(R.styleable.TextSubtextAndImageCell_header);
    partnerAttr = a.getString(R.styleable.TextSubtextAndImageCell_partner);
    imageUrlAttr = a.getString(R.styleable.TextSubtextAndImageCell_imageUrl);
    imageAttr = a.getResourceId(R.styleable.TextSubtextAndImageCell_image, 0);
    showLineAttr = a.getBoolean(R.styleable.TextSubtextAndImageCell_showLine, true);
  }

  private void setupButtons() {
    root.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCellClick();
      }
    });
  }

  public void setImage(String imageUrl) {
    image.setVisibility(showBiggerImage() ? GONE : VISIBLE);
    ratioImage.setVisibility(showBiggerImage() ? VISIBLE : GONE);
    setupImageView(showBiggerImage() ? ratioImage : image, imageUrl);
  }

  private void setupImageView(ImageView imageView, String imageUrl) {
    if (imageUrl != null) {
      PicassoHelper.loadImageFromUrl(imageView, imageUrl, getContext(), ContextCompat.getDrawable(getContext(),
        getImageId()));
    } else {
      setImageFromDrawable(getImageId());
    }
  }

  public void setHeader(String header) {
    this.header.setText(header);
  }

  public void setSubheader(String subheader) {
    this.subheader.setText(subheader);
  }

  public void setBottomLineVisibility(boolean shouldShowVisibility) {
    separator.setVisibility(shouldShowVisibility ? VISIBLE : GONE);
  }

  public void setImageFromDrawable(int drawable) {
    (showBiggerImage() ? ratioImage : image).setImageDrawable(ContextCompat.getDrawable(getContext(), drawable));
  }

  public interface HealthcareCellListener {
    void onCellClick();
  }

  public void setListener(HealthcareCellListener healthcareCellListener) {
    listener = healthcareCellListener;
  }

  private void hideIfHasSubheader() {
    subheaderContainer.setVisibility(getSubheader() ? VISIBLE : GONE);
  }

}
