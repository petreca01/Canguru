package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.CommentCellViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.PostCellViewHolder;
import br.com.taqtile.android.app.listings.cells.CommentCell;
import br.com.taqtile.android.app.listings.cells.PostCell;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 12/04/17.
 */

public class PostDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  protected Context context;
  private List<ListingsViewModel> postsList;

  private static int POST_DETAIL = 0;
  private static int COMMENT = 1;

  private PostDetailListener listener;
  private Integer userId;
  private CommentCell.CommentCellListener commentCellListener;
  private PostCell.PostListener postListener;

  public interface PostDetailListener {
    void onProfileClicked(Integer profileId, Integer postId);
    void onItemLiked(Integer id, boolean isLikeActivated);
    void onPostNewComment(Integer postId, String message);
    void onDeletePost(Integer postId);
    void onReportPost(Integer postId);
    void onDeleteComment(Integer commentId);
  }

  public PostDetailAdapter(Context context, List<ListingsViewModel> postsList) {
    this.context = context;
    this.postsList = postsList;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    setupListeners();
    if (viewType == POST_DETAIL) {
      return new PostCellViewHolder(new PostCell(parent.getContext()));
    } else {
      return new CommentCellViewHolder(new CommentCell(parent.getContext()));
    }
  }

  private void setupListeners() {
    this.postListener = new PostCell.PostListener() {

      @Override
      public void onProfileRegionClick(Integer profileId, Integer postId) {
        if (listener != null) {
          listener.onProfileClicked(profileId, postId);
        }
      }

      @Override
      public void onCardClick(Integer id) {
      }

      @Override
      public void onLikeClick(boolean alreadyLiked, Integer id, CounterWithDrawable counterWithDrawable) {
        if (listener != null) {
          listener.onItemLiked(id, alreadyLiked);
        }
      }

      @Override
      public void onCommentsCounterClick(Integer id) {
      }

      @Override
      public void onOverflowMenuItemSelected(String item) {
        if (listener != null) {
          if (item.contains(context.getString(R.string.channel_post_adapter_delete)))
            listener.onDeletePost(getPostId());
          else if (item.contains(context.getString(R.string.channel_post_adapter_report)))
            listener.onReportPost(getPostId());
        }
      }

      @Override
      public void onBodyClick(Integer id) {
      }
    };

    this.commentCellListener = new CommentCell.CommentCellListener() {
      @Override
      public void onCellLongClick(Integer id) {
        if (listener != null) {
          listener.onDeleteComment(id);
        }
      }

      @Override
      public void onLikeClick(Integer id, boolean isLikeActivated) {
        if (listener != null) {
          listener.onItemLiked(id, isLikeActivated);
        }
      }

      @Override
      public void onProfileRegionClick(Integer commentId) {
        if (listener != null) {
          listener.onProfileClicked(getUserId(commentId), commentId);
        }
      }
    };
  }

  @Override
  public int getItemViewType(int position) {
    if (postsList == null || postsList.isEmpty()) {
      return -1;
    } else if (position == 0) {
      return POST_DETAIL;
    } else {
      return COMMENT;
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if (getItemViewType(position) == POST_DETAIL) {

      SocialFeedViewModel socialFeedImplViewModel = null;
      PostCellViewHolder postCellViewHolder = (PostCellViewHolder) holder;

      if (postsList.get(position) instanceof SocialFeedChannelImplViewModel) {

        socialFeedImplViewModel = (SocialFeedChannelImplViewModel) postsList.get(position);
        postCellViewHolder.getPostCell().setImage(((SocialFeedChannelImplViewModel) socialFeedImplViewModel).getImageUrl());
        postCellViewHolder.getPostCell().setProfileImage(R.drawable.ic_channel_placeholder);
        postCellViewHolder.getPostCell().setProfileId(((SocialFeedChannelImplViewModel) socialFeedImplViewModel).getChannelId());
        postCellViewHolder.getPostCell().setVideoUrl(((SocialFeedChannelImplViewModel) socialFeedImplViewModel).getVideoUrl());
        postCellViewHolder.getPostCell().setVideo();

      } else if (postsList.get(position) instanceof SocialFeedCommunityImplViewModel) {

        socialFeedImplViewModel = (SocialFeedCommunityImplViewModel) postsList.get(position);
        if (thereIsNoPicture((SocialFeedCommunityImplViewModel) socialFeedImplViewModel)) {
          postCellViewHolder.getPostCell().setProfileImage(R.drawable.ic_profile_placeholder);
        } else {
          postCellViewHolder.getPostCell().setProfileImage(((SocialFeedCommunityImplViewModel) socialFeedImplViewModel).getProfileUrl());
        }
      }

      if (socialFeedImplViewModel != null) {

        postCellViewHolder.getPostCell().setBody(socialFeedImplViewModel.getPostBody());
        postCellViewHolder.getPostCell().setHeading(socialFeedImplViewModel.getPostTitle());
        postCellViewHolder.getPostCell().setLikesCounter(socialFeedImplViewModel.getLikesCount());
        postCellViewHolder.getPostCell().setLiked(socialFeedImplViewModel.getLikeActivated());
        if (socialFeedImplViewModel instanceof SocialFeedChannelImplViewModel) {
          postCellViewHolder.getPostCell().setProfileName(((SocialFeedChannelImplViewModel) socialFeedImplViewModel).getChannelName());
          postCellViewHolder.getPostCell().setProfileId(((SocialFeedChannelImplViewModel) socialFeedImplViewModel).getChannelId());
          postCellViewHolder.getPostCell().setDate(socialFeedImplViewModel.getDate(), socialFeedImplViewModel.getPosterName());
        } else {
          postCellViewHolder.getPostCell().setProfileName(socialFeedImplViewModel.getPosterName());
          postCellViewHolder.getPostCell().setProfileId(((SocialFeedCommunityImplViewModel) socialFeedImplViewModel).getUserId());
          postCellViewHolder.getPostCell().setDate(socialFeedImplViewModel.getDate());
        }
        postCellViewHolder.getPostCell().setSeparatorHidden(false);
        postCellViewHolder.getPostCell().setTextUnlimited();
        postCellViewHolder.getPostCell().setId(socialFeedImplViewModel.getPostId());
        postCellViewHolder.getPostCell().setOverflowMenuList(getOverflowMenuList(socialFeedImplViewModel));
        postCellViewHolder.getPostCell().setListener(getPostListener());
      }

      postCellViewHolder.getPostCell().getLikesCounter().setVisibility(View.VISIBLE);

    } else if (getItemViewType(position) == COMMENT) {

      SocialFeedCommunityImplViewModel socialFeedChannelImplViewModel = (SocialFeedCommunityImplViewModel) postsList.get(position);

      CommentCellViewHolder commentCellViewHolder = (CommentCellViewHolder) holder;
      commentCellViewHolder.getCommentCell().setUserName(socialFeedChannelImplViewModel.getPosterName());
      commentCellViewHolder.getCommentCell().setBody(socialFeedChannelImplViewModel.getPostBody());
      commentCellViewHolder.getCommentCell().setDate(socialFeedChannelImplViewModel.getDate());
      commentCellViewHolder.getCommentCell().setIcon(socialFeedChannelImplViewModel.getProfileUrl());
      commentCellViewHolder.getCommentCell().setLikesCount(socialFeedChannelImplViewModel.getLikesCount());
      commentCellViewHolder.getCommentCell().setLikeActivated(socialFeedChannelImplViewModel.getLikeActivated());
      commentCellViewHolder.getCommentCell().setCommentId(socialFeedChannelImplViewModel.getPostId());
      commentCellViewHolder.getCommentCell().setUserCertified(socialFeedChannelImplViewModel.isCertifiedProfessional());
      commentCellViewHolder.getCommentCell().setCommentMine(isLoggedUserPost(socialFeedChannelImplViewModel));
      commentCellViewHolder.getCommentCell().setListener(getCommentCellListener());

      boolean isLastCommentCell = position == postsList.size() - 1;

      if (isLastCommentCell) {
        commentCellViewHolder.getCommentCell().hideSeparator();
      }
    }
  }

  private boolean thereIsNoPicture(SocialFeedCommunityImplViewModel socialFeedImplViewModel) {
    return socialFeedImplViewModel.getProfileUrl() == null ||
      socialFeedImplViewModel.getProfileUrl().isEmpty();
  }

  private PostCell.PostListener getPostListener() {
    return this.postListener;
  }

  private List<String> getOverflowMenuList(SocialFeedViewModel socialFeedImplViewModel) {
    ArrayList<String> menuList = new ArrayList<>();

    if (isCommunityPost(socialFeedImplViewModel) && isLoggedUserPost(socialFeedImplViewModel)) {
      menuList.add(context.getString(R.string.channel_post_adapter_delete));
    }
    if (isCommunityPost(socialFeedImplViewModel) && !isLoggedUserPost(socialFeedImplViewModel)) {
      menuList.add(context.getString(R.string.channel_post_adapter_report));
    }

    return menuList;
  }

  private boolean isLoggedUserPost(SocialFeedViewModel socialFeedImplViewModel) {
    return userId.equals(((SocialFeedCommunityImplViewModel) socialFeedImplViewModel).getUserId());
  }

  private boolean isCommunityPost(SocialFeedViewModel socialFeedImplViewModel) {
    return socialFeedImplViewModel instanceof SocialFeedCommunityImplViewModel;
  }

  private CommentCell.CommentCellListener getCommentCellListener() {
    return this.commentCellListener;
  }

  private Integer getPostId() {
    if (postsList != null && !postsList.isEmpty()) {
      return ((SocialFeedViewModel) postsList.get(0)).getPostId();
    } else {
      return null;
    }
  }

  private Integer getUserId(Integer commentId) {
    return StreamSupport.stream(postsList)
      .filter(post -> this.filterPost(post, commentId))
      .map(post -> ((SocialFeedCommunityImplViewModel) post).getUserId())
      .findFirst()
      .orElse(0);
  }

  private boolean filterPost(ListingsViewModel listingsViewModel, Integer commentId) {
    SocialFeedViewModel post = (SocialFeedViewModel) listingsViewModel;
    if (isCommunityPost(post)) {
      return post.getPostId() == commentId;
    } else {
      return post.getPostId() == commentId;
    }
  }

  @Override
  public int getItemCount() {
    return postsList.size();
  }

  public void setListener(PostDetailListener listener) {
    this.listener = listener;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }
}
