package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import br.com.taqtile.android.app.buttons.IconButtonWithLabel;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 20/04/17.
 */

public class ConnectedProfessionalsCell extends FrameLayout implements IconButtonWithLabel.Listener {

  public interface Listener {
    void onActiveButtonClick(String professionalId);
    void onInactiveButtonClick(String professionalId);


  }

  private String id;
  private CircleImageView image;
  private CustomTextView name;
  private CustomTextView CRMNumber;
  private CustomTextView authorizedLabel;
  private IconButtonWithLabel permissionButton;
  private View separator;
  private boolean isAuthorized;
  private Listener listener;

  public ConnectedProfessionalsCell(@NonNull Context context) {
    super(context);
    init();
  }

  public ConnectedProfessionalsCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ConnectedProfessionalsCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_connected_professionals_cell, this, true);

    bindViews();

    setupView();
  }

  private void bindViews(){
    name = (CustomTextView) findViewById(R.id.component_connected_professionals_cell_header);
    image = (CircleImageView) findViewById(R.id.component_connected_professionals_cell_image);
    CRMNumber = (CustomTextView) findViewById(R.id.component_connected_professionals_cell_crm_number);
    authorizedLabel = (CustomTextView) findViewById(R.id.component_connected_professionals_cell_authorized_label);
    permissionButton = (IconButtonWithLabel) findViewById(R.id.component_connected_professionals_cell_permission_button);
    separator = (View) findViewById(R.id.component_connected_professionals_cell_separator);
  }

  private void setupView(){
    setupLayoutParams();
    setupPermissionButton();
    isAuthorized = false;
  }

  private void setupLayoutParams(){
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setLayoutParams(layoutParams);
  }

  private void setupPermissionButton() {
    permissionButton.setActiveIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_check_button));
    permissionButton.setInactiveIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_check_button_disabled));
    permissionButton.setListener(this);
  }

  public void setViewId(String id){
    this.id = id;
  }

  public String getViewId(){
    return id;
  }

  public boolean isAuthorized(){
    return isAuthorized;
  }

  private void showAuthorizedLabel(){
    authorizedLabel.setVisibility(VISIBLE);
  }

  private void hideAuthorizedLabel(){
    authorizedLabel.setVisibility(GONE);
  }

  public void showAccessPermission(){
    permissionButton.setVisibility(VISIBLE);
    showSeparator();
  }

  public void hideAccessPermission(){
    permissionButton.setVisibility(GONE);
    hideSeparator();
  }

  public void showSeparator(){
    separator.setVisibility(VISIBLE);
  }

  public void hideSeparator(){
    separator.setVisibility(GONE);
  }

  public void setListener(Listener listener){
    this.listener = listener;
  }


  public void setupCell(ConnectedProfessionalViewModel connectedProfessionalViewModel) {
    setViewId(connectedProfessionalViewModel.getProfessionalId());
    if (connectedProfessionalViewModel.getAuthorized()) {
      setAuthorized(connectedProfessionalViewModel.getAuthorizationDate());
    } else {
      setUnauthorized();
    }
    setName(connectedProfessionalViewModel.getName());

    CRMNumber.setVisibility(GONE);
  }

  public boolean isIdEqualsTo(String id) {
    return this.id.equalsIgnoreCase(id);
  }

  public void rollbackButton() {
    permissionButton.setButtonActive(permissionButton.isButtonActive());
  }

  public void setAuthorized(String authorizationDate) {
    permissionButton.setLabel(getResources().getString(R.string.component_connected_professionals_cell_authorized_button_label));
    permissionButton.setButtonActive(true);
    setAuthorizedLabel(true, authorizationDate);
  }

  public void setUnauthorized() {
    permissionButton.setLabel(getResources().getString(R.string.component_connected_professionals_cell_unauthorized_button_label));
    permissionButton.setButtonActive(false);
    setAuthorizedLabel(false, null);
  }

  private void setAuthorizedLabel(boolean isAuthorized, String authorizedLabel){
    if (authorizedLabel != null) {
      authorizedLabel = formatDate(authorizedLabel);
    }

    this.isAuthorized = isAuthorized;
    if (isAuthorized) {
      this.authorizedLabel.setText(getResources().getString(R.string.component_connected_professionals_cell_authorized_access_text, authorizedLabel));
    } else {
      this.authorizedLabel.setText(getResources().getString(R.string.component_connected_professionals_cells_unauthorized_access_text));
    }
  }

  private void setImage(String imageUrl){
    if (!imageUrl.equals("")) {
      PicassoHelper.loadImageFromUrl(image, imageUrl, getContext());
    }
  }

  private void setName(String name){
    this.name.setText(name);
  }

  private String formatDate(String authorizationDate) {
    return DateFormatterHelper.formatDateWithHours(authorizationDate, getContext());
  }

  @Override
  public void onActiveButtonClick() {
    if (listener != null) {
      listener.onActiveButtonClick(id);
    }
  }

  @Override
  public void onInactiveButtonClick() {
    if (listener != null) {
      listener.onInactiveButtonClick(id);
    }

  }

}
