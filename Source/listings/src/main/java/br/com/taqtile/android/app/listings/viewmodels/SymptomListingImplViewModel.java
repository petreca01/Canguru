package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.SymptomListingViewModel;

/**
 * Created by taqtile on 3/23/17.
 */

public class SymptomListingImplViewModel implements SymptomListingViewModel {
  private String id;
  private String topicName;
  private String description;
  private int totalPosts;
  private int contentId;
  private int contentInParentId;

  public SymptomListingImplViewModel(String id, String topicName) {
    this.id = id;
    this.topicName = topicName;
    this.description = "";
  }

  public SymptomListingImplViewModel(String id, String topicName, String description, int totalPosts, int contentId, int contentInParentId) {
    this.id = id;
    this.topicName = topicName;
    this.description = description;
    this.totalPosts = totalPosts;
    this.contentInParentId = contentInParentId;
    this.contentId = contentId;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setTopicName(String topicName) {

    this.topicName = topicName;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setContentId(int contentId) {
    this.contentId = contentId;
  }

  @Override
  public int getContentId() {
    return contentId;
  }

  @Override
  public void setContentParentId(int contentParentId) {
    this.contentInParentId = contentParentId;
  }

  @Override
  public int getContentParentId() {
    return contentInParentId;
  }

  @Override
  public void setTotalPosts(int totalPosts) {
    this.totalPosts = totalPosts;
  }

  @Override
  public int getTotalPosts() {
    return totalPosts;
  }
}
