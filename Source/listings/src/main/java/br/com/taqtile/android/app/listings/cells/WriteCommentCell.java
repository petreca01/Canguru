package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;

/**
 * Created by taqtile on 17/04/17.
 */

public class WriteCommentCell extends FrameLayout {

  private CustomSimpleEditText comment;
  private FrameLayout send;

  private WriteCommentCellListener listener;

  public WriteCommentCell(@NonNull Context context) {
    super(context);
    init();
  }

  public WriteCommentCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public WriteCommentCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_write_comment_cell, this, true);

    bindViews();
    setupListeners();
  }

  private void bindViews() {
    comment = (CustomSimpleEditText) findViewById(R.id.component_write_comment_cell_edit_text);
    send = (FrameLayout) findViewById(R.id.component_write_comment_cell_image_button);

    comment.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
  }

  private void setupListeners() {
    send.setOnClickListener(view -> {
      if (listener != null) {
        listener.onSendClicked(comment.getText().toString());
      }
    });
  }

  public void resetCell() {
    comment.setText("");
  }

  public void requestEditTextFocus() {
    comment.requestFocus();
    SoftKeyboardHelper.showKeyboard(getContext());
  }

  public boolean isEditTextEmpty() {
    return comment.getText().toString().isEmpty();
  }

  public interface WriteCommentCellListener {
    void onSendClicked(String message);
  }

  public void setListener(WriteCommentCellListener listener) {
    this.listener = listener;
  }

  public void setSendEnable(boolean enabled) {
    send.setEnabled(enabled);
    comment.setEnabled(enabled);
  }
}
