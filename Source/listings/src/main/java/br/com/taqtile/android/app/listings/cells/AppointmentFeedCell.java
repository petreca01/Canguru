package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 04/05/17.
 */
public class AppointmentFeedCell extends RelativeLayout {
  public final static int EXAM_UNSCHEDULED = 0;
  public final static int EXAM_SCHEDULED = 1;
  public final static int EXAM_DONE = 2;
  private final static int EXAM_DONE_APPEARING_TIME = 3000;
  private final static int TUTORIAL_ANIMATION_DURATION = 1000;
  private View infoLayout;
  private LinearLayout examInformationContainer;
  private CustomTextView appointmentName;
  private CustomTextView appointmentDate;
  private FrameLayout examDoneLayout;
  private FrameLayout undoButtonContainer;
  private ImageView stateDrawable;
  private View clickableView;

  private OnLongClickListener clickListener;
  private int id;
  private boolean isCustomAppointment;
  private Listener listener;
  private Handler handlerForAnimationDelay;
  private int currentState;

  public AppointmentFeedCell(@NonNull Context context) {
    super(context);
    init();
  }

  public AppointmentFeedCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public AppointmentFeedCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_appointment_feed_cell, this, true);
    bindViews();
    setupView();
    setupButtons();
    clickListener = v -> {
      setupPopupMenu();
      return false;
    };
  }

  private void bindViews() {
    appointmentName = (CustomTextView) findViewById(R.id.component_appointment_feed_cell_appointment_name);
    appointmentDate = (CustomTextView) findViewById(R.id.component_appointment_feed_cell_appointment_date);
    examInformationContainer = (LinearLayout) findViewById(R.id.component_appointment_feed_cell_swipe_cell_container);
    examDoneLayout = (FrameLayout) findViewById(R.id.component_appointment_feed_cell_exam_done_container);
    undoButtonContainer = (FrameLayout) findViewById(R.id.component_appointment_feed_cell_undo_button_container);
    stateDrawable = (ImageView) findViewById(R.id.component_appointment_feed_cell_state_drawable);
    clickableView = findViewById(R.id.component_appointment_feed_cell_clickable_view);
    infoLayout = findViewById(R.id.component_appointment_feed_cell_appointment_info_layout);
  }

  private void setupButtons() {
    undoButtonContainer.setOnClickListener(v -> {
      if (listener != null) {
        setNormalCellState(true);
      }
    });
    clickableView.setOnLongClickListener(v -> {
      setupPopupMenu();
      return false;
    });
    clickableView.setOnClickListener(v -> {
      if (listener != null) {
        listener.onExamCellClick(id);
      }
    });
  }

  public void setNormalCellState(Boolean shouldUpdate) {
    handlerForAnimationDelay.removeCallbacksAndMessages(null);
    hideExamDoneContainer();
    if (shouldUpdate) {
      listener.onUndoButtonClick(id, this);
    }
  }

  private void setupPopupMenu() {
    if (listener != null) {
      PopupMenu popupMenu = new PopupMenu(getContext(), examInformationContainer);
      if (currentState != EXAM_DONE) {
        popupMenu.getMenu().add(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_menu_option));
      }
      if (isCustomAppointment) {
        popupMenu.getMenu().add(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_menu_delete_option));
      }
      popupMenu.setOnMenuItemClickListener(item -> {
        if (item.getTitle().equals(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_menu_option))) {
          this.triggerExamDoneAnimation(EXAM_DONE_APPEARING_TIME, true);
          listener.onFinishButtonClick(id, this);
        } else {
          listener.onCellDeleted(id, this);
        }
        return false;
      });
      popupMenu.show();
    }
  }

  public void triggerTutorialAnimation() {
    clickableView.setPressed(true);
    handlerForAnimationDelay.postDelayed(() -> {
      clickableView.setPressed(false);
      triggerExamDoneAnimation(TUTORIAL_ANIMATION_DURATION, false);
    }, TUTORIAL_ANIMATION_DURATION);
  }

  public void triggerExamDoneAnimation(int duration, boolean notifyListener) {
    FadeEffectHelper.fadeInView(examDoneLayout);
    clickableView.setVisibility(GONE);
    handlerForAnimationDelay.postDelayed(() -> {
      FadeEffectHelper.fadeOutView(examDoneLayout);
      if (notifyListener) {
        listener.onUndoExamStateExpired(id, this);
      }
      setCellState(EXAM_DONE, "");
    }, duration);
  }

  public void setViewId(int id) {
    this.id = id;
  }

  public int getViewId() {
    return id;
  }

  private void setupView() {
    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setLayoutParams(layoutParams);
    handlerForAnimationDelay = new Handler();
    this.currentState = EXAM_UNSCHEDULED;
  }

  public void setAppointmentName(String appointmentName) {
    this.appointmentName.setText(appointmentName);
  }

  public void setCustomAppointment(boolean customAppointment) {
    isCustomAppointment = customAppointment;
  }

  // TODO: 08/05/17 Change date formatting to the right one
  public void setCellState(int state, String appointmentDate) {
    switch (state) {
      case EXAM_UNSCHEDULED:
        stateDrawable.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_agenda_unscheduled_appointment));
        this.appointmentDate.setText(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_unscheduled_text));
        this.enableClickableView();
        clickableView.setOnLongClickListener(clickListener);
        this.currentState = EXAM_UNSCHEDULED;
        break;
      case EXAM_SCHEDULED:
        stateDrawable.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_agenda_scheduled_appointment));
        this.appointmentDate.setText(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_scheduled_text, appointmentDate));
        this.enableClickableView();
        this.currentState = EXAM_SCHEDULED;
        break;
      case EXAM_DONE:
        stateDrawable.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_agenda_check));
        this.appointmentDate.setText(getResources().getString(R.string.appointment_feed_cell_exam_information_cell_done_text));
        clickableView.setOnLongClickListener(clickListener);
        this.currentState = EXAM_DONE;
        break;
    }
  }

  public void showExamDoneContainer() {
    examDoneLayout.setVisibility(VISIBLE);
  }

  public void hideExamDoneContainer() {
    examDoneLayout.setVisibility(GONE);
  }

  public void enableClickableView() {
    clickableView.setVisibility(VISIBLE);
  }

  public void fadeOutCell() {
    FadeEffectHelper.fadeOutView(examDoneLayout);
  }

  public interface Listener {

    void onFinishButtonClick(int id, AppointmentFeedCell appointmentFeedCell);

    void onUndoButtonClick(int id, AppointmentFeedCell appointmentFeedCell);

    void onUndoExamStateExpired(int id, AppointmentFeedCell appointmentFeedCell);

    void onCellDeleted(int id, AppointmentFeedCell appointmentFeedCell);

    void onExamCellClick(int id);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  public View getInfoLayout() {
    return infoLayout;
  }
}
