package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;

import static br.com.taqtile.android.app.listings.Constants.CHANNEL_CELL;

/**
 * Created by taqtile on 3/16/17.
 */

public class ChannelCard extends CardView implements PostCell.PostListener {

  private PostCell postCell;
  private int type;

  public interface ChannelCardListener {
    void onProfileRegionClick(Integer profileId, Integer postId, int type);

    void onCardClick(Integer postId, int type);

//    void onCardClick(Integer postId, int type, CounterWithDrawable counterWithDrawable);

    void onLikeClick(boolean isCounterActivated, Integer postId, int type, CounterWithDrawable counterWithDrawable);

    void onCommentsCounterClick(Integer postId, int type);

    void onOverflowMenuItemSelected(String item);

    void onBodyClick(Integer postId, int type);
  }

  private ChannelCardListener listener;

  public ChannelCard(Context context) {
    super(context);
    init();
  }

  public ChannelCard(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ChannelCard(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_channel_card, this, true);

    type = CHANNEL_CELL;

    bindViews();
    setupView();
  }

  private void setupView() {
    setupLayoutParams();
    setupCornerRadius();
  }

  private void setupCornerRadius() {
    float cornerRadius = FloatToDpConverter.convertFromDpToFloat(getResources().getDimension(R.dimen.card_corner_radius), getContext());
    setRadius(cornerRadius);
  }

  private void setupLayoutParams() {
    FrameLayout.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    setClipChildren(false);
    setClipToPadding(false);
    setLayoutParams(layoutParams);
  }

  private void bindViews() {
    postCell = (PostCell) findViewById(R.id.component_channel_card_post_cell);
    postCell.setListener(this);
  }

  public void setProfileName(String profileName) {
    postCell.setProfileName(profileName);
  }

  public void setDate(String date) {
    postCell.setDate(date);
  }

  public void setDate(String date, String posterName) {
    postCell.setDate(date, posterName);
  }

  public void setHeading(String heading) {
    postCell.setHeading(heading);
  }

  public void setBody(String body) {
    postCell.setBody(body);
  }

  public void setImage(String imageUrl) {
    postCell.setImage(imageUrl);
  }

  public void setCommentsCounter(int commentsCounter) {
    postCell.setCommentsCounter(commentsCounter);
  }

  public void setLikesCounter(int likesCounter) {
    postCell.setLikesCounter(likesCounter);
    postCell.setupLikesLabel();
  }

  public void setLiked(boolean liked) {
    postCell.setLiked(liked);
  }

  public void setPostId(Integer postId) {
    postCell.setId(postId);
  }

  public PostCell getPostCell() {
    return postCell;
  }

  public void setChannelId(Integer channelId) { postCell.setProfileId(channelId); }

  public void modifyChannelLikeCounter(boolean like) {
    postCell.getLikesCounter().setLiked(like);
  }

  public void updateCard(boolean liked, Integer likesCount, Integer commentCount){
    setLiked(liked);
    setLikesCounter(likesCount);
    setCommentsCounter(commentCount);
  }

  @Override public void onProfileRegionClick(Integer profileId, Integer postId) {
    if (listener != null) {
      listener.onProfileRegionClick(profileId, postId, type);
    }
  }

  @Override
  public void onCardClick(Integer postId) {
    if (listener != null) {
      listener.onCardClick(postId, type);
    }
  }

  @Override
  public void onLikeClick(boolean isCounterActivated, Integer postId, CounterWithDrawable counterWithDrawable) {
    if (listener != null) {
      listener.onLikeClick(isCounterActivated, postId, type, counterWithDrawable);
    }
  }

  @Override
  public void onCommentsCounterClick(Integer postId) {
    if (listener != null) {
      listener.onCommentsCounterClick(postId, type);
    }
  }

  @Override
  public void onOverflowMenuItemSelected(String item) {
    if (listener != null) {
      listener.onOverflowMenuItemSelected(item);
    }
  }

  @Override
  public void onBodyClick(Integer postId) {
    if (listener != null) {
      listener.onBodyClick(postId, type);
    }
  }

  public void setListener(ChannelCardListener channelCardListener) {
    listener = channelCardListener;
  }

}
