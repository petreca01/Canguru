package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 5/23/17.
 */

public class VaccineCell extends LinearLayout {

  private CustomTextView cellName;
  private CustomTextView cellDate;

  public VaccineCell(Context context) {
    super(context);
    init();
  }

  public VaccineCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public VaccineCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void setupAttrs(AttributeSet attrs) {
    // TODO: 5/23/17 ADD STYLES
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.vaccine_cell, this, true);

    bindView();
  }

  private void bindView() {
    cellName = (CustomTextView) findViewById(R.id.vaccine_cell_vaccine_name);
    cellDate = (CustomTextView) findViewById(R.id.vaccine_cell_vaccine_date);
  }

  public void setCellName(String cellName) {
    this.cellName.setText(cellName);
  }

  public void setCellDate(String cellDate) {
    this.cellDate.setText(cellDate);
  }

}
