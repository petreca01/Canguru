package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 13/07/17.
 */

public interface ChannelSearchPostViewModel extends SearchListingViewModel, PaginationViewModel {
  void setLikesCounter(int likesCounter);

  int getLikesCounter();

  void setCommentsCounter(int followersCounter);

  int getCommentsCounter();

  void setDescription(String description);

  String getDescription();

  void setContentId(int contentId);

  int getContentId();

  void setContentParentId(int contentParentId);

  int getContentParentId();

}
