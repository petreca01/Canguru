package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 3/23/17.
 */

public class ChannelCell extends TextAndImageCell {

  public ChannelCell(Context context) {
    super(context);
  }

  public ChannelCell(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ChannelCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  public boolean isInstitutionNameVisible() {
    return true;
  }

  @Override
  protected String getFormattedLeftCounterText(int leftCounter) {
    String postsCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (leftCounter == 0) {
      postsCounterText = getResources().getString(R.string.component_channel_cell_no_posts);
    } else {
      postsCounterText = getResources().getQuantityString(R.plurals.postsCounter, leftCounter, leftCounter);
    }
    return postsCounterText;
  }

  @Override
  protected String getFormattedRightCounterText(int rightCounter) {
    String followersCounterText;
    //In portuguese language, the plural "zero" does not work due android implementation, so a 0 case was needed here
    if (rightCounter == 0) {
      followersCounterText = getResources().getString(R.string.component_channel_cell_no_followers);
    } else {
      followersCounterText = getResources().getQuantityString(R.plurals.followersCounter, rightCounter, rightCounter);
    }
    return followersCounterText;
  }

  @Override
  protected Drawable getIcon() {
    return ContextCompat.getDrawable(getContext(), R.drawable.ic_channel_listing_placeholder);
  }
}
