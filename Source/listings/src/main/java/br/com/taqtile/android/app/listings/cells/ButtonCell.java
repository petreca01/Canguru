package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 09/05/17.
 */

public class ButtonCell extends LinearLayout {

  private TemplateButton button;
  private ButtonCellListener listener;

  public ButtonCell(Context context) {
    super(context);
    init();
  }

  public ButtonCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ButtonCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_button_cell, this, true);

    bindView();
    setupView();
  }

  private void bindView() {
    button = (TemplateButton) findViewById(R.id.component_button_cell_button);
  }

  private void setupView() {
    button.setOnClickListener(view -> {
      if (listener != null) {
        listener.onButtonClicked(this);
      }
    });
  }

  public void setButtonText(String text) {
    button.setText(text);
  }

  public interface ButtonCellListener {
    void onButtonClicked(View view);
  }

  public void setListener(ButtonCellListener listener) {
    this.listener = listener;
  }
}
