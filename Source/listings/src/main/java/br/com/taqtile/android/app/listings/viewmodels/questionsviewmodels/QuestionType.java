package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

/**
 * Created by felipesabino on 5/22/17.
 */

public interface QuestionType {

  int QUESTION_TYPE_MULTIPLE  = 1;
  int QUESTION_TYPE_SELECT    = 2;
  int QUESTION_TYPE_RANGE     = 3;
  int QUESTION_TYPE_TEXT      = 4;
  int QUESTION_TYPE_BOOLEAN   = 5;
  int QUESTION_TYPE_IMC       = 6;
  int QUESTION_TYPE_VARCHAR   = 7;

  String QUESTION_BOOLEAN_VALUE = "1";

  int getType();
  int getQuestionId();
  String getDescription();
  String getQuestion();
}
