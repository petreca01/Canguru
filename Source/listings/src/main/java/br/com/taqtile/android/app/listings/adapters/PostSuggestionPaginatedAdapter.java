package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.adapters.viewholders.ChannelSearchPostViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.ForumViewHolder;
import br.com.taqtile.android.app.listings.adapters.viewholders.SymptomViewHolder;
import br.com.taqtile.android.app.listings.cells.ChannelSearchPostCell;
import br.com.taqtile.android.app.listings.cells.ForumCell;
import br.com.taqtile.android.app.listings.cells.SimpleTextImageCell;
import br.com.taqtile.android.app.listings.cells.TextAndImageCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelSearchPostViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumReplyListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SymptomListingViewModel;

/**
 * Created by taqtile on 03/07/17.
 */

public class PostSuggestionPaginatedAdapter extends RecyclerViewPaginatedAdapter {

  private List<SearchListingViewModel> postSuggestionsList;
  private TextAndImageCell.TextAndImageCellListener cellListener;

  public PostSuggestionPaginatedAdapter(Context context, List<SearchListingViewModel> postSuggestionsList, TextAndImageCell.TextAndImageCellListener cellListener) {
    super(context);
    this.postSuggestionsList = postSuggestionsList;
    this.cellListener = cellListener;
  }

  @Override
  public int getLoadingLayout() {
    return R.layout.component_social_placeholder_card;
  }

  @Override
  public RecyclerView.ViewHolder createAdditionalViewHolder(ViewGroup parent, int viewType) {
    switch (viewType) {
      case Constants.CHANNEL_LISTING_CELL:
        return new ChannelSearchPostViewHolder(new ChannelSearchPostCell(parent.getContext()));
      case Constants.FORUM_LISTING_CELL:
      case Constants.FORUM_REPLY_LISTING_CELL:
        return new ForumViewHolder(new ForumCell(parent.getContext()));
      case Constants.SYMPTOM_LISTING_CELL:
        return new SymptomViewHolder(new SimpleTextImageCell(parent.getContext()));
      default:
        return new SymptomViewHolder(new SimpleTextImageCell(parent.getContext()));
    }
  }

  @Override
  public void bindAdditionalViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case Constants.CHANNEL_LISTING_CELL:
        setupChannelCell((ChannelSearchPostViewHolder) holder, position);
        break;
      case Constants.FORUM_LISTING_CELL:
        setupForumCell((ForumViewHolder) holder, position);
        break;
      case Constants.SYMPTOM_LISTING_CELL:
        setupSymptomCell((SymptomViewHolder) holder, position);
        break;
      case Constants.FORUM_REPLY_LISTING_CELL:
        setupForumReplyCell((ForumViewHolder) holder, position);
        break;
      default:
        setupSymptomCell((SymptomViewHolder) holder, position);
        break;
    }
  }

  private void setupForumReplyCell(ForumViewHolder holder, int position) {
    ForumCell forumCell = holder.getForumCell();
    ForumReplyListingViewModel forumListingViewModel = (ForumReplyListingViewModel) postSuggestionsList.get(position);
    forumCell.setListener(cellListener);

    forumCell.setLeftCounter(forumListingViewModel.getLikesCounter());
    forumCell.setRightCounter(forumListingViewModel.getCommentsCounter());
    forumCell.setHeader(forumListingViewModel.getTopicName());
    forumCell.setCellId(forumListingViewModel.getId());
    forumCell.highlightText();
    forumCell.setDescription(forumListingViewModel.getDescription());
    forumCell.setContentId(forumListingViewModel.getContentId());
    forumCell.setContentParentId(forumListingViewModel.getContentParentId());
  }

  @Override
  public int getAdditionalViewTypes(int position) {
    SearchListingViewModel searchListingViewModel = postSuggestionsList.get(position);
    if (searchListingViewModel instanceof ForumListingViewModel) {
      return Constants.FORUM_LISTING_CELL;
    } else if (searchListingViewModel instanceof ChannelSearchPostViewModel) {
      return Constants.CHANNEL_LISTING_CELL;
    } else if (searchListingViewModel instanceof ForumReplyListingViewModel){
      return Constants.FORUM_REPLY_LISTING_CELL;
    } else {
      return Constants.SYMPTOM_LISTING_CELL;
    }
  }

  //TODO move to a binder later
  private void setupForumCell(ForumViewHolder holder, int position) {
    ForumCell forumCell = holder.getForumCell();
    ForumListingViewModel forumListingViewModel = (ForumListingViewModel) postSuggestionsList.get(position);
    forumCell.setListener(cellListener);

    forumCell.setLeftCounter(forumListingViewModel.getLikesCounter());
    forumCell.setRightCounter(forumListingViewModel.getCommentsCounter());
    forumCell.setHeader(forumListingViewModel.getTopicName());
    forumCell.setCellId(forumListingViewModel.getId());
    forumCell.highlightText();
    forumCell.setDescription(forumListingViewModel.getDescription());
    forumCell.setContentId(forumListingViewModel.getContentId());
    forumCell.setContentParentId(forumListingViewModel.getContentParentId());

  }

  private void setupChannelCell(ChannelSearchPostViewHolder holder, int position) {
    ChannelSearchPostCell channelSearchPostCell = holder.getChannelCell();
    ChannelSearchPostViewModel channelSearchPostViewModel = (ChannelSearchPostViewModel) postSuggestionsList.get(position);
    channelSearchPostCell.setListener(cellListener);

    channelSearchPostCell.setLeftCounter(channelSearchPostViewModel.getLikesCounter());
    channelSearchPostCell.setRightCounter(channelSearchPostViewModel.getCommentsCounter());
    channelSearchPostCell.setHeader(channelSearchPostViewModel.getTopicName());
    channelSearchPostCell.setCellId(channelSearchPostViewModel.getId());
    channelSearchPostCell.highlightText();
    channelSearchPostCell.setDescription(channelSearchPostViewModel.getDescription());
    channelSearchPostCell.setContentParentId(channelSearchPostViewModel.getContentParentId());
  }

  private void setupSymptomCell(SymptomViewHolder holder, int position) {
    SimpleTextImageCell symptomCell = holder.getSymptomCell();
    SymptomListingViewModel symptomListingViewModel = (SymptomListingViewModel) postSuggestionsList.get(position);
    symptomCell.setListener(cellListener);

    symptomCell.setHeader(symptomListingViewModel.getTopicName());
    symptomCell.setDescription(symptomListingViewModel.getDescription());
    symptomCell.setCellId(symptomListingViewModel.getId());
    symptomCell.setIcon(R.drawable.ic_symptom);
    symptomCell.highlightText();
    symptomCell.setContentId(symptomListingViewModel.getContentId());
    symptomCell.setContentParentId(symptomListingViewModel.getContentParentId());
  }

  @Override
  public int getAdditionalItemCount() {
    return postSuggestionsList.size();
  }
}
