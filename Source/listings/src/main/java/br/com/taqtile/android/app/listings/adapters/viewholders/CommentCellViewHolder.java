package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.CommentCell;

/**
 * Created by taqtile on 17/04/17.
 */

public class CommentCellViewHolder extends RecyclerView.ViewHolder {

  CommentCell commentCell;

  public CommentCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.commentCell = (CommentCell) itemView;
  }

  public CommentCell getCommentCell(){
    return commentCell;
  }
}
