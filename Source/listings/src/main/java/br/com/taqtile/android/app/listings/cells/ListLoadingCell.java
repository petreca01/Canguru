package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.listings.R;

/**
 * Created by taqtile on 06/06/17.
 */

public class ListLoadingCell extends FrameLayout {

  private ProgressBar progressBar;

  public ListLoadingCell(@NonNull Context context) {
    super(context);
    init();
  }

  public ListLoadingCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ListLoadingCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_list_loading_cell, this, true);

    bindViews();
    setupProgressBar();
  }

  private void bindViews() {
    progressBar = (ProgressBar) findViewById(R.id.component_list_loading_progress_bar);
  }

  private void setupProgressBar() {
    progressBar.getIndeterminateDrawable().setColorFilter(
      ContextCompat.getColor(getContext(), R.color.color_primary),
      android.graphics.PorterDuff.Mode.SRC_IN);
  }

}
