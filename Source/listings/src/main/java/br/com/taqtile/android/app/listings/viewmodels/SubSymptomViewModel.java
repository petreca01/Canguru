package br.com.taqtile.android.app.listings.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by taqtile on 5/9/17.
 */

public class SubSymptomViewModel implements Parcelable {

  private Integer id;
  private Integer parentId;
  private String name;
  private String explanation;
  private String howToAvoid;
  private String whenToWorry;
  private String date;

  public SubSymptomViewModel(Integer id, Integer parentId, String name, String explanation,
                             String howToAvoid, String whenToWorry) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
    this.explanation = explanation;
    this.howToAvoid = howToAvoid;
    this.whenToWorry = whenToWorry;
  }

  public Integer getId() {
    return id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public String getName() {
    return name;
  }

  public String getExplanation() {
    return explanation;
  }

  public String getHowToAvoid() {
    return howToAvoid;
  }

  public String getWhenToWorry() {
    return whenToWorry;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.id);
    dest.writeValue(this.parentId);
    dest.writeString(this.name);
    dest.writeString(this.explanation);
    dest.writeString(this.howToAvoid);
    dest.writeString(this.whenToWorry);
  }

  protected SubSymptomViewModel(Parcel in) {
    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    this.parentId = (Integer) in.readValue(Integer.class.getClassLoader());
    this.name = in.readString();
    this.explanation = in.readString();
    this.howToAvoid = in.readString();
    this.whenToWorry = in.readString();
  }

  public static final Creator<SubSymptomViewModel> CREATOR = new Creator<SubSymptomViewModel>() {
    @Override
    public SubSymptomViewModel createFromParcel(Parcel source) {
      return new SubSymptomViewModel(source);
    }

    @Override
    public SubSymptomViewModel[] newArray(int size) {
      return new SubSymptomViewModel[size];
    }
  };
}
