package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomsHeaderCell extends FrameLayout {

  private DescriptionHeaderWithImage descriptionHeaderWithImage;
  private SectionHeader sectionHeader;

  public SymptomsHeaderCell(@NonNull Context context) {
    super(context);
    init();
  }

  public SymptomsHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public SymptomsHeaderCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_symptoms_header_cell, this, true);

    bindViews();
  }

  private void bindViews() {
    descriptionHeaderWithImage = (DescriptionHeaderWithImage) findViewById(R.id.component_symptoms_header_description_header_with_image);
    sectionHeader = (SectionHeader) findViewById(R.id.component_symptoms_section_header);
  }

  public void setSectionHeaderTitle(String title) {
    sectionHeader.setText(title);
  }

  public void setDescriptionHeaderTitle(String title) {
    descriptionHeaderWithImage.setTitle(title);
  }

  public void setDescriptionHeaderDescription(String description) {
    descriptionHeaderWithImage.setDescription(description);
  }

  public void setDescriptionHeaderImage(int imageResourceId) {
    descriptionHeaderWithImage.setImage(imageResourceId);
  }
}
