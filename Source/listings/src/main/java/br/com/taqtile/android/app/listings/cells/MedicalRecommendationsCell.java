package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 04/05/17.
 */

public class MedicalRecommendationsCell extends LinearLayout {

  private CustomTextView recommendationTitle;
  private CustomTextView recommendationAuthor;
  private CustomTextView recommendationTime;
  private RelativeLayout clickableView;
  private View bottomSeparator;
  private ImageView notificationImage;
  private MedicalRecomendationsListener listener;

  private MedicalRecommendationViewModel medicalRecommendationViewModel;

  public MedicalRecommendationsCell(Context context) {
    super(context);
    init();
  }

  public MedicalRecommendationsCell(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public MedicalRecommendationsCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_medical_recommendations_cell, this, true);

    bindView();
    setupListener();
  }

  private void bindView() {
    recommendationTitle = (CustomTextView) findViewById(R.id.component_medical_reccommendations_cell_title);
    recommendationAuthor = (CustomTextView) findViewById(R.id.component_medical_reccommendations_cell_author);
    recommendationTime = (CustomTextView) findViewById(R.id.component_medical_reccommendations_cell_time);
    clickableView = (RelativeLayout) findViewById(R.id.component_medical_reccommendations_cell_clickable_view);
    bottomSeparator = findViewById(R.id.component_medical_reccommendations_cell_bottom_line);
    notificationImage = (ImageView) findViewById(R.id.component_medical_recommendations_cell_notification_image);
  }

  private void setupListener() {
    clickableView.setOnClickListener(v -> {
      if (listener != null) {
        listener.onClick(medicalRecommendationViewModel);
      }
    });
  }

  public void setupWithMedicalRecommendationViewModel(MedicalRecommendationViewModel medicalRecommendationViewModel, MedicalRecomendationsListener listener) {
    this.medicalRecommendationViewModel = medicalRecommendationViewModel;
    this.listener = listener;

    setRecommendationTitle(medicalRecommendationViewModel.getTitle());
    setRecommendationAuthor(medicalRecommendationViewModel.getDoctorName());
    setRecommendationRead(medicalRecommendationViewModel.isRead());
    setRecommendationTime(medicalRecommendationViewModel.getDate());
  }

  private void setRecommendationTitle(String title) {
    recommendationTitle.setText(title);
  }

  private void setRecommendationAuthor(String author) {
    recommendationAuthor.setText(author);
  }

  private void setRecommendationTime(String time) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(time, getContext());
    recommendationTime.setText(formattedDate);
  }

  private void setRecommendationRead(boolean read) {
    notificationImage.setVisibility(read ? GONE : VISIBLE);
    recommendationTitle.setNewTypeface(read ? "WorkSans-Regular.ttf" : "WorkSans-Medium.ttf");
  }

  public interface MedicalRecomendationsListener {
    void onClick(MedicalRecommendationViewModel medicalRecommendationViewModel);
  }
}
