package br.com.taqtile.android.app.listings;

/**
 * Created by taqtile on 11/8/16.
 */

public class Constants {
  //Home
  public final static int COMMUNITY_CELL = 1;
  public final static int CHANNEL_CELL = 2;

  //Channel
  public final static int CHANNEL_SECTION_HEADER = 0;
  public final static int CHANNEL_CELL_ = 1;

  //Search
  public final static int CHANNEL_LISTING_CELL = 1;
  public final static int FORUM_LISTING_CELL = 2;
  public final static int SYMPTOM_LISTING_CELL = 3;
  public final static int FORUM_REPLY_LISTING_CELL = 4;

  //Channel details
  public final static int CHANNEL_DETAILS_SECTION_SEPARATOR = 1;
  public final static int CHANNEL_DETAILS_CHANNEL_CARD = 2;
  public final static int CHANNEL_DETAILS_HEADER = 3;

  //Public profile
  public final static int PUBLIC_PROFILE_SECTION_SEPARATOR = 1;
  public final static int PUBLIC_PROFILE_SOCIAL_CARD = 2;
  public final static int PUBLIC_PROFILE_HEADER = 3;

  //Search healthcare
  public final static int HEALTHCARE_PARTNER = 0;
  public final static int HEALTHCARE_NOT_PARTNER = 1;

  //Symptoms guide
  public final static int SYMPTOMS_GUIDE_HEADER = 0;
  public final static int SYMPTOMS_GUIDE_SYMPTOM = 1;

  //Symptoms guide
  public final static int CONDITIONS_HEADER = 0;
  public final static int CONDITIONS_HISTORY_SECTION_HEADER = 1;
  public final static int CONDITIONS_HISTORY = 2;
  public final static int CONDITIONS_SECTION_HEADER = 3;
  public final static int CONDITION = 4;

  //Medical recommendations
  public final static int MEDICAL_RECOMMENDATIONS_HEADER = 0;
  public final static int MEDICAL_RECOMMENDATIONS_RECOMMENDATION = 1;

  //Symptoms history
  public final static int SYMPTOMS_HISTORY_HEADER = 0;
  public final static int SYMPTOMS_HISTORY_SYMPTOM = 1;
  public final static int SYMPTOMS_HISTORY_FOOTER = 2;

  //Maternity detail
  public final static int MATERNITY_DETAIL_HEADER = 0;
  public final static int MATERNITY_DETAIL_COMMENTS = 1;
  public final static int MATERNITY_DETAIL_NO_COMMENT_PLACEHOLDER = 2;

  //Google API KEY
  public final static String GOOGLE_API_KEY = "AIzaSyAAqBumXw2tP0JRQxAT4JJePvOKSleN25Q";

  //Maps
  public final static String MAPS_BASE_URL = "https://maps.googleapis.com/maps/api/staticmap";

}
