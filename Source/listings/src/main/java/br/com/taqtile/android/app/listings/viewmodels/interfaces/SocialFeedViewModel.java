package br.com.taqtile.android.app.listings.viewmodels.interfaces;

/**
 * Created by taqtile on 3/18/17.
 */

public interface SocialFeedViewModel extends ListingsViewModel {
    void setPostId(Integer id);
    Integer getPostId();
    void setPosterName(String posterName);
    String getPosterName();
    void setDate(String date);
    String getDate();
    long getDateInMilliseconds();
    void setPostTitle(String postTitle);
    String getPostTitle();
    void setPostBody(String body);
    String getPostBody();
    void setLikesCount(int likes);
    int getLikesCount();
    void setCommentsCount(int commentsCount);
    int getCommentsCount();
    void setLikeActivated(boolean isLikeActivated);
    boolean getLikeActivated();
    void setCertifiedProfessional(boolean isCertifiedProfessional);
    boolean isCertifiedProfessional();

}
