package br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels;

import android.os.Parcel;
import android.util.Log;

import java.util.List;

/**
 * Created by taqtile on 7/11/17.
 */

public class GestationConclusionQuestionViewModel extends QuestionViewModel {

  private static final String TAG = "GesConQuestionViewModel";

  public GestationConclusionQuestionViewModel(int questionId, int type, String question,
                                              String description, List<String> alternatives,
                                              List<String> answers, Integer min, Integer max,
                                              boolean hasNext, List<Integer> nextQuestionOptions) {
    super(questionId, type, question, description, alternatives, answers, min, max);
    this.hasNext = hasNext;
    this.nextQuestionOptions = nextQuestionOptions;
  }

  public GestationConclusionQuestionViewModel(int questionId, int type, String question,
                                              String description, List<String> alternatives,
                                              List<String> answers, boolean hasNext,
                                              List<Integer> nextQuestionOptions) {
    super(questionId, type, question, description, alternatives, answers);
    this.hasNext = hasNext;
    this.nextQuestionOptions = nextQuestionOptions;
  }

  public GestationConclusionQuestionViewModel(Parcel in, boolean hasNext, List<Integer> nextQuestionOptions) {
    super(in);
    this.hasNext = hasNext;
    this.nextQuestionOptions = nextQuestionOptions;
  }

  private boolean hasNext;
  private List<Integer> nextQuestionOptions;

  public boolean isHasNext() {
    return hasNext;
  }

  public List<Integer> getNextQuestionOptions() {
//    Log.e(TAG, "getNextQuestionOptions("+nextQuestionOptions+")");
    return nextQuestionOptions;
  }
}
