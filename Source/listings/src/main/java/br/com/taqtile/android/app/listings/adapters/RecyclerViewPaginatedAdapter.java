package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.adapters.viewholders.LoadingViewHolder;
import br.com.taqtile.android.app.listings.cells.ListLoadingCell;
import br.com.taqtile.android.app.listings.cells.SocialPlaceholderCard;

/**
 * Created by taqtile on 2/24/17.
 */

public abstract class RecyclerViewPaginatedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int FOOTER_LOADING = 0;

  protected Context context;
  private boolean isFooterButtonVisible = true;

  public RecyclerViewPaginatedAdapter(Context context) {
    this.context = context;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == FOOTER_LOADING) {
      return new LoadingViewHolder(new ListLoadingCell(parent.getContext()));
    } else {
      return createAdditionalViewHolder(parent, viewType);
    }
  }

  public abstract
  @LayoutRes
  int getLoadingLayout();

  public abstract RecyclerView.ViewHolder createAdditionalViewHolder(ViewGroup parent, int viewType);

  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if (holder.getItemViewType() == FOOTER_LOADING) {
      ((LoadingViewHolder) holder).getLayout();
      ((LoadingViewHolder) holder).getLayout();
    } else {
      bindAdditionalViewHolder(holder, position);
    }
  }

  public abstract void bindAdditionalViewHolder(RecyclerView.ViewHolder holder, int position);

  @Override
  public int getItemViewType(int position) {
    if (position == getItemCount() - 1 && isFooterButtonVisible()) {
      return FOOTER_LOADING;
    } else {
      return getAdditionalViewTypes(position);
    }
  }

  public abstract int getAdditionalViewTypes(int position);

  @Override
  public int getItemCount() {
    if (isFooterButtonVisible()) {
      return getAdditionalItemCount() + 1;
    } else {
      return getAdditionalItemCount();
    }
  }

  public abstract int getAdditionalItemCount();

  public boolean isFooterButtonVisible() {
    return isFooterButtonVisible;
  }

  public void setFooterButtonVisible(boolean footerButtonVisible) {
    isFooterButtonVisible = footerButtonVisible;
  }
}
