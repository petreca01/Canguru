package br.com.taqtile.android.app.listings.viewmodels;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ICarouselGridItemModel;

/**
 * Created by taqtile on 11/7/16.
 */

public class CarouselGridItemModel implements ICarouselGridItemModel {

    String mImageUrl;

    String mName;

    String mId;

    public CarouselGridItemModel(String mImageUrl, String mName, String mId) {
        this.mImageUrl = mImageUrl;
        this.mName = mName;
        this.mId = mId;
    }

    @Override
    public String getImageUrl() {
        return mImageUrl;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getId() {
        return mId;
    }
}
