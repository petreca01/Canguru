package br.com.taqtile.android.app.listings.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.listings.cells.ViewPagerCell;

/**
 * Created by taqtile on 10/19/16.
 */

public class ViewPagerAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;

    String[] mImageUrls;

    public ViewPagerAdapter(Context context, String[] imageUrls) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageUrls = imageUrls;
    }

    @Override
    public int getCount() {
        return mImageUrls.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewpager_item, container, false);

        ViewPagerCell viewPagerCell = (ViewPagerCell) itemView.findViewById(R.id.viewpager_item);

        viewPagerCell.setImage(mImageUrls[position]);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
