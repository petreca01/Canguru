package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.helpers.SkeletonLoadingGenerator;

/**
 * Created by taqtile on 3/31/17.
 */

public class SearchPlaceholderCell extends FrameLayout implements SkeletonLoadingGenerator.SkeletonLoadingWithShimmer{

  private FrameLayout layout;

  public static SearchPlaceholderCell newInstance(Context context){
    return new SearchPlaceholderCell(context);
  }

  public SearchPlaceholderCell(Context context) {
    super(context);
    init();
  }

  public SearchPlaceholderCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public SearchPlaceholderCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_search_placeholder_cell, this, true);

    bindViews();

    setupView();

  }

  private void bindViews(){
    layout = (FrameLayout) findViewById(R.id.component_search_placeholder_cell_shimmer_layout);
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setLayoutParams(layoutParams);
  }

  @Override
  public View instantiate() {
    return SearchPlaceholderCell.newInstance(getContext());
  }

}
