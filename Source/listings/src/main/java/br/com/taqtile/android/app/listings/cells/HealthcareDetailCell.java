package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.misc.utils.RatioImageView;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareDetailCell extends FrameLayout {

  private RatioImageView image;
  private CustomTextView header;
  private CustomTextViewWithHTML body;

  public HealthcareDetailCell(@NonNull Context context) {
    super(context);
    init();
  }

  public HealthcareDetailCell(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public HealthcareDetailCell(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_healthcare_detail_cell, this, true);

    bindViews();

    setupView();
  }

  private void bindViews() {
    image = (RatioImageView) findViewById(R.id.component_healthcare_detail_cell_image);
    header = (CustomTextView) findViewById(R.id.component_healthcare_detail_cell_header);
    body = (CustomTextViewWithHTML) findViewById(R.id.component_healthcare_detail_cell_body);
  }

  private void setupView() {
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    setLayoutParams(layoutParams);
  }

  public void setBody(String body) {
    this.body.setTextWithHTML(body);
  }

  public void setHeader(String header) {
    this.header.setText(header);
  }

  public void setImage(String imageUrl) {
    if (imageUrl != null) {
      PicassoHelper.loadImageFromUrlFitCenter(image, imageUrl, getContext(), ContextCompat.getDrawable(getContext(),
        R.drawable.ic_perfil_plano_de_saude_sintomas));
    } else {
      image.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_perfil_plano_de_saude_sintomas));
    }
  }

}
