package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ConditionCell;

/**
 * Created by taqtile on 5/10/17.
 */

public class ConditionsCellViewHolder extends RecyclerView.ViewHolder {

  ConditionCell conditionCell;

  public ConditionsCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.conditionCell = (ConditionCell) itemView;
  }

  public ConditionCell getConditionCell() {
    return conditionCell;
  }
}
