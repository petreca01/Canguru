package br.com.taqtile.android.app.listings.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.ChannelCell;

/**
 * Created by taqtile on 3/23/17.
 */

public class ChannelViewHolder extends RecyclerView.ViewHolder {

  ChannelCell channelCell;

  public ChannelViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.channelCell = (ChannelCell) itemView;
  }

  public ChannelCell getChannelCell() {
    return channelCell;
  }
}
