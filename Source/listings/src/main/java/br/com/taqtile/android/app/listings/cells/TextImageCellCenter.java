package br.com.taqtile.android.app.listings.cells;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.taqtile.android.app.listings.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/28/17.
 */

public class TextImageCellCenter extends LinearLayout {

  private ImageView icon;
  protected CustomTextView leftText;

  protected CustomTextView rightText;
  private View separator;
  private LinearLayout root;
  private FrameLayout imageContainer;

  private String leftTextAttr;
  private int iconAttr;
  private String rightTextAttr;
  private boolean showLineAttr = true;

  private TextImageCellCenterListener listener;

  public TextImageCellCenter(Context context) {
    super(context);
    init();
  }

  public TextImageCellCenter(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public TextImageCellCenter(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_image_cell_center, this, true);

    bindViews();
    setupButtons();
    setupViews();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextImageCellCenter);

    leftTextAttr = a.getString(R.styleable.TextImageCellCenter_leftTextCellCenter);
    rightTextAttr = a.getString(R.styleable.TextImageCellCenter_rightTextCellCenter);
    iconAttr = a.getResourceId(R.styleable.TextImageCellCenter_iconCellCenter, 0);
    showLineAttr = a.getBoolean(R.styleable.TextImageCellCenter_showLineCellCenter, true);

    a.recycle();
  }

  private void bindViews() {
    root = (LinearLayout) findViewById(R.id.component_text_image_cell_center_root);
    leftText = (CustomTextView) findViewById(R.id.component_text_image_cell_center_left_text);
    rightText = (CustomTextView) findViewById(R.id.fragment_text_image_cell_center_right_text);
    icon = (ImageView) findViewById(R.id.component_text_image_cell_center_image);
    separator = findViewById(R.id.component_text_image_cell_center_separator);
    imageContainer = (FrameLayout) findViewById(R.id.component_text_image_cell_center_image_container);
  }

  private void setupViews() {
    if (leftTextAttr != null) {
      this.leftText.setText(leftTextAttr);
    }

    if (rightTextAttr != null){
      setRightText(rightTextAttr);
    }

    if (iconAttr != 0) {
      icon.setImageResource(iconAttr);
    } else {
      imageContainer.setVisibility(INVISIBLE);
    }

    if (!showLineAttr) {
      separator.setVisibility(GONE);
    }

  }

  public void setRightText(String rightText){
    this.rightText.setVisibility(VISIBLE);
    this.rightText.setText(rightText);
  }

  private void setupButtons() {
    root.setOnClickListener(view -> {
      if (listener != null) {
        listener.onCellClick();
      }
    });
  }

  public interface TextImageCellCenterListener {
    void onCellClick();
  }

  public void setListener(TextImageCellCenterListener textImageCellCenterListener) {
    listener = textImageCellCenterListener;
  }

  public void setIcon(int image) {
    icon.setImageResource(image);
    imageContainer.setVisibility(View.VISIBLE);
  }

}
