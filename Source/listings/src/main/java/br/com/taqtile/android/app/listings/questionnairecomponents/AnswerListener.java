package br.com.taqtile.android.app.listings.questionnairecomponents;

/**
 * Created by taqtile on 24/05/17.
 */

public interface AnswerListener {
  void onAnswerSelected(QuestionnaireResult result);
}
