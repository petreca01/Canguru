package br.com.taqtile.android.app.support;

/**
 * Created by taqtile on 7/12/16.
 */

public class Constants implements BuildConstants {

  @Override
  public String getBaseUrl() {
    return "http://dsv.api.qeepme.com/";
  }

  @Override
  public String getGoogleAPIUrl() {
    return "https://maps.googleapis.com/";
  }

  @Override
  public String getGoogleAnalyticsId() {
    return "UA-99858161-1";
  }

  @Override
  public String getSenderId() {
    return "307523867781";
  }

}
