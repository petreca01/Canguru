package br.com.taqtile.android.app.support;

/**
 * Created by taqtile on 7/12/16.
 */

public class Constants implements BuildConstants {

  @Override
  public String getBaseUrl() {
    return "https://dsv.api.qeepme.com/";
  }

  @Override
  public String getGoogleAnalyticsId() {
    return "UA-104792502-1";
  }
}
