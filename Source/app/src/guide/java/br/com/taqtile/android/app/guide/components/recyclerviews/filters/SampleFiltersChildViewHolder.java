package br.com.taqtile.android.app.components.recyclerviews.filters;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import br.com.taqtile.android.recyclerviews.filtersrecyclerview.viewholder.FiltersChildViewHolder;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 6/7/16.
 */
public class SampleFiltersChildViewHolder extends FiltersChildViewHolder {

    private RadioButton mRadioView;

    private View mDividerView;

    private View mClickableView;

    private CustomTextView mLabelView;

    private CheckBox mCheckboxView;

    public SampleFiltersChildViewHolder(View itemView) {
        super(itemView);
        bindViews(itemView);
    }

    @Override
    public void bindViews(View root) {
        mRadioView = (RadioButton) root.findViewById(R.id.filters_child_radio);
        mDividerView = (View) root.findViewById(R.id.filters_child_divider);
        mClickableView = (View) root.findViewById(R.id.filters_child_clickable_view);
        mLabelView = (CustomTextView) root.findViewById(R.id.filters_child_label);
        mCheckboxView = (CheckBox) root.findViewById(R.id.filters_child_checkbox);
    }

    @Override
    public RadioButton getRadioButtonView() {
        return mRadioView;
    }

    @Override
    public View getDividerView() {
        return mDividerView;
    }

    @Override
    public View getClickableView() {
        return mClickableView;
    }

    @Override
    public TextView getLabelView() {
        return mLabelView;
    }

    @Override
    public CheckBox getCheckBoxView() {
        return mCheckboxView;
    }
}
