package br.com.taqtile.android.app.guide.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.guide.adapter.MainActivityAdapter;

/**
 * Created by taqtile on 10/4/16.
 */

public class MainGuideActivity extends AppCompatActivity{
    private ArrayList mArrayList = new ArrayList();

    private RecyclerView mRecyclerView;
    private MainActivityAdapter mRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setStyleGuidesOptions(mArrayList);

        mRecyclerAdapter = new MainActivityAdapter(this, mArrayList);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void setStyleGuidesOptions(ArrayList mArrayList) {
        Resources res = getResources();
        String[] styleGuideOptions = res.getStringArray(R.array.activity_main_items);
        Collections.addAll(mArrayList, styleGuideOptions);
    }

    public static void navigate(Context context) {
        Intent intent = new Intent(context, MainGuideActivity.class);
        context.startActivity(intent);
    }
}
