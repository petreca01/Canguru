package br.com.taqtile.android.app.guide.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import br.com.taqtile.android.app.R;

/**
 * Created by taqtile on 10/4/16.
 */

public class TypographyActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_typography);
    }
}
