package br.com.taqtile.android.app.components.recyclerviews.filters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.taqtile.android.recyclerviews.filtersrecyclerview.viewholder.FiltersParentViewHolder;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 6/7/16.
 */
public class SampleFiltersParentViewHolder extends FiltersParentViewHolder {

    private View mHeaderView;

    private CustomTextView mLabelView;

    private ImageView mArrowView;


    public SampleFiltersParentViewHolder(View itemView) {
        super(itemView);
        bindViews(itemView);
    }

    @Override
    public void bindViews(View root) {
        mHeaderView = (View) root.findViewById(R.id.filters_parent_header);
        mLabelView = (CustomTextView) root.findViewById(R.id.filters_parent_label);
        mArrowView = (ImageView) root.findViewById(R.id.filters_parent_arrow);
    }

    @Override
    public View getHeaderView() {
        return mHeaderView;
    }

    @Override
    public TextView getLabelView() {
        return mLabelView;
    }

    @Override
    public ImageView getArrowView() {
        return mArrowView;
    }

    @Override
    public int getArrowColorId() {
        return R.color.color_gray_dark;
    }

}
