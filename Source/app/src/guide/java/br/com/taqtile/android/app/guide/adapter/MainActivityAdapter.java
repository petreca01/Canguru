package br.com.taqtile.android.app.guide.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.guide.activities.ButtonsActivity;
import br.com.taqtile.android.app.guide.activities.ColorsActivity;
import br.com.taqtile.android.app.guide.activities.FormsActivity;
import br.com.taqtile.android.app.guide.activities.ListingsActivity;
import br.com.taqtile.android.app.guide.activities.OtherComponentsActivity;
import br.com.taqtile.android.app.guide.activities.ScreensActivity;
import br.com.taqtile.android.app.guide.activities.TypographyActivity;
import br.com.taqtile.android.app.guide.components.recycleritems.MainActivityRecyclerItem;

public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder>
        implements MainActivityRecyclerItem.SelectionItemListener {

    private ArrayList mArrayList;
    private Context mContext;

    public MainActivityAdapter(Context context, ArrayList<String> items) {
        mArrayList = items;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_main_activity_recycler_item, parent, false);
        ViewHolder vH = new ViewHolder(v);
        return vH;
    }

    @Override
    public void onBindViewHolder(MainActivityAdapter.ViewHolder holder, int position) {
        holder.mMainActivityRecyclerItem.setContent(mArrayList, position);
    }

    @Override
    public int getItemCount() {
        return (mArrayList != null ? mArrayList.size() : 0);
    }

    @Override
    public void onItemClick(View itemView, final int position) {
        switch (position) {
            case 0:
                mContext.startActivity(new Intent(mContext, ColorsActivity.class));
                break;
            case 1:
                mContext.startActivity(new Intent(mContext, TypographyActivity.class));
                break;
            case 2:
                mContext.startActivity(new Intent(mContext, ButtonsActivity.class));
                break;
            case 3:
                mContext.startActivity(new Intent(mContext, FormsActivity.class));
                break;
            case 4:
                mContext.startActivity(new Intent(mContext, ListingsActivity.class));
                break;
            case 5:
                mContext.startActivity(new Intent(mContext, ScreensActivity.class));
                break;
            case 6:
                mContext.startActivity(new Intent(mContext, OtherComponentsActivity.class));
                break;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MainActivityRecyclerItem mMainActivityRecyclerItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mMainActivityRecyclerItem = new MainActivityRecyclerItem(mContext, itemView);
            mMainActivityRecyclerItem.setListener(MainActivityAdapter.this);
        }
    }
}

