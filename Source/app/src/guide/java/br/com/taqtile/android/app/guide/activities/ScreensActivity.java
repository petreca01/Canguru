package br.com.taqtile.android.app.guide.activities;

//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
//import android.view.View;

//import br.com.taqtile.android.app.R;
//import br.com.taqtile.android.app.buttons.TemplateButton;
//import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
//import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
//import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
//import br.com.taqtile.android.app.presentation.about.AboutCanguruActivity;
//import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
//import br.com.taqtile.android.app.presentation.additionalinfoedit.AdditionalInfoEditActivity;
//import br.com.taqtile.android.app.presentation.appointmentdetail.AppointmentDetailActivity;
//import br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity;
//import br.com.taqtile.android.app.presentation.birthplan.BirthplanActivity;
//import br.com.taqtile.android.app.presentation.birthplanquestionnaire.BirthplanQuestionnaireActivity;
//import br.com.taqtile.android.app.presentation.changepassword.ChangePasswordActivity;
//import br.com.taqtile.android.app.presentation.channel.ChannelActivity;
//import br.com.taqtile.android.app.presentation.channeldetail.ChannelDetailsActivity;
//import br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity;
//import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
//import br.com.taqtile.android.app.presentation.communitypostdetail.CommunityPostDetailActivity;
//import br.com.taqtile.android.app.presentation.conditiondetails.ConditionDetailsActivity;
//import br.com.taqtile.android.app.presentation.conditions.ConditionsActivity;
//import br.com.taqtile.android.app.presentation.connectedprofessionals.ConnectedProfessionalsActivity;
//import br.com.taqtile.android.app.presentation.diary.DiaryActivity;
//import br.com.taqtile.android.app.presentation.forgotpassword.ForgotPasswordActivity;
//import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
//import br.com.taqtile.android.app.presentation.gestationbabydevelopmentdetails.GestationBabyDevelopmentDetailsActivity;
//import br.com.taqtile.android.app.presentation.gestationconclusion.GestationConclusionActivity;
//import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.GestationPregnancyInformationActivity;
//import br.com.taqtile.android.app.presentation.healthcaredetails.HealthcareDetailsActivity;
//import br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo.HealthcareUserAdditionalInfoActivity;
//import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginActivity;
//import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleCreator;
//import br.com.taqtile.android.app.presentation.main.MainActivity;
//import br.com.taqtile.android.app.presentation.manageprofessionals.ManageProfessionalsActivity;
//import br.com.taqtile.android.app.presentation.maternitycitysearch.MaternityCitySearchActivity;
//import br.com.taqtile.android.app.presentation.maternitydetail.MaternityDetailActivity;
//import br.com.taqtile.android.app.presentation.maternityrating.MaternityRatingActivity;
//import br.com.taqtile.android.app.presentation.medicalrecommendationdetail.MedicalRecommendationDetailActivity;
//import br.com.taqtile.android.app.presentation.medicalrecommendationdetail.MedicalRecommendationDetailBundleCreator;
//import br.com.taqtile.android.app.presentation.medicalrecommendations.MedicalRecommendationsActivity;
//import br.com.taqtile.android.app.presentation.personalrisk.SignInToSignUpActivity;
//import br.com.taqtile.android.app.presentation.personalriskform.PersonalRiskFormActivity;
//import br.com.taqtile.android.app.presentation.postcommunity.PostCommunityActivity;
//import br.com.taqtile.android.app.presentation.prenatalcard.PrenatalCardActivity;
//import br.com.taqtile.android.app.presentation.probablebirthdate.ProbableBirthdateActivity;
//import br.com.taqtile.android.app.presentation.profilemenu.ProfileMenuActivity;
//import br.com.taqtile.android.app.presentation.profilepublic.PublicProfileActivity;
//import br.com.taqtile.android.app.presentation.search.SearchActivity;
//import br.com.taqtile.android.app.presentation.searchhealthcare.SearchHealthcareActivity;
//import br.com.taqtile.android.app.presentation.signin.SignInActivity;
//import br.com.taqtile.android.app.presentation.signup.SignUpActivity;
//import br.com.taqtile.android.app.presentation.staticscreen.StaticScreenActivity;
//import br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailActivity;
//import br.com.taqtile.android.app.presentation.symptomsguide.SymptomsGuideActivity;
//import br.com.taqtile.android.app.presentation.symptomshistory.SymptomsHistoryActivity;
//import butterknife.BindView;
//import butterknife.ButterKnife;

import static br.com.taqtile.android.app.presentation.maternityrating.MaternityRatingActivity.MATERNITY_TAG;

/**
 * Created by taqtile on 10/6/16.
 */

public class ScreensActivity extends AppCompatActivity {

//  @BindView(R.id.activity_guide_screens_login)
//  TemplateButton loginButton;
//
//  @BindView(R.id.activity_guide_screens_forgot_password)
//  TemplateButton forgotPasswordButton;
//
//  @BindView(R.id.activity_guide_screens_signup)
//  TemplateButton signupButton;
//
//  @BindView(R.id.activity_guide_screens_initial_login)
//  TemplateButton initialLoginButton;
//
//  @BindView(R.id.activity_guide_screens_change_password)
//  TemplateButton changePasswordButton;
//
//  @BindView(R.id.activity_guide_screens_maternity_activity)
//  TemplateButton maternityActivityButton;
//
//  @BindView(R.id.activity_guide_screens_maternity_city_search)
//  TemplateButton maternityCitySearch;
//
//  @BindView(R.id.activity_guide_screens_main_activity)
//  TemplateButton mainActivityButton;
//
//  @BindView(R.id.activity_guide_screens_channels)
//  TemplateButton channelsButton;
//
//  @BindView(R.id.activity_guide_screens_tutorial)
//  TemplateButton tutorialButton;
//
//  @BindView(R.id.activity_guide_screens_search)
//  TemplateButton searchButton;
//
//  @BindView(R.id.activity_guide_screens_channel_detail)
//  TemplateButton channelDetailButton;
//
//  @BindView(R.id.activity_guide_screens_post_community)
//  TemplateButton postCommunityButton;
//
//  @BindView(R.id.activity_guide_screens_static_screen)
//  TemplateButton staticScreenButton;
//
//  @BindView(R.id.activity_guide_screens_channel_post_detail)
//  TemplateButton channelPostDetailButton;
//
//  @BindView(R.id.activity_guide_screens_search_healthcare)
//  TemplateButton searchHealthcareButton;
//
//  @BindView(R.id.activity_guide_screens_community_post_detail)
//  TemplateButton communityPostDetailButton;
//
//  @BindView(R.id.activity_guide_screens_personal_risk)
//  TemplateButton personalRiskButton;
//
//  @BindView(R.id.activity_guide_screens_profile_menu)
//  TemplateButton profileMenuButton;
//
//  @BindView(R.id.activity_guide_screens_healthcare_details)
//  TemplateButton healthcareDetailsButton;
//
//  @BindView(R.id.activity_guide_screens_healthcare_additional_info)
//  TemplateButton healthcareUserAdditionalInfoButton;
//
//  @BindView(R.id.activity_guide_screens_personal_risk_form)
//  TemplateButton personalRiskFormButton;
//
//  @BindView(R.id.activity_guide_screens_additinal_info)
//  TemplateButton additionalInfoButton;
//
//  @BindView(R.id.activity_guide_screens_additinal_info_edit)
//  TemplateButton additionalInfoEditButton;
//
//  @BindView(R.id.activity_guide_screens_public_profile)
//  TemplateButton publicProfileButton;
//
//  @BindView(R.id.activity_guide_screens_connected_professionals)
//  TemplateButton connectedProfessionalsButton;
//
//  @BindView(R.id.activity_guide_screens_manage_professionals)
//  TemplateButton manageProfessionalsButton;
//
//  @BindView(R.id.activity_guide_screens_appointment_detail)
//  TemplateButton appointmentDetailButton;
//
//  @BindView(R.id.activity_guide_screens_appointment_edit_schedule)
//  TemplateButton appointmentEditSchedule;
//
//  @BindView(R.id.activity_guide_screens_symptoms_guide)
//  TemplateButton symptomsGuideButton;
//
//  @BindView(R.id.activity_guide_screens_diary)
//  TemplateButton diaryButton;
//
//  @BindView(R.id.activity_guide_screens_medical_recommendations)
//  TemplateButton medicalRecommendationsButton;
//
//  @BindView(R.id.activity_guide_screens_medical_recommendation_detail)
//  TemplateButton medicalRecommendationDetailButton;
//
//  @BindView(R.id.activity_guide_screens_symptom_detail_history_symptom_detail)
//  TemplateButton symptomDetailHistorySymptomDetailButton;
//
//  @BindView(R.id.activity_guide_screens_conditions)
//  TemplateButton conditionsButton;
//
//  @BindView(R.id.activity_guide_screens_symptoms_history)
//  TemplateButton symptomsHistoryButton;
//
//  @BindView(R.id.activity_guide_screens_birthplan)
//  TemplateButton birthplanButton;
//
//  @BindView(R.id.activity_guide_screens_conditions_detail)
//  TemplateButton conditionsDetailButton;
//
//  @BindView(R.id.activity_guide_screens_maternity_detail)
//  TemplateButton maternityDetailButton;
//
//  @BindView(R.id.activity_guide_screens_maternity_rating)
//  TemplateButton maternityRatingButton;
//
//  @BindView(R.id.activity_guide_screens_gestation_baby_development_details)
//  TemplateButton gestationBabyDevelopmentDetailsButton;
//
//  @BindView(R.id.activity_guide_screens_gestation_pregnancy_information)
//  TemplateButton gestationPregnancyInformationButton;
//
//  @BindView(R.id.activity_guide_screens_birthplan_questionnaire)
//  TemplateButton birthplanQuestionnaireButton;
//
//  @BindView(R.id.activity_guide_screens_prenatal_card)
//  TemplateButton prenatalCardButton;
//
//  @BindView(R.id.activity_guide_screens_gestation_conclusion)
//  TemplateButton gestationConclusionButton;
//
//  @BindView(R.id.activity_guide_screens_probable_birthdate)
//  TemplateButton probableBirthdateButton;
//
//  @BindView(R.id.activity_guide_screens_about_canguru)
//  TemplateButton aboutCanguruButton;
//
//  @Override
//  protected void onCreate(@Nullable Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_guide_screens);
//    ButterKnife.bind(this);
//    setupButtons();
//  }

//  private void setupButtons() {
//    setupActivityButton(mainActivityButton, MainActivity.class);
//    setupActivityButton(loginButton, SignInActivity.class);
//    setupActivityButton(forgotPasswordButton, ForgotPasswordActivity.class);
//    signupButton.setOnClickListener(this::navigateToSignUpActivity);
//    setupActivityButton(initialLoginButton, InitialLoginActivity.class);
//    setupActivityButton(changePasswordButton, ChangePasswordActivity.class);
//    setupActivityButton(tutorialButton, TutorialActivity.class);
//    setupActivityButton(maternityActivityButton, MaternityActivity.class);
//    setupActivityButton(maternityCitySearch, MaternityCitySearchActivity.class);
//    setupActivityButton(channelsButton, ChannelActivity.class);
//    channelDetailButton.setOnClickListener(this::navigateToChannelDetail);
//    setupActivityButton(searchButton, SearchActivity.class);
//    setupActivityButton(postCommunityButton, PostCommunityActivity.class);
//    setupActivityButton(staticScreenButton, StaticScreenActivity.class);
//    channelPostDetailButton.setOnClickListener(this::navigateToChannelPostDetail);
//    setupActivityButton(searchHealthcareButton, SearchHealthcareActivity.class);
//    communityPostDetailButton.setOnClickListener(this::navigateToCommunityPostDetail);
//    setupActivityButton(personalRiskButton, SignInToSignUpActivity.class);
//    setupActivityButton(profileMenuButton, ProfileMenuActivity.class);
//    setupActivityButton(healthcareDetailsButton, HealthcareDetailsActivity.class);
//    setupActivityButton(healthcareUserAdditionalInfoButton, HealthcareUserAdditionalInfoActivity.class);
//    setupActivityButton(personalRiskFormButton, PersonalRiskFormActivity.class);
//    setupActivityButton(additionalInfoButton, AdditionalInfoActivity.class);
//    setupActivityButton(additionalInfoEditButton, AdditionalInfoEditActivity.class);
//    publicProfileButton.setOnClickListener(this::navigateToPublicProfile);
//    setupActivityButton(connectedProfessionalsButton, ConnectedProfessionalsActivity.class);
//    setupActivityButton(manageProfessionalsButton, ManageProfessionalsActivity.class);
//    appointmentDetailButton.setOnClickListener(this::navigateToAppointmentDetail);
//    setupActivityButton(appointmentEditSchedule, AppointmentEditScheduleActivity.class);
//    setupActivityButton(symptomsGuideButton, SymptomsGuideActivity.class);
//    setupActivityButton(diaryButton, DiaryActivity.class);
//    setupActivityButton(medicalRecommendationsButton, MedicalRecommendationsActivity.class);
//    medicalRecommendationDetailButton.setOnClickListener(this::navigateToMedicalRecommendationDetails);
//    setupActivityButton(symptomDetailHistorySymptomDetailButton, SymptomDetailActivity.class);
//    setupActivityButton(symptomsHistoryButton, SymptomsHistoryActivity.class);
//    setupActivityButton(birthplanButton, BirthplanActivity.class);
//    setupActivityButton(conditionsButton, ConditionsActivity.class);
//    setupActivityButton(conditionsDetailButton, ConditionDetailsActivity.class);
//    setupActivityButton(maternityDetailButton, MaternityDetailActivity.class);
//    setupMaternityRating(maternityRatingButton, MaternityRatingActivity.class);
//    setupActivityButton(gestationBabyDevelopmentDetailsButton, GestationBabyDevelopmentDetailsActivity.class);
//    gestationBabyDevelopmentDetailsButton.setOnClickListener(this::navigateToGestationBabyDevelopmentDetail);
//    setupActivityButton(gestationPregnancyInformationButton, GestationPregnancyInformationActivity.class);
//    setupActivityButton(birthplanButton, BirthplanActivity.class);
//    setupActivityButton(birthplanQuestionnaireButton, BirthplanQuestionnaireActivity.class);
//    setupActivityButton(prenatalCardButton, PrenatalCardActivity.class);
//    setupActivityButton(gestationConclusionButton, GestationConclusionActivity.class);
//    setupActivityButton(probableBirthdateButton, ProbableBirthdateActivity.class);
//    setupActivityButton(aboutCanguruButton, AboutCanguruActivity.class);
//  }
//
//  private void setupActivityButton(View button, final Class activity) {
//    button.setOnClickListener(v -> startActivity(new Intent(v.getContext(), activity)));
//  }
//
//  private void setupMaternityRating(View button, final Class activity) {
//    button.setOnClickListener(v -> {
//      Intent intent = new Intent(v.getContext(), activity);
//      intent.putExtra(MATERNITY_TAG,
//        new MaternityViewModel(
//          1,
//          "Minas",
//          "Piumhi",
//          "Santa casa",
//          "seila",
//          "Praça do parquinhi",
//          "3371-2313",
//          "jwoejfwe",
//          "image",
//           4.3f,
//          500,
//          "perform",
//          "source",
//          43.3f,
//          12.3f,
//          1,
//          2,
//          3,
//          4,
//          5,
//          12,
//          "",
//          "",
//          "",
//          "",
//          "",
//          "",
//          ""
//        ));
//      startActivity(intent);
//    });
//  }
//
//  private void navigateToSignUpActivity(View v) {
//    Bundle bundle = InitialLoginBundleCreator.getBundle("",
//      "", "");
//    SignUpActivity.navigate(v.getContext(), bundle);
//  }
//
//  private void navigateToChannelDetail(View v) {
//    ChannelDetailsActivity.navigate(v.getContext(), 6);
//  }
//
//  private void navigateToCommunityPostDetail(View v) {
//    CommunityPostDetailActivity.navigate(v.getContext(), null, 7750, false);
//  }
//
//  private void navigateToChannelPostDetail(View v) {
//    ChannelPostDetailActivity.navigate(v.getContext(), 57, true);
//  }
//
//  private void navigateToPublicProfile(View v) {
//    PublicProfileActivity.navigate(v.getContext(), 7974, null);
//  }
//
//  private void navigateToAppointmentDetail(View v) {
//    AgendaItemViewModel agenda = new AgendaItemViewModel(true, 1, true, 1, "Some","code", 1, "tupe", "fe",3,"dwdw","wddw", "", "", "", 0, "efe", "wd", null, "wdw", 0);
//    AppointmentDetailActivity.navigate(null, this, agenda);
//  }
//
//  private void navigateToMedicalRecommendationDetails(View v) {
//    MedicalRecommendationViewModel medicalRecommendation = new MedicalRecommendationViewModel("1", "Não beba!", "Dr. Obvío", "efe", "Não beba!", false);
//    MedicalRecommendationDetailActivity.navigate(this, MedicalRecommendationDetailBundleCreator.getBundle(medicalRecommendation));
//  }
//
//  private void navigateToGestationBabyDevelopmentDetail(View v) {
//    UserViewModel userViewModel =
//      new UserViewModel("some@email.com", "11 98569-2940", "21/05/1996", "somecpf", "zipcode", 1, "Unimed", "patient", 10, 5, 2, 1, "Manuela", "12", "11/12/2017", "", "", "", "", "", false);
//    WeeklyContentViewModel weeklyContentViewModel =
//      new WeeklyContentViewModel(1, "", "Lots of text about preganicy, baby education, health, what to eat and not eat and lots of Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
//    GestationBabyDevelopmentDetailsActivity.navigate(this, userViewModel, weeklyContentViewModel);
//  }
}

