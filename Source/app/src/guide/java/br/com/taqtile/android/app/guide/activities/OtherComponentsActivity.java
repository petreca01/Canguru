package br.com.taqtile.android.app.guide.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.ViewPagerAdapter;
import br.com.taqtile.android.app.listings.components.ProgressBarAndCaptionComponent;
import br.com.taqtile.android.app.listings.grids.CarouselGridWrapper;
import br.com.taqtile.android.app.listings.viewmodels.CarouselGridItemModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ICarouselGridItemModel;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityCell;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 10/14/16.
 */

public class OtherComponentsActivity extends AppCompatActivity {

  @BindView(R.id.activity_guide_other_components_content)
  LinearLayout mContent;

  private ViewPagerAdapter mViewPagerAdapter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_guide_other_components);
    ButterKnife.bind(this);
    setupViews();
  }

  //Region setup screen

  //This method is responsible for instantiating classes and adding a header to them
  private void setupViews() {
    ProgressBarAndCaptionComponent progressBarAndCaptionComponent = new ProgressBarAndCaptionComponent(this);
    progressBarAndCaptionComponent.setProgress(23);
    progressBarAndCaptionComponent.setProgressDrawable(R.drawable.bkg_progressbar_color_success);
    addViewToLayoutWithParams("ProgressBarAndCaptionComponent.java", progressBarAndCaptionComponent, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    MaternityCell maternityCell = new MaternityCell(this);
    addViewToLayout("MaternityCell.java", maternityCell);
    maternityCell.setData(getMockMaternityViewModel());

    ViewPager viewPager = new ViewPager(this);
    addViewToLayout("ViewPager.java", viewPager);
    CirclePageIndicator pageController = new CirclePageIndicator(this);
    addViewToLayout("", pageController);
    setupViewPager(viewPager, pageController);

    CarouselGridWrapper carouselGridWrapperContentState = new CarouselGridWrapper(this);
    addViewToLayout("CarouselGridWrapper.java - Content State", carouselGridWrapperContentState);
    setupCarouselGridWrapperContentState(carouselGridWrapperContentState);

    CarouselGridWrapper carouselGridWrapperErrorState = new CarouselGridWrapper(this);
    addViewToLayout("CarouselGridWrapper.java - Error State", carouselGridWrapperErrorState);
    CarouselGridWrapper carouselGridWrapperLoadingState = new CarouselGridWrapper(this);

    setupCarouselGridWrapperLoadingState(carouselGridWrapperLoadingState);
    addViewToLayout("CarouselGridWrapper.java - Loading State", carouselGridWrapperLoadingState);
    setupCarouselGridWrapperErrorState(carouselGridWrapperErrorState);
  }

  //This method is responsible for adding the instantiated views to the screen. It receives a text that will go
  //in the header and a view. Layout parameters are also set here
  private void addViewToLayout(String text, View view) {
    addViewToLayoutWithParams(text, view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
  }

  private void addViewToLayoutWithParams(String text, View view, int width, int heigth) {
    CustomTextView header = new CustomTextView(new ContextThemeWrapper(this, R.style.TextElement_H3));
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, heigth);
    if (text.isEmpty()) {
      header.setVisibility(View.GONE);
    }
    layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    layoutParams.topMargin = (int) getResources().getDimension(R.dimen.margin_medium);
    header.setText(text);
    mContent.addView(header, layoutParams);
    mContent.addView(view);
  }

  //end region

  //Region viewpager

  //This section concerns the configuration of the viewpager component and a page controller which
  //determines in which page the viewpager is currently at. Layout parameters are also set here
  private void setupViewPager(ViewPager viewPager, CirclePageIndicator pageController) {
    mViewPagerAdapter = new ViewPagerAdapter(this, getMockImageUrls());
    viewPager.setAdapter(mViewPagerAdapter);

    pageController.setViewPager(viewPager);
    pageController.setStrokeColor(ContextCompat.getColor(this, R.color.color_primary));
    pageController.setFillColor(ContextCompat.getColor(this, R.color.color_primary));

    LinearLayout.LayoutParams viewPagerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.viewpager_height));
    LinearLayout.LayoutParams pageControllerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    pageController.setPadding((int) getResources().getDimension(R.dimen.page_controller_padding), (int) getResources().getDimension(R.dimen.page_controller_padding), (int) getResources().getDimension(R.dimen.page_controller_padding), (int) getResources().getDimension(R.dimen.page_controller_padding));

    viewPager.setLayoutParams(viewPagerParams);
    pageController.setLayoutParams(pageControllerParams);
  }

  //end region

  //Carousel grid region

  //This region concerns methods related to the carousel grid, which is a horizontal recycler view
  //and also defines important states: default, loading and error

  //This method simulates the content (default) state
  private void setupCarouselGridWrapperContentState(final CarouselGridWrapper carouselGridWrapper) {
    carouselGridWrapper.setData(getMockCarouselItems());
  }

  //This method simulates the error state
  private void setupCarouselGridWrapperErrorState(final CarouselGridWrapper carouselGridWrapper) {
    carouselGridWrapper.setErrorState(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
    });

  }

  //This method simulates the loading state
  private void setupCarouselGridWrapperLoadingState(final CarouselGridWrapper carouselGridWrapper) {
    carouselGridWrapper.setLoadingState();
  }

  //end region

  private void setupMaternityCellContentState(final MaternityCell maternityCell) {
    maternityCell.setData(getMockMaternityViewModel());
  }

  private MaternityViewModel getMockMaternityViewModel() {
    return new MaternityViewModel(1, "São Paulo", "São Paulo", "Maternidade Santa Marcelina",
      "Seção Sobre a Maternidade", "Rua da Paz, 1242", "9999-9999", "http://url da imagem", "http://url do logo",
      2f, 20, "url mapa", "birth source", 2.2f, 2.2f, 1, 2, 3, 4, 5, 5, "", "", "", "", "", "", "");
  }
  //Carousel grid region

  //Mock region

  //This region is composed of methods used to simulate data that would be otherwise received from the
  //server

  private String[] getMockImageUrls() {
    return new String[]{
      null,
      null,
      null};
  }

  private ArrayList<ICarouselGridItemModel> getMockCarouselItems() {
    ArrayList<ICarouselGridItemModel> carouselGridItemModels = new ArrayList<>();
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));
    carouselGridItemModels.add(new CarouselGridItemModel(null, "Text", "1"));

    return carouselGridItemModels;
  }

  //end region
}
