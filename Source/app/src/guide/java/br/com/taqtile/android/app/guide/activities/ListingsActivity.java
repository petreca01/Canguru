package br.com.taqtile.android.app.guide.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.cells.ChannelCard;
import br.com.taqtile.android.app.listings.cells.CommunityCard;
import br.com.taqtile.android.app.listings.cells.ContentCard;
import br.com.taqtile.android.app.listings.cells.EmptyCard;
import br.com.taqtile.android.app.listings.cells.FormDetailCell;
import br.com.taqtile.android.app.listings.cells.HomeHeaderCell;
import br.com.taqtile.android.app.listings.cells.PostCell;
import br.com.taqtile.android.app.listings.cells.ProfileHeaderCell;
import br.com.taqtile.android.app.listings.cells.TextCell;
import br.com.taqtile.android.app.listings.cells.TextImageCell;
import br.com.taqtile.android.app.listings.cells.WriteCommentCell;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 10/6/16.
 */

public class ListingsActivity extends AppCompatActivity{

    @BindView(R.id.activity_guide_listings_layout)
    LinearLayout mListingsLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_listings);
        ButterKnife.bind(this);
        populateLayout();

    }

    private void populateLayout(){
      //To add a view to the layout, just copy and paste the method below, passing the correct arguments

      addViewToLayout("ContentCard.java", new ContentCard(this));
      addViewToLayout("EmptyCard.java", new EmptyCard(this));
      addViewToLayout("TextCell.java", new TextCell(this));
      addViewToLayout("TextImageCell.java", new TextImageCell(this));
      addViewToLayout("FormDetailCell.java", new FormDetailCell(this));

      HomeHeaderCell homeHeaderCell = new HomeHeaderCell(this);
      setupHomeHeaderCell(homeHeaderCell);
      addViewToLayout("HomeHeaderCell.java", homeHeaderCell);

      CommunityCard communityCard = new CommunityCard(this);
      setupCommunityCard(communityCard);
      addViewToLayout("CommunityCard.java", communityCard);

      ChannelCard channelCard = new ChannelCard(this);
      setupChannelCard(channelCard);
      addViewToLayout("ChannelCard.java", channelCard);

      PostCell postCell = new PostCell(this);
      setupPost(postCell);
      addViewToLayout("PostCell.java", postCell);

      addViewToLayout("WriteCommentCell.java", new WriteCommentCell(this));
      addViewToLayout("ProfileHeaderCell.java", new ProfileHeaderCell(this));

    }

    private void addViewToLayout(String text, View view){
        CustomTextView header = new CustomTextView(new ContextThemeWrapper(this, R.style.TextElement_H3));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (text.isEmpty()){
            header.setVisibility(View.GONE);
        }
        layoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        header.setText(text);
        mListingsLayout.addView(header, layoutParams);
        mListingsLayout.addView(view);

    }

    private void setupHomeHeaderCell(HomeHeaderCell homeHeaderCell){
        homeHeaderCell.setImage("https://upload.wikimedia.org/wikipedia/pt/5/52/Foucault5.jpg");
    }

    private void setupCommunityCard(CommunityCard communityCard){
        communityCard.setProfileImage("https://upload.wikimedia.org/wikipedia/pt/5/52/Foucault5.jpg");
        communityCard.setDate("2017-12-12 17:00:00");
        communityCard.setProfileName("Juliana da Silva");
        communityCard.setHeading("Gripe");
        communityCard.setBody("Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore…Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore…");
        communityCard.setCommentsCounter(1);
    }

    private void setupChannelCard(ChannelCard channelCard){
        channelCard.setDate("2017-12-12 17:00:00");
        channelCard.setProfileName("Juliana da Silva");
        channelCard.setHeading("Gripe");
        channelCard.setBody("Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore…Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore…");
        channelCard.setCommentsCounter(1);
        channelCard.setLikesCounter(200);
        channelCard.setImage("https://upload.wikimedia.org/wikipedia/pt/5/52/Foucault5.jpg");
    }

  private void setupPost(PostCell postCell){
    postCell.setDate("2017-12-12 17:00:00");
    postCell.setProfileName("Juliana da Silva");
    postCell.setHeading("Gripe");
    postCell.setBody("Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore. Lorem ipsum dolor sit amet, mei altera epicuri ei, vis te nihil bonorum philosophia, ceteros definitiones quo an. Id nec legere meliore.");
    postCell.setCommentsCounter(1);
    postCell.setLikesCounter(4);
  }

}
