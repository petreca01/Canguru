package br.com.taqtile.android.app.components.recyclerviews.filters;

import android.content.Context;
import android.view.View;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.List;

import br.com.taqtile.android.recyclerviews.filtersrecyclerview.adapter.FiltersRecyclerAdapter;
import br.com.taqtile.android.recyclerviews.filtersrecyclerview.model.FilterCategoryModel;
import br.com.taqtile.android.app.R;

/**
 * Created by taqtile on 6/7/16.
 */
public class SampleFiltersRecyclerAdapter extends FiltersRecyclerAdapter {

    public SampleFiltersRecyclerAdapter(Context context, List<FilterCategoryModel> filters) {
        super(context, filters);
    }

    public SampleFiltersRecyclerAdapter(Context context, List<FilterCategoryModel> filters, boolean showHeader) {
        super(context, filters, showHeader);
    }

    @Override
    public int getFilterTextFormatStringId() {
        return R.string.component_filter_recycler_format;
    }

    @Override
    public int getParentViewId() {
        return R.layout.component_filter_parent;
    }

    @Override
    public int getChildViewId() {
        return R.layout.component_filter_child;
    }

    @Override
    public ParentViewHolder getParentViewHolder(View view) {
        return new SampleFiltersParentViewHolder(view);
    }

    @Override
    public ChildViewHolder getChildViewHolder(View view) {
        return new SampleFiltersChildViewHolder(view);
    }

}
