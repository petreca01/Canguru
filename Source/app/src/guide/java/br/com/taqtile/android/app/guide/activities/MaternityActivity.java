package br.com.taqtile.android.app.guide.activities;

/**
 * Created by taqtile on 5/9/17.
 */

import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.maternity.MaternityFragment;
import br.com.taqtile.android.app.presentation.maternity.MaternityPresenter;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

public class MaternityActivity extends TemplateBackActivity {

  private MaternityFragment fragment;
  private MaternityPresenter presenter;

  @Override
  public Fragment getFragment() {
    MaternityFragment fragment = (MaternityFragment) getSupportFragmentManager().findFragmentById(this.getFragmentContainerId());
    if (fragment == null) {
      fragment = MaternityFragment.newInstance();
    }
    this.fragment = fragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(getMaternityPresenter());
    return presenter;
  }

  private MaternityPresenter getMaternityPresenter() {
    if (presenter == null) {
      presenter = new MaternityPresenter();
    }
    return presenter;
  }
}
