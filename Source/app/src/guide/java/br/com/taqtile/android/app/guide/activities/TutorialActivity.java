package br.com.taqtile.android.app.guide.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.tutorial.TutorialComponent;
import br.com.taqtile.android.app.presentation.tutorial.TutorialViewPagerCellModel;

/**
 * Created by taqtile on 5/16/17.
 */

public class TutorialActivity extends AppCompatActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tutorial);
  }


  @Override
  protected void onResume() {
    super.onResume();
    ViewGroup parent = (ViewGroup) this.findViewById(android.R.id.content);
    int parentIndex = parent.getChildCount();
    TutorialComponent tutorialComponent = new TutorialComponent(this, getTutorialContent());
    parent.addView(tutorialComponent, parentIndex);
  }

  private List<TutorialViewPagerCellModel> getTutorialContent() {
    return Arrays.asList(getTutorialInitialContent(), getTutorialHomeContent(), getTutorialAgendaContent());
  }

  private TutorialViewPagerCellModel getTutorialInitialContent() {
    Resources resources = getResources();
    return new TutorialViewPagerCellModel(R.drawable.ic_tutorial_inital,
      resources.getString(R.string.tutorial_initial_title),
      resources.getString(R.string.tutorial_initial_body));
  }

  private TutorialViewPagerCellModel getTutorialHomeContent() {
    Resources resources = getResources();
    return new TutorialViewPagerCellModel(R.drawable.ic_tutorial_home,
      resources.getString(R.string.tutorial_home_title),
      resources.getString(R.string.tutorial_home_body));
  }

  private TutorialViewPagerCellModel getTutorialAgendaContent() {
    Resources resources = getResources();
    return new TutorialViewPagerCellModel(R.drawable.ic_tutorial_agenda,
      resources.getString(R.string.tutorial_agenda_title),
      resources.getString(R.string.tutorial_agenda_body));
  }
}

