package br.com.taqtile.android.app.guide.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import br.com.taqtile.android.edittexts.components.ConfirmPasswordLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.PasswordLabelEditTextCaption;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.edittexts.validatablehelpers.FormComparisonValidatable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 10/4/16.
 */

public class FormsActivity extends AppCompatActivity {

    @BindView(R.id.activity_guide_forms_password)
    PasswordLabelEditTextCaption passwordForm;

    @BindView(R.id.activity_guide_forms_confirm_password)
    ConfirmPasswordLabelEditTextCaption confirmPasswordForm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_forms);
        ButterKnife.bind(this);
        setupForms();
    }

    private void setupForms(){
        //If you want to compare two password, pass them in the PasswordComparationValidatable constructor
        final FormComparisonValidatable passwordComparationValidatable = new FormComparisonValidatable(passwordForm, confirmPasswordForm);
    }

}
