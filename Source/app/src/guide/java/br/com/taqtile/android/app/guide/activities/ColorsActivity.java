package br.com.taqtile.android.app.guide.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 10/4/16.
 */

public class ColorsActivity extends AppCompatActivity {

    @BindView(R.id.activity_guide_colors_layout)
    LinearLayout mColorsLayout;

    private LinearLayout.LayoutParams mHeaderLayoutParams;

    private LinearLayout.LayoutParams mColorsLayoutParams;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_colors);
        ButterKnife.bind(this);
        init();
        populateWithColors();
    }

    private void init(){
        mHeaderLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mColorsLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.margin_medium));

    }

    private void populateWithColors(){
        setColorHeaderLayoutParams();
        setColorLayoutParams();

        ArrayList<Integer> colorsArray = new ArrayList<>();

            for (int color : getResources().getIntArray(R.array.guide_colors)) {
                colorsArray.add(color);
            }

        //INSTRUCTIONS

        //This array is composed of the strings defined in the array activity_guide_colors_subheaders
        // in the strings.xml file; if you need to add another color, insert the color name in that array,
        //after you have added a color to colors.xml file
        String[] headersArray = getResources().getStringArray(R.array.activity_guide_colors_subheaders);

        for(int i = 0; i < colorsArray.size(); i++){
            CustomTextView header = new CustomTextView(new ContextThemeWrapper(this, R.style.TextElement_H3));
            ImageView color = new ImageView(this);
            header.setText(headersArray[i]);
            color.setBackgroundColor(colorsArray.get(i));
            mColorsLayout.addView(header, mHeaderLayoutParams);
            mColorsLayout.addView(color, mColorsLayoutParams);
        }
    }

    private void setColorHeaderLayoutParams(){
        mHeaderLayoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        mHeaderLayoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        mHeaderLayoutParams.topMargin = (int) getResources().getDimension(R.dimen.margin_small);
        mHeaderLayoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_extra_small);
    }

    private void setColorLayoutParams(){
        mColorsLayoutParams.leftMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        mColorsLayoutParams.rightMargin = (int) getResources().getDimension(R.dimen.margin_medium);
        mColorsLayoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.margin_small);

    }
}
