package br.com.taqtile.android.app.domain.profile;

import br.com.taqtile.android.app.data.profile.ProfileRepository;
import br.com.taqtile.android.app.data.profile.remote.mappers.ProfileDetailsAndPostsToPublicProfileResultMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.profile.models.UserProfileParams;
import br.com.taqtile.android.app.domain.profile.mapper.PublicProfileResultToViewModelMapper;
import br.com.taqtile.android.app.presentation.profilepublic.viewmodel.PublicProfileViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class PublicProfileUserDataUseCase extends BaseUseCase<PublicProfileViewModel, UserProfileParams,
  ProfileRepository> {

  ProfileRepository profileRepository;

  public PublicProfileUserDataUseCase(ProfileRepository profileRepository) {
    super(profileRepository);
    this.profileRepository = profileRepository;
  }

  @Override
  public Observable<PublicProfileViewModel> execute(UserProfileParams userProfileParams) {
    return Observable.zip(getRepository().getProfileDetails(userProfileParams),
      getRepository().getPosts(userProfileParams),
      ProfileDetailsAndPostsToPublicProfileResultMapper::perform)
      .map(PublicProfileResultToViewModelMapper::perform);
  }

}
