package br.com.taqtile.android.app.presentation.gestation.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentViewModel implements Parcelable{

  private Integer week;
  private String WeekContentImage;
  private String WeekContentBody;

  public WeeklyContentViewModel(Integer week, String weekContentImage, String weekContentBody) {
    this.week = week;
    WeekContentImage = weekContentImage;
    WeekContentBody = weekContentBody;
  }

  public Integer getWeek() {
    return week;
  }

  public String getWeekContentImage() {
    return WeekContentImage;
  }

  public String getWeekContentBody() {
    return WeekContentBody;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.week);
    dest.writeString(this.WeekContentImage);
    dest.writeString(this.WeekContentBody);
  }

  protected WeeklyContentViewModel(Parcel in) {
    this.week = (Integer) in.readValue(Integer.class.getClassLoader());
    this.WeekContentImage = in.readString();
    this.WeekContentBody = in.readString();
  }

  public static final Creator<WeeklyContentViewModel> CREATOR = new Creator<WeeklyContentViewModel>() {
    @Override
    public WeeklyContentViewModel createFromParcel(Parcel source) {
      return new WeeklyContentViewModel(source);
    }

    @Override
    public WeeklyContentViewModel[] newArray(int size) {
      return new WeeklyContentViewModel[size];
    }
  };
}
