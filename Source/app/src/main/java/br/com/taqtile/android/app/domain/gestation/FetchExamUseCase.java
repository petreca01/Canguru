package br.com.taqtile.android.app.domain.gestation;

import br.com.taqtile.android.app.data.exams.ExamsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.mapper.ExamsResultToViewModelMapper;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model.ExamViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/16/17.
 */

public class FetchExamUseCase extends BaseUseCase<ExamViewModel, Void, ExamsRepository> {

  public FetchExamUseCase(ExamsRepository examsRepository) {
    super(examsRepository);
  }

  @Override
  public Observable<ExamViewModel> execute(Void aVoid) {
    return getRepository().fetchExam()
      .map(ExamsResultToViewModelMapper::perform);
  }
}
