package br.com.taqtile.android.app.domain.channel.mappers;

import br.com.taqtile.android.app.domain.channel.models.ChannelCategoryResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelWithCategoryResult;
import br.com.taqtile.android.app.listings.viewmodels.ChannelHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ChannelListingImplViewModel;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 29/03/17.
 */

public class ChannelResultListToChannelListingViewModelListMapper {
  public static List<SearchListingViewModel> map(ChannelCategoryResult channelResultList) {
    List<ChannelWithCategoryResult> channelsList = channelResultList.getSection().getChannelList();
    List<SearchListingViewModel> formattedChannelList = new ArrayList<>();
    for (ChannelWithCategoryResult channel : channelsList) {
      formattedChannelList.addAll(ChannelResultListToChannelListingViewModelListMapper.map(channel));
    }
    return formattedChannelList;
  }

  private static List<SearchListingViewModel> map(ChannelWithCategoryResult channelWithCategoryResult) {
    List<SearchListingViewModel> channelsWithHeaders = new ArrayList<>();
    channelsWithHeaders.add(new ChannelHeaderImplViewModel(channelWithCategoryResult.getCategory()));
    channelsWithHeaders.addAll(ChannelResultListToChannelListingViewModelListMapper.mapChannels(channelWithCategoryResult.getChannels()));
    return channelsWithHeaders;
  }

  private static List<SearchListingViewModel> mapChannels(List<ChannelResult> channels){
    return StreamSupport.stream(channels)
      .map(ChannelResultListToChannelListingViewModelListMapper::mapChannel)
      .collect(Collectors.toList());
  }

  private static ChannelListingImplViewModel mapChannel(ChannelResult channelResult){
    return new ChannelListingImplViewModel(
      channelResult.getAuthor(),
      channelResult.getTotalPosts(),
      channelResult.getFollowers(),
      String.valueOf(channelResult.getId()),
      channelResult.getName()
    );
  }

}
