package br.com.taqtile.android.app.presentation.maternity;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.listings.cells.TextImageCellCenter;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityHeaderCell;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.LocationHelper;
import br.com.taqtile.android.app.support.helpers.StringScoreFilter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.MATERNITY_CITY_TAG;

/**
 * Created by taqtile on 3/17/17.
 */

public class MaternityFragment extends AppBaseFragment implements MaternityHeaderCell.Listener, LocationHelper.RequestLocationResponse {

  @BindView(R.id.fragment_maternity_recycler_view)
  RecyclerView maternityCellAndHeaderRecyclerView;
  MaternityAdapter maternityAdapter;

  @BindView(R.id.fragment_maternity_not_found_maternity_button)
  TextImageCellCenter fragmentMaternityNotFoundMaternityButton;

  private NavigationManager navigationManager;
  private MaternityContract.Presenter presenter;
  private CustomToolbar customToolbar;

  private List<MaternityViewModel> maternityViewModelsList;
  private String city;

  private LocationHelper locationHelper;
  public boolean viewAlreadyConfigured = false;
  public boolean tutorialAlreadySeen = false;

  public static MaternityFragment newInstance() {
    return new MaternityFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_maternity, container, false);
    ButterKnife.bind(this, v);
    return v;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    this.setupRecyclerView();
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    this.unsubscribeAll();
  }

  @Override
  public void willAppear() {
    super.willAppear();
    setupToolbar();

    if (!tutorialAlreadySeen) {
      checkTutorial();
    } else if (!viewAlreadyConfigured) {
      defineSearchCity();
      viewAlreadyConfigured = true;
    }

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_hospital),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void checkTutorial() {

    // check tutorial already seen
    Subscription subscription =
      Toolbox.getInstance().getMainTutorial().subscribe(
        (alreadySeen) -> {
          if (alreadySeen) {
            tutorialAlreadySeen = true;
            if (!viewAlreadyConfigured) {
              defineSearchCity();
            }
          } else {
            hideLoading();
          }
        });
    compositeSubscription.add(subscription);
  }

  private void defineSearchCity() {

    hideContent();
    showLoading();

    Subscription subscription = getUserCity().subscribe(
      (userCity) -> {
        if (userCity == null || userCity.isEmpty()) {
          getCurrentCity();
        } else {
          this.city = userCity;
          viewAlreadyConfigured = true;
          fetchMaternity();
        }
      },
      this::onGetUserCityFailure);
    compositeSubscription.add(subscription);
  }

  private Observable<String> getUserCity() {
    // try to get city from user data
    return this.presenter.fetchUserData().map(UserViewModel::getCity);
  }

  private void onGetUserCityFailure(Throwable throwable) {
    getCurrentCity();
  }

  private void getCurrentCity() {
    locationHelper = new LocationHelper(this, this);
    locationHelper.getCurrentLocation();
    // the continuation this is in onRequestLocationResponse method, because depends on user response
  }

  @Override
  public void onRequestLocationResponse(@Nullable Location location) {

    if (location != null) {

      Address address = LocationHelper.getAddressByLocation(getContext(), location);
      if (address != null &&
        address.getSubAdminArea() != null &&
        !address.getSubAdminArea().isEmpty()) {

        // if the city was identified, get the city's maternity
        this.city = address.getSubAdminArea();
        viewAlreadyConfigured = true;
        fetchMaternity();

      } else {
        showCityNotDefined();
      }
    } else {
      showCityNotDefined();
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.hideLeftButton();
      customToolbar.setTitle(getResources()
        .getString(R.string.fragment_maternity_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));

      ViewCompat.setElevation(getToolbar(), 0);
    }
  }

  private void setupRecyclerView() {
    maternityViewModelsList = new ArrayList<>();
    maternityAdapter = new MaternityAdapter(maternityViewModelsList, this::onMaternityCellClicked);
    maternityAdapter.setListener(this);
    maternityCellAndHeaderRecyclerView.setAdapter(maternityAdapter);
    maternityCellAndHeaderRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override
  public void onFilterSubmitted(String text) {
    StringScoreFilter.orderMaternitiesByScore(maternityViewModelsList, text);
    maternityAdapter.notifyDataSetChanged();
  }

  @Override
  public void onChangeCityClicked() {
    goToMaternitySearch();
  }

  private void onReturnFromMaternityCitySearch(Bundle bundle, int resultCode) {

    if (resultCode == Activity.RESULT_OK) {
      String city = bundle.getString(MATERNITY_CITY_TAG, this.city);
      this.city = city;
      fetchMaternity();
    } else if (resultCode == Activity.RESULT_CANCELED) {
      hideLoading();

      if (this.city == null || this.city.isEmpty() ||
        (this.maternityViewModelsList == null || this.maternityViewModelsList.isEmpty())) {
        showCityNotDefined();
      }
    }
  }

  private void showCityNotDefined() {
    hideContent();
    hideLoading();
    placeholder.setPlaceholderType(CustomPlaceholder.CITY_NOT_DEFINED);
    showPlaceholder();
  }

  private void fetchMaternity() {
    Subscription subscription = presenter.fetchMaternitiesByCity(this.city)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchMaternitySuccess,
        this::onFetchMaternityFailure);
    compositeSubscription.add(subscription);
  }

  private void onFetchMaternitySuccess(List<MaternityViewModel> maternityViewModels) {
    this.maternityViewModelsList.clear();
    if (maternityViewModels == null || maternityViewModels.isEmpty()) {
      showCityNotDefined();
    } else {
      this.maternityViewModelsList.addAll(maternityViewModels);
      this.maternityAdapter.notifyDataSetChanged();
      this.showContent();
      this.hideLoading();
      this.hidePlaceholder();
    }
  }

  private void onFetchMaternityFailure(Throwable throwable) {
    hideContent();
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(MaternityContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void onMaternityCellClicked(int id) {
    navigationManager.showMaternityDetail(id);
  }

  private void showContent() {
    maternityCellAndHeaderRecyclerView.setVisibility(View.VISIBLE);
  }

  private void hideContent() {
    maternityCellAndHeaderRecyclerView.setVisibility(View.GONE);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_maternity_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    hideContent();
    showLoading();

    goToMaternitySearch();
    maternityNotFound();
  }

  private void goToMaternitySearch() {
    navigationManager.showMaternityCitySearch(this::onReturnFromMaternityCitySearch);
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public boolean refresh() {
    return super.refresh();
  }

  private void maternityNotFound() {

    fragmentMaternityNotFoundMaternityButton.setListener(() -> navigationManager.showMaternityNotFoundUI());

  }


}
