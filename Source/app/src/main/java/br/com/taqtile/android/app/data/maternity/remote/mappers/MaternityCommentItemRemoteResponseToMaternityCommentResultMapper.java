package br.com.taqtile.android.app.data.maternity.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityCommentItemRemoteResponse;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityCommentItemRemoteResponseToMaternityCommentResultMapper {

  public static List<MaternityCommentResult> perform(
    List<MaternityCommentItemRemoteResponse> maternityCommentItemRemoteResponseList) {
    return StreamSupport.stream(maternityCommentItemRemoteResponseList)
      .map(MaternityCommentItemRemoteResponseToMaternityCommentResultMapper::perform)
      .collect(Collectors.toList());
  }

  public static MaternityCommentResult perform(
    MaternityCommentItemRemoteResponse maternityCommentItemRemoteResponse) {
    return new MaternityCommentResult(maternityCommentItemRemoteResponse.getId(),
      maternityCommentItemRemoteResponse.getUserId(),
      maternityCommentItemRemoteResponse.getHospitalId(),
      maternityCommentItemRemoteResponse.getComment(),
      maternityCommentItemRemoteResponse.getActive(),
      maternityCommentItemRemoteResponse.getCreatedAt(),
      maternityCommentItemRemoteResponse.getUpdatedAt(),
      maternityCommentItemRemoteResponse.getUsername(),
      maternityCommentItemRemoteResponse.getUserPicture());
  }
}
