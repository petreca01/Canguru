package br.com.taqtile.android.app.data.account.remote.models.request;


import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class EditUserRemoteRequest extends BaseRemoteRequest {
  @RequestName("nome")
  private String name;

  @RequestName("senha")
  private String password;

  @RequestName("user_type")
  private String userType;

  @RequestName("baby_name")
  private String babyName;

  @RequestName("cpf")
  private String cpf;

  @RequestName("operator_document")
  private String operatorDocument;

  @RequestName("user_birthdate")
  private String userBirthdate;

  @RequestName("celular")
  private String cellphone;

  @RequestName("zip_code")
  private String zipCode;

  @RequestName("operator_id")
  private Integer operatorId;

  @RequestName("operator_can_see_risk_answers")
  private Integer operatorCanSeeRiskAnswers;

  @RequestName("dum")
  private String dum;

  @RequestName("dolp_updated_at")
  private String dolpUpdatedAt;

  @RequestName("sure")
  private Integer sure;

  @RequestName("conclusion_date")
  private String conclusionDate;

  @RequestName("conclusion_method")
  private String conclusionMethod;

  @RequestName("conclusion_text")
  private String conclusionText;

  @RequestName("deactivation_reason")
  private String deactivationReason;

  @RequestName("deactivated_at")
  private String deactivatedAt;

  @RequestName("city")
  private String city;

  @RequestName("state")
  private String state;

  @RequestName("latitude")
  private String latitude;

  @RequestName("longitude")
  private String longitude;

  @RequestName("location_privacy_agreed")
  private Boolean locationPrivacyAgreed;

  public EditUserRemoteRequest(String cpf, String userBirthdate, String cellphone,
                               String zipCode, Integer operatorId, String city,
                               String state, String latitude, String longitude,
                               Boolean locationPrivacyAgreed, String operatorDocument) {
    this.cpf = cpf;
    this.userBirthdate = userBirthdate;
    this.cellphone = cellphone;
    this.zipCode = zipCode;
    this.operatorId = operatorId;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
    this.operatorDocument = operatorDocument;
  }

  public EditUserRemoteRequest(String password) {
    this.password = password;
  }

  public EditUserRemoteRequest(String dum, String userType, String babyName, String dolpUpdatedAt) {
    this.dum = dum;
    this.userType = userType;
    this.babyName = babyName;
    this.dolpUpdatedAt = dolpUpdatedAt;
  }

}
