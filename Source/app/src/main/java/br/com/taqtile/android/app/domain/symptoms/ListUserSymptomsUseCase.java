package br.com.taqtile.android.app.domain.symptoms;

import br.com.taqtile.android.app.data.symptoms.SymptomsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.symptoms.mapper.UserSymptomResultToSymptomHistoryViewModelMapper;
import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ListUserSymptomsUseCase extends
  BaseUseCase<List<SymptomHistoryViewModel>, Void, SymptomsRepository> {

  public ListUserSymptomsUseCase(SymptomsRepository symptomsRepository) {
    super(symptomsRepository);
  }

  @Override
  public Observable<List<SymptomHistoryViewModel>> execute(Void aVoid) {
    return getRepository().listUserSymptoms(new ViewModeParams())
      .map(UserSymptomResultToSymptomHistoryViewModelMapper::perform);
  }
}
