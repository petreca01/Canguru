package br.com.taqtile.android.app.data.weeklycontent.remote;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.weeklycontent.remote.models.response.WeeklyContentRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by taqtile on 5/16/17.
 */

public interface WeeklyContentServices {

  @GET("weekly_content/{week}")
  Observable<Result<BaseRemoteResponse<WeeklyContentRemoteResponse>>>
  fetchContent(@Path("week") String week);
}
