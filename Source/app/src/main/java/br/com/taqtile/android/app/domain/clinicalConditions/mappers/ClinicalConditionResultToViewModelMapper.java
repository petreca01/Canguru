package br.com.taqtile.android.app.domain.clinicalConditions.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionItemResult;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionResult;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/14/17.
 */

public class ClinicalConditionResultToViewModelMapper {

  private static final String SUBMENU = "submenu";
  private static final String SUBITEM = "subitem";
  private static final String PREGNANCY_WEEKS = "pregnancy_weeks";
  private static final String LESS_THAN = "less_than";
  private static final String GREATER_THAN_OR_EQUAL_TO = "greater_than_or_equal_to";

  public static List<ClinicalConditionViewModel> perform(List<ClinicalConditionResult> clinicalConditionResults,
                                                         Integer gestationWeeks) {
    return StreamSupport.stream(clinicalConditionResults)
      .filter(Objects::nonNull)
      .map(condition -> ClinicalConditionResultToViewModelMapper.map(condition, gestationWeeks))
      .collect(Collectors.toList());
  }

  private static ClinicalConditionViewModel map(ClinicalConditionResult clinicalConditionResult,
                                                Integer gestationWeeks) {
    return new ClinicalConditionViewModel(
      ClinicalConditionResultToViewModelMapper
        .mapParent(clinicalConditionResult, gestationWeeks),
      ClinicalConditionResultToViewModelMapper
        .mapChildren(clinicalConditionResult.getChildren()));
  }

  private static ClinicalConditionItemViewModel mapParent(
    ClinicalConditionResult clinicalConditionResult, Integer gestationWeeks) {

    ClinicalConditionItemResult clinicalConditionItemResult = clinicalConditionResult.getClinicalConditionResult();

    if (clinicalConditionItemResult.getLogic() != null &&
      clinicalConditionItemResult.getLogic().equals(PREGNANCY_WEEKS)) {

      return findChild(clinicalConditionResult.getChildren(), gestationWeeks);
    } else {

      return new ClinicalConditionItemViewModel(
        clinicalConditionItemResult.getId(), clinicalConditionItemResult.getParentId(),
        clinicalConditionItemResult.getName(), clinicalConditionItemResult.getContent());
    }

  }

  private static ClinicalConditionItemViewModel findChild(
    List<ClinicalConditionItemResult> clinicalConditionItemResults, Integer gestationWeeks) {
    return StreamSupport.stream(clinicalConditionItemResults)
      .filter(Objects::nonNull)
      .filter(clinicalCondition -> satisfiesLogicConditions(clinicalCondition, gestationWeeks))
      .map(ClinicalConditionResultToViewModelMapper::mapChild)
      .findFirst()
      .orElse(null);
  }

  private static List<ClinicalConditionItemViewModel> mapChildren(
    List<ClinicalConditionItemResult> clinicalConditionItemResults) {
    return StreamSupport.stream(clinicalConditionItemResults)
      .filter(Objects::nonNull)
      .filter(condition -> condition.getLogic().equals(SUBITEM))
      .map(ClinicalConditionResultToViewModelMapper::mapChild)
      .collect(Collectors.toList());
  }

  private static ClinicalConditionItemViewModel mapChild(
    ClinicalConditionItemResult clinicalConditionItemResult) {

    String conditionName = clinicalConditionItemResult.getLogic().equals(SUBITEM) ?
      clinicalConditionItemResult.getName() + " " + clinicalConditionItemResult.getLogicTrigger() :
      clinicalConditionItemResult.getName();

    return new ClinicalConditionItemViewModel(
      clinicalConditionItemResult.getId(), clinicalConditionItemResult.getParentId(),
      conditionName, clinicalConditionItemResult.getContent());
  }

  private static boolean satisfiesLogicConditions(
    ClinicalConditionItemResult clinicalConditionItemResult, Integer gestationWeeks) {
    return (clinicalConditionItemResult.getLogic().equals(LESS_THAN) &&
      gestationWeeks < Integer.valueOf(clinicalConditionItemResult.getLogicTrigger())) ||
      (clinicalConditionItemResult.getLogic().equals(GREATER_THAN_OR_EQUAL_TO) &&
        gestationWeeks >= Integer.valueOf(clinicalConditionItemResult.getLogicTrigger()));
  }
}
