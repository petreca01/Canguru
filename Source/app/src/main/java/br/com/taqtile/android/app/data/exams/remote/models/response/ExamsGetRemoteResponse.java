package br.com.taqtile.android.app.data.exams.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/22/17.
 */

public class ExamsGetRemoteResponse {
    @SerializedName("values")
    private ExamsValuesRemoteResponse values;

    @SerializedName("done")
    private Integer done;

    @SerializedName("observations")
    private String observations;

    @SerializedName("date")
    private String date;

    public ExamsValuesRemoteResponse getValues() {
        return values;
    }

    public Integer getDone() {
        return done;
    }

    public String getObservations() {
        return observations;
    }

    public String getDate() {
        return date;
    }
}
