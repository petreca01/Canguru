package br.com.taqtile.android.app.data.followcard.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardVaccinesRemoteResponse;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardVaccinesResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardVaccinesRemoteResponseToResultMapper {

  public static List<FollowCardVaccinesResult> perform(
    List<FollowCardVaccinesRemoteResponse> followCardVaccinesRemoteResponse) {
    return StreamSupport.stream(followCardVaccinesRemoteResponse)
      .map(FollowCardVaccinesRemoteResponseToResultMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardVaccinesResult map(
    FollowCardVaccinesRemoteResponse followCardVaccinesRemoteResponse) {
    return new FollowCardVaccinesResult(
      followCardVaccinesRemoteResponse.getId(),
      followCardVaccinesRemoteResponse.getName(),
      followCardVaccinesRemoteResponse.getTaken(),
      followCardVaccinesRemoteResponse.getCreatedAt());
  }
}
