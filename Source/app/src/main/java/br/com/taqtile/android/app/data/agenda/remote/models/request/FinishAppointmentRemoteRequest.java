package br.com.taqtile.android.app.data.agenda.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/21/17.
 */

public class FinishAppointmentRemoteRequest extends BaseRemoteRequest {
    @RequestName("agenda_id")
    private Integer agendaId;

    @RequestName("isCompromisso")
    private Integer isAppointment;

    @RequestName("realizado")
    private Integer done;

  public FinishAppointmentRemoteRequest(Integer agendaId, Integer isAppointment, Integer done) {
    this.agendaId = agendaId;
    this.isAppointment = isAppointment;
    this.done = done;
  }
}
