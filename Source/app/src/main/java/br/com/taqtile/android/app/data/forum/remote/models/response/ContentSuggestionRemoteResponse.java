package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionRemoteResponse {

  @SerializedName("total_contents")
  private Integer totalContents;

  @SerializedName("contents_in_page")
  private Integer contentsInPage;

  @SerializedName("contents")
  private List<ContentSuggestionItemRemoteResponse> contents;

  public Integer getTotalContents() {
    return totalContents;
  }

  public Integer getContentsInPage() {
    return contentsInPage;
  }

  public List<ContentSuggestionItemRemoteResponse> getContents() {
    return contents;
  }
}
