package br.com.taqtile.android.app.presentation.changepassword;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.UpdatePasswordUseCase;
import br.com.taqtile.android.app.domain.account.models.EditUserPasswordParams;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 11/15/16.
 */

public class ChangePasswordPresenter implements ChangePasswordContract.Presenter {

  private UpdatePasswordUseCase updatePasswordUseCase;

  public ChangePasswordPresenter() {
    this.updatePasswordUseCase = Injection.provideUpdatePasswordUseCase();
  }

  @Override
  public Observable<EmptyResult> changePassword(String newPassword, String confirmPassword) {
    return updatePasswordUseCase.execute(new EditUserPasswordParams(newPassword));
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }
}
