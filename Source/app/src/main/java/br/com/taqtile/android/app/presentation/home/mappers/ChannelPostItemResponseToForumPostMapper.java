package br.com.taqtile.android.app.presentation.home.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 3/24/17.
 */

public class ChannelPostItemResponseToForumPostMapper {

  public static List<SocialFeedViewModel> perform(List<ChannelPost> channelPostList) {
    if (channelPostList == null) {
      return new ArrayList<>();
    }

    return StreamSupport.stream(channelPostList)
      .map(ChannelPostItemResponseToForumPostMapper::mapChannelList)
      .collect(Collectors.toList());

  }

  private static SocialFeedViewModel mapChannelList(ChannelPost channelPost) {

    return new SocialFeedChannelImplViewModel(
      channelPost.getVideo() != null ? channelPost.getVideo() : "",
      channelPost.getImage() != null ? channelPost.getImage() : "",
      channelPost.getId(),
      channelPost.getChannelId(),
      channelPost.getPostAuthor(),
      channelPost.getChannelName(),
      channelPost.getCreatedAt(),
      channelPost.getTitle(),
      channelPost.getDescription(),
      channelPost.getLikes(),
      channelPost.getReplies(),
      channelPost.getLiked());
  }
}
