package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostReplyDeleteParams {
  private Integer postId;
  private Integer replyId;

  public ChannelPostReplyDeleteParams(Integer postId, Integer replyId) {
    this.postId = postId;
    this.replyId = replyId;
  }

  public Integer getPostId() {
    return postId;
  }

  public Integer getReplyId() {
    return replyId;
  }

}
