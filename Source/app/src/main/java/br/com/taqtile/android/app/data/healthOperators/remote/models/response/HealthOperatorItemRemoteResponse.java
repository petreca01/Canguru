package br.com.taqtile.android.app.data.healthOperators.remote.models.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class HealthOperatorItemRemoteResponse {
  private static final String TAG = "HOperatorItemRemoteRps";
  @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("about")
    private String about;

    @SerializedName("logo")
    private String logo;

    @SerializedName("health_plan_active")
    private Boolean healthPlanActive;

    @SerializedName("health_plan_config")
    private HealthPlanConfigRemoteResponse healthPlanConfig;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("document_type")
    private String documentType;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        Log.e(TAG, "getName(). name: "+name);
        return name;
    }

    public String getAbout() {
        return about;
    }

    public String getLogo() {
        return logo;
    }

    public Boolean getHealthPlanActive() {
        Log.e(TAG, "getHealthPlanActive(). healthPlanActive: "+healthPlanActive);
        return healthPlanActive;
    }

    public HealthPlanConfigRemoteResponse getHealthPlanConfig() {
        return healthPlanConfig;
    }

    public Boolean getActive() {
        return active;
    }

    public String getDocumentType() {
        Log.e(TAG, "getDocumentType(). documentType: "+documentType);
        return documentType;
    }
}

