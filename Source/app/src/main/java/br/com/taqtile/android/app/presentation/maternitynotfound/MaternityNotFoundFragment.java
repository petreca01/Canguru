package br.com.taqtile.android.app.presentation.maternitynotfound;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.maternity.MaternityAdapter;
import br.com.taqtile.android.app.presentation.maternity.MaternityContract;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityHeaderCell;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.LocationHelper;
import br.com.taqtile.android.app.support.helpers.StringScoreFilter;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.MATERNITY_CITY_TAG;

public class MaternityNotFoundFragment extends AppBaseFragment {

  private CustomToolbar customToolbar;
  private NavigationManager navigationManager;
  private MaternityNotFoundContract.Presenter presenter;

  public static MaternityNotFoundFragment newInstance() {
    return new MaternityNotFoundFragment();
  }


  public MaternityNotFoundFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_maternity_not_found, container, false);
    ButterKnife.bind(this, v);
    return v;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    //TODO: REMOVER TESTE DE TOAST ABAIXO

    Toast.makeText(getContext(), "XML fragment_maternity_not_found aberto ", Toast.LENGTH_LONG).show();

    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  public void setNavigationManager(NavigationManager navigationManager){
    this.navigationManager = navigationManager;
  }

  public void setPresenter(MaternityNotFoundContract.Presenter presenter){
    this.presenter = presenter;
  }

  @Override
  public boolean refresh() {
    return super.refresh();
  }

}
