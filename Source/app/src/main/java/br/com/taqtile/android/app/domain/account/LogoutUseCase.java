package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 4/29/17.
 */

public class LogoutUseCase extends BaseUseCase<EmptyResult, Void,AccountRepository> {

  public LogoutUseCase(AccountRepository accountRepository) {
    super(accountRepository);
  }

  @Override
  public Observable<EmptyResult> execute(Void aVoid) {
    return getRepository().logout();
  }
}
