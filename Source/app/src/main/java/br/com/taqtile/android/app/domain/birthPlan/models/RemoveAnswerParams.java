package br.com.taqtile.android.app.domain.birthPlan.models;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class RemoveAnswerParams {
  private String answerId;

  public RemoveAnswerParams(String answerId) {
    this.answerId = answerId;
  }

  public String getAnswerId() {
    return answerId;
  }
}
