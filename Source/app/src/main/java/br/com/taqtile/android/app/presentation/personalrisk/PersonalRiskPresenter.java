package br.com.taqtile.android.app.presentation.personalrisk;

import java.util.List;

import br.com.taqtile.android.app.domain.riskAnalysis.ListGroupsUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by Renato on 4/17/17.
 */

public class PersonalRiskPresenter implements PersonalRiskContract.Presenter {

  private ListGroupsUseCase listGroupsUseCase;

  public PersonalRiskPresenter() {
    this.listGroupsUseCase = Injection.provideListGroupsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<RiskAnalysisQuestionGroupResult>> fetchRiskAnalysisQuestionAnswersList() {
    return listGroupsUseCase.execute(null);
  }

  @Override
  public Observable<List<RiskAnalysisQuestionGroupResult>> sendRiskAnalysisQuestionAnswersList(String email) {
    return listGroupsUseCase.execute(email);
  }
}
