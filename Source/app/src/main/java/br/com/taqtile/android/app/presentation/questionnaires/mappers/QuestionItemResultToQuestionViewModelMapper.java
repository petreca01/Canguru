package br.com.taqtile.android.app.presentation.questionnaires.mappers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModelFactory;

import static br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest.MULTIPLE_CHOICES_SEPARATOR;

/**
 * Created by felipesabino on 5/22/17.
 */

public class QuestionItemResultToQuestionViewModelMapper {

  private final static String BOOLEAN = "boolean";
  private final static String IMC = "imc";
  private final static String TEXT = "text";
  private final static String VARCHAR = "varchar";
  private final static String MULTIPLE = "multiple";
  private final static String SELECT = "select";

  public static QuestionViewModel perform(QuestionItemResult question) throws DomainError {

    QuestionViewModel viewModel;
    AnswerItemResult answerModel = question.getAnswer() != null ?
      question.getAnswer() : new AnswerItemResult();
    String answer = answerModel.getAnswer() != null ?
      answerModel.getAnswer() : "";

    switch (question.getAnswerType()) {
      case BOOLEAN: {
        viewModel = QuestionViewModelFactory.newBooleanType(question.getId(), question.getQuestion(),
          question.getDescription(), answer);
        break;
      }
      case IMC: {
        viewModel = QuestionViewModelFactory.newImcType(question.getId(), question.getQuestion(),
          question.getDescription(), answer);
        break;
      }
      case TEXT: {
        viewModel = QuestionViewModelFactory.newTextType(question.getId(), question.getQuestion(),
          question.getDescription(), answer);
        break;
      }
      case VARCHAR: {
        viewModel = QuestionViewModelFactory.newVarcharType(question.getId(), question.getQuestion(),
          question.getDescription(), answer);
        break;
      }
      case MULTIPLE: {
        List<String> answers;
        if (answer != null) {
          answers = Arrays.asList(answer.split(MULTIPLE_CHOICES_SEPARATOR));
        } else {
          answers = Collections.emptyList();
        }
        viewModel = QuestionViewModelFactory.newMultipleType(question.getId(), question.getQuestion(),
          question.getDescription(), question.getAlternatives(), answers);
        break;
      }
      case SELECT: {
        if (question.getAlternatives() != null) {
          viewModel = QuestionViewModelFactory.newSelectType(question.getId(), question.getQuestion(),
            question.getDescription(), question.getAlternatives(), answer);
        } else if (question.getRange() != null) {
          RangedAnswerResult range = question.getRange();
          viewModel = QuestionViewModelFactory.newRangeType(question.getId(), question.getQuestion(),
            question.getDescription(), answer, range.getMin(), range.getMax());
        } else {
          DomainError error = new DomainError();
          error.setErrorType(DomainError.INVALID_DATA);
          throw error;
        }
        break;
      }
      default:
        DomainError error = new DomainError();
        error.setErrorType(DomainError.INVALID_DATA);
        throw error;
    }

    return viewModel;

  }
}
