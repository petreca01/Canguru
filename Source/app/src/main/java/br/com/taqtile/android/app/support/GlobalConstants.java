package br.com.taqtile.android.app.support;

/**
 * Created by taqtile on 3/17/17.
 */

public class GlobalConstants {

  //  REGION SHARED PREFERENCES
  public static final String USER_TOKEN_KEY = "userTokenKey";
  public static final String USER_PROFILE_PICTURE_URL_KEY = "userProfilePictureURLKey";
  public static final String USER_ID_KEY = "userIdKey";
  public static final String USER_NAME_KEY = "userNameKey";
  public static final String USER_HAS_CPF_AND_PHONE_NUMBER_KEY = "userHasCPFAndPhoneNumberKey";
  public static final String USER_ZIP_CODE_KEY = "userZipCode";
  public static final String MAIN_TUTORIAL_KEY = "mainTutorialKey";
  public static final String DIARY_TUTORIAL_KEY = "diaryTutorialKey";
  public static final String LAST_SEEN_NOTIFICATION_ID_KEY = "lastSeenNotificationIdKey";
  public static final String NOTIFICATION_CACHE_KEY = "notificationCacheKey";
  public static final String USER_EMAIL_KEY = "userEmailKey";

  // REGION EXTRAS KEYS
  public static final String ADDITIONAL_INFO_TAG = "additionalInfo";
  public static final String MATERNITY_CITY_TAG = "maternityCity";
  public static final String USER_DATA_KEY = "userDataKey";
  public static final String WEEKLY_CONTENT_DATA_KEY = "weeklyContentDataKey";
  public static final String CLINICAL_CONDITIONS_TAG = "clinicalConditionsKey";
  public final static String REQUEST_CODE = "requestCode";
  public final static String BIRTH_PLAN_ANSWERED_PERCENTAGE_KEY = "birthPlanAnsweredPercentageKey";
  public final static String BIRTH_PLAN_QUESTIONNAIRE_KEY = "birthPlanQuestionnaireKey";
  public final static String BIRTH_PLAN_QUESTIONNAIRE_QUESTION_INDEX_KEY = "birthPlanQuestionnaireQuestionKey";
  public final static String RISK_ANALYSIS_QUESTIONNAIRE_KEY = "riskAnalysisQuestionnaireKey";
  public final static String FORGOT_PASSWORD_KEY = "forgotPasswordKey";
  public final static String FORGOT_PASSWORD_EMAIL_KEY = "forgotPasswordEmailKey";
  public final static String SIGNUP_FROM_SIGNIN_EMAIL_KEY = "signinFromSignupEmailKey";
  public final static String SIGNUP_FROM_SIGNIN_PASSWORD_KEY = "signinFromSignupPasswordKey";
  public final static String RATING_MATERNITY_KEY = "ratingMaternityKey";
  public final static String POST_DETAIL_ACTION_KEY = "postDetailActionKey";
  public final static String POST_DETAIL_ACTION_POST_ID_KEY = "postDetailActionPostIdKey";
  public final static String SYMPTOM_DETAIL_PARENT_ID_KEY = "symptomDetailParentIdKey";
  public final static String SYMPTOM_DETAIL_ID_KEY = "symptomDetailIdKey";

  // REGION REQUEST CODES
  public static final int EDIT_GESTATION_CODE = 0010;
  public static final int EDIT_APPOINTMENT_REQUEST_CODE = 1212;
  public static final int SYMPTOM_DETAILS_REQUEST_CODE = 2521;
  public static final int CLINICAL_CONDITIONS_DETAILS_REQUEST_CODE = 2522;
  public static final int NOTIFICATION_MAX_AMOUNT_TO_KEEP = 100;
  public static final int MATERNITY_CITY_SEARCH_REQUEST_CODE = 1234;
  public static final int BIRTH_PLAN_REQUEST_CODE = 1233;
  public static final int FORGOT_PASSWORD_CODE = 1232;
  public static final int CREATE_POST_REQUEST_CODE = 3333;
  public static final int VIEW_POST_CODE = 6666;
  public static final int GESTATION_CONCLUSION_REQUEST_CODE = 6667;

  // REGION GENERAL
  public static final String PREGNANT = "pregnant";

  // REGION MAPPERS
  public static final String RISK_ANALYSIS_GROUP_ABOUT_YOU = "Sobre você";
  public static final String RISK_ANALYSIS_GROUP_ABOUT_YOUR_OBSTETRICAL_PAST = "Seu passado obstétrico";
  public static final String RISK_ANALYSIS_GROUP_ABOUT_YOUR_FAMILY = "Sobre sua família";
  public static final String RISK_ANALYSIS_GROUP_ABOUT_YOUR_HEALTH = "Sobre sua saúde";
}
