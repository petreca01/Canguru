package br.com.taqtile.android.app.data.symptoms;

import java.util.List;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.symptoms.mappers.ReportSymptomParamsToReportSymptomRemoteRequestMapper;
import br.com.taqtile.android.app.data.symptoms.mappers.ReportSymptomRemoteResponseToReportSymptomResultMapper;
import br.com.taqtile.android.app.data.symptoms.mappers.SymptomsItemRemoteResponseListToSymptomResultListMapper;
import br.com.taqtile.android.app.data.symptoms.mappers.UserSymptomItemRemoteResponseListToUserSymptomResultListMapper;
import br.com.taqtile.android.app.data.symptoms.remote.SymptomsRemoteDataSource;
import br.com.taqtile.android.app.data.view.mappers.ViewModeParamsToViewModeRemoteRequestMapper;
import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomParams;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomResult;
import br.com.taqtile.android.app.domain.symptoms.models.SymptomResult;
import br.com.taqtile.android.app.domain.symptoms.models.UserSymptomResult;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class SymptomsRepository extends BaseRepository<SymptomsRemoteDataSource, Void> {

  public SymptomsRepository(SymptomsRemoteDataSource symptomsRemoteDataSource) {
    super(symptomsRemoteDataSource, null);
  }

  public Observable<List<SymptomResult>> list() {
    return getRemoteDataSource().list()
      .map(result -> SymptomsItemRemoteResponseListToSymptomResultListMapper
        .perform(result.getResult()));
  }

  public Observable<List<UserSymptomResult>> listUserSymptoms(ViewModeParams viewModeParams) {
    return getRemoteDataSource().listUserSymptoms(
      ViewModeParamsToViewModeRemoteRequestMapper.perform(viewModeParams))
      .map(result -> UserSymptomItemRemoteResponseListToUserSymptomResultListMapper
        .perform(result.getResult()));
  }

  public Observable<ReportSymptomResult> reportSymptom(ReportSymptomParams reportSymptomParams) {
    return getRemoteDataSource().reportSymptom(
      ReportSymptomParamsToReportSymptomRemoteRequestMapper.perform(reportSymptomParams))
      .map(result -> ReportSymptomRemoteResponseToReportSymptomResultMapper
        .perform(result.getResult()));
  }

}
