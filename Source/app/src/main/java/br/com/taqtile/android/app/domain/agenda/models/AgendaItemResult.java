package br.com.taqtile.android.app.domain.agenda.models;

/**
 * Created by taqtile on 4/7/17.
 */

public class AgendaItemResult {

  private Boolean loaded;
  private Integer id;
  private Boolean operatorHighlight;
  private Integer quarter;
  private String name;
  private String code;
  private Integer typeId;
  private String type;
  private String weekStart;
  private Integer weekEnd;
  private String dayEnd;
  private String description;
  private String descriptionImageUrl;
  private String completeDescription;
  private String observation;
  private Integer done;
  private String createdAt;
  private String updatedAt;
  private String markingDate;
  private String markingObservation;
  private Integer isAppointment;

  public AgendaItemResult(Boolean loaded, Integer id, Boolean operatorHighlight, Integer quarter,
                          String name, String code, Integer typeId, String type, String weekStart,
                          Integer weekEnd, String dayEnd, String description,
                          String descriptionImageUrl, String completeDescription,
                          String observation, Integer done, String createdAt, String updatedAt,
                          String markingDate, String markingObservation, Integer isAppointment) {
    this.loaded = loaded;
    this.id = id;
    this.operatorHighlight = operatorHighlight;
    this.quarter = quarter;
    this.name = name;
    this.code = code;
    this.typeId = typeId;
    this.type = type;
    this.weekStart = weekStart;
    this.weekEnd = weekEnd;
    this.dayEnd = dayEnd;
    this.description = description;
    this.descriptionImageUrl = descriptionImageUrl;
    this.completeDescription = completeDescription;
    this.observation = observation;
    this.done = done;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.markingDate = markingDate;
    this.markingObservation = markingObservation;
    this.isAppointment = isAppointment;
  }

  public Boolean getLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public Boolean getOperatorHighlight() {
    return operatorHighlight;
  }

  public Integer getQuarter() {
    return quarter;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public Integer getTypeId() {
    return typeId;
  }

  public String getType() {
    return type;
  }

  public String getWeekStart() {
    return weekStart;
  }

  public Integer getWeekEnd() {
    return weekEnd;
  }

  public String getDayEnd() {
    return dayEnd;
  }

  public String getDescription() {
    return description;
  }

  public String getDescriptionImageUrl() {
    return descriptionImageUrl;
  }

  public String getCompleteDescription() {
    return completeDescription;
  }

  public String getObservation() {
    return observation;
  }

  public Integer getDone() {
    return done;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getMarkingDate() {
    return markingDate;
  }

  public String getMarkingObservation() {
    return markingObservation;
  }

  public Integer getIsAppointment() {
    return isAppointment;
  }
}
