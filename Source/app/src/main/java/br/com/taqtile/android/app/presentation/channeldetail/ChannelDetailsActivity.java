package br.com.taqtile.android.app.presentation.channeldetail;

import android.content.Context;
import android.content.Intent;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.VIEW_POST_CODE;

/**
 * Created by taqtile on 4/5/17.
 */

public class ChannelDetailsActivity extends TemplateBackActivity {

  private ChannelDetailsFragment fragment;
  public final static String CHANNEL_ID_TAG = "postId";

  @Override
  public ChannelDetailsFragment getFragment() {
    ChannelDetailsFragment channelDetailFragment = (ChannelDetailsFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (channelDetailFragment == null) {
      channelDetailFragment = ChannelDetailsFragment.newInstance();
    }
    fragment = channelDetailFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ChannelDetailsPresenter channelDetailsPresenter = new ChannelDetailsPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(channelDetailsPresenter);

    return channelDetailsPresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener, int id) {
    Intent intent = new Intent(context, ChannelDetailsActivity.class);
    intent.putExtra(CHANNEL_ID_TAG, id);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener, VIEW_POST_CODE);
      ((MainActivity) context).startActivityForResult(intent, VIEW_POST_CODE);
    } else {
      context.startActivity(intent);
    }
  }
}
