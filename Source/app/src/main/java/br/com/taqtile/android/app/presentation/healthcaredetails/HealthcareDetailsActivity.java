package br.com.taqtile.android.app.presentation.healthcaredetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.searchhealthcare.SearchHealthcareActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareDetailsActivity extends TemplateBackActivity {

  private static final String TAG = "HealthcareDetailsActivi";
  private HealthcareDetailsFragment fragment;
  private static HealthOperatorViewModel healthOperatorViewModel;
  private static boolean toAdd;
  private final static int CHANGE_USER_HEALTHCARE_DETAIL_REQUEST_CODE = 1314;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private int onActivityResultCode;
  private int onActivityRequestCode;
  private Bundle onActivityResultBundle;

  @Override
  public Fragment getFragment() {
    HealthcareDetailsFragment healthcareDetailsFragment = (HealthcareDetailsFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (healthcareDetailsFragment == null) {
      healthcareDetailsFragment = HealthcareDetailsFragment.newInstance();
    }
    fragment = healthcareDetailsFragment;
    fragment.setHealthOperatorViewModel(healthOperatorViewModel);
    fragment.setToAdd(toAdd);

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    HealthcareDetailsPresenter healthcareDetailsPresenter = new HealthcareDetailsPresenter();
    fragment.setPresenter(healthcareDetailsPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return healthcareDetailsPresenter;
  }

  public static void navigate(Context context, HealthOperatorViewModel healthOperator,
                              boolean add, CustomNavigationResultListener listener) {

//    Log.e(TAG, "navigate(). healthOperator.getDocumentType(): "+healthOperator.getDocumentType());
//    Log.e(TAG, "navigate(). healthOperatorViewModel.getDocumentType(): "+healthOperatorViewModel.getDocumentType());

    healthOperatorViewModel = healthOperator;
    toAdd = add;
    Intent intent = new Intent(context, HealthcareDetailsActivity.class);
    if (context instanceof AdditionalInfoActivity) {
      ((AdditionalInfoActivity) context).setResultListener(listener,
        CHANGE_USER_HEALTHCARE_DETAIL_REQUEST_CODE);
      ((AdditionalInfoActivity) context).startActivityForResult(intent,
        CHANGE_USER_HEALTHCARE_DETAIL_REQUEST_CODE);
    } else if (context instanceof SearchHealthcareActivity) {
      ((SearchHealthcareActivity) context).setResultListener(listener,
        CHANGE_USER_HEALTHCARE_DETAIL_REQUEST_CODE);
      ((SearchHealthcareActivity) context).startActivityForResult(intent,
        CHANGE_USER_HEALTHCARE_DETAIL_REQUEST_CODE);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
      onActivityResultBundle = data != null ? data.getExtras() : new Bundle();
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
