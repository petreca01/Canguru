package br.com.taqtile.android.app.domain.symptoms;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.symptoms.SymptomsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class SendUserSymptomsUseCase extends BaseUseCase<EmptyResult, String, SymptomsRepository> {

  public SendUserSymptomsUseCase(SymptomsRepository symptomsRepository) {
    super(symptomsRepository);
  }

  @Override
  public Observable<EmptyResult> execute(String email) {
    return getRepository().listUserSymptoms(new ViewModeParams(email))
      .map(userSymptomResults -> new EmptyResult());
  }
}
