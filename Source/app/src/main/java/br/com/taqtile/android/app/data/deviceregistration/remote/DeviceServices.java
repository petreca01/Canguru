package br.com.taqtile.android.app.data.deviceregistration.remote;

import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/30/17.
 */

public interface DeviceServices {
  @FormUrlEncoded
  @POST("devices")
  Observable<Result<BaseRemoteResponse<EmptyResult>>> register(@FieldMap Map<String, String> deviceRequestModel);
}
