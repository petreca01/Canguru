package br.com.taqtile.android.app.data.symptoms.mappers;

import br.com.taqtile.android.app.data.symptoms.remote.models.request.ReportSymptomRemoteRequest;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomParams;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class ReportSymptomParamsToReportSymptomRemoteRequestMapper {

  public static ReportSymptomRemoteRequest perform(ReportSymptomParams reportSymptomParams) {
    return new ReportSymptomRemoteRequest(reportSymptomParams.getSymptomId(),
      reportSymptomParams.getDate(), reportSymptomParams.getDescription());
  }
}
