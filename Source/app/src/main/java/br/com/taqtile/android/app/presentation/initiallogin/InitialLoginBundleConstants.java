package br.com.taqtile.android.app.presentation.initiallogin;

/**
 * Created by taqtile on 26/04/17.
 */

class InitialLoginBundleConstants {
  protected static final String EMAIL_KEY = "email";
  protected static final String NAME_KEY = "name";
  protected static final String FB_TOKEN_KEY = "fbTokenKey";
}
