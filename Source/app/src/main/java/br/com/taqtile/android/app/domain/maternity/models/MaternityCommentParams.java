package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityCommentParams extends MaternityParams {
  private String comment;

  public MaternityCommentParams(String comment, Integer maternityId) {
    super(maternityId);
    this.comment = comment;

  }
  public String getComment() {
    return comment;
  }
}
