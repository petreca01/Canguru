package br.com.taqtile.android.app.presentation.tutorial;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by taqtile on 5/19/17.
 */

public class TutorialViewPagerCellModel {

  public static final int INVALID_INDEX = -1;
  @DrawableRes int icon;
  String title;
  String body;
  public int targetIndex;

  public TutorialViewPagerCellModel(int icon, String title, String body) {
    this.icon = icon;
    this.title = title;
    this.body = body;
    targetIndex = INVALID_INDEX;

  }

  public TutorialViewPagerCellModel(int icon, String title, String body, int targetIndex) {
    this.icon = icon;
    this.title = title;
    this.body = body;
    this.targetIndex = targetIndex;
  }
}
