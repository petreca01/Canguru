package br.com.taqtile.android.app.domain.birthPlan;

import java.util.List;

import br.com.taqtile.android.app.data.birthPlan.BirthPlanRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class ListBirthPlanQuestionAnswersUseCase extends
  BaseUseCase<List<QuestionItemResult>, String, BirthPlanRepository> {

  public ListBirthPlanQuestionAnswersUseCase(BirthPlanRepository birthPlanRepository) {
    super(birthPlanRepository);
  }

  @Override
  public Observable<List<QuestionItemResult>> execute(String email) {
    if (email != null && !email.isEmpty()) {
      return getRepository()
        .listQuestionsAndAnswers(new ViewModeParams(email));
    } else {
      return getRepository()
        .listQuestionsAndAnswers(new ViewModeParams());
    }
  }

}
