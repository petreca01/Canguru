package br.com.taqtile.android.app.presentation.maternityownermaternity;

import android.support.v4.app.Fragment;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by duh on 3/12/18.
 */

public class MaternityOwnerMaternityActivity extends TemplateBackActivity {

  private MaternityOwnerMaternityFragment fragment;

  @Override
  public Fragment getFragment() {

    MaternityOwnerMaternityFragment maternityOwnerMaternityFragment = (MaternityOwnerMaternityFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());

    if (maternityOwnerMaternityFragment == null) {
      maternityOwnerMaternityFragment = MaternityOwnerMaternityFragment.newInstance();
    }
    fragment = maternityOwnerMaternityFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {

    MaternityOwnerMaternityPresenter maternityOwnerMaternityPresenter = new MaternityOwnerMaternityPresenter();
    fragment.setPresenter(maternityOwnerMaternityPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return maternityOwnerMaternityPresenter;

  }

}
