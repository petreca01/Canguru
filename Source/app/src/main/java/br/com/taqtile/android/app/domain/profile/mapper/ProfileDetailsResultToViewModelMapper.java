package br.com.taqtile.android.app.domain.profile.mapper;

import br.com.taqtile.android.app.domain.profile.models.ProfileDetailsResult;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;

/**
 * Created by taqtile on 4/27/17.
 */

public class ProfileDetailsResultToViewModelMapper {

  public static ProfileDetailsViewModel perform(ProfileDetailsResult profileDetailsResult) {

    return new ProfileDetailsViewModel(profileDetailsResult.getId(),
      profileDetailsResult.getUserType(), profileDetailsResult.getName(),
      profileDetailsResult.getBabyName(), profileDetailsResult.getAbout(),
      profileDetailsResult.getProfilePicture(), profileDetailsResult.getCoverPicture(),
      profileDetailsResult.getPregnancyWeeks(), profileDetailsResult.getPregnancyDays(),
      profileDetailsResult.isFollowing(), profileDetailsResult.getCity(),
      profileDetailsResult.getState(), profileDetailsResult.isLocationPrivacyAgreed(),
      profileDetailsResult.getCreatedAt(),
      profileDetailsResult.getUpdatedAt());
  }

}
