package br.com.taqtile.android.app.presentation.channeldetail;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.ChannelDetailsPaginatedAdapter;
import br.com.taqtile.android.app.listings.cells.ChannelCard;
import br.com.taqtile.android.app.listings.cells.ChannelDescriptionCell;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelDetailsHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.helpers.HeterogeneousRecyclerViewItemDecoration;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.channeldetail.ChannelDetailsActivity.CHANNEL_ID_TAG;
import static br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment.COMMENT_POST;
import static br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment.DELETE_COMMENT_POST;
import static br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment.DISLIKE_POST;
import static br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment.LIKE_POST;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_POST_ID_KEY;


/**
 * Created by taqtile on 4/5/17.
 */

public class ChannelDetailsFragment extends AppBaseFragment implements
  MenuItem.OnMenuItemClickListener,
  ChannelDescriptionCell.Listener, ChannelCard.ChannelCardListener {

  @BindView(R.id.fragment_channel_detail_recycler_view)
  RecyclerView channelDetailsRecyclerView;

  private RecyclerView.OnScrollListener onScrollListener;
  private LinearLayoutManager linearLayoutManager;
  private boolean loadMoreTriggered;
  private boolean hasMorePosts;

  private NavigationManager navigationManager;
  private ChannelDetailsContract.Presenter presenter;
  private CustomToolbar customToolbar;

  private List<ListingsViewModel> channelDetailsModels;
  private ChannelDetailsPaginatedAdapter channelDetailsPaginatedAdapter;

  private Integer channelId;

  public static ChannelDetailsFragment newInstance() {
    return new ChannelDetailsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_channel_detail, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    channelId = getActivity().getIntent().getIntExtra(CHANNEL_ID_TAG, 0);
    setupRecyclerView();
    setupToolbar();
    fetchChannelDetailsModelsWithHeader();
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  private void setupListeners(ChannelDetailsPaginatedAdapter channelDetailsPaginatedAdapter) {
    setupRecyclerViewListener();
  }

  public void setupRecyclerViewListener() {
    loadMoreTriggered = false;
    hasMorePosts = true;

    onScrollListener = new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (hasRecyclerViewReachedTheEnd() && !loadMoreTriggered && hasMorePosts) {
          loadMoreTriggered = true;
          fetchMoreChannelPosts();
        }
      }
    };
    channelDetailsRecyclerView.addOnScrollListener(onScrollListener);
  }

  private boolean hasRecyclerViewReachedTheEnd() {
    int visibleItemCount = linearLayoutManager.getChildCount();
    int totalItemCount = linearLayoutManager.getItemCount();
    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
    return pastVisibleItems + visibleItemCount >= totalItemCount && pastVisibleItems > 0;
  }

  private ChannelDetailsPaginatedAdapter setupAdapter() {
    channelDetailsModels = new ArrayList<>();
    ChannelDetailsPaginatedAdapter channelDetailsPaginatedAdapter = new ChannelDetailsPaginatedAdapter(getContext(),
      channelDetailsModels, this, this);
    setupListeners(channelDetailsPaginatedAdapter);
    return channelDetailsPaginatedAdapter;
  }

  private void setupRecyclerView() {
    channelDetailsPaginatedAdapter = setupAdapter();
    linearLayoutManager = new LinearLayoutManager(getContext());
    channelDetailsRecyclerView.setLayoutManager(linearLayoutManager);
    channelDetailsRecyclerView.addItemDecoration(
      new HeterogeneousRecyclerViewItemDecoration(
        (int) getResources().getDimension(R.dimen.margin_medium)) {
      });
    channelDetailsRecyclerView.setAdapter(channelDetailsPaginatedAdapter);
    setupRecyclerviewScrollListener();
  }

  private void setupRecyclerviewScrollListener() {
    channelDetailsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (customToolbar == null) {
          return;
        }
        setupToolbarTitleBehavior();
      }
    });
  }

  private void setupToolbarTitleBehavior() {
    if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
      if (channelDetailsModels.get(0) instanceof ChannelDetailsHeaderViewModel) {
        customToolbar.setTitle(((ChannelDetailsHeaderViewModel) channelDetailsModels.get(0)).getChannelName());
      }
    } else {
      customToolbar.hideTitle();
    }
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.hideTitle();
      ViewCompat.setElevation(getToolbar(), 0);
    }
  }

  public void setPresenter(ChannelDetailsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void fetchChannelDetailsModelsWithHeader() {
    showLoading();
    Subscription channelDetailSubscription = presenter.fetchChannelDetailsModels(channelId, true)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchChannelDetailsModelsWithHeaderSuccess, this::onFetchModelsFailure);
    compositeSubscription.add(channelDetailSubscription);
  }

  private void onFetchChannelDetailsModelsWithHeaderSuccess(List<ListingsViewModel> channelDetailsModels) {
    setChannelDetails(channelDetailsModels);
    setMorePosts(channelDetailsModels);
    channelDetailsPaginatedAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
    hideLoading();
  }

  private void setChannelDetails(List<ListingsViewModel> channelDetailsModels) {
    this.channelDetailsModels.clear();
    this.channelDetailsModels.addAll(channelDetailsModels);
  }

  private void setMorePosts(List<ListingsViewModel> channelDetailsModels) {
    if (channelDetailsModels != null && channelDetailsModels.size() > 0 && getTotalPosts() > 0) {
      hasMorePosts = true;
      channelDetailsPaginatedAdapter.setFooterButtonVisible(true);
    } else {
      hasMorePosts = false;
      channelDetailsPaginatedAdapter.setFooterButtonVisible(false);
    }
  }

  private int getTotalPosts() {
    if (channelDetailsModels != null) {
      List<ListingsViewModel> list = StreamSupport.stream(channelDetailsModels)
        .filter(listingsViewModel -> (listingsViewModel instanceof ChannelDetailsHeaderImplViewModel))
        .collect(Collectors.toList());

      if (list.size() > 0) {
        return ((ChannelDetailsHeaderImplViewModel) list.get(0)).getTotalPosts();
      }

    }
    return 0;
  }

  private void fetchMoreChannelPosts() {
    Subscription channelDetailSubscription = presenter.fetchChannelDetailsModels(channelId, false)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchMoreChannelPostsSuccess,
        this::onFetchModelsFailure,
        this::hideLoading);

    compositeSubscription.add(channelDetailSubscription);
  }


  private void onFetchMoreChannelPostsSuccess(List<ListingsViewModel> channelDetailsModels) {
    setMoreChannelPosts(channelDetailsModels);
    setMorePosts(channelDetailsModels);
    channelDetailsPaginatedAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
  }

  private void setMoreChannelPosts(List<ListingsViewModel> channelDetailsModels) {
    this.channelDetailsModels.addAll(channelDetailsModels);
  }

  private void onFetchModelsFailure(Throwable throwable) {
    channelDetailsPaginatedAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
    setMorePosts(null);
    if (channelDetailsModels != null && channelDetailsModels.isEmpty()) {
      handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    } else {
      if (throwable.getMessage() != null) {
        showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
      } else {
        showSnackbar(R.string.error_generic_message, Snackbar.LENGTH_SHORT);
      }
    }
  }

  @Override
  public boolean onMenuItemClick(MenuItem menuItem) {
    if (menuItem.getItemId() == R.id.channel_detail_search) {
      navigationManager.showSearchUI();
    }
    return false;
  }

  @Override
  public void onProfileRegionClick(Integer profileId, Integer postId, int type) {

  }

  public void onCardClick(Integer id, int type) {
    navigationManager.showChannelPostDetailUI(null, id, true);
  }

  @Override
  public void onLikeClick(boolean isCounterActivated, Integer id, int type, CounterWithDrawable counterWithDrawable) {
    Subscription likePostSubscription = presenter.likeItem(Integer.valueOf(id), null, isCounterActivated)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onItemLikedSuccess,
        failure -> this.onItemLikedFailure(failure, counterWithDrawable)
      );
    compositeSubscription.add(likePostSubscription);
  }

  private void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels) {
    // do nothing
  }

  private void onItemLikedFailure(Throwable throwable, CounterWithDrawable counterWithDrawable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    counterWithDrawable.unpaintLike();
  }

  @Override
  public void onCommentsCounterClick(Integer id, int type) {
    navigationManager.showChannelPostDetailUI(null, id, true);
  }

  @Override
  public void onOverflowMenuItemSelected(String item) {
  }

  @Override
  public void onBodyClick(Integer id, int type) {
    navigationManager.showChannelPostDetailUI(null, id, true);
  }

  @Override
  public void onFollowButtonClick(int type) {
    executeFollowObservable(channelId, true);
  }

  @Override
  public void onActiveFollowButtonClick(int type) {
    executeFollowObservable(channelId, false);
  }

  private void onFollowUnfollowChannelSuccess(boolean isFollowing) {
    if (channelDetailsModels.get(0) instanceof ChannelDetailsHeaderViewModel) {
      ((ChannelDetailsHeaderViewModel) channelDetailsModels.get(0)).setIsFollowing(isFollowing);
      channelDetailsPaginatedAdapter.notifyDataSetChanged();
    } else {
      throw new ClassCastException("The first item of the channelDetailsModels should be the header!");
    }

  }

  private void onFollowUnfollowChannelFailure(Throwable error) {
    channelDetailsPaginatedAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
    showSnackbar(R.string.error_generic_message, Snackbar.LENGTH_SHORT);
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    hideLoading();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_channel_detail_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchChannelDetailsModelsWithHeader();
    hidePlaceholder();
  }

  //region Observables executer

  private void executeFollowObservable(int channelId, boolean isFollowing) {
    Subscription subscription = presenter.followChannel(channelId, isFollowing)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(emptyResult -> this.onFollowUnfollowChannelSuccess(isFollowing),
        this::onFollowUnfollowChannelFailure);

    compositeSubscription.add(subscription);
  }

  //endregion

  //region loading

  //endregion
}
