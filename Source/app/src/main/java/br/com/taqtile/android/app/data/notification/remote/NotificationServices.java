package br.com.taqtile.android.app.data.notification.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationCountRemoteResponse;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface NotificationServices {
    @POST("notifications/dashboard/count")
    Observable<Result<BaseRemoteResponse<NotificationCountRemoteResponse>>> count();

    @FormUrlEncoded
    @POST("usuario/notifications")
    Observable<Result<BaseRemoteResponse<List<NotificationItemRemoteResponse>>>> list(@FieldMap Map<String, String> listRequestModel);

    @POST("usuario/notifications")
    Observable<Result<BaseRemoteResponse<List<NotificationItemRemoteResponse>>>> list();


  @FormUrlEncoded
    @POST("usuario/notifications/set_visualized")
    Observable<Result<BaseRemoteResponse<Boolean>>> setVisualized(@FieldMap Map<String, String> setVisualizedRequestModel);
}
