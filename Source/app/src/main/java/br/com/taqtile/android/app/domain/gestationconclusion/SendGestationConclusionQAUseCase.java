package br.com.taqtile.android.app.domain.gestationconclusion;

import java.util.List;

import br.com.taqtile.android.app.data.gestationconclusion.GestationConclusionRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import rx.Observable;

/**
 * Created by taqtile on 7/11/17.
 */

public class SendGestationConclusionQAUseCase extends BaseUseCase<List<GestationConclusionQuestionItemResult>,
  AnswerQuestionnairesParams, GestationConclusionRepository> {

  public SendGestationConclusionQAUseCase(GestationConclusionRepository gestationConclusionRepository) {
    super(gestationConclusionRepository);
  }

  @Override
  public Observable<List<GestationConclusionQuestionItemResult>> execute(AnswerQuestionnairesParams
                                                        answerQuestionnairesParams) {
    return getRepository().answer(answerQuestionnairesParams);
  }
}
