package br.com.taqtile.android.app.presentation.medicalrecommendations;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.symptomshistory.SymptomsHistoryActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsActivity extends TemplateBackActivity {

  private MedicalRecommendationsFragment fragment;

  @Override
  public Fragment getFragment() {
    MedicalRecommendationsFragment medicalRecommendationsFragment =
      (MedicalRecommendationsFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (medicalRecommendationsFragment == null) {
      medicalRecommendationsFragment = MedicalRecommendationsFragment.newInstance();
    }
    fragment = medicalRecommendationsFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    MedicalRecommendationsPresenter medicalRecommendationsPresenter = new MedicalRecommendationsPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(medicalRecommendationsPresenter);

    return medicalRecommendationsPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, MedicalRecommendationsActivity.class);
    context.startActivity(intent);
  }
}
