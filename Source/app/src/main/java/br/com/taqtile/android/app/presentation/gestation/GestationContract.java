package br.com.taqtile.android.app.presentation.gestation;

import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public interface GestationContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> fetchUserData();

    Observable<WeeklyContentViewModel> fetchWeeklyContent(WeeklyContentParams weeklyContentParams);

  }

}
