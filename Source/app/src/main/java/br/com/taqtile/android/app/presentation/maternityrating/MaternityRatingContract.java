package br.com.taqtile.android.app.presentation.maternityrating;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/12/17.
 */

public interface MaternityRatingContract {

  interface Presenter extends BasePresenter {
    Observable<MaternityViewModel> rateMaternity(RateMaternityParams rateMaternityParams);
    Observable<List<MaternityCommentViewModel>> comment(MaternityCommentParams maternityCommentParams);
  }
}
