package br.com.taqtile.android.app.presentation.searchhealthcare;

import java.util.List;

import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public interface SearchHealthcareContract {

  interface Presenter extends BasePresenter {
    Observable<List<HealthOperatorViewModel>> fetchListHealthOperators();
    Observable<List<HealthOperatorViewModel>> fetchSearchHealthOperators(SearchHealthOperatorParams searchTerm);
  }

}
