package br.com.taqtile.android.app.domain.profile.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class UserProfileParams {
  private Integer profileId;

  public UserProfileParams(Integer profileId) {
    this.profileId = profileId;
  }

  public Integer getProfileId() {
    return profileId;
  }
}
