package br.com.taqtile.android.app.presentation.conditiondetails;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.condition.ConditionsActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.CLINICAL_CONDITIONS_DETAILS_REQUEST_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.CLINICAL_CONDITIONS_TAG;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionDetailsActivity extends TemplateBackActivity {

  ConditionDetailsFragment fragment;

  @Override
  public Fragment getFragment() {
    ConditionDetailsFragment conditionDetailsFragment = (ConditionDetailsFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (conditionDetailsFragment == null) {
      conditionDetailsFragment = ConditionDetailsFragment.newInstance();
    }
    fragment = conditionDetailsFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ConditionDetailsPresenter conditionDetailsPresenter = new ConditionDetailsPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(conditionDetailsPresenter);

    return conditionDetailsPresenter;
  }


  public static void navigate(Context context, ClinicalConditionItemViewModel clinicalConditionViewModel,
                              CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, ConditionDetailsActivity.class);
    intent.putExtra(CLINICAL_CONDITIONS_TAG, clinicalConditionViewModel);

    if (context instanceof ConditionsActivity) {
      ((ConditionsActivity) context).setResultListener(listener,
        CLINICAL_CONDITIONS_DETAILS_REQUEST_CODE);
      ((ConditionsActivity) context).startActivityForResult(intent,
        CLINICAL_CONDITIONS_DETAILS_REQUEST_CODE);
    }
  }
}
