package br.com.taqtile.android.app.presentation.healthcaredetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.listings.cells.HealthcareDetailCell;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;
import rx.subscriptions.Subscriptions;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareDetailsFragment extends AppBaseFragment {

  private static final String TAG = "HealthcareDetailsFragm";
  @BindView(R.id.fragment_healthcare_details_details)
  HealthcareDetailCell healthcareDetailCell;

  @BindView(R.id.fragment_healthcare_details_add_plan_button)
  TemplateButton addPlanButton;

  @BindView(R.id.fragment_healthcare_details_remove_plan_button)
  TemplateButton changePlanButton;

  private CustomToolbar customToolbar;

  private HealthcareDetailsContract.Presenter presenter;
  private NavigationManager navigationManager;

  private HealthOperatorViewModel healthOperatorViewModel;
  private boolean toAdd;

  public static HealthcareDetailsFragment newInstance() {
    return new HealthcareDetailsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_healthcare_details, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupButtons();
    setupHealthcareDetails();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());

      navigationManager.showAdditionalInfo();

      customToolbar.setTitle(getResources().getString(R.string.fragment_healthcare_details_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setupHealthcareDetails() {
    if (healthOperatorViewModel == null) {
      healthOperatorViewModel = new HealthOperatorViewModel();
    }

    if (toAdd) {
      showAddPlanButton();
      hideRemovePlanButton();
    } else {
      showRemovePlanButton();
      hideAddPlanButton();
    }
    healthcareDetailCell.setImage(healthOperatorViewModel.getLogo());
    healthcareDetailCell.setHeader(healthOperatorViewModel.getName());
    healthcareDetailCell.setBody(healthOperatorViewModel.getAbout());
  }

  private void setupButtons() {
    addPlanButton.setOnClickListener(this::onButtonClicked);
    changePlanButton.setOnClickListener(this::onChangePlanButtonClicked);
  }

  public void onButtonClicked(View view) {
    disableAddPlanButton();
    checkUserData();
  }

  private void checkUserData() {
//    Observable<Integer> observable = Observable.create(new Observable.OnSubscribeFunc<Integer>() {
//      @Override
//      public Subscription onSubscribe(Observer<? super Integer> observer) {
//        observer.onNext(3);
//        observer.onNext(2);
//        observer.onNext(1);
//        observer.onCompleted();
//
//        return Subscriptions.empty();
//      }
//    });


//    Subject<Object, Object> subject = new SerializedSubject<>(BehaviorSubject.create());
//    subject.onNext(new SomeEvent("opa"));

    Subscription checkUserDataSubscription = presenter.checkUserData("OPA")
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchUserDataSuccess, this::onAddHealthOperatorFailure,
        this::hideLoading);
    compositeSubscription.add(checkUserDataSubscription);
  }


  private void onFetchUserDataSuccess(UserViewModel userViewModel) {
    if (checkUserCPFAndTelephone(userViewModel)) {
      addHealthOperator();
    } else {
        navigationManager.showHealthOperatorsAdditionalInfoUI(this.healthOperatorViewModel, this::returningFromEditUserData);
    }
  }

  private void returningFromEditUserData(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      addHealthOperator();
    }
  }

  private void addHealthOperator() {
    if (healthOperatorViewModel.getId() != null) {
      Subscription addHealthOperatorSubscription = presenter.addHealthOperator(
        new EditUserDataParams(healthOperatorViewModel.getId()))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onAddHealthOperatorSuccess, this::onAddHealthOperatorFailure,
          this::hideLoading);
      compositeSubscription.add(addHealthOperatorSubscription);
    }
  }

  private void onAddHealthOperatorSuccess(UserViewModel userViewModel) {
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
  }

  private void onAddHealthOperatorFailure(Throwable throwable) {
    enableAddPlanButton();
    subscriptionFailure(throwable);
  }

  private boolean checkUserCPFAndTelephone(UserViewModel userViewModel) {
    return false;

//    return userViewModel.getCpf() != null && !userViewModel.getCpf().isEmpty() &&
//      userViewModel.getTelephone() != null && !userViewModel.getTelephone().isEmpty();
  }

  private void onChangePlanButtonClicked(View view) {
    navigationManager.showSearchHealthOperatorsUI(this::returningFromSearchHealthOperatorsUI);
  }

  private void returningFromSearchHealthOperatorsUI(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      Intent intent = new Intent();
      getActivity().setResult(responseCode, intent);
      getActivity().finish();
    }
  }

  private void subscriptionFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  public void setPresenter(HealthcareDetailsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setHealthOperatorViewModel(HealthOperatorViewModel healthOperatorViewModel) {
    this.healthOperatorViewModel = healthOperatorViewModel;
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  public void setToAdd(boolean toAdd) {
    this.toAdd = toAdd;
  }

  private void showAddPlanButton() {
    addPlanButton.setVisibility(View.VISIBLE);
  }

  private void enableAddPlanButton() {
    addPlanButton.setEnabled(true);
  }

  private void enableRemovePlanButton() {
    changePlanButton.setEnabled(true);
  }

  private void disableAddPlanButton() {
    addPlanButton.setEnabled(false);
  }

  private void disableRemovePlanButton() {
    changePlanButton.setEnabled(false);
  }

  private void hideAddPlanButton() {
    addPlanButton.setVisibility(View.GONE);
  }

  private void showRemovePlanButton() {
    changePlanButton.setVisibility(View.VISIBLE);
  }

  private void hideRemovePlanButton() {
    changePlanButton.setVisibility(View.GONE);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_healthcare_details),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
