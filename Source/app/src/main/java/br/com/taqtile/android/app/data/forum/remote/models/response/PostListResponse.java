package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 3/17/17.
 */

public class PostListResponse {
    @SerializedName("total_posts")
    private Integer totalPosts;

    @SerializedName("posts_in_page")
    private Integer postsInPage;

    @SerializedName("posts")
    private List<PostItemResponse> posts;

    @SerializedName("search")
    private String search;

    public Integer getTotalPosts() {
        return totalPosts;
    }

    public Integer getPostsInPage() {
        return postsInPage;
    }

    public List<PostItemResponse> getPosts() {
        return posts;
    }

    public String getSearch() {
        return search;
    }
}
