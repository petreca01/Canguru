package br.com.taqtile.android.app.data.gestationconclusion.remote;

import java.util.List;

import br.com.taqtile.android.app.data.birthPlan.remote.models.request.RemoveAnswerRemoteRequest;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.gestationconclusion.remote.models.GestationConclusionQuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest;
import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import rx.Observable;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionRemoteDataSource extends BaseRemoteDataSource {
  private Observable<GestationConclusionServices> services;

  public GestationConclusionRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(GestationConclusionServices.class);
  }

  public Observable<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>> listQuestionsAndAnswers(
    ViewModeRemoteRequest listQuestionsAndAnswersRequestModel) {
    return performRequest(services.flatMap(services -> services.listQuestionsAndAnswers(
      listQuestionsAndAnswersRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>> answer(
    AnswerQuestionnairesRemoteRequest answersRequestModel) {
    return performRequest(
      services.flatMap(services -> services.answer(answersRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>> removeAnswer(
    RemoveAnswerRemoteRequest removeAnswerRequestModel) {
    return performRequest(
      services.flatMap(services -> services.removeAnswer(removeAnswerRequestModel.getFormData())));
  }
}
