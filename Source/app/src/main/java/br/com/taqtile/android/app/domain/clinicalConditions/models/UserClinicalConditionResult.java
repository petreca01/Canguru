package br.com.taqtile.android.app.domain.clinicalConditions.models;

/**
 * Created by taqtile on 4/12/17.
 */

public class UserClinicalConditionResult {
  private Integer pregnancyConditionId;
  private String name;
  private String date;
  private Integer gestationAgeWeeks;
  private Integer gestationAgeDays;
  private Integer risk;

  public UserClinicalConditionResult(Integer pregnancyConditionId, String name, String date,
    Integer gestationAgeWeeks, Integer gestationAgeDays, Integer risk) {
    this.pregnancyConditionId = pregnancyConditionId;
    this.name = name;
    this.date = date;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.gestationAgeDays = gestationAgeDays;
    this.risk = risk;
  }

  public Integer getPregnancyConditionId() {
    return pregnancyConditionId;
  }

  public String getName() {
    return name;
  }

  public String getDate() {
    return date;
  }

  public Integer getGestationAgeWeeks() {
    return gestationAgeWeeks;
  }

  public Integer getGestationAgeDays() {
    return gestationAgeDays;
  }

  public Integer getRisk() {
    return risk;
  }
}
