package br.com.taqtile.android.app.presentation.gestationbabydevelopmentdetails;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.USER_DATA_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.WEEKLY_CONTENT_DATA_KEY;

/**
 * Created by taqtile on 12/05/17.
 */

public class GestationBabyDevelopmentDetailsActivity extends TemplateBackActivity {

  private GestationBabyDevelopmentDetailsFragment fragment;

  @Override
  public Fragment getFragment() {
    GestationBabyDevelopmentDetailsFragment gestationBabyDevelopmentDetailsFragment =
      (GestationBabyDevelopmentDetailsFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (gestationBabyDevelopmentDetailsFragment == null) {
      gestationBabyDevelopmentDetailsFragment = GestationBabyDevelopmentDetailsFragment.newInstance();
    }
    fragment = gestationBabyDevelopmentDetailsFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    GestationBabyDevelopmentDetailsPresenter gestationBabyDevelopmentDetailsPresenter =
      new GestationBabyDevelopmentDetailsPresenter();
    return gestationBabyDevelopmentDetailsPresenter;
  }

  public static void navigate(Context context, UserViewModel userData,
                              WeeklyContentViewModel currentWeeklyContent) {
    Intent intent = new Intent(context, GestationBabyDevelopmentDetailsActivity.class);
    intent.putExtra(USER_DATA_KEY, userData);
    intent.putExtra(WEEKLY_CONTENT_DATA_KEY, currentWeeklyContent);
    context.startActivity(intent);
  }
}
