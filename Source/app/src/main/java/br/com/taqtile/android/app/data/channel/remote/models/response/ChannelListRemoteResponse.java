package br.com.taqtile.android.app.data.channel.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.List;

/**
 * Created by taqtile on 3/17/17.
 */

public class ChannelListRemoteResponse {
  @SerializedName("section_name")
  private String sectionName;
  @SerializedName("total_channels")
  private Integer totalChannels;
  @SerializedName("channels_in_page")
  private Integer channelsInPage;
  @SerializedName("channels")
  private List<ChannelItemRemoteResponse> channels;
  @SerializedName("search")
  private String search;

  public String getSectionName(){ return sectionName; }

  public Integer getTotalChannels() {
    return totalChannels;
  }

  public Integer getChannelsInPage() {
    return channelsInPage;
  }

  public List<ChannelItemRemoteResponse> getChannels() {
    return channels;
  }

  public String getSearch() {
    return search;
  }
}
