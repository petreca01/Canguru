package br.com.taqtile.android.app.presentation.forgotpassword;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_EMAIL_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_KEY;

/**
 * Created by taqtile on 11/10/16.
 */

public class ForgotPasswordFragment extends AppBaseFragment implements ForgotPasswordContract.View,
  ProgressBarButton.ProgressBarButtonButtonListener {

  @BindView(R.id.fragment_forgot_password_email_form)
  EmailLabelEditTextCaption emailEditText;

  @BindView(R.id.fragment_forgot_password_send_button)
  ProgressBarButton sendButton;

  @BindView(R.id.fragment_forgot_password_content)
  LinearLayout contentLayout;

  private CustomToolbar toolbar;
  private ForgotPasswordContract.Presenter presenter;
  private NavigationManager navigationManager;

  private List<Validatable> errorForms;
  private Validatable[] forms;
  private String errorMessage;

  public ForgotPasswordFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_forgot_password, container, false);
    ButterKnife.bind(this, root);
    setupViews();
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupFormsList();
    setupToolbar();
    SoftKeyboardHelper.showKeyboardDelayed(getContext());
    setEmail();
  }

  private void setEmail() {
    emailEditText.setText(getActivity().getIntent().getExtras().getString(FORGOT_PASSWORD_EMAIL_KEY, ""));
  }

  public static ForgotPasswordFragment newInstance() {
    return new ForgotPasswordFragment();
  }

  private void setupViews() {
    sendButton.setButtonListener(this);
  }

  private void setupFormsList() {
    forms = new Validatable[]{emailEditText};
    errorForms = new ArrayList<Validatable>();
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (toolbar == null) {
        toolbar = getToolbar(CustomToolbar.class);
      }
      toolbar.setTitle(getResources().getString(R.string.fragment_forgot_password_title));
    }
  }


  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_forgot_password_placeholder;
  }

  @Override
  public void setPresenter(ForgotPasswordContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    sendButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    sendButton.showDefaultState();
  }

  public boolean locallyValidateForm() {
    boolean isValid = true;
    FocusHelper.clearViewFocus(emailEditText, contentLayout);
    errorForms = ValidateAndScrollHelper.validateForms(forms);
    if (!errorForms.isEmpty()) {
      showEmailFormError();
      isValid = false;
    }

    return isValid;
  }

  private void showEmailFormError() {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);
  }

  @Override
  public void onButtonClicked() {
    if (locallyValidateForm()) {
      showLoading();
      Subscription subscription = observeForgotPassword();
      compositeSubscription.add(subscription);
    }
  }

  private Subscription observeForgotPassword() {

    return presenter.recoverPassword(emailEditText.getText())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onPasswordRecoveringSuccess,
        this::onPasswordRecoveringFailed
      );
  }

  private void onPasswordRecoveringSuccess(ForgotPasswordResult forgotPasswordResult) {
    this.hideLoading();

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forgot_Password),
      getString(R.string.analytics_event_request),
      getString(R.string.analytics_label_user_requested_new_password)
    ));

    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    intent.putExtra(FORGOT_PASSWORD_KEY, forgotPasswordResult.getMessage());
    getActivity().finish();

  }

  private void onPasswordRecoveringFailed(Throwable error) {
    this.hideLoading();
    showSnackbar(error.getMessage(), Snackbar.LENGTH_LONG);
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms = new String[errorViews.size()];
    for (int i = 0; i < errorViews.size(); i++) {
      errorForms[i] = getResources().getString(((CustomLabelEditTextCaption) errorViews.get(i).getView()).getInputPlaceholderTextRes());
    }
    return StringListFormatterHelper.buildStringList(errorForms);
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forgot_Password),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
