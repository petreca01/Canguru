package br.com.taqtile.android.app.data.city.geocondingAPI.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 6/5/17.
 */

public class AddressComponentGeocodingAPIResponse {

  @SerializedName("long_name")
  private String longName;
  @SerializedName("short_name")
  private String shortName;
  @SerializedName("types")
  private List<String> types;

  public String getLongName() {
    return longName;
  }

  public String getShortName() {
    return shortName;
  }

  public List<String> getTypes() {
    return types;
  }
}
