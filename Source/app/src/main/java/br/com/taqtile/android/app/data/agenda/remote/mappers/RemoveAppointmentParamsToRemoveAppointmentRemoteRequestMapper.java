package br.com.taqtile.android.app.data.agenda.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.request.RemoveAppointmentRemoteRequest;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;

/**
 * Created by taqtile on 4/10/17.
 */

public class RemoveAppointmentParamsToRemoveAppointmentRemoteRequestMapper {
  public static RemoveAppointmentRemoteRequest perform(RemoveAppointmentParams params) {
    return new RemoveAppointmentRemoteRequest(params.getAppointmentId());
  }
}
