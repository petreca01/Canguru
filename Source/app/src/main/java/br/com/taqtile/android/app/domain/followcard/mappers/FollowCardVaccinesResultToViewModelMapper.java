package br.com.taqtile.android.app.domain.followcard.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.followcard.models.FollowCardVaccinesResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardVaccinesViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardVaccinesResultToViewModelMapper {

  public static List<FollowCardVaccinesViewModel> perform(
    List<FollowCardVaccinesResult> followCardVaccinesResult) {
    return StreamSupport.stream(followCardVaccinesResult)
      .filter(Objects::nonNull)
      .map(FollowCardVaccinesResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardVaccinesViewModel map(
    FollowCardVaccinesResult followCardVaccinesResult) {
    return new FollowCardVaccinesViewModel(followCardVaccinesResult.getId(),
      followCardVaccinesResult.getName(), followCardVaccinesResult.getResult(),
      followCardVaccinesResult.getCreatedAt());
  }
}
