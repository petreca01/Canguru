package br.com.taqtile.android.app.presentation.additionalinfo.model;

import br.com.taqtile.android.app.presentation.common.user.UserViewModel;

/**
 * Created by taqtile on 4/26/17.
 */

public class AdditionalInfoUserViewModel extends UserViewModel {

  public String healthOperatorIcon;
  public String healthOperatorAbout;
  public Boolean healthOperatorIsPartner;
  public String healthOperatorDocumentType;

  public AdditionalInfoUserViewModel(String email, String telephone, String birthDate,
                                     String cpf, String zipCode, Integer operatorId,
                                     String operatorName, Boolean locationPolicyAgreed) {
    super(email, telephone, birthDate, cpf, zipCode, operatorId, operatorName, locationPolicyAgreed);
  }

  public AdditionalInfoUserViewModel(String email, String telephone, String birthDate,
                                     String cpf, String zipCode, Integer operatorId,
                                     String operatorName, Boolean locationPolicyAgreed,
                                     String healthOperatorIcon,
                                     String healthOperatorAbout,
                                     Boolean healthOperatorIsPartner,
                                     String healthOperatorDocumentType) {
    super(email, telephone, birthDate, cpf, zipCode, operatorId, operatorName, locationPolicyAgreed);
    this.healthOperatorIcon = healthOperatorIcon;
    this.healthOperatorAbout = healthOperatorAbout;
    this.healthOperatorIsPartner = healthOperatorIsPartner;
    this.healthOperatorDocumentType = healthOperatorDocumentType;
  }

  public Boolean isPartner() {
    return healthOperatorIsPartner;
  }

  public String getHealthOperatorIcon() {
    return healthOperatorIcon;
  }

  public String getHealthOperatorAbout() {
    return healthOperatorAbout;
  }

  public String getHealthOperatorDocumentType() {
    return healthOperatorDocumentType;
  }
}
