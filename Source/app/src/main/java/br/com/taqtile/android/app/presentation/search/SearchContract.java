package br.com.taqtile.android.app.presentation.search;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface SearchContract {

  interface Presenter extends BasePresenter {
    Observable<List<ForumListingViewModel>> search(String searchKeyword, boolean reload);
  }

}
