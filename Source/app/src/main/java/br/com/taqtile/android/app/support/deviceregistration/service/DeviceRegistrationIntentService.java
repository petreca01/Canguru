package br.com.taqtile.android.app.support.deviceregistration.service;

import br.com.taqtile.android.app.support.Constants;
import br.com.taqtile.android.app.support.Injection;
import br.com.taqtile.android.app.support.deviceregistration.controller.DeviceRegistrationController;
import br.com.taqtile.android.cleanbase.push.deviceregister.BaseDeviceRegistrationContract;
import br.com.taqtile.android.cleanbase.push.deviceregister.BaseDeviceRegistrationIntentService;

/**
 * Created by taqtile on 7/11/16.
 */

public class DeviceRegistrationIntentService extends BaseDeviceRegistrationIntentService {

  public DeviceRegistrationIntentService() {

  }

  @Override
  public <T extends BaseDeviceRegistrationContract.Controller> T getDeviceRegistrationController() {
    return (T) new DeviceRegistrationController(
      Injection.provideSaveDeviceTokenUseCase()
    );
  }

  @Override
  public String getGCMSenderId() {
    Constants constants = new Constants();
    return constants.getSenderId();
  }
}
