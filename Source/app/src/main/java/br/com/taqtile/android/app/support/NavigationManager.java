package br.com.taqtile.android.app.support;

import android.os.Bundle;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.cleanbase.presentation.view.BaseNavigationManager;

/**
 * Created by taqtile on 7/5/16.
 */

public interface NavigationManager extends BaseNavigationManager {

  void showChannelDetailUI(int id, CustomNavigationResultListener listener);

  void showSearchHealthOperatorsUI(CustomNavigationResultListener listener);

  void showHealthOperatorsAdditionalInfoUI(HealthOperatorViewModel healthOperatorViewModel, CustomNavigationResultListener listener);

  void showHealthOperatorsDetailUI(HealthOperatorViewModel healthOperatorViewModel, boolean toAdd, CustomNavigationResultListener listener);

  void showSignInUI(CustomNavigationResultListener listener);

  void showPublicProfileUI(Integer id, CustomNavigationResultListener listener);

  void showSignUpUI(CustomNavigationResultListener listener);

  void showSignUpUI(CustomNavigationResultListener listener, String email, String password);

  void showForgotPasswordUI(CustomNavigationResultListener listener, String email);

  void showInitialLoginUI();

  void showChangePasswordUI();

  void showSearchUI();

  void showChannelPostDetailUI(CustomNavigationResultListener listener, Integer id, Boolean fromChannelDetail);

  void showCommunityPostDetailUI(CustomNavigationResultListener listener, Integer id, Boolean fromProfile);

  void showChannelListUI();

  void showAboutCanguruUI();

  void showPrivacyPolicyUI();

  void showUseTermsUI();

  void showProfileMenu();

  void showAdditionalInfo();

  void showConnectedProfessionals();

  void showPostCommunity(CustomNavigationResultListener listener);

  void showPersonalRisk();

  void showPersonalRiskForm(String riskGroup);

  void showManagedProfessionalsUI(Bundle bundle);

  void showEditAdditionalInfoUserData(AdditionalInfoUserViewModel additionalInfoUserData, CustomNavigationResultListener listener);
  void showSymptomGuideUI();

  void showSymptomDetailUI(CustomNavigationResultListener listener, Bundle bundle);

  void showSymptomDetailUI(int parentSymptomId, int symptomId);

  void showSymptomEditUI(CustomNavigationResultListener listener);

  void showSymptomHistoryUI();

  void showAppointmentDetailUI(CustomNavigationResultListener listener, AgendaItemViewModel agendaItemViewModel);

  void showAppointmentEditScheduleUI(CustomNavigationResultListener listener, AgendaItemViewModel agendaItemViewModel);

  void showAppointmentCreateUI(CustomNavigationResultListener listener);

  void showClinicalConditionsUI();

  void showMedicalRecommendations();

  void showMedicalRecommendationDetail(Bundle bundle);

  void showBirthPlanUI(CustomNavigationResultListener listener);

  void showBirthPlanQuestionnaireUI(List<QuestionViewModel> questionnaire, String index);

  void showMaternityCitySearch(CustomNavigationResultListener listener);

  void showPrenatalCard();

  void showMaternityDetail(int id);

  void showMaternityRating(MaternityViewModel maternityViewModel, CustomNavigationResultListener listener);

  void showGestationEditUI(UserViewModel userData, CustomNavigationResultListener listener);

  void showWeeklyContentDetailsUI(UserViewModel userData, WeeklyContentViewModel currentWeeklyContent);

  void showClinicalConditionsDetailsUI(ClinicalConditionItemViewModel clinicalConditionItemViewModel,
                                       CustomNavigationResultListener listener);

  void showGestationConclusionUI(CustomNavigationResultListener listener);

  void showProbableBirthdateUI();

  void showMaternityNotFoundUI();
}
