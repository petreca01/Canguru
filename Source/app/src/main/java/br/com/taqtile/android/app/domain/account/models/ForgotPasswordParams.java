package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordParams {

    private String email;

    public ForgotPasswordParams(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
