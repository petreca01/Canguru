package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class DeleteCommentParams extends MaternityParams {
  private Integer commentId;

  public DeleteCommentParams(Integer commentId, Integer maternityId) {
    super(maternityId);
    this.commentId = commentId;
  }

  public Integer getCommentId() {
    return commentId;
  }
}
