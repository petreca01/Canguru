package br.com.taqtile.android.app.presentation.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.BuildConfig;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.presentation.debugpanel.DebugPanelActivity;

/**
 * Created by taqtile on 6/6/16.
 */

public abstract class TemplateBackActivity
  extends br.com.taqtile.android.cleanbase.presentation.activity.BackActivity {

  private Toolbar toolbar;
  private int count = 0;
  private long startMillis = 0;
  private ProgressBar progressBarLoading;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if(BuildConfig.BUILD_TYPE.equals("debug")) {
      setupDebugPanelOnToolbar();
    }
    setupProgressBar();
  }

  private void setupProgressBar()
  {
    progressBarLoading = (ProgressBar) findViewById(R.id.view_loading_progress_bar);
    progressBarLoading.getIndeterminateDrawable().setColorFilter(
      ContextCompat.getColor(this, R.color.color_primary),
      android.graphics.PorterDuff.Mode.SRC_IN);
  }

  private void setupDebugPanelOnToolbar() {
    toolbar = (Toolbar) findViewById(R.id.activity_base_back_toolbar);
    toolbar.setOnTouchListener(this::onToolbarHold);
  }

  private boolean onToolbarHold(View view, MotionEvent motionEvent) {

    int eventaction = motionEvent.getAction();
    if (eventaction == MotionEvent.ACTION_UP) {
      long time = System.currentTimeMillis();
      if (startMillis == 0 || (time - startMillis > 2000)) {
        startMillis = time;
        count = 1;
      }else {
        count++;
      }
      if (count == 5) {
        DebugPanelActivity.navigate(this);
      }
      return true;
    }
    return false;
  }

  @Override
  public int getActivityLayoutId() {
    return R.layout.activity_base_back;
  }

  @Override
  public int getFragmentContainerId() {
    return R.id.activity_base_back_contentFrame;
  }

  @Override
  public int getToolbarId() {
    return R.id.activity_base_back_toolbar;
  }

  @Override
  public int getAppBarLayoutId() {
    return 0;
  }

  @Override
  public int getLoadingViewContainerId() {
    return R.id.loading_view_root;
  }

  @Override
  public int getBackButtonIcon() {
    return R.drawable.ic_arrow_back;
  }

  @Override
  public int getBackButtonColorRes() {
    return R.color.color_white;
  }

  @Override
  public int getFABId() {
    return 0;
  }
}
