package br.com.taqtile.android.app.presentation.staticscreen;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 18/04/17.
 */

public class StaticScreenPresenter implements BasePresenter {
  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }
}
