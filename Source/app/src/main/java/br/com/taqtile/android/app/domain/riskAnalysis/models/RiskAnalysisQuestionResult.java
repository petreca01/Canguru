package br.com.taqtile.android.app.domain.riskAnalysis.models;

import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;

/**
 * Created by felipesabino on 5/17/17.
 */

public class RiskAnalysisQuestionResult {
  private Integer totalQuestionsForGroup;
  private Integer currentQuestionIndex;
  private Boolean hasNextQuestion;
  private QuestionItemResult question;

  public Integer getTotalQuestionsForGroup() {
    return totalQuestionsForGroup;
  }

  public void setTotalQuestionsForGroup(Integer totalQuestionsForGroup) {
    this.totalQuestionsForGroup = totalQuestionsForGroup;
  }

  public Integer getCurrentQuestionIndex() {
    return currentQuestionIndex;
  }

  public void setCurrentQuestionIndex(Integer currentQuestionIndex) {
    this.currentQuestionIndex = currentQuestionIndex;
  }

  public Boolean getHasNextQuestion() {
    return hasNextQuestion;
  }

  public void setHasNextQuestion(Boolean hasNextQuestion) {
    this.hasNextQuestion = hasNextQuestion;
  }

  public QuestionItemResult getQuestion() {
    return question;
  }

  public void setQuestion(QuestionItemResult question) {
    this.question = question;
  }
}


