package br.com.taqtile.android.app.domain.profile.mapper;

import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;

/**
 * Created by taqtile on 4/27/17.
 */

public class UpdateProfileDetailsResultToViewModelMapper {

  public static ProfileDetailsViewModel perform(
    UpdateProfileDetailsResult updateProfileDetailsResult) {

    return new ProfileDetailsViewModel(updateProfileDetailsResult.getId(),
      updateProfileDetailsResult.getName(), updateProfileDetailsResult.getBabyName(),
      updateProfileDetailsResult.getAbout(), updateProfileDetailsResult.getProfilePicture(),
      updateProfileDetailsResult.getCoverPicture(), updateProfileDetailsResult.getPregnancyWeeks(),
      updateProfileDetailsResult.getPregnancyDays(), updateProfileDetailsResult.getCity(),
      updateProfileDetailsResult.getState(), updateProfileDetailsResult.getLocationPrivacyAgreed(),
      updateProfileDetailsResult.getCreatedAt(), updateProfileDetailsResult.getUpdatedAt());
  }

}
