package br.com.taqtile.android.app.data.medicalRecommendations.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class MedicalRecommendationItemRemoteResponse {
    @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("doctor_id")
    private Integer doctorId;

    @SerializedName("doctor_name")
    private String doctorName;

    @SerializedName("name")
    private String name;

    @SerializedName("recommended_at")
    private String recommendedAt;

    @SerializedName("acknowledged_at")
    private String acknowledgedAt;

    @SerializedName("content")
    private String content;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getName() {
        return name;
    }

    public String getRecommendedAt() {
        return recommendedAt;
    }

    public String getAcknowledgedAt() {
        return acknowledgedAt;
    }

    public String getContent() {
        return content;
    }
}
