package br.com.taqtile.android.app.domain.followcard.mappers;

import br.com.taqtile.android.app.domain.followcard.models.FollowCardResult;
import br.com.taqtile.android.app.domain.followcard.models.GestationHistoryResult;
import br.com.taqtile.android.app.domain.followcard.models.RisksResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.GestationHistoryViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.RisksViewModel;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardResultToViewModelMapper {

  public static FollowCardViewModel perform(FollowCardResult followCardResult) {
    return new FollowCardViewModel(getModelName(followCardResult), getModelAge(followCardResult),
      FollowCardResultToViewModelMapper.map(followCardResult.getGpa()),
      FollowCardResultToViewModelMapper.map(followCardResult.getRisks()));
  }

  private static GestationHistoryViewModel map(GestationHistoryResult gestationHistoryResult) {
    return new GestationHistoryViewModel(gestationHistoryResult.getPregnancy(),
      gestationHistoryResult.getNormalDelivery(), gestationHistoryResult.getCaesarean(),
      gestationHistoryResult.getMiscarriage());
  }

  private static RisksViewModel map(RisksResult risksResult) {
    return new RisksViewModel(risksResult.getAlert(), risksResult.getRisk());
  }

  private static String getModelName(FollowCardResult followCardResult) {
    return followCardResult != null && !followCardResult.getName().isEmpty() ? followCardResult.getName() : "-";
  }

  private static String getModelAge(FollowCardResult followCardResult) {
    return followCardResult != null && followCardResult.getAge() != null &&
      !followCardResult.getAge().isEmpty() ? followCardResult.getAge() : "-";
  }

}
