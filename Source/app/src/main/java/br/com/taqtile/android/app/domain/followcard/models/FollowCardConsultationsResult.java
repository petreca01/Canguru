package br.com.taqtile.android.app.domain.followcard.models;


/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardConsultationsResult {

  private Integer id;
  private Integer doctorId;
  private Integer patientId;
  private Integer usuarioId;
  private String date;
  private Integer pregnancyWeeks;
  private Integer pregnancyDays;
  private Integer height;
  private Integer weight;
  private String reason;
  private Integer bloodPressureSystolic;
  private Integer bloodPressureDiastolic;
  private Integer usingFerricSulfate;
  private Integer usingFolicAcid;
  private Integer uterineHeight;
  private String fetalPresentation;
  private Integer fetalHeartRate;
  private Integer fetalMovement;
  private Integer edema;
  private String toque;
  private String createdAt;
  private String updatedAt;
  private String doctorName;

  public FollowCardConsultationsResult(Integer id, Integer doctorId, Integer patientId,
                                       Integer usuarioId, String date, Integer pregnancyWeeks,
                                       Integer pregnancyDays, Integer height, Integer weight,
                                       String reason, Integer bloodPressureSystolic,
                                       Integer bloodPressureDiastolic, Integer usingFerricSulfate,
                                       Integer usingFolicAcid, Integer uterineHeight,
                                       String fetalPresentation, Integer fetalHeartRate,
                                       Integer fetalMovement, Integer edema, String toque,
                                       String createdAt, String updatedAt, String doctorName) {
    this.id = id;
    this.doctorId = doctorId;
    this.patientId = patientId;
    this.usuarioId = usuarioId;
    this.date = date;
    this.pregnancyWeeks = pregnancyWeeks;
    this.pregnancyDays = pregnancyDays;
    this.height = height;
    this.weight = weight;
    this.reason = reason;
    this.bloodPressureSystolic = bloodPressureSystolic;
    this.bloodPressureDiastolic = bloodPressureDiastolic;
    this.usingFerricSulfate = usingFerricSulfate;
    this.usingFolicAcid = usingFolicAcid;
    this.uterineHeight = uterineHeight;
    this.fetalPresentation = fetalPresentation;
    this.fetalHeartRate = fetalHeartRate;
    this.fetalMovement = fetalMovement;
    this.edema = edema;
    this.toque = toque;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.doctorName = doctorName;
  }

  public Integer getId() {
    return id;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public Integer getUsuarioId() {
    return usuarioId;
  }

  public String getDate() {
    return date;
  }

  public Integer getPregnancyWeeks() {
    return pregnancyWeeks;
  }

  public Integer getPregnancyDays() {
    return pregnancyDays;
  }

  public Integer getHeight() {
    return height;
  }

  public Integer getWeight() {
    return weight;
  }

  public String getReason() {
    return reason;
  }

  public Integer getBloodPressureSystolic() {
    return bloodPressureSystolic;
  }

  public Integer getBloodPressureDiastolic() {
    return bloodPressureDiastolic;
  }

  public Integer getUsingFerricSulfate() {
    return usingFerricSulfate;
  }

  public Integer getUsingFolicAcid() {
    return usingFolicAcid;
  }

  public Integer getUterineHeight() {
    return uterineHeight;
  }

  public String getFetalPresentation() {
    return fetalPresentation;
  }

  public Integer getFetalHeartRate() {
    return fetalHeartRate;
  }

  public Integer getFetalMovement() {
    return fetalMovement;
  }

  public Integer getEdema() {
    return edema;
  }

  public String getToque() {
    return toque;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getDoctorName() {
    return doctorName;
  }
}
