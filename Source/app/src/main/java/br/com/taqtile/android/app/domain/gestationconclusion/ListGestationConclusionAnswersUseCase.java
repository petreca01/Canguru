package br.com.taqtile.android.app.domain.gestationconclusion;

import java.util.List;

import br.com.taqtile.android.app.data.gestationconclusion.GestationConclusionRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import rx.Observable;

/**
 * Created by taqtile on 7/11/17.
 */

public class ListGestationConclusionAnswersUseCase extends
  BaseUseCase<List<GestationConclusionQuestionItemResult>, Void, GestationConclusionRepository> {

  public ListGestationConclusionAnswersUseCase(GestationConclusionRepository gestationConclusionRepository) {
    super(gestationConclusionRepository);
  }

  @Override
  public Observable<List<GestationConclusionQuestionItemResult>> execute(Void aVoid) {
      return getRepository()
        .listQuestionsAndAnswers(new ViewModeParams());

  }

}
