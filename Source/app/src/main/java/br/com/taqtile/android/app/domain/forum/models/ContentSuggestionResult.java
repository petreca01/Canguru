package br.com.taqtile.android.app.domain.forum.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionResult {

  private Integer contentsInPage;
  private List<ContentSuggestionItemResult> contents;
  private Integer totalContents;

  public ContentSuggestionResult(Integer contentsInPage, List<ContentSuggestionItemResult> contents, Integer totalContents) {
    this.contentsInPage = contentsInPage;
    this.contents = contents;
    this.totalContents = totalContents;
  }

  public Integer getContentsInPage() {
    return contentsInPage;
  }

  public List<ContentSuggestionItemResult> getContents() {
    return contents;
  }

  public Integer getTotalContents() {
    return totalContents;
  }

}
