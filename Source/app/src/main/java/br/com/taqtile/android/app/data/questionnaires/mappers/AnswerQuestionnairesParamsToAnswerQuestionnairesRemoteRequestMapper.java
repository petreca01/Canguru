package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest;
import br.com.taqtile.android.app.data.questionnaires.models.AnswerRemoteRequest;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import java.util.List;

/**
 * Created by taqtile on 07/04/17.
 */

public class AnswerQuestionnairesParamsToAnswerQuestionnairesRemoteRequestMapper {

  public static AnswerQuestionnairesRemoteRequest perform(
    AnswerQuestionnairesParams answerQuestionnairesParams) {
    List<AnswerRemoteRequest> answers = AnswerParamsListToAnswerRemoteRequestListMapper.perform(
      answerQuestionnairesParams.getAnswers());

    return new AnswerQuestionnairesRemoteRequest(answers, answerQuestionnairesParams.getFinishPregnancy());
  }
}
