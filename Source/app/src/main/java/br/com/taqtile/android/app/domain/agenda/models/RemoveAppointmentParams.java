package br.com.taqtile.android.app.domain.agenda.models;

/**
 * Created by taqtile on 4/10/17.
 */

public class RemoveAppointmentParams {
  private Integer appointmentId;

  public RemoveAppointmentParams(Integer appointmentId) {
    this.appointmentId = appointmentId;
  }

  public Integer getAppointmentId() {
    return appointmentId;
  }
}
