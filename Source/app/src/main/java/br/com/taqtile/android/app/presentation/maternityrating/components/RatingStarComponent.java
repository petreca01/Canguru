package br.com.taqtile.android.app.presentation.maternityrating.components;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import br.com.taqtile.android.app.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/17/17.
 */

public class RatingStarComponent extends FrameLayout {

  @DrawableRes
  private static final int starFullDrawable = R.drawable.ic_star_full;
  @DrawableRes
  private static final int starEmptyDrawable = R.drawable.ic_star_empty;

  @BindView(R.id.component_rating_star_image_view)
  ImageView star;

  private boolean isSelected;

  public RatingStarComponent(Context context) {
    super(context);
    init(context);
  }

  public RatingStarComponent(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public RatingStarComponent(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_rating_star, this, true);
    ButterKnife.bind(this, v);
  }

  public void setStarFull() {
    setStarDrawable(starFullDrawable);
    isSelected = true;
  }

  public boolean isSelected() {
    return isSelected;
  }


  public void setStarEmpty() {
    setStarDrawable(starEmptyDrawable);
    isSelected = false;
  }

  private void setStarDrawable(@DrawableRes int starDrawable) {
    star.setImageDrawable(ContextCompat.getDrawable(getContext(), starDrawable));
  }
}
