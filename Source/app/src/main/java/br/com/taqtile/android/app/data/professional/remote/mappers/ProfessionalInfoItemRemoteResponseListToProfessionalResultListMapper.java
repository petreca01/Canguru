package br.com.taqtile.android.app.data.professional.remote.mappers;

import br.com.taqtile.android.app.data.professional.remote.models.response.ProfessionalInfoItemRemoteResponse;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfessionalInfoItemRemoteResponseListToProfessionalResultListMapper {

  public static List<ProfessionalResult> perform(List<ProfessionalInfoItemRemoteResponse> professionalInfoItemRemoteResponseList) {
    return StreamSupport.stream(professionalInfoItemRemoteResponseList)
      .map(ProfessionalInfoItemRemoteResponseToProfessionalResultMapper::perform)
      .collect(Collectors.toList());
  }
}
