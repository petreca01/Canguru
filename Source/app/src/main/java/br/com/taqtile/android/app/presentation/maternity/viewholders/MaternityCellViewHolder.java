package br.com.taqtile.android.app.presentation.maternity.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityCell;

/**
 * Created by taqtile on 5/11/17.
 */

public class MaternityCellViewHolder extends RecyclerView.ViewHolder {

  private MaternityCell maternityCell;

  public MaternityCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.maternityCell = (MaternityCell) itemView;
  }

  public void setData(MaternityViewModel maternity) {
    this.maternityCell.setData(maternity);
  }

  public MaternityCell getMaternityCell() {
    return maternityCell;
  }
}
