package br.com.taqtile.android.app.presentation.appointmenteditschedule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.edittexts.CustomDropDown;
import br.com.taqtile.android.app.edittexts.DateLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.HourLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.ObservationLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.TitleLabelEditTextCaption;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FormTextHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.ExternalIntent;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity.APPOINTMENT_MODEL_KEY;
import static br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity.EDITED_APPOINTMENT_MODEL_KEY;

/**
 * Created by taqtile on 27/04/17.
 */

public class AppointmentEditScheduleFragment extends AppBaseFragment {

  @BindView(R.id.fragment_appointment_edit_schedule_scroll_view)
  NestedScrollView scrollView;
  @BindView(R.id.fragment_appointment_edit_schedule_content_layout)
  LinearLayout contentLayout;
  @BindView(R.id.fragment_appointment_edit_schedule_dropdown)
  CustomDropDown appointmentDropdown;
  @BindView(R.id.fragment_appointment_edit_schedule_title)
  TitleLabelEditTextCaption appointmentTitle;
  @BindView(R.id.fragment_appointment_edit_schedule_date)
  DateLabelEditTextCaption appointmentDate;
  @BindView(R.id.fragment_appointment_edit_schedule_hour)
  HourLabelEditTextCaption appointmentHour;
  @BindView(R.id.fragment_appointment_edit_schedule_observations)
  ObservationLabelEditTextCaption appointmentObservations;
  @BindView(R.id.fragment_appointment_edit_schedule_save_button)
  ProgressBarButton saveButton;
  @BindView(R.id.fragment_appointment_edit_schedule_fixed_title)
  CustomTextView appointmentFixedTitle;

  private NavigationManager navigationManager;
  private AppointmentEditScheduleContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private AgendaItemViewModel appointment;
  private ArrayList<String> appointmentOptionsList;

  private String errorMessage;
  private Validatable[] forms;
  private List<Validatable> errorForms;

  public static AppointmentEditScheduleFragment newInstance() {
    return new AppointmentEditScheduleFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_appointment_edit_schedule, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();
    setupView();
    setupListeners();
    setupFormsList();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().onBackPressed());
    }
  }

  private void setupView() {
    appointment = getActivity().getIntent().getParcelableExtra(APPOINTMENT_MODEL_KEY);
    appointmentObservations.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    appointmentTitle.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    if (toEditCustomAppointment()) {
      setViewForEditCustomAppointment();
    } else if (toCreateCustomAppointment()) {
      setViewForCreateAppointment();
    } else {
      setViewForScheduleAppointment();
    }
  }

  private void setViewForCreateAppointment() {
    if (customToolbar != null) {
      customToolbar.setTitle(getString(R.string.fragment_appointment_edit_schedule_create_title));
    }
    appointmentFixedTitle.setVisibility(View.GONE);

    fillAppointmentOptions();
  }

  private void setViewForEditCustomAppointment() {
    if (customToolbar != null) {
      customToolbar.setTitle(getString(R.string.fragment_appointment_edit_schedule_edit_title));
    }
    appointmentFixedTitle.setVisibility(View.GONE);

    fillAppointmentOptions();
    setTextField();
  }

  private void fillAppointmentOptions() {
    String[] appointmentOptions = getResources()
      .getStringArray(R.array.fragment_appointment_schedule_appointment_options);
    appointmentOptionsList = new ArrayList<>();
    Collections.addAll(appointmentOptionsList, appointmentOptions);
    appointmentDropdown.setValues(appointmentOptionsList);
  }

  private void setViewForScheduleAppointment() {
    if (customToolbar != null) {
      customToolbar.setTitle(getString(R.string.fragment_appointment_edit_schedule_schedule_title));
    }
    appointmentDropdown.setVisibility(View.GONE);
    appointmentTitle.setVisibility(View.GONE);

    appointmentFixedTitle.setText(appointment != null ? appointment.getName() : "");

    setTextField();
  }

  private void setTextField() {
    if (toEditCustomAppointment()) {
      appointmentDropdown.setSelectedValue(appointment.getTypeId());
      appointmentTitle.setText(appointment.getName());
    }

    if (appointment.getMarkingDate() != null && !appointment.getMarkingDate().isEmpty()) {
      appointmentDate.setDate(DateFormatterHelper.getDay(appointment.getMarkingDate()),
        DateFormatterHelper.getMonth(appointment.getMarkingDate()),
        DateFormatterHelper.getYear(appointment.getMarkingDate()));
      appointmentHour.setTime(DateFormatterHelper.getHour(appointment.getMarkingDate()),
        DateFormatterHelper.getMinutes(appointment.getMarkingDate()));
    }
    appointmentObservations.setText(appointment.getObservation());
  }

  private void setupListeners() {
    saveButton.setButtonListener(this::onSaveButtonClicked);
  }

  private void setupFormsList() {
    if (toEditCustomAppointment() || toCreateCustomAppointment()) {
      forms = new Validatable[]{appointmentTitle, appointmentDate, appointmentHour};
    } else {
      forms = new Validatable[]{appointmentObservations, appointmentDate, appointmentHour};
    }
    errorForms = new ArrayList<Validatable>();
  }

  private void onSaveButtonClicked() {
    if (validAppointmentType() && locallyValidateForms()) {
      if (toCreateCustomAppointment()) {
        createAppointment();
      } else if (toEditCustomAppointment()) {
        scheduleAppointment(buildEditCustomAppointmentParams());
      } else {
        scheduleAppointment(buildDefaultAppointmentParam());
      }
    }
  }

  private void createAppointment() {
    AppointmentParams appointmentParams = buildCreateCustomAppointmentParams();
    Intent intent = ExternalIntent.getCalendarInsertEventIntent(appointmentParams);

    showLoading();
    Subscription subscription = presenter.createAppointment(appointmentParams)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(agendaItemViewModel -> onSubscriptionSuccess(intent, agendaItemViewModel),
        this::onSubscriptionFailure);
    compositeSubscription.add(subscription);
  }

  private boolean validAppointmentType() {
    if (toEditCustomAppointment() || toCreateCustomAppointment()) {

      if (appointmentDropdown.getSelectedValueIndex() > 0) {
        appointmentDropdown.showDefaultState();
        return appointmentDropdown.getSelectedValueIndex() > 0;
      } else {
        appointmentDropdown.showErrorState();
        showFormValidationError(appointmentDropdown.getLabel());
        return false;
      }
    } else {
      return true;
    }
  }

  private AppointmentParams buildCreateCustomAppointmentParams() {
    return new AppointmentParams(appointmentDropdown.getSelectedValueIndex(),
      appointmentTitle.getText(), DateFormatterHelper.formatDateForAPIWithHour(
      appointmentHour.getText(), appointmentDate.getText()), appointmentObservations.getText());
  }

  private ScheduleParams buildEditCustomAppointmentParams() {
    return new ScheduleParams(appointment.getId(), appointment.getIsAppointment(),
      DateFormatterHelper.formatDateForAPIWithHour(appointmentHour.getText(),
        appointmentDate.getText()), appointmentObservations.getText(), appointmentTitle.getText(),
      appointmentDropdown.getSelectedValueIndex());
  }

  private ScheduleParams buildDefaultAppointmentParam() {
    return new ScheduleParams(appointment.getId(),
      DateFormatterHelper.formatDateForAPIWithHour(appointmentHour.getText(),
        appointmentDate.getText()), appointmentObservations.getText());
  }

  private void scheduleAppointment(ScheduleParams scheduleParams) {
    Intent intent = ExternalIntent.getCalendarInsertEventIntent(scheduleParams, appointment);

    showLoading();
    Subscription subscription = presenter
      .scheduleAppointment(scheduleParams)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(agendaItemViewModel -> onSubscriptionSuccess(intent, agendaItemViewModel),
        this::onSubscriptionFailure, this::hideLoading);
    compositeSubscription.add(subscription);
  }

  private void onSubscriptionSuccess(Intent intent, AgendaItemViewModel agendaItemViewModel) {

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_new),
      getString(R.string.analytics_label_user_created_schedule)
    ));

    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
      startActivity(intent);
      intent.putExtra(EDITED_APPOINTMENT_MODEL_KEY, agendaItemViewModel);
      getActivity().setResult(Activity.RESULT_OK, intent);
      getActivity().finish();
    }
  }

  private void onSubscriptionFailure(Throwable throwable) {
    showSnackBar(throwable.getMessage());
    hideLoading();
  }

  //region common
  private boolean toEditCustomAppointment() {
    return isCustomAppointment() && appointment.getId() != null;
  }

  private boolean toCreateCustomAppointment() {
    return isValidAppointment() && appointment.getIsAppointment() == null &&
      appointment.getId() == null;
  }

  private boolean isCustomAppointment() {
    return isValidAppointment() && appointment.getIsAppointment() != null &&
      appointment.getIsAppointment().equals(1);
  }

  private boolean isValidAppointment() {
    return appointment != null;
  }
  //endregion

  // validation forms
  public boolean locallyValidateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      showFormValidationError(buildErrorMessage(errorForms));
      ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      isValid = false;
    }
    return isValid;
  }

  public void showFormValidationError(String text) {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, text), Snackbar.LENGTH_SHORT);

  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms = FormTextHelper.getFormsInputPlaceholderText(errorViews);
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  // endregion

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  public void setPresenter(AppointmentEditScheduleContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    saveButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    saveButton.showDefaultState();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_appointment_edit_schedule),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
