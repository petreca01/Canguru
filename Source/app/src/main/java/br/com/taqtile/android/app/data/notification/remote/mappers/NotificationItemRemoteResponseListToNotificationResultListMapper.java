package br.com.taqtile.android.app.data.notification.remote.mappers;
;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import br.com.taqtile.android.app.domain.notification.models.NotificationResult;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 07/04/17.
 */

public class NotificationItemRemoteResponseListToNotificationResultListMapper {
  public static List<NotificationResult> perform(
    List<NotificationItemRemoteResponse> list) {
    return StreamSupport.stream(list)
      .map(NotificationItemRemoteResponseToNotificationResultMapper::perform)
      .collect(Collectors.toList());
  }
}
