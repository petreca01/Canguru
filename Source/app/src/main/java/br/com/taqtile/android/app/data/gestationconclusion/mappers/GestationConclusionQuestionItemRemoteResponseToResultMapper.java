package br.com.taqtile.android.app.data.gestationconclusion.mappers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import br.com.taqtile.android.app.data.gestationconclusion.remote.models.GestationConclusionQuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.mappers.AnswerItemRemoteResponseToAnswerItemResultMapper;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionQuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionQuestionItemRemoteResponseToResultMapper {
  public static GestationConclusionQuestionItemResult perform(GestationConclusionQuestionItemRemoteResponse response) {

    AnswerItemResult answerItemResult = null;
    if (response.getAnswer() != null) {
      answerItemResult = AnswerItemRemoteResponseToAnswerItemResultMapper
        .perform(response.getAnswer());
    } else {
      answerItemResult = new AnswerItemResult(null, null);
    }

    List<String> alternatives = null;
    if (response.getAnswers().isJsonArray()) {
      Type listType = new TypeToken<List<String>>() {
      }.getType();
      alternatives = new Gson().fromJson(response.getAnswers().getAsJsonArray(), listType);
    }

    RangedAnswerResult range = null;
    if (response.getAnswers().isJsonObject() &&
      response.getAnswers().getAsJsonObject().get("min").getAsNumber() != null &&
      response.getAnswers().getAsJsonObject().get("max").getAsNumber() != null) {
      Type rangeType = new TypeToken<RangedAnswerResult>() {
      }.getType();
      range = new Gson().fromJson(response.getAnswers().getAsJsonObject(), rangeType);
    }

    return new GestationConclusionQuestionItemResult(
      response.getId(),
      response.getQuestion(),
      response.getAnswerType(),
      alternatives,
      range,
      response.getNextQuestion(),
      response.getNextQuestionRules(),
      answerItemResult);
  }
}
