package br.com.taqtile.android.app.data.notification;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.notification.local.NotificationLocalDataSource;
import br.com.taqtile.android.app.data.notification.remote.NotificationRemoteDataSource;
import br.com.taqtile.android.app.data.notification.remote.mappers.NotificationCountRemoteResponseToNotificationCountResultMapper;
import br.com.taqtile.android.app.data.notification.remote.mappers.NotificationItemRemoteResponseListToNotificationResultListMapper;
import br.com.taqtile.android.app.data.notification.remote.mappers.SetVisualizedNotificationParamsToSetVisualizedNotificationRemoteRequestMapper;
import br.com.taqtile.android.app.data.notification.remote.models.request.ListNotificationsRemoteRequest;
import br.com.taqtile.android.app.data.notification.remote.models.request.SetVisualizedNotificationRemoteRequest;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import br.com.taqtile.android.app.domain.notification.models.NotificationCountResult;
import br.com.taqtile.android.app.domain.notification.models.NotificationResult;
import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.support.GlobalConstants;
import java8.util.Objects;
import java8.util.Optional;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import rx.Observable;

import static br.com.taqtile.android.app.presentation.notifications.NotificationsFragment.ALL_NOTIFICATIONS;

/**
 * Created by taqtile on 3/23/17.
 */

public class NotificationRepository
  extends BaseRepository<NotificationRemoteDataSource, NotificationLocalDataSource> {

  public NotificationRepository(NotificationRemoteDataSource notificationRemoteDataSource,
    NotificationLocalDataSource notificationLocalDataSource) {
    super(notificationRemoteDataSource, notificationLocalDataSource);
  }

  public Observable<NotificationCountResult> count() {
    return getRemoteDataSource().count()
      .map(result -> NotificationCountRemoteResponseToNotificationCountResultMapper.perform(
        result.getResult()));
  }

  public Observable<Boolean> setVisualized(
    SetVisualizedNotificationParams setVisualizedNotificationParams) {
    return Observable.just(setVisualizedNotificationParams)
      .map(SetVisualizedNotificationParamsToSetVisualizedNotificationRemoteRequestMapper::perform)
      .flatMap(getRemoteDataSource()::setVisualized)
      .flatMap(result -> this.updateLocalNotificationAsRead(setVisualizedNotificationParams.getId())
        .map(aVoid -> result))
      .map(BaseRemoteResponse::getResult);
  }

  public Observable<List<NotificationResult>> list() {
    return this.listWithCache()
      .map(NotificationItemRemoteResponseListToNotificationResultListMapper::perform);
  }

  private Observable<Void> updateLocalNotificationAsRead(String id) {
    return this.listWithCache()
      .flatMap(result -> {
        // TODO: 6/21/17 REMOVE "ALL_NOTIFICATIONS" VERIFICATION AFTER FIX NOTIFICATION REDIRECTION
        if (id.equalsIgnoreCase(ALL_NOTIFICATIONS)) {
          StreamSupport.stream(result)
            .filter(Objects::nonNull)
            .forEach(this::setNotificationAsVisualized);

          return this.saveNotificationsLocally(result);
        }

        // finds notification by id
        Optional<NotificationItemRemoteResponse> maybeNotification = StreamSupport.stream(result)
          .filter(Objects::nonNull)
          .filter(notification -> notification.getId().intValue() == Integer.valueOf(id))
          .findFirst();
        if (maybeNotification.isPresent()) {
          // sets cached data and saves it
          NotificationItemRemoteResponse notification = maybeNotification.get();
          setNotificationAsVisualized(notification);

          return this.saveNotificationsLocally(result)
            .map(any -> null);
        } else {
          // if not cached, just ignores it and continue
          return Observable.just(null);
        }
      })
      .map(any -> null);
  }

  private void setNotificationAsVisualized(NotificationItemRemoteResponse notification) {
    notification.setVisualized(true);
    String now = DateFormatterHelper.stringFromDateWithAPIFormat(new Date());
    notification.setVisualizedAt(now);
    notification.setUpdatedAt(now);
  }

  private Observable<List<NotificationItemRemoteResponse>> listWithCache() {

    // fetch all local notifications sorted
    return this.getLastLocalNotifications()
      .flatMap(localNotifications -> {
        Observable<List<NotificationItemRemoteResponse>> remoteNotificationsObservable;
        if (localNotifications.size() > 0) {
          // fetch only latest notifications if we have any locally
          Integer lastId = localNotifications.get(0).getId();
          remoteNotificationsObservable = this.getRemoteNotificationsAfterId(lastId);
        } else {
          // fetch all notifications in case there aren't any local ones
          remoteNotificationsObservable = this.getAllRemoteNotifications();
        }
        // merge both local and remote notifications, sort and retrieve only the latest N ones
        return remoteNotificationsObservable.map(remoteNotifications -> this.mergeNotification(localNotifications, remoteNotifications));
      })
      // save a copy of all notifications
      .flatMap(this::saveNotificationsLocally);
  }

  //region notification fetch helper methods

  private Observable<List<NotificationItemRemoteResponse>> getLastLocalNotifications() {
    return getLocalDataSource().getNotifications()
      .map(response ->
        StreamSupport.stream(response)
          // sort by id to enable removing the oldest from bottom of the queue
          .sorted((o1, o2) -> o1.getId() < o2.getId() ? 1 : -1)
          .collect(Collectors.toList())
      );
  }

  private Observable<List<NotificationItemRemoteResponse>> getAllRemoteNotifications() {
    return getRemoteDataSource().listAll()
      .map(BaseRemoteResponse::getResult);
  }

  private Observable<List<NotificationItemRemoteResponse>> getRemoteNotificationsAfterId(Integer id) {
    ListNotificationsRemoteRequest request = new ListNotificationsRemoteRequest(id);
    return getRemoteDataSource().listAfterId(request)
      .map(BaseRemoteResponse::getResult);
  }

  @SafeVarargs
  final private List<NotificationItemRemoteResponse> mergeNotification(List<NotificationItemRemoteResponse>... notifications) {

    return StreamSupport.stream(Arrays.asList(notifications))
      .flatMap(StreamSupport::stream)
      .sorted((o1, o2) -> o1.getId() < o2.getId() ? 1 : -1)
      .limit(GlobalConstants.NOTIFICATION_MAX_AMOUNT_TO_KEEP)
      .collect(Collectors.toList());
  }

  private Observable<List<NotificationItemRemoteResponse>> saveNotificationsLocally(List<NotificationItemRemoteResponse> notifications) {
    return getLocalDataSource().saveNotifications(notifications).map(result -> notifications);
  }

  //endregion

}
