package br.com.taqtile.android.app.presentation.symptomedit;


import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.symptoms.ReportSymptomUseCase;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomParams;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 27/04/17.
 */

public class SymptomEditPresenter implements SymptomEditContract.Presenter {

  private ReportSymptomUseCase reportSymptomUseCase;

  public SymptomEditPresenter() {
    reportSymptomUseCase = Injection.provideReportSymptomUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<EmptyResult> reportCustomSymptom(String description, String date) {
    return reportSymptomUseCase.execute(new ReportSymptomParams(description, date));
  }
}
