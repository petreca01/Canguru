package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.request.RateMaternityRemoteRequest;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;

/**
 * Created by taqtile on 06/04/17.
 */

public class RateMaternityParamsToRateMaternityRemoteRequestMapper {

  public static RateMaternityRemoteRequest perform(RateMaternityParams rateMaternityParams) {
    RateMaternityRemoteRequest rateMaternityRemoteRequest = new RateMaternityRemoteRequest();

    rateMaternityRemoteRequest.setRate(rateMaternityParams.getRate());

    return rateMaternityRemoteRequest;
  }

}
