package br.com.taqtile.android.app.domain.channel;

import java.util.List;

import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.domain.channel.mappers.ChannelPostResultToListingsViewModelMapper;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostLikeReplyParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class LikeChannelPostReplyUseCase extends BaseUseCase<List<ListingsViewModel>, ChannelPostLikeReplyParams, ChannelRepository> {

  public LikeChannelPostReplyUseCase(ChannelRepository channelRepository) {
    super(channelRepository);
  }

  @Override
  public Observable<List<ListingsViewModel>> execute(ChannelPostLikeReplyParams channelPostLikeReplyParams) {
    return getRepository().likeReply(channelPostLikeReplyParams)
      .map(ChannelPostResultToListingsViewModelMapper::perform);
  }
}
