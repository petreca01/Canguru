package br.com.taqtile.android.app.presentation.condition;

import java.util.List;

import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.ListConditionsUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.ListUserConditionsUseCase;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.condition.mappers.ClinicalConditionsViewModelToListingsMapper;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionsPresenter implements ConditionsContract.Presenter {

  private UserDataUseCase userDataUseCase;
  private ListConditionsUseCase listConditionsUseCase;
  private ListUserConditionsUseCase listUserConditionsUseCase;

  public ConditionsPresenter() {
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.listConditionsUseCase = Injection.provideListConditionsUseCase();
    this.listUserConditionsUseCase = Injection.provideListUserConditionsUseCase();
  }

  @Override
  public void start() {
  }

  @Override
  public void resume() {
  }


  @Override
  public Observable<UserViewModel> fetchUserData() {
    return userDataUseCase.execute(null);
  }

  @Override
  public Observable<List<ListingsViewModel>> listConditionsUseCase(Integer gestationWeeks) {
    return listConditionsUseCase.execute(gestationWeeks)
      .flatMap(allClinicalConditions ->
        listUserConditionsUseCase.execute(null)
          .map(userHistoryClinicalConditions ->
            ClinicalConditionsViewModelToListingsMapper
              .perform(allClinicalConditions, userHistoryClinicalConditions)));
  }

}
