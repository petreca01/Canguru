package br.com.taqtile.android.app.domain.signin;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInParams;
import rx.Observable;

/**
 * Sign in a user
 */
public class SignInUseCase extends BaseUseCase<EmptyResult, AccountSignInParams, AccountRepository> {

    public SignInUseCase(AccountRepository accountRepository) {
        super(accountRepository);
    }

    @Override
    public Observable<EmptyResult> execute(AccountSignInParams accountSignInParams) {
        return getRepository().login(accountSignInParams);
    }
}
