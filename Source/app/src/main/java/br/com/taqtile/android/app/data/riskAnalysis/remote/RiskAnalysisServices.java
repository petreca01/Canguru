package br.com.taqtile.android.app.data.riskAnalysis.remote;

import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisAnswerRemoteResponse;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisQuestionsRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public interface RiskAnalysisServices {
  @FormUrlEncoded
  @POST("risk_questions")
  Observable<Result<BaseRemoteResponse<RiskAnalysisQuestionsRemoteResponse>>> list(@FieldMap Map<String, String> listRequestModel);

  @FormUrlEncoded
  @POST("risk_questions/answer")
  Observable<Result<BaseRemoteResponse<RiskAnalysisAnswerRemoteResponse>>> answer(@FieldMap Map<String, String> answerRequestModel);
}
