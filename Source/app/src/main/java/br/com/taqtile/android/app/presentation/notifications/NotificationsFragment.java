package br.com.taqtile.android.app.presentation.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;
import br.com.taqtile.android.app.listings.adapters.NotificationsAdapter;
import br.com.taqtile.android.app.listings.viewmodels.NotificationViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.TemplateBottomNavActivity;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.main.MainActivity.GESTATION_INDEX;
import static br.com.taqtile.android.app.presentation.main.MainActivity.HOME_INDEX;
import static br.com.taqtile.android.app.presentation.main.MainActivity.NOTIFICATION_INDEX;
import static br.com.taqtile.android.app.support.pushnotification.service.PushNotificationService.PUSH_REDIRECTION;

/**
 * Created by taqtile on 3/17/17.
 */

public class NotificationsFragment extends AppBaseFragment implements NotificationsAdapter.Listener {

  public static final String ALL_NOTIFICATIONS = "all";
  @BindView(R.id.fragment_notifications_recycler_view)
  RecyclerView notificationsRecyclerView;

  private NotificationsAdapter notificationsAdapter;
  private NotificationsContract.Presenter presenter;
  private NavigationManager navigationManager;
  private List<NotificationViewModel> notificationViewModels;
  private CustomToolbar customToolbar;
  private static final String FORUM = "forum";
  private static final String CHANNEL = "channel";
  private static final String GESTATION = "ig";
  private static final String RECOMMENDATION = "recommendation";
  private static final String DOCTOR_CONNECTION = "doctor-connection";
  private static final String DOCTOR_CONNECTION_REQUEST = "doc_link_req";
  private static final String FOLLOWER = "follower";

  public static NotificationsFragment newInstance() {
    return new NotificationsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);

    View root = inflater.inflate(R.layout.fragment_notifications, container, false);
    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupRecyclerView();
    checkNotifications();
    checkPushNotificationRedirection();
  }


  private void checkPushNotificationRedirection() {
    if (getActivity() != null && getActivity().getIntent() != null &&
      getActivity().getIntent().hasExtra(PUSH_REDIRECTION)) {
      ((MainActivity) getActivity()).setActiveItem(NOTIFICATION_INDEX);
    }
  }

  private void checkNotifications() {
    Subscription subscription = getNotificationsObservable()
      .subscribe(this::onCheckNotificationsSuccess, this::onCheckNotificationsFailure);
    compositeSubscription.add(subscription);
  }

  private void onCheckNotificationsSuccess(List<NotificationViewModel> notificationViewModels) {
    int count = (int) StreamSupport.stream(notificationViewModels)
      .filter(notification -> !notification.isVisualized())
      .count();
    if (count > 0) {
      showNotificationsBadge(count);
    }
  }

  private void onCheckNotificationsFailure(Throwable throwable) {
    // do nothing
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  private void setupRecyclerView() {
    notificationViewModels = new ArrayList<>();
    notificationsAdapter = new NotificationsAdapter(notificationViewModels, this);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    notificationsRecyclerView.setLayoutManager(linearLayoutManager);
    notificationsRecyclerView.setAdapter(notificationsAdapter);
  }

  private void fetchNotifications() {
    showLoading();
    Subscription subscription = getNotificationsObservable()
      .subscribe(this::onNotificationsFetched, this::onNotificationsFailure);
    compositeSubscription.add(subscription);
  }

  private Observable<List<NotificationViewModel>> getNotificationsObservable() {
    return presenter.fetchNotifications()
      .observeOn(AndroidSchedulers.mainThread());
  }

  private void onNotificationsFetched(List<NotificationViewModel> notificationViewModels) {
    resetNotifications();
    this.notificationViewModels.addAll(notificationViewModels);
    notificationsAdapter.notifyDataSetChanged();
    hideLoading();
    if (this.notificationViewModels.isEmpty()) {
      showNoNotificationsPlaceholder();
    }

  }

  private void resetNotifications() {
    if (this.notificationViewModels != null) {
      this.notificationViewModels.clear();
    }
  }

  private void showNoNotificationsPlaceholder() {
    placeholder.setPlaceholderType(CustomPlaceholder.NO_NOTIFICATIONS);
    showPlaceholder();
  }

  private void onNotificationsFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  public void setPresenter(NotificationsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void setNotificationVisualized() {
    Subscription subscription = presenter.setNotificationVisualized(
      // TODO: 6/21/17 CHANGE THIS "ALL_NOTIFICATIONS" FOR NOTIFICATION ID AFTER FIX NOTIFICATIONS REDIRECTION
      new SetVisualizedNotificationParams(ALL_NOTIFICATIONS))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onSetNotificationVisualizedSuccess,
        this::onSetNotificationVisualizedFailure
      );
    compositeSubscription.add(subscription);
  }

  private void onSetNotificationVisualizedFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
  }

  private void onSetNotificationVisualizedSuccess(Boolean aBoolean) {
    fetchNotifications();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    this.unsubscribeAll();
    if (!this.notificationViewModels.isEmpty()) {
      setNotificationVisualized();
      hideNotificationsBadge();
    }
  }

  private void showNotificationsBadge(int count) {
    ((TemplateBottomNavActivity) getActivity()).showNotificationBadge(String.valueOf(count));
  }

  private void hideNotificationsBadge() {
    ((TemplateBottomNavActivity) getActivity()).hideNotificationBadge();
  }

  @Override
  public void willAppear() {
    setupToolbar();
    fetchNotifications();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_notifications),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.bottom_nav_notifications), Gravity.LEFT);
      customToolbar.hideOptionsMenu();
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchNotifications();
    hidePlaceholder();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_notifications_placeholder;
  }

  @Override
  public void onCellClick(NotificationViewModel notification) {
    if (notification.getType().equals(FORUM)) {
      navigationManager.showCommunityPostDetailUI(null, notification.getEntityId(), false);
    } else if (notification.getType().equals(CHANNEL)) {
      navigationManager.showChannelPostDetailUI(null, notification.getEntityId(), false);
    } else if (notification.getType().equals(GESTATION)) {
      ((MainActivity) getActivity()).setActiveItem(GESTATION_INDEX);
    } else if (notification.getType().equals(RECOMMENDATION)) {
      navigationManager.showMedicalRecommendations();
    } else if (notification.getType().equals(DOCTOR_CONNECTION) || notification.getType().equals(DOCTOR_CONNECTION_REQUEST)) {
      navigationManager.showConnectedProfessionals();
    } else if (notification.getType().equals(FOLLOWER)) {
      navigationManager.showPublicProfileUI(notification.getEntityId(), null);
    } else {
      ((MainActivity) getActivity()).setActiveItem(HOME_INDEX);
    }
    notification.getContent();
  }
}
