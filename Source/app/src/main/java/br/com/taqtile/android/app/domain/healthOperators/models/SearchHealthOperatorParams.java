package br.com.taqtile.android.app.domain.healthOperators.models;

/**
 * Created by taqtile on 4/10/17.
 */

public class SearchHealthOperatorParams {
  private String search;

  public SearchHealthOperatorParams(String search) {
    this.search = search;
  }

  public String getSearch() {
    return search;
  }
}
