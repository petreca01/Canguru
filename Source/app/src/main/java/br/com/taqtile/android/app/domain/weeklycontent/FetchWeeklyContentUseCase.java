package br.com.taqtile.android.app.domain.weeklycontent;

import br.com.taqtile.android.app.data.weeklycontent.WeeklyContentRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.weeklycontent.mapper.WeeklyContentResultToViewModel;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/16/17.
 */

public class FetchWeeklyContentUseCase extends BaseUseCase<WeeklyContentViewModel,
  WeeklyContentParams, WeeklyContentRepository> {

  public FetchWeeklyContentUseCase(WeeklyContentRepository weeklyContentRepository) {
    super(weeklyContentRepository);
  }

  @Override
  public Observable<WeeklyContentViewModel> execute(WeeklyContentParams weeklyContentParams) {
    return getRepository().fetchWeeklyContent(weeklyContentParams)
      .map(WeeklyContentResultToViewModel::perform);
  }
}
