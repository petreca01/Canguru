package br.com.taqtile.android.app.presentation.birthplanquestionnaire;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_QUESTIONNAIRE_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_QUESTIONNAIRE_QUESTION_INDEX_KEY;

/**
 * Created by taqtile on 11/05/17.
 */

public class BirthplanQuestionnaireActivity extends TemplateBackActivity {

  private BirthplanQuestionnaireFragment fragment;

  @Override
  public Fragment getFragment() {
    BirthplanQuestionnaireFragment birthplanQuestionnaireFragment = (BirthplanQuestionnaireFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (birthplanQuestionnaireFragment == null) {
      birthplanQuestionnaireFragment = BirthplanQuestionnaireFragment.newInstance();
    }
    fragment = birthplanQuestionnaireFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    BirthplanQuestionnairePresenter birthplanQuestionnairePresenter = new BirthplanQuestionnairePresenter();
    fragment.setPresenter(birthplanQuestionnairePresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return birthplanQuestionnairePresenter;
  }

  public static void navigate(Context context, List<QuestionViewModel> questionnaire, String index) {
    Intent intent = new Intent(context, BirthplanQuestionnaireActivity.class);
    intent.putExtra(BIRTH_PLAN_QUESTIONNAIRE_QUESTION_INDEX_KEY, index);
    intent.putParcelableArrayListExtra(BIRTH_PLAN_QUESTIONNAIRE_KEY,
      (ArrayList<? extends Parcelable>) questionnaire);
    context.startActivity(intent);
  }
}
