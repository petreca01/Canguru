package br.com.taqtile.android.app.data.profile.remote.mappers;

import br.com.taqtile.android.app.data.profile.remote.models.response.UpdateProfileDetailsRemoteResponse;
import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;

/**
 * Created by taqtile on 05/04/17.
 */

public class UpdateProfileDetailsRemoteResponseToResultMapper {

  public static UpdateProfileDetailsResult perform(
    UpdateProfileDetailsRemoteResponse updateProfileDetailsRemoteResponse) {
    return new UpdateProfileDetailsResult(
      getId(updateProfileDetailsRemoteResponse.getId()),
      getName(updateProfileDetailsRemoteResponse.getName()),
      getBabyName(updateProfileDetailsRemoteResponse.getBabyName()),
      getAbout(updateProfileDetailsRemoteResponse.getAbout()),
      getProfilePicture(updateProfileDetailsRemoteResponse.getProfilePicture()),
      getCoverPicture(updateProfileDetailsRemoteResponse.getCoverPicture()),
      getPregnancyWeeks(updateProfileDetailsRemoteResponse.getPregnancyWeeks()),
      getPregnancyDays(updateProfileDetailsRemoteResponse.getPregnancyDays()),
      getCity(updateProfileDetailsRemoteResponse.getCity()),
      getState(updateProfileDetailsRemoteResponse.getState()),
      getLocationPrivacyAgreed(updateProfileDetailsRemoteResponse.getLocationPrivacyAgreed()),
      getCreatedAt(updateProfileDetailsRemoteResponse.getCreatedAt()),
      getUpdatedAt(updateProfileDetailsRemoteResponse.getUpdatedAt()));
  }

  private static Integer getId(Integer id) {
    return id;
  }

  private static String getName(String name) {
    return name != null ? name : "";
  }

  private static String getBabyName(String babyName) {
    return babyName != null ? babyName : "";
  }

  private static String getAbout(String about) {
    return about != null ? about : "";
  }

  private static String getProfilePicture(String profilePicture) {
    return profilePicture != null ? profilePicture : "";
  }

  private static String getCoverPicture(String coverPicture) {
    return coverPicture != null ? coverPicture : "";
  }

  private static Integer getPregnancyWeeks(Integer pregnancyWeeks) {
    return pregnancyWeeks != null ? pregnancyWeeks : 0;
  }

  private static Integer getPregnancyDays(Integer pregnancyDays) {
    return pregnancyDays != null ? pregnancyDays : 0;
  }

  private static String getCity(String city) {
    return city != null ? city : "";
  }

  private static String getState(String state) {
    return state != null ? state : "";
  }

  private static boolean getLocationPrivacyAgreed(Boolean locationPrivacyAgreed) {
    return locationPrivacyAgreed != null ? locationPrivacyAgreed : false;
  }

  private static String getCreatedAt(String createdAt) {
    return createdAt != null ? createdAt : "";
  }

  private static String getUpdatedAt(String updatedAt) {
    return updatedAt != null ? updatedAt : "";
  }
}
