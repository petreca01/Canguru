package br.com.taqtile.android.app.presentation.prenatalcard;

import java.util.List;

import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardConsultationsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardExamsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardUltrasoundsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardVaccinesViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/22/17.
 */

public interface PrenatalCardContract {

  interface Presenter extends BasePresenter {

    Observable<FollowCardViewModel> fetchAntecedentsCard();

    Observable<List<FollowCardConsultationsViewModel>> fetchMedicalAppointments();

    Observable<List<FollowCardExamsViewModel>> fetchExamsCard();

    Observable<List<FollowCardUltrasoundsViewModel>> fetchUltraSoundCard();

    Observable<List<FollowCardVaccinesViewModel>> fetchVaccinesCard();


  }
}
