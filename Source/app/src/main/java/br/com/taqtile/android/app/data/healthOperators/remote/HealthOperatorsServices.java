package br.com.taqtile.android.app.data.healthOperators.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.healthOperators.remote.models.response.HealthOperatorItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public interface HealthOperatorsServices {
    @POST("operators")
    Observable<Result<BaseRemoteResponse<List<HealthOperatorItemRemoteResponse>>>> list();

    @FormUrlEncoded
    @POST("operators/search")
    Observable<Result<BaseRemoteResponse<List<HealthOperatorItemRemoteResponse>>>> search(
      @FieldMap Map<String, String> searchRequestModel);
}
