package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostLikeReplyRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelPostParamsToChannelPostLikeReplyRemoteRequestMapper {
  public static ChannelPostLikeReplyRemoteRequest perform(ChannelPostParams channelPostParams) {
    ChannelPostLikeReplyRemoteRequest channelPostLikeReplyRemoteRequest = new ChannelPostLikeReplyRemoteRequest();

    if (channelPostParams.isLike() != null && channelPostParams.isLike()) {
      channelPostLikeReplyRemoteRequest.setLike(1);
    } else if (channelPostParams.isLike() != null) {
      channelPostLikeReplyRemoteRequest.setDislike(1);
    }

    channelPostLikeReplyRemoteRequest.setPostId(channelPostParams.getPostId());
    channelPostLikeReplyRemoteRequest.setReplyId(channelPostParams.getReplyId());

    return channelPostLikeReplyRemoteRequest;
  }
}
