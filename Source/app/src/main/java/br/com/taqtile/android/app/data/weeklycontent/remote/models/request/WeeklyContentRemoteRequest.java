package br.com.taqtile.android.app.data.weeklycontent.remote.models.request;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentRemoteRequest {

  private String week;

  public WeeklyContentRemoteRequest(String week) {
    this.week = week;
  }

  public String getWeek() {
    return week;
  }

}
