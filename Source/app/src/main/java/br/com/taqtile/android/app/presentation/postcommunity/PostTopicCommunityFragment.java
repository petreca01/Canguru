package br.com.taqtile.android.app.presentation.postcommunity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.LabelToolbarButton;
import br.com.taqtile.android.app.edittexts.MessageTopicLabelEditTextCaption;
import br.com.taqtile.android.app.listings.adapters.PostSuggestionPaginatedAdapter;
import br.com.taqtile.android.app.listings.cells.TextAndImageCell;
import br.com.taqtile.android.app.listings.viewmodels.ChannelSearchPostImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ForumListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ForumReplyListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelSearchPostViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.PaginationViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 11/04/17.
 */

public class PostTopicCommunityFragment extends AppBaseFragment implements
  TextAndImageCell.TextAndImageCellListener, MenuItem.OnMenuItemClickListener, LabelToolbarButton.Listener {

  private final static int FIRST_MENU_ITEM_POSITION = 0;

  private final static int REQUEST_TIME_DELAY = 2000;

  private Handler requestDelayTimeHandler;
  private Runnable requestDelayRunnable;

  @BindView(R.id.fragment_post_community_topic)
  MessageTopicLabelEditTextCaption topic;

  @BindView(R.id.fragment_post_topic_community_suggestions_list)
  RecyclerView suggestionsListRecyclerView;
  private boolean loadMoreTriggered;
  private boolean hasMorePosts;
  private RecyclerView.OnScrollListener onScrollListener;
  private LinearLayoutManager linearLayoutManager;

  private String searchTerm;

  private PostSuggestionPaginatedAdapter suggestionsListAdapter;
  private List<SearchListingViewModel> suggestionsList;

  @BindView(R.id.fragment_search_search_list_header_container)
  FrameLayout suggestionsListHeaderContainer;

  private PostCommunityContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  public static PostTopicCommunityFragment newInstance() {
    return new PostTopicCommunityFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_post_topic_community, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupView();
    setupRequestTimeHandler();
    setupRequestTimeRunnable();
    setupMessageTopicFormListeners();
    setupSuggestionsListRecyclerView();
  }

  private List<SearchListingViewModel> getContentSuggestionList() {
    suggestionsList = new ArrayList<>();
    return suggestionsList;
  }

  private void setupMessageTopicFormListeners() {
    LabelToolbarButton continueButton = ((LabelToolbarButton) (customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION)).getActionView());
    setupTextChangedListener(continueButton);
    setupKeyboardListener();
  }

  private void setupRequestTimeRunnable() {
    requestDelayRunnable = this::fetchContentSuggestion;
  }

  private void setupKeyboardListener() {
    // TODO: 05/07/17 this is not working in all APIs, find out why
    topic.getEditText().setOnKeyListener((v, keyCode, event) -> {
      if (event.getAction() == MotionEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER && !topic.getText().isEmpty()) {
        onContinueButtonClick();
      }
      return false;
    });
  }

  private void setupRequestTimeHandler() {
    requestDelayTimeHandler = new Handler();
  }

  private void setupTextChangedListener(LabelToolbarButton continueButton) {
    topic.setTextChangedListener(new TextWatcher() {
                                   @Override
                                   public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                   }

                                   @Override
                                   public void onTextChanged(CharSequence s, int start, int before, int count) {
                                     if (topic.getText().isEmpty()) {
                                       requestDelayTimeHandler.removeCallbacks(requestDelayRunnable);
                                       continueButton.disableButton();
                                       hideSuggestionsList();
                                     } else {
                                       searchTerm = String.valueOf(s);
                                       continueButton.enableButton();
                                       requestDelayTimeHandler.removeCallbacks(requestDelayRunnable);
                                       requestDelayTimeHandler.postDelayed(requestDelayRunnable, REQUEST_TIME_DELAY);
                                     }
                                   }

                                   @Override
                                   public void afterTextChanged(Editable s) {

                                   }
                                 }

    );

  }

  private void fetchContentSuggestion() {
    Subscription subscription = presenter.getContentSuggestion(searchTerm, true)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchContentSuggestionSuccessful,
        this::onFetchContentSuggestionFailure);
    compositeSubscription.add(subscription);
  }

  private void onFetchContentSuggestionSuccessful(List<SearchListingViewModel> searchListingViewModels) {
    showSuggestionsList(searchListingViewModels);
    suggestionsList.clear();
    suggestionsList.addAll(searchListingViewModels);
    suggestionsListAdapter.notifyDataSetChanged();
    setMorePosts(searchListingViewModels);
    suggestionsListRecyclerView.scrollToPosition(0);
  }

  private void onFetchContentSuggestionFailure(Throwable throwable) {
    //do nothing
  }

  private void fetchMoreContentSuggestion() {
    Subscription subscription = presenter.getContentSuggestion(searchTerm, false)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchMoreContentSuggestionSuccessful,
        this::onFetchMoreContentSuggestionFailure);
    compositeSubscription.add(subscription);
  }

  private void onFetchMoreContentSuggestionSuccessful(List<SearchListingViewModel> searchListingViewModels) {
    suggestionsList.addAll(searchListingViewModels);
    suggestionsListAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
    setMorePosts(searchListingViewModels);
  }

  private void onFetchMoreContentSuggestionFailure(Throwable throwable) {
    loadMoreTriggered = false;
    suggestionsListAdapter.setFooterButtonVisible(false);
  }

  private void setMorePosts(List<SearchListingViewModel> contentSuggestionList) {
    if (contentSuggestionList != null && contentSuggestionList.size() > 0 && !areAllPostsShowing()) {
      hasMorePosts = true;
      suggestionsListAdapter.setFooterButtonVisible(true);
    } else {
      hasMorePosts = false;
      suggestionsListAdapter.setFooterButtonVisible(false);
    }
  }

  private boolean areAllPostsShowing() {
    if (suggestionsList != null && !suggestionsList.isEmpty()) {
      return ((PaginationViewModel) suggestionsList.get(0)).getTotalPosts() == suggestionsList.size();
    } else {
      return false;
    }
  }

  public void setupRecyclerViewListener() {
    loadMoreTriggered = false;
    hasMorePosts = true;

    onScrollListener = new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (hasRecyclerViewReachedTheEnd() && !loadMoreTriggered && hasMorePosts) {
          loadMoreTriggered = true;
          fetchMoreContentSuggestion();
        }
      }
    };
    suggestionsListRecyclerView.addOnScrollListener(onScrollListener);
  }

  private boolean hasRecyclerViewReachedTheEnd() {
    int visibleItemCount = linearLayoutManager.getChildCount();
    int totalItemCount = linearLayoutManager.getItemCount();
    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
    return pastVisibleItems + visibleItemCount >= totalItemCount && pastVisibleItems > 0;
  }

  private void showSuggestionsList(List<SearchListingViewModel> searchListingViewModels) {
    if (!searchListingViewModels.isEmpty()) {
      suggestionsListHeaderContainer.setVisibility(View.VISIBLE);
      suggestionsListRecyclerView.setVisibility(View.VISIBLE);
    } else {
      suggestionsListHeaderContainer.setVisibility(View.GONE);
      suggestionsListRecyclerView.setVisibility(View.GONE);
    }
  }

  private void hideSuggestionsList() {
    suggestionsListHeaderContainer.setVisibility(View.GONE);
    suggestionsListRecyclerView.setVisibility(View.GONE);
  }

  private void setupSuggestionsListRecyclerView() {
    suggestionsListRecyclerView.setAdapter(getSuggestionsListAdapter());

    linearLayoutManager = new LinearLayoutManager(getContext());
    suggestionsListRecyclerView.setLayoutManager(linearLayoutManager);
    setupRecyclerViewListener();
  }

  private PostSuggestionPaginatedAdapter getSuggestionsListAdapter() {
    suggestionsListAdapter = new PostSuggestionPaginatedAdapter(getContext(), getContentSuggestionList(), this);
    return suggestionsListAdapter;
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getString(R.string.fragment_post_community_title), Gravity.LEFT);
      setupToolbarMenu();
    }
  }

  private void setupToolbarMenu() {
    customToolbar.inflateOptionsMenu(R.menu.menu_continue, this);
    customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION).setActionView(getContinueButton());
    setupContinueButton();

  }

  private LabelToolbarButton getContinueButton() {
    return new LabelToolbarButton(getContext());
  }

  private void setupContinueButton() {
    LabelToolbarButton continueButton = ((LabelToolbarButton) (customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION)).getActionView());
    continueButton.setListener(this);
  }

  private void setupView() {
    LabelToolbarButton continueButton = ((LabelToolbarButton) (customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION)).getActionView());
    continueButton.disableButton();
    continueButton.setText(getResources().getString(R.string.continues));
    topic.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

    searchTerm = "";
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  public void setPresenter(PostCommunityContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void onCellClick(String id) {
    SearchListingViewModel clickedCell = getClickedCell(id);
    if (clickedCell instanceof ChannelSearchPostViewModel) {
      navigationManager.showChannelPostDetailUI(null, ((ChannelSearchPostImplViewModel) clickedCell).getContentId(), false);
    } else if (clickedCell instanceof ForumListingImplViewModel) {
      navigationManager.showCommunityPostDetailUI(null, ((ForumListingImplViewModel) clickedCell).getContentId(), false);
    } else if (clickedCell instanceof SymptomListingImplViewModel) {
      navigationManager.showSymptomDetailUI(((SymptomListingImplViewModel) clickedCell).getContentParentId(),
        ((SymptomListingImplViewModel) clickedCell).getContentId());
    } else if (clickedCell instanceof ForumReplyListingImplViewModel){
      navigationManager.showCommunityPostDetailUI(null, ((ForumReplyListingImplViewModel) clickedCell).getContentParentId(), false);
    }
  }

  private SearchListingViewModel getClickedCell(String id) {
    return StreamSupport.stream(suggestionsList)
      .filter(searchListingViewModel -> searchListingViewModel.getId().equals(id))
      .findFirst()
      .get();
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    return false;
  }

  public void onContinueButtonClick() {
    ((PostCommunityActivity) getActivity()).navigateToMessageFragment(topic.getText());
  }

  @Override
  public void onLabelClick() {
    onContinueButtonClick();
  }

  public void unsubscribeAll() {
    if (compositeSubscription.hasSubscriptions()) {
      compositeSubscription.clear();
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum_post_subject),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
