package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostItemRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPost;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelPostItemRemoteResponseToChannelPostMapper {

  public static ChannelPost perform(ChannelPostItemRemoteResponse channelPostItemRemoteResponse) {
    ChannelPost post = new ChannelPost();

    post.setId(channelPostItemRemoteResponse.getId());
    post.setType(channelPostItemRemoteResponse.getType());
    post.setChannelId(channelPostItemRemoteResponse.getChannelId());
    post.setChannelName(channelPostItemRemoteResponse.getChannelName());
    post.setPostAuthor(channelPostItemRemoteResponse.getChannelAuthor());
    post.setTitle(channelPostItemRemoteResponse.getTitle());
    post.setDescription(channelPostItemRemoteResponse.getDescription());
    post.setPost(channelPostItemRemoteResponse.getPost());
    post.setImage(channelPostItemRemoteResponse.getImage());
    post.setVideo(channelPostItemRemoteResponse.getVideo());
    post.setSchedule(channelPostItemRemoteResponse.getSchedule());
    post.setVisualizations(channelPostItemRemoteResponse.getVisualizations());
    post.setActive(channelPostItemRemoteResponse.getActive());
    post.setCreatedAt(channelPostItemRemoteResponse.getCreatedAt());
    post.setReplies(channelPostItemRemoteResponse.getReplies());
    post.setLikes(channelPostItemRemoteResponse.getLikes());
    post.setLiked(channelPostItemRemoteResponse.getLiked());

    return post;
  }

  public static Boolean isValidChannelPost(ChannelPostItemRemoteResponse channelPost) {
    return channelPost.getId() != null &&
      channelPost.getChannelName() != null &&
      channelPost.getTitle() != null &&
      channelPost.getTitle() != null &&
      channelPost.getDescription() != null &&
      channelPost.getLikes() != null &&
      channelPost.getReplies() != null;
  }
}
