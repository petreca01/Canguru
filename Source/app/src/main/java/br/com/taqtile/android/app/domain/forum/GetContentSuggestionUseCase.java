package br.com.taqtile.android.app.domain.forum;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionParams;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionResult;
import rx.Observable;

/**
 * Created by taqtile on 12/07/17.
 */

public class GetContentSuggestionUseCase {

  private Integer currentPage;
  private static final Integer CONTENT_PER_PAGE = 10;
  private ForumRepository forumRepository;

  public GetContentSuggestionUseCase(ForumRepository forumRepository) {
    this.forumRepository = forumRepository;
    currentPage = 1;
  }

  public Observable<ContentSuggestionResult> execute(ContentSuggestionParams contentSuggestionParams,
                                                     boolean reload) {
    currentPage = reload ? 1 : currentPage;
    contentSuggestionParams.setPage(currentPage);
    contentSuggestionParams.setPerPage(CONTENT_PER_PAGE);
    return this.forumRepository.getContentSuggestion(contentSuggestionParams)
      .doOnNext(contentSuggestionResult -> currentPage++);
  }

}
