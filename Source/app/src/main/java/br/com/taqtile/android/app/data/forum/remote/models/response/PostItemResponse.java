package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 3/17/17.
 */

public class PostItemResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("operator_id")
    private Integer operatorId;

    @SerializedName("usuario_id")
    private Integer userId;

    @SerializedName("title")
    private String title;

    @SerializedName("content")
    private String content;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("username")
    private String username;

    @SerializedName("user_picture")
    private String userPicture;

    @SerializedName("likes")
    private Integer likes;

    @SerializedName("count_replies")
    private Integer countReplies;

    @SerializedName("liked")
    private Boolean liked;

    @SerializedName("categories")
    private List<CategoryItemResponse> categories;

    public Integer getId() {
        return id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Boolean getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUserType() {
        return userType;
    }

    public String getUsername() {
        return username;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public Integer getLikes() {
        return likes;
    }

    public Integer getCountReplies() {
        return countReplies;
    }

    public Boolean getLiked() {
        return liked;
    }

    public List<CategoryItemResponse> getCategories() {
        return categories;
    }
}
