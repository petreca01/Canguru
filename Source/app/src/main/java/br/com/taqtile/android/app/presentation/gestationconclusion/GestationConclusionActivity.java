package br.com.taqtile.android.app.presentation.gestationconclusion;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.GESTATION_CONCLUSION_REQUEST_CODE;

/**
 * Created by taqtile on 07/07/17.
 */

public class GestationConclusionActivity extends TemplateBackActivity {

  private GestationConclusionFragment gestationConclusionFragment;

  @Override
  public Fragment getFragment() {
    GestationConclusionFragment gestationConclusionFragment =
      (GestationConclusionFragment) getSupportFragmentManager()
        .findFragmentById(getFragmentContainerId());
    if (gestationConclusionFragment == null) {
      gestationConclusionFragment = GestationConclusionFragment.newInstance();
    }
    this.gestationConclusionFragment = gestationConclusionFragment;

    return this.gestationConclusionFragment;
  }

  @Override
  public BasePresenter getPresenter() {
    GestationConclusionPresenter gestationConclusionPresenter = new GestationConclusionPresenter();
    gestationConclusionFragment.setNavigationManager(new NavigationHelper(this));
    gestationConclusionFragment.setPresenter(gestationConclusionPresenter);
    return gestationConclusionPresenter;
  }

  public static void navigate(Activity context, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, GestationConclusionActivity.class);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        GESTATION_CONCLUSION_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        GESTATION_CONCLUSION_REQUEST_CODE);
    }
  }
}
