package br.com.taqtile.android.app.support.helpers;

import android.content.Context;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;

/**
 * Created by taqtile on 6/6/17.
 */

public class GestationTextFormatterHelper {

  public static String composeFullChildbirth(Context context, UserViewModel userViewModel) {

    String babyName = userViewModel.getBabyName();

    String babyAgeWeeks = context.getResources().getQuantityString(br.com.taqtile.android.app.listings.R.plurals.babyAgeWeeks,
      userViewModel.getGestationWeeks(), userViewModel.getGestationWeeks());

    String babyAgeDays = context.getResources().getQuantityString(br.com.taqtile.android.app.listings.R.plurals.babyAgeDays,
      userViewModel.getGestationDays(), userViewModel.getGestationDays());

    String babyAgeMonths = context.getResources().getQuantityString(br.com.taqtile.android.app.listings.R.plurals.babyAgeMonths,
      userViewModel.getGestationMonths(), userViewModel.getGestationMonths());

    String babyAge = context.getResources().getString(
      R.string.fragment_gestation_information_gestation_header_baby_age, babyAgeWeeks, babyAgeDays);

    // replace by this after api fixed
//    String babyAge = context.getResources().getString(
//      R.string.fragment_gestation_information_gestation_header_baby_age_full, babyAgeWeeks, babyAgeDays, babyAgeMonths);

    String fullChildbirth = context.getResources().getString(
      R.string.fragment_gestation_information_gestation_header_baby_age_placeholder, babyName, babyAge);

    return fullChildbirth;
  }

  public static String composeDevelopmentWeek(Context context, String week) {
    return context.getResources().getString(R.string.fragment_gestation_baby_development_details_week_text, week);
  }
}
