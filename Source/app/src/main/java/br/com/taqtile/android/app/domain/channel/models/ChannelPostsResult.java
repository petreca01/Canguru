package br.com.taqtile.android.app.domain.channel.models;

import java.util.List;

/**
 * Created by taqtile on 3/24/17.
 */

public class ChannelPostsResult {

  private ChannelResult channelResult;
  private List<ChannelPost> channelPosts;
  private Integer totalPosts;
  private Integer postsInPage;

  public ChannelResult getChannelResult() {
    return channelResult;
  }

  public void setChannelResult(ChannelResult channelResult) {
    this.channelResult = channelResult;
  }

  public List<ChannelPost> getChannelPosts() {
    return channelPosts;
  }

  public void setChannelPosts(List<ChannelPost> channelPosts) {
    this.channelPosts = channelPosts;
  }

  public Integer getTotalPosts() {
    return totalPosts;
  }

  public void setTotalPosts(Integer totalPosts) {
    this.totalPosts = totalPosts;
  }

  public Integer getPostsInPage() {
    return postsInPage;
  }

  public void setPostsInPage(Integer postsInPage) {
    this.postsInPage = postsInPage;
  }
}
