package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelItemRemoteResponseToChannelResultMapper;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostResult;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelPostRemoteResponseToChannelPostResultMapper {
  public static ChannelPostResult perform(ChannelPostRemoteResponse channelPostRemoteResponse) {
    ChannelPostResult channelPostResult = new ChannelPostResult();

    channelPostResult.setChannelPost(ChannelPostItemRemoteResponseToChannelPostMapper.perform(
      channelPostRemoteResponse.getPost()));
    channelPostResult.setChannelResult(ChannelItemRemoteResponseToChannelResultMapper.perform(
      channelPostRemoteResponse.getChannel()));
    channelPostResult.setReplies(ChannelReplyRemoteResponseToChannelPostReplyMapper.perform(
      channelPostRemoteResponse.getReplies()));

    return channelPostResult;
  }
}
