package br.com.taqtile.android.app.presentation.maternity.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityHeaderCell;

/**
 * Created by taqtile on 5/11/17.
 */

public class MaternityHeaderViewHolder extends RecyclerView.ViewHolder {

  MaternityHeaderCell maternityHeaderCell;

  public MaternityHeaderViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    maternityHeaderCell = (MaternityHeaderCell) itemView;
  }

  public void setFileInputListener(MaternityHeaderCell.Listener listener) {
    this.maternityHeaderCell.setListener(listener);
  }

  public void setCityName(String cityName) {
    this.maternityHeaderCell.setCityName(
      String.format(
        maternityHeaderCell.getContext().getString(R.string.component_maternity_header_cell_city),
        cityName));
  }

  public void resetFilter() {
    this.maternityHeaderCell.resetFilter();
  }
}
