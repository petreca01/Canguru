package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.graphics.Canvas;
import android.view.View;

import br.com.taqtile.android.app.presentation.tutorial.showcase.paints.TransparentPaintBuilder;

/**
 * Created by taqtile on 5/30/17.
 */

public class SemiCircleRectangle implements SurroundingShape {

  public static class DrawingParams {
    int width;
    int height;

    public DrawingParams(int width, int height) {
      this.width = width;
      this.height = height;
    }
  }

  private int top, left;
  private DrawingParams drawingParams;

  public SemiCircleRectangle(DrawingParams drawingParams) {
    this.top = 0;
    this.left = 0;
    this.drawingParams = drawingParams;
  }

  @Override
  public void focusIn(View target) {
    int[] location = new int[2];

    target.getLocationInWindow(location);
    int viewLeft = location[0];
    int viewTop = location[1];

    int viewCenterX = viewLeft + target.getWidth()/2;
    int viewCenterY = viewTop + target.getHeight()/2;

    left = viewCenterX - drawingParams.width/2;
    top = viewCenterY - drawingParams.height/2;

  }

  @Override
  public void drawOnCanvas(Canvas canvas) {
    TransparentCircle semiCircle = new TransparentCircle(new TransparentCircle.DrawingParams(drawingParams.height/2, TransparentPaintBuilder.build()));
    semiCircle.setCenter(left, top + drawingParams.height/2);

    Rectangle rectangle = new Rectangle(new Rectangle.DrawingParams(drawingParams.width, drawingParams.height, TransparentPaintBuilder.build()));
    rectangle.setPosition(left, top);

    rectangle.drawOnCanvas(canvas);
    semiCircle.drawOnCanvas(canvas);

  }
}
