package br.com.taqtile.android.app.data.view.models.request;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class ViewModeRemoteRequest extends BaseRemoteRequest {
    private String view;
    private String email;

    public ViewModeRemoteRequest(String view, String email) {
      this.view = view;
      this.email = email;
    }

}
