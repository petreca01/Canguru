package br.com.taqtile.android.app.presentation.connectedprofessionals;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 19/04/17.
 */

public class ConnectedProfessionalsActivity extends TemplateBackActivity {

  private ConnectedProfessionalsFragment fragment;

  @Override
  public Fragment getFragment() {
    ConnectedProfessionalsFragment connectedProfessionalsFragment = (ConnectedProfessionalsFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (connectedProfessionalsFragment == null) {
      connectedProfessionalsFragment = ConnectedProfessionalsFragment.newInstance();
    }
    fragment = connectedProfessionalsFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ConnectedProfessionalsPresenter connectedProfessionalsPresenter = new ConnectedProfessionalsPresenter();
    fragment.setPresenter(connectedProfessionalsPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return connectedProfessionalsPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, ConnectedProfessionalsActivity.class);
    context.startActivity(intent);
  }
}
