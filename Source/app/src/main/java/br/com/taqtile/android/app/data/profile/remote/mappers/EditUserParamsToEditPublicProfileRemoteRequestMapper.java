package br.com.taqtile.android.app.data.profile.remote.mappers;

import br.com.taqtile.android.app.data.profile.local.models.UserImagesResponse;
import br.com.taqtile.android.app.data.profile.remote.models.request.EditPublicProfileRemoteRequest;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class EditUserParamsToEditPublicProfileRemoteRequestMapper {

  public static EditPublicProfileRemoteRequest perform(EditProfileParams editProfileParams,
                                                       UserImagesResponse userImagesResponse) {
    return new EditPublicProfileRemoteRequest(
      editProfileParams.getName(),
      editProfileParams.getBabyName(),
      editProfileParams.getAbout(),
      editProfileParams.getConclusionDate(),
      editProfileParams.getConclusionMethod(),
      editProfileParams.getConclusionText(),
      userImagesResponse.getProfilePicture(),
      userImagesResponse.getCoverPhoto(),
      editProfileParams.getRemoveCover(),
      editProfileParams.getRemoveProfilePicture());
  }


}
