package br.com.taqtile.android.app.presentation.probablebirthdate;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 19/07/17.
 */

public class ProbableBirthdateActivity extends TemplateBackActivity {

  private ProbableBirthdateFragment fragment;

  @Override
  public Fragment getFragment() {
    ProbableBirthdateFragment probableBirthdateFragment= (ProbableBirthdateFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (probableBirthdateFragment == null) {
      probableBirthdateFragment = ProbableBirthdateFragment.newInstance();
    }
    fragment = probableBirthdateFragment;

    return fragment;

  }

  @Override
  public BasePresenter getPresenter() {
    ProbableBirthdatePresenter probableBirthdatePresenter = new ProbableBirthdatePresenter();
    fragment.setPresenter(probableBirthdatePresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return probableBirthdatePresenter;
  }

  public static void navigate(Context context){
    context.startActivity(new Intent(context, ProbableBirthdateActivity.class));
  }

}
