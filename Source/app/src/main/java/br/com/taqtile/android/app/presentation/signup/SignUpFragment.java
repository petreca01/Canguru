package br.com.taqtile.android.app.presentation.signup;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.edittexts.ConfirmEmailLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.CustomNameLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.Password4DigitMinimumLabelEditTextCaption;
import br.com.taqtile.android.app.misc.dialog.TextViewInDialog;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleAccessor;
import br.com.taqtile.android.app.presentation.signin.SignInContract;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.checkboxes.CustomCheckbox;
import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.BirthdateLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;
import br.com.taqtile.android.edittexts.validatablehelpers.FormComparisonValidatable;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.SIGNUP_FROM_SIGNIN_EMAIL_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.SIGNUP_FROM_SIGNIN_PASSWORD_KEY;

/**
 * Created by taqtile on 11/10/16.
 */

public class SignUpFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  private static final String TAG = "SignUpFragment";
  @BindView(R.id.fragment_signup_scrollview)
  ScrollView scrollView;

  @BindView(R.id.fragment_signup_birthdate_form)
  BirthdateLabelEditTextCaption birthDateEditText;

  @BindView(R.id.fragment_signup_content)
  LinearLayout contentLayout;

  @BindView(R.id.fragment_signup_create_account_button)
  ProgressBarButton createAccountButton;

  @BindView(R.id.fragment_signup_email_form)
  EmailLabelEditTextCaption emailEditText;

  @BindView(R.id.fragment_signup_terms_agreement)
  CustomCheckbox termsAgreement;

  @BindView(R.id.fragment_signup_terms)
  CustomTextView terms;

  @BindView(R.id.fragment_signup_name_form)
  CustomNameLabelEditTextCaption nameEditText;

  @BindView(R.id.fragment_signup_confirm_email_form)
  ConfirmEmailLabelEditTextCaption confirmEmailEditText;

  @BindView(R.id.fragment_signup_confirm_password_form)
  Password4DigitMinimumLabelEditTextCaption passwordEditText;

  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;
  private SignUpContract.Presenter presenter;
  private SignInContract.Presenter presenterLogin;

  private String errorMessage;
  private Validatable[] forms;
  private List<Validatable> errorForms;
  private FormComparisonValidatable formsComparisonValidatable;

  private InitialLoginBundleAccessor initialLoginBundleAccessor;

  public static SignUpFragment newInstance() {
    return new SignUpFragment();
  }

  public SignUpFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_signup, container, false);
    ButterKnife.bind(this, root);
    setupViews();
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupTermsLink();
    setupFormsList();
    setupToolbar();
    setupListener();
    setupView();
    setupFormsComparisonValidatable();
  }

  private void setupFormsList() {
    forms = new Validatable[]{nameEditText, emailEditText, passwordEditText,
      confirmEmailEditText, birthDateEditText};
    errorForms = new ArrayList<Validatable>();
  }

  private void setupViews() {
    createAccountButton.setButtonListener(this);
    setEmailPass();
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_signup_title));
    }
  }

  private void setupListener() {
    termsAgreement.setOnCheckedChangeListener((buttonView, isChecked) -> {
      if (isChecked) {
        createAccountButton.enableClick();
      } else {
        createAccountButton.disableClick();
      }
    });
  }

  private void setupView() {
    birthDateEditText.setHintCaption("dd/mm/aaaa");
    nameEditText.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
    createAccountButton.disableClick();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_signup_placeholder;
  }

  public void setPresenter(SignUpContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setLoginPresenter(SignInContract.Presenter presenter) {
    this.presenterLogin = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;
    errorMessage = "";
    isValid = validateForms();

    if (!isValid) {
      //Display feedback with all views with an error
      showFormValidationError();
      if (!errorForms.isEmpty()) {
        ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      }
    }

    return isValid;
  }

  public void showFormValidationError() {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);

  }

  @Override
  public void showLoading() {
    createAccountButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    createAccountButton.showDefaultState();
  }

  @Override
  public void onButtonClicked() {
    if (termsAgreement.isChecked() && locallyValidateForms()) {
      showLoading();
      Subscription subscription = presenter.createAccount(emailEditText.getText(),
        passwordEditText.getText(), nameEditText.getText(),
        DateFormatterHelper.formatDateForAPIWithoutHour(birthDateEditText.getText()))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onSignUpSuccess, this::onSignUpFailure);
      compositeSubscription.add(subscription);
    }
  }

  private void onSignUpSuccess(EmptyResult emptyResult) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_registration),
      getString(R.string.analytics_event_email),
      getString(R.string.analytics_label_user_registered_using_email)
    ));
    hideLoading();
    getActivity().setResult(Activity.RESULT_OK, new Intent());
    getActivity().finish();
  }

  private void onSignUpFailure(Throwable throwable) {
    hideLoading();
    showGenericErrorSnackbar(throwable);
  }

  private void showGenericErrorSnackbar(Throwable throwable) {
    showSnackBar(throwable.getMessage());

    if (throwable.getMessage().equals("E-mail registrado, senha correta")) {
      showSignInDialogLogin();
    } else if (throwable.getMessage().equals("E-mail registrado, senha incorreta")) {
      showSignInDialogPassRecovery();
    }
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  private void setupTermsLink() {
    String descriptionText = getResources().getString(R.string.fragment_signup_terms_description);
    String useTermslinkText = getResources().getString(R.string.fragment_signup_terms_link_text);
    String descriptionText2 = getResources().getString(R.string.fragment_signup_policy_description);
    String privacyPolicylinkText = getResources().getString(R.string.fragment_signup_policy_link_text);

    String completeText = String.format("%s %s %s %s", descriptionText, useTermslinkText,
      descriptionText2, privacyPolicylinkText);
    SpannableString ss = new SpannableString(completeText);

    int useTermsInitialLength = descriptionText.length();
    int useTermsFinalLength = descriptionText.length() + useTermslinkText.length();
    ss.setSpan(getUseTermsClickableSpan(), useTermsInitialLength, useTermsFinalLength,
      Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    int spaceLength = 1;
    int privacyPolicyInitialLength = descriptionText.length() + useTermslinkText.length() +
      descriptionText2.length() + 3 * spaceLength;
    int privacyPolicyFinalLength = descriptionText.length() + useTermslinkText.length() +
      descriptionText2.length() + privacyPolicylinkText.length() + 2 * spaceLength;
    ss.setSpan(getPrivacyPolicyClickableSpan(), privacyPolicyInitialLength,
      privacyPolicyFinalLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    terms.setText(ss);
    terms.setMovementMethod(LinkMovementMethod.getInstance());
    terms.setHighlightColor(Color.TRANSPARENT);
  }

  private ClickableSpan getUseTermsClickableSpan() {
    ClickableSpan useTermsClickableSpan = new ClickableSpan() {
      @Override
      public void onClick(View textView) {
        navigationManager.showUseTermsUI();
      }

      @Override
      public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
      }
    };
    return useTermsClickableSpan;
  }

  private ClickableSpan getPrivacyPolicyClickableSpan() {
    ClickableSpan privacyPolicyClickableSpan = new ClickableSpan() {
      @Override
      public void onClick(View textView) {
        navigationManager.showPrivacyPolicyUI();
      }

      @Override
      public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
      }
    };
    return privacyPolicyClickableSpan;
  }

  private boolean validateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      isValid = false;
    }
    return isValid;
  }

  private void setupFormsComparisonValidatable() {
    formsComparisonValidatable = new FormComparisonValidatable(emailEditText, confirmEmailEditText);
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms;

    errorForms = new String[errorViews.size()];
    for (int i = 0; i < errorForms.length; i++) {
      errorForms[i] = getResources().getString(((CustomLabelEditTextCaption) errorViews.get(i).getView()).getInputPlaceholderTextRes());
    }
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
  }

  public void setInitialLoginBundleAccessor(InitialLoginBundleAccessor initialLoginBundleAccessor) {
    this.initialLoginBundleAccessor = initialLoginBundleAccessor;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_signup),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setEmailPass() {
    if (getActivity().getIntent().getExtras() != null) {
      String email = getActivity().getIntent().getExtras().getString(SIGNUP_FROM_SIGNIN_EMAIL_KEY, "");
      String password = getActivity().getIntent().getExtras().getString(SIGNUP_FROM_SIGNIN_PASSWORD_KEY, "");
      emailEditText.setText(email);
      confirmEmailEditText.setText(email);
      passwordEditText.setText(password);
    }
  }

  private void showSignInDialogLogin() {
    String defaultValue = getString(R.string.fragment_signup_dialog_to_signup_text_signed_login);

    final TextViewInDialog input = getTextViewForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_signup_dialog_to_signup_title))
      .setView(input)
      .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
          doLogin();
        }
      ).setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
// Do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));

  }

  private void showSignInDialogPassRecovery() {
    String defaultValue = getString(R.string.fragment_signup_dialog_to_signup_text_signed_wrong_passwod);

    final TextViewInDialog input = getTextViewForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_signup_dialog_to_signup_title))
      .setView(input)
      .setPositiveButton(getString(R.string.fragment_signup_dialog_to_signup_button_recovery_password), (dialog, which) -> {
          navigationManager.showForgotPasswordUI(this::returningFromForgotPassword, emailEditText.getText());
        }
      ).setNegativeButton(getString(R.string.fragment_signup_dialog_to_signup_button_password_rewrite), (dialog, which) -> {
// Do Nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));
  }

  private void returningFromForgotPassword(Bundle bundle, int responseCode) {
    Log.e(TAG, "returningFromForgotPassword() responseCode: " + responseCode);

    if (responseCode == Activity.RESULT_OK) {
      String successMessage = bundle.getString(FORGOT_PASSWORD_KEY, "");
      showSnackBar(successMessage);
    }
  }

  private TextViewInDialog getTextViewForDialog() {
    return new TextViewInDialog(getContext());
  }

  private void doLogin() {
    if (locallyValidateForms()) {
      showLoading();
      Subscription subscription = presenterLogin.signIn(emailEditText.getText(), passwordEditText.getText())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(result -> onSignInSuccess(),
          error -> onSignInFailure(error));
      compositeSubscription.add(subscription);
    }
  }

  private void onSignInSuccess() {
    this.analytics.trackUser(emailEditText.getText());
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_login),
      getString(R.string.analytics_event_email),
      getString(R.string.analytics_label_login_email)
    ));
    hideLoading();
    getActivity().setResult(Activity.RESULT_OK, new Intent());
    getActivity().finish();
  }

  private void onSignInFailure(Throwable throwable) {
    hideLoading();
    showGenericErrorSnackbar(throwable);
  }
}
