package br.com.taqtile.android.app.domain.questionnaires.models;

import com.google.common.base.Joiner;

import java.util.List;
import java.util.stream.Collectors;

import java8.util.StringJoiner;

/**
 * Created by taqtile on 07/04/17.
 */

public class AnswerParams {
  private String answer;
  private Integer questionId;

  private AnswerParams(Integer questionId) {
    this.questionId = questionId;
  }

  public AnswerParams(String answer, Integer questionId) {
    this(questionId);
    this.answer = answer;
  }

  public AnswerParams(List<String> answers, Integer questionId) {
    this(questionId);
    this.answer = Joiner.on(";").join(answers);
  }

  public String getAnswer() {
    return answer;
  }

  public Integer getQuestionId() {
    return questionId;
  }
}
