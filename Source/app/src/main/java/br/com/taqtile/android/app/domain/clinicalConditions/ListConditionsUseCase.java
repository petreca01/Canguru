package br.com.taqtile.android.app.domain.clinicalConditions;

import br.com.taqtile.android.app.data.clinicalcondition.ClinicalConditionRepository;
import br.com.taqtile.android.app.domain.clinicalConditions.mappers.ClinicalConditionResultToViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionViewModel;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ListConditionsUseCase
  extends BaseUseCase<List<ClinicalConditionViewModel>, Integer, ClinicalConditionRepository> {

  public ListConditionsUseCase(ClinicalConditionRepository clinicalConditionRepository) {
    super(clinicalConditionRepository);
  }

  @Override public Observable<List<ClinicalConditionViewModel>> execute(Integer gestationWeeks) {
    return getRepository().list()
      .map(conditions -> ClinicalConditionResultToViewModelMapper.perform(conditions, gestationWeeks));
  }
}
