package br.com.taqtile.android.app.data.symptoms.mappers;

import br.com.taqtile.android.app.data.common.mappers.UserRemoteResponseToUserResultMapper;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.ReportSymptomRemoteResponse;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomResult;
import br.com.taqtile.android.app.domain.symptoms.models.SubSymptomResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class ReportSymptomRemoteResponseToReportSymptomResultMapper {

  public static ReportSymptomResult perform(ReportSymptomRemoteResponse reportSymptomRemoteResponse) {
    UserResult userResult = UserRemoteResponseToUserResultMapper.perform(reportSymptomRemoteResponse.getUser());

    SubSymptomResult subSymptomResult = null;

    if(reportSymptomRemoteResponse.getSymptom() != null) {
      subSymptomResult = SubSymptomItemRemoteResponseToSubSymptomResultMapper
        .perform(reportSymptomRemoteResponse.getSymptom());
    }

    return new ReportSymptomResult(
      reportSymptomRemoteResponse.getId(),
      userResult,
      reportSymptomRemoteResponse.getUserId(),
      subSymptomResult,
      reportSymptomRemoteResponse.getSymptomId(),
      reportSymptomRemoteResponse.getDescription(),
      reportSymptomRemoteResponse.getDate(),
      reportSymptomRemoteResponse.getGestationAgeWeeks(),
      reportSymptomRemoteResponse.getGestationAgeDays()
    );
  }
}
