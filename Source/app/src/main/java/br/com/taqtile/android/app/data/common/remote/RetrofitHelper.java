package br.com.taqtile.android.app.data.common.remote;


import br.com.taqtile.android.app.support.Constants;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by taqtile on 3/3/17.
 */

public class RetrofitHelper {

  public static Retrofit.Builder getRetrofitBuilder() {

    String url = new Constants().getBaseUrl();

    return new Retrofit.Builder()
      .addConverterFactory(StringConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
      .addConverterFactory(GsonConverterFactory.create())
      .baseUrl(url);

  }
}
