package br.com.taqtile.android.app.presentation.common.post;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 12/04/17.
 */

public interface PostDetailContract {

  interface Presenter extends BasePresenter {
    Observable<List<ListingsViewModel>> fetchPostModels(Integer postId);
    Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked);
    Observable<List<ListingsViewModel>> postComment(Integer postId, String message);
  }
}
