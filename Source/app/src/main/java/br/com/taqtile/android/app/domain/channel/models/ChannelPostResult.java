package br.com.taqtile.android.app.domain.channel.models;

import java.util.List;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelPostResult {
  private ChannelPost channelPost;
  private ChannelResult channelResult;
  private List<ChannelPostReply> replies;

  public ChannelPost getChannelPost() {
    return channelPost;
  }

  public void setChannelPost(ChannelPost channelPost) {
    this.channelPost = channelPost;
  }

  public ChannelResult getChannelResult() {
    return channelResult;
  }

  public void setChannelResult(ChannelResult channelResult) {
    this.channelResult = channelResult;
  }

  public List<ChannelPostReply> getReplies() {
    return replies;
  }

  public void setReplies(List<ChannelPostReply> replies) {
    this.replies = replies;
  }
}

