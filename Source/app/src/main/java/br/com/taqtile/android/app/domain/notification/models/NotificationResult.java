package br.com.taqtile.android.app.domain.notification.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class NotificationResult {

  private boolean loaded;

  private Integer id;

  private Integer userId;

  private Integer doctorId;

  private Integer operatorId;

  private String type;

  private boolean important;

  private String content;

  private String entity;

  private Integer entityId;

  private String action;

  private Integer userPerformerId;

  private boolean visualized;

  private String visualizedAt;

  private String createdAt;

  private String updatedAt;

  private String userPerformerName;

  private String userPerformerPicture;

  public NotificationResult(boolean loaded, Integer id, Integer userId, Integer doctorId,
    Integer operatorId, String type, boolean important, String content, String entity,
    Integer entityId, String action, Integer userPerformerId, boolean visualized,
    String visualizedAt, String createdAt, String updatedAt, String userPerformerName,
    String userPerformerPicture) {
    this.loaded = loaded;
    this.id = id;
    this.userId = userId;
    this.doctorId = doctorId;
    this.operatorId = operatorId;
    this.type = type;
    this.important = important;
    this.content = content;
    this.entity = entity;
    this.entityId = entityId;
    this.action = action;
    this.userPerformerId = userPerformerId;
    this.visualized = visualized;
    this.visualizedAt = visualizedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.userPerformerName = userPerformerName;
    this.userPerformerPicture = userPerformerPicture;
  }

  public boolean isLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public Integer getUserId() {
    return userId;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public String getType() {
    return type;
  }

  public boolean isImportant() {
    return important;
  }

  public String getContent() {
    return content;
  }

  public String getEntity() {
    return entity;
  }

  public Integer getEntityId() {
    return entityId;
  }

  public String getAction() {
    return action;
  }

  public Integer getUserPerformerId() {
    return userPerformerId;
  }

  public boolean isVisualized() {
    return visualized;
  }

  public String getVisualizedAt() {
    return visualizedAt;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getUserPerformerName() {
    return userPerformerName;
  }

  public String getUserPerformerPicture() {
    return userPerformerPicture;
  }
}
