package br.com.taqtile.android.app.data.agenda.remote;

import br.com.taqtile.android.app.data.agenda.remote.models.request.AppointmentRemoteRequest;
import br.com.taqtile.android.app.data.agenda.remote.models.request.FinishAppointmentRemoteRequest;
import br.com.taqtile.android.app.data.agenda.remote.models.request.RemoveAppointmentRemoteRequest;
import br.com.taqtile.android.app.data.agenda.remote.models.request.ScheaduleRemoteRequest;
import br.com.taqtile.android.app.data.agenda.remote.models.response.AgendaItemRemoteResponse;
import br.com.taqtile.android.app.data.agenda.remote.models.response.AppointmentItemRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/21/17.
 */

public class AgendaRemoteDataSource extends BaseRemoteDataSource {
  private Observable<AgendaServices> services;

  public AgendaRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(AgendaServices.class);
  }

  public Observable<BaseRemoteResponse<List<AgendaItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(AgendaServices::list));
  }

  public Observable<BaseRemoteResponse<AgendaItemRemoteResponse>> schedule(
    ScheaduleRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.schedule(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<AppointmentItemRemoteResponse>> createAppointment(
    AppointmentRemoteRequest request) {
    return performRequest(
      services.flatMap(services -> services.createAppointment(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<AgendaItemRemoteResponse>> finish(
    FinishAppointmentRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.finish(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<String>> remove(RemoveAppointmentRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.remove(request.getFormData())));
  }
}
