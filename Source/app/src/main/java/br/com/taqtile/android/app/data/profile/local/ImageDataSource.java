package br.com.taqtile.android.app.data.profile.local;

import br.com.taqtile.android.app.data.profile.local.models.UserImagesRequest;
import br.com.taqtile.android.app.data.profile.local.models.UserImagesResponse;
import java.io.File;
import rx.Observable;

/**
 * Created by taqtile on 17/04/17.
 */

public class ImageDataSource {

  public Observable<UserImagesResponse> getFile(UserImagesRequest userImagesRequest) {
    File coverPhoto = null;
    File profilePicture = null;

    if (isValidPath(userImagesRequest.getCoverPhotoPath())) {
      coverPhoto = new File(userImagesRequest.getCoverPhotoPath());
    }

    if (isValidPath(userImagesRequest.getProfilePicturePath())) {
      profilePicture = new File(userImagesRequest.getProfilePicturePath());
    }

    return Observable.just(new UserImagesResponse(coverPhoto, profilePicture));
  }

  private boolean isValidPath(String path) {
    return path != null && !path.isEmpty();
  }
}
