package br.com.taqtile.android.app.presentation.tutorial;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.presentation.tutorial.showcase.builders.ShowCaseBuilder;
import br.com.taqtile.android.app.presentation.tutorial.showcase.paints.TransparentPaintBuilder;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.TransparentCircle;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 5/16/17.
 */

public class TutorialComponent extends FrameLayout implements ViewPager.OnPageChangeListener {

  private Subscription saveSubscription;

  public interface TutorialPageListener {
    void onPageChanged(int bottomNavIndex);

    void onTutorialClose();
  }

  private static final
  @ColorRes
  int BACKGROUND_COLOR = R.color.color_fab_overlay;
  private static final
  @DimenRes
  int CIRCLE_RADIUS = R.dimen.tutorial_focus_circle_radius;
  private static final int FOCUS_ANIMATION_DURATION = 250;

  @BindView(R.id.component_tutorial_background)
  ImageView background;
  @BindView(R.id.component_tutorial_close_button)
  ImageView closeButton;

  @BindView(R.id.component_tutorial_circle_page_indicator)
  CirclePageIndicator pageIndicator;
  @BindView(R.id.component_tutorial_view_pager)
  ViewPager viewPager;

  TutorialViewPagerAdapter viewPagerAdapter;
  List<TutorialViewPagerCellModel> tutorialContent;
  private View viewInFocus;

  private TutorialPageListener tutorialPageListener;

  public TutorialComponent(Context context, List<TutorialViewPagerCellModel> tutorialContent) {
    super(context);
    this.tutorialContent = tutorialContent;
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_tutorial, this, true);
    ButterKnife.bind(this, v);
    setupViewPager(context);
    setupInteractions();
  }

  private void setupViewPager(Context context) {

    viewPagerAdapter = new TutorialViewPagerAdapter(context, tutorialContent);
    viewPager.setAdapter(viewPagerAdapter);
    pageIndicator.setViewPager(viewPager);
    viewPager.addOnPageChangeListener(this);

  }

  private void setupInteractions() {
    closeButton.setOnClickListener(v -> this.onCloseButtonClicked());
    background.setOnClickListener(v -> this.onBackgroundTap());
  }

  private void onCloseButtonClicked() {
    if (tutorialPageListener != null) {
      saveSubscription = Toolbox.getInstance().saveMainTutorial()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onSaveTutorialSuccess, this::onSaveTutorialFailure);
    }
  }

  private void onSaveTutorialSuccess(EmptyResult emptyResult) {
    saveSubscription.unsubscribe();
    tutorialPageListener.onTutorialClose();
  }

  private void onSaveTutorialFailure(Throwable throwable) {
    saveSubscription.unsubscribe();
  }

  private void onBackgroundTap() {
    if (viewPager.getCurrentItem() + 1 < tutorialContent.size()) {
      viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    } else if (viewPager.getCurrentItem() + 1 == tutorialContent.size()) {
      this.onCloseButtonClicked();
    }
  }

  public void focusOnView(View targetView) {
    viewInFocus = targetView;
    this.animateAppearFocus(targetView);
  }

  private ValueAnimator animateDisappearFocus(View view) {
    final ValueAnimator animator = ValueAnimator.ofInt(getResources().getDimensionPixelOffset(CIRCLE_RADIUS), 0);
    this.animateFocusChange(animator, view);
    return animator;
  }

  private void animateAppearFocus(View view) {
    final ValueAnimator animator = ValueAnimator.ofInt(0, getResources().getDimensionPixelOffset(CIRCLE_RADIUS));
    this.animateFocusChange(animator, view);
  }

  private void animateFocusChange(ValueAnimator animator, View view) {
    animator.setDuration(FOCUS_ANIMATION_DURATION);
    animator.addUpdateListener((animation) -> setBackgroundShowCase(view, (int) animation.getAnimatedValue()));
    animator.start();

  }

  private void setBackgroundShowCase(View view, int radius) {
    background.setImageBitmap(
      ShowCaseBuilder.build(getContext(),
        BACKGROUND_COLOR,
        view,
        new TransparentCircle(new TransparentCircle.DrawingParams(radius, TransparentPaintBuilder.build()))
      )
    );
  }

  public void setNoFocus() {
    viewInFocus = null;
    background.setImageResource(R.color.color_fab_overlay);
  }

  public void setTutorialPageListener(TutorialPageListener tutorialPageListener) {
    this.tutorialPageListener = tutorialPageListener;
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    if (viewInFocus != null) {
      ValueAnimator animator = animateDisappearFocus(viewInFocus);
      animator.addListener(buildEndAnimationListener(position));
    } else {
      tutorialPageListener.onPageChanged(position);
    }
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  private Animator.AnimatorListener buildEndAnimationListener(int position) {
    return new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animation) {

      }

      @Override
      public void onAnimationEnd(Animator animation) {
        if (tutorialPageListener != null) {
          tutorialPageListener.onPageChanged(position);
        }
      }

      @Override
      public void onAnimationCancel(Animator animation) {

      }

      @Override
      public void onAnimationRepeat(Animator animation) {

      }
    };
  }

}
