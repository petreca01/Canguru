package br.com.taqtile.android.app.domain.symptoms.mapper;

import br.com.taqtile.android.app.domain.symptoms.models.UserSymptomResult;
import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 12/05/17.
 */

public class UserSymptomResultToSymptomHistoryViewModelMapper {

  public static List<SymptomHistoryViewModel> perform(List<UserSymptomResult> userSymptomResults) {
    return StreamSupport.stream(userSymptomResults)
      .map(UserSymptomResultToSymptomHistoryViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static SymptomHistoryViewModel map(UserSymptomResult userSymptomResult) {
    return new SymptomHistoryViewModel(userSymptomResult.getDate(), userSymptomResult.getSymptomName());
  }
}
