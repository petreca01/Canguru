package br.com.taqtile.android.app.presentation.birthplan;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.questionnaires.models.BirthPlanQuestionsViewModel;
import br.com.taqtile.android.app.listings.cells.TextWithLabelAndDotCell;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.dialog.EmailFormInDialog;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 10/05/17.
 */

public class BirthplanFragment extends AppBaseFragment implements MenuItem.OnMenuItemClickListener,
  OverflowButton.OverflowButtonListener, TextWithLabelAndDotCell.Listener {

  private static final String ALL = "all";
  @BindView(R.id.fragment_birthplan_container)
  LinearLayout birthPlanContainer;

  @BindView(R.id.fragment_birthplan_initial_screen_container)
  FrameLayout initialScreenContainer;

  @BindView(R.id.fragment_birthplan_filled_questionnaire_container)
  LinearLayout filledQuestionnaireContainer;

  @BindView(R.id.fragment_birthplan_completed_percentage_questionnaire_text)
  CustomTextView questionnaireText;

  @BindView(R.id.fragment_birthplan_pie_chart_percentage)
  CustomTextView pieChartPercentageText;

  @BindView(R.id.fragment_birthplan_pie_chart)
  PieChart answeredQuestionsChart;

  @BindView(R.id.fragment_birthplan_answered_questions_container)
  LinearLayout answeredQuestionsContainer;

  @BindView(R.id.fragment_birthplan_start_questionnaire_button)
  TemplateButton startOrContinueQuestionnaireButton;
  private View.OnClickListener startOrContinueQuestionnaireButtonListener;

  @BindView(R.id.fragment_birthplan_send_questionnaire_button)
  TemplateButton sendQuestionnaireButton;

  @BindView(R.id.fragment_birthplan_loading)
  ProgressBarLoading loading;

  private BirthplanContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;
  private final static int FIRST_MENU_ITEM_POSITION = 0;

  private boolean hasQuestionnaireBeenStarted;
  private double answeredPercentage;
  private String userEmail;
  private Subscription birthPlanQuestionnaire;
  private Subscription restartBirthplanSubscription;
  private List<QuestionViewModel> questionnaire;
  private List<QuestionViewModel> answeredQuestionnaire;
  private Subscription sendBirthPlanQuestionAnswersListSubscription;

  public static BirthplanFragment newInstance() {
    return new BirthplanFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_birthplan, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupData();
  }

  private void setupView() {
    this.questionnaire = new ArrayList<>();
    this.answeredQuestionnaire = new ArrayList<>();
  }

  private void setupData() {
    Subscription subscription = Toolbox.getInstance()
      .getUserEmail()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        (email) -> { this.userEmail = email; }
      );
    compositeSubscription.add(subscription);
  }

  private void fetchBirthPlanQuestionnaire() {
    showLoading();
    birthPlanQuestionnaire = presenter.fetchBirthPlanQuestionAnswersList()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onBirthPlanQuestionnaireFetchSuccess,
        this::onBirthPlanQuestionnaireFetchFailure, this::hideLoading);
    compositeSubscription.add(birthPlanQuestionnaire);
  }

  private void onBirthPlanQuestionnaireFetchSuccess(BirthPlanQuestionsViewModel birthPlanQuestionsViewModel) {
    clearLists();
    this.questionnaire.addAll(birthPlanQuestionsViewModel.getQuestionViewModels());
    this.answeredQuestionnaire.addAll(birthPlanQuestionsViewModel.getAnsweredQuestionViewModels());

    setUpQuestionnaire();
  }

  private void clearLists() {
    this.questionnaire.clear();
    this.answeredQuestionnaire.clear();
    this.answeredQuestionsContainer.removeAllViews();
  }

  private void setUpQuestionnaire() {
    hasQuestionnaireBeenStarted = checkIfQuestionnaireHasBeenStarted();
    setupFilledQuestionnaireContainerVisibility();
    setupToolbarMenu();
  }

  private boolean checkIfQuestionnaireHasBeenStarted() {
    return this.answeredQuestionnaire != null && !this.answeredQuestionnaire.isEmpty();
  }

  private double calculatePercentageAnswered() {
    return Math.round((double) this.answeredQuestionnaire.size() /
      (double) questionnaire.size() * 100);
  }

  private void onBirthPlanQuestionnaireFetchFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
    fetchBirthPlanQuestionnaire();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_birthplan_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
      setupToolbarMenu();
    }
  }

  private void setupToolbarMenu() {
    if (hasQuestionnaireBeenStarted) {
      customToolbar.inflateOptionsMenu(R.menu.menu_overflow, this::onMenuItemClick);
      customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION).setActionView(getOverflowButton());
    } else {
      customToolbar.hideOptionsMenu();
    }
  }

  private OverflowButton getOverflowButton() {
    OverflowButton overflowButton = new OverflowButton(getContext());
    setupOverflowButton(overflowButton);
    return overflowButton;
  }

  private void setupOverflowButton(OverflowButton overflowButton) {
    List<String> overflowOptions = new ArrayList();
    overflowOptions.add(getResources().getString(
      R.string.fragment_birthplan_overflow_text));
    overflowButton.onMenuItemSelected(this);
    overflowButton.setMenuList(overflowOptions);
  }

  private void addAnsweredQuestion(String question, String answer, int questionId) {
    TextWithLabelAndDotCell answeredQuestion = new TextWithLabelAndDotCell(getContext());
    answeredQuestion.setHeader(question);
    answeredQuestion.setLabel(answer);
    answeredQuestion.setViewIndex(questionId);
    answeredQuestion.setListener(this::onViewClick);
    answeredQuestionsContainer.addView(answeredQuestion);
  }

  private void setupFilledQuestionnaireContainerVisibility() {
    if (hasQuestionnaireBeenStarted) {

      setupContinueQuestionnaireButton();
      setupSendQuestionnaireButton();
      addAnsweredQuestions();
      this.answeredPercentage = calculatePercentageAnswered();
      setupPieChart();
      setQuestionnaireViewMode();
    } else {
      setInitialScreenViewMode();
      setupStartQuestionnaireButton();

      setupToolbarMenu();
    }
    startOrContinueQuestionnaireButton.setOnClickListener(startOrContinueQuestionnaireButtonListener);
  }

  private void setInitialScreenViewMode() {
    sendQuestionnaireButton.setVisibility(View.GONE);
    filledQuestionnaireContainer.setVisibility(View.GONE);
    initialScreenContainer.setVisibility(View.VISIBLE);
  }

  private void setQuestionnaireViewMode() {
    sendQuestionnaireButton.setVisibility(View.VISIBLE);
    filledQuestionnaireContainer.setVisibility(View.VISIBLE);
    initialScreenContainer.setVisibility(View.GONE);
  }

  private void setupSendQuestionnaireButton() {
    sendQuestionnaireButton.setOnClickListener(v -> this.showSendDialog(this.userEmail));
  }

  private void showSendDialog(@Nullable String defaultValue) {
    final EmailFormInDialog input = getEditTextForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_birth_plan_send_questionnaire_text))
      .setView(input)
      .setPositiveButton(getString(R.string.send), (dialog, which) -> {
        this.sendQuestionnaire(input.getFormText());
        showSnackBar(getString(R.string.fragment_birth_plan_questionnaire_send_success_text));
      })
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
        // do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(getResources().getColor(R.color.color_gray_dark));

  }

  private EmailFormInDialog getEditTextForDialog() {
    return new EmailFormInDialog(getContext());
  }

  private void sendQuestionnaire(String email) {
    if (email != null && !email.isEmpty()) {
      sendBirthPlanQuestionAnswersListSubscription = presenter.sendBirthPlanQuestionAnswersList(email)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onSendQuestionnaireSuccess, this::onSendQuestionnaireFailure);
      compositeSubscription.add(sendBirthPlanQuestionAnswersListSubscription);
    }
  }

  private void onSendQuestionnaireSuccess(BirthPlanQuestionsViewModel birthPlanQuestionsViewModel) {
  }

  private void onSendQuestionnaireFailure(Throwable throwable) {
    showSnackBar(throwable.getMessage());
  }

  private void addAnsweredQuestions() {
    for (QuestionViewModel question : this.answeredQuestionnaire) {
      addAnsweredQuestion(question.getQuestion(), question.getAnswer(), question.getQuestionId());
    }
  }

  private void setupContinueQuestionnaireButton() {
    startOrContinueQuestionnaireButton.setText(getResources().getString(R.string.fragment_birthplan_initial_screen_continue_questionnaire_button_text));
    startOrContinueQuestionnaireButtonListener = v -> onContinueQuestionnaireButtonClick();
  }

  private void setupStartQuestionnaireButton() {
    startOrContinueQuestionnaireButton.setText(getResources().getString(R.string.fragment_birthplan_initial_screen_start_questionnaire_button_text));
    startOrContinueQuestionnaireButtonListener = v -> onStartQuestionnaireButtonClick();
  }

  //region Pie chart

  private void setupPieChart() {

    setPieChartData(calculatePieChartProportion(answeredPercentage), answeredQuestionsChart);
    setQuestionnairePercentageText(answeredPercentage);
    animatePieChart(answeredQuestionsChart);
  }

  private void setQuestionnairePercentageText(double questionnairePercentageText) {
    this.questionnaireText.setText(getResources().
      getString(R.string.fragment_birthplan_pie_chart_questionnaire_text, questionnairePercentageText));
    this.pieChartPercentageText.setText(String.format(
      getString(R.string.fragment_diary_piechart_percent_value), questionnairePercentageText));
  }

  private ArrayList<Double> calculatePieChartProportion(Double filledPercentage) {
    ArrayList<Double> pieChartMock = new ArrayList<>();
    pieChartMock.add(filledPercentage);
    pieChartMock.add(100 - filledPercentage);
    return pieChartMock;
  }

  public void animatePieChart(PieChart pieChart) {
    pieChart.animateXY(0, 700);
  }

  public void setPieChartData(ArrayList<Double> data, PieChart pieChart) {
    ArrayList<PieEntry> chartValues = new ArrayList<>();
    ArrayList<Integer> colors = getColors();
    if (data.size() == 0) {
      chartValues.add(new PieEntry(.0f));
    } else {
      for (double pieChartValue : data) {
        chartValues.add(new PieEntry(((float) pieChartValue)));
      }
    }
    setupPieChart(chartValues, colors, pieChart);
  }

  private ArrayList<Integer> getColors() {
    ArrayList<Integer> colors = new ArrayList<>();
    colors.add(ContextCompat.getColor(getContext(), R.color.color_brand_orange));
    colors.add(ContextCompat.getColor(getContext(), R.color.color_birthplan_pie_chart_empty));
    return colors;
  }

  private void setupPieChart(ArrayList<PieEntry> chartValues, ArrayList<Integer> colors, PieChart pieChart) {

    // TODO: 08/05/17 Pass the value of the holeRadius to dps
    TypedValue typeHoleRadius = new TypedValue();
    getResources().getValue(R.dimen.pie_chart_default_hole_radius, typeHoleRadius, true);
    float holeRadius = typeHoleRadius.getFloat();

    PieDataSet pieDataSet = new PieDataSet(chartValues, "");

    pieDataSet.setColors(colors);
    pieDataSet.setDrawValues(false);

    PieData pieData = new PieData(pieDataSet);

    Description description = new Description();
    description.setText("");
    pieChart.setVisibility(View.VISIBLE);
    pieChart.setDescription(description);
    pieChart.setDrawCenterText(false);
    pieChart.setDrawEntryLabels(false);
    pieChart.setHoleRadius(holeRadius);
    pieChart.setHoleColor(Color.TRANSPARENT);
    pieChart.setTransparentCircleRadius(0);
    pieChart.getLegend().setEnabled(false);
    pieChart.setTouchEnabled(false);

    pieChart.setData(pieData);
    pieChart.invalidate();
    pieChart.notifyDataSetChanged();
  }

  //end region

  public void setPresenter(BirthplanContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    return false;
  }

  @Override
  public void onItemSelected(String item) {
    AlertDialog restartConfirmationDialog = new AlertDialog.Builder(getContext())
      .setMessage(getResources().getString(R.string.fragment_birthplan_restart_birthplan_dialog_body))
      .setTitle(getResources().getString(R.string.fragment_birthplan_restart_birthplan_dialog_title))
      .setPositiveButton(getResources().getString(R.string.restart), (dialog, which) -> onRestartBirthplanButtonClick())
      .setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {
        // do nothing
      })
      .show();
  }

  private void onRestartBirthplanButtonClick() {
    showLoading();
    restartBirthplanSubscription = presenter.restartBirthPlanAnswers(ALL)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onRestartBirthplanSuccess, this::onRestartBirthplanFailure,
        this::hideLoading);
    compositeSubscription.add(restartBirthplanSubscription);
  }

  private void onRestartBirthplanSuccess(BirthPlanQuestionsViewModel birthPlanQuestionsViewModel) {
    onBirthPlanQuestionnaireFetchSuccess(birthPlanQuestionsViewModel);
  }

  private void onRestartBirthplanFailure(Throwable throwable) {
    showSnackBar(throwable.getMessage());
  }

  private void onStartQuestionnaireButtonClick() {
    navigationManager.showBirthPlanQuestionnaireUI(this.questionnaire, null);
  }

  private void onContinueQuestionnaireButtonClick() {
    navigationManager.showBirthPlanQuestionnaireUI(this.questionnaire, getLastAnsweredQuestion());
  }


  private String getLastAnsweredQuestion() {
    return String.valueOf(StreamSupport.stream(this.questionnaire)
      .filter(question -> question.getAnswer() != null)
      .reduce((first, last) -> last).get().getQuestionId());
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void onViewClick(int id) {
    navigationManager.showBirthPlanQuestionnaireUI(this.questionnaire, String.valueOf(id));
  }

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    loading.setVisibility(View.GONE);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_birthplan_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    fetchBirthPlanQuestionnaire();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_birth_plan),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
