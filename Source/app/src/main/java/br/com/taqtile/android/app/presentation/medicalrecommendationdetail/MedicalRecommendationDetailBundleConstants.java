package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

/**
 * Created by taqtile on 09/05/17.
 */

public class MedicalRecommendationDetailBundleConstants {
  protected static final String MEDICAL_RECOMMENDATION_KEY = "medicalRecommendationKey";
}
