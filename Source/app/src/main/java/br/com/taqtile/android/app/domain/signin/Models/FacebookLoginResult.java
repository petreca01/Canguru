package br.com.taqtile.android.app.domain.signin.Models;

/**
 * Created by taqtile on 25/04/17.
 */

public class FacebookLoginResult {

  private Boolean success;

  private String email;

  public FacebookLoginResult(Boolean success, String email) {
    this.success = success;
    this.email = email;
  }

  public Boolean isSuccessful() {
    return success;
  }

  public String getEmail() {
    return email;
  }
}
