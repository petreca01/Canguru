package br.com.taqtile.android.app.domain.professional;

import br.com.taqtile.android.app.data.professional.ProfessionalRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalAuthorizationParams;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class AuthorizeProfessionalUseCase extends BaseUseCase<String,
  ProfessionalAuthorizationParams, ProfessionalRepository> {

  public AuthorizeProfessionalUseCase(ProfessionalRepository professionalRepository) {
    super(professionalRepository);
  }

  @Override
  public Observable<String> execute(ProfessionalAuthorizationParams professionalAuthorizationParams) {
    return getRepository().authorizeAccess(professionalAuthorizationParams);
  }
}
