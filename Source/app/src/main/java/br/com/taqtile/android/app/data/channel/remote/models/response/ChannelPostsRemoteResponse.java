package br.com.taqtile.android.app.data.channel.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostsRemoteResponse {
    @SerializedName("channel")
    private ChannelItemRemoteResponse channel;
    @SerializedName("total_posts")
    private Integer totalPosts;
    @SerializedName("posts")
    private List<ChannelPostItemRemoteResponse> posts;
    @SerializedName("posts_in_page")
    private Integer postsInPage;
    @SerializedName("search")
    private String search;


    public ChannelItemRemoteResponse getChannel() {
        return channel;
    }

    public Integer getTotalPosts() {
        return totalPosts;
    }

    public List<ChannelPostItemRemoteResponse> getPosts() {
        return posts;
    }

    public Integer getPostsInPage() {
        return postsInPage;
    }

    public String getSearch() {
        return search;
    }
}
