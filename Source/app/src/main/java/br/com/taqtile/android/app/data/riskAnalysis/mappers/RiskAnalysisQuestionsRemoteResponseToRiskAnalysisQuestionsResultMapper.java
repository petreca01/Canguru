package br.com.taqtile.android.app.data.riskAnalysis.mappers;

import br.com.taqtile.android.app.data.questionnaires.mappers.QuestionItemRemoteResponseListToQuestionItemResultListMapper;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisQuestionsRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionsResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taqtile on 07/04/17.
 */

public class RiskAnalysisQuestionsRemoteResponseToRiskAnalysisQuestionsResultMapper {

  public static RiskAnalysisQuestionsResult perform(
    RiskAnalysisQuestionsRemoteResponse riskAnalysisQuestionsRemoteResponse) {

    List<QuestionItemResult> aboutYou = new ArrayList<>();
    if (riskAnalysisQuestionsRemoteResponse.getAboutYou() != null) {
      aboutYou = QuestionItemRemoteResponseListToQuestionItemResultListMapper.perform(
        riskAnalysisQuestionsRemoteResponse.getAboutYou());
    }

    List<QuestionItemResult> aboutYourObstetricalPast = new ArrayList<>();
    if (riskAnalysisQuestionsRemoteResponse.getAboutYourObstetricalPast() != null) {
      aboutYourObstetricalPast = QuestionItemRemoteResponseListToQuestionItemResultListMapper.perform(
        riskAnalysisQuestionsRemoteResponse.getAboutYourObstetricalPast());
    }

    List<QuestionItemResult> aboutYourFamily = new ArrayList<>();
    if (riskAnalysisQuestionsRemoteResponse.getAboutYourFamily() != null) {
      aboutYourFamily = QuestionItemRemoteResponseListToQuestionItemResultListMapper.perform(
        riskAnalysisQuestionsRemoteResponse.getAboutYourFamily());
    }

    List<QuestionItemResult> aboutYourHealth = new ArrayList<>();
    if (riskAnalysisQuestionsRemoteResponse.getAboutYourHealth() != null) {
      aboutYourHealth = QuestionItemRemoteResponseListToQuestionItemResultListMapper.perform(
        riskAnalysisQuestionsRemoteResponse.getAboutYourHealth());
    }

    return new RiskAnalysisQuestionsResult(aboutYou, aboutYourObstetricalPast, aboutYourFamily,
      aboutYourHealth);
  }
}
