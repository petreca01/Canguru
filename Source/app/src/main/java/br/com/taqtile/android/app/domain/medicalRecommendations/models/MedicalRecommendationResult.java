package br.com.taqtile.android.app.domain.medicalRecommendations.models;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class MedicalRecommendationResult {

  private boolean loaded;
  private Integer id;
  private Integer doctorId;
  private String doctorName;
  private String name;
  private String recommendedAt;
  private String acknowledgedAt;
  private String content;

  public MedicalRecommendationResult(boolean loaded, Integer id, Integer doctorId,
                                     String doctorName, String name, String recommendedAt,
                                     String acknowledgedAt, String content) {
    this.loaded = loaded;
    this.id = id;
    this.doctorId = doctorId;
    this.doctorName = doctorName;
    this.name = name;
    this.recommendedAt = recommendedAt;
    this.acknowledgedAt = acknowledgedAt;
    this.content = content;
  }

  public boolean getLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public String getDoctorName() {
    return doctorName;
  }

  public String getName() {
    return name;
  }

  public String getRecommendedAt() {
    return recommendedAt;
  }

  public String getAcknowledgedAt() {
    return acknowledgedAt;
  }

  public String getContent() {
    return content;
  }
}
