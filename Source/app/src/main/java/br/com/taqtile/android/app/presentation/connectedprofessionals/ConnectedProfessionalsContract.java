package br.com.taqtile.android.app.presentation.connectedprofessionals;

import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public interface ConnectedProfessionalsContract {

  interface Presenter extends BasePresenter {
    Observable<List<ConnectedProfessionalViewModel>> fetchProfessionals();
  }

}
