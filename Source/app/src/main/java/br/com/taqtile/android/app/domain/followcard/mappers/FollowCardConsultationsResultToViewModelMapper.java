package br.com.taqtile.android.app.domain.followcard.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.followcard.models.FollowCardConsultationsResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardConsultationsViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardConsultationsResultToViewModelMapper {

  public static List<FollowCardConsultationsViewModel> perform(
    List<FollowCardConsultationsResult> followCardConsultationsResults) {
    return StreamSupport.stream(followCardConsultationsResults)
      .filter(Objects::nonNull)
      .map(FollowCardConsultationsResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardConsultationsViewModel map(
    FollowCardConsultationsResult followCardConsultationsResult) {
    return new FollowCardConsultationsViewModel(
      followCardConsultationsResult.getId(),
      followCardConsultationsResult.getDoctorId(),
      followCardConsultationsResult.getPatientId(),
      followCardConsultationsResult.getUsuarioId(),
      followCardConsultationsResult.getDate(),
      followCardConsultationsResult.getPregnancyWeeks(),
      followCardConsultationsResult.getPregnancyDays(),
      followCardConsultationsResult.getHeight(),
      followCardConsultationsResult.getWeight(),
      followCardConsultationsResult.getReason(),
      followCardConsultationsResult.getBloodPressureSystolic(),
      followCardConsultationsResult.getBloodPressureDiastolic(),
      followCardConsultationsResult.getUsingFerricSulfate(),
      followCardConsultationsResult.getUsingFolicAcid(),
      followCardConsultationsResult.getUterineHeight(),
      followCardConsultationsResult.getFetalPresentation(),
      followCardConsultationsResult.getFetalHeartRate(),
      followCardConsultationsResult.getFetalMovement(),
      followCardConsultationsResult.getEdema(),
      followCardConsultationsResult.getToque(),
      followCardConsultationsResult.getCreatedAt(),
      followCardConsultationsResult.getUpdatedAt(),
      followCardConsultationsResult.getDoctorName()
    );
  }
}
