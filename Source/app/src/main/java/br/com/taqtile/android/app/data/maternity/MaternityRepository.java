package br.com.taqtile.android.app.data.maternity;

import java.util.List;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.MaternityRemoteDataSource;
import br.com.taqtile.android.app.data.maternity.remote.mappers.MaternityCommentItemRemoteResponseToMaternityCommentResultMapper;
import br.com.taqtile.android.app.data.maternity.remote.mappers.MaternityCommentParamsToCommentMaternityRemoteRequestMapper;
import br.com.taqtile.android.app.data.maternity.remote.mappers.MaternityDetailsRemoteResponseToMaternityDetailsResultMapper;
import br.com.taqtile.android.app.data.maternity.remote.mappers.MaternityItemRemoteResponseListToMaternityDataResultListMapper;
import br.com.taqtile.android.app.data.maternity.remote.mappers.MaternityItemRemoteResponseToMaternityDataResultMapper;
import br.com.taqtile.android.app.data.maternity.remote.mappers.RateMaternityParamsToRateMaternityRemoteRequestMapper;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityCommentItemRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityItemRemoteResponse;
import br.com.taqtile.android.app.domain.maternity.models.DeleteCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDetailsResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityParams;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class MaternityRepository extends BaseRepository<MaternityRemoteDataSource, Void> {

  public MaternityRepository(MaternityRemoteDataSource maternityRemoteDataSource) {
    super(maternityRemoteDataSource, null);
  }

  public Observable<List<String>> listCities() {
    return getRemoteDataSource().listCities().map(BaseRemoteResponse::getResult);
  }

  public Observable<List<MaternityDataResult>> listByCity(String cityName) {
    return convertToMaternityDataResultList(getRemoteDataSource().listByCity(cityName));
  }

  public Observable<MaternityDetailsResult> getDetails(MaternityParams maternityParams) {
    return getRemoteDataSource().getDetails(maternityParams.getMaternityId())
      .map(result -> MaternityDetailsRemoteResponseToMaternityDetailsResultMapper.perform(
        result.getResult()));
  }

  public Observable<MaternityDataResult> rate(RateMaternityParams rateMaternityParams) {
    return convertToMaternityDataResult(
      getRemoteDataSource().rate(
        RateMaternityParamsToRateMaternityRemoteRequestMapper.perform(rateMaternityParams),
        rateMaternityParams.getMaternityId()));
  }

  public Observable<MaternityDataResult> followOrUnfollow(MaternityParams maternityParams) {
    return convertToMaternityDataResult(
      getRemoteDataSource().followOrUnfollow(maternityParams.getMaternityId()));
  }

  public Observable<List<MaternityCommentResult>> comment(MaternityCommentParams maternityCommentParams) {
    return convertToMaternityCommentResultList(
      getRemoteDataSource().comment(
      MaternityCommentParamsToCommentMaternityRemoteRequestMapper.perform(maternityCommentParams),
      maternityCommentParams.getMaternityId()));
  }

  public Observable<List<MaternityCommentResult>> deleteComment(
    DeleteCommentParams deleteCommentParams) {
    return convertToMaternityCommentResultList(
      getRemoteDataSource().deleteComment(
        deleteCommentParams.getMaternityId(),
        deleteCommentParams.getCommentId()));
  }

  private Observable<MaternityDataResult> convertToMaternityDataResult(
    Observable<BaseRemoteResponse<MaternityItemRemoteResponse>> observable) {
    return observable.map(
      result -> MaternityItemRemoteResponseToMaternityDataResultMapper.perform(result.getResult()));
  }

  private Observable<List<MaternityDataResult>> convertToMaternityDataResultList(
    Observable<BaseRemoteResponse<List<MaternityItemRemoteResponse>>> observable) {
    return observable.map(
      result -> MaternityItemRemoteResponseListToMaternityDataResultListMapper.perform(
        result.getResult()));
  }

  private Observable<MaternityCommentResult> convertToMaternityCommentResult(
    Observable<BaseRemoteResponse<MaternityCommentItemRemoteResponse>> observable) {
    return observable.map(
      result -> MaternityCommentItemRemoteResponseToMaternityCommentResultMapper.perform(
        result.getResult()));
  }

  private Observable<List<MaternityCommentResult>> convertToMaternityCommentResultList(
    Observable<BaseRemoteResponse<List<MaternityCommentItemRemoteResponse>>> observable) {
    return observable.map(
      result -> MaternityCommentItemRemoteResponseToMaternityCommentResultMapper.perform(
        result.getResult()));
  }
}
