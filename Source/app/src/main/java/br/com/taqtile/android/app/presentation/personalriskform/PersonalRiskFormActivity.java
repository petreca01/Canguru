package br.com.taqtile.android.app.presentation.personalriskform;

import android.content.Context;
import android.content.Intent;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.RISK_ANALYSIS_QUESTIONNAIRE_KEY;

/**
 * Created by Renato on 4/19/17.
 */

public class PersonalRiskFormActivity extends TemplateBackActivity {

  private PersonalRiskFormFragment fragment;

  @Override
  public PersonalRiskFormFragment getFragment() {
    PersonalRiskFormFragment fragment = (PersonalRiskFormFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      fragment = PersonalRiskFormFragment.newInstance();
    }
    this.fragment = fragment;

    return this.fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    PersonalRiskFormPresenter presenter = new PersonalRiskFormPresenter();
    fragment.setPresenter(presenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return presenter;
  }

  public static void navigate(Context context, String riskGroup) {
    Intent intent = new Intent(context, PersonalRiskFormActivity.class);
    intent.putExtra(RISK_ANALYSIS_QUESTIONNAIRE_KEY, riskGroup);
    context.startActivity(intent);
  }

}
