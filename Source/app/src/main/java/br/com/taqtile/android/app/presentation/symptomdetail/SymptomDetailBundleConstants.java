package br.com.taqtile.android.app.presentation.symptomdetail;

/**
 * Created by taqtile on 10/05/17.
 */

public class SymptomDetailBundleConstants {

  public static final String SYMPTOM_KEY = "symptomKey";

}
