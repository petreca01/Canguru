package br.com.taqtile.android.app.data.common.remote;

import rx.Observable;

/**
 * Created by taqtile on 3/6/17.
 */

public class ServiceFactory {

  public static <T> Observable<T> getObservable(final Class<T> classService) {
    return new RetrofitBuilder<T>()
      .withClass(classService)
      .build();
  }

  public static <T> Observable<T> getObservableAuthenticated(final Class<T> classService) {
    return new RetrofitBuilder<T>()
      .withAuthentication()
      .withClass(classService)
      .build();
  }

}
