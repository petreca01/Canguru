package br.com.taqtile.android.app.presentation.tutorial;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import br.com.taqtile.android.app.R;

/**
 * Created by taqtile on 5/19/17.
 */

public class TutorialViewPagerAdapter extends PagerAdapter {

  LayoutInflater layoutInflater;
  List<TutorialViewPagerCellModel> models;

  public TutorialViewPagerAdapter(Context context, List<TutorialViewPagerCellModel> models) {
    layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.models = models;
  }


  @Override
  public int getCount() {
    return models.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == ((FrameLayout) object);
  }

  @Override
  public Object instantiateItem(ViewGroup container, final int position) {

    TutorialViewPagerCell tutorialViewPagerCell = new TutorialViewPagerCell(container.getContext());

    tutorialViewPagerCell.setData(models.get(position));

    container.addView(tutorialViewPagerCell);

    return tutorialViewPagerCell;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((FrameLayout) object);
  }

}
