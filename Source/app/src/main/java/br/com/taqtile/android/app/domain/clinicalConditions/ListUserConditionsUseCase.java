package br.com.taqtile.android.app.domain.clinicalConditions;

import java.util.List;

import br.com.taqtile.android.app.data.clinicalcondition.ClinicalConditionRepository;
import br.com.taqtile.android.app.domain.clinicalConditions.mappers.UserClinicalConditionResultToViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionViewModel;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ListUserConditionsUseCase
  extends BaseUseCase<List<UserClinicalConditionViewModel>, Void, ClinicalConditionRepository> {

  public ListUserConditionsUseCase(ClinicalConditionRepository clinicalConditionRepository) {
    super(clinicalConditionRepository);
  }

  // TODO: Change the return type to the proper view model
  @Override
  public Observable<List<UserClinicalConditionViewModel>> execute(Void aVoid) {
    return getRepository().listUserClinicalCondition()
      .map(UserClinicalConditionResultToViewModelMapper::perform);
  }
}
