package br.com.taqtile.android.app.presentation.forgotpassword;

import android.content.Context;
import android.content.Intent;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.presentation.signup.SignUpActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_EMAIL_KEY;

/**
 * Created by taqtile on 11/10/16.
 */

public class ForgotPasswordActivity extends TemplateBackActivity {

  private ForgotPasswordFragment mFragment;

  @Override
  public ForgotPasswordFragment getFragment() {
    ForgotPasswordFragment forgotPasswordFragment = (ForgotPasswordFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (forgotPasswordFragment == null) {
      forgotPasswordFragment = forgotPasswordFragment.newInstance();
    }

    mFragment = forgotPasswordFragment;

    return mFragment;
  }

  @Override
  public BasePresenter getPresenter() {
    return new ForgotPasswordPresenter(mFragment, new NavigationHelper(this));
  }

  public static void navigate(Context context, CustomNavigationResultListener listener, String email) {
    Intent intent = new Intent(context, ForgotPasswordActivity.class);
    intent.putExtra(FORGOT_PASSWORD_EMAIL_KEY, email);

    if (context instanceof SignInActivity) {
      ((SignInActivity) context).setResultListener(listener, FORGOT_PASSWORD_CODE);
      ((SignInActivity) context).startActivityForResult(intent, FORGOT_PASSWORD_CODE);
    } else if (context instanceof SignUpActivity) {
      ((SignUpActivity) context).setResultListener(listener, FORGOT_PASSWORD_CODE);
      ((SignUpActivity) context).startActivityForResult(intent, FORGOT_PASSWORD_CODE);
    }
  }
}
