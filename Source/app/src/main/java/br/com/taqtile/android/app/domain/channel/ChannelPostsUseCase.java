package br.com.taqtile.android.app.domain.channel;

import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelPostsUseCase extends BaseUseCase<ChannelPostsResult, ChannelPostsParams, ChannelRepository> {
  private Integer currentPage;
  private static final Integer perPage = 10;

  public ChannelPostsUseCase(ChannelRepository channelRepository) {
    super(channelRepository);
    currentPage = 1;
  }

  @Override
  public Observable<ChannelPostsResult> execute(ChannelPostsParams channelPostsParams) {
    channelPostsParams.setPage(currentPage);
    channelPostsParams.setPerPage(perPage);
    return getRepository().listChannelPosts(channelPostsParams)
      .doOnNext(timelineModel -> currentPage++);
  }
}
