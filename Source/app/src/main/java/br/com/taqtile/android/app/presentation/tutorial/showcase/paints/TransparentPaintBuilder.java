package br.com.taqtile.android.app.presentation.tutorial.showcase.paints;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

/**
 * Created by taqtile on 5/30/17.
 */

public class TransparentPaintBuilder {

  public static Paint build() {
    Paint paint = new Paint();
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    paint.setAlpha(0xFF);
    return paint;
  }
}
