package br.com.taqtile.android.app.presentation.search;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.search.SearchContract;
import br.com.taqtile.android.app.presentation.search.SearchFragment;
import br.com.taqtile.android.app.presentation.search.SearchPresenter;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 3/23/17.
 */

public class SearchActivity extends TemplateBackActivity {

    private SearchFragment fragment;

    @Override
    public Fragment getFragment() {
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager()
                .findFragmentById(getFragmentContainerId());
        if (searchFragment == null) {
            searchFragment = SearchFragment.newInstance();
        }
        fragment = searchFragment;

        return fragment;
    }

    @Override
    public BasePresenter getPresenter() {
        SearchPresenter presenter = new SearchPresenter();
        fragment.setNavigationManager(new NavigationHelper(this));
        fragment.setPresenter(presenter);

        return presenter;
    }

    public static void navigate(Context context){
        Intent intent = new Intent(context, SearchActivity.class);
        context.startActivity(intent);
    }
}
