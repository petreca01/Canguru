package br.com.taqtile.android.app.presentation.symptomdetail;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.symptoms.ListSymptomsUseCase;
import br.com.taqtile.android.app.domain.symptoms.ReportSymptomUseCase;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomParams;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomDetailPresenter implements SymptomDetailContract.Presenter {

  private ReportSymptomUseCase reportSymptomUseCase;
  private UserDataUseCase userDataUseCase;
  private ListSymptomsUseCase listSymptomsUseCase;

  public SymptomDetailPresenter() {
    reportSymptomUseCase = Injection.provideReportSymptomUseCase();
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.listSymptomsUseCase = Injection.provideListSymptomsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<EmptyResult> reportSymptom(SubSymptomViewModel subSymptomViewModel) {
    return reportSymptomUseCase.execute(new ReportSymptomParams(subSymptomViewModel.getId(),
      subSymptomViewModel.getDate()));
  }

  @Override
  public Observable<UserViewModel> fetchUserData() {
    return userDataUseCase.execute(null);
  }

  @Override
  public Observable<List<SymptomViewModel>> listSymptoms(Integer gestationWeeks) {
    return listSymptomsUseCase.execute(gestationWeeks);
  }

}
