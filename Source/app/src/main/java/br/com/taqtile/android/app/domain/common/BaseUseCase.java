package br.com.taqtile.android.app.domain.common;

import rx.Observable;

/**
 * Created by taqtile on 3/6/17.
 */

public abstract class BaseUseCase <Return, Params, Repository> {
    private Repository repository;

    public BaseUseCase(Repository repository) {
        this.repository = repository;
    }

    public abstract Observable<Return> execute(Params params);

    protected Repository getRepository() {
        return repository;
    }
}
