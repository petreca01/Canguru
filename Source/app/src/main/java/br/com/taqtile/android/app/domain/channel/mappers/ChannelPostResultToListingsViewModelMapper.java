package br.com.taqtile.android.app.domain.channel.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReply;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostResult;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/20/17.
 */

public class ChannelPostResultToListingsViewModelMapper {

  public static List<ListingsViewModel> perform(ChannelPostResult channelPostResult) {
    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.add(map(channelPostResult.getChannelPost()));
    listingsViewModels.addAll(map(channelPostResult.getReplies()));
    return listingsViewModels;
  }

  private static SocialFeedChannelImplViewModel map(ChannelPost channelPost) {
    return new SocialFeedChannelImplViewModel(channelPost.getVideo(), channelPost.getImage(),
      channelPost.getId(), channelPost.getChannelId(), channelPost.getPostAuthor(),
      channelPost.getChannelName(), channelPost.getCreatedAt(), channelPost.getTitle(),
      channelPost.getPost(), channelPost.getLikes(),
      channelPost.getReplies(), channelPost.getLiked());
  }

  private static List<SocialFeedCommunityImplViewModel> map(List<ChannelPostReply> channelPostReplies) {
    return StreamSupport.stream(channelPostReplies)
      .map(ChannelPostResultToListingsViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static SocialFeedCommunityImplViewModel map(ChannelPostReply channelPostReplies) {
    return new SocialFeedCommunityImplViewModel(channelPostReplies.getId(),
      channelPostReplies.getUserId(), channelPostReplies.getUserPicture(),
      channelPostReplies.getUsername(), channelPostReplies.getCreatedAt(),
      channelPostReplies.getReply(),
      channelPostReplies.getLikes(),
      channelPostReplies.getLiked()
    );
  }

}
