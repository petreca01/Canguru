package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditUserDataParams {

  private String telephone;
  private String birthDate;
  private String cpf;
  private String operatorDocument;
  private String zipCode;
  private Integer operator;
  private String city;
  private String state;
  private String latitude;
  private String longitude;
  private Boolean locationPolicyAgreed;

  public EditUserDataParams(Integer operator) {
    this.operator = operator;
  }

  public EditUserDataParams(Boolean locationPolicyAgreed){
    this.locationPolicyAgreed = locationPolicyAgreed;
  }

  public EditUserDataParams(Integer operatorId, String telephone, String documentId, String documentType) {
    this.operator = operatorId;
    this.telephone = telephone;
    if (documentType.equals("cpf")) {
      this.cpf = documentId;
    } else {
      this.operatorDocument = documentId;
    }
  }

  public EditUserDataParams(String telephone, String birthDate, String cpf, String zipCode,
                            Integer operator, String city, String state, String latitude,
                            String longitude) {
    this.telephone = telephone;
    this.birthDate = birthDate;
    this.cpf = cpf;
    this.zipCode = zipCode;
    this.operator = operator;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public EditUserDataParams(String zipCode, String city, String state, String latitude,
                            String longitude, Boolean locationPolicyAgreed) {
    this.zipCode = zipCode;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
    this.locationPolicyAgreed = locationPolicyAgreed;
  }

  public String getTelephone() {
    return telephone;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getCpf() {
    return cpf;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public Boolean getLocationPolicyAgreed() {
    return locationPolicyAgreed;
  }

  public Integer getOperator() {
    return operator;
  }

  public String getOperatorDocument() {
    return operatorDocument;
  }
}
