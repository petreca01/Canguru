package br.com.taqtile.android.app.data.channel;

import java.util.List;

import br.com.taqtile.android.app.data.channel.mappers.ChannelFollowParamsToChannelPostFollowRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.mappers.ChannelPostParamsToChannelPostLikeReplyRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.mappers.ChannelPostRemoteResponseToChannelPostResultMapper;
import br.com.taqtile.android.app.data.channel.mappers.ChannelPostReplyDeleteParamsToChannelPostReplyDestroyRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.mappers.ChannelPostsItemsRemoteResponseToChannelPostsMapper;
import br.com.taqtile.android.app.data.channel.remote.ChannelRemoteDataSource;
import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelListRemoteResponseToChannelListMapper;
import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelPostLikeReplyParamsToChannelPostLikeReplyRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelPostReplyParamsToChannelPostReplyRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelPostsParamsToChannelPostsListRemoteRequestMapper;
import br.com.taqtile.android.app.data.channel.remote.mappers.ChannelPostsRemoteResponseToChannelPostsResultMapper;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelCategoryResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelFollowParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostLikeReplyParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyDeleteParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class ChannelRepository extends BaseRepository<ChannelRemoteDataSource, Void> {

  public ChannelRepository(ChannelRemoteDataSource channelRemoteDataSource) {
    super(channelRemoteDataSource, null);
  }

  public Observable<List<ChannelPost>> listPostsOfFollowedChannels() {
    return getRemoteDataSource().listPostsOfFollowedChannels()
      .map(
        result -> ChannelPostsItemsRemoteResponseToChannelPostsMapper.perform(result.getResult()));
  }

  public Observable<ChannelCategoryResult> list() {
    return getRemoteDataSource().list()
      .map(result -> ChannelListRemoteResponseToChannelListMapper.map(result.getResult()));
  }

  public Observable<ChannelPostsResult> listChannelPosts(ChannelPostsParams channelPostsParams) {

    return getRemoteDataSource().listPosts(
      ChannelPostsParamsToChannelPostsListRemoteRequestMapper.perform(channelPostsParams))
      .map(
        result -> ChannelPostsRemoteResponseToChannelPostsResultMapper.perform(result.getResult()));
  }

  public Observable<EmptyResult> follow(ChannelFollowParams channelFollowParams) {

    return getRemoteDataSource().follow(
      ChannelFollowParamsToChannelPostFollowRemoteRequestMapper.perform(channelFollowParams))
      .map(BaseRemoteResponse::getResult);
  }

  public Observable<ChannelPostResult> post(ChannelPostParams channelPostParams) {

    return getRemoteDataSource().post(
      ChannelPostParamsToChannelPostLikeReplyRemoteRequestMapper.perform(channelPostParams))
      .map(
        result -> ChannelPostRemoteResponseToChannelPostResultMapper.perform(result.getResult()));
  }

  public Observable<ChannelPostResult> postReply(ChannelPostReplyParams channelPostReplyParams) {

    return getRemoteDataSource().postReply(
      ChannelPostReplyParamsToChannelPostReplyRemoteRequestMapper.perform(channelPostReplyParams))
      .map(
        result -> ChannelPostRemoteResponseToChannelPostResultMapper.perform(result.getResult()));
  }

  public Observable<ChannelPostResult> likeReply(
    ChannelPostLikeReplyParams channelPostLikeReplyParams) {

    return getRemoteDataSource().likeReply(
      ChannelPostLikeReplyParamsToChannelPostLikeReplyRemoteRequestMapper.perform(
      channelPostLikeReplyParams))
      .map(
        result -> ChannelPostRemoteResponseToChannelPostResultMapper.perform(result.getResult()));
  }

  public Observable<ChannelPostResult> deleteReply(
    ChannelPostReplyDeleteParams channelPostReplyDeleteParams) {

    return getRemoteDataSource().deleteReply(
      ChannelPostReplyDeleteParamsToChannelPostReplyDestroyRemoteRequestMapper.perform(
        channelPostReplyDeleteParams))
      .map(
        result -> ChannelPostRemoteResponseToChannelPostResultMapper.perform(result.getResult()));
  }
}
