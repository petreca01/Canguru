package br.com.taqtile.android.app.domain.notification.mapper;

import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import br.com.taqtile.android.app.domain.notification.models.NotificationResult;
import br.com.taqtile.android.app.listings.viewmodels.NotificationViewModel;

/**
 * Created by taqtile on 5/9/17.
 */

public class NotificationResultToNotificationViewModelMapper {

  public static List<NotificationViewModel> perform(List<NotificationResult> notificationResults) {
    return StreamSupport.stream(notificationResults)
      .map(NotificationResultToNotificationViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static NotificationViewModel map(NotificationResult notificationResult) {
    return new NotificationViewModel(
      notificationResult.getId(),
      notificationResult.getContent(),
      notificationResult.getType(),
      notificationResult.getEntity(),
      notificationResult.getEntityId(),
      notificationResult.getCreatedAt(),
      notificationResult.isVisualized(),
      notificationResult.getUserPerformerName());
  }
}
