package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.forum.remote.models.request.PostsListRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.PostsListParams;

/**
 * Created by taqtile on 4/3/17.
 */

public class PostsListParamsToPostRemoteRequestMapper {
  public static PostsListRemoteRequest perform(PostsListParams params) {
    return new PostsListRemoteRequest(
      params.getPage(),
      params.getPerPage(),
      params.getOrderBy(),
      params.getOrderDirection(),
      params.getSearch()
    );
  }
}
