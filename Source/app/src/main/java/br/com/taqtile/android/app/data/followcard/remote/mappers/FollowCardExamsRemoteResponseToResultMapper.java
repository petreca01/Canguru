package br.com.taqtile.android.app.data.followcard.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardExamsRemoteResponse;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardExamsResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardExamsRemoteResponseToResultMapper {

  public static List<FollowCardExamsResult> perform(
    List<FollowCardExamsRemoteResponse> followCardExamsRemoteResponse) {
    return StreamSupport.stream(followCardExamsRemoteResponse)
      .map(FollowCardExamsRemoteResponseToResultMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardExamsResult map(
    FollowCardExamsRemoteResponse followCardExamsRemoteResponse) {
    return new FollowCardExamsResult(
      followCardExamsRemoteResponse.getId(),
      followCardExamsRemoteResponse.getName(),
      followCardExamsRemoteResponse.getResult(),
      followCardExamsRemoteResponse.getDate(),
      followCardExamsRemoteResponse.getCreatedAt());
  }
}
