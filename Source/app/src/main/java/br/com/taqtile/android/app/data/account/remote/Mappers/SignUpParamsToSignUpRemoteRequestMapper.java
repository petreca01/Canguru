package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.account.remote.models.request.SignUpRemoteRequest;
import br.com.taqtile.android.app.domain.account.models.SignUpParams;

/**
 * Created by taqtile on 31/03/17.
 */

public class SignUpParamsToSignUpRemoteRequestMapper {

  public static SignUpRemoteRequest perform(SignUpParams signUpParams) {
    SignUpRemoteRequest retVal;
    if (signUpParams.getLastPeriod() != null) {
      retVal = new SignUpRemoteRequest(signUpParams.getEmail(),
        signUpParams.getPassword(),
        signUpParams.getName(),
        signUpParams.getUserType(),
        signUpParams.getLastPeriod());
    } else {
      retVal = new SignUpRemoteRequest(signUpParams.getEmail(),
        signUpParams.getPassword(),
        signUpParams.getName(),
        signUpParams.getUserType());
    }

    if (signUpParams.getBirthDate() != null && signUpParams.getBirthDate().length() > 0) {
      retVal.addBirthDate(signUpParams.getBirthDate());
    }

    return retVal;
  }
}
