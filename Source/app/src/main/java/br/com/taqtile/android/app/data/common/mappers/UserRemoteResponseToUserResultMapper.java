package br.com.taqtile.android.app.data.common.mappers;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by taqtile on 05/04/17.
 */

public class UserRemoteResponseToUserResultMapper {

  public static UserResult perform(UserRemoteResponse userRemoteResponse) {
    return new UserResult(
      userRemoteResponse.getAbout(),
      userRemoteResponse.getActive(),
      userRemoteResponse.getBabyName(),
      userRemoteResponse.getCellphoneNumber(),
      userRemoteResponse.getConclusionDate(),
      userRemoteResponse.getConclusionMethod(),
      userRemoteResponse.getConclusionText(),
      userRemoteResponse.getCoverPicture(),
      userRemoteResponse.getCpf(),
      userRemoteResponse.getCreatedAt(),
      userRemoteResponse.getDeactivatedAt(),
      userRemoteResponse.getDeactivationReason(),
      userRemoteResponse.getDolpUpdatedAt(),
      userRemoteResponse.getDum(),
      userRemoteResponse.getDpp(),
      userRemoteResponse.getDppBased(),
      userRemoteResponse.getEmail(),
      userRemoteResponse.getId(),
      userRemoteResponse.getIgTrimesters(),
      userRemoteResponse.getIgMonths(),
      userRemoteResponse.getIgDays(),
      userRemoteResponse.getIgWeeks(),
      userRemoteResponse.getLoaded(),
      userRemoteResponse.getLocationBlockedAt(),
      userRemoteResponse.getLocationPrivacyAgreedAt(),
      userRemoteResponse.getName(),
      userRemoteResponse.getOperator(),
      userRemoteResponse.getOperatorCanSeeRiskAnswers(),
      userRemoteResponse.getOperatorId(),
      userRemoteResponse.getOperatorDocument(),
      userRemoteResponse.getPregnancyDatesUpdatedAt(),
      userRemoteResponse.getProfilePicture(),
      userRemoteResponse.getRisk(),
      userRemoteResponse.getScheduleEngagement(),
      userRemoteResponse.getSure(),
      userRemoteResponse.getToken(),
      userRemoteResponse.getUpdatedAt(),
      userRemoteResponse.getUserBirthdate(),
      userRemoteResponse.getUserType(),
      userRemoteResponse.getZipCode(),
      userRemoteResponse.getCity(),
      userRemoteResponse.getState(),
      userRemoteResponse.getLatitude(),
      userRemoteResponse.getLongitude(),
      userRemoteResponse.getLocationPolicyAgreed());
  }
}
