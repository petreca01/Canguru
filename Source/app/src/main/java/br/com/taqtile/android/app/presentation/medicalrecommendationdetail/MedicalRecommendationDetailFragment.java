package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.cells.PostCell;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 08/05/17.
 */

public class MedicalRecommendationDetailFragment extends AppBaseFragment {

  @BindView(R.id.fragment_medical_recommendation_detail_post_cell)
  PostCell recommendationPost;

  private MedicalRecommendationDetailContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private MedicalRecommendationDetailAccessor medicalRecommendationDetailAccessor;

  public static MedicalRecommendationDetailFragment newInstance() {
    return new MedicalRecommendationDetailFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_medical_recommendation_detail, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();

    MedicalRecommendationViewModel medicalRecommendationViewModel =
      medicalRecommendationDetailAccessor.getMedicalRecommendationViewModel();
    setupToolbar();
    if (medicalRecommendationViewModel != null) {
      setupView(medicalRecommendationViewModel);
      markAsRead(medicalRecommendationViewModel.getId());
    } else {
      showUnexpectedErrorPlaceholder();
    }
  }

  private void showUnexpectedErrorPlaceholder(){
    placeholder.setPlaceholderType(CustomPlaceholder.UNEXPECTED_ERROR);
    showPlaceholder();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_medical_recommendations_title));
    }
  }

  private void setupView(MedicalRecommendationViewModel medicalRecommendationViewModel) {
    recommendationPost.setProfileName(medicalRecommendationViewModel.getDoctorName());
    recommendationPost.setTextUnlimited();
    recommendationPost.setDate(medicalRecommendationViewModel.getDate());

    recommendationPost.setHeading(medicalRecommendationViewModel.getTitle());
    recommendationPost.setBody(medicalRecommendationViewModel.getContent());

    recommendationPost.setProfileImage(R.drawable.ic_doctor_placeholder);
    recommendationPost.getContentRoot().setClickable(false);
  }

  private void markAsRead(String id) {
    Subscription subscription = presenter.markAsRead(id)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(emptyResult -> {
      }, this::onMarkAsReadFailure);

    compositeSubscription.add(subscription);
  }

  private void onMarkAsReadFailure(Throwable error) {
    showSnackbar(error.getMessage(), Snackbar.LENGTH_LONG);
  }

  public void setPresenter(MedicalRecommendationDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_medical_recommendations_detail_placeholder;
  }

  public void setMedicalRecommendationDetailAccessor(
    MedicalRecommendationDetailAccessor medicalRecommendationDetailAccessor) {
    this.medicalRecommendationDetailAccessor = medicalRecommendationDetailAccessor;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_medical_recommendation_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
