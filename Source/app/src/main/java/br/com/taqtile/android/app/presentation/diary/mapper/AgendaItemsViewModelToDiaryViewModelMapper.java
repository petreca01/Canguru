package br.com.taqtile.android.app.presentation.diary.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.presentation.diary.model.DiaryViewModel;
import java8.util.Objects;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/10/17.
 */

public class AgendaItemsViewModelToDiaryViewModelMapper {

  private static final Integer MIN_WEEK = 0;
  private static final Integer MAX_WEEK = 42;

  public static DiaryViewModel perform(boolean showDiaryTutorial,
                                       List<AgendaItemViewModel> itemsList) {
    return new DiaryViewModel(showDiaryTutorial,
      AgendaItemsViewModelToDiaryViewModelMapper.mapAppointmentsByWeek(itemsList));
  }

  private static TreeMap<Integer, List<AgendaItemViewModel>> mapAppointmentsByWeek(
    List<AgendaItemViewModel> itemsList) {
    TreeMap<Integer, List<AgendaItemViewModel>> weekAppointments = new TreeMap<>();
    StreamSupport.stream(itemsList)
      .filter(Objects::nonNull)
      .forEach(item -> AgendaItemsViewModelToDiaryViewModelMapper.sortAppointmentsByWeek(item, weekAppointments));
    return weekAppointments;
  }

  private static void sortAppointmentsByWeek(AgendaItemViewModel agendaItemViewModel, TreeMap<Integer,
    List<AgendaItemViewModel>> weekAppointments) {
    List<AgendaItemViewModel> appointments = new ArrayList<>();

    if (!isValidWeek(agendaItemViewModel)) {
      return;
    }

    if (AgendaItemsViewModelToDiaryViewModelMapper.weekAlreadyMapped(agendaItemViewModel, weekAppointments.keySet())) {

      appointments = weekAppointments.get(Integer.valueOf(agendaItemViewModel.getWeekStart()));
      appointments.add(agendaItemViewModel);

    } else {
      appointments.add(agendaItemViewModel);
    }

    weekAppointments.put(Integer.valueOf(agendaItemViewModel.getWeekStart()), appointments);
  }

  private static boolean isValidWeek(AgendaItemViewModel item) {
    return item.getWeekStart() != null && Integer.valueOf(item.getWeekStart()) > MIN_WEEK &&
      Integer.valueOf(item.getWeekStart()) <= MAX_WEEK;
  }

  private static boolean weekAlreadyMapped(AgendaItemViewModel agendaItemViewModel, Set<Integer> weeks) {
    return weeks.contains(Integer.valueOf(agendaItemViewModel.getWeekStart()));
  }
}
