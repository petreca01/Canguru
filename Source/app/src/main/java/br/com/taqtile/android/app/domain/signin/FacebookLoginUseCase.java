package br.com.taqtile.android.app.domain.signin;

import android.content.Context;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.facebook.FacebookRepository;
import br.com.taqtile.android.app.domain.account.models.SignUpParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInWithFacebookParams;
import br.com.taqtile.android.app.domain.signin.Models.FacebookLoginResult;
import br.com.taqtile.android.app.support.Injection;
import br.com.taqtile.android.app.support.TemplateApplication;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.analytics.AnalyticsToolBox;
import rx.Observable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 3/29/17.
 */

public class FacebookLoginUseCase extends BaseUseCase<FacebookLoginResult, Void, FacebookRepository> {

  private AccountRepository accountRepository;
  protected AnalyticsToolBox analytics;

  public FacebookLoginUseCase(FacebookRepository facebookRepository, AccountRepository accountRepository) {
    super(facebookRepository);
    this.accountRepository = accountRepository;
    analytics = Injection.provideAnalyticsToolBox();
  }

  @Override
  public Observable<FacebookLoginResult> execute(Void aVoid) {

    return getRepository().requestFacebookPermissions()
      .flatMap(this::loginWithFacebook)
      .map(userResult -> new FacebookLoginResult(true, userResult.getEmail()))
      .onErrorResumeNext(throwable -> createUser());
  }

  private Observable<UserResult> loginWithFacebook(String token) {
    return accountRepository.loginWithFacebook(new AccountSignInWithFacebookParams(token));
  }

  private Observable<FacebookLoginResult> createUser() {
    return
      getRepository()
        .getUser()
        .map(facebookUserResult -> new SignUpParams(
          facebookUserResult.getEmail(),
          facebookUserResult.getToken(),
          facebookUserResult.getName(),
          null))
        .flatMap(signUpParams -> accountRepository.signUp(signUpParams)
          .doOnCompleted(() -> {
            Context context = checkNotNull(TemplateApplication.getContext());

            this.analytics.trackEvent(new AnalyticsEvent(
              context.getString(R.string.analytics_category_registration),
              context.getString(R.string.analytics_event_facebook),
              context.getString(R.string.analytics_label_user_registered_using_facebook)
            ));

          }))
        .map(signupResult -> new FacebookLoginResult(true, signupResult.getEmail()));
  }
}
