package br.com.taqtile.android.app.data.channel.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostRemoteRequest extends BaseRemoteRequest {
    @RequestName("post_id")
    private Integer postId;

    private Integer like;

    private Integer dislike;

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public void setDislike(Integer dislike) {
        this.dislike = dislike;
    }
}
