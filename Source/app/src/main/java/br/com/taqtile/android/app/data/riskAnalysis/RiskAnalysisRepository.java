package br.com.taqtile.android.app.data.riskAnalysis;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.questionnaires.mappers.AnswerParamsToAnswerRemoteRequestMapper;
import br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest;
import br.com.taqtile.android.app.data.riskAnalysis.mappers.RiskAnalysisAnswerRemoteResponseToRiskAnalysisAnswerResultMapper;
import br.com.taqtile.android.app.data.riskAnalysis.mappers.RiskAnalysisQuestionsRemoteResponseToRiskAnalysisQuestionsResultMapper;
import br.com.taqtile.android.app.data.riskAnalysis.remote.RiskAnalysisRemoteDataSource;
import br.com.taqtile.android.app.data.view.mappers.ViewModeParamsToViewModeRemoteRequestMapper;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisAnswerResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionsResult;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class RiskAnalysisRepository extends BaseRepository<RiskAnalysisRemoteDataSource, Void> {

  public RiskAnalysisRepository(RiskAnalysisRemoteDataSource riskAnalysisRemoteDataSource) {
    super(riskAnalysisRemoteDataSource, null);
  }

  public Observable<RiskAnalysisQuestionsResult> list(ViewModeParams viewModeParams) {
    return getRemoteDataSource().list(ViewModeParamsToViewModeRemoteRequestMapper.perform(viewModeParams))
      .map(result ->
        RiskAnalysisQuestionsRemoteResponseToRiskAnalysisQuestionsResultMapper.perform(result.getResult()));
  }

  public Observable<RiskAnalysisAnswerResult> answer(AnswerParams answerParams) {
    return getRemoteDataSource().answer(
      new AnswerQuestionnairesRemoteRequest(
        AnswerParamsToAnswerRemoteRequestMapper.perform(answerParams)))
      .map(result ->
        RiskAnalysisAnswerRemoteResponseToRiskAnalysisAnswerResultMapper.perform(result.getResult()));
  }
}
