package br.com.taqtile.android.app.domain.followcard.models;


/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardResult {

  private String name;
  private String age;
  private GestationHistoryResult gpa;
  private RisksResult risks;

  public FollowCardResult(String name, String age, GestationHistoryResult gpa, RisksResult risks) {
    this.name = name;
    this.age = age;
    this.gpa = gpa;
    this.risks = risks;
  }

  public String getName() {
    return name;
  }

  public String getAge() {
    return age;
  }

  public GestationHistoryResult getGpa() {
    return gpa;
  }

  public RisksResult getRisks() {
    return risks;
  }
}
