package br.com.taqtile.android.app.presentation.staticscreen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import java.lang.annotation.Retention;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.cleanbase.presentation.view.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by taqtile on 18/04/17.
 */

public class StaticScreenFragment extends BaseFragment {

  @Retention(SOURCE)
  @StringDef({
    ABOUT_CANGURU,
    PRIVACY_POLICY,
    USE_TERMS
  })
  public @interface StaticScreenURL {
  }

  public static final String ABOUT_CANGURU = "file:///android_asset/html_css/about_canguru.html";
  public static final String PRIVACY_POLICY = "file:///android_asset/html_css/privacy_policy.html";
  public static final String USE_TERMS = "file:///android_asset/html_css/use_terms.html";

  public static final String HTML_TAG = "html_tag";
  public static final int delay = 200;

  @BindView(R.id.fragment_static_screen_webview)
  WebView webView;

  private CustomToolbar customToolbar;
  private StaticScreenPresenter presenter;
  private NavigationManager navigationManager;

  public static StaticScreenFragment newInstance(@StaticScreenURL String url) {
    Bundle args = new Bundle();
    args.putString(HTML_TAG, url);

    StaticScreenFragment fragment = new StaticScreenFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_static_screen, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();
    setupView();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
    }
  }

  private void setupView() {
    openLinksOnDefaultBrowser();

    webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

    final String url = getArguments().getString(HTML_TAG);
    setTitle(url);

    new Handler().postDelayed(() -> webView.loadUrl(url), delay);
  }

  // TODO: use this to open links in text. review the need for https links.
  // Source: http://stackoverflow.com/a/6343852
  private void openLinksOnDefaultBrowser() {
    webView.setWebViewClient(new WebViewClient() {
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url != null && url.startsWith("http://")) {
          view.getContext().startActivity(
            new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
          return true;
        } else {
          return false;
        }
      }
    });
  }

  private void setTitle(String url) {
    switch (url) {
      case ABOUT_CANGURU:
        customToolbar.setTitle(getString(R.string.fragment_static_screen_about_canguru_title), Gravity.LEFT);
        break;
      case PRIVACY_POLICY:
        customToolbar.setTitle(getString(R.string.fragment_static_screen_privacy_policy_title), Gravity.LEFT);
        break;
      case USE_TERMS:
        customToolbar.setTitle(getString(R.string.fragment_static_screen_use_terms_title), Gravity.LEFT);
        break;
    }
  }

  public void setPresenter(StaticScreenPresenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {

  }

  @Override
  public void willDisappear() {
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }
}
