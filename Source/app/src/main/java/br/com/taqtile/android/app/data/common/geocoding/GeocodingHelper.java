package br.com.taqtile.android.app.data.common.geocoding;

import br.com.taqtile.android.app.data.common.remote.StringConverterFactory;
import br.com.taqtile.android.app.support.Constants;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by taqtile on 6/2/17.
 */

public class GeocodingHelper {

  public static Retrofit.Builder getRetrofitBuilder() {

    String googleGeocodingUrl = new Constants().getGoogleAPIUrl();

    return new Retrofit.Builder()
      .addConverterFactory(StringConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
      .addConverterFactory(GsonConverterFactory.create())
      .baseUrl(googleGeocodingUrl);
  }
}
