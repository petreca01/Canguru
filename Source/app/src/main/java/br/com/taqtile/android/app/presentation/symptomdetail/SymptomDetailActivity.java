package br.com.taqtile.android.app.presentation.symptomdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.postcommunity.PostCommunityActivity;
import br.com.taqtile.android.app.presentation.symptomsguide.SymptomsGuideActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAILS_REQUEST_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAIL_ID_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAIL_PARENT_ID_KEY;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomDetailActivity extends TemplateBackActivity {

  private SymptomDetailFragment fragment;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    SymptomDetailAccessor bundleAccessor =
      new SymptomDetailAccessor(getIntent().getExtras());

    if (fragment != null) {
      fragment.setSubSymptomViewModel(bundleAccessor.getSymptom());
    }
  }

  @Override
  public Fragment getFragment() {
    SymptomDetailFragment symptomDetailFragment = (SymptomDetailFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      symptomDetailFragment = SymptomDetailFragment.newInstance();
    }
    fragment = symptomDetailFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    SymptomDetailPresenter symptomDetailPresenter = new SymptomDetailPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(symptomDetailPresenter);
    return symptomDetailPresenter;
  }

  public static void navigate(Context context, int parentSymptomId, int symptomId) {
    Intent intent = new Intent(context, SymptomDetailActivity.class);
    intent.putExtra(SYMPTOM_DETAIL_PARENT_ID_KEY, parentSymptomId);
    intent.putExtra(SYMPTOM_DETAIL_ID_KEY, symptomId);

     if (context instanceof PostCommunityActivity){
      ((PostCommunityActivity) context).startActivityForResult(intent,
        SYMPTOM_DETAILS_REQUEST_CODE);
    }
  }

  public static void navigate(Context context, Bundle bundle, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, SymptomDetailActivity.class);
    intent.putExtras(bundle);

    if (context instanceof SymptomsGuideActivity) {
      ((SymptomsGuideActivity) context).setResultListener(listener,
        SYMPTOM_DETAILS_REQUEST_CODE);
      ((SymptomsGuideActivity) context).startActivityForResult(intent,
        SYMPTOM_DETAILS_REQUEST_CODE);
    }
  }
}
