package br.com.taqtile.android.app.data.channel.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelPostFollowRemoteRequest extends BaseRemoteRequest{

  @RequestName("channel_id")
  private Integer channelId;

  private Integer follow;

  private Integer unfollow;

  public Integer getChannelId() {
    return channelId;
  }

  public void setChannelId(Integer channelId) {
    this.channelId = channelId;
  }

  public Integer getFollow() {
    return follow;
  }

  public void setFollow(Integer follow) {
    this.follow = follow;
  }

  public Integer getUnfollow() {
    return unfollow;
  }

  public void setUnfollow(Integer unfollow) {
    this.unfollow = unfollow;
  }
}
