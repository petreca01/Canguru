package br.com.taqtile.android.app.data.agenda.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/21/17.
 */

public class AppointmentRemoteRequest extends BaseRemoteRequest {
  @RequestName("tipo_id")
  private Integer typeId;

  @RequestName("nome")
  private String name;

  @RequestName("data")
  private String date;

  @RequestName("observacao")
  private String observation;

  public AppointmentRemoteRequest(Integer typeId, String name, String date, String observation) {
    this.typeId = typeId;
    this.name = name;
    this.date = date;
    this.observation = observation;
  }
}
