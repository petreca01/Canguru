package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionParams {

  private String search;

  private Integer page;

  private Integer perPage;

  public ContentSuggestionParams(String search, Integer page, Integer perPage) {
    this.search = search;
    this.page = page;
    this.perPage = perPage;
  }

  public String getSearch() {
    return search;
  }

  public Integer getPage() {
    return page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }
}
