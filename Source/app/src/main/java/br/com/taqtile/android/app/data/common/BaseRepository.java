package br.com.taqtile.android.app.data.common;

/**
 * Created by taqtile on 3/19/17.
 */

public class BaseRepository<RemoteDataSource, LocalDataSource> {

  private RemoteDataSource dataSource;
  private LocalDataSource localDataSource;

  public BaseRepository(RemoteDataSource dataSource, LocalDataSource localDataSource) {
    this.dataSource = dataSource;
    this.localDataSource = localDataSource;
  }

  protected RemoteDataSource getRemoteDataSource() {
    return dataSource;
  }

  protected LocalDataSource getLocalDataSource() { return localDataSource; }

}
