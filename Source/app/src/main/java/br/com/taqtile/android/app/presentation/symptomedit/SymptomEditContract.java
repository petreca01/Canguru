package br.com.taqtile.android.app.presentation.symptomedit;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 27/04/17.
 */

public interface SymptomEditContract {

  interface Presenter extends BasePresenter {

    Observable<EmptyResult> reportCustomSymptom(String description, String date);

  }
}
