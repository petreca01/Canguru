package br.com.taqtile.android.app.presentation.gestationpregnancyinformation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;


import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import br.com.taqtile.android.app.edittexts.BabyNameEditTextCaption;
import br.com.taqtile.android.app.edittexts.CustomDropDown;
import br.com.taqtile.android.app.edittexts.DaysLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.ExamDateLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.LastPeriodLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.WeeksLabelEditTextCaption;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model.ExamViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.PREGNANT;
import static br.com.taqtile.android.app.support.GlobalConstants.USER_DATA_KEY;

/**
 * Created by taqtile on 12/05/17.
 */

public class GestationPregnancyInformationFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  @BindView(R.id.fragment_gestation_pregnancy_information_filled_DUM_container)
  LinearLayout DUMFilledInformationContainer;

  @BindView(R.id.fragment_gestation_pregnancy_information_not_filled_DUM_container)
  LinearLayout DUMNotFilledInformationContainer;

  @BindView(R.id.fragment_gestation_pregnancy_information_DUM_date)
  CustomTextView DUMDateLabel;

  @BindView(R.id.fragment_gestation_pregnancy_information_DUM_date_form)
  LastPeriodLabelEditTextCaption lastPeriodForm;

  @BindView(R.id.fragment_gestation_pregnancy_exam_date_form)
  ExamDateLabelEditTextCaption examDateForm;

  @BindView(R.id.fragment_gestation_pregnancy_information_weeks_form)
  WeeksLabelEditTextCaption weeksForm;

  @BindView(R.id.fragment_gestation_pregnancy_information_days_form)
  DaysLabelEditTextCaption daysForm;

  @BindView(R.id.fragment_gestation_pregnancy_information_baby_name_form)
  BabyNameEditTextCaption babyNameForm;

  @BindView(R.id.fragment_gestation_pregnancy_information_save_button)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_gestation_pregnancy_information_gender_dropdown)
  CustomDropDown genderDropdown;

  @BindView(R.id.fragment_gestation_pregnancy_information_scroll_view)
  ScrollView scrollView;

  @BindView(R.id.fragment_gestation_pregnancy_information_loading)
  ProgressBarLoading loading;

  private boolean userIsPregnant;
  private boolean dumAlreadyChanged;

  private Validatable[] forms;
  private List<Validatable> errorForms;
  private String errorMessage;

  private NavigationManager navigationManager;
  private GestationPregnancyInformationContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private UserViewModel userData;

  public static GestationPregnancyInformationFragment newInstance() {
    return new GestationPregnancyInformationFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_gestation_pregnancy_information, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setFormData();
    setupFormsList();
    setupListeners();
    setupGenderDropdown();
  }

  private void setFormData() {

    lastPeriodForm.setHintCaption("dd/mm/aaaa");
    examDateForm.setHintCaption("dd/mm/aaaa");
    babyNameForm.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
    this.userData = getActivity().getIntent().getParcelableExtra(USER_DATA_KEY);

    if (userData != null) {

      dumAlreadyChanged = !userData.getDolpUpdatedAt().isEmpty();
      userIsPregnant = verifyUserType();

      showLoading();
      setDUMContainerVisibility();
      if (dumAlreadyChanged){
        DUMDateLabel.setText(DateFormatterHelper.formatDateWithoutHours(userData.getUserDUM()));
      }else{
        lastPeriodForm.setText(DateFormatterHelper.formatDateWithoutHours(userData.getUserDUM()));
      }
      if (!userData.getBabyName().equals(getString(R.string.baby_name_default_text))) {
        babyNameForm.setText(userData.getBabyName());
      }

      if (userIsPregnant){
        fillFormWithExamData();
      }else{
        hideLoading();
      }

    } else {
      placeholder.setPlaceholderType(CustomPlaceholder.UNEXPECTED_ERROR);
      FocusHelper.clearViewFocus(examDateForm, scrollView);
      FocusHelper.clearViewFocus(lastPeriodForm, scrollView);
      showPlaceholder();
    }
  }

  private void fillFormWithExamData() {

    Subscription subscription = presenter.fetchExam()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onExamFetchSuccess,
        this::onExamFetchFailure);
    compositeSubscription.add(subscription);
  }

  private void onExamFetchSuccess(ExamViewModel examViewModel) {

    examDateForm.setText(DateFormatterHelper.formatDateWithoutHours(examViewModel.getExamDate()));
    weeksForm.setText(examViewModel.getWeeks());
    daysForm.setText(examViewModel.getDays());

    hideLoading();
  }

  private void onExamFetchFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private boolean verifyUserType() {
    return userData != null && userData.getUserType() != null && !userData.getUserType().isEmpty()
      && userData.getUserType().equals(PREGNANT);
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupGenderDropdown() {
    List<String> genderList = new ArrayList<>();
    genderList.add(getResources().getString(R.string.male));
    genderList.add(getResources().getString(R.string.female));
    genderDropdown.setValues(genderList);
  }

  private void setupListeners() {
    saveButton.setButtonListener(this);
  }

  private void setupFormsList() {
    errorForms = new ArrayList<Validatable>();
    if (dumAlreadyChanged) {
      forms = new Validatable[]{};
    } else {
      forms = new Validatable[]{lastPeriodForm};
    }
  }

  public boolean locallyValidateForms() {
    boolean isValid;
    errorMessage = "";

    isValid = validateForms();

    if (isValid) {

      isValid = examDataValid();
      if (!isValid) {
        showSnackbar(getResources().getString(R.string.fragment_gestation_pregnancy_information_exam_forms_error_text), Snackbar.LENGTH_SHORT);
      }
    } else {
      //Display feedback with all views with an error
      showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);
      if (!errorForms.isEmpty()) {
        ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      }
    }
    return isValid;
  }

  private boolean validateForms() {
    boolean isValid;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), scrollView);
    }

    errorForms.clear();
    errorForms.addAll(ValidateAndScrollHelper.validateForms(forms));
    if (!dumAlreadyChanged) {
      if (lastPeriodForm.getState() == CustomLabelEditTextCaption.STATE_INVALID && !errorForms.contains(lastPeriodForm)) {
        errorForms.add(lastPeriodForm);
      }
    }

    isValid = errorForms.isEmpty();
    return isValid;
  }

  private boolean examDataValid() {

    boolean isValid = true;

    // if the user fill out the exam date but not the week e day, the form is not valid
    if (!examDateForm.getText().isEmpty() &&
       (weeksForm.getText().isEmpty() || daysForm.getText().isEmpty())){
      isValid = false;
    }
    return isValid;

  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms;

    errorForms = new String[errorViews.size()];
    for (int i = 0; i < errorForms.length; i++) {
      errorForms[i] = getResources().getString(((CustomLabelEditTextCaption) errorViews.get(i).getView()).getInputPlaceholderTextRes());
    }
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      int buttonIcon;
      int toolbarTitle;
      if (userIsPregnant) {
        buttonIcon = R.drawable.ic_arrow_back;
        toolbarTitle = R.string.fragment_gestation_pregnancy_information_edit_information_title;
      } else {
        buttonIcon = R.drawable.ic_close;
        toolbarTitle = R.string.fragment_gestation_initial_screen_start_gestation_button;
      }
      customToolbar.setLeftButton(buttonIcon, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(toolbarTitle), Gravity.START);

    }
    ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
  }

  private void setDUMContainerVisibility() {
    if (dumAlreadyChanged && userIsPregnant) {
      DUMNotFilledInformationContainer.setVisibility(View.GONE);
      DUMFilledInformationContainer.setVisibility(View.VISIBLE);
    } else {
      DUMNotFilledInformationContainer.setVisibility(View.VISIBLE);
      DUMFilledInformationContainer.setVisibility(View.GONE);
    }
  }

  @Override
  public void onButtonClicked() {

    if (locallyValidateForms()) {
      showButtonLoading();
      Subscription subscription = presenter.startOrEditGestation(buildParams())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onStartOrEditGestationSuccess, this::onStartOrEditGestationFailure,
          this::hideButtonLoading);
      compositeSubscription.add(subscription);
    }
  }

  private void showButtonLoading() {
    saveButton.showLoadingState();
  }

  private void hideButtonLoading() {
    saveButton.showDefaultState();
  }

  private StartOrEditGestationParams buildParams() {

    if (dumAlreadyChanged) {

      // in this case, the user is a pregnant and already changed DUM before (DUM=lastPeriodForm)
      return new StartOrEditGestationParams(babyNameForm.getText(),
        getFormNumbers(daysForm.getText()), getFormNumbers(weeksForm.getText()),
        DateFormatterHelper.formatDateForAPIWithoutHour(examDateForm.getText()));

    } else {

      String currentDUM = DateFormatterHelper.formatDateWithoutHours(userData.getUserDUM());
      String newDUM = lastPeriodForm.getText();

      // if user changed DUM for the first time, send this information to server
      if (!userData.getUserDUM().isEmpty() && !currentDUM.equals(newDUM))
      {
        // in this case, the user is a pregnant and wants changing DUM already saved
        Date currentDate = new Date();
        return new StartOrEditGestationParams(
          DateFormatterHelper.formatDateForAPIWithoutHour(lastPeriodForm.getText()),
          DateFormatterHelper.stringFromDateWithAPIFormat(currentDate),
          babyNameForm.getText(),
          getFormNumbers(daysForm.getText()),
          getFormNumbers(weeksForm.getText()),
          DateFormatterHelper.formatDateForAPIWithoutHour(examDateForm.getText()));

      } else {

        // in this case, the user is a new pregnant or DUM is the same.
        return new StartOrEditGestationParams(
          DateFormatterHelper.formatDateForAPIWithoutHour(lastPeriodForm.getText()),
          babyNameForm.getText(),
          getFormNumbers(daysForm.getText()),
          getFormNumbers(weeksForm.getText()),
          DateFormatterHelper.formatDateForAPIWithoutHour(examDateForm.getText()));
      }
    }
  }

  private Integer getFormNumbers(String text) {
    return text.isEmpty() ? null : Integer.valueOf(text);
  }

  private void onStartOrEditGestationSuccess(UserViewModel userViewModel) {
    Intent intent = new Intent();
    intent.putExtra(USER_DATA_KEY, userViewModel);
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
  }

  private void onStartOrEditGestationFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
    hideButtonLoading();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_gestation_pregnancy_information_placeholder;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(GestationPregnancyInformationContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void showPlaceholder() {
    super.showPlaceholder();
    SoftKeyboardHelper.hideKeyboard(getContext());
    scrollView.setVisibility(View.GONE);
  }

  @Override
  public void hidePlaceholder() {
    super.hidePlaceholder();
    scrollView.setVisibility(View.VISIBLE);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    setFormData();
  }

  @Override
  public void showLoading() {
    scrollView.setVisibility(View.GONE);
    loading.setVisibility(View.VISIBLE);
    hidePlaceholder();
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
    scrollView.setVisibility(View.VISIBLE);
    hidePlaceholder();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_gestation_pregnancy_information),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
