package br.com.taqtile.android.app.data.profile.local.mappers;

import br.com.taqtile.android.app.data.profile.local.models.UserImagesRequest;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;

/**
 * Created by taqtile on 17/04/17.
 */

public class EditUserParamsToUserImagesRequestMapper {
  public static UserImagesRequest perform(EditProfileParams editProfileParams) {
    return new UserImagesRequest(editProfileParams.getCoverPicture(),
      editProfileParams.getProfilePicture());
  }
}
