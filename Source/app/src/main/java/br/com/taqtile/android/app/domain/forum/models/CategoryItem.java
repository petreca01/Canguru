package br.com.taqtile.android.app.domain.forum.models;


/**
 * Created by taqtile on 3/21/17.
 */

public class CategoryItem {
  private Integer id;
  private String name;

  public CategoryItem(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

}
