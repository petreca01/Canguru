package br.com.taqtile.android.app.presentation.appointmentdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.diary.DiaryActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity.APPOINTMENT_MODEL_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.EDIT_APPOINTMENT_REQUEST_CODE;
import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by taqtile on 26/04/17.
 */

public class AppointmentDetailActivity extends TemplateBackActivity {

  private AppointmentDetailFragment fragment;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public Fragment getFragment() {
    AppointmentDetailFragment appointmentDetailFragment = (AppointmentDetailFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (appointmentDetailFragment == null) {
      appointmentDetailFragment = AppointmentDetailFragment.newInstance();
    }
    fragment = appointmentDetailFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    AppointmentDetailPresenter appointmentDetailPresenter = new AppointmentDetailPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(appointmentDetailPresenter);

    return appointmentDetailPresenter;
  }

  public static void navigate(CustomNavigationResultListener listener, Context context,
                              AgendaItemViewModel agendaItemViewModel) {
    Intent intent = new Intent(context, AppointmentDetailActivity.class);
    intent.putExtra(APPOINTMENT_MODEL_KEY, agendaItemViewModel);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        EDIT_APPOINTMENT_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        EDIT_APPOINTMENT_REQUEST_CODE);
    } else {
      context.startActivity(intent);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }

}
