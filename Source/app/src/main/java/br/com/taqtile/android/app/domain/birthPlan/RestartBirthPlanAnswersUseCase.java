package br.com.taqtile.android.app.domain.birthPlan;

import java.util.List;

import br.com.taqtile.android.app.data.birthPlan.BirthPlanRepository;
import br.com.taqtile.android.app.domain.birthPlan.models.RemoveAnswerParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import java8.util.stream.StreamSupport;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class RestartBirthPlanAnswersUseCase extends
  BaseUseCase<List<QuestionItemResult>, String, BirthPlanRepository> {

  public RestartBirthPlanAnswersUseCase(BirthPlanRepository birthPlanRepository) {
    super(birthPlanRepository);
  }

  @Override
  public Observable<List<QuestionItemResult>> execute(String id) {

    return getRepository().removeAnswer(new RemoveAnswerParams(id));

  }

}
