package br.com.taqtile.android.app.domain.healthOperators;

import java.util.List;

import br.com.taqtile.android.app.data.healthOperators.HealthOperatorsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.healthOperators.mappers.HealthOperatorItemResultToViewModelMapper;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class ListHealthOperatorsUseCase extends
  BaseUseCase<List<HealthOperatorViewModel>, Void, HealthOperatorsRepository> {

  public ListHealthOperatorsUseCase(HealthOperatorsRepository healthOperatorsRepository) {
    super(healthOperatorsRepository);
  }

  @Override
  public Observable<List<HealthOperatorViewModel>> execute(Void aVoid) {
    return getRepository().list()
      .map(HealthOperatorItemResultToViewModelMapper::perform);
  }
}
