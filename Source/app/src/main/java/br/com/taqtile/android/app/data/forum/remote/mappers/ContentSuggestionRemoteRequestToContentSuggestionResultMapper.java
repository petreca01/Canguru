package br.com.taqtile.android.app.data.forum.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.request.ContentSuggestionRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.response.ContentSuggestionItemRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.ContentSuggestionRemoteResponse;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionItemResult;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionRemoteRequestToContentSuggestionResultMapper {

  public static ContentSuggestionResult perform(BaseRemoteResponse<ContentSuggestionRemoteResponse> response){
    ContentSuggestionRemoteResponse contentSuggestionRemoteResponse = response.getResult();
    return new ContentSuggestionResult(
      contentSuggestionRemoteResponse.getContentsInPage(),
      ContentSuggestionRemoteRequestToContentSuggestionResultMapper.mapSuggestions(contentSuggestionRemoteResponse.getContents()),
      contentSuggestionRemoteResponse.getTotalContents());
  }

  private static List<ContentSuggestionItemResult> mapSuggestions(List<ContentSuggestionItemRemoteResponse> contentSuggestionItemRemoteResponses){
    return StreamSupport.stream(contentSuggestionItemRemoteResponses)
      .map(ContentSuggestionRemoteRequestToContentSuggestionResultMapper::mapSuggestion)
      .collect(Collectors.toList());
  }

  private static ContentSuggestionItemResult mapSuggestion(ContentSuggestionItemRemoteResponse contentSuggestionItemRemoteResponse){
    return new ContentSuggestionItemResult(contentSuggestionItemRemoteResponse.getId(),
      contentSuggestionItemRemoteResponse.getType(),
      contentSuggestionItemRemoteResponse.getTitle(),
      contentSuggestionItemRemoteResponse.getResume(),
      contentSuggestionItemRemoteResponse.getLikes(),
      contentSuggestionItemRemoteResponse.getCountReplies(),
      contentSuggestionItemRemoteResponse.getContentParentId(),
      contentSuggestionItemRemoteResponse.getContentId()
      );
  }

}
