package br.com.taqtile.android.app.presentation.forgotpassword;

import br.com.taqtile.android.app.domain.account.ForgotPasswordUseCase;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordParams;
import br.com.taqtile.android.app.support.Injection;
import br.com.taqtile.android.app.support.NavigationManager;
import rx.Observable;

/**
 * Created by taqtile on 11/10/16.
 */

public class ForgotPasswordPresenter implements ForgotPasswordContract.Presenter {

    private ForgotPasswordContract.View mView;
    private ForgotPasswordUseCase useCase;

    public ForgotPasswordPresenter(ForgotPasswordContract.View forgotPasswordFragment, NavigationManager navigationManager) {
        mView = forgotPasswordFragment;

        useCase = Injection.provideForgotPasswordUseCase();

        mView.setPresenter(this);
        mView.setNavigationManager(navigationManager);
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public Observable<ForgotPasswordResult> recoverPassword(String email) {
        return useCase.execute(new ForgotPasswordParams(email));
    }
}
