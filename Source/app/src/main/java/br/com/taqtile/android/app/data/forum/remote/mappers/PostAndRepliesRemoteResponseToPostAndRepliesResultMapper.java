package br.com.taqtile.android.app.data.forum.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.CategoryItemResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostAndRepliesRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostItemResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.ReplyRemoteResponse;
import br.com.taqtile.android.app.domain.forum.models.CategoryItem;
import br.com.taqtile.android.app.domain.forum.models.PostAndRepliesResult;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.domain.forum.models.ReplyResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/5/17.
 */

public class PostAndRepliesRemoteResponseToPostAndRepliesResultMapper {
  public static PostAndRepliesResult perform(BaseRemoteResponse<PostAndRepliesRemoteResponse> response) {
    PostAndRepliesRemoteResponse result = response.getResult();
    return new PostAndRepliesResult(map(result.getPost()), mapReplies(result.getReplies()));
  }

  private static PostResult map(PostItemResponse postItemResponse) {
    return new PostResult(postItemResponse.getId(), postItemResponse.getOperatorId(),
      postItemResponse.getUserId(), postItemResponse.getTitle(), postItemResponse.getContent(),
      postItemResponse.getActive(), postItemResponse.getCreatedAt(),
      postItemResponse.getUpdatedAt(), postItemResponse.getUserType(),
      postItemResponse.getUsername(), postItemResponse.getUserPicture(),
      postItemResponse.getLikes(), postItemResponse.getCountReplies(),
      postItemResponse.getLiked(), map(postItemResponse.getCategories()));
  }

  private static List<CategoryItem> map(List<CategoryItemResponse> categoryItemResponses) {
    return StreamSupport.stream(categoryItemResponses).map(categoryItem ->
      new CategoryItem(categoryItem.getId(), categoryItem.getName()))
      .collect(Collectors.toList());
  }

  private static List<ReplyResult> mapReplies(List<ReplyRemoteResponse> replies) {
    return StreamSupport.stream(replies).map(reply -> new ReplyResult(reply.getId(),
      reply.getId(),
      reply.getOperatorId(),
      reply.getUserId(),
      reply.getReply(),
      reply.getActive(),
      reply.getCreatedAt(),
      reply.getUpdatedAt(),
      reply.getUserType(),
      reply.getUsername(),
      reply.getUserPicture(),
      reply.getLikes(),
      reply.getLiked())).collect(Collectors.toList());
  }

}
