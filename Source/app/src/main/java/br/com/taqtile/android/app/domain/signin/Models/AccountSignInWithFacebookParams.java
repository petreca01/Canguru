package br.com.taqtile.android.app.domain.signin.Models;

/**
 * Created by taqtile on 3/29/17.
 */

public class AccountSignInWithFacebookParams {
  private String facebookToken;

  public AccountSignInWithFacebookParams(String facebookToken) {
    this.facebookToken = facebookToken;
  }


  public String getFacebookToken() {
    return facebookToken;
  }

  public void setFacebookToken(String facebookToken) {
    this.facebookToken = facebookToken;
  }
}
