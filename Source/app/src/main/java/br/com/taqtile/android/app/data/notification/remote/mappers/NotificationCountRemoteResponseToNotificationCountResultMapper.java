package br.com.taqtile.android.app.data.notification.remote.mappers;

import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationCountRemoteResponse;
import br.com.taqtile.android.app.domain.notification.models.NotificationCountResult;

/**
 * Created by taqtile on 07/04/17.
 */

public class NotificationCountRemoteResponseToNotificationCountResultMapper {

  public static NotificationCountResult perform(
    NotificationCountRemoteResponse notificationCountRemoteResponse) {
    return new NotificationCountResult(notificationCountRemoteResponse.getNotifications(),
      notificationCountRemoteResponse.getInboxes());
  }
}
