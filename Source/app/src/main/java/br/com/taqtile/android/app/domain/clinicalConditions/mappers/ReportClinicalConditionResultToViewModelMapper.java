package br.com.taqtile.android.app.domain.clinicalConditions.mappers;

import br.com.taqtile.android.app.domain.account.mappers.UserDataResultToViewModelMapper;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportClinicalConditionResult;
import br.com.taqtile.android.app.presentation.condition.models.ReportClinicalConditionViewModel;

/**
 * Created by taqtile on 5/19/17.
 */

public class ReportClinicalConditionResultToViewModelMapper {

  public static ReportClinicalConditionViewModel perform(
    ReportClinicalConditionResult reportClinicalConditionResult) {
    return new ReportClinicalConditionViewModel(
      UserDataResultToViewModelMapper.perform(reportClinicalConditionResult.getUser()),
      UserClinicalConditionResultToViewModelMapper
        .perform(reportClinicalConditionResult.getUserCondition()));
  }

}
