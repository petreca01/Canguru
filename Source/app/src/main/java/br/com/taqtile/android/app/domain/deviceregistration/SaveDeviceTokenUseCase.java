package br.com.taqtile.android.app.domain.deviceregistration;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.deviceregistration.DeviceInfoRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.deviceregistration.models.DeviceInfoParams;
import rx.Observable;

/**
 * Created by taqtile on 7/11/16.
 */

public class SaveDeviceTokenUseCase extends BaseUseCase<EmptyResult, DeviceInfoParams,
  DeviceInfoRepository>{

  public SaveDeviceTokenUseCase(DeviceInfoRepository deviceInfoRepository) {
    super(deviceInfoRepository);
  }

  @Override
  public Observable<EmptyResult> execute(DeviceInfoParams deviceInfoParams) {
    return getRepository().register(deviceInfoParams);
  }
}
