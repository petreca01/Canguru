package br.com.taqtile.android.app.presentation.signin;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInParams;
import br.com.taqtile.android.app.domain.signin.SignInUseCase;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/23/16.
 */

public class SignInPresenter implements SignInContract.Presenter {
    private SignInUseCase signInUseCase;

    public SignInPresenter() {
        this.setupUseCase();
    }

    private void setupUseCase() {
        this.signInUseCase = Injection.provideSignInUseCase();
    }

    @Override
    public Observable<EmptyResult> signIn(String email, String password) {
        return this.signInUseCase.execute(new AccountSignInParams(email, password));
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }
}
