package br.com.taqtile.android.app.presentation.appointmentdetail;

import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 26/04/17.
 */

public interface AppointmentDetailContract {

  interface Presenter extends BasePresenter {

    Observable<AgendaItemViewModel> finishAppointment(FinishAppointmentParams finishAppointmentParams);

    Observable<String> removeAppointment(RemoveAppointmentParams removeAppointmentParams);

    Observable<AgendaItemViewModel> clearScheduleAppointment(ScheduleParams scheduleParams);
  }
}
