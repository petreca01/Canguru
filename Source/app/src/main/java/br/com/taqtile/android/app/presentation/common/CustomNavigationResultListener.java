package br.com.taqtile.android.app.presentation.common;

import android.os.Bundle;

/**
 * Created by taqtile on 4/26/17.
 */

public interface CustomNavigationResultListener {
  void onNavigationResult(Bundle data, int resultCode);
}
