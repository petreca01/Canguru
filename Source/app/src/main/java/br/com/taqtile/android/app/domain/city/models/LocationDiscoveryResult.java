package br.com.taqtile.android.app.domain.city.models;

/**
 * Created by taqtile on 4/19/17.
 */

public class LocationDiscoveryResult {

  private String zipCode;
  private String city;
  private String state;
  private String latitude;
  private String longitude;

  public LocationDiscoveryResult() {
  }

  public LocationDiscoveryResult(String zipCode, String city, String state, String latitude,
                                 String longitude) {
    this.zipCode = zipCode;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }
}
