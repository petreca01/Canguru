package br.com.taqtile.android.app.domain.exams.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamDataResult {

  private Boolean loaded;

  private Integer id;

  private String userType;

  private Integer operatorId;

  private String operator;

  private Boolean canOperatorSeeRiskAnswers;

  private String name;

  private String about;

  private String profilePicture;

  private String coverPicture;

  private String babyName;

  private String cpf;

  private String userBirthdate;

  private String email;

  private String cellphone;

  private String state;

  private String zipCode;

  private String latitude;

  private String longitude;

  private String token;

  private String dum;

  private String dolpUpdatedAt;

  private Integer sure;

  private Integer igWeeks;

  private Integer igDays;

  private Integer scheduleEngagement;

  private Integer risk;

  private Boolean active;

  private String deactivatedAt;

  private String deactivationReason;

  private String conclusionDate;

  private String conclusionText;

  private String conclusionMethod;

  private String pregnancyDatesUpdatedAt;

  private String locationBlockedAt;

  private String locationPrivacyAgreedAt;

  private String createdAt;

  private String updatedAt;

  private Boolean operatorHighlight;

  private String quarter;

  private String code;

  private Integer idType;

  private Integer type;

  private String weekBegin;

  private String weekEnd;

  private String dayEnd;

  private String description;

  private String descriptionImagemUrl;

  private String fullDescription;

  private Integer optional;

  private String observation;

  private Integer accomplished;

  private String dataMark;

  private String observationMark;

  private Integer isAppointment;

  public ExamDataResult(Boolean loaded, Integer id, String userType, Integer operatorId,
    String operator, Boolean canOperatorSeeRiskAnswers, String name, String about,
    String profilePicture, String coverPicture, String babyName, String cpf, String userBirthdate,
    String email, String cellphone, String state, String zipCode, String latitude, String longitude,
    String token, String dum, String dolpUpdatedAt, Integer sure, Integer igWeeks, Integer igDays,
    Integer scheduleEngagement, Integer risk, Boolean active, String deactivatedAt,
    String deactivationReason, String conclusionDate, String conclusionText,
    String conclusionMethod, String pregnancyDatesUpdatedAt, String locationBlockedAt,
    String locationPrivacyAgreedAt, String createdAt, String updatedAt, Boolean operatorHighlight,
    String quarter, String code, Integer idType, Integer type, String weekBegin, String weekEnd,
    String dayEnd, String description, String descriptionImagemUrl, String fullDescription,
    Integer optional, String observation, Integer accomplished, String dataMark,
    String observationMark, Integer isAppointment) {
    this.loaded = loaded;
    this.id = id;
    this.userType = userType;
    this.operatorId = operatorId;
    this.operator = operator;
    this.canOperatorSeeRiskAnswers = canOperatorSeeRiskAnswers;
    this.name = name;
    this.about = about;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.babyName = babyName;
    this.cpf = cpf;
    this.userBirthdate = userBirthdate;
    this.email = email;
    this.cellphone = cellphone;
    this.state = state;
    this.zipCode = zipCode;
    this.latitude = latitude;
    this.longitude = longitude;
    this.token = token;
    this.dum = dum;
    this.dolpUpdatedAt = dolpUpdatedAt;
    this.sure = sure;
    this.igWeeks = igWeeks;
    this.igDays = igDays;
    this.scheduleEngagement = scheduleEngagement;
    this.risk = risk;
    this.active = active;
    this.deactivatedAt = deactivatedAt;
    this.deactivationReason = deactivationReason;
    this.conclusionDate = conclusionDate;
    this.conclusionText = conclusionText;
    this.conclusionMethod = conclusionMethod;
    this.pregnancyDatesUpdatedAt = pregnancyDatesUpdatedAt;
    this.locationBlockedAt = locationBlockedAt;
    this.locationPrivacyAgreedAt = locationPrivacyAgreedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.operatorHighlight = operatorHighlight;
    this.quarter = quarter;
    this.code = code;
    this.idType = idType;
    this.type = type;
    this.weekBegin = weekBegin;
    this.weekEnd = weekEnd;
    this.dayEnd = dayEnd;
    this.description = description;
    this.descriptionImagemUrl = descriptionImagemUrl;
    this.fullDescription = fullDescription;
    this.optional = optional;
    this.observation = observation;
    this.accomplished = accomplished;
    this.dataMark = dataMark;
    this.observationMark = observationMark;
    this.isAppointment = isAppointment;
  }

  public Boolean isLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public String getUserType() {
    return userType;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public String getOperator() {
    return operator;
  }

  public Boolean isCanOperatorSeeRiskAnswers() {
    return canOperatorSeeRiskAnswers;
  }

  public String getName() {
    return name;
  }

  public String getAbout() {
    return about;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getCpf() {
    return cpf;
  }

  public String getUserBirthdate() {
    return userBirthdate;
  }

  public String getEmail() {
    return email;
  }

  public String getCellphone() {
    return cellphone;
  }

  public String getState() {
    return state;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public String getToken() {
    return token;
  }

  public String getDum() {
    return dum;
  }

  public String getDolpUpdatedAt() {
    return dolpUpdatedAt;
  }

  public Integer getSure() {
    return sure;
  }

  public Integer getIgWeeks() {
    return igWeeks;
  }

  public Integer getIgDays() {
    return igDays;
  }

  public Integer getScheduleEngagement() {
    return scheduleEngagement;
  }

  public Integer getRisk() {
    return risk;
  }

  public Boolean isActive() {
    return active;
  }

  public String getDeactivatedAt() {
    return deactivatedAt;
  }

  public String getDeactivationReason() {
    return deactivationReason;
  }

  public String getConclusionDate() {
    return conclusionDate;
  }

  public String getConclusionText() {
    return conclusionText;
  }

  public String getConclusionMethod() {
    return conclusionMethod;
  }

  public String getPregnancyDatesUpdatedAt() {
    return pregnancyDatesUpdatedAt;
  }

  public String getLocationBlockedAt() {
    return locationBlockedAt;
  }

  public String getLocationPrivacyAgreedAt() {
    return locationPrivacyAgreedAt;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public Boolean isOperatorHighlight() {
    return operatorHighlight;
  }

  public String getQuarter() {
    return quarter;
  }

  public String getCode() {
    return code;
  }

  public Integer getIdType() {
    return idType;
  }

  public Integer getType() {
    return type;
  }

  public String getWeekBegin() {
    return weekBegin;
  }

  public String getWeekEnd() {
    return weekEnd;
  }

  public String getDayEnd() {
    return dayEnd;
  }

  public String getDescription() {
    return description;
  }

  public String getDescriptionImagemUrl() {
    return descriptionImagemUrl;
  }

  public String getFullDescription() {
    return fullDescription;
  }

  public Integer getOptional() {
    return optional;
  }

  public String getObservation() {
    return observation;
  }

  public Integer getAccomplished() {
    return accomplished;
  }

  public String getDataMark() {
    return dataMark;
  }

  public String getObservationMark() {
    return observationMark;
  }

  public Integer getIsAppointment() {
    return isAppointment;
  }
}
