package br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo;

import br.com.taqtile.android.app.domain.account.EditUserDataUseCase;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 20/04/17.
 */

public class HealthcareUserAdditionalInfoPresenter implements HealthcareUserAdditionalInfoContract.Presenter {

  private EditUserDataUseCase editUserDataUseCase;

  public HealthcareUserAdditionalInfoPresenter() {
    this.editUserDataUseCase = Injection.provideEditUserDataUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> updateUserData(Integer operatorId, String telephone, String cpf, String documentType) {
    return editUserDataUseCase.execute(new EditUserDataParams(operatorId, telephone, cpf, documentType));
  }
}
