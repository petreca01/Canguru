package br.com.taqtile.android.app.data.symptoms.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class ReportSymptomRemoteRequest extends BaseRemoteRequest {
    @RequestName("symptom_id")
    private Integer symptomId;

    @RequestName("date")
    private String date;

    @RequestName("description")
    private String description;

  public ReportSymptomRemoteRequest(Integer symptomId, String date, String description) {
    this.symptomId = symptomId;
    this.date = date;
    this.description = description;
  }

}
