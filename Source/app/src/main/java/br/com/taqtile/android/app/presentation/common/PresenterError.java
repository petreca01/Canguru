package br.com.taqtile.android.app.presentation.common;

import android.support.annotation.StringDef;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by taqtile on 4/30/17.
 */

public class PresenterError extends Throwable {

  public static final String INVALID_DATA = "invalid_data";
  public static final String NO_CONNECTION = "no_connection";
  public static final String NOT_LOGGED_IN = "not_logged_in";
  public static final String NOT_FOUND = "not_found";
  public static final String NOT_AUTHORIZED = "not_authorized";
  public static final String TIMEOUT = "timeout";
  public static final String UNKNOWN = "unknown";

  private static @ErrorType String errorType = UNKNOWN;

  @Retention(SOURCE)
  @StringDef({
    INVALID_DATA,
    NO_CONNECTION,
    NOT_LOGGED_IN,
    NOT_FOUND,
    NOT_AUTHORIZED,
    TIMEOUT,
    UNKNOWN
  })
  public @interface ErrorType {}

  @NotNull
  private String message;

  public void setErrorType(@ErrorType String errorType) {
    this.errorType = errorType;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public @ErrorType String getErrorType() {
    return errorType;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
