package br.com.taqtile.android.app.presentation.channelpostdetail;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 4/24/17.
 */

public interface ChannelPostDetailContract {

  interface Presenter extends BasePresenter {
    Observable<List<ListingsViewModel>> deleteComment(Integer postId, Integer commentId);
  }

}
