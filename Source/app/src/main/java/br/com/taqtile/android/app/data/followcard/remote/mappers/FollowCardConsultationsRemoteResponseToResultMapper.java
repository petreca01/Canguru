package br.com.taqtile.android.app.data.followcard.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardConsultationsRemoteResponse;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardConsultationsResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardConsultationsRemoteResponseToResultMapper {

  public static List<FollowCardConsultationsResult> perform(
    List<FollowCardConsultationsRemoteResponse> followCardConsultationsRemoteResponse) {
    return StreamSupport.stream(followCardConsultationsRemoteResponse)
      .map(FollowCardConsultationsRemoteResponseToResultMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardConsultationsResult map(
    FollowCardConsultationsRemoteResponse followCardConsultationsRemoteResponse) {
    return new FollowCardConsultationsResult(
      followCardConsultationsRemoteResponse.getId(),
      followCardConsultationsRemoteResponse.getDoctorId(),
      followCardConsultationsRemoteResponse.getPatientId(),
      followCardConsultationsRemoteResponse.getUsuarioId(),
      followCardConsultationsRemoteResponse.getDate(),
      followCardConsultationsRemoteResponse.getPregnancyWeeks(),
      followCardConsultationsRemoteResponse.getPregnancyDays(),
      followCardConsultationsRemoteResponse.getHeight(),
      followCardConsultationsRemoteResponse.getWeight(),
      followCardConsultationsRemoteResponse.getReason(),
      followCardConsultationsRemoteResponse.getBloodPressureSystolic(),
      followCardConsultationsRemoteResponse.getBloodPressureDiastolic(),
      followCardConsultationsRemoteResponse.getUsingFerricSulfate(),
      followCardConsultationsRemoteResponse.getUsingFolicAcid(),
      followCardConsultationsRemoteResponse.getUterineHeight(),
      followCardConsultationsRemoteResponse.getFetalPresentation(),
      followCardConsultationsRemoteResponse.getFetalHeartRate(),
      followCardConsultationsRemoteResponse.getFetalMovement(),
      followCardConsultationsRemoteResponse.getEdema(),
      followCardConsultationsRemoteResponse.getToque(),
      followCardConsultationsRemoteResponse.getCreatedAt(),
      followCardConsultationsRemoteResponse.getUpdatedAt(),
      followCardConsultationsRemoteResponse.getDoctorName());
  }
}
