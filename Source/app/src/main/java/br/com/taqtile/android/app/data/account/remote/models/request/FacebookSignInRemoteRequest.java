package br.com.taqtile.android.app.data.account.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/29/17.
 */

public class FacebookSignInRemoteRequest extends BaseRemoteRequest {

  @RequestName("token")
  private String facebookToken;

  public FacebookSignInRemoteRequest(String facebookToken) {
    this.facebookToken = facebookToken;
  }

  public String getFacebookToken() {
    return facebookToken;
  }
}
