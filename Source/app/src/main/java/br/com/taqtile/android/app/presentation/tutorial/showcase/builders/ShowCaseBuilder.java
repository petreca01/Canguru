package br.com.taqtile.android.app.presentation.tutorial.showcase.builders;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;

import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.Rectangle;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.RectangleWithShape;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.SurroundingShape;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.TransparentCircle;

/**
 * Created by taqtile on 5/23/17.
 */

public class ShowCaseBuilder {

  public static Bitmap build(Context context, @ColorRes int backgroundColor, View target, SurroundingShape shapeFocusedInView) {
    Rectangle background = FullScreenRectangleBuilder.build(context, ContextCompat.getColor(context, backgroundColor));
    shapeFocusedInView.focusIn(target);

    return new RectangleWithShape(background, shapeFocusedInView).draw();
  }

}

