package br.com.taqtile.android.app.domain.exams.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsResult {

  private ExamsValuesResult examsValuesResult;

  private Integer done;

  private String observations;

  private String date;

  public ExamsResult() {
  }

  public ExamsResult(ExamsValuesResult examsValuesResult, Integer done, String observations,
                     String date) {
    this.examsValuesResult = examsValuesResult;
    this.done = done;
    this.observations = observations;
    this.date = date;
  }

  public ExamsValuesResult getExamsValuesResult() {
    return examsValuesResult;
  }

  public Integer getDone() {
    return done;
  }

  public String getObservations() {
    return observations;
  }

  public String getDate() {
    return date;
  }
}
