package br.com.taqtile.android.app.presentation.symptomsguide;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.symptomshistory.SymptomsHistoryActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomsGuideActivity extends TemplateBackActivity {

  private SymptomsGuideFragment fragment;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public Fragment getFragment() {
    SymptomsGuideFragment symptomsGuideFragment = (SymptomsGuideFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (symptomsGuideFragment == null) {
      symptomsGuideFragment = SymptomsGuideFragment.newInstance();
    }
    fragment = symptomsGuideFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    SymptomsGuidePresenter symptomsGuidePresenter = new SymptomsGuidePresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(symptomsGuidePresenter);

    return symptomsGuidePresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, SymptomsGuideActivity.class);
    context.startActivity(intent);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
