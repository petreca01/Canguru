package br.com.taqtile.android.app.presentation.changepassword;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.edittexts.ConfirmPasswordLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.NewPasswordLabelEditTextCaption;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FormTextHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.validatablehelpers.FormComparisonValidatable;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 11/15/16.
 */

public class ChangePasswordFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  @BindView(R.id.fragment_change_password_new_password_form)
  NewPasswordLabelEditTextCaption newPasswordEditText;

  @BindView(R.id.fragment_change_password_confirm_password_form)
  ConfirmPasswordLabelEditTextCaption confirmPasswordEditText;

  @BindView(R.id.fragment_change_password_content)
  ScrollView contentLayout;

  @BindView(R.id.fragment_change_password_send_button)
  ProgressBarButton sendButton;

  private ChangePasswordContract.Presenter presenter;

  private NavigationManager navigationManager;

  private String errorMessage;

  private CustomToolbar customToolbar;

  private Validatable[] forms;

  private List<Validatable> errorForms;

  private FormComparisonValidatable passwordComparationValidatable;

  public static ChangePasswordFragment newInstance() {
    return new ChangePasswordFragment();
  }

  public ChangePasswordFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_change_password, container, false);
    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();

    setupPasswordForms();
    setupViews();
    setupFormsList();
  }

  private void setupViews() {
    sendButton.setButtonListener(this);
  }

  private void setupPasswordForms() {
    //If you want to compare two password, pass them in the PasswordComparationValidatable constructor
    passwordComparationValidatable = new FormComparisonValidatable(newPasswordEditText, confirmPasswordEditText);
  }

  private void setupFormsList() {
    forms = new Validatable[]{newPasswordEditText, confirmPasswordEditText};
    errorForms = new ArrayList<Validatable>();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_change_password_placeholder;
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      showFormValidationError();
      isValid = false;
    } else {
      isValid = comparePasswords();
    }

    return isValid;
  }

  private boolean comparePasswords() {
    if (!confirmPasswordEditText.getText().contentEquals(newPasswordEditText.getText())) {
      showSnackbar(getResources().getString(R.string.fragment_change_password_error_text), Snackbar.LENGTH_LONG);
      return false;
    }

    return true;
  }

  private void showFormValidationError() {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_change_password_title));
    }
  }

  public void setPresenter(ChangePasswordContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    sendButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    sendButton.showDefaultState();
  }

  @Override
  public void onButtonClicked() {
    if (locallyValidateForms()) {
      showLoading();
      Subscription subscription = presenter.changePassword(newPasswordEditText.getText(),
        confirmPasswordEditText.getText())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(emptyResult -> onChangePasswordSuccess(),
          this::onChangePasswordFailure
        );
      compositeSubscription.add(subscription);
    }
  }

  private void onChangePasswordSuccess() {
    hideLoading();
    getActivity().finish();
  }

  private void onChangePasswordFailure(Throwable error) {
    hideLoading();
    showSnackbar(error.getMessage(), Snackbar.LENGTH_LONG);
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorFormsInputPlaceholderTexts = FormTextHelper.getFormsInputPlaceholderText(errorViews);
    return StringListFormatterHelper.buildStringList(errorFormsInputPlaceholderTexts, getResources().getString(R.string.required_symbol));
  }

  @Override
  public void willAppear() {
    super.willAppear();
    setupToolbar();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_change_password),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
