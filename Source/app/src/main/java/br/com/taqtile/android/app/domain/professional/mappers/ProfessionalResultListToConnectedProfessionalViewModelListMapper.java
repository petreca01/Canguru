package br.com.taqtile.android.app.domain.professional.mappers;

import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 27/04/17.
 */

public class ProfessionalResultListToConnectedProfessionalViewModelListMapper {
  public static List<ConnectedProfessionalViewModel> perform(List<ProfessionalResult> professionalResultList) {
    return StreamSupport.stream(professionalResultList)
      .map(ProfessionalResultToConnectedProfessionalViewModelMapper::perform)
      .collect(Collectors.toList());
  }
}
