package br.com.taqtile.android.app.data.professional.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.professional.remote.models.request.ProfessionalAuthorizationRemoteRequest;
import br.com.taqtile.android.app.data.professional.remote.models.response.ProfessionalInfoItemRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfessionalRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ProfessionalServices> services;

  public ProfessionalRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(ProfessionalServices.class);
  }

  public Observable<BaseRemoteResponse<List<ProfessionalInfoItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(ProfessionalServices::list));
  }

  public Observable<BaseRemoteResponse<ProfessionalInfoItemRemoteResponse>> authorizedAccess(
    ProfessionalAuthorizationRemoteRequest authorizedAccessRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.authorizedAccess(authorizedAccessRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<ProfessionalInfoItemRemoteResponse>> revokeAccess(
    ProfessionalAuthorizationRemoteRequest revokeAccessRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.revokeAccess(revokeAccessRemoteRequest.getFormData())));
  }
}
