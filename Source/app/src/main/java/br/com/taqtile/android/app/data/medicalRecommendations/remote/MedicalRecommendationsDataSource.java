package br.com.taqtile.android.app.data.medicalRecommendations.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.medicalRecommendations.remote.models.MedicalRecommendationItemRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class MedicalRecommendationsDataSource extends BaseRemoteDataSource {
  private Observable<MedicalRecommendationsServices> services;

  public MedicalRecommendationsDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(MedicalRecommendationsServices.class);
  }

  public Observable<BaseRemoteResponse<List<MedicalRecommendationItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(MedicalRecommendationsServices::list));
  }

  public Observable<BaseRemoteResponse<MedicalRecommendationItemRemoteResponse>> markAsRead(
    Integer medicalRecommendationId) {
    return performRequest(
      services.flatMap(services -> services.markAsRead(medicalRecommendationId)));
  }
}
