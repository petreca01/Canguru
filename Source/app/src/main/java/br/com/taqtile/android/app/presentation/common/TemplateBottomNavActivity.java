package br.com.taqtile.android.app.presentation.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.misc.general.FABMenu;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.bottomnav.component.BottomNavigation;
import br.com.taqtile.android.cleanbase.presentation.activity.BaseActivityFragment;
import br.com.taqtile.android.app.R;

/**
 * Created by taqtile on 7/4/16.
 */

public abstract class TemplateBottomNavActivity extends BaseActivityFragment {

  public final static int NOTIFICATION_BOTTOM_NAV_POSITION = 4;

  protected BottomNavigation bottomNav;
  private FABMenu fabMenu;
  private FloatingActionButton floatingActionButton;
  private FrameLayout fabOverlay;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupNavBar();
    bindViews();
  }

  @Override
  public int getLoadingViewContainerId() {
    return R.id.loading_view_root;
  }

  @Override
  public int getActivityLayoutId() {
    return R.layout.activity_base_fragment;
  }

  @Override
  public int getToolbarId() {
    return R.id.activity_base_fragment_toolbar;
  }

  @Override
  public int getAppBarLayoutId() {
    return 0;
  }

  @Override
  public int getViewPagerId() {
    return R.id.activity_base_fragment_viewpager;
  }

  @Override
  public int getBottomNavViewId() {
    return R.id.activity_base_fragment_bottomnav;
  }

  @Override
  public boolean isBottomNavigationColoredMode() {
    return false;
  }

  @Override
  public int getFABId() {
    return R.id.activity_base_fragment_fab;
  }

  private void setupNavBar() {
    bottomNav = (BottomNavigation) findViewById(R.id.activity_base_fragment_bottomnav);
    bottomNav.setActiveColor(ContextCompat.getColor(getApplicationContext(), R.color.color_primary));
    bottomNav.setInactiveColor(ContextCompat.getColor(getApplicationContext(), R.color.color_gray_extra_dark));
    bottomNav.setNotificationBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bkg_notification_badge));
    bottomNav.setNotificationMarginLeft((int) getResources().getDimension(R.dimen.margin_small), (int) getResources().getDimension(R.dimen.margin_small));
    //Make the text invisible, because the badge only appears if there is text set
    bottomNav.setNotificationTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_white));
  }

  public void showNotificationBadge(String count){
    bottomNav.setNotification(count, NOTIFICATION_BOTTOM_NAV_POSITION);
  }

  public void hideNotificationBadge(){
    bottomNav.setNotification("", NOTIFICATION_BOTTOM_NAV_POSITION);
  }

  public void hideBottomNav() {
    bottomNav.setVisibility(View.GONE);
  }

  public FABMenu getFABMenu() {
    return fabMenu;
  }

  public FloatingActionButton getFAB(){
    return floatingActionButton;
  }

  public void showFABOverlay(){
    FadeEffectHelper.fadeInView(fabOverlay);
  }

  public void hideFABOverlay(){
    FadeEffectHelper.fadeOutView(fabOverlay);
  }

  public FrameLayout getFABOverlay(){
    return fabOverlay;
  }

  private void bindViews() {
    fabMenu = (FABMenu) findViewById(R.id.activity_base_fragment_fab_menu);
    floatingActionButton = (FloatingActionButton) findViewById(getFABId());
    fabOverlay = (FrameLayout) findViewById(R.id.activity_base_fragment_fab_overlay);
  }

  public void setActiveItem(int position) {
    bottomNav.setActiveItem(position);
  }

}
