package br.com.taqtile.android.app.data.city.geocondingAPI.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 6/5/17.
 */

public class GeographicCoordinateGeocodingAPIResponse {

  @SerializedName("lat")
  private String latitude;
  @SerializedName("lng")
  private String longitude;

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }
}
