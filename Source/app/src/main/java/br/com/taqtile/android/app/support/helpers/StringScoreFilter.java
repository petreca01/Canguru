package br.com.taqtile.android.app.support.helpers;

import android.util.Log;

import java.util.Collections;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;

/**
 * Created by taqtile on 4/18/17.
 */

public class StringScoreFilter {

  public static void orderMaternitiesByScore(List<MaternityViewModel> maternities, String text) {
    Collections.sort(maternities,
      (firstValue, secondValue) -> compare(firstValue, secondValue, text));
  }

  public static void orderCitiesByScore(List<String> cities, String text) {
    Collections.sort(cities,
      (firstValue, secondValue) -> compare(StringScore.score(firstValue, text), StringScore.score(secondValue, text)));
  }

  public static void orderHealthOperatorsByScore(List<HealthOperatorViewModel> options, String text) {
    Collections.sort(options, (firstValue, secondValue) -> compare(firstValue, secondValue, text));
  }

  private static int compare(MaternityViewModel firstValue, MaternityViewModel secondValue, String text) {
    if (firstValue == null || secondValue == null) {
      return 0;
    }

    return compare(StringScore.scoreComposedWord(firstValue.getName(), text),
      StringScore.scoreComposedWord(secondValue.getName(), text));
  }

  private static int compare(HealthOperatorViewModel firstValue, HealthOperatorViewModel secondValue, String text) {
    if (firstValue == null || secondValue == null) {
      return 0;
    }

    return compare(StringScore.score(firstValue.getName(), text),
      StringScore.score(secondValue.getName(), text));
  }

  private static int compare(double a, double b) {
    return a < b ? 1
      : a > b ? -1
      : 0;
  }


}
