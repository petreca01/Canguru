package br.com.taqtile.android.app.support;

import android.app.Activity;

import com.facebook.CallbackManager;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.account.remote.AccountRemoteDataSource;
import br.com.taqtile.android.app.data.account.sharedpreferences.AccountSharedPreferencesDataSource;
import br.com.taqtile.android.app.data.agenda.AgendaRepository;
import br.com.taqtile.android.app.data.agenda.remote.AgendaRemoteDataSource;
import br.com.taqtile.android.app.data.birthPlan.BirthPlanRepository;
import br.com.taqtile.android.app.data.birthPlan.remote.BirthPlanRemoteDataSource;
import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.data.channel.remote.ChannelRemoteDataSource;
import br.com.taqtile.android.app.data.city.LocationDiscoveryRepository;
import br.com.taqtile.android.app.data.city.geocondingAPI.LocationDiscoveryGeocodingAPIDataSource;
import br.com.taqtile.android.app.data.clinicalcondition.ClinicalConditionRepository;
import br.com.taqtile.android.app.data.clinicalcondition.remote.ClinicalConditionRemoteDataSource;
import br.com.taqtile.android.app.data.deviceregistration.DeviceInfoRepository;
import br.com.taqtile.android.app.data.deviceregistration.remote.DeviceRemoteDataSource;
import br.com.taqtile.android.app.data.exams.ExamsRepository;
import br.com.taqtile.android.app.data.exams.remote.ExamsRemoteDataSource;
import br.com.taqtile.android.app.data.facebook.FacebookRepository;
import br.com.taqtile.android.app.data.facebook.remote.FacebookRemoteDataSource;
import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.data.followcard.remote.FollowCardRemoteDataSource;
import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.data.forum.remote.ForumRemoteDataSource;
import br.com.taqtile.android.app.data.gestationconclusion.GestationConclusionRepository;
import br.com.taqtile.android.app.data.gestationconclusion.remote.GestationConclusionRemoteDataSource;
import br.com.taqtile.android.app.data.healthOperators.HealthOperatorsRepository;
import br.com.taqtile.android.app.data.healthOperators.remote.HealthOperatorsRemoteDataSource;
import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.data.maternity.remote.MaternityRemoteDataSource;
import br.com.taqtile.android.app.data.medicalRecommendations.MedicalRecommendationsRepository;
import br.com.taqtile.android.app.data.medicalRecommendations.remote.MedicalRecommendationsDataSource;
import br.com.taqtile.android.app.data.notification.NotificationRepository;
import br.com.taqtile.android.app.data.notification.local.NotificationLocalDataSource;
import br.com.taqtile.android.app.data.notification.remote.NotificationRemoteDataSource;
import br.com.taqtile.android.app.data.professional.ProfessionalRepository;
import br.com.taqtile.android.app.data.professional.remote.ProfessionalRemoteDataSource;
import br.com.taqtile.android.app.data.profile.ProfileRepository;
import br.com.taqtile.android.app.data.profile.local.ImageDataSource;
import br.com.taqtile.android.app.data.profile.remote.ProfileRemoteDataSource;
import br.com.taqtile.android.app.data.riskAnalysis.RiskAnalysisRepository;
import br.com.taqtile.android.app.data.riskAnalysis.remote.RiskAnalysisRemoteDataSource;
import br.com.taqtile.android.app.data.symptoms.SymptomsRepository;
import br.com.taqtile.android.app.data.symptoms.remote.SymptomsRemoteDataSource;
import br.com.taqtile.android.app.data.weeklycontent.WeeklyContentRepository;
import br.com.taqtile.android.app.data.weeklycontent.remote.WeeklyContentDataSource;
import br.com.taqtile.android.app.domain.account.EditUserDataUseCase;
import br.com.taqtile.android.app.domain.account.ForgotPasswordUseCase;
import br.com.taqtile.android.app.domain.account.LogoutUseCase;
import br.com.taqtile.android.app.domain.account.SignUpUseCase;
import br.com.taqtile.android.app.domain.account.UpdatePasswordUseCase;
import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.agenda.CreateAppointmentUseCase;
import br.com.taqtile.android.app.domain.agenda.FinishAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.RemoveAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.ScheduleAppointmentUseCase;
import br.com.taqtile.android.app.domain.agenda.ShowAgendaUseCase;
import br.com.taqtile.android.app.domain.birthPlan.ListBirthPlanQuestionAnswersUseCase;
import br.com.taqtile.android.app.domain.birthPlan.RestartBirthPlanAnswersUseCase;
import br.com.taqtile.android.app.domain.birthPlan.SendBirthPlanQAUseCase;
import br.com.taqtile.android.app.domain.channel.ChannelListUseCase;
import br.com.taqtile.android.app.domain.channel.ChannelPostDataUseCase;
import br.com.taqtile.android.app.domain.channel.ChannelPostsUseCase;
import br.com.taqtile.android.app.domain.channel.DeleteChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.FollowChannelUseCase;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostReplyUseCase;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.ReplyChannelPostUseCase;
import br.com.taqtile.android.app.domain.city.LocationDiscoveryByZipCodeUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.ListConditionsUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.ListUserConditionsUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.ReportConditionUseCase;
import br.com.taqtile.android.app.domain.deviceregistration.SaveDeviceTokenUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardConsultationsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardExamsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardUltrasoundsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardVaccinesUseCase;
import br.com.taqtile.android.app.domain.forum.CreatePostUseCase;
import br.com.taqtile.android.app.domain.forum.DeleteForumPostUseCase;
import br.com.taqtile.android.app.domain.forum.ForumPostDataUseCase;
import br.com.taqtile.android.app.domain.forum.GetContentSuggestionUseCase;
import br.com.taqtile.android.app.domain.forum.LikeForumPostItemUseCase;
import br.com.taqtile.android.app.domain.forum.ReplyForumPostUseCase;
import br.com.taqtile.android.app.domain.forum.WarnPostUseCase;
import br.com.taqtile.android.app.domain.gestation.FetchExamUseCase;
import br.com.taqtile.android.app.domain.gestation.StartOrEditGestationUseCase;
import br.com.taqtile.android.app.domain.gestationconclusion.ListGestationConclusionAnswersUseCase;
import br.com.taqtile.android.app.domain.gestationconclusion.SendGestationConclusionQAUseCase;
import br.com.taqtile.android.app.domain.healthOperators.ListHealthOperatorsUseCase;
import br.com.taqtile.android.app.domain.healthOperators.SearchHealthOperatorsUseCase;
import br.com.taqtile.android.app.domain.maternity.CommentAboutMaternityUseCase;
import br.com.taqtile.android.app.domain.maternity.DeleteMaternityCommentUseCase;
import br.com.taqtile.android.app.domain.maternity.GetMaternityDetailsUseCase;
import br.com.taqtile.android.app.domain.maternity.ListByCityUseCase;
import br.com.taqtile.android.app.domain.maternity.ListCitiesUseCase;
import br.com.taqtile.android.app.domain.maternity.RateMaternityUseCase;
import br.com.taqtile.android.app.domain.medicalRecommendations.ListRecommendationsUseCase;
import br.com.taqtile.android.app.domain.medicalRecommendations.MarkAsReadUseCase;
import br.com.taqtile.android.app.domain.notification.ListNotificationsUseCase;
import br.com.taqtile.android.app.domain.notification.SetNotificationVisualizedUseCase;
import br.com.taqtile.android.app.domain.professional.AuthorizeProfessionalUseCase;
import br.com.taqtile.android.app.domain.professional.ListProfessionalsUseCase;
import br.com.taqtile.android.app.domain.professional.RevokeProfessionalUseCase;
import br.com.taqtile.android.app.domain.profile.EditPublicProfileUserDataUseCase;
import br.com.taqtile.android.app.domain.profile.PublicProfileUserDataUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.FirstQuestionForGroupUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.ListGroupsUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.SendAnswersUseCase;
import br.com.taqtile.android.app.domain.search.SearchPostsUseCase;
import br.com.taqtile.android.app.domain.signin.FacebookLoginUseCase;
import br.com.taqtile.android.app.domain.signin.SignInUseCase;
import br.com.taqtile.android.app.domain.symptoms.ListSymptomsUseCase;
import br.com.taqtile.android.app.domain.symptoms.ListUserSymptomsUseCase;
import br.com.taqtile.android.app.domain.symptoms.ReportSymptomUseCase;
import br.com.taqtile.android.app.domain.symptoms.SendUserSymptomsUseCase;
import br.com.taqtile.android.app.domain.timeline.FetchTimelineUseCase;
import br.com.taqtile.android.app.domain.weeklycontent.FetchWeeklyContentUseCase;
import br.com.taqtile.android.app.support.analytics.AnalyticsToolBox;

/**
 * Enables injection of mock implementations for data sources
 * at compile time. This is useful for testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

  public static AnalyticsToolBox provideAnalyticsToolBox() {
    return new AnalyticsToolBox();
  }

  //region DataSources

  private static ChannelRemoteDataSource provideChannelRemoteDataSource() {
    return new ChannelRemoteDataSource();
  }

  private static ForumRemoteDataSource provideForumRemoteDataSource() {
    return new ForumRemoteDataSource();
  }

  private static AccountRemoteDataSource provideAccountRemoteDataSource() {
    return new AccountRemoteDataSource();
  }

  private static ProfessionalRemoteDataSource provideProfessionalRemoteDataSource() {
    return new ProfessionalRemoteDataSource();
  }

  private static ProfileRemoteDataSource provideProfileRemoteDataSource() {
    return new ProfileRemoteDataSource();
  }

  private static ExamsRemoteDataSource provideExamsRemoteDataSource() {
    return new ExamsRemoteDataSource();
  }

  private static NotificationRemoteDataSource provideNotificationRemoteDataSource() {
    return new NotificationRemoteDataSource();
  }

  private static RiskAnalysisRemoteDataSource provideRiskAnalysisRemoteDataSource() {
    return new RiskAnalysisRemoteDataSource();
  }

  private static MedicalRecommendationsDataSource provideMedicalRecommendationsDataSource() {
    return new MedicalRecommendationsDataSource();
  }

  private static BirthPlanRemoteDataSource provideBirthPlanRemoteDataSource() {
    return new BirthPlanRemoteDataSource();
  }

  private static SymptomsRemoteDataSource provideSymptomsRemoteDataSource() {
    return new SymptomsRemoteDataSource();
  }

  private static MaternityRemoteDataSource provideMaternityRemoteDataSource() {
    return new MaternityRemoteDataSource();
  }

  private static ImageDataSource provideImageDataSource() {
    return new ImageDataSource();
  }

  private static AccountSharedPreferencesDataSource provideAccountSharedPreferencesDataSource() {
    return new AccountSharedPreferencesDataSource();
  }

  private static NotificationLocalDataSource provideNotificationLocalDataSource() {
    return new NotificationLocalDataSource();
  }

  private static ClinicalConditionRemoteDataSource provideClinicalConditionRemoteDataSource() {
    return new ClinicalConditionRemoteDataSource();
  }

  private static FacebookRemoteDataSource provideFacebookRemoteDataSource(Activity activity,
                                                                          CallbackManager callbackManager) {
    return new FacebookRemoteDataSource(activity, callbackManager);
  }

  private static HealthOperatorsRemoteDataSource provideHealthOperatorsRemoteDataSource() {
    return new HealthOperatorsRemoteDataSource();
  }

  private static AgendaRemoteDataSource provideAgendaRemoteDataSource() {
    return new AgendaRemoteDataSource();
  }

  private static WeeklyContentDataSource provideWeeklyContentDataSource() {
    return new WeeklyContentDataSource();
  }

  private static FollowCardRemoteDataSource provideFollowCardRemoteDataSource() {
    return new FollowCardRemoteDataSource();
  }

  private static DeviceRemoteDataSource provideDeviceRemoteDataSource() {
    return new DeviceRemoteDataSource();
  }

  private static LocationDiscoveryGeocodingAPIDataSource provideLocationDiscoveryGeocodingAPIDataSource() {
    return new LocationDiscoveryGeocodingAPIDataSource();
  }

  private static GestationConclusionRemoteDataSource provideGestationConclusionRemoteDataSource() {
    return new GestationConclusionRemoteDataSource();
  }

  //endregion

  //region Repositories

  private static ChannelRepository provideChannelRepository() {
    return new ChannelRepository(provideChannelRemoteDataSource());
  }

  private static ForumRepository provideForumRepository() {
    return new ForumRepository(provideForumRemoteDataSource());
  }

  private static AccountRepository provideAccountRepository() {
    return new AccountRepository(provideAccountRemoteDataSource(),
      provideAccountSharedPreferencesDataSource());
  }

  private static ProfessionalRepository provideProfessionalRepository() {
    return new ProfessionalRepository(provideProfessionalRemoteDataSource());
  }

  private static ProfileRepository provideProfileRepository() {
    return new ProfileRepository(provideProfileRemoteDataSource(), provideImageDataSource());
  }

  private static ExamsRepository provideExamsRepository() {
    return new ExamsRepository(provideExamsRemoteDataSource());
  }

  private static NotificationRepository provideNotificationRepository() {
    return new NotificationRepository(provideNotificationRemoteDataSource(),
      provideNotificationLocalDataSource());
  }

  private static RiskAnalysisRepository provideRiskAnalysisRepository() {
    return new RiskAnalysisRepository(provideRiskAnalysisRemoteDataSource());
  }

  private static MedicalRecommendationsRepository provideMedicalRecommendationsRepository() {
    return new MedicalRecommendationsRepository(provideMedicalRecommendationsDataSource());
  }

  private static SymptomsRepository provideSymptomsRepository() {
    return new SymptomsRepository(provideSymptomsRemoteDataSource());
  }

  private static MaternityRepository provideMaternityRepository() {
    return new MaternityRepository(provideMaternityRemoteDataSource());
  }

  private static ClinicalConditionRepository provideClinicalConditionRepository() {
    return new ClinicalConditionRepository(provideClinicalConditionRemoteDataSource());
  }

  private static BirthPlanRepository provideBirthPlanRepository() {
    return new BirthPlanRepository(provideBirthPlanRemoteDataSource());
  }

  private static FacebookRepository provideFacebookRepository(Activity activity,
                                                              CallbackManager callbackManager) {
    return new FacebookRepository(provideFacebookRemoteDataSource(activity, callbackManager));
  }

  private static HealthOperatorsRepository provideHealthOperatorsRepository() {
    return new HealthOperatorsRepository(provideHealthOperatorsRemoteDataSource());
  }

  private static AgendaRepository provideAgendaRepository() {
    return new AgendaRepository(provideAgendaRemoteDataSource());
  }

  private static WeeklyContentRepository provideWeeklyContentRepository() {
    return new WeeklyContentRepository(provideWeeklyContentDataSource());
  }

  private static FollowCardRepository provideFollowCardRepository() {
    return new FollowCardRepository(provideFollowCardRemoteDataSource());
  }

  private static DeviceInfoRepository provideDeviceInfoRepository() {
    return new DeviceInfoRepository(provideDeviceRemoteDataSource());
  }

  private static LocationDiscoveryRepository provideLocationDiscoveryRepository() {
    return new LocationDiscoveryRepository(provideLocationDiscoveryGeocodingAPIDataSource());
  }

  private static GestationConclusionRepository provideGestationConclusionRepository() {
    return new GestationConclusionRepository(provideGestationConclusionRemoteDataSource());
  }

  //endregion

  //region Use Cases
  public static SetNotificationVisualizedUseCase provideSetNotificationVisualizedUseCase() {
    return new SetNotificationVisualizedUseCase(provideNotificationRepository());
  }

  public static ListNotificationsUseCase provideListNotificationsUseCase() {
    return new ListNotificationsUseCase(provideNotificationRepository());
  }

  public static LocationDiscoveryByZipCodeUseCase provideDiscoveryByZipCodeUseCase() {
    return new LocationDiscoveryByZipCodeUseCase(provideLocationDiscoveryRepository());
  }

  public static ListByCityUseCase provideListByCityUseCase() {
    return new ListByCityUseCase(provideMaternityRepository());
  }

  public static ListCitiesUseCase provideListCitiesUseCase() {
    return new ListCitiesUseCase(provideMaternityRepository());
  }

  public static GetMaternityDetailsUseCase provideGetMaternityDetailsUseCase() {
    return new GetMaternityDetailsUseCase(provideMaternityRepository());
  }

  public static DeleteMaternityCommentUseCase provideDeleteMaternityUseCase() {
    return new DeleteMaternityCommentUseCase(provideMaternityRepository());
  }

  public static CommentAboutMaternityUseCase provideCommentAboutMaternityUseCase() {
    return new CommentAboutMaternityUseCase(provideMaternityRepository());
  }

  public static RateMaternityUseCase provideRateMaternityUseCase() {
    return new RateMaternityUseCase(provideMaternityRepository());
  }

  public static ChannelListUseCase provideChannelListUseCase() {
    return new ChannelListUseCase(provideChannelRepository());
  }

  public static SearchPostsUseCase provideSearchPostsUseCase() {
    return new SearchPostsUseCase(provideForumRepository());
  }

  public static ForgotPasswordUseCase provideForgotPasswordUseCase() {
    return new ForgotPasswordUseCase(provideAccountRepository());
  }

  public static UpdatePasswordUseCase provideUpdatePasswordUseCase() {
    return new UpdatePasswordUseCase(provideAccountRepository());
  }

  public static SignInUseCase provideSignInUseCase() {
    return new SignInUseCase(provideAccountRepository());
  }

  public static FetchTimelineUseCase provideFetchTimelineUseCase() {
    return new FetchTimelineUseCase(provideForumRepository(), provideChannelRepository());
  }

  public static FollowChannelUseCase provideFollowChannelUseCase() {
    return new FollowChannelUseCase(provideChannelRepository());
  }

  public static ChannelPostsUseCase provideChannelPostsUseCase() {
    return new ChannelPostsUseCase(provideChannelRepository());
  }

  public static ChannelPostDataUseCase provideChannelPostDataUseCase() {
    return new ChannelPostDataUseCase(provideChannelRepository());
  }

  public static DeleteChannelPostUseCase provideDeleteChannelPostUseCase() {
    return new DeleteChannelPostUseCase(provideChannelRepository());
  }

  public static LikeChannelPostReplyUseCase provideLikeChannelPostReplyUseCase() {
    return new LikeChannelPostReplyUseCase(provideChannelRepository());
  }

  public static LikeChannelPostUseCase provideLikeChannelPostUseCase() {
    return new LikeChannelPostUseCase(provideChannelRepository());
  }

  public static ReplyChannelPostUseCase provideReplyChannelPostUseCase() {
    return new ReplyChannelPostUseCase(provideChannelRepository());
  }

  public static ForumPostDataUseCase provideForumPostDataUseCase() {
    return new ForumPostDataUseCase(provideForumRepository());
  }

  public static DeleteForumPostUseCase provideDeleteForumPostUseCase() {
    return new DeleteForumPostUseCase(provideForumRepository());
  }

  public static LikeForumPostItemUseCase provideLikeForumPostReplyUseCase() {
    return new LikeForumPostItemUseCase(provideForumRepository());
  }

  public static ReplyForumPostUseCase provideReplyForumPostUseCase() {
    return new ReplyForumPostUseCase(provideForumRepository());
  }

  public static WarnPostUseCase provideWarnPostUseCase() {
    return new WarnPostUseCase(provideForumRepository());
  }

  public static SignUpUseCase provideSignUpUseCase() {
    return new SignUpUseCase(provideAccountRepository());
  }

  public static EditPublicProfileUserDataUseCase provideEditPublicProfileUserDataUseCase() {
    return new EditPublicProfileUserDataUseCase(provideProfileRepository(), provideAccountRepository());
  }

  public static PublicProfileUserDataUseCase providePublicProfileUserDataUseCase() {
    return new PublicProfileUserDataUseCase(provideProfileRepository());
  }

  public static UserDataUseCase provideUserDataUseCase() {
    return new UserDataUseCase(provideAccountRepository());
  }

  public static EditUserDataUseCase provideEditUserDataUseCase() {
    return new EditUserDataUseCase(provideAccountRepository(), provideAccountRepository());
  }

  public static CreatePostUseCase provideCreatePostUseCase() {
    return new CreatePostUseCase(provideForumRepository());
  }

  public static GetContentSuggestionUseCase provideGetContentSuggestionUseCase(){
    return new GetContentSuggestionUseCase(provideForumRepository());
  }

  public static StartOrEditGestationUseCase provideStartGestationUseCase() {
    return new StartOrEditGestationUseCase(provideAccountRepository(), provideExamsRepository());
  }

  public static SendUserSymptomsUseCase provideSendUserSymptomsUseCase() {
    return new SendUserSymptomsUseCase(provideSymptomsRepository());
  }

  public static ReportSymptomUseCase provideReportSymptomUseCase() {
    return new ReportSymptomUseCase(provideSymptomsRepository());
  }

  public static ReportConditionUseCase provideReportConditionUseCase() {
    return new ReportConditionUseCase(provideClinicalConditionRepository());
  }

  public static ListUserSymptomsUseCase provideListUserSymptomsUseCase() {
    return new ListUserSymptomsUseCase(provideSymptomsRepository());
  }

  public static ListUserConditionsUseCase provideListUserConditionsUseCase() {
    return new ListUserConditionsUseCase(provideClinicalConditionRepository());
  }

  public static ListSymptomsUseCase provideListSymptomsUseCase() {
    return new ListSymptomsUseCase(provideSymptomsRepository());
  }

  public static ListConditionsUseCase provideListConditionsUseCase() {
    return new ListConditionsUseCase(provideClinicalConditionRepository());
  }

  public static ListBirthPlanQuestionAnswersUseCase provideListBirthPlanQuestionAnswersUseCase() {
    return new ListBirthPlanQuestionAnswersUseCase(provideBirthPlanRepository());
  }

  public static RestartBirthPlanAnswersUseCase provideRestartBirthPlanAnswersUseCase() {
    return new RestartBirthPlanAnswersUseCase(provideBirthPlanRepository());
  }

  public static SendBirthPlanQAUseCase provideSendBirthPlanQAUseCase() {
    return new SendBirthPlanQAUseCase(provideBirthPlanRepository());
  }

  public static FirstQuestionForGroupUseCase provideFirstQuestionForGroupUseCase() {
    return new FirstQuestionForGroupUseCase(provideRiskAnalysisRepository());
  }

  public static ListGroupsUseCase provideListGroupsUseCase() {
    return new ListGroupsUseCase(provideRiskAnalysisRepository());
  }

  public static SendAnswersUseCase provideSendAnswersUseCase() {
    return new SendAnswersUseCase(provideRiskAnalysisRepository());
  }

  public static FacebookLoginUseCase provideFacebookLoginUseCase(Activity activity,
                                                                 CallbackManager callbackManager) {
    return new FacebookLoginUseCase(provideFacebookRepository(activity, callbackManager),
      provideAccountRepository());
  }

  public static ListHealthOperatorsUseCase provideListHealthOperatorsUseCase() {
    return new ListHealthOperatorsUseCase(provideHealthOperatorsRepository());
  }

  public static ListProfessionalsUseCase provideListProfessionalsUseCase() {
    return new ListProfessionalsUseCase(provideProfessionalRepository());
  }

  public static AuthorizeProfessionalUseCase provideAuthorizeProfessionalUseCase() {
    return new AuthorizeProfessionalUseCase(provideProfessionalRepository());
  }

  public static RevokeProfessionalUseCase provideRevokeProfessionalUseCase() {
    return new RevokeProfessionalUseCase(provideProfessionalRepository());
  }

  public static SearchHealthOperatorsUseCase provideSearchHealthOperatorsUseCase() {
    return new SearchHealthOperatorsUseCase(provideHealthOperatorsRepository());
  }

  public static LogoutUseCase provideLogoutUseCase() {
    return new LogoutUseCase(provideAccountRepository());
  }

  public static ShowAgendaUseCase provideShowAgendaUseCase() {
    return new ShowAgendaUseCase(provideAgendaRepository());
  }

  public static CreateAppointmentUseCase provideCreateAppointmentUseCase() {
    return new CreateAppointmentUseCase(provideAgendaRepository());
  }

  public static FinishAgendaItemUseCase provideFinishAgendaItemUseCase() {
    return new FinishAgendaItemUseCase(provideAgendaRepository());
  }

  public static RemoveAgendaItemUseCase provideRemoveAgendaItemUseCase() {
    return new RemoveAgendaItemUseCase(provideAgendaRepository());
  }

  public static ScheduleAppointmentUseCase provideScheduleAppoitmentUseCase() {
    return new ScheduleAppointmentUseCase(provideAgendaRepository());
  }

  public static ListRecommendationsUseCase provideListRecommendationsUseCase() {
    return new ListRecommendationsUseCase(provideMedicalRecommendationsRepository());
  }

  public static MarkAsReadUseCase provideMarkAsReadUseCase() {
    return new MarkAsReadUseCase(provideMedicalRecommendationsRepository());
  }

  public static FetchExamUseCase provideFetchExamUseCase() {
    return new FetchExamUseCase(provideExamsRepository());
  }

  public static FetchWeeklyContentUseCase provideFetchWeeklyContentUseCase() {
    return new FetchWeeklyContentUseCase(provideWeeklyContentRepository());
  }

  public static FollowCardUseCase provideFollowCardUseCase() {
    return new FollowCardUseCase(provideFollowCardRepository());
  }

  public static FollowCardConsultationsUseCase provideFollowCardConsultationsUseCase() {
    return new FollowCardConsultationsUseCase(provideFollowCardRepository());
  }

  public static FollowCardUltrasoundsUseCase provideFollowCardUltrasoundsUseCase() {
    return new FollowCardUltrasoundsUseCase(provideFollowCardRepository());
  }

  public static FollowCardExamsUseCase provideFollowCardExamsUseCase() {
    return new FollowCardExamsUseCase(provideFollowCardRepository());
  }

  public static FollowCardVaccinesUseCase provideFollowCardVaccinesUseCase() {
    return new FollowCardVaccinesUseCase(provideFollowCardRepository());
  }

  public static SaveDeviceTokenUseCase provideSaveDeviceTokenUseCase() {
    return new SaveDeviceTokenUseCase(provideDeviceInfoRepository());
  }

  public static ListGestationConclusionAnswersUseCase provideListGestationConclusionAnswersUseCase() {
    return new ListGestationConclusionAnswersUseCase(provideGestationConclusionRepository());
  }

  public static SendGestationConclusionQAUseCase provideSendGestationConclusionQAUseCase() {
    return new SendGestationConclusionQAUseCase(provideGestationConclusionRepository());
  }
  //endregion
}
