package br.com.taqtile.android.app.domain.symptoms.mapper;

import java.util.List;

import br.com.taqtile.android.app.domain.clinicalConditions.mappers.ClinicalConditionResultToViewModelMapper;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionItemResult;
import br.com.taqtile.android.app.domain.symptoms.models.SubSymptomResult;
import br.com.taqtile.android.app.domain.symptoms.models.SymptomResult;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/9/17.
 */

public class SymptomResultToViewModelMapper {

  private static final String SUBMENU = "submenu";
  private static final String PREGNANCY_WEEKS = "pregnancy_weeks";
  private static final String LESS_THAN = "less_than";
  private static final String GREATER_THAN_OR_EQUAL_TO = "greater_than_or_equal_to";
  private static final String SUBITEM = "subitem";

  public static List<SymptomViewModel> perform(List<SymptomResult> symptomResults,
                                               Integer gestationWeeks) {
    return StreamSupport.stream(symptomResults)
      .filter(Objects::nonNull)
      .map(symptom -> SymptomResultToViewModelMapper.map(symptom, gestationWeeks))
      .collect(Collectors.toList());
  }

  private static SymptomViewModel map(SymptomResult symptomResult, Integer gestationWeeks) {
    return new SymptomViewModel(
      SymptomResultToViewModelMapper.mapParent(symptomResult, gestationWeeks),
      SymptomResultToViewModelMapper.mapChildren(symptomResult.getChildren()));
  }

  private static SubSymptomViewModel mapParent(SymptomResult symptomResult, Integer gestationWeeks) {


    if (symptomResult.getCondition() != null &&
      symptomResult.getCondition().equals(PREGNANCY_WEEKS)) {

      return findChild(symptomResult.getChildren(), gestationWeeks);
    } else {
      return new SubSymptomViewModel(symptomResult.getId(), symptomResult.getParentId(),
        symptomResult.getName(), symptomResult.getExplanation(),
        symptomResult.getHowToAvoid(), symptomResult.getWhenToWorry());
    }
  }

  private static SubSymptomViewModel findChild(
    List<SubSymptomResult> subSymptomResults, Integer gestationWeeks) {
    return StreamSupport.stream(subSymptomResults)
      .filter(Objects::nonNull)
      .filter(symptom -> satisfiesConditions(symptom, gestationWeeks))
      .map(SymptomResultToViewModelMapper::mapChild)
      .findFirst()
      .orElse(null);
  }

  private static List<SubSymptomViewModel> mapChildren(List<SubSymptomResult> subSymptomResult) {
    return StreamSupport.stream(subSymptomResult)
      .filter(Objects::nonNull)
      .filter(symptom -> symptom.getCondition().equals(SUBITEM))
      .map(SymptomResultToViewModelMapper::mapChild)
      .collect(Collectors.toList());
  }

  private static SubSymptomViewModel mapChild(SubSymptomResult subSymptomResult) {

    String symptomName = subSymptomResult.getCondition().equals(SUBITEM) ?
      subSymptomResult.getName() + " " + subSymptomResult.getConditionTrigger() :
      subSymptomResult.getName();

    return new SubSymptomViewModel(subSymptomResult.getId(), subSymptomResult.getParentId(),
      symptomName, subSymptomResult.getExplanation(),
      subSymptomResult.getHowToAvoid(), subSymptomResult.getWhenToWorry());
  }

  private static boolean satisfiesConditions(SubSymptomResult subSymptomResult,
                                             Integer gestationWeeks) {
    return (subSymptomResult.getCondition().equals(LESS_THAN) &&
      gestationWeeks < Integer.valueOf(subSymptomResult.getConditionTrigger())) ||
      (subSymptomResult.getCondition().equals(GREATER_THAN_OR_EQUAL_TO) &&
        gestationWeeks >= Integer.valueOf(subSymptomResult.getConditionTrigger()));
  }
}
