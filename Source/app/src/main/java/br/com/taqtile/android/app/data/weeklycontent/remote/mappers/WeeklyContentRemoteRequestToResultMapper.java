package br.com.taqtile.android.app.data.weeklycontent.remote.mappers;

import br.com.taqtile.android.app.data.weeklycontent.remote.models.response.WeeklyContentRemoteResponse;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentResult;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentRemoteRequestToResultMapper {

  public static WeeklyContentResult perform(WeeklyContentRemoteResponse weeklyContentRemoteResponse) {
    return new WeeklyContentResult(weeklyContentRemoteResponse.getWeek(),
      weeklyContentRemoteResponse.getImage_url(), weeklyContentRemoteResponse.getContent());
  }
}
