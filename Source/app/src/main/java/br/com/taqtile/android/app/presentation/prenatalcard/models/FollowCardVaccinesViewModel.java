package br.com.taqtile.android.app.presentation.prenatalcard.models;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardVaccinesViewModel {

  private Integer id;
  private String name;
  private String result;
  private String createdAt;

  public FollowCardVaccinesViewModel(Integer id, String name, String result, String createdAt) {
    this.id = id;
    this.name = name;
    this.result = result;
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getResult() {
    return result;
  }

  public String getCreatedAt() {
    return createdAt;
  }

}
