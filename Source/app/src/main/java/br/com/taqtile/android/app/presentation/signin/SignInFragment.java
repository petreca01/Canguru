package br.com.taqtile.android.app.presentation.signin;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.edittexts.PasswordNoValidationLabelEditTextCaption;
import br.com.taqtile.android.app.misc.dialog.EmailFormInDialog;
import br.com.taqtile.android.app.misc.dialog.TextViewInDialog;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FormTextHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginContract;
import br.com.taqtile.android.app.presentation.signup.SignUpActivity;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.FORGOT_PASSWORD_KEY;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Created by taqtile on 5/23/16.
 */

public class SignInFragment extends AppBaseFragment
  implements View.OnClickListener, MenuItem.OnMenuItemClickListener, ProgressBarButton.ProgressBarButtonButtonListener {
  private static final String TAG = "SignInFragment";

  //region View Components

  @BindView(R.id.fragment_signin_scroll_view)
  ScrollView scrollView;

  @BindView(R.id.fragment_signin_email_cpf_edit_text)
  EmailLabelEditTextCaption emailEditText;

  @BindView(R.id.fragment_signin_password_edit_text)
  PasswordNoValidationLabelEditTextCaption passwordEditText;

  @BindView(R.id.fragment_signin_forgot_button)
  Button forgotPasswordButton;

  @BindView(R.id.fragment_sign_in_progress_bar_button)
  ProgressBarButton enterButton;

  @BindView(R.id.fragment_sign_in_content_layout)
  LinearLayout contentLayout;

  private SignInContract.Presenter presenter;
  private NavigationManager navigationManager;

  private String errorMessage;
  private CustomToolbar customToolbar;
  private Validatable[] forms;
  private List<Validatable> errorForms;

  //endregion

  public static SignInFragment newInstance() {
    return new SignInFragment();
  }

  public SignInFragment() {

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_signin, container, false);
    ButterKnife.bind(this, root);
    setupViews();
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupFormsList();
  }

  private void setupViews() {
    enterButton.setButtonListener(this);
    forgotPasswordButton.setOnClickListener(this);
  }

  private void setupFormsList() {
    forms = new Validatable[]{emailEditText, passwordEditText};
    errorForms = new ArrayList<Validatable>();
  }

  public void setPresenter(@NonNull SignInContract.Presenter presenter) {
    this.presenter = checkNotNull(presenter);
  }

  @Override
  public void willAppear() {
    super.willAppear();
    setupToolbar();
    enterButton.showDefaultState();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_signin),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_signin_title), Gravity.LEFT);
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> {
        getActivity().setResult(Activity.RESULT_CANCELED, new Intent());
        getActivity().finish();
      });
    }
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_signin_placeholder;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.fragment_signin_forgot_button:
        navigationManager.showForgotPasswordUI(this::returningFromForgotPassword, emailEditText.getText());
        break;
      default:
        break;
    }
  }

  private void returningFromForgotPassword(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      String successMessage = bundle.getString(FORGOT_PASSWORD_KEY, "");
      showSnackBar(successMessage);
    }
  }

  @Override
  public void showLoading() {
    enterButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    enterButton.showDefaultState();
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      showFormValidationError();
      ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      isValid = false;
    }
    return isValid;
  }

  public void showFormValidationError() {
    errorMessage = "";
    showSnackBar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)));
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms = FormTextHelper.getFormsInputPlaceholderText(errorViews);
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  private void showDefaultSignInButtonState() {
    enterButton.showDefaultState();
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    return true;
  }

  @Override
  public void onButtonClicked() {
    if (locallyValidateForms()) {
      showLoading();
      Subscription subscription = presenter.signIn(emailEditText.getText(), passwordEditText.getText())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(result -> onSignInSuccess(),
          error -> onSignInFailure(error));
      compositeSubscription.add(subscription);
    }
  }

  private void onSignInSuccess() {
    this.analytics.trackUser(emailEditText.getText());
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_login),
      getString(R.string.analytics_event_email),
      getString(R.string.analytics_label_login_email)
    ));
    hideLoading();
    getActivity().setResult(Activity.RESULT_OK, new Intent());
    getActivity().finish();
  }

  private void onSignInFailure(Throwable throwable) {
    hideLoading();
    showGenericErrorSnackbar(throwable);
    showDefaultSignInButtonState();
  }

  private void showGenericErrorSnackbar(Throwable throwable) {
    showSnackBar(throwable.getMessage());

    if (throwable.getMessage().equals("E-mail não registrado")) {
      showSignUpDialog();
    }
  }

  private void returningFromSignUp(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      getActivity().finish();
    }
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void showSignUpDialog() {
    String defaultValue = getString(R.string.fragment_signin_dialog_to_signup_text);

    final TextViewInDialog input = getTextViewForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_signin_dialog_to_signup_title))
      .setView(input)
      .setPositiveButton(getString(R.string.fragment_signin_dialog_to_signup_button_positive), (dialog, which) -> {
          navigationManager.showSignUpUI(this::returningFromSignUp, emailEditText.getText(), passwordEditText.getText());
        }
      ).setNegativeButton(getString(R.string.fragment_signin_dialog_to_signup_button_negative), (dialog, which) -> {
        // do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));

  }

  private TextViewInDialog getTextViewForDialog() {
    return new TextViewInDialog(getContext());
  }

}
