package br.com.taqtile.android.app.domain.followcard.models;


/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardExamsResult {

  private Integer id;
  private String name;
  private String result;
  private String date;
  private String createdAt;

  public FollowCardExamsResult(Integer id, String name, String result, String date,
                               String createdAt) {
    this.id = id;
    this.name = name;
    this.result = result;
    this.date = date;
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getResult() {
    return result;
  }

  public String getDate() {
    return date;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
