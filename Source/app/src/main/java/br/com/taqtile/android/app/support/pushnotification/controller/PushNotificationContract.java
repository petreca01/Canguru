package br.com.taqtile.android.app.support.pushnotification.controller;

import br.com.taqtile.android.cleanbase.push.notification.BaseGcmListenerContract;

/**
 * Created by taqtile on 7/11/16.
 */

public interface PushNotificationContract extends BaseGcmListenerContract {

    interface Service {

        void showNotificationUI(String message, String title, String action);

    }

    interface Controller {

        void simplePushReceived(String message, String title, String action);

    }
}
