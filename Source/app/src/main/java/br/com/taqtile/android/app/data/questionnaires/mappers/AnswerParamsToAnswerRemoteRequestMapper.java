package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerRemoteRequest;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;

/**
 * Created by taqtile on 07/04/17.
 */

public class AnswerParamsToAnswerRemoteRequestMapper {

  public static AnswerRemoteRequest perform(AnswerParams answerParams) {
    return new AnswerRemoteRequest<>(answerParams.getAnswer(), answerParams.getQuestionId());
  }
}
