package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.account.remote.models.request.EditUserRemoteRequest;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;

/**
 * Created by taqtile on 19/04/17.
 */

public class StartGestationParamsToEditUserRemoteRequestMapper {

  public static EditUserRemoteRequest perform(StartOrEditGestationParams startOrEditGestationParams) {
    return new EditUserRemoteRequest(startOrEditGestationParams.getDataForLastPeriod(),
      startOrEditGestationParams.getUserType(), startOrEditGestationParams.getBabyName(), startOrEditGestationParams.getDolpUpdatedAt());
  }
}
