package br.com.taqtile.android.app.data.agenda.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.response.AgendaItemRemoteResponse;
import br.com.taqtile.android.app.data.agenda.remote.models.response.AppointmentItemRemoteResponse;
import br.com.taqtile.android.app.domain.agenda.models.AgendaItemResult;

/**
 * Created by taqtile on 4/7/17.
 */

public class AgendaItemRemoteResponseToAgendaItemResultMapper {
  public static AgendaItemResult perform(AgendaItemRemoteResponse response) {

    return new AgendaItemResult(response.getLoaded(),
        response.getId(),
        response.getOperatorHighlight(),
        response.getQuarter(),
        response.getName(),
        response.getCode(),
        response.getTypeId(),
        response.getType(),
        response.getWeekStart(),
        response.getWeekEnd(),
        response.getDayEnd(),
        response.getDescription(),
        response.getDescriptionImageUrl(),
        response.getCompleteDescription(),
        response.getObservation(),
        response.getDone(),
        response.getCreatedAt(),
        response.getUpdatedAt(),
        response.getMarkingDate(),
        response.getMarkingObservation(),
        response.getIsAppointment()
        );
  }

  public static AgendaItemResult perform(AppointmentItemRemoteResponse response) {
    return new AgendaItemResult(response.getLoaded(),
      response.getId(),
      response.getOperatorHighlight(),
      response.getQuarter(),
      response.getName(),
      response.getCode(),
      response.getTypeId(),
      response.getType(),
      response.getWeekStart(),
      response.getWeekEnd(),
      response.getDayEnd(),
      response.getDescription(),
      response.getDescriptionImageUrl(),
      response.getCompleteDescription(),
      response.getObservation(),
      response.getDone(),
      response.getCreatedAt(),
      response.getUpdatedAt(),
      response.getMarkingDate(),
      response.getMarkingObservation(),
      response.getIsAppointment()
    );
  }
}
