package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by taqtile on 5/19/17.
 */

public class RisksRemoteResponse {

  @SerializedName("alert")
  private ArrayList<String> alert;

  @SerializedName("risk")
  private ArrayList<String> risk;

  public ArrayList<String> getAlert() {
    return alert;
  }

  public ArrayList<String> getRisk() {
    return risk;
  }
}
