package br.com.taqtile.android.app.domain.questionnaires.models;

import java.util.List;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class QuestionItemResult {

  private Integer id;

  private String question;

  private String description;

  private String answerType;

  private List<String> alternatives;

  private RangedAnswerResult range;

  private boolean unlocksQuestions;

  private Integer dependsOnQuestionId;

  private QuestionItemUnlockResult answerUnlockTrigger;

  private AnswerItemResult answer;

  public QuestionItemResult(Integer id, String question, String description, String answerType,
                            List<String> alternatives, RangedAnswerResult range,
                            boolean unlocksQuestions, Integer dependsOnQuestionId,
                            QuestionItemUnlockResult answerUnlockTrigger, AnswerItemResult answer) {
    this.id = id;
    this.question = question;
    this.description = description;
    this.answerType = answerType;
    this.alternatives = alternatives;
    this.range = range;
    this.unlocksQuestions = unlocksQuestions;
    this.dependsOnQuestionId = dependsOnQuestionId;
    this.answerUnlockTrigger = answerUnlockTrigger;
    this.answer = answer;
  }

  public Integer getId() {
    return id;
  }

  public String getQuestion() {
    return question;
  }

  public String getDescription() {
    return description;
  }

  public String getAnswerType() {
    return answerType;
  }

  public boolean isUnlocksQuestions() {
    return unlocksQuestions;
  }

  public Integer getDependsOnQuestionId() {
    return dependsOnQuestionId;
  }

  public QuestionItemUnlockResult getAnswerUnlockTrigger() {
    return answerUnlockTrigger;
  }

  public AnswerItemResult getAnswer() {
    return answer;
  }

  public List<String> getAlternatives() {
    return alternatives;
  }

  public RangedAnswerResult getRange() {
    return range;
  }
}
