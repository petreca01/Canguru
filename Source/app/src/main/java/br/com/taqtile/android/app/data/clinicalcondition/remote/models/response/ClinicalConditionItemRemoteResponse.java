package br.com.taqtile.android.app.data.clinicalcondition.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 3/23/17.
 */

public class ClinicalConditionItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("parent_id")
    private Integer parentId;

    @SerializedName("name")
    private String name;

    @SerializedName("logic")
    private String logic;

    @SerializedName("logic_trigger")
    private String logicTrigger;

    @SerializedName("risk")
    private Integer risk;

    @SerializedName("content")
    private String content;

    @SerializedName("childs")
    private List<ClinicalConditionItemRemoteResponse> childs;

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getLogic() {
        return logic;
    }

    public String getLogicTrigger() {
        return logicTrigger;
    }

    public Integer getRisk() {
        return risk;
    }

    public String getContent() {
        return content;
    }

    public List<ClinicalConditionItemRemoteResponse> getChilds() {
        return childs;
    }
}
