package br.com.taqtile.android.app.presentation.symptomdetail;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import br.com.taqtile.android.app.misc.general.CustomFlag;
import br.com.taqtile.android.app.misc.general.DatePickerCustomTitle;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAIL_ID_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAIL_PARENT_ID_KEY;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomDetailFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  @BindView(R.id.fragment_symptom_detail_add_symptom_button)
  ProgressBarButton addSymptomButton;
  @BindView(R.id.fragment_symptom_detail_avoid_section_description_body)
  CustomTextViewWithHTML avoidDescriptionBody;
  @BindView(R.id.fragment_symptom_detail_concerns_section_description_body)
  CustomTextViewWithHTML concernsDescriptionBody;
  @BindView(R.id.fragment_symptom_detail_description_header_with_image)
  DescriptionHeaderWithImage headerWithDescription;
  @BindView(R.id.fragment_symptom_detail_avoid_section_header)
  SectionHeader avoidSectionHeader;
  @BindView(R.id.fragment_symptom_detail_concerns_section_header)
  SectionHeader concernsSectionHeader;
  private DatePickerDialog datePickerDialog;
  @BindView(R.id.fragment_symptom_detail_loading)
  ProgressBarLoading loading;

  private NavigationManager navigationManager;
  private SymptomDetailContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private SubSymptomViewModel subSymptomViewModel;
  private int parentSymptomId;
  private int symptomId;

  public static SymptomDetailFragment newInstance() {
    return new SymptomDetailFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_symptom_detail, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupButtons();
    setupDatePicker();
  }

  private void setupDatePicker() {
    GregorianCalendar calendar = new GregorianCalendar();
    datePickerDialog = new DatePickerDialog(getContext(),
      getDatePickerListener(), calendar.get(Calendar.YEAR),
      calendar.get(Calendar.MONTH),
      calendar.get(Calendar.DAY_OF_MONTH));
    datePickerDialog.setCustomTitle(new DatePickerCustomTitle(getContext()));
  }

  private DatePickerDialog.OnDateSetListener getDatePickerListener() {
    return (view, year, month, dayOfMonth) -> {
      String date = DateFormatterHelper.formatDateForAPIWithoutHour(
        String.format(getString(R.string.fragment_symptom_detail_date_format), dayOfMonth, month + 1,
          year));
      subSymptomViewModel.setDate(date);
      registerSymptom();
    };
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_symptom_detail_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setupView() {
    headerWithDescription.hideImage();
    setupSectionHeaders();
    setupSymptom();
  }

  private void setupSectionHeaders() {
    avoidSectionHeader.setText(getResources().getString(R.string.fragment_symptom_detail_avoid_section_header_text));
    concernsSectionHeader.setText(getResources().getString(R.string.fragment_symptom_detail_concerns_section_header_text));
  }

  private void setupSymptom() {
    if (subSymptomViewModel != null) {
      setHeaderWithDescription(subSymptomViewModel.getName(), subSymptomViewModel.getExplanation());
      setAvoidDescriptionBody(subSymptomViewModel.getHowToAvoid());
      setConcernsDescriptionBody(subSymptomViewModel.getWhenToWorry());
    } else {
      parentSymptomId = getActivity().getIntent().getExtras().getInt(SYMPTOM_DETAIL_PARENT_ID_KEY, 0);
      symptomId = getActivity().getIntent().getExtras().getInt(SYMPTOM_DETAIL_ID_KEY);
      findSymptom(parentSymptomId, symptomId);
    }
  }

  private void setupButtons() {
    addSymptomButton.setButtonListener(this::onAddSymptomButtonClick);
  }

  private void onAddSymptomButtonClick() {
    datePickerDialog.show();
  }

  private void registerSymptom() {
    showButtonLoading();
    Subscription reportSymptomSubscription = presenter.reportSymptom(subSymptomViewModel)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onReportSuccess, this::onReportFailure, this::hideButtonLoading);
    compositeSubscription.add(reportSymptomSubscription);
  }

  private void onReportSuccess(EmptyResult emptyResult) {
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom),
      getString(R.string.analytics_event_registration),
      getString(R.string.analytics_label_user_registered_symptom)
    ));
  }

  private void onReportFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
  }

  private void setHeaderWithDescription(String title, String description) {
    headerWithDescription.setTitle(title);
    headerWithDescription.setDescription(description);
  }

  private void setSymptomRegistrationFlag(String registrationFlagText) {
    headerWithDescription.setFlagTypeAndText(CustomFlag.FlagType.Neutral, registrationFlagText);
  }

  private void setAvoidDescriptionBody(String avoidDescriptionBody) {
    this.avoidDescriptionBody.setTextWithHTML(avoidDescriptionBody);
  }

  private void setConcernsDescriptionBody(String concernsDescriptionBody) {
    this.concernsDescriptionBody.setTextWithHTML(concernsDescriptionBody);
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(SymptomDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }

  private void showButtonLoading() {
    addSymptomButton.showLoadingState();
  }

  private void hideButtonLoading() {
    addSymptomButton.showDefaultState();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_viewed_symptom)
    ));
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    findSymptom(parentSymptomId, symptomId);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_symptom_detail_placeholder;
  }

  public void setSubSymptomViewModel(SubSymptomViewModel subSymptomViewModel) {
    this.subSymptomViewModel = subSymptomViewModel;
  }

  @Override
  public void onButtonClicked() {

  }

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
    hidePlaceholder();
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
    hidePlaceholder();
  }

  private void findSymptom(int parentSymptomId, int symptomId) {
    showLoading();

    Subscription checkUserDataSubscription = presenter.fetchUserData()
      .flatMap(userData -> presenter.listSymptoms(userData.getGestationWeeks()))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(success -> this.onListSuccess(success, parentSymptomId, symptomId),
        this::onListFailure, this::hideLoading);
    compositeSubscription.add(checkUserDataSubscription);
  }

  private void onListSuccess(List<SymptomViewModel> symptomViewModels, int parentSymptomId, int symptomId) {

    SubSymptomViewModel subSymptomViewModel = null;

    if (parentSymptomId <= 0) {

      subSymptomViewModel = StreamSupport.stream(symptomViewModels)
        .filter(symptom -> symptom.getId() == symptomId)
        .map(SymptomViewModel::getSymptom)
        .findFirst()
        .get();

    } else {
      SymptomViewModel symptomViewModel = StreamSupport.stream(symptomViewModels)
        .filter(symptom -> symptom.getId() == parentSymptomId)
        .findFirst()
        .get();

      subSymptomViewModel = StreamSupport.stream(symptomViewModel.getChildren())
        .filter(symptom -> symptom.getId() == symptomId)
        .findFirst()
        .get();
    }

    this.subSymptomViewModel = subSymptomViewModel;
    if (subSymptomViewModel != null) {
      setupSymptom();
    }
    hideLoading();
  }

  private void onListFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

}
