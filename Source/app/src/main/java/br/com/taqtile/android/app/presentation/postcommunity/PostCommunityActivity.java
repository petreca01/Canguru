package br.com.taqtile.android.app.presentation.postcommunity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.CREATE_POST_REQUEST_CODE;
import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by taqtile on 11/04/17.
 */

public class PostCommunityActivity extends TemplateBackActivity {

  private PostTopicCommunityFragment postTopicCommunityFragment;
  private PostMessageCommunityFragment postMessageCommunityFragment;
  private PostCommunityPresenter presenter;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public Fragment getFragment() {
    PostTopicCommunityFragment postTopicCommunityFragment = (PostTopicCommunityFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (postTopicCommunityFragment == null) {
      postTopicCommunityFragment = PostTopicCommunityFragment.newInstance();
    }

    this.postTopicCommunityFragment = postTopicCommunityFragment;
    return postTopicCommunityFragment;
  }

  @Override
  public BasePresenter getPresenter() {
    postTopicCommunityFragment.setNavigationManager(new NavigationHelper(this));
    postTopicCommunityFragment.setPresenter(getCommunityPresenter());
    return presenter;
  }

  protected void navigateToMessageFragment(String topic) {
    postMessageCommunityFragment = PostMessageCommunityFragment.newInstance();
    postMessageCommunityFragment.setTopic(topic);
    postMessageCommunityFragment.setNavigationManager(new NavigationHelper(this));
    postMessageCommunityFragment.setPresenter(getCommunityPresenter());

    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.replace(getFragmentContainerId(), postMessageCommunityFragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }

  private PostCommunityPresenter getCommunityPresenter() {
    if (presenter == null) {
      presenter = new PostCommunityPresenter();
    }
    return presenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {

    Intent intent = new Intent(context, PostCommunityActivity.class);
    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener, CREATE_POST_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent, CREATE_POST_REQUEST_CODE);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
