package br.com.taqtile.android.app.presentation.diary.model;

import java.util.List;
import java.util.TreeMap;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;

/**
 * Created by taqtile on 5/10/17.
 */

public class DiaryViewModel {

  private boolean showDiaryTutorial;

  private TreeMap<Integer, List<AgendaItemViewModel>> weeksAppointments;

  public DiaryViewModel(boolean showDiaryTutorial,
                        TreeMap<Integer, List<AgendaItemViewModel>> weeksAppointments) {
    this.showDiaryTutorial = showDiaryTutorial;
    this.weeksAppointments = weeksAppointments;
  }

  public boolean showDiaryTutorial() {
    return showDiaryTutorial;
  }

  public TreeMap<Integer, List<AgendaItemViewModel>> getWeeksAppointments() {
    return weeksAppointments;
  }
}
