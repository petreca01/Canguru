package br.com.taqtile.android.app.domain.account.models;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 30/03/17.
 */

public class PasswordRecoveryParams extends BaseRemoteRequest {
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
