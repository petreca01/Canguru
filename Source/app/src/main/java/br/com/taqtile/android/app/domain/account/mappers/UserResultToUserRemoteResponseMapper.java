package br.com.taqtile.android.app.domain.account.mappers;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by taqtile on 6/12/17.
 */

public class UserResultToUserRemoteResponseMapper {

  public static UserRemoteResponse perform(UserResult userResult) {
    return new UserRemoteResponse(userResult.getZipCode(), userResult.getCpf(),
      userResult.getCellphoneNumber());
  }
}
