package br.com.taqtile.android.app.domain.professional;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.professional.ProfessionalRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalAuthorizationParams;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class RevokeProfessionalUseCase extends BaseUseCase<EmptyResult,
  ProfessionalAuthorizationParams, ProfessionalRepository> {

  public RevokeProfessionalUseCase(ProfessionalRepository professionalRepository) {
    super(professionalRepository);
  }

  @Override
  public Observable<EmptyResult> execute(
    ProfessionalAuthorizationParams professionalAuthorizationParams) {
    return getRepository().revokeAccess(professionalAuthorizationParams);
  }
}
