package br.com.taqtile.android.app.presentation.personalrisk;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.listings.cells.TextCell;
import br.com.taqtile.android.app.misc.dialog.EmailFormInDialog;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Renato on 4/17/17.
 */

public class PersonalRiskFragment extends AppBaseFragment implements TextCell.Listener {

  private PersonalRiskContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;
  private String userEmail;

  @BindView(R.id.fragment_personal_risk_container)
  LinearLayout personalRiskContainer;

  @BindView(R.id.fragment_personal_risk_about_you_button)
  TextCell aboutYouButton;

  @BindView(R.id.fragment_personal_risk_obstetric_history_button)
  TextCell obstetricHistoryButton;

  @BindView(R.id.fragment_personal_risk_family_button)
  TextCell familyButton;

  @BindView(R.id.fragment_personal_risk_health_button)
  TextCell healthButton;

  @BindView(R.id.fragment_personal_risk_send_questionnaire_button)
  TemplateButton sendPersonalRisk;

  @BindView(R.id.fragment_personal_risk_loading)
  ProgressBarLoading loading;

  public static PersonalRiskFragment newInstance() {
    return new PersonalRiskFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_personal_risk, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupListeners();
    setupSendQuestionnaireButton();
    fetchRiskAnalysis();

    setUserEmail();
  }

  private void setupView() {
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_personal_risk_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    setupView();
  }

  private void setUserEmail() {
    Subscription subscription = Toolbox.getInstance()
      .getUserEmail()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        (email) -> { this.userEmail = email; }
      );
    compositeSubscription.add(subscription);
  }

  private void setupListeners() {
    aboutYouButton.setListener(this);
    obstetricHistoryButton.setListener(this);
    familyButton.setListener(this);
    healthButton.setListener(this);
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void showLoading() {
    super.showLoading();
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    loading.setVisibility(View.GONE);
  }

  private void fetchRiskAnalysis() {
    showLoading();
    Subscription fetchRiskAnalysisSubscription = presenter.fetchRiskAnalysisQuestionAnswersList()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onRiskAnalysisSuccess, this::onRiskAnalysisFailure);
    compositeSubscription.add(fetchRiskAnalysisSubscription);
  }

  private void onRiskAnalysisSuccess(List<RiskAnalysisQuestionGroupResult> riskAnalysisQuestionGroupResults) {
    aboutYouButton.setLeftText(riskAnalysisQuestionGroupResults.get(0).getName());
    obstetricHistoryButton.setLeftText(riskAnalysisQuestionGroupResults.get(1).getName());
    familyButton.setLeftText(riskAnalysisQuestionGroupResults.get(2).getName());
    healthButton.setLeftText(riskAnalysisQuestionGroupResults.get(3).getName());
    hideLoading();
  }

  private void onRiskAnalysisFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupSendQuestionnaireButton() {
    sendPersonalRisk.setOnClickListener(v -> this.showSendDialog(this.userEmail));
  }

  private void showSendDialog(@Nullable String defaultValue) {
    final EmailFormInDialog input = getEditTextForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_risk_analysis_send_questionnaire_text))
      .setView(input)
      .setPositiveButton(getString(R.string.send), (dialog, which) -> {
          this.sendQuestionnaire(input.getFormText());
          showSnackBar(getString(R.string.fragment_risk_analysis_questionnaire_send_success_text));
        }
        ).setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
      // do nothing
    })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));

  }

  private EmailFormInDialog getEditTextForDialog() {
    return new EmailFormInDialog(getContext());
  }

  private void sendQuestionnaire(String email) {
    if (email != null && !email.isEmpty()) {
      Subscription sendRiskAnalysisQuestionAnswersListSubscription = presenter
        .sendRiskAnalysisQuestionAnswersList(email)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onSendQuestionnaireSuccess, this::onSendQuestionnaireFailure);
      compositeSubscription.add(sendRiskAnalysisQuestionAnswersListSubscription);
    }
  }

  private void onSendQuestionnaireSuccess(List<RiskAnalysisQuestionGroupResult> riskAnalysisQuestionGroupResults) {
  }

  private void onSendQuestionnaireFailure(Throwable throwable) {
    showSnackBar(getString(R.string.error_generic_message));
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_personal_risk_placeholder;
  }

  public void setPresenter(PersonalRiskContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void onCellClick(View view, CharSequence text) {
    navigationManager.showPersonalRiskForm(text != null ? String.valueOf(text) : null);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchRiskAnalysis();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_personal_risk),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
