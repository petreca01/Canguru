package br.com.taqtile.android.app.presentation.imagecapture;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.ACTION_ID;
import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by taqtile on 4/28/17.
 */

public class ImageCaptureActivity extends TemplateBackActivity {

  private ImageCaptureFragment fragment;

  public static int REQUEST_PROFILE_IMAGE = 101;
  public static int REQUEST_COVER_IMAGE = 102;
  public static final String IMAGE_PATH = "imagePath";

  @Override
  public Fragment getFragment() {
    fragment = (ImageCaptureFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {

      fragment = ImageCaptureFragment.newInstance(imagePath -> {
        Intent data = new Intent();
        data.putExtra(IMAGE_PATH, imagePath);
        if (getParent() == null) {
          setResult(Activity.RESULT_OK, data);
        } else {
          getParent().setResult(Activity.RESULT_OK, data);
        }
        finishActivity(REQUEST_PROFILE_IMAGE);
      });
    }
    fragment.setActionId(getIntent().getIntExtra(ACTION_ID, 0));
    fragment.setRequestCode(getIntent().getIntExtra(REQUEST_CODE, 0));
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    return new BasePresenter() {
      @Override
      public void start() {
      }

      @Override
      public void resume() {
      }
    };
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    fragment.onActivityResult(requestCode, resultCode, data);
  }

}

