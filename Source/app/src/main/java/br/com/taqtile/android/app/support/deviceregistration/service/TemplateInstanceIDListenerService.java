package br.com.taqtile.android.app.support.deviceregistration.service;


import br.com.taqtile.android.cleanbase.push.deviceregister.BaseInstanceIDListenerService;

/**
 * Created by taqtile on 7/11/16.
 */

public class TemplateInstanceIDListenerService extends BaseInstanceIDListenerService {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public Class<?> getDeviceRegistrationIntentService() {
        return DeviceRegistrationIntentService.class;
    }
}
