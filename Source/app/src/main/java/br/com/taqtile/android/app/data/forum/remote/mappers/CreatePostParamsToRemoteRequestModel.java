package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.forum.remote.models.request.CreatePostRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.CreatePostParams;

/**
 * Created by taqtile on 4/17/17.
 */

public class CreatePostParamsToRemoteRequestModel {

  public static CreatePostRemoteRequest perform(CreatePostParams createPostParams) {
    return new CreatePostRemoteRequest(createPostParams.getTitle(),
      createPostParams.getContent());
  }
}
