package br.com.taqtile.android.app.data.profile.local.models;

import java.io.File;

/**
 * Created by taqtile on 17/04/17.
 */

public class UserImagesResponse {
  private File coverPhoto;
  private File profilePicture;

  public UserImagesResponse(File coverPhoto, File profilePicture) {
    this.coverPhoto = coverPhoto;
    this.profilePicture = profilePicture;
  }

  public File getCoverPhoto() {
    return coverPhoto;
  }

  public File getProfilePicture() {
    return profilePicture;
  }


}
