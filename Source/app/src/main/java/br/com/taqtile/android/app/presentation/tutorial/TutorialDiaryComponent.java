package br.com.taqtile.android.app.presentation.tutorial;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.tutorial.showcase.builders.ShowCaseBuilder;
import br.com.taqtile.android.app.presentation.tutorial.showcase.paints.TransparentPaintBuilder;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.SemiCircleRectangle;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.SurroundingShape;
import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.TransparentCircle;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/29/17.
 */

public class TutorialDiaryComponent extends FrameLayout {

  public interface Listener {
    void onPageChange(int index);

    void onCloseTutorial();
  }

  @BindView(R.id.component_tutorial_diary_close_button)
  ImageView closeButton;
  @BindView(R.id.component_tutorial_diary_background)
  ImageView background;
  @BindView(R.id.component_tutorial_diary_description)
  CustomTextView description;

  private List<TutorialDiaryPageViewModel> tutorialContent;
  private int currentPage;
  private Listener listener;

  public TutorialDiaryComponent(Context context, List<TutorialDiaryPageViewModel> tutorialContent, Listener listener) {
    super(context);
    if (listener == null) {
      throw new NullPointerException("The listener cannot be null");
    }
    this.listener = listener;
    init(context);
    setupTutorialContent(tutorialContent);
  }

  private void setupTutorialContent(List<TutorialDiaryPageViewModel> tutorialContent) {
    currentPage = 0;
    this.tutorialContent = tutorialContent;
    setTutorialPage(currentPage);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_tutorial_diary, this, true);
    ButterKnife.bind(this, v);
    setupListeners();
  }

  private void setupListeners() {
    background.setOnClickListener((view) -> {
      if (currentPage + 1 < tutorialContent.size()) {
        this.setTutorialPage(currentPage + 1);
      } else {
        if (listener != null) {
          listener.onCloseTutorial();
        }
      }
    });
    closeButton.setOnClickListener((view) -> {
      if (listener != null) {
        listener.onCloseTutorial();
      }
    });

  }

  private void setTutorialPage(int page) {
    if (page < tutorialContent.size()) {

      currentPage = page;
      TutorialDiaryPageViewModel tutorialPage = tutorialContent.get(page);
      description.setText(tutorialPage.text);

      if (tutorialPage.target != null) {
        if (tutorialPage.type == TutorialDiaryPageViewModel.CIRCLE) {
          setBackgroundShowCaseWithCircle(tutorialPage.target);
        } else if (tutorialPage.type == TutorialDiaryPageViewModel.SEMI_CIRCLE_RETANGLE) {
          setBackgroundShowCaseWithSemiCircleRectangle(tutorialPage.target);
        }
      }

      if (tutorialPage.notifyWhenSelected) {
        listener.onPageChange(page);
      }

    } else {
      throw new IndexOutOfBoundsException("You are trying to access a tutorial page with index bigger then the tutorialContent array");
    }
  }

  private void setBackgroundShowCaseWithCircle(View target) {
    TransparentCircle transparentCircle = new TransparentCircle(
      new TransparentCircle.DrawingParams(
        getResources().getDimensionPixelSize(R.dimen.margin_x_large),
        TransparentPaintBuilder.build()));
    setBackgroundShowCase(target, transparentCircle);
  }

  private void setBackgroundShowCaseWithSemiCircleRectangle(View target) {
    int padding = getResources().getDimensionPixelSize(R.dimen.margin_small);
    SemiCircleRectangle semiCircleRectangle = new SemiCircleRectangle(
      new SemiCircleRectangle.DrawingParams(target.getWidth() + 2 * padding, target.getHeight() + 2 * padding)
    );
    setBackgroundShowCase(target, semiCircleRectangle);
  }

  private void setBackgroundShowCase(View target, SurroundingShape surroundingShape) {
    background.setImageBitmap(
      ShowCaseBuilder.build(
        getContext(),
        R.color.color_fab_overlay,
        target,
        surroundingShape));
  }

}
