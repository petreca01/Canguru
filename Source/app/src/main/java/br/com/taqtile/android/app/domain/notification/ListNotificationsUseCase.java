package br.com.taqtile.android.app.domain.notification;

import br.com.taqtile.android.app.data.notification.NotificationRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.notification.models.NotificationResult;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public class ListNotificationsUseCase
  extends BaseUseCase<List<NotificationResult>, Void, NotificationRepository> {

  public ListNotificationsUseCase(NotificationRepository notificationRepository) {
    super(notificationRepository);
  }

  @Override public Observable<List<NotificationResult>> execute(Void aVoid) {
    return getRepository().list();
  }
}
