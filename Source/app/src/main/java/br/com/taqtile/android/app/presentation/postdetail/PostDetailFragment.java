package br.com.taqtile.android.app.presentation.postdetail;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.adapters.PostDetailAdapter;
import br.com.taqtile.android.app.listings.cells.WriteCommentCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedChannelViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedCommunityViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.SlideEffectAnimator;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.post.PostDetailContract;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity.FROM_PROFILE_TAG;
import static br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity.POST_ID_TAG;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_POST_ID_KEY;

/**
 * Created by indigo on 26/09/2017.
 */

public abstract class PostDetailFragment extends AppBaseFragment
  implements PostDetailAdapter.PostDetailListener {

  public static final int DELETE_POST = 0;
  public static final int LIKE_POST = 1;
  public static final int DISLIKE_POST = 2;
  public static final int COMMENT_POST = 3;
  public static final int DELETE_COMMENT_POST = 4;

  @BindView(R.id.fragment_channel_community_post_detail_recycler_view)
  RecyclerView channelDetailsRecyclerView;
  @BindView(R.id.fragment_channel_community_post_detail_fab)
  FloatingActionButton channelDetailsFab;
  @BindView(R.id.fragment_channel_community_post_detail_write_comment)
  WriteCommentCell writeComment;
  @BindView(R.id.fragment_channel_community_post_detail_root)
  FrameLayout root;
  @BindView(R.id.fragment_channel_community_post_detail_loading)
  ProgressBarLoading loading;

  Intent intent = new Intent();

  protected NavigationManager navigationManager;
  protected PostDetailContract.Presenter presenter;
  protected CustomToolbar customToolbar;

  protected PostDetailAdapter channelCommunityPostDetailAdapter;
  protected LinearLayoutManager linearLayoutManager;
  protected List<ListingsViewModel> postList = new ArrayList<>();

  protected Integer postId;
  protected boolean fromProfile;
  private Subscription likePostSubscription;

  private int keypadHeight;
  private int screenHeight;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_channel_community_post_detail, container, false);
    ButterKnife.bind(this, root);

    this.postId = getActivity().getIntent().getIntExtra(POST_ID_TAG, 0);
    this.fromProfile = getActivity().getIntent().getBooleanExtra(FROM_PROFILE_TAG, false);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupRecyclerView();
    setupListeners();
    fetchPostDetails();
    setupKeyboardShowingListener();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    hideLoading();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
    }
  }

  private void setupView() {
    writeComment.setVisibility(View.GONE);
  }

  private void setupRecyclerView() {
    linearLayoutManager = new LinearLayoutManager(getContext());
    channelDetailsRecyclerView.setLayoutManager(linearLayoutManager);
    channelCommunityPostDetailAdapter = new PostDetailAdapter(getContext(), postList);
    channelCommunityPostDetailAdapter.setListener(this);
    channelDetailsRecyclerView.setAdapter(channelCommunityPostDetailAdapter);
  }

  private void setupListeners() {
    channelDetailsFab.setOnClickListener(view -> onFabClicked());

    writeComment.setListener(message -> {
      Integer postId = (isPostListFirstItemASocialFeedObject() && isChannel()) ?
        ((SocialFeedChannelViewModel) postList.get(0)).getPostId() :
        ((SocialFeedCommunityViewModel) postList.get(0)).getPostId();

      onPostNewComment(postId, message);
    });

    channelDetailsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy < 0 && shouldShowFab()) {
          showFabAndHideWriteCommentField();
        } else if (dy > 0 && shouldHideFab()) {
          hideFabAndShowWriteCommentField();
        }
      }
    });
  }

  private boolean isChannel() {
    return postList.get(0) instanceof SocialFeedChannelViewModel;
  }

  private boolean shouldShowFab() {
    return writeComment.isEditTextEmpty() && channelDetailsFab.getVisibility() != View.VISIBLE && !isKeyboardShowing();
  }

  private boolean shouldHideFab() {
    boolean isFabVisible = channelDetailsFab.getVisibility() == View.VISIBLE;
    boolean isChannelPostListNotEmpty = !postList.isEmpty();
    boolean hasReachedLastItem = linearLayoutManager.findLastCompletelyVisibleItemPosition() == postList.size() - 1;

    return isFabVisible && isChannelPostListNotEmpty && hasReachedLastItem;
  }

  private void onFabClicked() {
    SlideEffectAnimator.slideView(channelDetailsFab);
    SlideEffectAnimator.slideView(writeComment);
    writeComment.requestEditTextFocus();
  }

  private void hideFabAndShowWriteCommentField() {
    SlideEffectAnimator.slideView(channelDetailsFab);
    SlideEffectAnimator.slideView(writeComment);
  }

  private void showFabAndHideWriteCommentField() {
    SoftKeyboardHelper.hideKeyboard(getContext());
    SlideEffectAnimator.slideView(channelDetailsFab);
    new Handler().postDelayed(() -> {
      SlideEffectAnimator.slideView(writeComment);
    }, 100);
  }

  private void setupKeyboardShowingListener() {
    root.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

      Rect r = new Rect();
      root.getWindowVisibleDisplayFrame(r);
      screenHeight = root.getRootView().getHeight();

      // r.bottom is the position above soft keypad or device button.
      // if keypad is shown, the r.bottom is smaller than that before.
      keypadHeight = screenHeight - r.bottom;

    });
  }

  private boolean isKeyboardShowing() {
    return keypadHeight > screenHeight * 0.15;
  }

  protected void fetchPostDetails() {
    showLoading();
    SoftKeyboardHelper.hideKeyboard(getContext());
    Subscription fetchPostSubscription = Toolbox.getInstance().getUserId()
      .flatMap(userId -> {
        channelCommunityPostDetailAdapter.setUserId(userId);
        return presenter.fetchPostModels(postId);
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchPostDetailsSuccess,
        this::onFetchPostDetailsFailure);
    compositeSubscription.add(fetchPostSubscription);
  }
  protected void onFetchPostDetailsSuccess(List<ListingsViewModel> listingsViewModels) {
    refreshPostDetail(listingsViewModels);
    setTitle();
    hideLoading();
  }
  protected void onFetchPostDetailsFailure(Throwable throwable) {
    hideFAB();
    hideLoading();
    // show placeholder
    super.handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  @Override
  public void onItemLiked(Integer itemId, boolean alreadyLiked) {
    if (likePostSubscription != null) {
      likePostSubscription.unsubscribe();
    }
    // check if the like is for post or comment
    Integer replayId = null;
    if(!itemId.equals(postId)){
      replayId = itemId;
    }
    likePostSubscription = presenter.likeItem(this.postId, replayId, alreadyLiked)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(success ->
          this.onItemLikedSuccess(success, itemId, alreadyLiked),
        this::onItemLikedFailure
      );
    compositeSubscription.add(likePostSubscription);
  }
  protected void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels, Integer itemId, boolean alreadyLiked) {
    this.refreshPostDetail(listingsViewModels);

    // ih the like it was in post (and not in the commnet).
    if (itemId.equals(this.postId)) {
      this.intent.putExtra(POST_DETAIL_ACTION_POST_ID_KEY, this.postId);
      this.intent.putExtra(POST_DETAIL_ACTION_KEY, alreadyLiked ? DISLIKE_POST : LIKE_POST);
      getActivity().setResult(Activity.RESULT_OK, this.intent);
    }
  }
  protected void onItemLikedFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
  }

  @Override
  public void onPostNewComment(Integer postId, String message) {
    SoftKeyboardHelper.hideKeyboard(getContext());
    writeComment.setSendEnable(false);
    Subscription commentPostSubscription = presenter.postComment(postId, message)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onPostNewCommentSuccess,
        this::onPostNewCommentFailure,
        this::hideLoading);
    compositeSubscription.add(commentPostSubscription);
  }
  protected void onPostNewCommentSuccess(List<ListingsViewModel> listingsViewModels) {
    this.refreshPostDetail(listingsViewModels);
    writeComment.resetCell();
    writeComment.setSendEnable(true);
    intent.putExtra(POST_DETAIL_ACTION_POST_ID_KEY, this.postId);
    intent.putExtra(POST_DETAIL_ACTION_KEY, COMMENT_POST);
    getActivity().setResult(Activity.RESULT_OK, this.intent);
  }
  protected void onPostNewCommentFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
    writeComment.setSendEnable(true);
  }

  @Override
  public abstract void onDeletePost(Integer postId);
  protected void onDeletePostSuccess(String message) {
    this.intent.putExtra(POST_DETAIL_ACTION_POST_ID_KEY, this.postId);
    this.intent.putExtra(POST_DETAIL_ACTION_KEY, DELETE_POST);
    getActivity().setResult(Activity.RESULT_OK, this.intent);
    getActivity().finish();
  }
  protected void onDeletePostFailure(Throwable throwable){
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
  }

  @Override
  public abstract void onReportPost(Integer postId);
  protected abstract void onReportPostSuccess(EmptyResult emptyResult);
  protected void onReportPostFailure(Throwable throwable){
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);;
  }

  @Override
  public abstract void onDeleteComment(Integer commentId);
  protected void onDeleteCommentSuccess(){
    this.intent.putExtra(POST_DETAIL_ACTION_POST_ID_KEY, this.postId);
    this.intent.putExtra(POST_DETAIL_ACTION_KEY, DELETE_COMMENT_POST);
    getActivity().setResult(Activity.RESULT_OK, this.intent);

    fetchPostDetails();
  }
  protected void onDeleteCommentFailure(Throwable throwable){
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
  }

  protected void setupAlertDialog(AlertDialog alertDialog) {
    Button button = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));
  }

  protected void refreshPostDetail(List<ListingsViewModel> listingsViewModels){
    postList.clear();
    postList.addAll(listingsViewModels);
    channelCommunityPostDetailAdapter.notifyDataSetChanged();
  }

  private void setTitle() {
    if (isPostListFirstItemASocialFeedObject()) {
      customToolbar.setTitle(((SocialFeedViewModel) postList.get(0)).getPostTitle(), Gravity.LEFT);
    }
  }

  private boolean isPostListFirstItemASocialFeedObject() {
    return !postList.isEmpty() && postList.get(0) instanceof SocialFeedViewModel;
  }

  protected void setPresenter(PostDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_channel__community_post_detail_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchPostDetails();
    hidePlaceholder();
    showFAB();
  }

  @Override
  public void showLoading() {
    super.showLoading();
    channelDetailsRecyclerView.setVisibility(View.GONE);
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    channelDetailsRecyclerView.setVisibility(View.VISIBLE);
    loading.setVisibility(View.GONE);
  }

  private void hideFAB() {
    channelDetailsFab.setClickable(false);
    channelDetailsFab.setVisibility(View.GONE);
  }

  private void showFAB() {
    channelDetailsFab.setClickable(true);
    channelDetailsFab.setVisibility(View.VISIBLE);
  }
}


