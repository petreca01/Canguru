package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.forum.remote.models.request.WarnPostRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.WarnPostParams;

/**
 * Created by taqtile on 12/04/17.
 */

public class WarnPostParamsToWarnPostRemoteRequestMapper {

  public static WarnPostRemoteRequest perform(WarnPostParams warnPostParams) {
    return new WarnPostRemoteRequest(warnPostParams.getPostId(), warnPostParams.getContent());
  }
}
