package br.com.taqtile.android.app.presentation.medicalrecommendations;

import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 28/04/17.
 */

public interface MedicalRecommendationsContract {
  interface Presenter extends BasePresenter {
    Observable<List<MedicalRecommendationViewModel>> fetchRecommendations();
  }
}
