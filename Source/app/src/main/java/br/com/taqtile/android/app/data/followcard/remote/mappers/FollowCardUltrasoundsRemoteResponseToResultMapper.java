package br.com.taqtile.android.app.data.followcard.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardUltrasoundsRemoteResponse;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardUltrasoundsResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardUltrasoundsRemoteResponseToResultMapper {

  public static List<FollowCardUltrasoundsResult> perform(
    List<FollowCardUltrasoundsRemoteResponse> followCardUltrasoundsRemoteResponse) {
    return StreamSupport.stream(followCardUltrasoundsRemoteResponse)
      .map(FollowCardUltrasoundsRemoteResponseToResultMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardUltrasoundsResult map(
    FollowCardUltrasoundsRemoteResponse followCardUltrasoundsRemoteResponse) {
    return new FollowCardUltrasoundsResult(
      followCardUltrasoundsRemoteResponse.getId(),
      followCardUltrasoundsRemoteResponse.getConsultationId(),
      followCardUltrasoundsRemoteResponse.getDate(),
      followCardUltrasoundsRemoteResponse.getPeriodPregnancyWeeks(),
      followCardUltrasoundsRemoteResponse.getPeriodPregnancyDays(),
      followCardUltrasoundsRemoteResponse.getUltrasoundPregnancyWeeks(),
      followCardUltrasoundsRemoteResponse.getUltrasoundPregnancyDays(),
      followCardUltrasoundsRemoteResponse.getFetalWeight(),
      followCardUltrasoundsRemoteResponse.getPlacenta(),
      followCardUltrasoundsRemoteResponse.getLiquid(),
      followCardUltrasoundsRemoteResponse.getPbf(),
      followCardUltrasoundsRemoteResponse.getCreatedAt(),
      followCardUltrasoundsRemoteResponse.getDoctorId(),
      followCardUltrasoundsRemoteResponse.getDoctorName());
  }
}
