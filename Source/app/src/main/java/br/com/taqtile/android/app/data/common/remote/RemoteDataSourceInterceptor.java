package br.com.taqtile.android.app.data.common.remote;

import java.util.concurrent.TimeUnit;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.RemoteError;
import rx.Observable;

/**
 * Created by taqtile on 5/5/17.
 */

public class RemoteDataSourceInterceptor {

  private static Boolean makeRequestWait = false;
  private static Boolean makeResquestReturnError = false;
  private static int delaySeconds = 5;

  public static <T> Observable<BaseRemoteResponse<T>> intercept(Observable<BaseRemoteResponse<T>> observableIntercepted) {
    return interceptWithError(interceptWithDelay(observableIntercepted));

  }

  private static <T> Observable<BaseRemoteResponse<T>> interceptWithDelay(Observable<BaseRemoteResponse<T>> observableIntercepted) {
    if (makeRequestWait) {
      return observableIntercepted.delay(delaySeconds, TimeUnit.SECONDS);
    } else {
      return observableIntercepted;
    }
  }

  private static <T> Observable<BaseRemoteResponse<T>> interceptWithError(Observable<BaseRemoteResponse<T>> observableIntercepted) {
    if (makeResquestReturnError) {
      RemoteError remoteError = new RemoteError();
      remoteError.setErrorType(RemoteError.UNKNOWN);
      remoteError.setMessage("Error criado pelo interceptor");
      return Observable.error(remoteError);
    } else {
      return observableIntercepted;
    }
  }

  public static void setMakeResquestReturnError(Boolean makeResquestReturnError) {
    RemoteDataSourceInterceptor.makeResquestReturnError = makeResquestReturnError;
  }

  public static void setMakeRequestWait(Boolean makeRequestWait, int seconds) {
    RemoteDataSourceInterceptor.makeRequestWait = makeRequestWait;
    delaySeconds = seconds;
  }

  public static Boolean getMakeRequestWait() {
    return makeRequestWait;
  }

  public static Boolean getMakeResquestReturnError() {
    return makeResquestReturnError;
  }

  public static int getDelaySeconds() {
    return delaySeconds;
  }
}
