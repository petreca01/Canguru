package br.com.taqtile.android.app.domain.followcard;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.followcard.mappers.FollowCardUltrasoundsResultToViewModelMapper;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardUltrasoundsResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardUltrasoundsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardUltrasoundsUseCase extends BaseUseCase<List<FollowCardUltrasoundsViewModel>, Void,
  FollowCardRepository> {

  public FollowCardUltrasoundsUseCase(FollowCardRepository followCardRepository) {
    super(followCardRepository);
  }

  @Override
  public Observable<List<FollowCardUltrasoundsViewModel>> execute(Void aVoid) {
    return getRepository().fetchFollowCardUltrasounds()
      .map(FollowCardUltrasoundsResultToViewModelMapper::perform);
  }
}
