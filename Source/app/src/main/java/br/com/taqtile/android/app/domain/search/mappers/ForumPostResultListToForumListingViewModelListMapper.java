package br.com.taqtile.android.app.domain.search.mappers;

import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.listings.viewmodels.ForumListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 28/03/17.
 */

public class ForumPostResultListToForumListingViewModelListMapper {
  public static List<ForumListingViewModel> perform(List<PostResult> posts) {
    return StreamSupport.stream(posts)
        .map(ForumPostResultListToForumListingViewModelListMapper::perform)
        .collect(Collectors.toList());
  }

  private static ForumListingViewModel perform(PostResult post) {
    return new ForumListingImplViewModel(
      String.valueOf(post.getId()),
      post.getTitle(),
      post.getLikes(),
      post.getCountReplies());
  }
}
