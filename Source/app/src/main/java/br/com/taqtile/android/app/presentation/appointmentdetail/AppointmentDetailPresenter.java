package br.com.taqtile.android.app.presentation.appointmentdetail;

import br.com.taqtile.android.app.domain.agenda.FinishAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.RemoveAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.ScheduleAppointmentUseCase;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 26/04/17.
 */

public class AppointmentDetailPresenter implements AppointmentDetailContract.Presenter {

  private FinishAgendaItemUseCase finishAgendaItemUseCase;
  private RemoveAgendaItemUseCase removeAgendaItemUseCase;
  private ScheduleAppointmentUseCase scheduleAppointmentUseCase;

  public AppointmentDetailPresenter() {
    this.finishAgendaItemUseCase = Injection.provideFinishAgendaItemUseCase();
    this.removeAgendaItemUseCase = Injection.provideRemoveAgendaItemUseCase();
    this.scheduleAppointmentUseCase = Injection.provideScheduleAppoitmentUseCase();
  }

  @Override
  public void start() {
  }

  @Override
  public void resume() {
  }

  @Override
  public Observable<AgendaItemViewModel> finishAppointment(
    FinishAppointmentParams finishAppointmentParams) {
    return finishAgendaItemUseCase.execute(finishAppointmentParams);
  }

  @Override
  public Observable<String> removeAppointment(RemoveAppointmentParams removeAppointmentParams) {
    return removeAgendaItemUseCase.execute(removeAppointmentParams);
  }

  @Override
  public Observable<AgendaItemViewModel> clearScheduleAppointment(ScheduleParams scheduleParams) {
    return scheduleAppointmentUseCase.execute(scheduleParams);
  }
}
