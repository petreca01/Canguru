package br.com.taqtile.android.app.presentation.additionalinfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoActivity extends TemplateBackActivity {

  private AdditionalInfoFragment fragment;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public AdditionalInfoFragment getFragment() {
    AdditionalInfoFragment fragment = (AdditionalInfoFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      fragment = AdditionalInfoFragment.newInstance();
    }
    this.fragment = fragment;

    return this.fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    AdditionalInfoPresenter presenter = new AdditionalInfoPresenter();
    fragment.setPresenter(presenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return presenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, AdditionalInfoActivity.class);
    context.startActivity(intent);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
