package br.com.taqtile.android.app.support;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.about.AboutCanguruActivity;
import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.additionalinfoedit.AdditionalInfoEditActivity;
import br.com.taqtile.android.app.presentation.appointmentdetail.AppointmentDetailActivity;
import br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity;
import br.com.taqtile.android.app.presentation.birthplan.BirthplanActivity;
import br.com.taqtile.android.app.presentation.birthplanquestionnaire.BirthplanQuestionnaireActivity;
import br.com.taqtile.android.app.presentation.changepassword.ChangePasswordActivity;
import br.com.taqtile.android.app.presentation.channel.ChannelActivity;
import br.com.taqtile.android.app.presentation.channeldetail.ChannelDetailsActivity;
import br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.communitypostdetail.CommunityPostDetailActivity;
import br.com.taqtile.android.app.presentation.conditiondetails.ConditionDetailsActivity;
import br.com.taqtile.android.app.presentation.condition.ConditionsActivity;
import br.com.taqtile.android.app.presentation.connectedprofessionals.ConnectedProfessionalsActivity;
import br.com.taqtile.android.app.presentation.forgotpassword.ForgotPasswordActivity;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.app.presentation.gestationbabydevelopmentdetails.GestationBabyDevelopmentDetailsActivity;
import br.com.taqtile.android.app.presentation.gestationconclusion.GestationConclusionActivity;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.GestationPregnancyInformationActivity;
import br.com.taqtile.android.app.presentation.healthcaredetails.HealthcareDetailsActivity;
import br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo.HealthcareUserAdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginActivity;
import br.com.taqtile.android.app.presentation.manageprofessionals.ManageProfessionalsActivity;
import br.com.taqtile.android.app.presentation.maternitycitysearch.MaternityCitySearchActivity;
import br.com.taqtile.android.app.presentation.maternitydetail.MaternityDetailActivity;
import br.com.taqtile.android.app.presentation.maternitynotfound.MaternityNotFoundActivity;
import br.com.taqtile.android.app.presentation.maternityrating.MaternityRatingActivity;
import br.com.taqtile.android.app.presentation.medicalrecommendationdetail.MedicalRecommendationDetailActivity;
import br.com.taqtile.android.app.presentation.medicalrecommendations.MedicalRecommendationsActivity;
import br.com.taqtile.android.app.presentation.personalrisk.PersonalRiskActivity;
import br.com.taqtile.android.app.presentation.personalriskform.PersonalRiskFormActivity;
import br.com.taqtile.android.app.presentation.postcommunity.PostCommunityActivity;
import br.com.taqtile.android.app.presentation.prenatalcard.PrenatalCardActivity;
import br.com.taqtile.android.app.presentation.probablebirthdate.ProbableBirthdateActivity;
import br.com.taqtile.android.app.presentation.profilemenu.ProfileMenuActivity;
import br.com.taqtile.android.app.presentation.profilepublic.PublicProfileActivity;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.presentation.search.SearchActivity;
import br.com.taqtile.android.app.presentation.searchhealthcare.SearchHealthcareActivity;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.presentation.signup.SignUpActivity;
import br.com.taqtile.android.app.presentation.staticscreen.StaticScreenActivity;
import br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailActivity;
import br.com.taqtile.android.app.presentation.symptomedit.SymptomEditActivity;
import br.com.taqtile.android.app.presentation.symptomsguide.SymptomsGuideActivity;
import br.com.taqtile.android.app.presentation.symptomshistory.SymptomsHistoryActivity;
import br.com.taqtile.android.cleanbase.presentation.activity.BaseActivityFragment;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 7/5/16.
 */

public class NavigationHelper implements NavigationManager {

  private static final String TAG = "NavigationHelper";
  private Activity activityContext;

  public NavigationHelper(@NonNull Activity activityContext) {
    this.activityContext = checkNotNull(activityContext, "ActivityContext cannot be null!");
  }

  @Override
  public void showSignInUI(CustomNavigationResultListener listener) {
    SignInActivity.navigate(activityContext, listener);
  }

  @Override
  public void showSignUpUI(CustomNavigationResultListener listener) {
    SignUpActivity.navigate(activityContext, listener);
  }

  @Override
  public void showSignUpUI(CustomNavigationResultListener listener, String email, String password) {
    SignUpActivity.navigate(activityContext, listener, email, password);
  }

  @Override
  public void showForgotPasswordUI(CustomNavigationResultListener listener, String email) {
    ForgotPasswordActivity.navigate(activityContext, listener, email);
  }

  @Override
  public void showInitialLoginUI() {
    InitialLoginActivity.navigate(activityContext);
  }

  @Override
  public void showChangePasswordUI() {
    ChangePasswordActivity.navigate(activityContext);
  }

  @Override
  public void showPublicProfileUI(Integer id, CustomNavigationResultListener listener) {
    PublicProfileActivity.navigate(activityContext, id, listener);
  }

  @Override
  public void showSearchUI() {
    SearchActivity.navigate(activityContext);
  }

  @Override
  public void showCommunityPostDetailUI(CustomNavigationResultListener listener, Integer id,
                                        Boolean fromProfile) {
    CommunityPostDetailActivity.navigate(activityContext, listener, id, fromProfile);
  }

  @Override
  public void showChannelListUI() {
    ChannelActivity.navigate(activityContext);
  }

  @Override
  public void showChannelDetailUI(int id, CustomNavigationResultListener listener) {
    ChannelDetailsActivity.navigate(activityContext, listener, id);
  }

  @Override
  public void showSearchHealthOperatorsUI(CustomNavigationResultListener listener) {
    activityContext.finish();
    SearchHealthcareActivity.navigate(activityContext, listener);
  }

  @Override
  public void showChannelPostDetailUI(CustomNavigationResultListener listener, Integer id, Boolean fromChannelDetail) {
    ChannelPostDetailActivity.navigate(activityContext, listener, id, fromChannelDetail);
  }

  @Override
  public void showProfileMenu() {
    ProfileMenuActivity.navigate(activityContext);
  }

  public void showAboutCanguruUI() {
    AboutCanguruActivity.navigate(activityContext);
  }

  @Override
  public void showPrivacyPolicyUI() {
    StaticScreenActivity.navigate(activityContext, StaticScreenActivity.PRIVACY_POLICY);
  }

  @Override
  public void showUseTermsUI() {
    StaticScreenActivity.navigate(activityContext, StaticScreenActivity.USE_TERMS);
  }

  @Override
  public void showAdditionalInfo() {
    AdditionalInfoActivity.navigate(activityContext);
  }

  @Override
  public void showConnectedProfessionals() {
    ConnectedProfessionalsActivity.navigate(activityContext);
  }

  @Override
  public void showPostCommunity(CustomNavigationResultListener listener) {
    PostCommunityActivity.navigate(activityContext, listener);
  }

  @Override
  public void showPersonalRisk() {
    PersonalRiskActivity.navigate(activityContext);
  }

  @Override
  public void showPersonalRiskForm(String riskGroup) {
    PersonalRiskFormActivity.navigate(activityContext, riskGroup);
  }

  @Override
  public void showHealthOperatorsDetailUI(HealthOperatorViewModel healthOperatorViewModel,
                                          boolean toAdd, CustomNavigationResultListener listener) {
    HealthcareDetailsActivity.navigate(activityContext, healthOperatorViewModel, toAdd, listener);
  }

  public void showHealthOperatorsAdditionalInfoUI(HealthOperatorViewModel healthOperatorViewModel, CustomNavigationResultListener listener) {
    HealthcareUserAdditionalInfoActivity.navigate(activityContext, healthOperatorViewModel, listener);
  }

  @Override
  public void showEditAdditionalInfoUserData(AdditionalInfoUserViewModel additionalInfoUserData,
                                             CustomNavigationResultListener listener) {
    AdditionalInfoEditActivity.navigate(activityContext, additionalInfoUserData, listener);
  }

  @Override
  public void showSymptomGuideUI() {
    SymptomsGuideActivity.navigate(activityContext);
  }

  @Override
  public void showSymptomDetailUI(CustomNavigationResultListener listener, Bundle bundle) {
    SymptomDetailActivity.navigate(activityContext, bundle, listener);
  }

  @Override
  public void showSymptomDetailUI(int parentSymptomId, int symptomId) {
    SymptomDetailActivity.navigate(activityContext, parentSymptomId, symptomId);
  }

  @Override
  public void showSymptomEditUI(CustomNavigationResultListener listener) {
    SymptomEditActivity.navigate(activityContext, listener);
  }

  @Override
  public void showSymptomHistoryUI() {
    SymptomsHistoryActivity.navigate(activityContext);
  }

  @Override
  public void showAppointmentDetailUI(CustomNavigationResultListener listener,
                                      AgendaItemViewModel agendaItemViewModel) {
    AppointmentDetailActivity.navigate(listener, activityContext, agendaItemViewModel);
  }

  @Override
  public void showAppointmentEditScheduleUI(CustomNavigationResultListener listener,
                                            AgendaItemViewModel agendaItemViewModel) {
    AppointmentEditScheduleActivity.navigate(activityContext, listener, agendaItemViewModel);
  }

  @Override
  public void showAppointmentCreateUI(CustomNavigationResultListener listener) {
    AppointmentEditScheduleActivity.navigate(activityContext, listener);
  }

  @Override
  public void showClinicalConditionsUI() {
    ConditionsActivity.navigate(activityContext);
  }

  @Override
  public void showManagedProfessionalsUI(Bundle bundle) {
    ManageProfessionalsActivity.navigate(activityContext, bundle);
  }

  @Override
  public void showMedicalRecommendations() {
    MedicalRecommendationsActivity.navigate(activityContext);
  }

  @Override
  public void showMedicalRecommendationDetail(Bundle bundle) {
    MedicalRecommendationDetailActivity.navigate(activityContext, bundle);
  }

  @Override
  public void showBirthPlanUI(CustomNavigationResultListener listener) {
    BirthplanActivity.navigate(activityContext, listener);
  }

  @Override
  public void showBirthPlanQuestionnaireUI(List<QuestionViewModel> questionnaire, String index) {
    BirthplanQuestionnaireActivity.navigate(activityContext, questionnaire, index);
  }

  @Override
  public void showMaternityCitySearch(CustomNavigationResultListener listener) {
    MaternityCitySearchActivity.navigate(activityContext, listener);
  }

  @Override
  public void showPrenatalCard() {
    PrenatalCardActivity.navigate(activityContext);
  }

  @Override
  public void showMaternityDetail(int id) {
    MaternityDetailActivity.navigate(activityContext, id);
  }

  @Override
  public void showMaternityRating(MaternityViewModel maternityViewModel, CustomNavigationResultListener listener) {
    MaternityRatingActivity.navigate(activityContext, maternityViewModel, listener);
  }

  @Override
  public void showGestationEditUI(UserViewModel userData, CustomNavigationResultListener listener) {
    GestationPregnancyInformationActivity.navigate(activityContext, userData, listener);
  }

  @Override
  public void showWeeklyContentDetailsUI(UserViewModel userData,
                                         WeeklyContentViewModel currentWeeklyContent) {
    GestationBabyDevelopmentDetailsActivity.navigate(activityContext, userData, currentWeeklyContent);
  }

  @Override
  public void showClinicalConditionsDetailsUI(ClinicalConditionItemViewModel clinicalConditionItemViewModel,
                                              CustomNavigationResultListener listener) {
    ConditionDetailsActivity.navigate(activityContext, clinicalConditionItemViewModel, listener);
  }

  @Override
  public void showGestationConclusionUI(CustomNavigationResultListener listener) {
    GestationConclusionActivity.navigate(activityContext, listener);
  }

  @Override
  public void showProbableBirthdateUI() {
    ProbableBirthdateActivity.navigate(activityContext);
  }

  @Override
  public void showMaternityNotFoundUI() {
    MaternityNotFoundActivity.navigate(activityContext);
  }

  private BaseActivityFragment getBaseActivityFragment(@NonNull Activity activityContext) {
    if (activityContext instanceof BaseActivityFragment) {
      return (BaseActivityFragment) activityContext;
    } else {
      throw new RuntimeException("Operation requires a BaseActivityFragment!");
    }
  }

}
