package br.com.taqtile.android.app.presentation.appointmenteditschedule;


import br.com.taqtile.android.app.domain.agenda.CreateAppointmentUseCase;
import br.com.taqtile.android.app.domain.agenda.ScheduleAppointmentUseCase;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 27/04/17.
 */

public class AppointmentEditSchedulePresenter implements AppointmentEditScheduleContract.Presenter {

  private ScheduleAppointmentUseCase scheduleAppointmentUseCase;
  private CreateAppointmentUseCase createAppointmentUseCase;

  public AppointmentEditSchedulePresenter() {
    this.scheduleAppointmentUseCase = Injection.provideScheduleAppoitmentUseCase();
    this.createAppointmentUseCase = Injection.provideCreateAppointmentUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<AgendaItemViewModel> scheduleAppointment(ScheduleParams scheduleParams) {
    return scheduleAppointmentUseCase.execute(scheduleParams);
  }

  @Override
  public Observable<AgendaItemViewModel> createAppointment(AppointmentParams appointmentParams) {
    return createAppointmentUseCase.execute(appointmentParams);
  }
}
