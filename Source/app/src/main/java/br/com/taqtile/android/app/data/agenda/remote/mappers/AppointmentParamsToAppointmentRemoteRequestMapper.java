package br.com.taqtile.android.app.data.agenda.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.request.AppointmentRemoteRequest;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;

/**
 * Created by taqtile on 4/7/17.
 */

public class AppointmentParamsToAppointmentRemoteRequestMapper {
  public static AppointmentRemoteRequest perform(AppointmentParams params) {
    return new AppointmentRemoteRequest(params.getTypeId(),
      params.getName(),
      params.getDate(),
      params.getObservation());
  }
}
