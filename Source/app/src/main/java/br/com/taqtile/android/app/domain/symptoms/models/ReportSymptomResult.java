package br.com.taqtile.android.app.domain.symptoms.models;

import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class ReportSymptomResult {

  private Integer id;
  private UserResult user;
  private Integer userId;
  private SubSymptomResult symptom;
  private Integer symptomId;
  private String description;
  private String date;
  private Integer gestationAgeWeeks;
  private Integer gestationAgeDays;

  public ReportSymptomResult(Integer id, UserResult user, Integer userId, SubSymptomResult symptom,
    Integer symptomId, String description, String date, Integer gestationAgeWeeks,
    Integer gestationAgeDays) {
    this.id = id;
    this.user = user;
    this.userId = userId;
    this.symptom = symptom;
    this.symptomId = symptomId;
    this.description = description;
    this.date = date;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.gestationAgeDays = gestationAgeDays;
  }

  public Integer getId() {
    return id;
  }

  public UserResult getUser() {
    return user;
  }

  public Integer getUserId() {
    return userId;
  }

  public SubSymptomResult getSymptom() {
    return symptom;
  }

  public Integer getSymptomId() {
    return symptomId;
  }

  public String getDescription() {
    return description;
  }

  public String getDate() {
    return date;
  }

  public Integer getGestationAgeWeeks() {
    return gestationAgeWeeks;
  }

  public Integer getGestationAgeDays() {
    return gestationAgeDays;
  }
}
