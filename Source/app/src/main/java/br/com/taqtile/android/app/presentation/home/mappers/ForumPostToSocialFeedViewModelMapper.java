package br.com.taqtile.android.app.presentation.home.mappers;

import br.com.taqtile.android.app.domain.common.ProfessionalValidator;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import java.util.ArrayList;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 3/24/17.
 */

public class ForumPostToSocialFeedViewModelMapper {

  public static List<SocialFeedViewModel> perform(List<PostResult> postResultList) {
    if (postResultList == null) {
      return new ArrayList<>();
    }

    return StreamSupport.stream(postResultList)
      .map(ForumPostToSocialFeedViewModelMapper::mapForumList)
      .collect(Collectors.toList());
  }

  private static SocialFeedViewModel mapForumList(PostResult postResult) {

    return new SocialFeedCommunityImplViewModel(postResult.getId(), postResult.getUserId(),
      postResult.getUserPicture() != null ? postResult.getUserPicture() : "",
      postResult.getUsername(), postResult.getCreatedAt(), postResult.getTitle(),
      postResult.getContent(), postResult.getLikes(), postResult.getCountReplies(),
      postResult.getLiked(), ProfessionalValidator.isProfessional(postResult.getUserType()));
  }
}
