package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 08/05/17.
 */

public class MedicalRecommendationDetailActivity extends TemplateBackActivity {

  private MedicalRecommendationDetailFragment fragment;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    MedicalRecommendationDetailAccessor bundleAccessor =
      new MedicalRecommendationDetailAccessor(getIntent().getExtras());

    if (fragment != null) {
      fragment.setMedicalRecommendationDetailAccessor(bundleAccessor);
    }
  }

  @Override public Fragment getFragment() {
    MedicalRecommendationDetailFragment medicalRecommendationDetailFragment =
      (MedicalRecommendationDetailFragment) getSupportFragmentManager().findFragmentById(
        getFragmentContainerId());
    if (medicalRecommendationDetailFragment == null) {
      medicalRecommendationDetailFragment = MedicalRecommendationDetailFragment.newInstance();
    }
    fragment = medicalRecommendationDetailFragment;

    return fragment;
  }

  @Override public BasePresenter getPresenter() {
    MedicalRecommendationDetailPresenter medicalRecommendationDetailPresenter =
      new MedicalRecommendationDetailPresenter();
    fragment.setPresenter(medicalRecommendationDetailPresenter);

    return medicalRecommendationDetailPresenter;
  }

  public static void navigate(Context context, Bundle bundle) {
    Intent intent = new Intent(context, MedicalRecommendationDetailActivity.class);
    intent.putExtras(bundle);
    context.startActivity(intent);
  }
}
