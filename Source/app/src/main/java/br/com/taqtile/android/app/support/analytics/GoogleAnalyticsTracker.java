package br.com.taqtile.android.app.support.analytics;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import br.com.taqtile.android.app.support.BuildConstants;
import br.com.taqtile.android.app.support.Constants;

/**
 * Created by felipesabino on 5/24/17.
 */

public class GoogleAnalyticsTracker {

  private static GoogleAnalytics analytics = null;
  private static Tracker tracker = null;

  public static void setup(Context context) {
    Constants constants = new Constants();
    if (constants.getGoogleAnalyticsId() != null) {
      analytics = GoogleAnalytics.getInstance(context);
      tracker = analytics.newTracker(constants.getGoogleAnalyticsId());
    }
  }

  public static void trackEvent(AnalyticsEvent event) {

    if (tracker == null) {
      return;
    }

    HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
      .setCategory(event.getCategory());

    if (event.getAction() != null) {
      builder = builder.setAction(event.getAction());
    }

    if (event.getLabel()!= null) {
      builder = builder.setLabel(event.getLabel());
    }

    tracker.send(builder.build());
  }

  public static void trackUser(String userId) {

    if (tracker == null) {
      return;
    }

    tracker.setClientId(userId);
  }
}
