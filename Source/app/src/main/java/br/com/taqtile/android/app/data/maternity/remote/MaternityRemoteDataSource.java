package br.com.taqtile.android.app.data.maternity.remote;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.request.CommentMaternityRemoteRequest;
import br.com.taqtile.android.app.data.maternity.remote.models.request.RateMaternityRemoteRequest;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityCommentItemRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityDetailsRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityItemRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class MaternityRemoteDataSource extends BaseRemoteDataSource {
  private Observable<MaternityServices> services;

  public MaternityRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(MaternityServices.class);
  }

  public Observable<BaseRemoteResponse<List<String>>> listCities() {
    return performRequest(services.flatMap(MaternityServices::listCities));
  }

  public Observable<BaseRemoteResponse<List<MaternityItemRemoteResponse>>> listByCity(
    String cityName) {
    return performRequest(services.flatMap(services -> services.listByCity(cityName)));
  }

  public Observable<BaseRemoteResponse<MaternityDetailsRemoteResponse>> getDetails(
    Integer maternityId) {
    return performRequest(services.flatMap(services -> services.getDetails(maternityId)));
  }

  public Observable<BaseRemoteResponse<MaternityItemRemoteResponse>> rate(
    RateMaternityRemoteRequest rateMaternityRemoteRequest, Integer maternityId) {
    return performRequest(services.flatMap(
      services -> services.rate(rateMaternityRemoteRequest.getFormData(), maternityId)));
  }

  public Observable<BaseRemoteResponse<List<MaternityCommentItemRemoteResponse>>> comment(
    CommentMaternityRemoteRequest commentMaternityRemoteRequest, Integer maternityId) {
    return performRequest(services.flatMap(
      services -> services.comment(commentMaternityRemoteRequest.getFormData(), maternityId)));
  }

  public Observable<BaseRemoteResponse<List<MaternityCommentItemRemoteResponse>>> deleteComment(
    Integer maternityId, Integer commentId) {
    return performRequest(
      services.flatMap(services -> services.deleteComment(maternityId, commentId)));
  }

  public Observable<BaseRemoteResponse<MaternityItemRemoteResponse>> followOrUnfollow(
    Integer maternityId) {
    return performRequest(services.flatMap(services -> services.followOrUnfollow(maternityId)));
  }
}

