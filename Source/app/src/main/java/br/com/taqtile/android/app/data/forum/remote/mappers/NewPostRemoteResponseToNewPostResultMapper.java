package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.NewPostRemoteResponse;
import br.com.taqtile.android.app.domain.forum.models.NewPostResult;

/**
 * Created by taqtile on 4/3/17.
 */

public class NewPostRemoteResponseToNewPostResultMapper {
  public static NewPostResult perform(BaseRemoteResponse<NewPostRemoteResponse> response) {
    NewPostRemoteResponse result = response.getResult();
    return new NewPostResult(result.getId(),
      result.getOperatorId(),
      result.getUserId(),
      result.getTitle(),
      result.getContent(),
      result.getActive(),
      result.getCreatedAt(),
      result.getUpdatedAt());
  }
}
