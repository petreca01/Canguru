package br.com.taqtile.android.app.data.symptoms.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.SubSymptomItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.SubSymptomResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class SubSymptomItemRemoteResponseListToSubSymptomResultListMapper {

  public static List<SubSymptomResult> perform(List<SubSymptomItemRemoteResponse> subSymptomItemRemoteResponseList) {
    return StreamSupport.stream(subSymptomItemRemoteResponseList)
      .map(SubSymptomItemRemoteResponseToSubSymptomResultMapper::perform)
      .collect(Collectors.toList());
  }
}
