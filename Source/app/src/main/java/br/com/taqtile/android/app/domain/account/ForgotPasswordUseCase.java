package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordParams;
import rx.Observable;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordUseCase extends BaseUseCase<ForgotPasswordResult, ForgotPasswordParams, AccountRepository> {

    public ForgotPasswordUseCase(AccountRepository accountRepository) {
        super(accountRepository);
    }

    @Override
    public Observable<ForgotPasswordResult> execute(ForgotPasswordParams forgotPasswordParams) {
        return getRepository().passwordRecovery(forgotPasswordParams);
    }
}
