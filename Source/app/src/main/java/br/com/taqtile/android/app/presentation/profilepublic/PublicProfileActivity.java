package br.com.taqtile.android.app.presentation.profilepublic;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.communitypostdetail.CommunityPostDetailActivity;
import br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo.HealthcareUserAdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.presentation.profilemenu.ProfileMenuActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.presentation.imagecapture.ImageCaptureActivity.REQUEST_COVER_IMAGE;
import static br.com.taqtile.android.app.presentation.imagecapture.ImageCaptureActivity.REQUEST_PROFILE_IMAGE;

/**
 * Created by taqtile on 20/04/17.
 */

public class PublicProfileActivity extends TemplateBackActivity {

  private PublicProfileFragment fragment;
  public static final String PUBLIC_PROFILE_ID_TAG = "profile_id";

  @Override
  public Fragment getFragment() {
    PublicProfileFragment publicProfileFragment = (PublicProfileFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (publicProfileFragment == null) {
      publicProfileFragment = PublicProfileFragment.newInstance();
    }
    fragment = publicProfileFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    PublicProfilePresenter presenter = new PublicProfilePresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(presenter);

    return presenter;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK && (requestCode == REQUEST_PROFILE_IMAGE ||
      requestCode == REQUEST_COVER_IMAGE)) {
      fragment.onActivityResult(requestCode, resultCode, data);
    }
  }

  public static void navigate(Context context, Integer id, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, PublicProfileActivity.class);
    intent.putExtra(PUBLIC_PROFILE_ID_TAG, id);

    //TODO: refactor this code below
    if (context instanceof ProfileMenuActivity) {
      ((ProfileMenuActivity) context).setResultListener(listener,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
      ((ProfileMenuActivity) context).startActivityForResult(intent,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
    } else if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
    } else {
      context.startActivity(intent);
    }
  }
}
