package br.com.taqtile.android.app.domain.gestation;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.exams.ExamsRepository;
import br.com.taqtile.android.app.domain.account.mappers.UserDataResultToViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class StartOrEditGestationUseCase
  extends BaseUseCase<UserViewModel, StartOrEditGestationParams, AccountRepository> {

  private ExamsRepository examsRepository;

  public StartOrEditGestationUseCase(AccountRepository accountRepository,
                                     ExamsRepository examsRepository) {
    super(accountRepository);
    this.examsRepository = examsRepository;
  }

  @Override
  public Observable<UserViewModel> execute(StartOrEditGestationParams startOrEditGestationParams) {

    if (shouldCreateUltrasound(startOrEditGestationParams)) {
      return examsRepository.createUltrasound(startOrEditGestationParams)
        .flatMap(examDataResults -> getRepository().editUserDataToStartGestation(startOrEditGestationParams))
        .map(UserDataResultToViewModelMapper::perform);
    } else {
      return getRepository().editUserDataToStartGestation(startOrEditGestationParams)
        .map(UserDataResultToViewModelMapper::perform);
    }
  }

  private boolean shouldCreateUltrasound(StartOrEditGestationParams startOrEditGestationParams) {
    return startOrEditGestationParams.getUltrasoundDate() != null
      && !startOrEditGestationParams.getUltrasoundDate().isEmpty();
  }
}
