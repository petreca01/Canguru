package br.com.taqtile.android.app.domain.common.models;

import android.support.annotation.StringDef;
import org.jetbrains.annotations.NotNull;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by felipesabino on 5/17/17.
 */

public class DomainError extends Throwable {

  public static final String INVALID_DATA = "invalid_data";
  public static final String UNKNOWN = "unknown";

  private static @DomainError.ErrorType
  String errorType = UNKNOWN;

  @Retention(SOURCE)
  @StringDef({
    INVALID_DATA,
    UNKNOWN
  })
  public @interface ErrorType {}

  @NotNull
  private String message;

  public void setErrorType(@DomainError.ErrorType String errorType) {
    this.errorType = errorType;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public @DomainError.ErrorType
  String getErrorType() {
    return errorType;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
