package br.com.taqtile.android.app.domain.medicalRecommendations;

import br.com.taqtile.android.app.data.medicalRecommendations.MedicalRecommendationsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.medicalRecommendations.mappers.MedicalRecommendationResultToViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class ListRecommendationsUseCase extends
  BaseUseCase<List<MedicalRecommendationViewModel>, Void, MedicalRecommendationsRepository> {

  public ListRecommendationsUseCase(
    MedicalRecommendationsRepository medicalRecommendationsRepository) {
    super(medicalRecommendationsRepository);
  }

  @Override public Observable<List<MedicalRecommendationViewModel>> execute(Void aVoid) {
    return getRepository().list()
      .map(MedicalRecommendationResultToViewModel::perform);
  }
}
