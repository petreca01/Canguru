package br.com.taqtile.android.app.presentation.symptomedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.edittexts.DateLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.GenericTextLabelEditTextCaption;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 27/04/17.
 */

public class SymptomEditFragment extends AppBaseFragment {

  @BindView(R.id.fragment_symptom_edit_scroll_view)
  ScrollView scrollView;
  @BindView(R.id.fragment_symptom_edit_content_layout)
  LinearLayout contentLayout;
  @BindView(R.id.fragment_symptom_edit_title)
  GenericTextLabelEditTextCaption symptomTitle;
  @BindView(R.id.fragment_symptom_edit_date)
  DateLabelEditTextCaption symptomDate;
  @BindView(R.id.fragment_symptom_edit_save_button)
  ProgressBarButton saveButton;

  private NavigationManager navigationManager;
  private SymptomEditContract.Presenter presenter;
  private CustomToolbar customToolbar;

  private String errorMessage;
  private Validatable[] forms;
  private List<Validatable> errorForms;

  public static SymptomEditFragment newInstance() {
    return new SymptomEditFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_symptom_edit, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupListeners();
    setupFormsList();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().onBackPressed());
      customToolbar.setTitle(getString(R.string.fragment_symptom_edit_title));
    }
  }

  private void setupView() {
    symptomTitle.setInputPlaceholder(getString(R.string.fragment_symptom_description_text));
    symptomTitle.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
  }

  private void setupListeners() {
    saveButton.setButtonListener(this::onSaveButtonClicked);
  }

  private void setupFormsList() {
    forms = new Validatable[]{symptomTitle};
    errorForms = new ArrayList<Validatable>();
  }

  private void onSaveButtonClicked() {
    registerSymptom();
  }

  private void registerSymptom() {
    if(locallyValidateForms()) {
      showButtonLoading();
      Subscription reportSymptomSubscription = presenter.reportCustomSymptom(symptomTitle.getText(), parseDate(symptomDate.getText()))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onReportSuccess, this::onReportFailure, this::hideButtonLoading);
      compositeSubscription.add(reportSymptomSubscription);
    }
  }

  private void onReportSuccess(EmptyResult emptyResult) {
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom),
      getString(R.string.analytics_event_registration),
      getString(R.string.analytics_label_user_registered_symptom)
    ));
  }

  private void onReportFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
  }

  private void hideButtonLoading() {
    saveButton.showDefaultState();
  }

  private String parseDate(String dateString) {
    DateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd");
    if (dateString == null || dateString.isEmpty()) {
      Date date = new Date();
      return serverFormat.format(date);
    }
    DateFormat appFormat = new SimpleDateFormat("dd/MM/yyyy");
    try {
      return serverFormat.format(appFormat.parse(dateString));
    } catch (ParseException e) {
      return null;
    }
  }

  private void showButtonLoading() {
    saveButton.showLoadingState();
  }

  // validation forms
  public boolean locallyValidateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      showFormValidationError(buildErrorMessage(errorForms));
      ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      isValid = false;
    }
    return isValid;
  }

  public void showFormValidationError(String text) {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, text), Snackbar.LENGTH_SHORT);
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    //TODO Properly populate errorforms array
    String[] errorForms = {getString(R.string.fragment_symptom_description_text)};
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  // endregion

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  public void setPresenter(SymptomEditContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    saveButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    saveButton.showDefaultState();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
