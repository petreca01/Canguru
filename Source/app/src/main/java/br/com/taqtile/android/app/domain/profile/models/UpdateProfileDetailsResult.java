package br.com.taqtile.android.app.domain.profile.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class UpdateProfileDetailsResult {

  private Integer id;

  private String name;

  private String babyName;

  private String about;

  private String profilePicture;

  private String coverPicture;

  private Integer pregnancyWeeks;

  private Integer pregnancyDays;

  private String city;

  private String state;

  private Boolean locationPrivacyAgreed;

  private String createdAt;

  private String updatedAt;

  public UpdateProfileDetailsResult(Integer id, String name, String babyName,
                                    String about, String profilePicture, String coverPicture,
                                    Integer pregnancyWeeks, Integer pregnancyDays,
                                    String city, String state, Boolean locationPrivacyAgreed,
                                    String createdAt, String updatedAt) {
    this.id = id;
    this.name = name;
    this.babyName = babyName;
    this.about = about;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.pregnancyWeeks = pregnancyWeeks;
    this.pregnancyDays = pregnancyDays;
    this.city = city;
    this.state = state;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getAbout() {
    return about;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public Integer getPregnancyWeeks() {
    return pregnancyWeeks;
  }

  public Integer getPregnancyDays() {
    return pregnancyDays;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public Boolean getLocationPrivacyAgreed() {
    return locationPrivacyAgreed;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }
}
