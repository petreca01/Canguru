package br.com.taqtile.android.app.data.maternity.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class MaternityItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("channel_id")
    private Integer channelId;

    @SerializedName("state")
    private String state;

    @SerializedName("city")
    private String city;

    @SerializedName("name")
    private String name;

    @SerializedName("about")
    private String about;

    @SerializedName("address")
    private String address;

    @SerializedName("website")
    private String website;

    @SerializedName("phone")
    private String phone;

    @SerializedName("logo")
    private String logo;

    @SerializedName("image")
    private String image;

    @SerializedName("birth_school")
    private String birthSchool;

    @SerializedName("visit_rules")
    private String visitRules;

    @SerializedName("available_midwife")
    private String availableMidwife;

    @SerializedName("bath_birth")
    private String bathBirth;

    @SerializedName("ppp_room")
    private String pppRoom;

    @SerializedName("crowd_room")
    private String crowdRoom;

    @SerializedName("accreditations")
    private String accreditations;

    @SerializedName("rate")
    private Float rate;

    @SerializedName("total_rates")
    private Integer totalRates;

    @SerializedName("show_image_on_top")
    private String showImageOnTop;

    @SerializedName("show_map")
    private String showMap;

    @SerializedName("current_births_source")
    private String currentBirthsSource;

    @SerializedName("current_total_births")
    private Integer currentTotalBirths;

    @SerializedName("current_percent_normal_birth")
    private Float currentPercentNormalBirth;

    @SerializedName("current_percent_csections")
    private Float currentPercentCsections;

    @SerializedName("highlight")
    private Boolean highlight;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("one_rates")
    private Integer oneRates;

    @SerializedName("two_rates")
    private Integer twoRates;

    @SerializedName("four_rates")
    private Integer fourRates;

    @SerializedName("three_rates")
    private Integer threeRates;

    @SerializedName("five_rates")
    private Integer fiveRates;

    @SerializedName("my_rate")
    private Integer myRate;

    @SerializedName("is_following")
    private Boolean isFollowing;

    public Integer getId() {
        return id;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getAbout() {
        return about;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhone() {
        return phone;
    }

    public String getLogo() {
        return logo;
    }

    public String getImage() {
        return image;
    }

    public String getBirthSchool() {
        return birthSchool;
    }

    public String getVisitRules() {
        return visitRules;
    }

    public String getAvailableMidwife() {
        return availableMidwife;
    }

    public String getBathBirth() {
        return bathBirth;
    }

    public String getPppRoom() {
        return pppRoom;
    }

    public String getCrowdRoom() {
        return crowdRoom;
    }

    public String getAccreditations() {
        return accreditations;
    }

    public Float getRate() {
        return rate;
    }

    public Integer getTotalRates() {
        return totalRates;
    }

    public String getShowImageOnTop() {
        return showImageOnTop;
    }

    public String getShowMap() {
        return showMap;
    }

    public String getCurrentBirthsSource() {
        return currentBirthsSource;
    }

    public Integer getCurrentTotalBirths() {
        return currentTotalBirths;
    }

    public Float getCurrentPercentNormalBirth() {
        return currentPercentNormalBirth;
    }

    public Float getCurrentPercentCsections() {
        return currentPercentCsections;
    }

    public Boolean getHighlight() {
        return highlight;
    }

    public Boolean getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Integer getOneRates() {
        return oneRates;
    }

    public Integer getTwoRates() {
        return twoRates;
    }

    public Integer getFourRates() {
        return fourRates;
    }

    public Integer getThreeRates() {
        return threeRates;
    }

    public Integer getFiveRates() {
        return fiveRates;
    }

    public Integer getMyRate() {
        return myRate;
    }

    public Boolean getFollowing() {
        return isFollowing;
    }
}
