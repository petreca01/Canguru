package br.com.taqtile.android.app.data.exams;

import java.util.List;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.exams.remote.ExamsRemoteDataSource;
import br.com.taqtile.android.app.data.exams.remote.mappers.ExamsGetRemoteResponseToExamsResultMapper;
import br.com.taqtile.android.app.data.exams.remote.mappers.ExamsInfoItemRemoteResponseListToExamDataResultListMapper;
import br.com.taqtile.android.app.data.exams.remote.mappers.StartGestationParamsToUltrasoundRemoteRequestMapper;
import br.com.taqtile.android.app.data.exams.remote.models.request.UltrasoundRemoteRequest;
import br.com.taqtile.android.app.domain.exams.models.ExamDataResult;
import br.com.taqtile.android.app.domain.exams.models.ExamsResult;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import rx.Observable;

/**
 * Created by taqtile on 3/22/17.
 */

public class ExamsRepository extends BaseRepository<ExamsRemoteDataSource, Void> {

  public ExamsRepository(ExamsRemoteDataSource examsRemoteDataSource) {
    super(examsRemoteDataSource, null);
  }

  public Observable<ExamsResult> fetchExam() {
    return getRemoteDataSource().fetchExam()
      .map(result -> ExamsGetRemoteResponseToExamsResultMapper.perform(result.getResult()))
      // TODO: 6/20/17 Will be removed later. It is an easy and fast approach to cover an APIs issue
      .onErrorResumeNext(getRemoteDataSource().fetchExamLegacy().map(any -> new ExamsResult()));
  }

  public Observable<List<ExamDataResult>> createUltrasound(StartOrEditGestationParams startOrEditGestationParams) {
    UltrasoundRemoteRequest ultrasoundRemoteRequest =
      StartGestationParamsToUltrasoundRemoteRequestMapper.perform(startOrEditGestationParams);

    return getRemoteDataSource().createUltrasound(ultrasoundRemoteRequest)
      .map(result -> ExamsInfoItemRemoteResponseListToExamDataResultListMapper.perform(result.getResult()));
  }
}
