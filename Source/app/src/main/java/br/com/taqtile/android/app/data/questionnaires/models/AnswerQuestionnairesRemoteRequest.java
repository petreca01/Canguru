package br.com.taqtile.android.app.data.questionnaires.models;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.remote.models.RemoteRequestModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by taqtile on 3/24/17.
 */

public class AnswerQuestionnairesRemoteRequest implements RemoteRequestModel {

  private List<AnswerRemoteRequest> answers;
  @SerializedName("finish_pregnancy")
  private boolean finishPregnancy;
  public static final String MULTIPLE_CHOICES_SEPARATOR = "; ";

  public AnswerQuestionnairesRemoteRequest(List<AnswerRemoteRequest> answers, boolean finishPregnancy) {
    this.answers = answers;
    this.finishPregnancy = finishPregnancy;
  }

  public AnswerQuestionnairesRemoteRequest(AnswerRemoteRequest answer) {
    this.answers = new ArrayList<AnswerRemoteRequest>() {{
      add(answer);
    }};
    finishPregnancy = false;
  }

  @Override
  public Map<String, String> getFormData() {

    HashMap<String, String> data = new HashMap<>();

    data.put("finish_pregnancy", String.valueOf(finishPregnancy));

    if (answers == null) {
      return data;
    }

    String fullAnswer = "";
    for (AnswerRemoteRequest answer : answers) {
      if (fullAnswer.isEmpty()) {
        fullAnswer = answer.getAnswer().toString();
      } else {
        fullAnswer = fullAnswer + MULTIPLE_CHOICES_SEPARATOR + answer.getAnswer().toString();
      }
    }
    String key = String.format("answer[%d]", answers.get(0).getQuestionId());
    data.put(key, fullAnswer);

    return data;
  }

  public boolean getFinishPregnancy() {
    return finishPregnancy;
  }
}
