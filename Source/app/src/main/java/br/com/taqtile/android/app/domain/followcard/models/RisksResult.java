package br.com.taqtile.android.app.domain.followcard.models;

import java.util.ArrayList;

/**
 * Created by taqtile on 5/19/17.
 */

public class RisksResult {

  private ArrayList<String> alert;
  private ArrayList<String> risk;

  public RisksResult(ArrayList<String> alert, ArrayList<String> risk) {
    this.alert = alert;
    this.risk = risk;
  }

  public ArrayList<String> getAlert() {
    return alert;
  }

  public ArrayList<String> getRisk() {
    return risk;
  }
}
