package br.com.taqtile.android.app.domain.account.mappers;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.TemplateApplication;

/**
 * Created by taqtile on 4/26/17.
 */

public class UserDataResultToViewModelMapper {

  public static UserViewModel perform(UserResult userDataResult) {
    return new UserViewModel(
      getEmail(userDataResult.getEmail()),
      getCellphoneNumber(userDataResult.getCellphoneNumber()),
      getUserBirthdate(userDataResult.getUserBirthdate()),
      getCpf(userDataResult.getCpf()),
      getOperatorDocument(userDataResult.getOperatorDocument()),
      getZipCode(userDataResult.getZipCode()),
      getOperatorId(userDataResult.getOperatorId()),
      getOperator(userDataResult.getOperator()),
      getUserType(userDataResult.getUserType()),
      getIgTrimesters(userDataResult.getIgTrimesters()),
      getIgMonths(userDataResult.getIgMonths()),
      getIgWeeks(userDataResult.getIgWeeks()),
      getIgDays(userDataResult.getIgDays()),
      getBabyName(userDataResult.getBabyName()),
      getDum(userDataResult.getDum()),
      getChildbirthEstimatedDate(userDataResult.getChildbirthEstimatedDate()),
      getChildbirthEstimatedDateBase(userDataResult.getChildbirthEstimatedDateBase()),
      getCity(userDataResult.getCity()),
      getState(userDataResult.getState()),
      getLatitude(userDataResult.getLatitude()),
      getLongitude(userDataResult.getLongitude()),
      getLocationPolicyAgreed(userDataResult.getLocationPolicyAgreed()),
      getPregnancyDatesUpdatedAt(userDataResult.getPregnancyDatesUpdatedAt()),
      getDolpUpdatedAt(userDataResult.getDolpUpdatedAt()));
  }

  private static String getEmail(String email) {
    return email != null ? email : "";
  }

  private static String getCellphoneNumber(String cellphoneNumber) {
    return cellphoneNumber != null ? cellphoneNumber : "";
  }

  private static String getUserBirthdate(String userBirthdate) {
    return userBirthdate != null ? userBirthdate : "";
  }

  private static String getCpf(String cpf) {
    return cpf != null ? cpf : "";
  }

  private static String getOperatorDocument(String documentId) {
    return documentId != null ? documentId : "";
  }

  private static String getZipCode(String zipCode) {
    return zipCode != null ? zipCode : "";
  }

  private static Integer getOperatorId(Integer operatorId) {
    return operatorId != null ? operatorId : 0;
  }

  private static String getOperator(String operator) {
    return operator != null ? operator : "";
  }

  private static String getUserType(String userType) {
    return userType != null ? userType : "";
  }

  private static Integer getIgTrimesters(Integer igTrimesters) {
    return igTrimesters != null ? igTrimesters : 0;
  }

  private static Integer getIgMonths(Integer igMonths) {
    return igMonths != null ? igMonths : 0;
  }

  private static Integer getIgWeeks(Integer igWeeks) {
    return igWeeks != null ? igWeeks : 0;
  }

  private static Integer getIgDays(Integer igDays) {
    return igDays != null ? igDays : 0;
  }

  private static String getDum(String dum) {
    return dum != null ? dum : "";
  }

  private static String getChildbirthEstimatedDate(String childbirthEstimatedDate) {
    return childbirthEstimatedDate != null ? childbirthEstimatedDate : "";
  }

  private static String getChildbirthEstimatedDateBase(String childbirthEstimatedDateBase) {
    return childbirthEstimatedDateBase != null ? childbirthEstimatedDateBase : "";
  }

  private static String getCity(String city) {
    return city != null ? city : "";
  }

  private static String getState(String state) {
    return state != null ? state : "";
  }

  private static String getLatitude(String latitude) {
    return latitude != null ? latitude : "";
  }

  private static String getLongitude(String longitude) {
    return longitude != null ? longitude : "";
  }

  private static Boolean getLocationPolicyAgreed(Boolean locationPolicyAgreed) {
    return locationPolicyAgreed != null ? locationPolicyAgreed : false;
  }

  private static String getBabyName(String babyName) {
    return babyName == null || babyName.isEmpty() ?
      TemplateApplication.getContext().getResources().getString(R.string.baby_name_default_text) :
      babyName;
  }

  private static String getPregnancyDatesUpdatedAt(String pregnancyDatesUpdatedAt) {
    return pregnancyDatesUpdatedAt != null ? pregnancyDatesUpdatedAt : "";
  }
  private static String getDolpUpdatedAt(String dolpUpdatedAt) {
    return dolpUpdatedAt != null ? dolpUpdatedAt : "";
  }

}
