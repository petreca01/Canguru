package br.com.taqtile.android.app.data.clinicalcondition.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class UserClinicalConditionRemoteResponse {
    @SerializedName("pregnancy_condition_id")
    private Integer pregnancyConditionId;

    @SerializedName("name")
    private String name;

    @SerializedName("date")
    private String date;

    @SerializedName("ga_weeks")
    private Integer gestationAgeWeeks;

    @SerializedName("ga_days")
    private Integer gestationAgeDays;

    @SerializedName("risk")
    private Integer risk;

    public Integer getPregnancyConditionId() {
        return pregnancyConditionId;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public Integer getGestationAgeWeeks() {
        return gestationAgeWeeks;
    }

    public Integer getGestationAgeDays() {
        return gestationAgeDays;
    }

    public Integer getRisk() {
        return risk;
    }
}
