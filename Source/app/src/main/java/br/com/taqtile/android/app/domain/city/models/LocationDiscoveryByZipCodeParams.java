package br.com.taqtile.android.app.domain.city.models;

/**
 * Created by taqtile on 4/19/17.
 */

public class LocationDiscoveryByZipCodeParams {

  private String zipCode;

  public LocationDiscoveryByZipCodeParams(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getZipCode() {
    return zipCode;
  }

}
