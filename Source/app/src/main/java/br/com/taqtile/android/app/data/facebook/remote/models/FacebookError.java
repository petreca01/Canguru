package br.com.taqtile.android.app.data.facebook.remote.models;

/**
 * Created by taqtile on 25/04/17.
 */

public class FacebookError {
  static final String LOGIN_CANCELED = "Login com Facebook cancelado";
  static final String LOGIN_FAILED = "Login com Facebook falhou";
  static final String SIGN_UP_FAILED = "Cadastro com o Facebook falhou";


  public static Throwable getLoginCanceledError() {
    return new Throwable(LOGIN_CANCELED);
  }

  public static Throwable getLoginFailedError() {
    return new Throwable(LOGIN_FAILED);
  }

  public static Throwable getSignUpFailledError() { return new Throwable(SIGN_UP_FAILED); }
}
