package br.com.taqtile.android.app.presentation.symptomdetail;

import android.os.Bundle;

import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;

import static br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailBundleConstants.SYMPTOM_KEY;

/**
 * Created by taqtile on 10/05/17.
 */

public class SymptomDetailBundleCreator {

  public static Bundle getBundle(SubSymptomViewModel subSymptomViewModel) {
    Bundle bundle = new Bundle(1);
    bundle.putParcelable(SYMPTOM_KEY, subSymptomViewModel);
    return bundle;
  }
}
