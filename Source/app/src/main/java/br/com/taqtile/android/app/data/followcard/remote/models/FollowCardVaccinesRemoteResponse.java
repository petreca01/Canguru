package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardVaccinesRemoteResponse {

  @SerializedName("id")
  private Integer id;

  @SerializedName("name")
  private String name;

  @SerializedName("taken")
  private String taken;

  @SerializedName("created_at")
  private String createdAt;

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getTaken() {
    return taken;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
