package br.com.taqtile.android.app.data.exams.remote.mappers;

import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsGetRemoteResponse;
import br.com.taqtile.android.app.domain.exams.models.ExamsResult;
import br.com.taqtile.android.app.domain.exams.models.ExamsValuesResult;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsGetRemoteResponseToExamsResultMapper {

  public static ExamsResult perform(ExamsGetRemoteResponse examsGetRemoteResponse) {
    return new ExamsResult(ExamsValuesRemoteResponseToExamsValuesResultMapper.perform(
      examsGetRemoteResponse.getValues()), examsGetRemoteResponse.getDone(),
      examsGetRemoteResponse.getObservations(), examsGetRemoteResponse.getDate());
  }
}
