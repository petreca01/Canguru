package br.com.taqtile.android.app.data.agenda.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/21/17.
 */

public class RemoveAppointmentRemoteRequest extends BaseRemoteRequest {
  @RequestName("compromisso_id")
  private Integer appointmentId;

  public RemoveAppointmentRemoteRequest(Integer appointmentId) {
    this.appointmentId = appointmentId;
  }
}
