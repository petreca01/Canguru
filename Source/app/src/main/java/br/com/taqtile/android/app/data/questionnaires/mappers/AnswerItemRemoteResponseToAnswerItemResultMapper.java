package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerItemRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerItemResult;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class AnswerItemRemoteResponseToAnswerItemResultMapper {

  public static AnswerItemResult perform(AnswerItemRemoteResponse answerItemRemoteResponse) {
    return new AnswerItemResult(answerItemRemoteResponse.getAnswer(),
      answerItemRemoteResponse.getCreatedAt());
  }
}
