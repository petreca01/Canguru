package br.com.taqtile.android.app.domain.maternity.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/11/17.
 */

public class MaternityDataResultToMaternityViewModelMapper {
  public static List<MaternityViewModel> map(List<MaternityDataResult> maternityDataResult) {
    return StreamSupport.stream(maternityDataResult)
      .map(MaternityDataResultToMaternityViewModelMapper::map)
      .collect(Collectors.toList());
  }

  public static MaternityViewModel map(MaternityDataResult maternityDataResult) {
    return new MaternityViewModel(
      maternityDataResult.getId(),
      maternityDataResult.getState(),
      maternityDataResult.getCity(),
      maternityDataResult.getName(),
      maternityDataResult.getAbout(),
      maternityDataResult.getAddress(),
      maternityDataResult.getPhone(),
      maternityDataResult.getLogo(),
      maternityDataResult.getImage(),
      maternityDataResult.getRate(),
      maternityDataResult.getTotalRates(),
      maternityDataResult.getShowMap(),
      maternityDataResult.getCurrentBirthsSource(),
      maternityDataResult.getCurrentPercentNormalBirth(),
      maternityDataResult.getCurrentPercentCsections(),
      maternityDataResult.getOneRates(),
      maternityDataResult.getTwoRates(),
      maternityDataResult.getThreeRates(),
      maternityDataResult.getFourRates(),
      maternityDataResult.getFiveRates(),
      maternityDataResult.getMyRate(),
      maternityDataResult.getBirthSchool(),
      maternityDataResult.getVisitRules(),
      maternityDataResult.getAvailableMidwife(),
      maternityDataResult.getBathBirth(),
      maternityDataResult.getPppRoom(),
      maternityDataResult.getCrowdRoom(),
      maternityDataResult.getAccreditations()
    );
  }
}
