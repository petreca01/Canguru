package br.com.taqtile.android.app.domain.clinicalConditions.models;

import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by taqtile on 4/12/17.
 */

public class ReportClinicalConditionResult {
  private UserResult user;
  private UserClinicalConditionResult userCondition;

  public ReportClinicalConditionResult(UserResult user, UserClinicalConditionResult userCondition) {
    this.user = user;
    this.userCondition = userCondition;
  }

  public UserResult getUser() {
    return user;
  }

  public UserClinicalConditionResult getUserCondition() {
    return userCondition;
  }
}
