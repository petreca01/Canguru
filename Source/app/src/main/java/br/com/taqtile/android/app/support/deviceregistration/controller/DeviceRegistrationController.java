package br.com.taqtile.android.app.support.deviceregistration.controller;

import android.support.annotation.NonNull;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.deviceregistration.SaveDeviceTokenUseCase;
import br.com.taqtile.android.app.domain.deviceregistration.models.DeviceInfoParams;
import rx.android.schedulers.AndroidSchedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 7/11/16.
 */

public class DeviceRegistrationController implements DeviceRegistrationContract.Controller {

  private final SaveDeviceTokenUseCase saveDeviceTokenUseCase;

  public DeviceRegistrationController(@NonNull SaveDeviceTokenUseCase saveDeviceTokenUseCase) {
    this.saveDeviceTokenUseCase = checkNotNull(saveDeviceTokenUseCase, "SaveDeviceToken use case cannot be null!");
  }

  @Override
  public void processDeviceToken(String deviceToken) {
    saveDeviceTokenUseCase.execute(new DeviceInfoParams(deviceToken))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onRegisterSuccess, this::onRegisterFailure);
  }

  private void onRegisterSuccess(EmptyResult emptyResult) {
    // do nothing
  }

  private void onRegisterFailure(Throwable throwable) {
    // do nothing
  }

}
