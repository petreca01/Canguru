package br.com.taqtile.android.app.presentation.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.CallbackManager;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginActivity;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleAccessor;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.presentation.signin.SignInPresenter;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.SIGNUP_FROM_SIGNIN_EMAIL_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.SIGNUP_FROM_SIGNIN_PASSWORD_KEY;

/**
 * Created by taqtile on 11/10/16.
 */

public class SignUpActivity extends TemplateBackActivity {

  private static final String TAG = "SignUpActivity";

  private SignUpFragment fragment;
  public static int SIGN_UP_REQUEST_CODE = 12345;

  private CallbackManager callbackManager;
  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private int onActivityRequestCode;
  private int onActivityResultCode;
  private Bundle onActivityResultBundle;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    InitialLoginBundleAccessor bundleAccessor = new InitialLoginBundleAccessor(getIntent().getExtras());

    if (fragment != null) {
      fragment.setInitialLoginBundleAccessor(bundleAccessor);
    }
  }

  @Override public Fragment getFragment() {
    SignUpFragment signUpFragment =
      (SignUpFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (signUpFragment == null) {
      signUpFragment = SignUpFragment.newInstance();
    }

    fragment = signUpFragment;

    return fragment;
  }

  @Override public BasePresenter getPresenter() {
    SignUpPresenter presenter = new SignUpPresenter();
    fragment.setPresenter(presenter);

    SignInPresenter presenterLogin = new SignInPresenter();
    fragment.setLoginPresenter(presenterLogin);

    NavigationHelper navigationHelper = new NavigationHelper(this);
    fragment.setNavigationManager(navigationHelper);
    return presenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, SignUpActivity.class);

    if (context instanceof InitialLoginActivity) {
      ((InitialLoginActivity) context).setResultListener(listener,
        SIGN_UP_REQUEST_CODE);
      ((InitialLoginActivity) context).startActivityForResult(intent,
        SIGN_UP_REQUEST_CODE);
    }
  }

  public static void navigate(Context context, CustomNavigationResultListener listener, String email, String password) {
    Intent intent = new Intent(context, SignUpActivity.class);
    intent.putExtra(SIGNUP_FROM_SIGNIN_EMAIL_KEY, email);
    intent.putExtra(SIGNUP_FROM_SIGNIN_PASSWORD_KEY, password);

    if (context instanceof SignInActivity) {
      ((SignInActivity) context).setResultListener(listener,
        SIGN_UP_REQUEST_CODE);
      ((SignInActivity) context).startActivityForResult(intent,
        SIGN_UP_REQUEST_CODE);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    Log.e(TAG, "onActivityResult() resultCode: "+resultCode);

    if (callbackManager != null) {
      callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    if (requestCode == observedRequestCode
      && resultListener != null) {
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
      onActivityResultBundle = data != null ? data.getExtras() : new Bundle();
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public static void navigate(Context context, Bundle bundle) {
    Intent intent = new Intent(context, SignUpActivity.class);
    if (bundle != null) {
      intent.putExtras(bundle);
    }
    context.startActivity(intent);
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
