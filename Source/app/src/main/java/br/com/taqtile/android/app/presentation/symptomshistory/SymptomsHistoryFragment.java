package br.com.taqtile.android.app.presentation.symptomshistory;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.listings.adapters.SymptomsHistoryAdapter;
import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import br.com.taqtile.android.app.misc.dialog.EmailFormInDialog;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomsHistoryFragment extends AppBaseFragment {

  @BindView(R.id.fragment_symptoms_history_recycler_view)
  RecyclerView symptomsHistoryRecyclerView;

  @BindView(R.id.fragment_symptoms_history_loading)
  ProgressBarLoading loading;

  private NavigationManager navigationManager;
  private SymptomsHistoryContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private String userEmail;

  protected SymptomsHistoryAdapter symptomsHistoryAdapter;
  protected LinearLayoutManager linearLayoutManager;
  protected List<SymptomHistoryViewModel> symptomsList;
  private Subscription symptomsHistorySubscription;

  public static SymptomsHistoryFragment newInstance() {
    return new SymptomsHistoryFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_symptoms_history, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setUserEmail();
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();
    setupRecyclerView();
    fetchSymptomsHistory();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_symptoms_history_title));
    }
  }

  private void setUserEmail() {
    Subscription subscription = Toolbox.getInstance()
      .getUserEmail()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        (email) -> { this.userEmail = email; }
      );
    compositeSubscription.add(subscription);
  }

  private void setupRecyclerView() {
    symptomsList = new ArrayList<>();
    linearLayoutManager = new LinearLayoutManager(getContext());
    symptomsHistoryRecyclerView.setLayoutManager(linearLayoutManager);
    symptomsHistoryAdapter = new SymptomsHistoryAdapter(symptomsList, getContext());
    symptomsHistoryRecyclerView.setAdapter(symptomsHistoryAdapter);
    symptomsHistoryAdapter.setListener(this::sendUserSymptomsHistory);
  }

  public void setPresenter(SymptomsHistoryContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_symptoms_history_placeholder;
  }

  private void fetchSymptomsHistory() {
    showLoading();
    symptomsHistorySubscription = presenter.getUserSymptoms()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchSymptomsHistorySuccess, this::onFetchSymptomsHistoryFailure);
    compositeSubscription.add(symptomsHistorySubscription);
  }

  private void onFetchSymptomsHistorySuccess(List<SymptomHistoryViewModel> symptoms) {
    if (symptoms != null && !symptoms.isEmpty()) {
      resetSymptomsList();
      symptomsList.addAll(symptoms);
      symptomsHistoryAdapter.notifyDataSetChanged();
      hideLoading();
      hidePlaceholder();
    } else {
      showNoSymptomHistoryPlaceholder();
    }
  }

  private void resetSymptomsList() {
    if (symptomsList != null) {
      symptomsList.clear();
    }
  }

  private void showNoSymptomHistoryPlaceholder() {
    placeholder.setPlaceholderType(CustomPlaceholder.NO_SYMPTOM_HISTORY);
    hideLoading();
    showPlaceholder();
  }


  private void onFetchSymptomsHistoryFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    fetchSymptomsHistory();
  }

  private void sendUserSymptomsHistory(View view) {
    showSendDialog(this.userEmail);
  }

  private void showSendDialog(@Nullable String defaultValue) {
    final EmailFormInDialog input = getEditTextForDialog();
    input.setText(defaultValue);

    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_symptoms_history_dialog_title))
      .setView(input)
      .setPositiveButton(getString(R.string.send), (dialog, which) -> {
        {
          Subscription subscription = presenter.sendUserSymptomsHistory(input.getFormText())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(e -> onEmailSent(),
                      this::onEmailNotSent);
          compositeSubscription.add(subscription);
        }
      })
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
        // do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));
  }

  private EmailFormInDialog getEditTextForDialog() {
    return new EmailFormInDialog(getContext());
  }

  @Override
  public void showLoading() {
    super.showLoading();
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    loading.setVisibility(View.GONE);
  }

  private void onEmailSent() {
    showSnackbar(getString(R.string.fragment_symptoms_history_email_sent_message), Snackbar.LENGTH_LONG);
  }

  private void onEmailNotSent(Throwable throwable) {
    showSnackbar(getString(R.string.fragment_symptoms_history_email_not_sent_message), Snackbar.LENGTH_LONG);
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom_history),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
