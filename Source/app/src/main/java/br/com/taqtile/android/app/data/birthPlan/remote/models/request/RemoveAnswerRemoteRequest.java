package br.com.taqtile.android.app.data.birthPlan.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class RemoveAnswerRemoteRequest extends BaseRemoteRequest {

  private String answer;

  public RemoveAnswerRemoteRequest(String answer) {
    this.answer = answer;
  }

  public String getAnswer() {
    return answer;
  }
}
