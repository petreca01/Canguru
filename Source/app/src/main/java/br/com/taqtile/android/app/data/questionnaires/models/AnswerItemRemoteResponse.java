package br.com.taqtile.android.app.data.questionnaires.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class AnswerItemRemoteResponse {
    @SerializedName("answer")
    private String answer;

    @SerializedName("created_at")
    private String createdAt;

    public String getAnswer() {
        return answer;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
