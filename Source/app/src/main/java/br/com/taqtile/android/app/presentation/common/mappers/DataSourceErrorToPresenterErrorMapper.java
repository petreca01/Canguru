package br.com.taqtile.android.app.presentation.common.mappers;

import java.util.Objects;

import br.com.taqtile.android.app.data.common.remote.models.RemoteError;
import br.com.taqtile.android.app.presentation.common.PresenterError;

/**
 * Created by taqtile on 4/30/17.
 */

public class DataSourceErrorToPresenterErrorMapper {

  public static PresenterError map(Throwable error) {
    PresenterError presenterError = new PresenterError();

    if (error instanceof RemoteError) {
      mapRemoteError(presenterError, (RemoteError) error);
    }

    return presenterError;
  }

  private static void mapRemoteError(PresenterError presenterError, RemoteError remoteError) {
    @RemoteError.ErrorType String errorType = remoteError.getErrorType();

    if (Objects.equals(errorType, RemoteError.NO_CONNECTION)) {
      presenterError.setErrorType(PresenterError.NO_CONNECTION);
    } else if (Objects.equals(errorType, RemoteError.INVALID_DATA)) {
      presenterError.setErrorType(PresenterError.INVALID_DATA);
    } else if (Objects.equals(errorType, RemoteError.NOT_FOUND)) {
      presenterError.setErrorType(PresenterError.NOT_FOUND);
    } else if (Objects.equals(errorType, RemoteError.NOT_AUTHORIZED)) {
      presenterError.setErrorType(PresenterError.NOT_AUTHORIZED);
    } else if (Objects.equals(errorType, RemoteError.TIMEOUT)) {
      presenterError.setErrorType(PresenterError.TIMEOUT);
    } else {
      presenterError.setErrorType(PresenterError.UNKNOWN);
    }

    presenterError.setMessage(remoteError.getMessage());
  }


}
