package br.com.taqtile.android.app.presentation.conditiondetails;

import br.com.taqtile.android.app.domain.clinicalConditions.ReportConditionUseCase;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;
import br.com.taqtile.android.app.presentation.condition.models.ReportClinicalConditionViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionDetailsPresenter implements ConditionDetailsContract.Presenter {

  private ReportConditionUseCase reportConditionUseCase;

  public ConditionDetailsPresenter() {
    this.reportConditionUseCase = Injection.provideReportConditionUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<ReportClinicalConditionViewModel> reportCondition(ReportConditionParams reportConditionParams) {
    return reportConditionUseCase.execute(reportConditionParams);
  }
}
