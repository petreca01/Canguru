package br.com.taqtile.android.app.domain.timeline.models;

import java.util.List;

import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.domain.forum.models.PostResult;

/**
 * Created by taqtile on 3/21/17.
 */

public class TimelineModel {
    List<PostResult> postResults;
    List<ChannelPost> channelPosts;

    public List<PostResult> getPostResults() {
        return postResults;
    }

    public void setPostResults(List<PostResult> formPosts) {
        this.postResults = formPosts;
    }

    public List<ChannelPost> getChannelPosts() {
        return channelPosts;
    }

    public void setChannelPosts(List<ChannelPost> channelPosts) {
        this.channelPosts = channelPosts;
    }
}
