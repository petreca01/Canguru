package br.com.taqtile.android.app.presentation.diary;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.agenda.FinishAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.RemoveAgendaItemUseCase;
import br.com.taqtile.android.app.domain.agenda.ShowAgendaUseCase;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.diary.mapper.AgendaItemsViewModelToDiaryViewModelMapper;
import br.com.taqtile.android.app.presentation.diary.model.DiaryViewModel;
import br.com.taqtile.android.app.support.Injection;
import java8.util.stream.Stream;
import java8.util.stream.StreamSupport;
import rx.Observable;

/**
 * Created by taqtile on 04/05/17.
 */

public class DiaryPresenter implements DiaryContract.Presenter {

  private UserDataUseCase userDataUseCase;
  private ShowAgendaUseCase showAgendaUseCase;
  private FinishAgendaItemUseCase finishAgendaItemUseCase;
  private RemoveAgendaItemUseCase removeAgendaItemUseCase;

  public DiaryPresenter() {
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.showAgendaUseCase = Injection.provideShowAgendaUseCase();
    this.finishAgendaItemUseCase = Injection.provideFinishAgendaItemUseCase();
    this.removeAgendaItemUseCase = Injection.provideRemoveAgendaItemUseCase();
  }


  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> checkUserData() {
    return userDataUseCase.execute(null);
  }

  @Override
  public double calculatePercentage(List<AgendaFeedViewModel> itemsList, int totalAppointmentsAccomplished) {
    double listSize = itemsList.size();
    double itemsAccomplished = (double) totalAppointmentsAccomplished;
    return Math.round(itemsAccomplished / listSize * 100);
  }

  @Override
  public int calculateAccomplishedTotal(List<AgendaFeedViewModel> itemsList) {
    return (int) getAgendaItems(itemsList)
      .filter(item -> ((AgendaItemViewModel) item).isAppointmentAttended())
      .count();
  }

  @Override
  public int calculateAccomplishedOrScheduledTotal(List<AgendaFeedViewModel> itemsList) {
    return (int) getAgendaItems(itemsList)
      .filter(item -> ((AgendaItemViewModel) item).isAppointmentAttended() ||
        ((AgendaItemViewModel) item).isAppointmentScheduled())
      .count();
  }

  private Stream<AgendaFeedViewModel> getAgendaItems(List<AgendaFeedViewModel> itemsList) {
    return StreamSupport.stream(itemsList)
      .filter(item -> item instanceof AgendaItemViewModel);
  }

  @Override
  public Observable<AgendaItemViewModel> finishAppointment(
    FinishAppointmentParams finishAppointmentParams) {
    return finishAgendaItemUseCase.execute(finishAppointmentParams);
  }

  @Override
  public Observable<String> removeAppointment(RemoveAppointmentParams removeAppointmentParams) {
    return removeAgendaItemUseCase.execute(removeAppointmentParams);
  }

  @Override
  public Observable<DiaryViewModel> listAppointments() {
    return Toolbox.getInstance().getDiaryTutorial()
      .flatMap(diaryTutorialAlreadySeen -> {
        if (diaryTutorialAlreadySeen) {
          return getRealAppointments();
        } else {
          return Toolbox.getInstance().getMainTutorial()
            .flatMap(this::getMockAppointments);
        }
      });
  }

  // This mock refers to tutorial data DON`T REMOVE IT
  private Observable<DiaryViewModel> getMockAppointments(Boolean mainTutorialAlreadySeen) {
    List<AgendaItemViewModel> mockItems = new ArrayList<>();
    mockItems.add(new AgendaItemViewModel(1, 1, "Primeiro Ultrassom", 1, "1", "1", 1, "1",
      "O primeiro ultrassom é muito importante", 0));
    mockItems.add(new AgendaItemViewModel(1, 1, "Primeiro Ultrassom", 1, "1", "1", 1, "1",
      "O primeiro ultrassom é muito importante", 1));
    return Observable.just(AgendaItemsViewModelToDiaryViewModelMapper
      .perform(mainTutorialAlreadySeen, mockItems));
  }


  private Observable<DiaryViewModel> getRealAppointments() {
    return showAgendaUseCase.execute(null)
      .map(itemsList -> AgendaItemsViewModelToDiaryViewModelMapper.perform(false,
        itemsList));
  }


}
