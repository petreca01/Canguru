package br.com.taqtile.android.app.presentation.gestation;

import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.weeklycontent.FetchWeeklyContentUseCase;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class GestationPresenter implements GestationContract.Presenter {

  private UserDataUseCase userDataUseCase;
  private FetchWeeklyContentUseCase weeklyContentUseCase;

  public GestationPresenter() {
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.weeklyContentUseCase = Injection.provideFetchWeeklyContentUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> fetchUserData() {
    return userDataUseCase.execute(null);
  }

  @Override
  public Observable<WeeklyContentViewModel> fetchWeeklyContent(WeeklyContentParams weeklyContentParams) {
    return weeklyContentUseCase.execute(weeklyContentParams);
  }

}
