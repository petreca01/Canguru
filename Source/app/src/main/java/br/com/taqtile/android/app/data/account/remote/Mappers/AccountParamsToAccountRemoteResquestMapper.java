package br.com.taqtile.android.app.data.account.remote.Mappers;

import android.support.annotation.NonNull;

import br.com.taqtile.android.app.data.account.remote.models.request.SignInRemoteRequest;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInParams;

/**
 * Created by taqtile on 3/19/17.
 */

public class AccountParamsToAccountRemoteResquestMapper {
    public static SignInRemoteRequest perform(@NonNull AccountSignInParams params) {
        SignInRemoteRequest requestModel = new SignInRemoteRequest();
        requestModel.setEmail(params.getEmail());
        requestModel.setPassword(params.getPassword());
        return requestModel;
    }
}
