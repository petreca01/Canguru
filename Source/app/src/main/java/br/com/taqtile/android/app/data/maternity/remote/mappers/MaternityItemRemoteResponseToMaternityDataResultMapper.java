package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityItemRemoteResponse;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityItemRemoteResponseToMaternityDataResultMapper {

  public static MaternityDataResult perform(
    MaternityItemRemoteResponse maternityItemRemoteResponse) {
    return new MaternityDataResult(maternityItemRemoteResponse.getId(),
      maternityItemRemoteResponse.getChannelId(), maternityItemRemoteResponse.getState(),
      maternityItemRemoteResponse.getCity(), maternityItemRemoteResponse.getName(),
      maternityItemRemoteResponse.getAbout(), maternityItemRemoteResponse.getAddress(),
      maternityItemRemoteResponse.getWebsite(), maternityItemRemoteResponse.getPhone(), maternityItemRemoteResponse.getLogo(),
      maternityItemRemoteResponse.getImage(), maternityItemRemoteResponse.getBirthSchool(),
      maternityItemRemoteResponse.getVisitRules(), maternityItemRemoteResponse.getAvailableMidwife(),
      maternityItemRemoteResponse.getBathBirth(), maternityItemRemoteResponse.getPppRoom(),
      maternityItemRemoteResponse.getCrowdRoom(), maternityItemRemoteResponse.getAccreditations(),
      maternityItemRemoteResponse.getRate(), maternityItemRemoteResponse.getTotalRates(),
      maternityItemRemoteResponse.getShowImageOnTop(), maternityItemRemoteResponse.getShowMap(),
      maternityItemRemoteResponse.getCurrentBirthsSource(), maternityItemRemoteResponse.getCurrentTotalBirths(),
      maternityItemRemoteResponse.getCurrentPercentNormalBirth(), maternityItemRemoteResponse.getCurrentPercentCsections(),
      maternityItemRemoteResponse.getHighlight(), maternityItemRemoteResponse.getActive(),
      maternityItemRemoteResponse.getCreatedAt(), maternityItemRemoteResponse.getUpdatedAt(), maternityItemRemoteResponse.getOneRates(),
      maternityItemRemoteResponse.getTwoRates(), maternityItemRemoteResponse.getFourRates(), maternityItemRemoteResponse.getThreeRates(),
      maternityItemRemoteResponse.getFiveRates(), maternityItemRemoteResponse.getMyRate(),
      maternityItemRemoteResponse.getFollowing());
  }
}
