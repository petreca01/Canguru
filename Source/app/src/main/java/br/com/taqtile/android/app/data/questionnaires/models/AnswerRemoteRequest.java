package br.com.taqtile.android.app.data.questionnaires.models;

/**
 * Created by taqtile on 3/24/17.
 */

public class AnswerRemoteRequest<T> {
    private T answer;
    private Integer questionId;

    public AnswerRemoteRequest(T answer, Integer questionId) {
        this.answer = answer;
        this.questionId = questionId;
    }

    public T getAnswer() {
        return answer;
    }

    public Integer getQuestionId() {
        return questionId;
    }
}
