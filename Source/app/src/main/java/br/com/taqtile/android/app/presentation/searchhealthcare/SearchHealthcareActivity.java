package br.com.taqtile.android.app.presentation.searchhealthcare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.healthcaredetails.HealthcareDetailsActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 18/04/17.
 */

public class SearchHealthcareActivity extends TemplateBackActivity {

  private final static int CHANGE_USER_HEALTHCARE_REQUEST_CODE = 1313;
  private SearchHealthcareFragment fragment;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private int onActivityResultCode;
  private int onActivityRequestCode;
  private Bundle onActivityResultBundle;

  @Override
  public Fragment getFragment() {
    SearchHealthcareFragment searchHealthcareFragment = (SearchHealthcareFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (searchHealthcareFragment == null) {
      searchHealthcareFragment = SearchHealthcareFragment.newInstance();
    }
    fragment = searchHealthcareFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {

    SearchHealthcarePresenter presenter = new SearchHealthcarePresenter();
    fragment.setPresenter(presenter);
    fragment.setNavigationManager(new NavigationHelper(this));

    return presenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, SearchHealthcareActivity.class);

    if (context instanceof AdditionalInfoActivity) {
      ((AdditionalInfoActivity) context).setResultListener(listener,
        CHANGE_USER_HEALTHCARE_REQUEST_CODE);
      ((AdditionalInfoActivity) context).startActivityForResult(intent,
        CHANGE_USER_HEALTHCARE_REQUEST_CODE);
    } else if (context instanceof HealthcareDetailsActivity) {
      ((HealthcareDetailsActivity) context).setResultListener(listener,
        CHANGE_USER_HEALTHCARE_REQUEST_CODE);
      ((HealthcareDetailsActivity) context).startActivityForResult(intent,
        CHANGE_USER_HEALTHCARE_REQUEST_CODE);
    }
  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
      onActivityResultBundle = data != null ? data.getExtras() : new Bundle();
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }

}
