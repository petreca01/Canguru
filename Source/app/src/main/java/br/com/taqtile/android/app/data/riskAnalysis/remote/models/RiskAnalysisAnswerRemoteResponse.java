package br.com.taqtile.android.app.data.riskAnalysis.remote.models;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;

/**
 * Created by taqtile on 3/24/17.
 */

public class RiskAnalysisAnswerRemoteResponse {
    @SerializedName("user")
    private UserRemoteResponse user;

    @SerializedName("questions")
    private RiskAnalysisQuestionsRemoteResponse questions;

    public UserRemoteResponse getUser() {
        return user;
    }

    public RiskAnalysisQuestionsRemoteResponse getQuestions() {
        return questions;
    }
}
