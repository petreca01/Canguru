package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditUserPasswordParams {

  private String password;

  public EditUserPasswordParams(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }
}
