package br.com.taqtile.android.app.domain.common.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class UserResult {

  private Integer scheduleEngagement;
  private String pregnancyDatesUpdatedAt;
  private String babyName;
  private Integer operatorId;
  private String about;
  private Boolean operatorCanSeeRiskAnswers;
  private String operatorDocument;
  private String userType;
  private Integer risk;
  private Integer id;
  private String token;
  private String createdAt;
  private String zipCode;
  private String dolpUpdatedAt;
  private String cpf;
  private String coverPicture;
  private String locationBlockedAt;
  private Integer sure;
  private String locationPrivacyAgreedAt;
  private String deactivatedAt;
  private String profilePicture;
  private String userBirthdate;
  private String operator;
  private String updatedAt;
  private Integer igTrimesters;
  private Integer igMonths;
  private Integer igWeeks;
  private String conclusionText;
  private Boolean loaded;
  private String dum;
  private String childbirthEstimatedDate;
  private String childbirthEstimatedDateBase;
  private String email;
  private String conclusionDate;
  private String conclusionMethod;
  private Boolean active;
  private String name;
  private String deactivationReason;
  private Integer igDays;
  private String cellphoneNumber;
  private String city;
  private String state;
  private String latitude;
  private String longitude;
  private Boolean locationPolicyAgreed;

  public UserResult(String about, Boolean active, String babyName, String cellphoneNumber,
                    String conclusionDate, String conclusionMethod, String conclusionText,
                    String coverPicture, String cpf, String createdAt, String deactivatedAt,
                    String deactivationReason, String dolpUpdatedAt, String dum, String dpp,
                    String dppBase, String email, Integer id, Integer igTrimesters,
                    Integer igMonths, Integer igDays, Integer igWeeks, Boolean loaded,
                    String locationBlockedAt, String locationPrivacyAgreedAt, String name,
                    String operator, Boolean operatorCanSeeRiskAnswers, Integer operatorId, String operatorDocument,
                    String pregnancyDatesUpdatedAt, String profilePicture, Integer risk,
                    Integer scheduleEngagement, Integer sure, String token, String updatedAt,
                    String userBirthdate, String userType, String zipCode, String city,
                    String state, String latitude, String longitude, Boolean locationPolicyAgreed) {

    this.scheduleEngagement = scheduleEngagement;
    this.pregnancyDatesUpdatedAt = pregnancyDatesUpdatedAt;
    this.babyName = babyName;
    this.operatorId = operatorId;
    this.about = about;
    this.state = state;
    this.operatorCanSeeRiskAnswers = operatorCanSeeRiskAnswers;
    this.operatorDocument = operatorDocument;
    this.userType = userType;
    this.risk = risk;
    this.id = id;
    this.token = token;
    this.createdAt = createdAt;
    this.zipCode = zipCode;
    this.dolpUpdatedAt = dolpUpdatedAt;
    this.cpf = cpf;
    this.longitude = longitude;
    this.coverPicture = coverPicture;
    this.locationBlockedAt = locationBlockedAt;
    this.sure = sure;
    this.locationPrivacyAgreedAt = locationPrivacyAgreedAt;
    this.deactivatedAt = deactivatedAt;
    this.profilePicture = profilePicture;
    this.userBirthdate = userBirthdate;
    this.operator = operator;
    this.updatedAt = updatedAt;
    this.igTrimesters = igTrimesters;
    this.igMonths = igMonths;
    this.igWeeks = igWeeks;
    this.conclusionText = conclusionText;
    this.loaded = loaded;
    this.dum = dum;
    this.childbirthEstimatedDate = dpp;
    this.childbirthEstimatedDateBase = dppBase;
    this.email = email;
    this.conclusionDate = conclusionDate;
    this.conclusionMethod = conclusionMethod;
    this.active = active;
    this.name = name;
    this.latitude = latitude;
    this.deactivationReason = deactivationReason;
    this.igDays = igDays;
    this.cellphoneNumber = cellphoneNumber;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
    this.locationPolicyAgreed = locationPolicyAgreed;
  }

  public String getBabyName() {
    return babyName;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public String getOperatorDocument() {
    return operatorDocument;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getCpf() {
    return cpf;
  }

  public String getEmail() {
    return email;
  }

  public String getUserBirthdate() {
    return userBirthdate;
  }

  public String getOperator() {
    return operator;
  }

  public String getCellphoneNumber() {
    return cellphoneNumber;
  }

  public Integer getIgWeeks() {
    return igWeeks;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getAbout() {
    return about;
  }

  public String getState() {
    return state;
  }

  public String getUserType() {
    return userType;
  }

  public String getName() {
    return name;
  }

  public Integer getIgTrimesters() {
    return igTrimesters;
  }

  public Integer getIgMonths() {
    return igMonths;
  }

  public Integer getIgDays() {
    return igDays;
  }

  public String getDum() {
    return dum;
  }

  public String getChildbirthEstimatedDate() {
    return childbirthEstimatedDate;
  }

  public String getChildbirthEstimatedDateBase() {
    return childbirthEstimatedDateBase;
  }

  public Integer getRisk() {
    return risk;
  }

  public Integer getId() {
    return id;
  }

  public String getToken() {
    return token;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public Boolean getLoaded() {
    return loaded;
  }

  public Boolean getActive() {
    return active;
  }

  public Integer getScheduleEngagement() {
    return scheduleEngagement;
  }

  public String getPregnancyDatesUpdatedAt() {
    return pregnancyDatesUpdatedAt;
  }

  public Boolean getOperatorCanSeeRiskAnswers() {
    return operatorCanSeeRiskAnswers;
  }

  public String getDolpUpdatedAt() {
    return dolpUpdatedAt;
  }

  public String getLongitude() {
    return longitude;
  }

  public String getLocationBlockedAt() {
    return locationBlockedAt;
  }

  public Integer getSure() {
    return sure;
  }

  public String getLocationPrivacyAgreedAt() {
    return locationPrivacyAgreedAt;
  }

  public String getDeactivatedAt() {
    return deactivatedAt;
  }

  public String getConclusionText() {
    return conclusionText;
  }

  public String getConclusionDate() {
    return conclusionDate;
  }

  public String getConclusionMethod() {
    return conclusionMethod;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getDeactivationReason() {
    return deactivationReason;
  }

  public String getCity() {
    return city;
  }

  public Boolean getLocationPolicyAgreed() {
    return locationPolicyAgreed;
  }
}
