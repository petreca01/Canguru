package br.com.taqtile.android.app.domain.followcard;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.followcard.mappers.FollowCardConsultationsResultToViewModelMapper;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardConsultationsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardConsultationsUseCase extends BaseUseCase<List<FollowCardConsultationsViewModel>, Void,
  FollowCardRepository> {

  public FollowCardConsultationsUseCase(FollowCardRepository followCardRepository) {
    super(followCardRepository);
  }

  @Override
  public Observable<List<FollowCardConsultationsViewModel>> execute(Void aVoid) {
    return getRepository().fetchFollowCardConsultations()
      .map(FollowCardConsultationsResultToViewModelMapper::perform);
  }
}
