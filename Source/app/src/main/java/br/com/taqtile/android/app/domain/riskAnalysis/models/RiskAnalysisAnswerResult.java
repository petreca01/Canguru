package br.com.taqtile.android.app.domain.riskAnalysis.models;

import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by taqtile on 07/04/17.
 */

public class RiskAnalysisAnswerResult {
  private UserResult user;

  private RiskAnalysisQuestionsResult questions;

  public RiskAnalysisAnswerResult(UserResult userResult, RiskAnalysisQuestionsResult riskAnalysisQuestionsResult) {
    this.user = userResult;
    this.questions = riskAnalysisQuestionsResult;
  }

  public UserResult getUser() {
    return user;
  }

  public RiskAnalysisQuestionsResult getQuestions() {
    return questions;
  }
}
