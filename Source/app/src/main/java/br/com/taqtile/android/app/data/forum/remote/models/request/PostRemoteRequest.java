package br.com.taqtile.android.app.data.forum.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 4/17/17.
 */

public class PostRemoteRequest extends BaseRemoteRequest {

  @RequestName("post_id")
  private String postId;

  @RequestName("reply_id")
  private String replyId;

  @RequestName("reply")
  private String reply;

  @RequestName("dislike")
  private Integer dislike;

  public PostRemoteRequest(String postId, String replyId, String reply, Integer dislike) {
    this.postId = postId;
    this.replyId = replyId;
    this.reply = reply;
    this.dislike = dislike;
  }
}
