package br.com.taqtile.android.app.data.birthPlan.remote;

import br.com.taqtile.android.app.data.birthPlan.remote.models.request.RemoveAnswerRemoteRequest;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest;
import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class BirthPlanRemoteDataSource extends BaseRemoteDataSource {
  private Observable<BirthPlanServices> services;

  public BirthPlanRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(BirthPlanServices.class);
  }

  public Observable<BaseRemoteResponse<List<QuestionItemRemoteResponse>>> listQuestionsAndAnswers(
    ViewModeRemoteRequest listQuestionsAndAnswersRequestModel) {
    return performRequest(services.flatMap(services -> services.listQuestionsAndAnswers(
      listQuestionsAndAnswersRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<QuestionItemRemoteResponse>>> answer(
    AnswerQuestionnairesRemoteRequest answersRequestModel) {
    return performRequest(
      services.flatMap(services -> services.answer(answersRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<QuestionItemRemoteResponse>>> removeAnswer(
    RemoveAnswerRemoteRequest removeAnswerRequestModel) {
    return performRequest(
      services.flatMap(services -> services.removeAnswer(removeAnswerRequestModel.getFormData())));
  }
}
