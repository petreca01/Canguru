package br.com.taqtile.android.app.data.birthPlan.mappers;

import br.com.taqtile.android.app.data.birthPlan.remote.models.request.RemoveAnswerRemoteRequest;
import br.com.taqtile.android.app.domain.birthPlan.models.RemoveAnswerParams;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class RemoveAnswerParamsToRemoveAnswerRemoteRequestMapper {

  public static RemoveAnswerRemoteRequest perform(RemoveAnswerParams removeAnswerParams) {
    return new RemoveAnswerRemoteRequest(removeAnswerParams.getAnswerId());
  }
}
