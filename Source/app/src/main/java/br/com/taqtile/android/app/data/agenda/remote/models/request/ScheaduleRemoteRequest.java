package br.com.taqtile.android.app.data.agenda.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/21/17.
 */

public class ScheaduleRemoteRequest extends BaseRemoteRequest {
    @RequestName("agenda_id")
    private Integer agendaId;

    @RequestName("isCompromisso")
    private Integer isAppointment;

    @RequestName("data")
    private String date;

    @RequestName("observacao")
    private String observation;

    @RequestName("nome")
    private String name;

    @RequestName("tipo_id")
    private Integer typeId;

  public ScheaduleRemoteRequest(Integer agendaId, Integer isAppointment, String date,
                                String observation, String name, Integer typeId) {
    this.agendaId = agendaId;
    this.isAppointment = isAppointment;
    this.date = date;
    this.observation = observation;
    this.name = name;
    this.typeId = typeId;
  }
}
