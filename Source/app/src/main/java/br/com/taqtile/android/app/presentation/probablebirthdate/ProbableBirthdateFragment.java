package br.com.taqtile.android.app.presentation.probablebirthdate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 19/07/17.
 */

public class ProbableBirthdateFragment extends AppBaseFragment {

  @BindView(R.id.fragment_probable_birthdate_text)
  CustomTextView probableBirthdateText;

  private CustomToolbar customToolbar;
  private NavigationManager navigationManager;
  private ProbableBirthdateContract.Presenter presenter;

  public static ProbableBirthdateFragment newInstance() {
    return new ProbableBirthdateFragment();
  }

  public ProbableBirthdateFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_probable_birthdate, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setProbableBirthdateText();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_probable_birthdate_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setProbableBirthdateText(){
    this.probableBirthdateText.setText(Html.fromHtml(getString(R.string.fragment_probable_birthdate_text)));
  }

  public void setNavigationManager(NavigationManager navigationManager){
    this.navigationManager = navigationManager;
  }

  public void setPresenter(ProbableBirthdateContract.Presenter presenter){
    this.presenter = presenter;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_probable_birthdate),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
