package br.com.taqtile.android.app.presentation.communitypostdetail;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.widget.LinearLayout;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment;
import br.com.taqtile.android.app.presentation.common.post.PostDetailContract;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 19/04/17.
 */

public class CommunityPostDetailFragment extends PostDetailFragment {

  protected CommunityPostDetailContract.Presenter communityPresenter;
  public static CommunityPostDetailFragment newInstance() {
    return new CommunityPostDetailFragment();
  }
  public void setPresenter(PostDetailContract.Presenter presenter) {
    super.setPresenter(presenter);
  }
  public void setCommunityPresenter(CommunityPostDetailContract.Presenter communityPresenter) {
    this.communityPresenter = communityPresenter;
  }

  @Override
  public void onProfileClicked(Integer profileId, Integer postId) {
    if ((postId.equals(this.postId) || isReplyFromThePoster(profileId)) && fromProfile) {
      this.getActivity().finish();
    } else {
      navigationManager.showPublicProfileUI(profileId, null);
    }
  }

  @Override
  protected void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels, Integer itemId, boolean alreadyLiked) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_like),
      getString(R.string.analytics_label_user_liked)
    ));

    super.onItemLikedSuccess(listingsViewModels, itemId, alreadyLiked);
  }

  @Override
  protected void onPostNewCommentSuccess(List<ListingsViewModel> listingsViewModels) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_comment),
      getString(R.string.analytics_label_user_replied_post)
    ));

    super.onPostNewCommentSuccess(listingsViewModels);
  }

  @Override
  public void onDeleteComment(Integer commentId){
    Subscription deleteCommentSubscription = communityPresenter.deleteComment(this.postId, commentId)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onDeleteCommentSuccess,
        this::onDeleteCommentFailure
      );
    compositeSubscription.add(deleteCommentSubscription);
  }
  protected void onDeleteCommentSuccess(String message) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_reply_delete),
      getString(R.string.analytics_label_user_deleted_reply)
    ));

    super.onDeleteCommentSuccess();
  }

  @Override
  public void onDeletePost(Integer postId){
    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_channel_post_detail_delete_post_dialog_title))
      .setMessage(getString(R.string.fragment_channel_post_detail_delete_post_dialog_message))
      .setPositiveButton(getString(R.string.delete), (dialog, which) -> {
          showLoading();
          Subscription subscription = communityPresenter.deletePost(postId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
              this::onDeletePostSuccess,
              this::onDeletePostFailure);
          compositeSubscription.add(subscription);
        }
      )
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> { }) // do nothing
      .show();
    setupAlertDialog(alert);
  }
  @Override
  protected void onDeletePostSuccess(String message) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_delete),
      getString(R.string.analytics_label_user_deleted)
    ));

    super.onDeletePostSuccess(message);
  }

  @Override
  public void onReportPost(Integer postId) {
    final CustomSimpleEditText input = getEditTextForDialog();
    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_channel_post_detail_report_post_dialog_title))
      .setMessage(getString(R.string.fragment_channel_post_detail_report_post_dialog_message))
      .setView(input)
      .setPositiveButton(getString(R.string.send), (dialog, which) -> {
          Subscription subscription = communityPresenter.reportPost(postId, input.getEditableText().toString())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
              this::onReportPostSuccess,
              this::onReportPostFailure
            );
          compositeSubscription.add(subscription);
        })
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {}) // do nothing
      .show();
    setupAlertDialog(alert);
  }
  @Override
  protected void onReportPostSuccess(EmptyResult emptyResult){
    showSnackbar(getString(R.string.fragment_post_community_report_success), Snackbar.LENGTH_LONG);

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_warn),
      getString(R.string.analytics_label_user_warned)
    ));
  }

  private CustomSimpleEditText getEditTextForDialog() {
    final CustomSimpleEditText input = new CustomSimpleEditText(getContext());
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
      LinearLayout.LayoutParams.MATCH_PARENT,
      LinearLayout.LayoutParams.MATCH_PARENT);
    input.setLayoutParams(params);

    return input;
  }
  private boolean isReplyFromThePoster(Integer profileId) {
    return ((SocialFeedCommunityImplViewModel) postList.get(0)).getUserId().equals(profileId);
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_community_post_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
