package br.com.taqtile.android.app.presentation.gestationconclusion;

import java.util.List;

import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionQuestionItemResult;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionViewModel;
import br.com.taqtile.android.app.domain.gestationconclusion.ListGestationConclusionAnswersUseCase;
import br.com.taqtile.android.app.domain.gestationconclusion.SendGestationConclusionQAUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import br.com.taqtile.android.app.presentation.gestationconclusion.mappers.GestationConclusionQuestionItemResultListToViewModelListMapper;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import rx.Observable;

import static br.com.taqtile.android.app.support.Injection.provideListGestationConclusionAnswersUseCase;
import static br.com.taqtile.android.app.support.Injection.provideSendGestationConclusionQAUseCase;

/**
 * Created by taqtile on 07/07/17.
 */

public class GestationConclusionPresenter implements GestationConclusionContract.Presenter{

  private ListGestationConclusionAnswersUseCase listGestationConclusionAnswersUseCase;
  private SendGestationConclusionQAUseCase sendGestationConclusionQAUseCase;

  public GestationConclusionPresenter() {
    this.listGestationConclusionAnswersUseCase = provideListGestationConclusionAnswersUseCase();
    this.sendGestationConclusionQAUseCase = provideSendGestationConclusionQAUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<GestationConclusionViewModel> fetchGestationConclusionQuestionnaire() {
    return this.map(listGestationConclusionAnswersUseCase.execute(null));
  }

  @Override
  public Observable<GestationConclusionViewModel> sendGestationConclusionQuestionnaireEmail() {
    return this.map(listGestationConclusionAnswersUseCase.execute(null));
  }

  @Override
  public Observable<GestationConclusionViewModel> saveAnswers(List<AnswerParams> answerParams, boolean finishPregnancy) {
    return this.map(sendGestationConclusionQAUseCase.execute(new AnswerQuestionnairesParams(answerParams, finishPregnancy)));
  }

  private Observable<GestationConclusionViewModel> map(Observable<List<GestationConclusionQuestionItemResult>> questionItemsResult){
    return questionItemsResult
      .map(result -> {
        try {
          return new GestationConclusionViewModel(
            GestationConclusionQuestionItemResultListToViewModelListMapper.perform(result),
            GestationConclusionQuestionItemResultListToViewModelListMapper.perform(answeredQuestions(result)));
        } catch (DomainError domainError) {
          domainError.printStackTrace();
          return null;
        }
      });
  }

  private List<GestationConclusionQuestionItemResult> answeredQuestions(List<GestationConclusionQuestionItemResult> questionItemResults) {
    return StreamSupport.stream(questionItemResults)
      .filter(question -> question.getAnswer() != null && question.getAnswer().getAnswer() != null)
      .collect(Collectors.toList());
  }


}
