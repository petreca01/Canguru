package br.com.taqtile.android.app.data.account.remote.models.request;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordRemoteRequest extends BaseRemoteRequest{

    private String email;

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
