package br.com.taqtile.android.app.data.exams.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/22/17.
 */

public class ExamsInfoItemRemoteResponse {
    @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("operator_id")
    private Integer operatorId;

    @SerializedName("operator")
    private String operator;

    @SerializedName("operator_can_see_risk_answers")
    private Boolean canOperatorSeeRiskAnswers;

    @SerializedName("nome")
    private String name;

    @SerializedName("about")
    private String about;

    @SerializedName("profile_picture")
    private String profilePicture;

    @SerializedName("cover_picture")
    private String coverPicture;

    @SerializedName("baby_name")
    private String babyName;

    @SerializedName("cpf")
    private String cpf;

    @SerializedName("user_birthdate")
    private String userBirthdate;

    @SerializedName("email")
    private String email;

    @SerializedName("celular")
    private String cellphone;

    @SerializedName("state")
    private String state;

    @SerializedName("zip_code")
    private String zipCode;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("token")
    private String token;

    @SerializedName("dum")
    private String dum;

    @SerializedName("dolp_updated_at")
    private String dolpUpdatedAt;

    @SerializedName("sure")
    private Integer sure;

    @SerializedName("ig_semanas")
    private Integer igWeaks;

    @SerializedName("ig_dias")
    private Integer igDays;

    @SerializedName("schedule_engagement")
    private Integer scheduleEngagement;

    @SerializedName("risk")
    private Integer risk;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("deactivated_at")
    private String deactivated_at;

    @SerializedName("deactivation_reason")
    private String deactivation_reason;

    @SerializedName("conclusion_date")
    private String conclusion_date;

    @SerializedName("conclusion_text")
    private String conclusion_text;

    @SerializedName("conclusion_method")
    private String conclusion_method;

    @SerializedName("pregnancy_dates_updated_at")
    private String pregnancy_dates_updated_at;

    @SerializedName("location_blocked_at")
    private String location_blocked_at;

    @SerializedName("location_privacy_agreed_at")
    private String locationPrivacyAgreedAt;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("operator_highlight")
    private Boolean operatorHighlight;

    @SerializedName("trimestre")
    private String quarter;

    @SerializedName("codigo")
    private String code;

    @SerializedName("tipo_id")
    private Integer idType;

    @SerializedName("tipo")
    private Integer type;

    @SerializedName("semana_inicio")
    private String weakBegin;

    @SerializedName("semana_termino")
    private String weakEnd;

    @SerializedName("dia_termino")
    private String dayEnd;

    @SerializedName("descricao")
    private String description;

    @SerializedName("descricao_imagem_url")
    private String descriptionImagemUrl;

    @SerializedName("descricao_extensa")
    private String fullDescription;

    @SerializedName("opcional")
    private Integer optional;

    @SerializedName("observacao")
    private String observation;

    @SerializedName("realizado")
    private Integer accomplished;

    @SerializedName("marcacao_data")
    private String dataMark;

    @SerializedName("marcacao_observacao")
    private String observationMark;

    @SerializedName("isCompromisso")
    private Integer isAppointment;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public String getUserType() {
        return userType;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public String getOperator() {
        return operator;
    }

    public Boolean getCanOperatorSeeRiskAnswers() {
        return canOperatorSeeRiskAnswers;
    }

    public String getName() {
        return name;
    }

    public String getAbout() {
        return about;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getCoverPicture() {
        return coverPicture;
    }

    public String getBabyName() {
        return babyName;
    }

    public String getCpf() {
        return cpf;
    }

    public String getUserBirthdate() {
        return userBirthdate;
    }

    public String getEmail() {
        return email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public String getState() {
        return state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getToken() {
        return token;
    }

    public String getDum() {
        return dum;
    }

    public String getDolpUpdatedAt() {
        return dolpUpdatedAt;
    }

    public Integer getSure() {
        return sure;
    }

    public Integer getIgWeaks() {
        return igWeaks;
    }

    public Integer getIgDays() {
        return igDays;
    }

    public Integer getScheduleEngagement() {
        return scheduleEngagement;
    }

    public Integer getRisk() {
        return risk;
    }

    public Boolean getActive() {
        return active;
    }

    public String getDeactivated_at() {
        return deactivated_at;
    }

    public String getDeactivation_reason() {
        return deactivation_reason;
    }

    public String getConclusion_date() {
        return conclusion_date;
    }

    public String getConclusion_text() {
        return conclusion_text;
    }

    public String getConclusion_method() {
        return conclusion_method;
    }

    public String getPregnancy_dates_updated_at() {
        return pregnancy_dates_updated_at;
    }

    public String getLocation_blocked_at() {
        return location_blocked_at;
    }

    public String getLocationPrivacyAgreedAt() {
        return locationPrivacyAgreedAt;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public Boolean getOperatorHighlight() {
        return operatorHighlight;
    }

    public String getQuarter() {
        return quarter;
    }

    public String getCode() {
        return code;
    }

    public Integer getIdType() {
        return idType;
    }

    public Integer getType() {
        return type;
    }

    public String getWeakBegin() {
        return weakBegin;
    }

    public String getWeakEnd() {
        return weakEnd;
    }

    public String getDayEnd() {
        return dayEnd;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionImagemUrl() {
        return descriptionImagemUrl;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public Integer getOptional() {
        return optional;
    }

    public String getObservation() {
        return observation;
    }

    public Integer getAccomplished() {
        return accomplished;
    }

    public String getDataMark() {
        return dataMark;
    }

    public String getObservationMark() {
        return observationMark;
    }

    public Integer getIsAppointment() {
        return isAppointment;
    }
}
