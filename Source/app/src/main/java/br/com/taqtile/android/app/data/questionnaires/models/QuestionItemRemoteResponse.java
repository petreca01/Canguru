package br.com.taqtile.android.app.data.questionnaires.models;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.taqtile.android.app.data.questionnaires.mappers.QuestionItemRemoteResponseToQuestionItemResultMapper;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;

/**
 * Created by taqtile on 3/24/17.
 */

public class QuestionItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("question")
    private String question;

    @SerializedName("description")
    private String description;

    @SerializedName("answer_type")
    private String answerType;

    @SerializedName("answers")
    private JsonElement answers;

    @SerializedName("unlocks_questions")
    private Boolean unlocksQuestions;

    @SerializedName("depends_on_question_id")
    private Integer dependsOnQuestionId;

    @SerializedName("answer_unlock_trigger")
    private AnswerUnlockTriggerRemoteResponse answerUnlockTrigger;

    @SerializedName("answer")
    private AnswerItemRemoteResponse answer;

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getDescription() {
        return description;
    }

    public String getAnswerType() {
        return answerType;
    }

  /**
   * Returning {@link JsonElement} as Response from API returns a dynamic object depending on the type of
   * question.
   *
   * If type is suppose to be a list of alternatives, the return is a string array with the possible
   * values, like the following:
   *
   * "answers": [
   * "Sim",
   * "Sim, mas não durante a gravidez",
   * "Não"
   * ],
   *
   * If  type is supposed to be a range of possible values, the return is an object with
   * 'min' and 'max' properties like the following:
   *
   * "answers": {
   * "min": 0,
   * "max": 19
   * }
   *
   * In this case, we let the casting of the actual response to an object to the mapper class,
   * leaving the property as {@link JsonElement} type to allow future checking on what was actually
   * returned.
   *
   * Check {@link QuestionItemRemoteResponseToQuestionItemResultMapper#perform(QuestionItemRemoteResponse)}
   * in order to see how the mapping is currently done.
   *
   * @return Either {@link List<String>} of an {@link RangedAnswerResult} object as {@link JsonElement}
   */
  public JsonElement getAnswers() {
        return answers;
    }

    public Boolean getUnlocksQuestions() {
        return unlocksQuestions;
    }

    public Integer getDependsOnQuestionId() {
        return dependsOnQuestionId;
    }

    public AnswerUnlockTriggerRemoteResponse getAnswerUnlockTrigger() {
        return answerUnlockTrigger;
    }

    public AnswerItemRemoteResponse getAnswer() {
        return answer;
    }
}
