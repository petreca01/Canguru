package br.com.taqtile.android.app.data.city.geocondingAPI.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 6/5/17.
 */

public class GeometryGeocodingAPIResponse {

  @SerializedName("location")
  private GeographicCoordinateGeocodingAPIResponse geographicCoordinate;

  public GeographicCoordinateGeocodingAPIResponse getGeographicCoordinate() {
    return geographicCoordinate;
  }
}
