package br.com.taqtile.android.app.domain.forum;

import java.util.List;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.channel.mappers.ForumPostAndRepliesResultToListingsViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class ForumPostDataUseCase extends BaseUseCase<List<ListingsViewModel> , PostParams, ForumRepository> {


  public ForumPostDataUseCase(ForumRepository forumRepository) {
    super(forumRepository);
  }

  @Override
  public Observable<List<ListingsViewModel>> execute(PostParams postParams) {
    return getRepository().getPostAndReplies(postParams)
      .map(ForumPostAndRepliesResultToListingsViewModelMapper::perform);
  }
}
