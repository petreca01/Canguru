package br.com.taqtile.android.app.domain.channel.models;

import java.util.List;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelListResult {

  private List<ChannelWithCategoryResult> channelList;

  public ChannelListResult(List<ChannelWithCategoryResult> channelList) {
    this.channelList = channelList;
  }

  public List<ChannelWithCategoryResult> getChannelList(){
    return channelList;
  }

}
