package br.com.taqtile.android.app.domain.clinicalConditions.models;

/**
 * Created by taqtile on 4/12/17.
 */

public class ReportConditionParams {
  private Integer conditionId;

  public ReportConditionParams(Integer conditionId) {
    this.conditionId = conditionId;
  }

  public Integer getConditionId() {
    return conditionId;
  }
}
