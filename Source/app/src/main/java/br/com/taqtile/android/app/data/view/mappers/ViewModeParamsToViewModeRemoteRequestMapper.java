package br.com.taqtile.android.app.data.view.mappers;

import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;

/**
 * Created by taqtile on 07/04/17.
 */

public class ViewModeParamsToViewModeRemoteRequestMapper {

  public static ViewModeRemoteRequest perform(ViewModeParams viewModeParams) {
    return new ViewModeRemoteRequest(viewModeParams.getView(), viewModeParams.getEmail());
  }
}
