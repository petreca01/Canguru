package br.com.taqtile.android.app.presentation.home;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 7/6/16.
 */

public interface HomeContract {

  interface Presenter extends BasePresenter {
    Observable<List<SocialFeedViewModel>> fetchCards(boolean reload);
    Observable<List<ListingsViewModel>> likeCommunityPost(Integer postId, boolean alreadyLiked);
    Observable<List<ListingsViewModel>> likeChannelPost(Integer postId, boolean alreadyLiked);
  }

}
