package br.com.taqtile.android.app.presentation.manageprofessionals;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 25/04/17.
 */

public interface ManageProfessionalsContract {

  interface Presenter extends BasePresenter {
    Observable<String> authorizeProfessional(String professionalId);
    Observable<EmptyResult> revokeProfessional(String professionalId);
  }

}
