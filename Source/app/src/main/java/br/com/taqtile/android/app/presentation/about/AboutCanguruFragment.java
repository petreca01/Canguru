package br.com.taqtile.android.app.presentation.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 20/07/17.
 */

public class AboutCanguruFragment extends AppBaseFragment {

  @BindView(R.id.fragment_about_canguru_body)
  CustomTextViewWithHTML aboutText;

  private AboutCanguruContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  public static AboutCanguruFragment newInstance(){ return new AboutCanguruFragment();}

  public AboutCanguruFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_about_canguru, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupAboutText();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_static_screen_about_canguru_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setupAboutText(){
    this.aboutText.setTextWithHTML(getResources().getString(R.string.fragment_static_screen_about_canguru_body));
  }

  public void setPresenter(AboutCanguruContract.Presenter presenter){
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager){
    this.navigationManager = navigationManager;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_about),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}

