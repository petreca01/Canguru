package br.com.taqtile.android.app.domain.weeklycontent.mapper;

import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentResult;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentResultToViewModel {

  public static WeeklyContentViewModel perform(WeeklyContentResult weeklyContentResult) {
    return new WeeklyContentViewModel(weeklyContentResult.getWeek(),
      weeklyContentResult.getImageUrl(), weeklyContentResult.getContent());
  }
}
