package br.com.taqtile.android.app.data.account.remote.Mappers;

import android.support.annotation.NonNull;

import br.com.taqtile.android.app.data.account.remote.models.request.FacebookSignInRemoteRequest;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInWithFacebookParams;

/**
 * Created by taqtile on 3/29/17.
 */

public class AccountSignInWithFacebookToFacebookAccountRemoteRequestMapper {

  public static FacebookSignInRemoteRequest map(@NonNull AccountSignInWithFacebookParams params) {
    return new FacebookSignInRemoteRequest(params.getFacebookToken());
  }
}
