package br.com.taqtile.android.app.data.birthPlan;

import java.util.List;

import br.com.taqtile.android.app.data.birthPlan.mappers.RemoveAnswerParamsToRemoveAnswerRemoteRequestMapper;
import br.com.taqtile.android.app.data.birthPlan.remote.BirthPlanRemoteDataSource;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.mappers.AnswerQuestionnairesParamsToAnswerQuestionnairesRemoteRequestMapper;
import br.com.taqtile.android.app.data.questionnaires.mappers.QuestionItemRemoteResponseListToQuestionItemResultListMapper;
import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.view.mappers.ViewModeParamsToViewModeRemoteRequestMapper;
import br.com.taqtile.android.app.domain.birthPlan.models.RemoveAnswerParams;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import rx.Observable;


/**
 * Created by taqtile on 3/24/17.
 */

public class BirthPlanRepository extends BaseRepository<BirthPlanRemoteDataSource, Void> {

  public BirthPlanRepository(BirthPlanRemoteDataSource birthPlanRemoteDataSource) {
    super(birthPlanRemoteDataSource, null);
  }

  public Observable<List<QuestionItemResult>> listQuestionsAndAnswers(ViewModeParams viewModeParams) {
    return performMap(getRemoteDataSource().listQuestionsAndAnswers(ViewModeParamsToViewModeRemoteRequestMapper.perform(viewModeParams)));
  }

  public Observable<List<QuestionItemResult>> answer(AnswerQuestionnairesParams answerQuestionnairesParams) {
    return performMap(getRemoteDataSource().answer(AnswerQuestionnairesParamsToAnswerQuestionnairesRemoteRequestMapper
      .perform(answerQuestionnairesParams)));
  }

  public Observable<List<QuestionItemResult>> removeAnswer(RemoveAnswerParams removeAnswerParams) {
    return performMap(getRemoteDataSource().removeAnswer(RemoveAnswerParamsToRemoveAnswerRemoteRequestMapper.perform(removeAnswerParams)));
  }

  private Observable<List<QuestionItemResult>> performMap(
    Observable<BaseRemoteResponse<List<QuestionItemRemoteResponse>>> remoteResponseObservable) {
    return remoteResponseObservable
      .map(result -> QuestionItemRemoteResponseListToQuestionItemResultListMapper.perform(result.getResult()));
  }
}
