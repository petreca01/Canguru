package br.com.taqtile.android.app.presentation.maternitycitysearch.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/12/17.
 */

public class CityCell extends LinearLayout {

  @BindView(R.id.component_city_cell_root)
  LinearLayout root;
  @BindView(R.id.component_city_cell_text)
  CustomTextView city;

  private CityCellListener listener;

  public CityCell(Context context, CityCellListener listener) {
    super(context);
    init(context);
    this.listener = listener;
  }

  public CityCell(Context context) {
    super(context);
    init(context);
  }

  public CityCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public CityCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_city_cell, this, true);
    ButterKnife.bind(this, v);
  }

  public void setCity(String city) {
    this.city.setText(city);
    root.setOnClickListener(view -> {
      if(listener != null) {
        listener.onCityCellClicked(city);
      }
    });
  }

  public interface CityCellListener {
    void onCityCellClicked(String city);
  }
}
