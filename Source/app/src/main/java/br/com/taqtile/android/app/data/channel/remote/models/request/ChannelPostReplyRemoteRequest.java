package br.com.taqtile.android.app.data.channel.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostReplyRemoteRequest extends BaseRemoteRequest {
    @RequestName("post_id")
    private Integer postId;

    private String reply;

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }
}
