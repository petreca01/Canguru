package br.com.taqtile.android.app.data.channel.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelCategoryListRemoteResponse {
  @SerializedName("sections")
  List<ChannelListRemoteResponse> sections;

  public List<ChannelListRemoteResponse> getSections(){
    return sections;
  }
}
