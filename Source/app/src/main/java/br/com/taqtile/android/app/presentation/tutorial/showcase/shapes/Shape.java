package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by taqtile on 5/23/17.
 */

public interface Shape {

  public void drawOnCanvas(Canvas canvas);
}
