package br.com.taqtile.android.app.support.analytics;

/**
 * Created by felipesabino on 5/24/17.
 */

public class AnalyticsToolBox {

  public void trackEvent(AnalyticsEvent event) {
    GoogleAnalyticsTracker.trackEvent(event);
  }

  public void trackUser(String userId) {
    GoogleAnalyticsTracker.trackUser(userId);
  }
}
