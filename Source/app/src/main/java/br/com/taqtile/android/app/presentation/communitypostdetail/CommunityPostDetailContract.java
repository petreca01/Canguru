package br.com.taqtile.android.app.presentation.communitypostdetail;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 4/24/17.
 */

public interface CommunityPostDetailContract {

  interface Presenter extends BasePresenter {
    Observable<String> deleteComment(Integer postId, Integer commentId);
    Observable<String> deletePost(Integer postId);
    Observable<EmptyResult> reportPost(Integer postId, String reason);

  }

}
