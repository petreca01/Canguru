package br.com.taqtile.android.app.data.healthOperators.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class SearchHealthOperatorRemoteRequest extends BaseRemoteRequest {
    private String search;

  public SearchHealthOperatorRemoteRequest(String search) {
    this.search = search;
  }
}
