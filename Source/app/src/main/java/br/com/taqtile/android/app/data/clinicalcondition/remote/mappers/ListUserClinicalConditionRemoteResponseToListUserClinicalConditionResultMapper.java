package br.com.taqtile.android.app.data.clinicalcondition.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.UserClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.domain.clinicalConditions.models.UserClinicalConditionResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/12/17.
 */

public class ListUserClinicalConditionRemoteResponseToListUserClinicalConditionResultMapper {
  public static List<UserClinicalConditionResult> perform(List<UserClinicalConditionRemoteResponse> responseList) {

    return StreamSupport.stream(responseList)
      .map(UserClinicalConditionRemoteResponseToUserClinicalConditionResultMapper::perform)
      .collect(Collectors.toList());
  }
}
