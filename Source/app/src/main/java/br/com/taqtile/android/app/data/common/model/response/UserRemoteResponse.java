package br.com.taqtile.android.app.data.common.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/15/17.
 */

public class UserRemoteResponse {

  @SerializedName("schedule_engagement")
  private Integer scheduleEngagement;

  @SerializedName("pregnancy_dates_updated_at")
  private String pregnancyDatesUpdatedAt;

  @SerializedName("baby_name")
  private String babyName;

  @SerializedName("operator_id")
  private Integer operatorId;

  @SerializedName("about")
  private String about;

  @SerializedName("city")
  private String city;

  @SerializedName("state")
  private String state;

  @SerializedName("operator_can_see_risk_answers")
  private Boolean operatorCanSeeRiskAnswers;

  @SerializedName("operator_document")
  private String operatorDocument;

  @SerializedName("user_type")
  private String userType;

  @SerializedName("risk")
  private Integer risk;

  @SerializedName("id")
  private Integer id;

  @SerializedName("token")
  private String token;

  @SerializedName("created_at")
  private String createdAt;

  @SerializedName("zip_code")
  private String zipCode;

  @SerializedName("dolp_updated_at")
  private String dolpUpdatedAt;

  @SerializedName("cpf")
  private String cpf;

  @SerializedName("longitude")
  private String longitude;

  @SerializedName("cover_picture")
  private String coverPicture;

  @SerializedName("location_blocked_at")
  private String locationBlockedAt;

  @SerializedName("sure")
  private Integer sure;

  @SerializedName("location_privacy_agreed_at")
  private String locationPrivacyAgreedAt;

  @SerializedName("deactivated_at")
  private String deactivatedAt;

  @SerializedName("profile_picture")
  private String profilePicture;

  @SerializedName("user_birthdate")
  private String userBirthdate;

  @SerializedName("operator")
  private String operator;

  @SerializedName("updated_at")
  private String updatedAt;

  @SerializedName("ig_trimestre")
  private Integer igTrimesters;

  @SerializedName("ig_mes")
  private Integer igMonths;

  @SerializedName("ig_semanas")
  private Integer igWeeks;

  @SerializedName("conclusion_text")
  private String conclusionText;

  @SerializedName("loaded")
  private Boolean loaded;

  @SerializedName("dum")
  private String dum;

  @SerializedName("dpp")
  private String dpp;

  @SerializedName("dpp_based")
  private String dppBased;

  @SerializedName("email")
  private String email;

  @SerializedName("conclusion_date")
  private String conclusionDate;

  @SerializedName("conclusion_method")
  private String conclusionMethod;

  @SerializedName("active")
  private Boolean active;

  @SerializedName("nome")
  private String name;

  @SerializedName("latitude")
  private String latitude;

  @SerializedName("deactivated_reason")
  private String deactivationReason;

  @SerializedName("ig_dias")
  private Integer igDays;

  @SerializedName("celular")
  private String cellphoneNumber;

  @SerializedName("location_privacy_agreed")
  private Boolean locationPolicyAgreed;

  public UserRemoteResponse(String profilePicture, String name) {
    this.profilePicture = profilePicture;
    this.name = name;
  }

  public UserRemoteResponse(String zipCode, String cpf, String cellphoneNumber) {
    this.zipCode = zipCode;
    this.cpf = cpf;
    this.cellphoneNumber = cellphoneNumber;
  }

  public Integer getScheduleEngagement() {
    return scheduleEngagement;
  }

  public String getPregnancyDatesUpdatedAt() {
    return pregnancyDatesUpdatedAt;
  }

  public String getBabyName() {
    return babyName;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public String getAbout() {
    return about;
  }

  public String getState() {
    return state;
  }

  public String getCity() {
    return city;
  }

  public Boolean getOperatorCanSeeRiskAnswers() {
    return operatorCanSeeRiskAnswers;
  }

  public String getOperatorDocument() {
    return operatorDocument;
  }

  public String getUserType() {
    return userType;
  }

  public Integer getRisk() {
    return risk;
  }

  public Integer getId() {
    return id;
  }

  public String getToken() {
    return token;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getDolpUpdatedAt() {
    return dolpUpdatedAt;
  }

  public String getCpf() {
    return cpf;
  }

  public String getLongitude() {
    return longitude;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public String getLocationBlockedAt() {
    return locationBlockedAt;
  }

  public Integer getSure() {
    return sure;
  }

  public String getLocationPrivacyAgreedAt() {
    return locationPrivacyAgreedAt;
  }

  public String getDeactivatedAt() {
    return deactivatedAt;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getUserBirthdate() {
    return userBirthdate;
  }

  public String getOperator() {
    return operator;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public Integer getIgWeeks() {
    return igWeeks;
  }

  public Integer getIgTrimesters() {
    return igTrimesters;
  }

  public Integer getIgMonths() {
    return igMonths;
  }

  public Boolean getLocationPolicyAgreed() {
    return locationPolicyAgreed;
  }

  public String getConclusionText() {
    return conclusionText;
  }

  public Boolean getLoaded() {
    return loaded;
  }

  public String getDum() {
    return dum;
  }

  public String getDpp() {
    return dpp;
  }

  public String getDppBased() {
    return dppBased;
  }

  public String getEmail() {
    return email;
  }

  public String getConclusionDate() {
    return conclusionDate;
  }

  public String getConclusionMethod() {
    return conclusionMethod;
  }

  public Boolean getActive() {
    return active;
  }

  public String getName() {
    return name;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getDeactivationReason() {
    return deactivationReason;
  }

  public Integer getIgDays() {
    return igDays;
  }

  public String getCellphoneNumber() {
    return cellphoneNumber;
  }


}
