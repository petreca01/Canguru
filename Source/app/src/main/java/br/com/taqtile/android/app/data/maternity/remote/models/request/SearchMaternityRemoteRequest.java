package br.com.taqtile.android.app.data.maternity.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class SearchMaternityRemoteRequest extends BaseRemoteRequest {
    private String city;
    private String search;

    public void setCity(String city) {
        this.city = city;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
