package br.com.taqtile.android.app.presentation.profilemenu;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.LogoutUseCase;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by Renato on 4/18/17.
 */

public class ProfileMenuPresenter implements ProfileMenuContract.Presenter {

  private LogoutUseCase logoutUseCase;

  public ProfileMenuPresenter() {
    this.logoutUseCase = Injection.provideLogoutUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<EmptyResult> logout() {
    return logoutUseCase.execute(null);
  }
}
