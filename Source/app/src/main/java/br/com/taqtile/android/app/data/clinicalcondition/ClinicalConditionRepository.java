package br.com.taqtile.android.app.data.clinicalcondition;

import java.util.List;

import br.com.taqtile.android.app.data.clinicalcondition.remote.ClinicalConditionRemoteDataSource;
import br.com.taqtile.android.app.data.clinicalcondition.remote.mappers.ClinicalConditionItemRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.clinicalcondition.remote.mappers.ListUserClinicalConditionRemoteResponseToListUserClinicalConditionResultMapper;
import br.com.taqtile.android.app.data.clinicalcondition.remote.mappers.ReportClinicalConditionRemoteResponseToReportClinicalConditionResultMapper;
import br.com.taqtile.android.app.data.clinicalcondition.remote.mappers.ReportConditionParamsToReportConditionRemoteRequestMapper;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionItemResult;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionResult;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportClinicalConditionResult;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;
import br.com.taqtile.android.app.domain.clinicalConditions.models.UserClinicalConditionResult;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class ClinicalConditionRepository extends BaseRepository<ClinicalConditionRemoteDataSource, Void> {
  public ClinicalConditionRepository(ClinicalConditionRemoteDataSource clinicalConditionRemoteDataSource) {
    super(clinicalConditionRemoteDataSource, null);
  }

  public Observable<List<ClinicalConditionResult>> list() {
    return getRemoteDataSource()
      .list()
      .map(result -> ClinicalConditionItemRemoteResponseToResultMapper.perform(result.getResult()));
  }

  public Observable<ReportClinicalConditionResult> reportClinicalCondition(ReportConditionParams params) {
    return getRemoteDataSource()
      .reportClinicalCondition(ReportConditionParamsToReportConditionRemoteRequestMapper.perform(params))
      .map(result -> ReportClinicalConditionRemoteResponseToReportClinicalConditionResultMapper.perform(result.getResult()));
  }

  public Observable<List<UserClinicalConditionResult>> listUserClinicalCondition() {
    return getRemoteDataSource()
      .listUserClinicalCondition()
      .map(result -> ListUserClinicalConditionRemoteResponseToListUserClinicalConditionResultMapper.perform(result.getResult()));
  }
}
