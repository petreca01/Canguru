package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 3/24/17.
 */

public class ChannelResult {
  private Integer id;
  private String name;
  private String author;
  private String description;
  private String logo;
  private String cover;
  private Boolean active;
  private Integer totalPosts;
  private Integer followers;
  private Boolean following;

  public ChannelResult(Integer id,
                       String name,
                       String author,
                       String description,
                       String logo,
                       String cover,
                       Boolean active,
                       Integer totalPosts,
                       Integer followers,
                       Boolean following) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.description = description;
    this.logo = logo;
    this.cover = cover;
    this.active = active;
    this.totalPosts = totalPosts;
    this.followers = followers;
    this.following = following;
  }

  public ChannelResult(Integer id,
                       String name,
                       String author,
                       String description,
                       String logo, String cover,
                       Boolean active, Integer totalPosts,
                       Integer followers, Boolean following,
                       String category) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.description = description;
    this.logo = logo;
    this.cover = cover;
    this.active = active;
    this.totalPosts = totalPosts;
    this.followers = followers;
    this.following = following;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAuthor() {
    return author;
  }

  public String getDescription() {
    return description;
  }

  public String getLogo() {
    return logo;
  }

  public String getCover() {
    return cover;
  }

  public Boolean getActive() {
    return active;
  }

  public Integer getTotalPosts() {
    return totalPosts;
  }

  public Integer getFollowers() {
    return followers;
  }

  public Boolean getFollowing() {
    return following;
  }

}
