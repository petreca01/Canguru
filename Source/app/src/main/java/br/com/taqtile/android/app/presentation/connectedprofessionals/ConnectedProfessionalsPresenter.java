package br.com.taqtile.android.app.presentation.connectedprofessionals;

import br.com.taqtile.android.app.domain.professional.ListProfessionalsUseCase;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.app.support.Injection;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ConnectedProfessionalsPresenter implements ConnectedProfessionalsContract.Presenter {

  private ListProfessionalsUseCase listProfessionalsUseCase;

  public ConnectedProfessionalsPresenter() {
    listProfessionalsUseCase = Injection.provideListProfessionalsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<ConnectedProfessionalViewModel>> fetchProfessionals() {
    return listProfessionalsUseCase.execute(null);
  }
}
