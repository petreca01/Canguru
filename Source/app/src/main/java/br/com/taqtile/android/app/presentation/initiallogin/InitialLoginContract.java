package br.com.taqtile.android.app.presentation.initiallogin;

import br.com.taqtile.android.app.domain.signin.Models.FacebookLoginResult;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import br.com.taqtile.android.cleanbase.presentation.view.BaseView;
import rx.Observable;

/**
 * Created by taqtile on 11/11/16.
 */

public interface InitialLoginContract {

  interface View extends BaseView<InitialLoginContract.Presenter, NavigationManager> {
  }

  interface Presenter extends BasePresenter {
    Observable<FacebookLoginResult> facebookLoginButtonTapped();
  }
}
