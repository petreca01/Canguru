package br.com.taqtile.android.app.presentation.postcommunity;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.forum.CreatePostUseCase;
import br.com.taqtile.android.app.domain.forum.GetContentSuggestionUseCase;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionParams;
import br.com.taqtile.android.app.domain.forum.models.CreatePostParams;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.presentation.postcommunity.mappers.ContentSuggestionResultToContentSuggestionViewModelMapper;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 11/04/17.
 */

public class PostCommunityPresenter implements PostCommunityContract.Presenter {

  private CreatePostUseCase createPostUseCase;
  private GetContentSuggestionUseCase getContentSuggestionUseCase;

  public PostCommunityPresenter() {
    createPostUseCase = Injection.provideCreatePostUseCase();
    getContentSuggestionUseCase = Injection.provideGetContentSuggestionUseCase();
  }

  @Override
  public Observable<EmptyResult> post(String topic, String message) {

    return createPostUseCase.execute(new CreatePostParams(topic, message))
      .map(newPostResult -> new EmptyResult());
  }

  @Override
  public Observable<List<SearchListingViewModel>> getContentSuggestion(String search, boolean reload) {
    return getContentSuggestionUseCase.execute(new ContentSuggestionParams(search, 1, 10), reload)
      .map(ContentSuggestionResultToContentSuggestionViewModelMapper::perform);
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }
}
