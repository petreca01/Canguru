package br.com.taqtile.android.app.domain.weeklycontent.models;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentParams {

  private String week;

  public WeeklyContentParams(String week) {
    this.week = week;
  }

  public String getWeek() {
    return week;
  }
}
