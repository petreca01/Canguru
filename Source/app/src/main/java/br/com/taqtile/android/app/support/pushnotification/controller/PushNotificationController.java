package br.com.taqtile.android.app.support.pushnotification.controller;

import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 7/11/16.
 */

public class PushNotificationController implements PushNotificationContract.Controller {

    private PushNotificationContract.Service mService;

    public PushNotificationController(@NonNull PushNotificationContract.Service service) {
        mService = checkNotNull(service, "PushNotificationService cannot be null!");
    }

    @Override
    public void simplePushReceived(String message, String title, String action) {
        mService.showNotificationUI(message, title, action);
    }
}
