package br.com.taqtile.android.app.data.notification.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class NotificationCountRemoteResponse {
    @SerializedName("notifications")
    private Integer notifications;

    @SerializedName("inboxes")
    private Integer inboxes;

    public Integer getNotifications() {
        return notifications;
    }

    public Integer getInboxes() {
        return inboxes;
    }
}
