package br.com.taqtile.android.app.domain.professional.mappers;

import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;

/**
 * Created by taqtile on 27/04/17.
 */

public class ProfessionalResultToConnectedProfessionalViewModelMapper {

  public static ConnectedProfessionalViewModel perform(ProfessionalResult professionalResult) {
    boolean authorized = professionalResult.getAuthorizedAt() != null ||
      (professionalResult.getRevokedAt() != null && !professionalResult.getRevokedAt().equalsIgnoreCase(professionalResult.getUpdatedAt()));

    String authorizationDate = professionalResult.getAuthorizedAt();
    if (authorizationDate == null) {
      authorizationDate = professionalResult.getUpdatedAt();
    }

    return new ConnectedProfessionalViewModel(
      String.valueOf(professionalResult.getProfessionalId()),
      professionalResult.getProfessionalName(), authorizationDate,
      authorized);
  }
}
