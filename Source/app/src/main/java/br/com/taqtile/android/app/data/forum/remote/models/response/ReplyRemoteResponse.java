package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/21/17.
 */

public class ReplyRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("forum_post_id")
    private Integer forumPostId;

    @SerializedName("operator_id")
    private Integer operatorId;

    @SerializedName("usuario_id")
    private Integer userId;

    @SerializedName("reply")
    private String reply;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("username")
    private String username;

    @SerializedName("user_picture")
    private String userPicture;

    @SerializedName("likes")
    private Integer likes;

    @SerializedName("liked")
    private Boolean liked;

  public Integer getId() {
    return id;
  }

  public Integer getForumPostId() {
    return forumPostId;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public Integer getUserId() {
    return userId;
  }

  public String getReply() {
    return reply;
  }

  public Boolean getActive() {
    return active;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getUserType() {
    return userType;
  }

  public String getUsername() {
    return username;
  }

  public String getUserPicture() {
    return userPicture;
  }

  public Integer getLikes() {
    return likes;
  }

  public Boolean getLiked() {
    return liked;
  }
}
