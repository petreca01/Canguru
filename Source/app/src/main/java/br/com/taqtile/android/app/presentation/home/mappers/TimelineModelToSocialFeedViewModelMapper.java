package br.com.taqtile.android.app.presentation.home.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.timeline.models.TimelineModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;

/**
 * Created by taqtile on 3/22/17.
 */

public class TimelineModelToSocialFeedViewModelMapper {

  public static List<SocialFeedViewModel> perform(TimelineModel timelineModel) {
    List<SocialFeedViewModel> socialFeedViewModels = new ArrayList<>();

    if (timelineModel == null) {
      return socialFeedViewModels;
    }

    List<SocialFeedViewModel> forumPostViewModelList =
      ForumPostToSocialFeedViewModelMapper.perform(timelineModel.getPostResults());
    List<SocialFeedViewModel> channelViewModelList =
      ChannelPostItemResponseToForumPostMapper.perform(timelineModel.getChannelPosts());

    socialFeedViewModels.addAll(forumPostViewModelList);

    //show 1 channel post between 2 community post
    int position = 2;
    for (int i = 0; i < channelViewModelList.size(); i++){
      if (socialFeedViewModels.size() >= position) {
        socialFeedViewModels.add(position, channelViewModelList.get(i));
        position = position + 3;
      } else{
        // add at end of the list
        socialFeedViewModels.add(channelViewModelList.get(i));
      }
    }
    return socialFeedViewModels;
  }

}
