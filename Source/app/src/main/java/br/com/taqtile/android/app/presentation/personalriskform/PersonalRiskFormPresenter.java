package br.com.taqtile.android.app.presentation.personalriskform;

import android.support.v4.util.Pair;

import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.riskAnalysis.FirstQuestionForGroupUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.SendAnswersUseCase;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by Renato on 4/19/17.
 */

public class PersonalRiskFormPresenter implements PersonalRiskFormContract.Presenter {

  private FirstQuestionForGroupUseCase firstQuestionForGroupUseCase;
  private SendAnswersUseCase sendAnswersUseCase;

  public PersonalRiskFormPresenter() {
    this.sendAnswersUseCase = Injection.provideSendAnswersUseCase();
    this.firstQuestionForGroupUseCase = Injection.provideFirstQuestionForGroupUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<RiskAnalysisQuestionResult> fetchFirstRiskAnalysisQuestionnaire(
    RiskAnalysisQuestionGroupResult params) {
    return firstQuestionForGroupUseCase.execute(params);
  }

  @Override
  public Observable<RiskAnalysisQuestionResult> saveAnswers(Pair<AnswerParams,
    RiskAnalysisQuestionGroupResult> params) {
    return sendAnswersUseCase.execute(params);
  }
}
