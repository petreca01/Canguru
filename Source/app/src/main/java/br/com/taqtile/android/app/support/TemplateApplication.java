package br.com.taqtile.android.app.support;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.squareup.leakcanary.LeakCanary;

import br.com.taqtile.android.app.support.analytics.GoogleAnalyticsTracker;

/**
 * Created by taqtile on 11/7/16.
 */

public class TemplateApplication extends MultiDexApplication {

  private static TemplateApplication instance;

  public TemplateApplication() {
    instance = this;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    setupLeakCanary();
    GoogleAnalyticsTracker.setup(this);
  }

  private void setupLeakCanary() {

    if (LeakCanary.isInAnalyzerProcess(this)) {
      // This process is dedicated to LeakCanary for heap analysis.
      // You should not init your app in this process.
      return;
    }
   LeakCanary.install(this);

  }

  public static Context getContext() {
    return instance;
  }
}
