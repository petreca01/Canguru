package br.com.taqtile.android.app.presentation.maternitycitysearch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.search.CustomSearchView;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.StringScoreFilter;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.MATERNITY_CITY_TAG;

public class MaternityCitySearchFragment extends AppBaseFragment {

  @BindView(R.id.fragment_maternity_search_cities_container)
  FrameLayout searchCitiesContainer;
  @BindView(R.id.fragment_maternity_search_cities_view)
  CustomSearchView searchView;
  @BindView(R.id.fragment_maternity_search_cities_results)
  FrameLayout citiesResultsContainer;
  @BindView(R.id.fragment_maternity_search_cities_recycler_view)
  RecyclerView citiesRecyclerView;

  private MaternitySearchCitiesAdapter citiesAdapter;
  private CustomToolbar customToolbar;
  private List<String> cities;

  private NavigationManager navigationManager;
  private MaternityCitySearchContract.Presenter presenter;

  public static MaternityCitySearchFragment newInstance() {
    return new MaternityCitySearchFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_maternity_search_cities, container, false);
    ButterKnife.bind(this, v);
    return v;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupCitiesRecyclerView();
    setupSearchView();

    searchView.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
  }

  @Override
  public void willAppear() {
    super.willAppear();
    showLoading();
    fetchCities();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_hospital_search_city),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> {
        getActivity().setResult(Activity.RESULT_CANCELED, new Intent());
        getActivity().finish();
      });
      customToolbar.setTitle(getResources()
        .getString(R.string.fragment_maternity_city_search_title), Gravity.LEFT);
    }
  }

  private void fetchCities() {
    showLoading();
    Subscription subscription = presenter.fetchCities()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchCitiesSuccess, this::onFetchCitiesFailure, this::hideLoading);
    compositeSubscription.add(subscription);
  }

  private void onFetchCitiesSuccess(List<String> cities) {
    this.cities.clear();
    this.cities.addAll(cities);
  }

  private void onFetchCitiesFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupCitiesRecyclerView() {
    cities = new ArrayList<>();
    citiesAdapter = new MaternitySearchCitiesAdapter(cities, this::onCityCellClicked);
    citiesRecyclerView.setAdapter(citiesAdapter);
    citiesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    citiesResultsContainer.setVisibility(View.INVISIBLE);
    citiesRecyclerView.setVisibility(View.INVISIBLE);
  }

  private void onCityCellClicked(String city) {
    Intent intent = new Intent();
    intent.putExtra(MATERNITY_CITY_TAG, city);
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
  }

  private void setupSearchView() {
    searchView.setHint(R.string.fragment_maternity_search_cities_hint);
    searchView.requestEditTextFocus();
    searchView.setSubmitSearchListener(text -> {
      orderResults(text);
      SoftKeyboardHelper.hideKeyboard(getContext());
    });
    SoftKeyboardHelper.showKeyboard(getContext());
  }

  private void orderResults(String text) {
    StringScoreFilter.orderCitiesByScore(cities, text);
    setView();
  }

  private void setView() {
    this.citiesAdapter.notifyDataSetChanged();
    if (cities.size() == 0) {
      citiesResultsContainer.setVisibility(View.INVISIBLE);
      citiesRecyclerView.setVisibility(View.INVISIBLE);
    } else {
      citiesResultsContainer.setVisibility(View.VISIBLE);
      citiesRecyclerView.setVisibility(View.VISIBLE);
      citiesRecyclerView.scrollToPosition(0);
    }
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(MaternityCitySearchContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void showLoading() {
    super.showLoading();
    searchCitiesContainer.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    searchCitiesContainer.setVisibility(View.VISIBLE);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    showLoading();
    fetchCities();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_maternity_city_search_placeholder;
  }
}
