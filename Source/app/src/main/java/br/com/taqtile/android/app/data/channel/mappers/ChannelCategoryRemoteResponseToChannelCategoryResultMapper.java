package br.com.taqtile.android.app.data.channel.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelCategoryListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelItemRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelCategoryResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelListResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelWithCategoryResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelCategoryRemoteResponseToChannelCategoryResultMapper {

  public static ChannelCategoryResult perform(ChannelCategoryListRemoteResponse response){

    return new ChannelCategoryResult(ChannelCategoryRemoteResponseToChannelCategoryResultMapper.map(response.getSections()));
  }

  private static ChannelListResult map(List<ChannelListRemoteResponse> sections){
    return new ChannelListResult(ChannelCategoryRemoteResponseToChannelCategoryResultMapper.mapCategories(sections));
  }

  private static List<ChannelWithCategoryResult> mapCategories(List<ChannelListRemoteResponse> channelList){
    return StreamSupport.stream(channelList)
      .map(ChannelCategoryRemoteResponseToChannelCategoryResultMapper::mapCategory)
      .collect(Collectors.toList());
  }

  private static ChannelWithCategoryResult mapCategory(ChannelListRemoteResponse channelListRemoteResponse){
    return new ChannelWithCategoryResult(channelListRemoteResponse.getSectionName(), mapChannels(channelListRemoteResponse.getChannels()));
  }

  private static List<ChannelResult> mapChannels(List<ChannelItemRemoteResponse> channelItemRemoteResponses){
    return StreamSupport.stream(channelItemRemoteResponses)
      .map(ChannelCategoryRemoteResponseToChannelCategoryResultMapper::mapChannel)
      .collect(Collectors.toList());
  }

  private static ChannelResult mapChannel(ChannelItemRemoteResponse channelItemRemoteResponse){
    return new ChannelResult(channelItemRemoteResponse.getId(),
      channelItemRemoteResponse.getName(),
      channelItemRemoteResponse.getAuthor(),
      channelItemRemoteResponse.getDescription(),
      channelItemRemoteResponse.getLogo(),
      channelItemRemoteResponse.getCover(),
      channelItemRemoteResponse.getActive(),
      channelItemRemoteResponse.getTotalPosts(),
      channelItemRemoteResponse.getFollowers(),
      channelItemRemoteResponse.getFollowing()
      );
  }


}
