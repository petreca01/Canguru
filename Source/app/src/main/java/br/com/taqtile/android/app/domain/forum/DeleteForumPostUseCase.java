package br.com.taqtile.android.app.domain.forum;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class DeleteForumPostUseCase extends BaseUseCase<String, PostParams, ForumRepository> {

  public DeleteForumPostUseCase(ForumRepository forumRepository) {
    super(forumRepository);
  }

  @Override
  public Observable<String> execute(PostParams postParams) {
    return getRepository().delete(postParams);
  }
}
