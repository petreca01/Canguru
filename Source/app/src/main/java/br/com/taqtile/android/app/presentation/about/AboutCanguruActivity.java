package br.com.taqtile.android.app.presentation.about;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.birthplan.BirthplanFragment;
import br.com.taqtile.android.app.presentation.birthplan.BirthplanPresenter;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 20/07/17.
 */

public class AboutCanguruActivity extends TemplateBackActivity {

  private AboutCanguruFragment fragment;

  @Override
  public Fragment getFragment() {
    AboutCanguruFragment aboutCanguruFragment = (AboutCanguruFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (aboutCanguruFragment == null){
      aboutCanguruFragment = AboutCanguruFragment.newInstance();
    }
    fragment = aboutCanguruFragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    AboutCanguruPresenter aboutCanguruPresenter= new AboutCanguruPresenter();
    fragment.setPresenter(aboutCanguruPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return aboutCanguruPresenter;
  }

  public static void navigate(Context context){
    context.startActivity(new Intent(context, AboutCanguruActivity.class));
  }

}
