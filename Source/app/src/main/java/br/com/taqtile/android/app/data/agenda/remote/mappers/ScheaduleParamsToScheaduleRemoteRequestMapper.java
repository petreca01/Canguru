package br.com.taqtile.android.app.data.agenda.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.request.ScheaduleRemoteRequest;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;

/**
 * Created by taqtile on 4/7/17.
 */

public class ScheaduleParamsToScheaduleRemoteRequestMapper {
  public static ScheaduleRemoteRequest perform(ScheduleParams params) {
    return new ScheaduleRemoteRequest(params.getAgendaId(),
      params.getIsAppointment(),
      params.getDate(),
      params.getObservation(),
      params.getName(),
      params.getTypeId());
  }
}
