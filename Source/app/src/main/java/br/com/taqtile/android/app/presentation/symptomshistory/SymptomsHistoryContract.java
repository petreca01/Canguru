package br.com.taqtile.android.app.presentation.symptomshistory;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 08/05/17.
 */

public interface SymptomsHistoryContract {
  interface Presenter extends BasePresenter {
    Observable<List<SymptomHistoryViewModel>> getUserSymptoms();
    Observable<EmptyResult> sendUserSymptomsHistory(String email);
  }
}
