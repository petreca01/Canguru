package br.com.taqtile.android.app.domain.followcard;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.followcard.mappers.FollowCardVaccinesResultToViewModelMapper;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardVaccinesResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardVaccinesViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardVaccinesUseCase extends BaseUseCase<List<FollowCardVaccinesViewModel>, Void,
  FollowCardRepository> {

  public FollowCardVaccinesUseCase(FollowCardRepository followCardRepository) {
    super(followCardRepository);
  }

  @Override
  public Observable<List<FollowCardVaccinesViewModel>> execute(Void aVoid) {
    return getRepository().fetchFollowCardVaccines()
      .map(FollowCardVaccinesResultToViewModelMapper::perform);
  }
}
