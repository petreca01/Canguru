package br.com.taqtile.android.app.data.facebook.remote;

import android.app.Activity;
import android.os.Bundle;
import br.com.taqtile.android.app.data.facebook.remote.models.FacebookError;
import br.com.taqtile.android.app.data.facebook.remote.models.FacebookUserResponse;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by taqtile on 25/04/17.
 */

public class FacebookRemoteDataSource {
  private WeakReference<Activity> activityWeakReference;

  private CallbackManager callbackManager;

  public FacebookRemoteDataSource(Activity activity, CallbackManager callbackManager) {
    activityWeakReference = new WeakReference<>(activity);
    this.callbackManager = callbackManager;
  }

  public Observable<String> requestFacebookPermissions() {
    return createRequestFacebookPermissionsObservable().subscribeOn(Schedulers.io());
  }

  private Observable<String> createRequestFacebookPermissionsObservable() {
    return Observable.create(subscriber -> {
      Activity activity = activityWeakReference.get();
      if (activity != null) {
        LoginManager loginManager = LoginManager.getInstance();

        loginManager.logInWithReadPermissions(activity,
          Arrays.asList(FacebookConstants.PROFILE_PERMISSION_KEY,
            FacebookConstants.EMAIL_PERMISSION_KEY));

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
          @Override public void onSuccess(LoginResult loginResult) {
            subscriber.onNext(loginResult.getAccessToken().getToken());
            subscriber.onCompleted();
          }

          @Override public void onCancel() {
            subscriber.onError(FacebookError.getLoginCanceledError());
          }

          @Override public void onError(FacebookException exception) {
            //https://stackoverflow.com/questions/29791618/using-facebook-sdk-on-android-gives-user-logged-in-as-different-facebook-user
            if (exception instanceof FacebookAuthorizationException) {
              if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut();
              }
            }
            subscriber.onError(FacebookError.getLoginFailedError());
          }
        });
      } else {
        subscriber.onError(FacebookError.getLoginFailedError());
      }
    });
  }

  public Observable<FacebookUserResponse> getUser() {
    return createGetUserObservable().subscribeOn(Schedulers.io());
  }

  private Observable<FacebookUserResponse> createGetUserObservable() {
    return Observable.create(subscriber -> {

      Bundle parameters = new Bundle();
      parameters.putString(FacebookConstants.FIELD_PROFILE_REQUEST_KEY,
        FacebookConstants.FIELD_PROFILE_REQUEST_VALUE);

      GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
        (object, graphResponse) -> userRetrieven(subscriber, object));

      request.setParameters(parameters);
      request.executeAsync();
    });
  }

  private void userRetrieven(Subscriber<? super FacebookUserResponse> subscriber,
    JSONObject object) {
    try {

      String name = getValue(object, FacebookConstants.FB_USER_RESPONSE_NAME_KEY);
      String email = getValue(object, FacebookConstants.FB_USER_RESPONSE_EMAIL_KEY);

      subscriber.onNext(
        new FacebookUserResponse(name, email, getToken()));
      subscriber.onCompleted();
    } catch (JSONException e) {
      subscriber.onError(FacebookError.getSignUpFailledError());
    }
  }

  private String getToken() {
    if (AccessToken.getCurrentAccessToken() == null) {
      return "";
    }
    return AccessToken.getCurrentAccessToken().getToken();
  }

  private String getValue(JSONObject object, String key) throws JSONException {
    String value = "";

    if (object != null && key != null && object.has(key)) {
      value = object.getString(key);
    }

    return value;
  }
}
