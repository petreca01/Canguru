package br.com.taqtile.android.app.data.account.remote;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import java.util.Map;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/15/17.
 */

public interface AccountServices {

  @FormUrlEncoded
  @POST("usuarios/autenticar")
  Observable<Result<BaseRemoteResponse<UserRemoteResponse>>> signIn(
    @FieldMap Map<String, String> loginRequestModel);

  @FormUrlEncoded
  @POST("users")
  Observable<Result<BaseRemoteResponse<UserRemoteResponse>>> signUp(
    @FieldMap Map<String, String> signUpRequestModel);

  @FormUrlEncoded
  @POST("usuario/alterar")
  Observable<Result<BaseRemoteResponse<UserRemoteResponse>>> edit(
    @FieldMap Map<String, String> editRequestModel);

  @POST("usuario")
  Observable<Result<BaseRemoteResponse<UserRemoteResponse>>> getUserData();

  @FormUrlEncoded
  @POST("usuarios/autenticar/facebook")
  Observable<Result<BaseRemoteResponse<UserRemoteResponse>>> signInWithFacebook(@FieldMap Map<String, String> facebookLoginRequestModel);

  @FormUrlEncoded
  @POST("usuario/recuperarSenha")
  Observable<Result<BaseRemoteResponse<String>>> passwordRecovery(
    @FieldMap Map<String, String> passwordRecoveryModel);
}
