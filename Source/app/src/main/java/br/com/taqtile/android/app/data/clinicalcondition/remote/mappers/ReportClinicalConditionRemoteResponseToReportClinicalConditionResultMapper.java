package br.com.taqtile.android.app.data.clinicalcondition.remote.mappers;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ReportClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.data.common.mappers.UserRemoteResponseToUserResultMapper;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportClinicalConditionResult;
import br.com.taqtile.android.app.domain.common.models.UserResult;

/**
 * Created by taqtile on 4/12/17.
 */

public class ReportClinicalConditionRemoteResponseToReportClinicalConditionResultMapper {
  public static ReportClinicalConditionResult perform(ReportClinicalConditionRemoteResponse response) {
    UserResult userResult = UserRemoteResponseToUserResultMapper.perform(response.getUser());
    return new ReportClinicalConditionResult(
      userResult,
      UserClinicalConditionRemoteResponseToUserClinicalConditionResultMapper
        .perform(response.getUserCondition()));
  }
}
