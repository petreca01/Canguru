package br.com.taqtile.android.app.presentation.birthplanquestionnaire;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.listings.adapters.QuestionnaireAdapter;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorBuilder;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorColors;
import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import br.com.taqtile.android.app.listings.questionnairecomponents.AnswerListener;
import br.com.taqtile.android.app.listings.questionnairecomponents.QuestionnaireResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import xyz.santeri.wvp.WrappingViewPager;

import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_QUESTIONNAIRE_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_QUESTIONNAIRE_QUESTION_INDEX_KEY;


/**
 * Created by taqtile on 11/05/17.
 */

public class BirthplanQuestionnaireFragment extends AppBaseFragment implements
  HorizontalSelectorCell.Listener, ViewPager.OnPageChangeListener, AnswerListener,
  ProgressBarButton.ProgressBarButtonButtonListener, ViewTreeObserver.OnGlobalLayoutListener {

  private final static String FIRST_QUESTION = "1";
  private final static int QUESTION_NUMBER_OFFSET = 1;

  @BindView(R.id.fragment_birthplan_questionnaire_view_pager)
  WrappingViewPager questionsViewPager;
  QuestionnaireAdapter questionsAdapter;

  private List<String> questionsNumbers;
  private List<AnswerParams> answersList;
  private String questionId;

  @BindView(R.id.fragment_birthplan_questionnaire_form_save_button)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_birthplan_questionnaire_skip_question_container)
  LinearLayout skipButtonContainer;
  @BindView(R.id.fragment_birthplan_questionnaire_skip_button)
  CustomTextView skipButton;

  @BindView(R.id.fragment_birthplan_questionnaire_selector_container)
  LinearLayout questionSelectorContainer;
  private HorizontalSelectorBuilder questionSelectorBuilder;

  private NavigationManager navigationManager;
  private BirthplanQuestionnaireContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private List<QuestionViewModel> questionnaire;
  private Subscription sendAnswerSubscription;
  private String currentQuestionIndex;

  public static BirthplanQuestionnaireFragment newInstance() {
    return new BirthplanQuestionnaireFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_birthplan_questionnaire, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupQuestionsAdapter();
    setupButtons();
    setupAnswersList();

    setQuestionnaire();
    populateHorizontalSelector();
  }

  private void setupQuestionsAdapter() {
    this.questionnaire = new ArrayList<>();
    questionsAdapter = new QuestionnaireAdapter(this::onAnswerSelected);
    questionsViewPager.setAdapter(questionsAdapter);
    questionsViewPager.addOnPageChangeListener(this);
  }

  private void setupButtons() {
    setupSaveButton();
    setupSkipButton();
  }

  private void setupSaveButton() {
    saveButton.setButtonListener(this);
  }

  private void saveQuestion() {
    showButtonLoading();
    checkAnswer();

    sendAnswerSubscription = presenter.sendBirthPlanQuestionAnswers(this.answersList)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onSaveSuccess, this::onSaveFailure);
    compositeSubscription.add(sendAnswerSubscription);
  }

  private void checkAnswer() {
    QuestionnaireResult questionResult = getQuestionResult();
    if (isAnswerListEmpty() && questionResult != null) {
      this.answersList.add(new AnswerParams(questionResult.getResult(),
        Integer.valueOf(questionResult.getQuestionId())));
    }
  }

  private boolean isAnswerListEmpty() {
    return this.answersList == null || this.answersList.isEmpty();
  }

  private void onSaveSuccess(EmptyResult emptyResult) {
    this.answersList.clear();
    selectNextQuestionPage();
    hideButtonLoading();
  }

  private void onSaveFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    hideButtonLoading();
  }

  private void setupSkipButton() {
    skipButtonContainer.setOnClickListener(v -> selectNextQuestionPage());
    skipButton.setOnClickListener(v -> selectNextQuestionPage());
  }

  private void setupAnswersList() {
    answersList = new ArrayList<>();
    currentQuestionIndex = FIRST_QUESTION;
  }

  private void setQuestionnaire() {
    showLoading();
    Bundle bundle = getActivity().getIntent().getExtras();
    this.questionnaire = bundle.getParcelableArrayList(BIRTH_PLAN_QUESTIONNAIRE_KEY);

    if (questionnaire != null) {
      this.currentQuestionIndex = getCurrentQuestionId(bundle);
      questionsAdapter.setQuestions(questionnaire);
    }
    questionsAdapter.notifyDataSetChanged();

    hideLoading();

    questionsViewPager.getViewTreeObserver().addOnGlobalLayoutListener(this);
  }

  @Override
  public void onGlobalLayout() {
    checkAnswer();
    setButtonEnabledOrDisabled(getQuestionResult());
    questionsViewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
  }

  private String getCurrentQuestionId(Bundle bundle) {
    String questionId = bundle.getString(BIRTH_PLAN_QUESTIONNAIRE_QUESTION_INDEX_KEY, FIRST_QUESTION);
    return Integer.valueOf(questionId) < this.questionnaire.size() ? questionId : FIRST_QUESTION;
  }

  private void populateHorizontalSelector() {
    HorizontalSelectorColors questionSelectorColors = getSelectorColors();

    questionsNumbers = new ArrayList<String>();
    for (int i = 1; i <= this.questionnaire.size(); i++) {
      questionsNumbers.add(String.valueOf(i));
    }

    questionSelectorBuilder = new HorizontalSelectorBuilder(questionSelectorContainer, getContext());
    addQuestions(questionsNumbers, questionSelectorColors.getTextActiveColor(),
      questionSelectorColors.getTextInactiveColor(),
      questionSelectorColors.getBackgroundSelectedColor());

    questionSelectorBuilder.setSelectedCell(currentQuestionIndex);
    questionsViewPager.setOffscreenPageLimit(questionsNumbers.size());
    this.onSelectorClick(true, currentQuestionIndex);
  }

  private HorizontalSelectorColors getSelectorColors() {
    return new HorizontalSelectorColors(ContextCompat.getColor(getContext(),
      R.color.color_brand_orange), ContextCompat.getColor(getContext(),
      R.color.color_questionnaire_number_default_color), R.color.color_birthplan_questionnaire_selected_cell_background);
  }

  private void addQuestions(List<String> questions, int textActiveColor, int textInactiveColor,
                            int backgroundSelectedColor) {
    for (String selectorText : questions) {
      questionSelectorBuilder.addSelectorCell(selectorText,
        this,
        textActiveColor,
        textInactiveColor,
        backgroundSelectedColor);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_birthplan_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  @Override
  public void onAnswerSelected(QuestionnaireResult result) {
    if (saveButton.isLoading()) {
      return;
    }
    answersList.clear();
    if (result.isResultValid()) {
      saveButton.enableClick();
      for (String answer : result.getResult()) {
        answersList.add(new AnswerParams(answer, Integer.valueOf(result.getQuestionId())));
      }
    } else {
      saveButton.disableClick();
    }

  }

  @Override
  public void onSelectorClick(boolean state, String number) {
    questionSelectorBuilder.setSelectedCell(number);
    selectQuestionPage(number);
  }

  //Use this method if you want to move to the next adjacent question
  private void selectNextQuestionPage() {
    int nextQuestionNumber;
    if (questionsViewPager.getCurrentItem() == 0) {
      nextQuestionNumber = questionsViewPager.getCurrentItem() + QUESTION_NUMBER_OFFSET;
    } else {
      nextQuestionNumber = Integer.valueOf(questionsNumbers.get(questionsViewPager.getCurrentItem()));
    }
    if (questionsViewPager.getCurrentItem() != questionsAdapter.getCount() - 1) {
      questionsViewPager.setCurrentItem(questionsViewPager.getCurrentItem() + 1);
      questionSelectorBuilder.setSelectedCell(Integer.toString(nextQuestionNumber + 1));
    } else {
      getActivity().finish();
    }
  }

  //Use this method to set the specific question page you want to go to
  private void selectQuestionPage(String nextQuestion) {
    if (!nextQuestion.equals("0")) {
      questionsViewPager.setCurrentItem(Integer.valueOf(nextQuestion) - QUESTION_NUMBER_OFFSET);
      questionSelectorBuilder.setSelectedCellWithoutUnselectingOtherCells(nextQuestion);
    }
  }

  private void showButtonLoading() {
    saveButton.showLoadingState();
  }

  private void hideButtonLoading() {
    saveButton.showDefaultState();
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
  }

  @Override
  public void onPageSelected(int position) {

    questionId = getQuestionId();
    questionSelectorBuilder.setSelectedCell(String.valueOf(position + QUESTION_NUMBER_OFFSET));
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }

  private String getQuestionId() {
    QuestionnaireResult answer = (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
    return answer != null ? answer.getQuestionId() : "";
  }

  private QuestionnaireResult getQuestionResult() {
    QuestionnaireResult answer = (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
    return answer;
  }

  private void setButtonEnabledOrDisabled(QuestionnaireResult questionnaireResult) {
    if (questionnaireResult == null) {
      saveButton.disableClick();
      return;
    }
    if (!questionnaireResult.getResult().isEmpty() &&
      !questionnaireResult.getResult().get(0).isEmpty()) {
      saveButton.enableClick();
    } else {
      saveButton.disableClick();
    }
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(BirthplanQuestionnaireContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void onButtonClicked() {
    saveQuestion();
  }

  //end region


  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_birth_plan_questions),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
