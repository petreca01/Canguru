package br.com.taqtile.android.app.data.common.remote;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Objects;

import br.com.taqtile.android.app.data.common.remote.models.RemoteError;
import br.com.taqtile.android.app.data.common.remote.models.ResponseError;
import retrofit2.Response;
import retrofit2.adapter.rxjava.Result;

/**
 * Created by taqtile on 3/6/17.
 */

public class RemoteErrorDecisionMaker {

    private static final String NO_CONNECTION_MESSAGE = "Sem conexão";
    private static final String NOT_FOUND_MESSAGE = "Link nao encontrado!";
    private static final String DEFAULT_MESSAGE = "Erro de API";
    private static final int NOT_AUTHORIZED = 401;
    private static final int NOT_FOUND = 404;

    private Result result;
    private RemoteError remoteError;

    public RemoteErrorDecisionMaker(Result result) {
        this.result = result;
        this.remoteError = new RemoteError();
    }

    public void decide() {
        setType();
        setMessages();
    }

    public RemoteError getRemoteError() {
        return this.remoteError;
    }

    private void setType() {
        int code = result.response() != null ? result.response().code() : 0;

        if (code == NOT_FOUND) {
            remoteError.setErrorType(RemoteError.NOT_FOUND);
        } else if (code == NOT_AUTHORIZED) {
            remoteError.setErrorType(RemoteError.NOT_AUTHORIZED);
        } else if (result.error() instanceof UnknownHostException) {
            remoteError.setErrorType(RemoteError.NO_CONNECTION);
        } else if (result.error() instanceof SocketTimeoutException) {
          remoteError.setErrorType(RemoteError.TIMEOUT);
        } else {
            remoteError.setErrorType(RemoteError.UNKNOWN);
        }

    }

    private void setMessages() {

        if (Objects.equals(remoteError.getErrorType(), RemoteError.NO_CONNECTION)) {
            remoteError.setMessage(NO_CONNECTION_MESSAGE);
        } else if (result.isError()) {
            setMessageFromResultError();
        } else if (hasErrorBodyOnResponse()) {
            remoteError.setMessage(getRemoteErrorMessage());
        } else {
            remoteError.setMessage(DEFAULT_MESSAGE);
        }
    }

    private String getRemoteErrorMessage() {
      Gson gson = new GsonBuilder().create();
      ResponseError responseError = new ResponseError();
      try {
        responseError = gson.fromJson(result.response().errorBody().string(), ResponseError.class);
      } catch (IOException exception) {
        responseError.setErrorMessage(NOT_FOUND_MESSAGE);
      }
      return responseError.getErrorMessage();
    }

    private void setMessageFromResultError() {
        remoteError.setMessage(result.error().getMessage());
    }

    private boolean hasErrorBodyOnResponse() {
        Response response = result.response();

        return response != null && response.errorBody() != null;
    }
}
