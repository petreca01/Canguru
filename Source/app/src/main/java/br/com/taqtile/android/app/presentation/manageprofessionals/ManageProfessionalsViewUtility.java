package br.com.taqtile.android.app.presentation.manageprofessionals;

import android.view.View;
import android.widget.LinearLayout;
import br.com.taqtile.android.app.listings.cells.ConnectedProfessionalsCell;

/**
 * Created by taqtile on 27/04/17.
 */

public class ManageProfessionalsViewUtility {

  public static ConnectedProfessionalsCell getConnectedProfessionalsCell(LinearLayout parent, String id) {
    for (int index = 0; index < parent.getChildCount(); index++) {
      View view = parent.getChildAt(index);

      if (view instanceof ConnectedProfessionalsCell) {
        ConnectedProfessionalsCell connectedProfessionalsCell = (ConnectedProfessionalsCell) view;

        if (connectedProfessionalsCell.isIdEqualsTo(id)) {
          return connectedProfessionalsCell;
        }
      }
    }

    return null;
  }
}
