package br.com.taqtile.android.app.domain.followcard;

import java.util.List;

import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.followcard.mappers.FollowCardExamsResultToViewModelMapper;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardExamsResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardExamsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardExamsUseCase extends BaseUseCase<List<FollowCardExamsViewModel>, Void,
  FollowCardRepository> {

  public FollowCardExamsUseCase(FollowCardRepository followCardRepository) {
    super(followCardRepository);
  }

  @Override
  public Observable<List<FollowCardExamsViewModel>> execute(Void aVoid) {
    return getRepository().fetchFollowCardExams()
      .map(FollowCardExamsResultToViewModelMapper::perform);
  }
}
