package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityCommentItemRemoteResponse;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityCommentItemRemoteResponseListToMaternityCommentResultListMapper {

  public static List<MaternityCommentResult> perform(
    List<MaternityCommentItemRemoteResponse> maternityCommentItemRemoteResponseList) {
    return StreamSupport.stream(maternityCommentItemRemoteResponseList)
      .map(MaternityCommentItemRemoteResponseToMaternityCommentResultMapper::perform)
      .collect(Collectors.toList());
  }
}
