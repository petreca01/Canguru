package br.com.taqtile.android.app.data.notification.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class NotificationItemRemoteResponse {
    @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("usuario_id")
    private Integer usuarioId;

    @SerializedName("doctor_id")
    private Integer doctorId;

    @SerializedName("operator_id")
    private Integer operatorId;

    @SerializedName("type")
    private String type;

    @SerializedName("important")
    private Boolean important;

    @SerializedName("content")
    private String content;

    @SerializedName("entity")
    private String entity;

    @SerializedName("entity_id")
    private Integer entityId;

    @SerializedName("action")
    private String action;

    @SerializedName("user_performer_id")
    private Integer userPerformerId;

  @SerializedName("visualized")
    private Boolean visualized;

    @SerializedName("visualized_at")
    private String visualizedAt;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_performer_name")
    private String userPerformerName;

    @SerializedName("user_performer_picture")
    private String userPerformerPicture;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public String getType() {
        return type;
    }

    public Boolean getImportant() {
        return important;
    }

    public String getContent() {
        return content;
    }

    public String getEntity() {
        return entity;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public String getAction() {
        return action;
    }

    public Integer getUserPerformerId() {
        return userPerformerId;
    }

    public Boolean getVisualized() {
    return visualized;
  }

    public void setVisualized(Boolean visualized) {
    this.visualized = visualized;
  }

    public String getVisualizedAt() {
        return visualizedAt;
    }

    public void setVisualizedAt(String visualizedAt) { this.visualizedAt = visualizedAt; }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) { this.updatedAt = updatedAt; }

    public String getUserPerformerName() {
        return userPerformerName;
    }

    public String getUserPerformerPicture() {
        return userPerformerPicture;
    }
}
