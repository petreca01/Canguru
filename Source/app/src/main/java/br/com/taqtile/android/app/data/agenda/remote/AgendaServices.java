package br.com.taqtile.android.app.data.agenda.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.agenda.remote.models.response.AgendaItemRemoteResponse;
import br.com.taqtile.android.app.data.agenda.remote.models.response.AppointmentItemRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/21/17.
 */

public interface AgendaServices {
    @POST("agenda")
    Observable<Result<BaseRemoteResponse<List<AgendaItemRemoteResponse>>>> list();

    @FormUrlEncoded
    @POST("agenda/agendar")
    Observable<Result<BaseRemoteResponse<AgendaItemRemoteResponse>>> schedule(@FieldMap Map<String, String> scheduleRequestModel);

    @FormUrlEncoded
    @POST("agenda/novoCompromisso")
    Observable<Result<BaseRemoteResponse<AppointmentItemRemoteResponse>>> createAppointment(@FieldMap Map<String, String> createAppointmentRequestModel);

    @FormUrlEncoded
    @POST("agenda/realizado")
    Observable<Result<BaseRemoteResponse<AgendaItemRemoteResponse>>> finish(@FieldMap Map<String, String> finishRequestModel);

    @FormUrlEncoded
    @POST("agenda/removerCompromisso")
    Observable<Result<BaseRemoteResponse<String>>> remove(@FieldMap Map<String, String> removeRequestModel);

}
