package br.com.taqtile.android.app.presentation.birthplanquestionnaire;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 11/05/17.
 */

public interface BirthplanQuestionnaireContract {

  interface Presenter extends BasePresenter {
    Observable<EmptyResult> sendBirthPlanQuestionAnswers(List<AnswerParams> answerParams);
  }

}
