package br.com.taqtile.android.app.presentation.searchhealthcare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.sax.TransformerHandler;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;
import br.com.taqtile.android.app.listings.adapters.SearchHealthcareAdapter;
import br.com.taqtile.android.app.listings.adapters.SearchPaginatedAdapter;
import br.com.taqtile.android.app.listings.cells.SearchPlaceholderCell;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.search.CustomSearchView;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.app.misc.utils.helpers.SkeletonLoadingGenerator;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.PresenterError;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.StringScoreFilter;
import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 18/04/17.
 */

public class SearchHealthcareFragment extends AppBaseFragment implements CustomSearchView.SubmitSearchListener {

  @BindView(R.id.fragment_search_healthcare_search_view)
  CustomSearchView searchView;
  @BindView(R.id.fragment_search_healthcare_search_list_header_container)
  FrameLayout searchListHeaderContainer;
  @BindView(R.id.fragment_search_search_list_header)
  CustomTextView searchListHeader;
  @BindView(R.id.fragment_search_healthcare_search_view_layout)
  LinearLayout searchViewLayout;
  @BindView(R.id.fragment_search_healthcare_recycler_view)
  RecyclerView searchRecyclerView;
  @BindView(R.id.fragment_search_healthcare_placeholder)
  LinearLayout placeholder;

  private CustomToolbar customToolbar;
  private SearchHealthcareAdapter searchAdapter;
  private Subscription healthOperatorsSubscription;
  private Subscription healthSearchOperatorsSubscription;
  private List<HealthOperatorViewModel> healthOperatorViewModels;
  private SearchHealthcareContract.Presenter presenter;
  private NavigationManager navigationManager;

  public static SearchHealthcareFragment newInstance() {
    return new SearchHealthcareFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_search_healthcare, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupView();
    setupRecyclerView();
    setupListeners();
    searchView.setHint(getResources().getString(R.string.fragment_search_healthcare_search_hint));

    showLoading();

  }

  @Override
  public void onResume() {
    super.onResume();
    setupView();
  }

  private void setupView() {
    setupToolbar();
  }

  private void setupRecyclerView() {
    healthOperatorViewModels = new ArrayList<>();
    searchAdapter = new SearchHealthcareAdapter(healthOperatorViewModels);
    searchRecyclerView.setAdapter(searchAdapter);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    searchRecyclerView.setLayoutManager(linearLayoutManager);
  }

  public void searchOperator(String searchTerm) {

    SoftKeyboardHelper.hideKeyboard(getContext());
    this.showLoading();

    SearchHealthOperatorParams searchParams = new SearchHealthOperatorParams(searchTerm);
    healthSearchOperatorsSubscription = presenter.fetchSearchHealthOperators(searchParams)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchSearchHealthOperatorsSuccess,
        this::onFetchSearchHealthOperatorsFailure);

    compositeSubscription.add(healthSearchOperatorsSubscription);
  }

  private void onFetchSearchHealthOperatorsSuccess(List<HealthOperatorViewModel> healthOperatorViewModels) {

    this.healthOperatorViewModels.clear();
    this.healthOperatorViewModels.addAll(healthOperatorViewModels);
    searchAdapter.notifyDataSetChanged();

    if (healthOperatorViewModels.isEmpty()) {
      showNoResultsPlaceholder();
    } else{
      showResults();
    }
  }

  private void onFetchSearchHealthOperatorsFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    this.hideLoading();
    showPlaceholder();
  }

  public void showNoResultsPlaceholder() {
    showPlaceholderByType(CustomPlaceholder.NO_HEALTHCARE_FOUND);
    this.hideLoading();
    showPlaceholder();
  }

  public void showResults(){
    searchListHeader.setText(getResources().getString(R.string.fragment_search_healthcare_list_header));
    searchListHeaderContainer.setVisibility(View.VISIBLE);
    searchRecyclerView.setVisibility(View.VISIBLE);
    this.hideLoading();
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
  }

  @Override
  public void showLoading() {
    super.showLoading();

    searchListHeaderContainer.setVisibility(View.GONE);
    searchRecyclerView.setVisibility(View.GONE);
    placeholder.setVisibility(View.GONE);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_search_healthcare_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setupListeners() {

    searchView.setSubmitSearchListener(this);
    searchAdapter.setListener(index ->
      navigationManager.showHealthOperatorsDetailUI(healthOperatorViewModels.get(index), true,
      this::returningFromHealthOperatorsDetail));
  }

  private void returningFromHealthOperatorsDetail(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      Intent intent = new Intent();
      getActivity().setResult(responseCode, intent);
      getActivity().finish();
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  public void setPresenter(SearchHealthcareContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    hideLoading();
  }

  @Override
  public void onSubmit(String text) {
    searchOperator(text);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    searchOperator(searchView.getText());
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return placeholder.getId();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_healthcare_search),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
