package br.com.taqtile.android.app.data.forum.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionRemoteRequest extends BaseRemoteRequest {

  @RequestName("page")
  private Integer page;

  @RequestName("per_page")
  private Integer perPage;

  @RequestName("search")
  private String search;

  public ContentSuggestionRemoteRequest(String search, Integer page, Integer perPage) {
    this.page = page;
    this.perPage = perPage;
    this.search = search;
  }

  public Integer getPage() {
    return page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public String getSearch() {
    return search;
  }



}


