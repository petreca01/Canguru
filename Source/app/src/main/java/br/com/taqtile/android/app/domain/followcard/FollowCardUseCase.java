package br.com.taqtile.android.app.domain.followcard;

import br.com.taqtile.android.app.data.followcard.FollowCardRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.followcard.mappers.FollowCardResultToViewModelMapper;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardUseCase extends BaseUseCase<FollowCardViewModel, Void, FollowCardRepository> {

  public FollowCardUseCase(FollowCardRepository followCardRepository) {
    super(followCardRepository);
  }

  @Override
  public Observable<FollowCardViewModel> execute(Void aVoid) {
    return getRepository().fetchFollowCard()
      .map(FollowCardResultToViewModelMapper::perform);
  }
}
