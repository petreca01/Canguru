package br.com.taqtile.android.app.data.channel.remote.models.response;

import java.util.List;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostRemoteResponse {
    private ChannelPostItemRemoteResponse post;
    private ChannelItemRemoteResponse channel;
    private List<ChannelReplyRemoteResponse> replies;

    public ChannelPostItemRemoteResponse getPost() {
        return post;
    }

    public ChannelItemRemoteResponse getChannel() {
        return channel;
    }

    public List<ChannelReplyRemoteResponse> getReplies() {
        return replies;
    }
}
