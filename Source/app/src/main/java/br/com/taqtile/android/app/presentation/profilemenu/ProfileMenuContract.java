package br.com.taqtile.android.app.presentation.profilemenu;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by Renato on 4/18/17.
 */

public interface ProfileMenuContract {
  interface Presenter extends BasePresenter {
    Observable<EmptyResult> logout();
  }

}
