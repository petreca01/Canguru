package br.com.taqtile.android.app.presentation.notifications;

import java.util.List;

import br.com.taqtile.android.app.domain.notification.ListNotificationsUseCase;
import br.com.taqtile.android.app.domain.notification.SetNotificationVisualizedUseCase;
import br.com.taqtile.android.app.domain.notification.mapper.NotificationResultToNotificationViewModelMapper;
import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;
import br.com.taqtile.android.app.listings.viewmodels.NotificationViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class NotificationsPresenter implements NotificationsContract.Presenter {

  private ListNotificationsUseCase listNotificationsUseCase;
  private SetNotificationVisualizedUseCase setNotificationVisualizedUseCase;

    public NotificationsPresenter() {
      listNotificationsUseCase = Injection.provideListNotificationsUseCase();
      setNotificationVisualizedUseCase = Injection.provideSetNotificationVisualizedUseCase();
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

  @Override
  public Observable<List<NotificationViewModel>> fetchNotifications() {
    return listNotificationsUseCase.execute(null).map(NotificationResultToNotificationViewModelMapper::perform);
  }

  @Override
  public Observable<Boolean> setNotificationVisualized(SetVisualizedNotificationParams setVisualizedNotificationParams) {
    return setNotificationVisualizedUseCase.execute(setVisualizedNotificationParams);
  }
}
