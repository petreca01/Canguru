package br.com.taqtile.android.app.support.helpers;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.imagecapture.ImageCaptureActivity;
import br.com.taqtile.android.app.presentation.profilepublic.PublicProfileActivity;

/**
 * Created by taqtile on 4/28/17.
 */

public class CallMediaFragmentHelper {
  private Fragment mFragment;
  private int actionId;
  public static final String ACTION_ID = "actionId";
  public static final String REQUEST_CODE = "requestCode";
  private static final int REQUEST_PERMISSIONS = 1;
  private static String[] PERMISSIONS_REQUIRED = {
    Manifest.permission.READ_EXTERNAL_STORAGE,
    Manifest.permission.WRITE_EXTERNAL_STORAGE,
    Manifest.permission.CAMERA
  };
  public static final int TAKE_PICTURE = 0;
  public static final int CHOOSE_PICTURE = 1;
  public static final int CHOOSE_OR_TAKE = 2;

  public CallMediaFragmentHelper(Fragment fragment) {
    this.mFragment = fragment;
  }

  public void callMediaActivity(int requestCode) {
    if (shouldRequestPermissions()) {
      mFragment.requestPermissions(PERMISSIONS_REQUIRED, REQUEST_PERMISSIONS);
    } else {
      Intent intent = new Intent(mFragment.getContext(), ImageCaptureActivity.class);
      intent.putExtra(ACTION_ID, actionId);
      intent.putExtra(REQUEST_CODE, requestCode);
      mFragment.getActivity().startActivityForResult(intent, requestCode);
    }
  }

  public void callMediaOnPermissionRequestResult(int requestCode) {
    if (!shouldRequestPermissions()) {
      Intent intent = new Intent(mFragment.getContext(), ImageCaptureActivity.class);
      intent.putExtra(ACTION_ID, actionId);
      intent.putExtra(REQUEST_CODE, requestCode);
      mFragment.getActivity().startActivityForResult(intent, requestCode);
    } else {
      Toast.makeText(mFragment.getActivity().getBaseContext(),
        mFragment.getString(R.string.activity_accept_permissions), Toast.LENGTH_SHORT).show();
    }
  }

  public boolean shouldRequestPermissions() {
    int readPermission = ActivityCompat.checkSelfPermission(mFragment.getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
    int writePermission = ActivityCompat.checkSelfPermission(mFragment.getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
    int cameraPermission = ActivityCompat.checkSelfPermission(mFragment.getActivity(), Manifest.permission.CAMERA);

    return (readPermission != PackageManager.PERMISSION_GRANTED ||
      writePermission != PackageManager.PERMISSION_GRANTED ||
      cameraPermission != PackageManager.PERMISSION_GRANTED);
  }

  public void setActionId(int actionId) {
    this.actionId = actionId;
  }

}

