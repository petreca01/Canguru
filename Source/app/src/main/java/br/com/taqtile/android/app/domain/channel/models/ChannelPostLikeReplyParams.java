package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostLikeReplyParams {

  private Integer postId;

  private Integer replyId;

  private boolean like;

  public ChannelPostLikeReplyParams(Integer postId, Integer replyId, boolean like) {
    this.postId = postId;
    this.replyId = replyId;
    this.like = like;
  }

  public Integer getPostId() {
    return postId;
  }

  public Integer getReplyId() {
    return replyId;
  }

  public boolean isLike() {
    return like;
  }

}
