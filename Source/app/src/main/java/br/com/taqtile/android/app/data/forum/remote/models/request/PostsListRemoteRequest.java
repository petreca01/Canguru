package br.com.taqtile.android.app.data.forum.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/21/17.
 */

public class PostsListRemoteRequest extends BaseRemoteRequest {
    private Integer page;

    @RequestName("per_page")
    private Integer perPage;

    @RequestName("order_by")
    private String orderBy;

    @RequestName("order_direction")
    private String orderDirection;

    private String search;

  public PostsListRemoteRequest(Integer page, Integer perPage, String orderBy, String orderDirection, String search) {
    this.page = page;
    this.perPage = perPage;
    this.orderBy = orderBy;
    this.orderDirection = orderDirection;
    this.search = search;
  }
}
