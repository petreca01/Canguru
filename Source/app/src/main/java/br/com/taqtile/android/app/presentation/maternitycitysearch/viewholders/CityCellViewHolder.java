package br.com.taqtile.android.app.presentation.maternitycitysearch.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.listings.cells.TextAndImageCell;
import br.com.taqtile.android.app.listings.cells.TextCell;
import br.com.taqtile.android.app.presentation.maternitycitysearch.components.CityCell;

/**
 * Created by taqtile on 5/12/17.
 */

public class CityCellViewHolder extends RecyclerView.ViewHolder {

  private CityCell cityCell;

  public CityCellViewHolder(View itemView) {
    super(itemView);
    itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    this.cityCell = (CityCell) itemView;
  }

  public void setCity(String city) {
    this.cityCell.setCity(city);
  }

}
