package br.com.taqtile.android.app.presentation.prenatalcard.models;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardExamsViewModel {

  private Integer id;
  private String name;
  private String result;
  private String date;
  private String createdAt;

  public FollowCardExamsViewModel(Integer id, String name, String result, String date,
                               String createdAt) {
    this.id = id;
    this.name = name;
    this.result = result;
    this.date = date;
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getResult() {
    return result;
  }

  public String getDate() {
    return date;
  }

  public String getCreatedAt() {
    return createdAt;
  }

}
