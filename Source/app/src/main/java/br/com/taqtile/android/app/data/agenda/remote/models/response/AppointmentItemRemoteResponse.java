package br.com.taqtile.android.app.data.agenda.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/21/17.
 */

public class AppointmentItemRemoteResponse {
    @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("operator_highlight")
    private Boolean operatorHighlight;

    @SerializedName("trimestre")
    private Integer quarter;

    @SerializedName("nome")
    private String name;

    @SerializedName("codigo")
    private String code;

    @SerializedName("tipo_id")
    private Integer typeId;

    @SerializedName("tipo")
    private String type;

    @SerializedName("semana_inicio")
    private String weekStart;

    @SerializedName("semana_termino")
    private Integer weekEnd;

    @SerializedName("dia_termino")
    private String dayEnd;

    @SerializedName("descricao")
    private String description;

    @SerializedName("descricao_imagem_url")
    private String descriptionImageUrl;

    @SerializedName("descricao_extensa")
    private String completeDescription;

    @SerializedName("observacao")
    private String observation;

    @SerializedName("realizado")
    private Integer done;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("marcacao_data")
    private String markingDate;

    @SerializedName("marcacao_observacao")
    private String markingObservation;

    @SerializedName("isCompromisso")
    private Integer isAppointment;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public Boolean getOperatorHighlight() {
        return operatorHighlight;
    }

    public Integer getQuarter() {
        return quarter;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public String getType() {
        return type;
    }

    public String getWeekStart() {
        return weekStart;
    }

    public Integer getWeekEnd() {
        return weekEnd;
    }

    public String getDayEnd() {
        return dayEnd;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionImageUrl() {
        return descriptionImageUrl;
    }

    public String getCompleteDescription() {
        return completeDescription;
    }

    public String getObservation() {
        return observation;
    }

    public Integer getDone() {
        return done;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getMarkingDate() {
        return markingDate;
    }

    public String getMarkingObservation() {
        return markingObservation;
    }

    public Integer getIsAppointment() {
        return isAppointment;
    }
}
