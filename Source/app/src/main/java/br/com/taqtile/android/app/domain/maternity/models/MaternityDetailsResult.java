package br.com.taqtile.android.app.domain.maternity.models;

import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import java.util.List;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityDetailsResult {

  private MaternityDataResult maternity;

  private List<MaternityCommentResult> comments;

  private List<ChannelPost> posts;

  public MaternityDetailsResult(MaternityDataResult maternity,
    List<MaternityCommentResult> comments, List<ChannelPost> posts) {
    this.maternity = maternity;
    this.comments = comments;
    this.posts = posts;
  }

  public MaternityDataResult getMaternity() {
    return maternity;
  }

  public List<MaternityCommentResult> getComments() {
    return comments;
  }

  public List<ChannelPost> getPosts() {
    return posts;
  }
}
