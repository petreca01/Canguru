package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.RangedAnswerRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;

/**
 * Created by taqtile on 07/04/17.
 */

public class RangedAnswerRemoteResponseToRangedAnswerResultMapper {

  public static RangedAnswerResult perform(RangedAnswerRemoteResponse rangedAnswerRemoteResponse) {
    if (rangedAnswerRemoteResponse == null ) {
      return null;
    }
    return new RangedAnswerResult(rangedAnswerRemoteResponse.getMin(),
      rangedAnswerRemoteResponse.getMax());
  }
}
