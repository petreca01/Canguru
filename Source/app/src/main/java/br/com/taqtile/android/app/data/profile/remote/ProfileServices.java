package br.com.taqtile.android.app.data.profile.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostItemResponse;
import br.com.taqtile.android.app.data.profile.remote.models.response.ProfileDetailsRemoteResponse;
import br.com.taqtile.android.app.data.profile.remote.models.response.UpdateProfileDetailsRemoteResponse;
import okhttp3.MultipartBody;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public interface ProfileServices {
    @FormUrlEncoded
    @POST("profiles")
    Observable<Result<BaseRemoteResponse<ProfileDetailsRemoteResponse>>>
    getProfileDetails(@FieldMap Map<String, String> getProfileDetailsRemoteRequest);

    @Multipart
    @POST("profiles/update")
    Observable<Result<BaseRemoteResponse<UpdateProfileDetailsRemoteResponse>>>
    editPublicProfile(@PartMap Map<String, String> editPublicProfileRemoteRequest,
                      @Part List<MultipartBody.Part> files);

    @POST("profiles/{profileId}/forum_posts")
    Observable<Result<BaseRemoteResponse<List<PostItemResponse>>>>
    listPosts(@Path("profileId") Integer profileId);
}
