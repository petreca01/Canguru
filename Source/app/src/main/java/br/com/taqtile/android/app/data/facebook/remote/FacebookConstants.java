package br.com.taqtile.android.app.data.facebook.remote;

/**
 * Created by taqtile on 25/04/17.
 */

class FacebookConstants {
  static final String PROFILE_PERMISSION_KEY = "public_profile";
  static final String EMAIL_PERMISSION_KEY = "email";

  static final String FIELD_PROFILE_REQUEST_KEY = "fields";
  static final String FIELD_PROFILE_REQUEST_VALUE = "email,name";

  static final String FB_USER_RESPONSE_EMAIL_KEY = "email";
  static final String FB_USER_RESPONSE_NAME_KEY = "name";
}
