package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 12/04/17.
 */

public class WarnPostParams {
  private Integer postId;
  private String content;

  public WarnPostParams(Integer postId, String content) {
    this.postId = postId;
    this.content = content;
  }

  public Integer getPostId() {
    return postId;
  }

  public String getContent() {
    return content;
  }
}
