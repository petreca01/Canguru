package br.com.taqtile.android.app.data.maternity.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class MaternityCommentItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("user_id")
    private Integer userId;

    @SerializedName("hospital_id")
    private Integer hospitalId;

    @SerializedName("comment")
    private String comment;

    @SerializedName("active")
    private Integer active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("username")
    private String username;

    @SerializedName("user_picture")
    private String userPicture;

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public String getComment() {
        return comment;
    }

    public Integer getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public String getUserPicture() {
        return userPicture;
    }
}
