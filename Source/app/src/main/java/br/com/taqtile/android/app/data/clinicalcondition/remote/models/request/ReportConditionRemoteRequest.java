package br.com.taqtile.android.app.data.clinicalcondition.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class ReportConditionRemoteRequest extends BaseRemoteRequest {
    @RequestName("condition_id")
    private Integer conditionId;

  public ReportConditionRemoteRequest(Integer conditionId) {
    this.conditionId = conditionId;
  }
}
