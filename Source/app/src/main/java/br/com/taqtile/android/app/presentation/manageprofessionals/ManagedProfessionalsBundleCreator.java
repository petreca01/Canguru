package br.com.taqtile.android.app.presentation.manageprofessionals;

import android.os.Bundle;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import java.util.ArrayList;

/**
 * Created by taqtile on 27/04/17.
 */

public class ManagedProfessionalsBundleCreator {

  public static Bundle getBundle(
    ArrayList<ConnectedProfessionalViewModel> connectedProfessionalViewModelList) {
    Bundle bundle = new Bundle(1);
    bundle.putParcelableArrayList(ManagedProfessionalsBundleConstants.PROFESSIONALS_LIST_KEY,
      connectedProfessionalViewModelList);
    return bundle;
  }
}
