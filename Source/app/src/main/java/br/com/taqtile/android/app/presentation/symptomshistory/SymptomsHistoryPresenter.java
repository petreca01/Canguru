package br.com.taqtile.android.app.presentation.symptomshistory;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.symptoms.ListUserSymptomsUseCase;
import br.com.taqtile.android.app.domain.symptoms.SendUserSymptomsUseCase;
import br.com.taqtile.android.app.listings.viewmodels.SymptomHistoryViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomsHistoryPresenter implements SymptomsHistoryContract.Presenter {
  ListUserSymptomsUseCase listUserSymptomsUseCase;
  SendUserSymptomsUseCase sendUserSymptomsUseCase;

  public SymptomsHistoryPresenter() {
    listUserSymptomsUseCase = Injection.provideListUserSymptomsUseCase();
    sendUserSymptomsUseCase = Injection.provideSendUserSymptomsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<SymptomHistoryViewModel>> getUserSymptoms() {
    return listUserSymptomsUseCase.execute(null);
  }

  @Override
  public Observable<EmptyResult> sendUserSymptomsHistory(String email) {
    return sendUserSymptomsUseCase.execute(email);
  }
}
