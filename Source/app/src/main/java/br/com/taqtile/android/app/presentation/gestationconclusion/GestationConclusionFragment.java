package br.com.taqtile.android.app.presentation.gestationconclusion;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionViewModel;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.listings.adapters.QuestionnaireAdapter;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorBuilder;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorColors;
import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import br.com.taqtile.android.app.listings.questionnairecomponents.AnswerListener;
import br.com.taqtile.android.app.listings.questionnairecomponents.QuestionnaireResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.GestationConclusionQuestionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.NoSwipeViewPager;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 07/07/17.
 */

public class GestationConclusionFragment extends AppBaseFragment implements
  ViewPager.OnPageChangeListener, AnswerListener, ProgressBarButton.ProgressBarButtonButtonListener {

  private final static String FIRST_QUESTION = "0";

  private final static int QUESTION_NUMBER_OFFSET = 1;
  private static final String TAG = "GesConclusionFragment";

  @BindView(R.id.fragment_gestation_conclusion_view_pager)
  NoSwipeViewPager questionsViewPager;
  QuestionnaireAdapter questionsAdapter;
  HorizontalSelectorBuilder questionMarkerBuilder;

  @BindView(R.id.fragment_gestation_conclusion_question_marker_container)
  LinearLayout questionMarkerContainer;

  @BindView(R.id.fragment_gestation_conclusion_save_button)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_gestation_conclusion_loading)
  ProgressBarLoading loading;

  HorizontalSelectorColors questionsMarkerColors;
  private List<String> questionsNumbers;
  private List<AnswerParams> answersList;
  private String questionId;
  private int currentQuestionIndex;

  private Subscription saveSubscription;

  private GestationConclusionContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  public static GestationConclusionFragment newInstance() {
    return new GestationConclusionFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_gestation_conclusion, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    showLoading();
    setupQuestionsAdapter();
    setupButtons();
    setupAnswersList();

    getQuestionnaireType();
  }

  private void setupQuestionsAdapter() {
    questionsAdapter = new QuestionnaireAdapter(this::onAnswerSelected);
    questionsViewPager.setAdapter(questionsAdapter);
    questionsViewPager.addOnPageChangeListener(this);
  }

  private void setupAnswersList() {
    answersList = new ArrayList<>();
  }

  private void setupButtons() {
    saveButton.setButtonListener(this);
  }

  private void sendQuestionnaire() {
    showButtonLoading();
    if (saveSubscription != null) {
      saveSubscription.unsubscribe();
    }
    saveSubscription = presenter.sendGestationConclusionQuestionnaireEmail()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onSendQuestionSuccess, this::onSendQuestionFailure);
    compositeSubscription.add(saveSubscription);
  }

  private void onSendQuestionSuccess(GestationConclusionViewModel gestationConclusionViewModel) {
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
  }

  private void onSendQuestionFailure(Throwable throwable) {
    onSaveQuestionFailure(throwable);
  }

  private void saveQuestion() {
    showButtonLoading();
    SoftKeyboardHelper.hideKeyboard(getContext());
    if (saveSubscription != null) {
      saveSubscription.unsubscribe();
    }
    if (this.answersList.isEmpty()) {
      this.answersList.add(new AnswerParams("", Integer.valueOf(getQuestionId())));
    }
    saveSubscription = presenter.saveAnswers(this.answersList, false)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onSaveQuestionSuccess, this::onSaveQuestionFailure);
    compositeSubscription.add(saveSubscription);
  }

  private void onSaveQuestionSuccess(GestationConclusionViewModel gestationConclusionViewModel) {
    GestationConclusionQuestionViewModel previousQuestion = gestationConclusionViewModel
      .getQuestionViewModels().get(currentQuestionIndex);
    nextStep(previousQuestion);
  }

  private void nextStep(GestationConclusionQuestionViewModel previousQuestion) {
    String previousAnswer = getQuestionResult().getResult().get(0);

//    Log.e(TAG, "nextStep("+previousQuestion.toString()+")");

    if (previousQuestion.isHasNext()) {
      int previousQuestionIndex = (previousQuestion.getAlternatives() != null && previousQuestion.getNextQuestionOptions().size() > 1) ? previousQuestion.getAlternatives().indexOf(previousAnswer) : 0;

//      Log.e(TAG, "nextStep() previousQuestionIndex: "+previousQuestionIndex);

//      Log.e(TAG, "nextStep() QUESTION_NUMBER_OFFSET: "+QUESTION_NUMBER_OFFSET);
//      Log.e(TAG, "nextStep() previousQuestion.getNextQuestionOptions().get(previousQuestionIndex) - QUESTION_NUMBER_OFFSET: "+(previousQuestion.getNextQuestionOptions().get(previousQuestionIndex) - QUESTION_NUMBER_OFFSET));
//      Log.e(TAG, "nextStep() previousQuestion.getAlternatives(): "+previousQuestion.getAlternatives());
//      Log.e(TAG, "nextStep() previousQuestion.getNextQuestionOptions().size(): "+previousQuestion.getNextQuestionOptions().size());

      this.selectQuestionPage(previousQuestion.getNextQuestionOptions().get(previousQuestionIndex) - QUESTION_NUMBER_OFFSET);

      answersList.clear();
      hideButtonLoading();
    } else {
      saveSubscription = presenter.saveAnswers(this.answersList, true)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::finishQuestionnaire, this::onSaveQuestionFailure);
      compositeSubscription.add(saveSubscription);
    }
  }

  private void finishQuestionnaire(GestationConclusionViewModel gestationConclusionViewModel) {
    this.sendQuestionnaire();
  }

  private void getQuestionnaireType() {
    fetchQuestionnaire();
  }

  private void fetchQuestionnaire() {
    showLoading();
    Subscription subscription = presenter.fetchGestationConclusionQuestionnaire()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchQuestionsSuccess, this::onFetchQuestionsFailure);
    compositeSubscription.add(subscription);
  }

  private void onFetchQuestionsSuccess(GestationConclusionViewModel gestationConclusionViewModel) {
    hideLoading();
    List<QuestionViewModel> questions = new ArrayList<>();
    for (GestationConclusionQuestionViewModel answer : gestationConclusionViewModel.getQuestionViewModels()) {
      questions.add(new QuestionViewModel(answer.getQuestionId(), answer.getType(),
        answer.getQuestion(), answer.getAnswer(), answer.getAlternatives(), answer.getAnswers()));
    }
    questionsAdapter.addQuestionViewModels(questions);
    questionsAdapter.notifyDataSetChanged();
    setupQuestionsMarker(gestationConclusionViewModel.getQuestionViewModels().size());
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  private void onFetchQuestionsFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupQuestionsMarker(Integer totalQuestionsForGroup) {
    if (this.questionsNumbers == null) {
      setupQuestionsMarkerColors();

      questionsNumbers = new ArrayList<>();
      for (int i = 1; i <= totalQuestionsForGroup; i++) {
        questionsNumbers.add(String.valueOf(i));
      }

      questionMarkerBuilder = new HorizontalSelectorBuilder(questionMarkerContainer, getContext());
      addQuestions(questionsNumbers,
        questionsMarkerColors.getTextActiveColor(),
        questionsMarkerColors.getTextInactiveColor(),
        questionsMarkerColors.getBackgroundSelectedColor());

      questionMarkerBuilder.setSelectedCell(questionsNumbers.get(0));
      questionsViewPager.setOffscreenPageLimit(questionsNumbers.size());
    }
  }

  private void addQuestions(List<String> questions, int textActiveColor, int textInactiveColor,
                            int backgroundSelectedColor) {
    for (String selectorText : questions) {
      questionMarkerBuilder.addSelectorCell(selectorText,
        null,
        textActiveColor,
        textInactiveColor,
        backgroundSelectedColor);
    }
  }

  private void setupQuestionsMarkerColors() {
    int textActiveColor = ContextCompat.getColor(getContext(), R.color.color_brand_orange);
    int textInactiveColor = ContextCompat.getColor(getContext(), R.color.color_questionnaire_number_default_color);
    int backgroundSelectedColor = Color.TRANSPARENT;
    questionsMarkerColors = new HorizontalSelectorColors(textActiveColor, textInactiveColor,
      backgroundSelectedColor);
  }

  private void onSaveQuestionFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    hideButtonLoading();
  }

  private void selectQuestionPage(Integer nextQuestion) {
//    Log.e(TAG, "selectQuestionPage("+nextQuestion+")" );

    if (!String.valueOf(nextQuestion).equals(FIRST_QUESTION)) {
      currentQuestionIndex = nextQuestion;
      questionsViewPager.setCurrentItem(currentQuestionIndex);
      questionMarkerBuilder.paintPreviousCells(
        String.valueOf(currentQuestionIndex));
    }
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
  }

  @Override
  public void onPageSelected(int position) {
    HorizontalSelectorCell selectedQuestion = (HorizontalSelectorCell) questionMarkerContainer.getChildAt(currentQuestionIndex);
    selectedQuestion.setTextColor(questionsMarkerColors.getTextActiveColor());

    questionId = getQuestionId();
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  private String getQuestionId() {
    QuestionnaireResult answer = (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
    return answer.getQuestionId();
  }

  private QuestionnaireResult getQuestionResult() {
    return (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
  }

  private void setButtonEnabledOrDisabled(QuestionnaireResult questionnaireResult) {
    if (questionnaireResult == null) {
      saveButton.disableClick();
      return;
    }
    if (questionnaireResult.isResultValid()) {
      saveButton.enableClick();
    } else {
      saveButton.disableClick();
    }
  }

  @Override
  public void onPageScrollStateChanged(int state) {
  }

  @Override
  public void onAnswerSelected(QuestionnaireResult result) {
    if (saveButton.isLoading()) {
      return;
    }
    if (result.isResultValid()) {
      saveButton.enableClick();
      answersList.clear();
      for (String answer : result.getResult()) {
        answersList.add(new AnswerParams(answer, Integer.valueOf(result.getQuestionId())));
      }
    } else {
      saveButton.disableClick();
    }
  }

  //end region

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
  }

  private void showButtonLoading() {
    saveButton.showLoadingState();
  }

  private void hideButtonLoading() {
    saveButton.showDefaultState();
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_gestation_conclusion_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    fetchQuestionnaire();
  }

  @Override
  public void onButtonClicked() {
    saveQuestion();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_gestation_conclusion_title));

    }
    ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
  }

  public void setPresenter(GestationConclusionContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_gestation_conclusion),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
