package br.com.taqtile.android.app.data.common.remote;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by taqtile on 3/3/17.
 */

public class RequestInterceptor implements Interceptor {
    private final String AUTHORIZATION_HEADER = "Authorization";
    private final String JSON_HEADER_KEY = "Content-Type", JSON_HEADER_VALUE = "application/json";

    private String userToken;

    public RequestInterceptor setAuthorizationTokenToHeader(String userToken) {
        this.userToken = userToken;
        return this;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();
        Request request = original.newBuilder().headers(buildHeaders(original)).build();

        return chain.proceed(request);
    }

    private Headers buildHeaders(Request original) {
        Headers.Builder builder = original.headers().newBuilder();

        builder.add(JSON_HEADER_KEY, JSON_HEADER_VALUE);

        if (userToken != null) {
            builder.add(AUTHORIZATION_HEADER, userToken);
        }

        return builder.build();
    }
}
