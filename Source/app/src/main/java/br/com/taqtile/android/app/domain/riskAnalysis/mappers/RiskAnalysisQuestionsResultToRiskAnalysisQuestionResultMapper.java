package br.com.taqtile.android.app.domain.riskAnalysis.mappers;

import java.util.HashMap;
import java.util.List;

import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemUnlockResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionsResult;
import br.com.taqtile.android.app.support.GlobalConstants;
import rx.Observable;

/**
 * Created by felipesabino on 5/17/17.
 */

public class RiskAnalysisQuestionsResultToRiskAnalysisQuestionResultMapper {

  public static Observable<RiskAnalysisQuestionResult> perform(RiskAnalysisQuestionsResult questionForm,
                                                               RiskAnalysisQuestionGroupResult group,
                                                               Integer previousId) {
    List<? extends QuestionItemResult> questions;
    switch (group.getName()) {
      case GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOU:
        questions = questionForm.getAboutYou();
        break;
      case GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_FAMILY:
        questions = questionForm.getAboutYourFamily();
        break;
      case GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_HEALTH:
        questions = questionForm.getAboutYourHealth();
        break;
      case GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_OBSTETRICAL_PAST:
        questions = questionForm.getAboutYourObstetricalPast();
        break;
      default:
        DomainError error = new DomainError();
        error.setErrorType(DomainError.INVALID_DATA);
        error.setMessage("Error ao obter perguntas para grupo '" + group.getName() + "'");
        return Observable.error(error);
    }

    Integer currentIndex = 0;
    QuestionItemResult nextQuestion = null;

    if (previousId >= 0) {

      HashMap<Integer, QuestionItemResult> questionsMap = new HashMap<>();

      Boolean hasMappedPreviousId = false;

      for (QuestionItemResult question : questions) {
        questionsMap.put(question.getId(), question);

        if (question.getId().equals(previousId)) {
          hasMappedPreviousId = true;
          // if found question with id, then skip to the next
          continue;
        }

        // on sequence, only after we already mapped the previous question and
        //  found the 'previousId' we can start actually checking the questions
        //  to see if they are valid to be the next one
        if (hasMappedPreviousId) {

          if (question.getDependsOnQuestionId() != null && question.getDependsOnQuestionId() > 0) {
            // If necessary, we check if this next question depends on previous answers
            QuestionItemResult dependent = questionsMap.get(question.getDependsOnQuestionId());
            QuestionItemUnlockResult answerUnlockTrigger = question.getAnswerUnlockTrigger();
            if (
              // If new rules other than "more than" are added, check them here
              answerUnlockTrigger.isMoreThan() &&
                Integer.parseInt(dependent.getAnswer().getAnswer()) > answerUnlockTrigger.getMoreThan()
              ) {
              nextQuestion = question;
              break;
            }
          } else {
            // If question does not depend on any answer, it is suitable to be the next one
            nextQuestion = question;
            break;
          }
        }
      }

    } else {
      nextQuestion = questions.get(0);
    }

    if (nextQuestion != null) {
      currentIndex = questions.indexOf(nextQuestion);
    }

    RiskAnalysisQuestionResult result = new RiskAnalysisQuestionResult();
    result.setTotalQuestionsForGroup(questions.size());
    result.setCurrentQuestionIndex(currentIndex);
    result.setQuestion(nextQuestion);
    result.setHasNextQuestion(nextQuestion != null);

    return Observable.just(result);
  }


}
