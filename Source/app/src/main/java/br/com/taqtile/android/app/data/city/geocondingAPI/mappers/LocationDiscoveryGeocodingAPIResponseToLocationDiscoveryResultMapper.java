package br.com.taqtile.android.app.data.city.geocondingAPI.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.city.geocondingAPI.models.AddressComponentGeocodingAPIResponse;
import br.com.taqtile.android.app.data.city.geocondingAPI.models.LocationDiscoveryGeocodingAPIResponse;
import br.com.taqtile.android.app.data.common.geocoding.models.BaseGeocodingResponse;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;

/**
 * Created by taqtile on 6/2/17.
 */

public class LocationDiscoveryGeocodingAPIResponseToLocationDiscoveryResultMapper {

  public static final String STATUS = "OK";
  public static final String TYPE_ZIP_CODE = "postal_code";
  public static final String TYPE_CITY = "administrative_area_level_2";
  public static final String TYPE_STATE = "administrative_area_level_1";

  public static LocationDiscoveryResult perform(
    BaseGeocodingResponse<List<LocationDiscoveryGeocodingAPIResponse>> baseGeocodingResponse) {
    String zipCode = "";
    String city = "";
    String state = "";
    String latitude = "";
    String longitude = "";

    if (baseGeocodingResponse.getStatus().equals(STATUS)) {

      LocationDiscoveryGeocodingAPIResponse result = baseGeocodingResponse.getResults().get(0);

      for (AddressComponentGeocodingAPIResponse addressComponent :
        result.getAddressComponents()) {

        switch (addressComponent.getTypes().get(0)) {
          case TYPE_ZIP_CODE:
            zipCode = addressComponent.getShortName();
            break;
          case TYPE_CITY:
            city = addressComponent.getShortName();
            break;
          case TYPE_STATE:
            state = addressComponent.getShortName();
            break;
        }
      }

      latitude = result.getGeometry().getGeographicCoordinate().getLatitude();
      longitude = result.getGeometry().getGeographicCoordinate().getLongitude();
    }

    return new LocationDiscoveryResult(
      zipCode,
      city,
      state,
      latitude != null ? latitude : "",
      longitude != null ? longitude : "");
  }
}
