package br.com.taqtile.android.app.domain.forum;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostAndRepliesResult;
import br.com.taqtile.android.app.domain.forum.models.WarnPostParams;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class WarnPostUseCase extends BaseUseCase<PostAndRepliesResult, WarnPostParams, ForumRepository> {

  public WarnPostUseCase(ForumRepository forumRepository) {
    super(forumRepository);
  }

  @Override
  public Observable<PostAndRepliesResult> execute(WarnPostParams warnPostParams) {
    return getRepository().warnPost(warnPostParams);
  }
}
