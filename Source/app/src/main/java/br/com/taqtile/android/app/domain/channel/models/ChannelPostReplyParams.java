package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostReplyParams {
  private Integer postId;
  private String reply;

  public ChannelPostReplyParams(Integer postId, String reply) {
    this.postId = postId;
    this.reply = reply;
  }

  public Integer getPostId() {
    return postId;
  }

  public String getReply() {
    return reply;
  }

}
