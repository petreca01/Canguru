package br.com.taqtile.android.app.data.channel.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostsListRemoteRequest extends BaseRemoteRequest {
    private Integer page;

    @RequestName("channel_id")
    private Integer channelId;

    @RequestName("per_page")
    private Integer perPage;

    @RequestName("search")
    private String search;

    @RequestName("follow")
    private Integer follow;

    @RequestName("unfollow")
    private Integer unfollow;

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setFollow(Integer follow) {
        this.follow = follow;
    }

    public void setUnfollow(Integer unfollow) {
        this.unfollow = unfollow;
    }
}