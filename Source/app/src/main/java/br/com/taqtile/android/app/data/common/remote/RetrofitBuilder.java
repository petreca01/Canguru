package br.com.taqtile.android.app.data.common.remote;

import java.util.concurrent.TimeUnit;

import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.common.remote.models.RemoteError;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by taqtile on 3/3/17.
 */

public class RetrofitBuilder<T> {

  public static final int TIMEOUT = 30;

  private boolean isAuthenticated;
  private Class<T> serviceClass = null;

  public RetrofitBuilder<T> withClass(final Class<T> serviceClass) {
    this.serviceClass = serviceClass;
    return this;
  }

  public RetrofitBuilder<T> withAuthentication() {
    this.isAuthenticated = true;
    return this;
  }

  public Observable<T> build() {
    if (serviceClass == null) {
      Observable.error(new RemoteError());
    }

    RequestInterceptor remoteInterceptor = new RequestInterceptor();

    return this.addUserTokenOnHeaderIfNeeded(remoteInterceptor)
      .map(any -> RetrofitHelper.getRetrofitBuilder()
        .client(new OkHttpClient.Builder()
          .readTimeout(TIMEOUT, TimeUnit.SECONDS)
          .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
          .addInterceptor(remoteInterceptor).build()))
      .map(Retrofit.Builder::build)
      .map(retrofit -> retrofit.create(this.serviceClass));
  }

  private Observable<EmptyResult> addUserTokenOnHeaderIfNeeded(RequestInterceptor remoteInterceptor) {
    if (!this.isAuthenticated) {
      return Observable.just(new EmptyResult());
    } else {
      return Toolbox.getInstance().getUserToken()
        .map(remoteInterceptor::setAuthorizationTokenToHeader)
        .map(any -> new EmptyResult());
    }
  }

}
