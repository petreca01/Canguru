package br.com.taqtile.android.app.presentation.birthplan;

import br.com.taqtile.android.app.domain.questionnaires.models.BirthPlanQuestionsViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 10/05/17.
 */

public interface BirthplanContract {
  interface Presenter extends BasePresenter {
    Observable<BirthPlanQuestionsViewModel> fetchBirthPlanQuestionAnswersList();

    Observable<BirthPlanQuestionsViewModel> sendBirthPlanQuestionAnswersList(String email);

    Observable<BirthPlanQuestionsViewModel> restartBirthPlanAnswers(String id);

  }
}
