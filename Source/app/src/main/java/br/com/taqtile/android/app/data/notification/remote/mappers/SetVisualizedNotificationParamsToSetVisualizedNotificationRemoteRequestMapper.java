package br.com.taqtile.android.app.data.notification.remote.mappers;

import br.com.taqtile.android.app.data.notification.remote.models.request.SetVisualizedNotificationRemoteRequest;
import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;

/**
 * Created by taqtile on 07/04/17.
 */

public class SetVisualizedNotificationParamsToSetVisualizedNotificationRemoteRequestMapper {

  public static SetVisualizedNotificationRemoteRequest perform(SetVisualizedNotificationParams setVisualizedNotificationParams) {
    return new SetVisualizedNotificationRemoteRequest(setVisualizedNotificationParams.getId());
  }
}
