package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 08/05/17.
 */

public interface MedicalRecommendationDetailContract {
  interface Presenter extends BasePresenter {
    Observable<EmptyResult> markAsRead(String id);
  }
}
