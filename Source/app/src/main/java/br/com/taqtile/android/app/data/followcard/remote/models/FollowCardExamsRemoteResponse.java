package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardExamsRemoteResponse {

  @SerializedName("id")
  private Integer id;

  @SerializedName("name")
  private String name;

  @SerializedName("result")
  private String result;

  @SerializedName("date")
  private String date;

  @SerializedName("created_at")
  private String createdAt;

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getResult() {
    return result;
  }

  public String getDate() {
    return date;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
