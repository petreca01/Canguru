package br.com.taqtile.android.app.data.professional;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.professional.remote.ProfessionalRemoteDataSource;
import br.com.taqtile.android.app.data.professional.remote.mappers.ProfessionalAuthorizationParamsToProfessionalAuthorizationRemoteRequestMapper;
import br.com.taqtile.android.app.data.professional.remote.mappers.ProfessionalInfoItemRemoteResponseListToProfessionalResultListMapper;
import br.com.taqtile.android.app.data.professional.remote.mappers.ProfessionalInfoItemRemoteResponseToProfessionalResultMapper;
import br.com.taqtile.android.app.data.professional.remote.models.request.ProfessionalAuthorizationRemoteRequest;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalAuthorizationParams;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfessionalRepository extends BaseRepository<ProfessionalRemoteDataSource, Void> {

  public ProfessionalRepository(ProfessionalRemoteDataSource professionalRemoteDataSource) {
    super(professionalRemoteDataSource, null);
  }

  public Observable<List<ProfessionalResult>> list() {
    return getRemoteDataSource().list()
      .map(result -> ProfessionalInfoItemRemoteResponseListToProfessionalResultListMapper.perform(
        result.getResult()));
  }

  public Observable<String> authorizeAccess(
    ProfessionalAuthorizationParams professionalAuthorizationParams) {

    return getRemoteDataSource().authorizedAccess(
      ProfessionalAuthorizationParamsToProfessionalAuthorizationRemoteRequestMapper.perform(
      professionalAuthorizationParams))
      .map(result ->
        result.getResult().getUpdatedAt());
  }

  public Observable<EmptyResult> revokeAccess(
    ProfessionalAuthorizationParams professionalAuthorizationParams) {

    return getRemoteDataSource().revokeAccess(
      ProfessionalAuthorizationParamsToProfessionalAuthorizationRemoteRequestMapper.perform(
      professionalAuthorizationParams))
      .map(result ->
        new EmptyResult());
  }
}
