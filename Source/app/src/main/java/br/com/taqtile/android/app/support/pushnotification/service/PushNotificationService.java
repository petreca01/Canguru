package br.com.taqtile.android.app.support.pushnotification.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.pushnotification.controller.PushNotificationContract;
import br.com.taqtile.android.app.support.pushnotification.controller.PushNotificationController;
import br.com.taqtile.android.cleanbase.push.notification.BaseGcmListenerService;

/**
 * Created by taqtile on 7/11/16.
 */

public class PushNotificationService extends BaseGcmListenerService implements PushNotificationContract.Service {

  private final static String MESSAGE = "message";
  private final static String TITLE = "title";
  private final static String ACTION = "action";
  private static final String RIGHT_ICON = "right_icon";
  private static final String ICON_ID = "id";
  private PushNotificationContract.Controller mController;
  public final static String PUSH_REDIRECTION = "pushRedirection";

  public PushNotificationService() {
    mController = new PushNotificationController(this);
  }

  public void processPushBundle(Bundle bundle) {
    //This is where the type of push must be determined
    String message = bundle.getString(MESSAGE);
    String title = bundle.getString(TITLE);
    String action = bundle.getString(ACTION);
    mController.simplePushReceived(message, title, action);
  }

  @Override
  public void showNotificationUI(String message, String title, String action) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra(PUSH_REDIRECTION, PUSH_REDIRECTION);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
      PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
      .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_canguru_icon))
      .setSmallIcon(R.mipmap.ic_canguru_notification)
      .setColor(ContextCompat.getColor(getBaseContext(), R.color.color_brand_orange))
      .setContentTitle(getString(R.string.app_name))
      .setContentText(message)
      .setAutoCancel(true)
      .setSound(defaultSoundUri)
      .setContentIntent(pendingIntent);

    NotificationManager notificationManager =
      (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    Notification notification = notificationBuilder.build();


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      int smallIconViewId = getResources().getIdentifier(RIGHT_ICON, ICON_ID, android.R.class.getPackage().getName());

      if (smallIconViewId != 0) {
        if (notification.contentView != null) {
          notification.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
        }

        if (notification.headsUpContentView != null) {
          notification.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
        }

        if (notification.bigContentView != null) {
          notification.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
        }
      }
    }

    notificationManager.notify(0, notification);

  }

}
