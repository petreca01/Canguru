package br.com.taqtile.android.app.presentation.connectedprofessionals;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.listings.cells.CellFactory;
import br.com.taqtile.android.app.listings.cells.ConnectedProfessionalsCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.manageprofessionals.ManagedProfessionalsBundleCreator;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;


/**
 * Created by taqtile on 19/04/17.
 */

public class ConnectedProfessionalsFragment extends AppBaseFragment
  implements MenuItem.OnMenuItemClickListener, OverflowButton.OverflowButtonListener {

  private final static int FIRST_MENU_ITEM_POSITION = 0;

  @BindView(R.id.fragment_connected_professionals_scrollview)
  ScrollView connectedProfessionalsScrollView;

  @BindView(R.id.fragment_connected_professionals_authorized_section_header_text)
  SectionHeader
    authorizedProfessionalSectionHeader;
  @BindView(R.id.fragment_connected_professionals_unauthorized_section_header_text)
  SectionHeader
    unauthorizedProfessionalSectionHeader;
  @BindView(R.id.fragment_connected_professionals_authorized_professionals_layout)
  LinearLayout
    authorizedProfessionalsLayout;
  @BindView(R.id.fragment_connected_professionals_unauthorized_professionals_layout)
  LinearLayout
    unauthorizedProfessionalsLayout;

  private ConnectedProfessionalsContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  private ArrayList<ConnectedProfessionalViewModel> connectedProfessionalViewModelsList;

  public static ConnectedProfessionalsFragment newInstance() {
    return new ConnectedProfessionalsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_connected_professionals, container, false);

    ButterKnife.bind(this, root);
    setHasOptionsMenu(true);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
  }

  @Override
  public void onResume() {
    super.onResume();
    removeConnectedProfessionals();
    setupToolbar();
    setupListeners();
    setupConnectedProfessionalsList();
    fetchProfessionals();
  }

  private void setupConnectedProfessionalsList() {
    connectedProfessionalViewModelsList = new ArrayList<>();
  }

  private void fetchProfessionals() {
    showLoading();
    Subscription subscription = presenter.fetchProfessionals()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchProfessionalsSuccess, this::onFetchProfessionalsFailure);

    compositeSubscription.add(subscription);
  }

  private void onFetchProfessionalsSuccess(List<ConnectedProfessionalViewModel> professionalViewModelList) {
    hideLoading();
    connectedProfessionalViewModelsList.clear();
    connectedProfessionalViewModelsList.addAll(professionalViewModelList);
    if (!professionalViewModelList.isEmpty()) {
      addProfessionals(professionalViewModelList);
    } else {
      showConnectedProfessionalsNotFound();
    }
  }

  private void showConnectedProfessionalsNotFound() {
    placeholder.setPlaceholderType(CustomPlaceholder.CONNECTED_PROFESSIONALS_NOT_FOUND);
    showPlaceholder();
    customToolbar.hideOptionsMenu();
  }

  private void addProfessionals(List<ConnectedProfessionalViewModel> professionalViewModels) {

    boolean hasAuthorizedProfessionals = false;
    boolean hasUnauthorizedProfessionals = false;

    for (ConnectedProfessionalViewModel connectedProfessionalViewModel : professionalViewModels) {
      addConnectedProfessionalWithPermission(connectedProfessionalViewModel);
      if (connectedProfessionalViewModel.getAuthorized()) {
        hasAuthorizedProfessionals = true;
      } else {
        hasUnauthorizedProfessionals = true;
      }
    }
    setHeadersVisibility(hasAuthorizedProfessionals, hasUnauthorizedProfessionals);
    if (hasAuthorizedProfessionals) {
      hideLastBottomDivider(authorizedProfessionalsLayout);
    }
    if (hasUnauthorizedProfessionals) {
      hideLastBottomDivider(unauthorizedProfessionalsLayout);
    }
    setupToolbar();
  }

  private void onFetchProfessionalsFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    customToolbar.hideOptionsMenu();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_connected_professionals_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchProfessionals();
    hidePlaceholder();
  }

  private void hideLastBottomDivider(LinearLayout layout) {
    int childCount = layout.getChildCount();
    ConnectedProfessionalsCell lastChild =
      (ConnectedProfessionalsCell) layout.getChildAt(childCount - 1);
    lastChild.hideSeparator();
  }

  private void setupListeners() {

  }

  private void setupView() {
    authorizedProfessionalSectionHeader.setText(getResources().getString(
      R.string.fragment_connected_professionals_authorized_professionals_section_text));
    unauthorizedProfessionalSectionHeader.setText(getResources().getString(
      R.string.fragment_connected_professionals_unauthorized_professionals_section_text));
  }

  public void setPresenter(ConnectedProfessionalsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(
        getResources().getString(R.string.fragment_connected_professionals_title), Gravity.LEFT);
      customToolbar.inflateOptionsMenu(R.menu.menu_overflow, this);
      customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION).setActionView(getOverflowButton());
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private OverflowButton getOverflowButton() {
    OverflowButton overflowButton = new OverflowButton(getContext());
    setupOverflowButton(overflowButton);
    return overflowButton;
  }

  private void setupOverflowButton(OverflowButton overflowButton) {
    List<String> overflowOptions = new ArrayList();
    overflowOptions.add(getResources().getString(
      R.string.fragment_connected_professionals_overflow_button_manage_access_text));
    overflowButton.onMenuItemSelected(this);
    overflowButton.setMenuList(overflowOptions);
  }

  private void addConnectedProfessionalWithPermission(
    ConnectedProfessionalViewModel connectedProfessionalViewModel) {

    ConnectedProfessionalsCell connectedProfessionalsCell =
      CellFactory.getConnectedProfessionalsCell(getContext(), connectedProfessionalViewModel);

    addCellsToLayouts(connectedProfessionalsCell, connectedProfessionalViewModel.getAuthorized());
  }

  private void addCellsToLayouts(ConnectedProfessionalsCell connectedProfessionalsCell,
                                 boolean isAuthorized) {
    if (isAuthorized) {
      authorizedProfessionalsLayout.addView(connectedProfessionalsCell);
    } else {
      unauthorizedProfessionalsLayout.addView(connectedProfessionalsCell);
    }
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    if (item.getItemId() == R.id.menu_overflow) {
    }
    return true;
  }

  @Override
  public void onItemSelected(String item) {
    Bundle bundle = ManagedProfessionalsBundleCreator.getBundle(connectedProfessionalViewModelsList);
    navigationManager.showManagedProfessionalsUI(bundle);
  }

  private void removeConnectedProfessionals() {
    removeChildViews(authorizedProfessionalsLayout);
    removeChildViews(unauthorizedProfessionalsLayout);
  }

  private void removeChildViews(LinearLayout linearLayout) {
    for (int i = 0; i < linearLayout.getChildCount(); i++) {
      View view = linearLayout.getChildAt(i);
      if (view instanceof ConnectedProfessionalsCell) {
        linearLayout.removeView(view);
      }
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  private void setHeadersVisibility(boolean hasAuthorizedProfessionals, boolean hasUnauthorizedProfessionals) {
    authorizedProfessionalsLayout.setVisibility(hasAuthorizedProfessionals ? View.VISIBLE : View.GONE);
    authorizedProfessionalSectionHeader.setVisibility(hasAuthorizedProfessionals ? View.VISIBLE : View.GONE);
    unauthorizedProfessionalsLayout.setVisibility(hasUnauthorizedProfessionals ? View.VISIBLE : View.GONE);
    unauthorizedProfessionalSectionHeader.setVisibility(hasUnauthorizedProfessionals ? View.VISIBLE : View.GONE);
  }

  @Override
  public void showLoading() {
    super.showLoading();
    connectedProfessionalsScrollView.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    connectedProfessionalsScrollView.setVisibility(View.VISIBLE);
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_connected_professionals),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
