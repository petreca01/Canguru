package br.com.taqtile.android.app.data.clinicalcondition.remote.mappers;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.request.ReportConditionRemoteRequest;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;

/**
 * Created by taqtile on 4/12/17.
 */

public class ReportConditionParamsToReportConditionRemoteRequestMapper {
  public static ReportConditionRemoteRequest perform(ReportConditionParams params) {
    return new ReportConditionRemoteRequest(params.getConditionId());
  }
}
