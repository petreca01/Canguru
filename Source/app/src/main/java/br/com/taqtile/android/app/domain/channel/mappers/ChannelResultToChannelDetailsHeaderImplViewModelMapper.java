package br.com.taqtile.android.app.domain.channel.mappers;

import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsHeaderImplViewModel;

/**
 * Created by filipe.mp on 4/10/17.
 */

class ChannelResultToChannelDetailsHeaderImplViewModelMapper {

  public static ChannelDetailsHeaderImplViewModel perform(ChannelResult channelResult) {
    return new ChannelDetailsHeaderImplViewModel(String.valueOf(channelResult.getId()),
      channelResult.getLogo(), channelResult.getName(), channelResult.getDescription(),
      channelResult.getFollowing(), channelResult.getTotalPosts());
  }
}
