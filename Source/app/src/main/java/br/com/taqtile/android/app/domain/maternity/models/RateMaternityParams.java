package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class RateMaternityParams extends MaternityParams {
  private Integer rate;

  public RateMaternityParams(Integer rate, Integer maternityId) {
    super(maternityId);
    this.rate = rate;
  }

  public Integer getRate() {
    return rate;
  }

}
