package br.com.taqtile.android.app.domain.medicalRecommendations;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.medicalRecommendations.MedicalRecommendationsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationParams;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class MarkAsReadUseCase
  extends BaseUseCase<EmptyResult, MedicalRecommendationParams, MedicalRecommendationsRepository> {

  public MarkAsReadUseCase(MedicalRecommendationsRepository medicalRecommendationsRepository) {
    super(medicalRecommendationsRepository);
  }

  @Override
  public Observable<EmptyResult> execute(MedicalRecommendationParams medicalRecommendationParams) {
    return getRepository().markAsRead(medicalRecommendationParams)
      .map(medicalRecommendationResult -> new EmptyResult());
  }
}
