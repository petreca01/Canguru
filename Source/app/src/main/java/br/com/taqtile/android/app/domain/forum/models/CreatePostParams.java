package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 4/17/17.
 */

public class CreatePostParams {

  private String title;
  private String content;

  public CreatePostParams(String title, String content) {
    this.title = title;
    this.content = content;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }
}
