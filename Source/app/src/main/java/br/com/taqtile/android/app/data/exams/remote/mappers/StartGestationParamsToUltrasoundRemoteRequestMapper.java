package br.com.taqtile.android.app.data.exams.remote.mappers;

import br.com.taqtile.android.app.data.exams.remote.models.request.UltrasoundRemoteRequest;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;

/**
 * Created by taqtile on 19/04/17.
 */

public class StartGestationParamsToUltrasoundRemoteRequestMapper {

  public static UltrasoundRemoteRequest perform(StartOrEditGestationParams startOrEditGestationParams) {
    return new UltrasoundRemoteRequest(startOrEditGestationParams.getGestationAgeDays(),
      startOrEditGestationParams.getGestationAgeWeeks(),
      startOrEditGestationParams.getUltrasoundDate());
  }
}
