package br.com.taqtile.android.app.data.notification.remote.mappers;

import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import br.com.taqtile.android.app.domain.notification.models.NotificationResult;

/**
 * Created by taqtile on 07/04/17.
 */

public class NotificationItemRemoteResponseToNotificationResultMapper {

  public static NotificationResult perform(
    NotificationItemRemoteResponse notificationItemRemoteResponse) {
    return new NotificationResult(notificationItemRemoteResponse.getLoaded(),
      notificationItemRemoteResponse.getId(), notificationItemRemoteResponse.getUsuarioId(),
      notificationItemRemoteResponse.getDoctorId(), notificationItemRemoteResponse.getOperatorId(),
      notificationItemRemoteResponse.getType(), notificationItemRemoteResponse.getImportant(),
      notificationItemRemoteResponse.getContent(), notificationItemRemoteResponse.getEntity(),
      notificationItemRemoteResponse.getEntityId(), notificationItemRemoteResponse.getAction(),
      notificationItemRemoteResponse.getUserPerformerId(),
      notificationItemRemoteResponse.getVisualized(),
      notificationItemRemoteResponse.getVisualizedAt(),
      notificationItemRemoteResponse.getCreatedAt(), notificationItemRemoteResponse.getUpdatedAt(),
      notificationItemRemoteResponse.getUserPerformerName(),
      notificationItemRemoteResponse.getUserPerformerPicture());
  }
}
