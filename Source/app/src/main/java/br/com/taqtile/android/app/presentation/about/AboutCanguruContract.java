package br.com.taqtile.android.app.presentation.about;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 20/07/17.
 */

public interface AboutCanguruContract {

  interface Presenter extends BasePresenter {
  }

}
