package br.com.taqtile.android.app.domain.medicalRecommendations.mappers;

import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationResult;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 09/05/17.
 */

public class MedicalRecommendationResultToViewModel {

  public static List<MedicalRecommendationViewModel> perform(
    List<MedicalRecommendationResult> medicalRecommendationResultList) {
    return StreamSupport.stream(medicalRecommendationResultList)
      .map(MedicalRecommendationResultToViewModel::map)
      .collect(Collectors.toList());
  }

  private static MedicalRecommendationViewModel map(
    MedicalRecommendationResult medicalRecommendationResult) {
    boolean read = medicalRecommendationResult.getAcknowledgedAt() != null;

    return new MedicalRecommendationViewModel(String.valueOf(medicalRecommendationResult.getId()),
      medicalRecommendationResult.getName(), medicalRecommendationResult.getDoctorName(),
      medicalRecommendationResult.getRecommendedAt(), medicalRecommendationResult.getContent(),
      read);
  }
}
