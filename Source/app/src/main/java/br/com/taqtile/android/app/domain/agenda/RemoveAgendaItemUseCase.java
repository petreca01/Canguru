package br.com.taqtile.android.app.domain.agenda;

import br.com.taqtile.android.app.data.agenda.AgendaRepository;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class RemoveAgendaItemUseCase extends BaseUseCase<String, RemoveAppointmentParams,
  AgendaRepository> {

  public RemoveAgendaItemUseCase(AgendaRepository agendaRepository) {
    super(agendaRepository);
  }

  @Override
  public Observable<String> execute(RemoveAppointmentParams removeAppointmentParams) {
    return getRepository().remove(removeAppointmentParams);
  }
}
