package br.com.taqtile.android.app.domain.channel;

import br.com.taqtile.android.app.domain.channel.models.ChannelCategoryResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import java.util.List;
import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class ChannelListUseCase extends BaseUseCase<ChannelCategoryResult, Void, ChannelRepository> {

    public ChannelListUseCase(ChannelRepository channelRepository) {
        super(channelRepository);
    }

    @Override
    public Observable<ChannelCategoryResult> execute(Void aVoid) {
      return getRepository().list();
    }

}
