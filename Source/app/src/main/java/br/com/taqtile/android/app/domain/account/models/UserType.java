package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 4/7/17.
 */

public class UserType {
  public static final String planning = "planning";
  public static final String pregnant = "pregnant";
}
