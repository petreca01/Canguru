package br.com.taqtile.android.app.data.weeklycontent.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentRemoteResponse {

  @SerializedName("loaded")
  private Boolean loaded;

  @SerializedName("id")
  private Integer id;

  @SerializedName("week")
  private Integer week;

  @SerializedName("image_url")
  private String image_url;

  @SerializedName("content")
  private String content;

  public Integer getWeek() {
    return week;
  }

  public String getImage_url() {
    return image_url;
  }

  public String getContent() {
    return content;
  }
}
