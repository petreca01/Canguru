package br.com.taqtile.android.app.presentation.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.CallbackManager;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.initiallogin.InitialLoginActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 5/24/16.
 */

public class SignInActivity extends TemplateBackActivity {

  private SignInFragment fragment;
  public static int SIGN_IN_REQUEST_CODE = 12345;

  private CallbackManager callbackManager;
  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private int onActivityRequestCode;
  private int onActivityResultCode;
  private Bundle onActivityResultBundle;


  @Override
  public SignInFragment getFragment() {
    SignInFragment signInFragment = (SignInFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (signInFragment == null) {
      signInFragment = SignInFragment.newInstance();
    }
    fragment = signInFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    SignInPresenter signInPresenter = new SignInPresenter();
    fragment.setPresenter(signInPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return signInPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, SignInActivity.class);
    context.startActivity(intent);
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {

    Intent intent = new Intent(context, SignInActivity.class);

    if (context instanceof InitialLoginActivity) {
      ((InitialLoginActivity) context).setResultListener(listener,
        SIGN_IN_REQUEST_CODE);
      ((InitialLoginActivity) context).startActivityForResult(intent,
        SIGN_IN_REQUEST_CODE);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (callbackManager != null) {
      callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    if (requestCode == observedRequestCode
      && resultListener != null) {
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
      onActivityResultBundle = data != null ? data.getExtras() : new Bundle();
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }

}
