package br.com.taqtile.android.app.domain.symptoms.models;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class ReportSymptomParams {
  private Integer symptomId;
  private String date;
  private String description;

  public ReportSymptomParams(Integer symptomId, String date) {
    this.symptomId = symptomId;
    this.date = date;
  }

  public ReportSymptomParams(String description, String date) {
    this.date = date;
    this.description = description;
  }

  public Integer getSymptomId() {
    return symptomId;
  }

  public String getDate() {
    return date;
  }

  public String getDescription() { return description; }
}
