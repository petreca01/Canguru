package br.com.taqtile.android.app.data.facebook.remote.models;

/**
 * Created by taqtile on 26/04/17.
 */

public class FacebookUserResponse {
  private String name;
  private String email;
  private String token;

  public FacebookUserResponse(String name, String email, String token) {
    this.email = email;
    this.name = name;
    this.token = token;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getToken() {
    return token;
  }
}
