package br.com.taqtile.android.app.domain.healthOperators.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.healthOperators.models.HealthOperatorItemResult;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/25/17.
 */

public class HealthOperatorItemResultToViewModelMapper {

  public static List<HealthOperatorViewModel> perform(
    List<HealthOperatorItemResult> healthOperatorItemResults) {

    return StreamSupport.stream(healthOperatorItemResults)
      .map(HealthOperatorItemResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static HealthOperatorViewModel map(
    HealthOperatorItemResult healthOperatorItemResult) {

    return new HealthOperatorViewModel(healthOperatorItemResult.getId(),
      healthOperatorItemResult.getName(), healthOperatorItemResult.getAbout(),
      healthOperatorItemResult.getLogo(), healthOperatorItemResult.getHealthPlanActive(),
      healthOperatorItemResult.getDocumentType());
  }
}
