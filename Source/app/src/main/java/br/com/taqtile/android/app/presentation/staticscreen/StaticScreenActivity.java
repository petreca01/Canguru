package br.com.taqtile.android.app.presentation.staticscreen;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;

import java.lang.annotation.Retention;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static android.R.attr.id;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by taqtile on 18/04/17.
 */

public class StaticScreenActivity extends TemplateBackActivity {

  private StaticScreenFragment fragment;
  private static
  @StaticScreenPage
  String typePage;

  @Retention(SOURCE)
  @StringDef({
    ABOUT_CANGURU,
    PRIVACY_POLICY,
    USE_TERMS
  })
  public @interface StaticScreenPage {
  }

  public static final String ABOUT_CANGURU = "about_canguru";
  public static final String PRIVACY_POLICY = "privacy_policy";
  public static final String USE_TERMS = "use_terms";

  @Override
  public Fragment getFragment() {
    StaticScreenFragment staticScreenFragment = (StaticScreenFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());

    if (typePage != null && staticScreenFragment == null) {
      if (typePage.equalsIgnoreCase(ABOUT_CANGURU)) {
        staticScreenFragment = StaticScreenFragment.newInstance(StaticScreenFragment.ABOUT_CANGURU);
      } else if (typePage.equalsIgnoreCase(PRIVACY_POLICY)) {
        staticScreenFragment = StaticScreenFragment.newInstance(StaticScreenFragment.PRIVACY_POLICY);
      } else if (typePage.equalsIgnoreCase(USE_TERMS)) {
        staticScreenFragment = StaticScreenFragment.newInstance(StaticScreenFragment.USE_TERMS);
      } else {
        staticScreenFragment = StaticScreenFragment.newInstance(StaticScreenFragment.ABOUT_CANGURU);
      }
    }
    fragment = staticScreenFragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    StaticScreenPresenter presenter = new StaticScreenPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(presenter);

    return presenter;
  }

  public static void navigate(Context context, @StaticScreenPage String typePage) {
    StaticScreenActivity.typePage = typePage;
    Intent intent = new Intent(context, StaticScreenActivity.class);
    context.startActivity(intent);
  }
}
