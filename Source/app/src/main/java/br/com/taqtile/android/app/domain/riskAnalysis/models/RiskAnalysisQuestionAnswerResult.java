package br.com.taqtile.android.app.domain.riskAnalysis.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class RiskAnalysisQuestionAnswerResult {

  private String answer;

  private Integer answerRiskLevel;

  private String createdAt;

  public RiskAnalysisQuestionAnswerResult(String answer, Integer answerRiskLevel, String createdAt) {
    this.answer = answer;
    this.answerRiskLevel = answerRiskLevel;
    this.createdAt = createdAt;
  }

  public String getAnswer() {
    return answer;
  }

  public Integer getAnswerRiskLevel() {
    return answerRiskLevel;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
