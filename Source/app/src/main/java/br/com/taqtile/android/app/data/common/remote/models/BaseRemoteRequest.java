package br.com.taqtile.android.app.data.common.remote.models;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.RequestName;

/**
 * Created by taqtile on 3/17/17.
 */

public class BaseRemoteRequest implements RemoteRequestModel {

  @Override
  public Map<String, String> getFormData() {
    Map<String, String> formData = new HashMap();
    Field[] properties = this.getClass().getDeclaredFields();
    for (Field property : properties) {
      property.setAccessible(true);
      String key = getFormDataKey(property);
      String value = getFormDataValue(property);

      if (key == null || value == null || key.isEmpty() ) {
        continue;
      }

      formData.put(key, value);
    }
    return formData;
  }

  protected String getFormDataKey(Field property) {

    if (property == null) {
      return null;
    }

    Annotation annotation = property.getAnnotation(RequestName.class);
    RequestName customName = (RequestName) annotation;
    String formDataKey = customName != null ? customName.value() : property.getName();
    return formDataKey;
  }

  private String getFormDataValue(Field property) {

    if (property == null) {
      return null;
    }

    String formDataValue;
    try {
      formDataValue = property.get(this).toString();
    } catch (Exception e) {
      formDataValue = null;
    }
    return formDataValue;
  }

}
