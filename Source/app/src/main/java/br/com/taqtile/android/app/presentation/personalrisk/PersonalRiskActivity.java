package br.com.taqtile.android.app.presentation.personalrisk;

import android.content.Context;
import android.content.Intent;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by Renato on 4/17/17.
 */

public class PersonalRiskActivity extends TemplateBackActivity {

  private PersonalRiskFragment fragment;

  @Override
  public PersonalRiskFragment getFragment() {
    PersonalRiskFragment channelFragment = (PersonalRiskFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (channelFragment == null) {
      channelFragment = PersonalRiskFragment.newInstance();
    }
    fragment = channelFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    PersonalRiskPresenter channelPresenter = new PersonalRiskPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(channelPresenter);

    return channelPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, PersonalRiskActivity.class);
    context.startActivity(intent);
  }
}
