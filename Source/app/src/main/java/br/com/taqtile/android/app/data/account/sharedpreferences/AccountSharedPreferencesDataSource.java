package br.com.taqtile.android.app.data.account.sharedpreferences;

import android.support.annotation.NonNull;

import br.com.taqtile.android.app.data.common.prefser.PrefserFactory;
import br.com.taqtile.android.app.data.common.prefser.PrefserWrapper;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.support.GlobalConstants;

import com.fernandocejas.arrow.optional.Optional;

import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class AccountSharedPreferencesDataSource {
  private PrefserWrapper prefserWrapper;

  public AccountSharedPreferencesDataSource() {
    prefserWrapper = new PrefserWrapper(PrefserFactory.build());
  }

  public Observable<EmptyResult> saveUserZipCode(String userZipCode) {
    return prefserWrapper.putString(GlobalConstants.USER_ZIP_CODE_KEY, userZipCode);
  }

  public Observable<EmptyResult> saveUserHasCPFAndPhoneNumber(boolean userHasCPFAndPhoneNumber) {
    return prefserWrapper.putBoolean(GlobalConstants.USER_HAS_CPF_AND_PHONE_NUMBER_KEY, userHasCPFAndPhoneNumber);
  }

  public Observable<EmptyResult> saveUserId(Integer userId) {
    return prefserWrapper.putInteger(GlobalConstants.USER_ID_KEY, userId);
  }

  public Observable<EmptyResult> saveUserName(String userName) {
    return prefserWrapper.putString(GlobalConstants.USER_NAME_KEY, userName);
  }

  public Observable<EmptyResult> saveProfilePictureURL(String profilePictureURL) {
    return prefserWrapper.putString(GlobalConstants.USER_PROFILE_PICTURE_URL_KEY, profilePictureURL);
  }

  public Observable<EmptyResult> saveToken(@NonNull Optional<String> token) {
    return prefserWrapper.putOptionalString(GlobalConstants.USER_TOKEN_KEY, token);
  }

  public Observable<EmptyResult> saveUserEmail(String email) {
    return prefserWrapper.putString(GlobalConstants.USER_EMAIL_KEY, email);
  }

  public Observable<EmptyResult> logout() {
    return prefserWrapper.remove(GlobalConstants.USER_TOKEN_KEY)
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_ID_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_NAME_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_PROFILE_PICTURE_URL_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_HAS_CPF_AND_PHONE_NUMBER_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_ZIP_CODE_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.NOTIFICATION_CACHE_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.LAST_SEEN_NOTIFICATION_ID_KEY))
      .flatMap(any -> prefserWrapper.remove(GlobalConstants.USER_EMAIL_KEY));
  }

}
