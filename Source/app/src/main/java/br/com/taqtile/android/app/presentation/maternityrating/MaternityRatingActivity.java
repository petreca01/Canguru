package br.com.taqtile.android.app.presentation.maternityrating;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.presentation.maternitydetail.MaternityDetailActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.EDIT_GESTATION_CODE;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityRatingActivity extends TemplateBackActivity {

  private MaternityRatingFragment fragment;
  public static final String MATERNITY_TAG = "maternity";

  @Override
  public Fragment getFragment() {
    MaternityRatingFragment maternityRatingFragment =
      (MaternityRatingFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (maternityRatingFragment == null) {
      maternityRatingFragment = MaternityRatingFragment.newInstance();
    }
    fragment = maternityRatingFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    MaternityRatingPresenter maternityRatingPresenter = new MaternityRatingPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(maternityRatingPresenter);

    return maternityRatingPresenter;
  }

  public static void navigate(Context context, MaternityViewModel maternityViewModel,
                              CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, MaternityRatingActivity.class);
    intent.putExtra(MATERNITY_TAG, maternityViewModel);

    if (context instanceof MaternityDetailActivity) {
      ((MaternityDetailActivity) context).setResultListener(listener,
        EDIT_GESTATION_CODE);
      ((MaternityDetailActivity) context).startActivityForResult(intent,
        EDIT_GESTATION_CODE);
    }
  }
}
