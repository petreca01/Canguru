package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityParams {
  private Integer maternityId;

  public MaternityParams(Integer maternityId) {
    this.maternityId = maternityId;
  }

  public Integer getMaternityId() {
    return maternityId;
  }
}
