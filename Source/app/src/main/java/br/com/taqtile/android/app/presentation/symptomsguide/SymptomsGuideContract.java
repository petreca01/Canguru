package br.com.taqtile.android.app.presentation.symptomsguide;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 28/04/17.
 */

public interface SymptomsGuideContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> fetchUserData();

    Observable<List<SymptomViewModel>> listSymptoms(Integer gestationWeeks);

  }
}
