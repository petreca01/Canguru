package br.com.taqtile.android.app.data.facebook;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.facebook.remote.FacebookRemoteDataSource;
import br.com.taqtile.android.app.data.facebook.remote.mappers.FacebookUserResponseToFacebookUserResultMapper;
import br.com.taqtile.android.app.domain.signin.Models.FacebookUserResult;
import rx.Observable;

/**
 * Created by taqtile on 3/29/17.
 */

public class FacebookRepository extends BaseRepository<FacebookRemoteDataSource, Void> {

  public FacebookRepository(FacebookRemoteDataSource facebookRemoteDataSource) {
    super(facebookRemoteDataSource, null);
  }

  public Observable<String> requestFacebookPermissions() {
    return getRemoteDataSource().requestFacebookPermissions();
  }

  public Observable<FacebookUserResult> getUser() {
    return getRemoteDataSource().getUser()
      .map(FacebookUserResponseToFacebookUserResultMapper::perform);
  }

}
