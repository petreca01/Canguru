package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerUnlockTriggerRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemUnlockResult;

/**
 * Created by felipesabino on 5/18/17.
 */

public class AnswerUnlockTriggerRemoteResponseToQuestionItemUnlockResultMapper {
  public static QuestionItemUnlockResult perform(AnswerUnlockTriggerRemoteResponse param) {
    if (param != null) {
      return new QuestionItemUnlockResult(param.getMoreThan());
    } else {
      return null;
    }
  }
}
