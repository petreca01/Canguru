package br.com.taqtile.android.app.presentation.symptomsguide;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.listings.adapters.SymptomsGuideAdapter;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailBundleCreator;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.listings.Constants.SYMPTOMS_GUIDE_SYMPTOM;

/**
 * Created by taqtile on 28/04/17.
 */

public class SymptomsGuideFragment extends AppBaseFragment{

  public final static int OTHER_SYMPTOM_FAKE_ID = -1;

  @BindView(R.id.fragment_symptoms_guide_recycler_view)
  RecyclerView symptomsGuideRecyclerView;

  private NavigationManager navigationManager;
  private SymptomsGuideContract.Presenter presenter;
  private CustomToolbar customToolbar;

  protected SymptomsGuideAdapter symptomsGuideAdapter;
  protected LinearLayoutManager linearLayoutManager;
  protected List<SymptomViewModel> symptomsList;

  public static SymptomsGuideFragment newInstance() {
    return new SymptomsGuideFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_symptoms_guide, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupRecyclerView();
    fetchUserData();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void fetchUserData() {
    showLoading();
    symptomsGuideRecyclerView.setVisibility(View.GONE);
    Subscription checkUserDataSubscription = presenter.fetchUserData()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchUserDataSuccess,
        this::onFetchUserDataFailure);
    compositeSubscription.add(checkUserDataSubscription);
  }

  private void onFetchUserDataSuccess(UserViewModel userDataViewModel) {
    loadSymptoms(userDataViewModel.getGestationWeeks());
  }

  private void onFetchUserDataFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
    showPlaceholder();
  }

  private void loadSymptoms(Integer gestationWeeks) {
    showLoading();
    Subscription symptomsListSubscription = presenter.listSymptoms(gestationWeeks)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onListSuccess,
        this::onListFailure);
    compositeSubscription.add(symptomsListSubscription);
  }

  private void onListSuccess(List<SymptomViewModel> symptomViewModels) {
    SubSymptomViewModel subSymptom = new SubSymptomViewModel(OTHER_SYMPTOM_FAKE_ID, OTHER_SYMPTOM_FAKE_ID, getString(R.string.fragment_symptoms_others), "",
      "", "");
    symptomViewModels.add(new SymptomViewModel(subSymptom, null));
    symptomsGuideAdapter.setSymptomsList(symptomViewModels);
    symptomsGuideAdapter.notifyDataSetChanged();
    hideLoading();
  }

  private void onListFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
    showPlaceholder();
  }

  private void setupToolbar() {
    if (getActivity() != null && customToolbar == null) {
      customToolbar = getToolbar(CustomToolbar.class);
    }
    customToolbar.setTitle(getString(R.string.fragment_symptoms_guide_title));
  }

  private void setupRecyclerView() {
    symptomsList = new ArrayList<>();
    linearLayoutManager = new LinearLayoutManager(getContext());
    symptomsGuideRecyclerView.setLayoutManager(linearLayoutManager);
    symptomsGuideAdapter = new SymptomsGuideAdapter(symptomsList, getContext());
    symptomsGuideAdapter.setListener(this::onSymptomCellClicked);
    symptomsGuideRecyclerView.setAdapter(symptomsGuideAdapter);
    symptomsGuideRecyclerView.getRecycledViewPool().setMaxRecycledViews(SYMPTOMS_GUIDE_SYMPTOM, 0);
  }

  private void onSymptomCellClicked(SubSymptomViewModel subSymptomViewModel) {
    if (subSymptomViewModel.getId() == OTHER_SYMPTOM_FAKE_ID) {
      navigationManager.showSymptomEditUI(this::returningFromDetails);
    }
    else {
      Bundle bundle = SymptomDetailBundleCreator.getBundle(subSymptomViewModel);
      navigationManager.showSymptomDetailUI(this::returningFromDetails, bundle);
    }
  }

  private void returningFromDetails(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      fetchUserData();
      showSnackbar(getString(R.string.fragment_symptom_detail_report_sucess), Snackbar.LENGTH_LONG);
    }
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    fetchUserData();
  }

  public void setPresenter(SymptomsGuideContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    symptomsGuideRecyclerView.setVisibility(View.VISIBLE);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_symptoms_guide_placeholder;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_symptom_guide),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
