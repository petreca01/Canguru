package br.com.taqtile.android.app.data.clinicalcondition.remote.models.response;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;

/**
 * Created by taqtile on 3/23/17.
 */

public class ReportClinicalConditionRemoteResponse {
    @SerializedName("user")
    private UserRemoteResponse user;

    @SerializedName("user_condition")
    private UserClinicalConditionRemoteResponse userCondition;

    public UserRemoteResponse getUser() {
        return user;
    }

    public UserClinicalConditionRemoteResponse getUserCondition() {
        return userCondition;
    }
}
