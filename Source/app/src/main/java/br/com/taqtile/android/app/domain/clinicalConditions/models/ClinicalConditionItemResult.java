package br.com.taqtile.android.app.domain.clinicalConditions.models;

/**
 * Created by taqtile on 4/10/17.
 */

public class ClinicalConditionItemResult {
  private Integer id;
  private Integer parentId;
  private String name;
  private String logic;
  private String logicTrigger;
  private Integer risk;
  private String content;

  public ClinicalConditionItemResult(Integer id, Integer parentId, String name, String logic,
                                     String logicTrigger, Integer risk, String content) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
    this.logic = logic;
    this.logicTrigger = logicTrigger;
    this.risk = risk;
    this.content = content;
  }

  public Integer getId() {
    return id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public String getName() {
    return name;
  }

  public String getLogic() {
    return logic;
  }

  public String getLogicTrigger() {
    return logicTrigger;
  }

  public Integer getRisk() {
    return risk;
  }

  public String getContent() {
    return content;
  }

}
