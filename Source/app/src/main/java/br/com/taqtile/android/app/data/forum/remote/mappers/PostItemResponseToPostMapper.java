package br.com.taqtile.android.app.data.forum.remote.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.CategoryItemResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostItemResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostListResponse;
import br.com.taqtile.android.app.domain.forum.models.CategoryItem;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 3/21/17.
 */

public class PostItemResponseToPostMapper {
  public static List<PostResult> perform
    (BaseRemoteResponse<PostListResponse> response) {
    PostListResponse forumListResponse = response.getResult();
    List<PostItemResponse> responseList = forumListResponse.getPosts();

    return perform(responseList);
  }

  public static List<PostResult> perform(List<PostItemResponse> postsResponse) {

    if (postsResponse == null) {
      return new ArrayList<>();
    }

    return StreamSupport.stream(postsResponse)
      .filter(PostItemResponseToPostMapper::isValidForumPost)
      .map(PostItemResponseToPostMapper::perform)
      .collect(Collectors.toList());
  }

  private static Boolean isValidForumPost(PostItemResponse forumPost) {
    return forumPost.getId() != null &&
      forumPost.getUsername() != null &&
      forumPost.getCreatedAt() != null &&
      forumPost.getTitle() != null &&
      forumPost.getContent() != null &&
      forumPost.getLikes() != null &&
      forumPost.getCountReplies() != null;
  }

  private static PostResult perform(PostItemResponse responseForumPost) {
    PostResult post = new PostResult(
      responseForumPost.getId(),
      responseForumPost.getOperatorId(),
      responseForumPost.getUserId(),
      responseForumPost.getTitle(),
      responseForumPost.getContent(),
      responseForumPost.getActive(),
      responseForumPost.getCreatedAt(),
      responseForumPost.getUpdatedAt(),
      responseForumPost.getUserType(),
      responseForumPost.getUsername(),
      responseForumPost.getUserPicture(),
      responseForumPost.getLikes(),
      responseForumPost.getCountReplies(),
      responseForumPost.getLiked(),
      mapCategoriesRemoteResponse(responseForumPost.getCategories()));
    return post;
  }

  private static List<CategoryItem> mapCategoriesRemoteResponse(List<CategoryItemResponse> categoriesListResponse) {
    return StreamSupport.stream(categoriesListResponse)
      .map(PostItemResponseToPostMapper::perform)
      .collect(Collectors.toList());
  }

  private static CategoryItem perform(CategoryItemResponse responseCategorty) {
    return new CategoryItem(responseCategorty.getId(), responseCategorty.getName());
  }
}
