package br.com.taqtile.android.app.presentation.manageprofessionals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 25/04/17.
 */

public class ManageProfessionalsActivity extends TemplateBackActivity {

  private ManageProfessionalsFragment fragment;

  @Override public Fragment getFragment() {
    ManageProfessionalsFragment manageProfessionalsFragment =
      (ManageProfessionalsFragment) getSupportFragmentManager().findFragmentById(
        getFragmentContainerId());
    if (fragment == null) {
      manageProfessionalsFragment = ManageProfessionalsFragment.newInstance();
    }
    fragment = manageProfessionalsFragment;

    ManagedProfessionalsBundleAccessor managedProfessionalsBundleAccessor =
      new ManagedProfessionalsBundleAccessor(getIntent().getExtras());

    fragment.setManagedProfessionalsBundleAccessor(managedProfessionalsBundleAccessor);

    return fragment;
  }

  @Override public BasePresenter getPresenter() {
    ManageProfessionalsPresenter manageProfessionalsPresenter = new ManageProfessionalsPresenter();
    fragment.setPresenter(manageProfessionalsPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return manageProfessionalsPresenter;
  }

  public static void navigate(Context context, Bundle bundle) {
    Intent intent = new Intent(context, ManageProfessionalsActivity.class);
    intent.putExtras(bundle);
    context.startActivity(intent);
  }
}
