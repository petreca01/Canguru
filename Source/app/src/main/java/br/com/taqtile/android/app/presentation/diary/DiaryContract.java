package br.com.taqtile.android.app.presentation.diary;

import java.util.List;

import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.diary.model.DiaryViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 04/05/17.
 */

public interface DiaryContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> checkUserData();

    Observable<DiaryViewModel> listAppointments();

    Observable<AgendaItemViewModel> finishAppointment(FinishAppointmentParams finishAppointmentParams);

    Observable<String> removeAppointment(RemoveAppointmentParams removeAppointmentParams);

    double calculatePercentage(List<AgendaFeedViewModel> itemsList, int totalAppointmentsAccomplished);

    int calculateAccomplishedTotal(List<AgendaFeedViewModel> itemsList);

    int calculateAccomplishedOrScheduledTotal(List<AgendaFeedViewModel> itemsList);
  }

}
