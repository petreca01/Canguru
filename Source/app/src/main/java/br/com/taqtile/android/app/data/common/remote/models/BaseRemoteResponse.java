package br.com.taqtile.android.app.data.common.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/15/17.
 */

public class BaseRemoteResponse<T> {
  @SerializedName("success")
  private Boolean success;
  @SerializedName("error")
  private String error;
  @SerializedName("result")
  private T result;

  public T getResult() {
    return result;
  }
}
