package br.com.taqtile.android.app.presentation.maternityrating;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.CommentAboutMaternityUseCase;
import br.com.taqtile.android.app.domain.maternity.RateMaternityUseCase;
import br.com.taqtile.android.app.domain.maternity.mappers.MaternityCommentResultToMaternityCommentViewModelMapper;
import br.com.taqtile.android.app.domain.maternity.mappers.MaternityDataResultToMaternityViewModelMapper;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityRatingPresenter implements MaternityRatingContract.Presenter {

  private RateMaternityUseCase rateMaternityUseCase;
  private CommentAboutMaternityUseCase commentAboutMaternityUseCase;

  public MaternityRatingPresenter() {
    this.rateMaternityUseCase = Injection.provideRateMaternityUseCase();
    this.commentAboutMaternityUseCase = Injection.provideCommentAboutMaternityUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<MaternityViewModel> rateMaternity(RateMaternityParams rateMaternityParams) {
    return rateMaternityUseCase.execute(rateMaternityParams).map(MaternityDataResultToMaternityViewModelMapper::map);
  }

  @Override
  public Observable<List<MaternityCommentViewModel>> comment(MaternityCommentParams maternityCommentParams) {
    return commentAboutMaternityUseCase.execute(maternityCommentParams).map(MaternityCommentResultToMaternityCommentViewModelMapper::map);
  }
}
