package br.com.taqtile.android.app.presentation.changepassword;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import br.com.taqtile.android.cleanbase.presentation.view.BaseView;
import br.com.taqtile.android.app.support.NavigationManager;
import rx.Observable;

/**
 * Created by taqtile on 11/15/16.
 */

public interface ChangePasswordContract {

    interface Presenter extends BasePresenter {
      Observable<EmptyResult> changePassword(String newPassword, String confirmPassword);
    }

}
