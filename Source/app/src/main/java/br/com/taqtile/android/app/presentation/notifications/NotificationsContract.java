package br.com.taqtile.android.app.presentation.notifications;

import java.util.List;

import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;
import br.com.taqtile.android.app.listings.viewmodels.NotificationViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public interface NotificationsContract {

  interface Presenter extends BasePresenter {
    Observable<List<NotificationViewModel>> fetchNotifications();

    Observable<Boolean> setNotificationVisualized(SetVisualizedNotificationParams setVisualizedNotificationParams);
  }
}
