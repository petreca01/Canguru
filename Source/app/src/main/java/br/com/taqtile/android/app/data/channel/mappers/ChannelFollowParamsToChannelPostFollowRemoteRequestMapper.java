package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostFollowRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelFollowParams;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelFollowParamsToChannelPostFollowRemoteRequestMapper {
  private static final Integer FOLLOW_PARAMETER_VALUE = 1;

  public static ChannelPostFollowRemoteRequest perform(ChannelFollowParams channelFollowParams) {
    ChannelPostFollowRemoteRequest channelPostFollowRemoteRequest = new ChannelPostFollowRemoteRequest();

    channelPostFollowRemoteRequest.setChannelId(channelFollowParams.getChannelId());

    if (channelFollowParams.isFollow()) {
      channelPostFollowRemoteRequest.setFollow(FOLLOW_PARAMETER_VALUE);
    } else {
      channelPostFollowRemoteRequest.setUnfollow(FOLLOW_PARAMETER_VALUE);
    }

    return channelPostFollowRemoteRequest;
  }
}
