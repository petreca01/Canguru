package br.com.taqtile.android.app.presentation.initiallogin;

import android.os.Bundle;
import br.com.taqtile.android.app.presentation.common.BundleAccessor;

import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.EMAIL_KEY;
import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.FB_TOKEN_KEY;
import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.NAME_KEY;

/**
 * Created by taqtile on 26/04/17.
 */

public class InitialLoginBundleAccessor extends BundleAccessor {

  public InitialLoginBundleAccessor(Bundle bundle) {
    super(bundle);
  }

  public String getName() {
    return getString(bundle, NAME_KEY);
  }

  public String getEmail() {
    return getString(bundle, EMAIL_KEY);
  }

  public String getFbTokenKey() {
    return getString(bundle, FB_TOKEN_KEY);
  }

  private String getString(Bundle bundle, String key) {
    if (bundle == null) {
      return "";
    }
    return bundle.getString(key);
  }
}
