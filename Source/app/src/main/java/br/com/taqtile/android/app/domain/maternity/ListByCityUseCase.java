package br.com.taqtile.android.app.domain.maternity;

import java.util.List;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class ListByCityUseCase extends BaseUseCase<List<MaternityDataResult>, String,
  MaternityRepository> {

  public ListByCityUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<List<MaternityDataResult>> execute(String city) {
    return getRepository().listByCity(city);
  }
}
