package br.com.taqtile.android.app.data.exams.remote.mappers;

import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsInfoItemRemoteResponse;
import br.com.taqtile.android.app.domain.exams.models.ExamDataResult;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsInfoItemRemoteResponseListToExamDataResultListMapper {

  public static List<ExamDataResult> perform(
    List<ExamsInfoItemRemoteResponse> examsInfoItemRemoteResponseList) {
    return StreamSupport.stream(examsInfoItemRemoteResponseList)
      .map(ExamsInfoItemRemoteResponseToExamDataResultMapper::perform)
      .collect(Collectors.toList());
  }

}
