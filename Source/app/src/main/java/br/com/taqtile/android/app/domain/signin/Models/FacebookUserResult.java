package br.com.taqtile.android.app.domain.signin.Models;

/**
 * Created by taqtile on 25/04/17.
 */

public class FacebookUserResult {

  private String name;
  private String email;
  private String token;

  public FacebookUserResult(String name, String email, String token) {
    this.name = name;
    this.email = email;
    this.token = token;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getToken() {
    return token;
  }
}
