package br.com.taqtile.android.app.data.common.geocoding.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 6/5/17.
 */

public class BaseGeocodingResponse<T> {
  @SerializedName("results")
  private T results;
  @SerializedName("status")
  private String status;

  public T getResults() {
    return results;
  }

  public String getStatus() {
    return status;
  }
}

