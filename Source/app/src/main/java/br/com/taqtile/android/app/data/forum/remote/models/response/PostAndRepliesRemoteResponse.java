package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 3/21/17.
 */

public class PostAndRepliesRemoteResponse {
    @SerializedName("post")
    private PostItemResponse post;

    @SerializedName("replies")
    private List<ReplyRemoteResponse> replies;

  public PostItemResponse getPost() {
    return post;
  }

  public List<ReplyRemoteResponse> getReplies() {
    return replies;
  }
}
