package br.com.taqtile.android.app.data.questionnaires.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by felipesabino on 5/17/17.
 */

public class AnswerUnlockTriggerRemoteResponse {
  @SerializedName("more_than")
  private Integer moreThan;

  public Integer getMoreThan() {
    return moreThan;
  }
}
