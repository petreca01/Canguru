package br.com.taqtile.android.app.data.city.geocondingAPI;

import java.util.List;

import br.com.taqtile.android.app.data.city.geocondingAPI.models.LocationDiscoveryGeocodingAPIResponse;
import br.com.taqtile.android.app.data.common.geocoding.models.BaseGeocodingResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by taqtile on 6/2/17.
 */

public interface LocationDiscoveryServices {

  @POST("maps/api/geocode/json")
  Observable<Result<BaseGeocodingResponse<List<LocationDiscoveryGeocodingAPIResponse>>>> searchCityByZipCode(
    @Query("address") String zipCode);
}
