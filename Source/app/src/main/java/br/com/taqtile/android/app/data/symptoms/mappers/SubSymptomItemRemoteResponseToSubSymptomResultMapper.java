package br.com.taqtile.android.app.data.symptoms.mappers;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.SubSymptomItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.SubSymptomResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class SubSymptomItemRemoteResponseToSubSymptomResultMapper {

  public static SubSymptomResult perform(SubSymptomItemRemoteResponse subSymptomItemRemoteResponse) {
    return new SubSymptomResult(
      subSymptomItemRemoteResponse.getId(),
      subSymptomItemRemoteResponse.getParentId(),
      subSymptomItemRemoteResponse.getName(),
      subSymptomItemRemoteResponse.getCiap(),
      subSymptomItemRemoteResponse.getCondition(),
      subSymptomItemRemoteResponse.getConditionTrigger(),
      subSymptomItemRemoteResponse.getExplanation(),
      subSymptomItemRemoteResponse.getHowToAvoid(),
      subSymptomItemRemoteResponse.getWhenToWorry(),
      subSymptomItemRemoteResponse.getDangerous());
  }
}
