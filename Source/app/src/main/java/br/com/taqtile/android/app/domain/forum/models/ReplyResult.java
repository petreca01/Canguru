package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 4/4/17.
 */

public class ReplyResult {
  private Integer id;
  private Integer forumPostId;
  private Integer operatorId;
  private Integer userId;
  private String reply;
  private Boolean active;
  private String createdAt;
  private String updatedAt;
  private String userType;
  private String username;
  private String userPicture;
  private Integer likes;
  private Boolean liked;

  public ReplyResult(Integer id, Integer forumPostId, Integer operatorId, Integer userId, String reply, Boolean active, String createdAt, String updatedAt, String userType, String username, String userPicture, Integer likes, Boolean liked) {
    this.id = id;
    this.forumPostId = forumPostId;
    this.operatorId = operatorId;
    this.userId = userId;
    this.reply = reply;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.userType = userType;
    this.username = username;
    this.userPicture = userPicture;
    this.likes = likes;
    this.liked = liked;
  }

  public Integer getId() {
    return id;
  }

  public Integer getForumPostId() {
    return forumPostId;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public Integer getUserId() {
    return userId;
  }

  public String getReply() {
    return reply;
  }

  public Boolean getActive() {
    return active;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getUserType() {
    return userType;
  }

  public String getUsername() {
    return username;
  }

  public String getUserPicture() {
    return userPicture;
  }

  public Integer getLikes() {
    return likes;
  }

  public Boolean getLiked() {
    return liked;
  }
}
