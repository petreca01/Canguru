package br.com.taqtile.android.app.data.profile.remote.mappers;

import br.com.taqtile.android.app.data.profile.remote.models.request.GetProfileDetailsRemoteRequest;
import br.com.taqtile.android.app.domain.profile.models.UserProfileParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class UserParamsToGetProfileDetailsRemoteRequestMapper {

  public static GetProfileDetailsRemoteRequest perform(UserProfileParams userProfileParams) {
    GetProfileDetailsRemoteRequest getProfileDetailsRemoteRequest = new GetProfileDetailsRemoteRequest();
    getProfileDetailsRemoteRequest.setProfileId(userProfileParams.getProfileId());

    return getProfileDetailsRemoteRequest;
  }
}
