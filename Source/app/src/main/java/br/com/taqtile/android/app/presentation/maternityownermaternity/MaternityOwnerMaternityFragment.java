package br.com.taqtile.android.app.presentation.maternityownermaternity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.edittexts.PhoneCellPhoneLabelEditTextCaption;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.GenericTextAreaLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.NameLabelEditTextCaption;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by duh on 3/12/18.
 */

public class MaternityOwnerMaternityFragment extends AppBaseFragment {

  @BindView(R.id.fragment_maternity_owner_maternity_edit_name)
  NameLabelEditTextCaption nameEdiText;

  @BindView(R.id.fragment_maternity_owner_maternity_edit_telephone)
  PhoneCellPhoneLabelEditTextCaption telephoneEditText;

  @BindView(R.id.fragment_maternity_owner_maternity_edit_email)
  EmailLabelEditTextCaption emailEditText;

  @BindView(R.id.fragment_maternity_owner_maternity_edit_comment)
  GenericTextAreaLabelEditTextCaption commentBody;

  @BindView(R.id.fragment_maternity_owner_maternity_edit_save)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_maternity_owner_maternity_content_layout)
  LinearLayout contentLayout;

  @BindView(R.id.fragment_additional_info_scroll_view)
  ScrollView scrollView;

  private CustomToolbar customToolbar;
  private NavigationManager navigationManager;
  private MaternityOwnerMaternityContract.Presenter presenter;

  private Validatable[] forms;
  private List<Validatable> errorForms;

  public static MaternityOwnerMaternityFragment newInstance() {
    return new MaternityOwnerMaternityFragment();
  }


  public MaternityOwnerMaternityFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_maternity_owner_maternity, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupToolbar();
    setupButton();
    setupFormsList();
    setupView();

    commentBody.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
  }

  private void setupView() {}

  private void setupFormsList() {
    forms = new Validatable[]{telephoneEditText, nameEdiText, emailEditText, commentBody};
    errorForms = new ArrayList<Validatable>();
  }

  private void setupButton() {
    saveButton.setButtonListener(this::sendInfoData);
  }

  private void sendInfoData() {}

  public void setPresenter(MaternityOwnerMaternityContract.Presenter presenter) {
    this.presenter = presenter;
  }


  @Override
  public void showLoading() {
    saveButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    saveButton.showDefaultState();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_maternity_detail_title), Gravity.LEFT);
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;
    isValid = validateForms();

    if (!isValid) {
      //Display feedback with all views with an error
      showFormValidationError();
      if (!errorForms.isEmpty()) {
        ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      }
    }

    return isValid;
  }

  public void showFormValidationError() {
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);

  }

  private boolean validateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      isValid = false;
    }
    return isValid;
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms;

    errorForms = new String[errorViews.size()];
    for (int i = 0; i < errorForms.length; i++) {
      errorForms[i] = getResources().getString(((CustomLabelEditTextCaption) errorViews.get(i).getView()).getInputPlaceholderTextRes());
    }
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_maternity_owner_maternity),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
