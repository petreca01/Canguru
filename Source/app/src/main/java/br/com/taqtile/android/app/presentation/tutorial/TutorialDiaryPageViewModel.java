package br.com.taqtile.android.app.presentation.tutorial;

import android.view.View;

/**
 * Created by taqtile on 5/29/17.
 */

public class TutorialDiaryPageViewModel {

  public static final int CIRCLE = 0, SEMI_CIRCLE_RETANGLE = 1;

  public View target;
  public String text;
  public int type;
  public boolean notifyWhenSelected;

  public TutorialDiaryPageViewModel(View target, String text, int type, boolean notifyWhenSelected) {
    this.target = target;
    this.text = text;
    if (type < CIRCLE || type > SEMI_CIRCLE_RETANGLE) {
      throw new IllegalArgumentException("You are trying to initialize a Diary Tutorial Page with a type that doesn't exists.");
    } else {
      this.type = type;
    }
    this.notifyWhenSelected = notifyWhenSelected;
  }
}
