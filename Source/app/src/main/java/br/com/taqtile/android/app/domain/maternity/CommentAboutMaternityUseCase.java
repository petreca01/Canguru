package br.com.taqtile.android.app.domain.maternity;

import java.util.List;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import rx.Observable;

/**
 * Created by taqtile on 5/12/17.
 */

public class CommentAboutMaternityUseCase extends
  BaseUseCase<List<MaternityCommentResult>, MaternityCommentParams, MaternityRepository> {

  public CommentAboutMaternityUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<List<MaternityCommentResult>> execute(MaternityCommentParams maternityCommentParams) {
    return getRepository().comment(maternityCommentParams);
  }
}
