package br.com.taqtile.android.app.domain.riskAnalysis;

import java.util.List;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.riskAnalysis.RiskAnalysisRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.riskAnalysis.mappers.RiskAnalysisQuestionsResultToRiskAnalysisQuestionGroupMapper;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import rx.Observable;


/**
 * Created by felipesabino on 5/17/17.
 */

public class ListGroupsUseCase extends BaseUseCase<List<RiskAnalysisQuestionGroupResult>, String,
  RiskAnalysisRepository> {

  public ListGroupsUseCase(RiskAnalysisRepository riskAnalysisRepository) {
    super(riskAnalysisRepository);
  }

  @Override
  public Observable<List<RiskAnalysisQuestionGroupResult>> execute(String email) {

    ViewModeParams viewMode;
    if (email != null && !email.isEmpty()) {
      viewMode = new ViewModeParams(email);
    } else {
      viewMode = new ViewModeParams();
    }

    return getRepository()
      .list(viewMode)
      .map(RiskAnalysisQuestionsResultToRiskAnalysisQuestionGroupMapper::perform);

  }
}
