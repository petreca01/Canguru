package br.com.taqtile.android.app.presentation.prenatalcard.models;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardViewModel {

  private String name;
  private String age;
  private GestationHistoryViewModel gpa;
  private RisksViewModel risks;

  public FollowCardViewModel(String name, String age, GestationHistoryViewModel gpa, RisksViewModel risks) {
    this.name = name;
    this.age = age;
    this.gpa = gpa;
    this.risks = risks;
  }

  public String getName() {
    return name;
  }

  public String getAge() {
    return age;
  }

  public GestationHistoryViewModel getGpa() {
    return gpa;
  }

  public RisksViewModel getRisks() {
    return risks;
  }

}
