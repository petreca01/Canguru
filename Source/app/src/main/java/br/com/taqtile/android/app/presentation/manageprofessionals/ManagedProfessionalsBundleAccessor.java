package br.com.taqtile.android.app.presentation.manageprofessionals;

import android.os.Bundle;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.app.presentation.common.BundleAccessor;
import java.util.List;

/**
 * Created by taqtile on 27/04/17.
 */

class ManagedProfessionalsBundleAccessor extends BundleAccessor {

  public ManagedProfessionalsBundleAccessor(Bundle bundle) {
    super(bundle);
  }

  public List<ConnectedProfessionalViewModel> getConnectedProfessionalViewModelList() {
    if (bundle == null) {
      return null;
    }
    return bundle.getParcelableArrayList(ManagedProfessionalsBundleConstants.PROFESSIONALS_LIST_KEY);
  }
}
