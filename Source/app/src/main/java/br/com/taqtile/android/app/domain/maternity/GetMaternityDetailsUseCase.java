package br.com.taqtile.android.app.domain.maternity;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDetailsResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityParams;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class GetMaternityDetailsUseCase extends BaseUseCase<MaternityDetailsResult, MaternityParams,
  MaternityRepository> {

  public GetMaternityDetailsUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<MaternityDetailsResult> execute(MaternityParams maternityParams) {
    return getRepository().getDetails(maternityParams);
  }
}
