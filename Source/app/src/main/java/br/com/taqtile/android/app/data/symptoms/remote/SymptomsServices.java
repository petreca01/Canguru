package br.com.taqtile.android.app.data.symptoms.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.ReportSymptomRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.SymptomsItemRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.UserSymptomItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface SymptomsServices {
  @POST("symptoms")
  Observable<Result<BaseRemoteResponse<List<SymptomsItemRemoteResponse>>>> list();

  @FormUrlEncoded
  @POST("user/symptoms")
  Observable<Result<BaseRemoteResponse<List<UserSymptomItemRemoteResponse>>>>
  listUserSymptoms(@FieldMap Map<String, String> listUserSymptomsRequestModel);

  @FormUrlEncoded
  @POST("user/symptom")
  Observable<Result<BaseRemoteResponse<ReportSymptomRemoteResponse>>>
  reportSymptom(@FieldMap Map<String, String> reportSymptomRequestModel);
}
