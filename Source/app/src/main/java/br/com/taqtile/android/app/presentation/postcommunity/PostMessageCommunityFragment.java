package br.com.taqtile.android.app.presentation.postcommunity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.edittexts.MessageBodyLabelEditTextCaption;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 12/04/17.
 */

public class PostMessageCommunityFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener, TextWatcher {

  @BindView(R.id.fragment_post_message_community_message)
  MessageBodyLabelEditTextCaption message;
  @BindView(R.id.fragment_post_message_community_post_button)
  ProgressBarButton postButton;

  private String topic;
  private CustomToolbar customToolbar;
  private NavigationManager navigationManager;
  private PostCommunityContract.Presenter presenter;
  private static final Integer MAX_LENGTH = 10000;

  public static PostMessageCommunityFragment newInstance() {
    return new PostMessageCommunityFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_post_message_community, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
    setupListeners();
    setupView();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().onBackPressed());
      customToolbar.setTitle(getString(R.string.fragment_post_community_title), Gravity.LEFT);
      customToolbar.hideOptionsMenu();
    }
  }

  private void setupListeners() {
    postButton.setButtonListener(this);
    message.setTextChangedListener(this);
    setupFormKeyListener();
  }

  private void setupFormKeyListener(){
    // TODO: 05/07/17 this is not working in all APIs, find out why
    message.getEditText().setOnKeyListener((v, keyCode, event) -> {
      if (event.getAction() == MotionEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER && !message.getText().isEmpty()) {
        postMessage();
      }
      return false;
    });
  }

  private void setupView() {
    postButton.showDefaultState();
    postButton.disableClick();
    message.setMaxLength(MAX_LENGTH);
    message.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    SoftKeyboardHelper.showKeyboard(getContext());
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public void setPresenter(PostCommunityContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    postButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    postButton.showDefaultState();
  }

  private void showDefaultPostButtonState() {
    postButton.showDefaultState();
  }

  public void onButtonClicked() {
    postMessage();
  }

  private void postMessage(){
    showLoading();
    Subscription subscription = presenter.post(topic, message.getText())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        result -> onPostSuccess(),
        this::onPostFailure
      );
    compositeSubscription.add(subscription);
  }

  private void onPostSuccess() {
    hideLoading();

    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_new),
      getString(R.string.analytics_label_user_posted)
    ));

    getActivity().finish();
  }

  private void onPostFailure(Throwable throwable) {
    hideLoading();
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    showDefaultPostButtonState();
  }

  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    if (charSequence.toString().isEmpty()) {
      postButton.disableClick();
    } else {
      postButton.enableClick();
    }
  }

  @Override
  public void afterTextChanged(Editable editable) {
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum_post_message),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
