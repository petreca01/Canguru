package br.com.taqtile.android.app.presentation.additionalinfoedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import br.com.taqtile.android.app.edittexts.PhoneCellPhoneLabelEditTextCaption;
import br.com.taqtile.android.app.listings.cells.FormDetailCell;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.BirthdateLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.CEPLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.CPFLabelEditTextCaption;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.ADDITIONAL_INFO_TAG;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoEditFragment extends AppBaseFragment {

  private AdditionalInfoEditContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  private Validatable[] forms;
  private List<Validatable> errorForms;

  @BindView(R.id.fragment_additional_info_scroll_view)
  ScrollView scrollView;

  @BindView(R.id.fragment_additional_info_content_layout)
  LinearLayout contentLayout;

  @BindView(R.id.fragment_additional_info_edit_email)
  FormDetailCell emailText;

  @BindView(R.id.fragment_additional_info_edit_telephone)
  PhoneCellPhoneLabelEditTextCaption telephoneEditText;

  @BindView(R.id.fragment_additional_info_edit_birth_date)
  BirthdateLabelEditTextCaption birthDateEditText;

  @BindView(R.id.fragment_additional_info_edit_cpf)
  CPFLabelEditTextCaption cpfEditText;

  @BindView(R.id.fragment_additional_info_edit_cep)
  CEPLabelEditTextCaption cepEditText;

  @BindView(R.id.fragment_additional_info_edit_save)
  ProgressBarButton saveButton;

  private AdditionalInfoUserViewModel additionalInfoUserData;

  public static AdditionalInfoEditFragment newInstance() {
    return new AdditionalInfoEditFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_additional_info_edit, container, false);
    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupButton();
    setupFormsList();
    setupView();
  }

  private void setupView() {
    birthDateEditText.setHintCaption("DD/MM/AAAA");
    if (additionalInfoUserData != null) {
      emailText.setupValue(additionalInfoUserData.getEmail());
      telephoneEditText.setText(additionalInfoUserData.getTelephone());
      birthDateEditText.setText(
        DateFormatterHelper.formatDateWithoutHours(additionalInfoUserData.getBirthDate()));
      cpfEditText.setText(additionalInfoUserData.getCpf());
      cepEditText.setText(additionalInfoUserData.getZipCode());
    }
  }

  private void setupFormsList() {
    forms = new Validatable[]{telephoneEditText, birthDateEditText, cpfEditText, cepEditText};
    errorForms = new ArrayList<Validatable>();
  }

  private void setupButton() {
    saveButton.setButtonListener(this::editUserData);
  }

  private void editUserData() {
    if (locallyValidateForms()) {
      showLoading();
      Subscription getLocationByZipCodeSubscription = discoverLocation()
        .flatMap(this::editUserDataFlatMap)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::finishFromEditUserData, this::onEditUserDataFailure,
          this::hideLoading);
      compositeSubscription.add(getLocationByZipCodeSubscription);
    }
  }

  private Observable<LocationDiscoveryResult> discoverLocation() {
    if (cepEditText.getText().isEmpty()) {
      return Observable.just(new LocationDiscoveryResult());
    } else {
      return presenter.getLocationDiscovery(new LocationDiscoveryByZipCodeParams(cepEditText.getText()));
    }
  }

  private Observable<UserViewModel> editUserDataFlatMap(LocationDiscoveryResult locationDiscoveryResult) {

    EditUserDataParams params = new EditUserDataParams(
      telephoneEditText.getText(),
      DateFormatterHelper.formatDateForAPIWithoutHour(birthDateEditText.getText()),
      cpfEditText.getText(),
      cepEditText.getText(),
      null,
      locationDiscoveryResult.getCity(),
      locationDiscoveryResult.getState(),
      locationDiscoveryResult.getLatitude(),
      locationDiscoveryResult.getLongitude());

    return presenter.editUserData(params)
      .observeOn(AndroidSchedulers.mainThread());
  }

  private void finishFromEditUserData(UserViewModel userViewModel) {
    Intent intent = new Intent();
    intent.putExtra(ADDITIONAL_INFO_TAG, userViewModel);
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();
  }

  private void onEditUserDataFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    hideLoading();
  }

  public void setPresenter(@NonNull AdditionalInfoEditContract.Presenter presenter) {
    this.presenter = checkNotNull(presenter);
  }

  @Override
  public void showLoading() {
    saveButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    saveButton.showDefaultState();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_additional_info_edit_title), Gravity.LEFT);
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
    }
  }

  public void setAdditionalInfoUserData(AdditionalInfoUserViewModel additionalInfoUserData) {
    this.additionalInfoUserData = additionalInfoUserData;
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;
    isValid = validateForms();

    if (!isValid) {
      //Display feedback with all views with an error
      showFormValidationError();
      if (!errorForms.isEmpty()) {
        ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      }
    }

    return isValid;
  }

  public void showFormValidationError() {
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);

  }

  private boolean validateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      isValid = false;
    }
    return isValid;
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms;

    errorForms = new String[errorViews.size()];
    for (int i = 0; i < errorForms.length; i++) {
      errorForms[i] = getResources().getString(((CustomLabelEditTextCaption) errorViews.get(i).getView()).getInputPlaceholderTextRes());
    }
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_additional_info_edit),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
