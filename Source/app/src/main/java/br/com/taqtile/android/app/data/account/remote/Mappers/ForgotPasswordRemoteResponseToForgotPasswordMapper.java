package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordRemoteResponseToForgotPasswordMapper {

    public static ForgotPasswordResult perform(BaseRemoteResponse<String> response) {
        ForgotPasswordResult forgotPasswordResult = new ForgotPasswordResult();

        if (response.getResult() == null) {
          forgotPasswordResult.setMessage("");
        } else {
          forgotPasswordResult.setMessage(response.getResult());
        }

        return forgotPasswordResult;
    }
}
