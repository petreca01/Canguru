package br.com.taqtile.android.app.data.profile.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostItemResponse;
import br.com.taqtile.android.app.data.profile.remote.models.request.EditPublicProfileRemoteRequest;
import br.com.taqtile.android.app.data.profile.remote.models.request.GetProfileDetailsRemoteRequest;
import br.com.taqtile.android.app.data.profile.remote.models.response.ProfileDetailsRemoteResponse;
import java.util.List;

import br.com.taqtile.android.app.data.profile.remote.models.response.UpdateProfileDetailsRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfileRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ProfileServices> services;

  public ProfileRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(ProfileServices.class);
  }

  public Observable<BaseRemoteResponse<ProfileDetailsRemoteResponse>> getProfileDetails(
    GetProfileDetailsRemoteRequest getProfileDetailsRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.getProfileDetails(getProfileDetailsRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<PostItemResponse>>> listPosts(Integer profileId) {
    return performRequest(services.flatMap(services -> services.listPosts(profileId)));
  }

  public Observable<BaseRemoteResponse<UpdateProfileDetailsRemoteResponse>> editPublicProfile(
    EditPublicProfileRemoteRequest editPublicProfileRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.editPublicProfile(editPublicProfileRemoteRequest.getFormData(),
        editPublicProfileRemoteRequest.getFiles())));
  }
}
