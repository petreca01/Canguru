package br.com.taqtile.android.app.domain.notification.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class SetVisualizedNotificationParams {
  private String id;

  public SetVisualizedNotificationParams(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

}
