package br.com.taqtile.android.app.domain.profile.models;

import java.util.List;

import br.com.taqtile.android.app.domain.forum.models.PostResult;

/**
 * Created by taqtile on 4/17/17.
 */

public class PublicProfileResult {

  private ProfileDetailsResult profileDetailsResult;
  private List<PostResult> postResults;

  public PublicProfileResult(ProfileDetailsResult profileDetailsResult,
                             List<PostResult> postResults) {
    this.profileDetailsResult = profileDetailsResult;
    this.postResults = postResults;
  }

  public ProfileDetailsResult getProfileDetailsResult() {
    return profileDetailsResult;
  }

  public List<PostResult> getPostResults() {
    return postResults;
  }
}
