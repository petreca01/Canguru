package br.com.taqtile.android.app.data.channel.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelPostReplyDestroyRemoteRequest extends BaseRemoteRequest {
        @RequestName("post_id")
        private Integer postId;

        @RequestName("reply_id")
        private Integer replyId;

        public void setPostId(Integer postId) {
            this.postId = postId;
        }

        public void setReplyId(Integer replyId) {
            this.replyId = replyId;
        }
}
