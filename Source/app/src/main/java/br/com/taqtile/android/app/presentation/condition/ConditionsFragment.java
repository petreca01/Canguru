package br.com.taqtile.android.app.presentation.condition;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.ConditionsAdapter;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.listings.Constants.CONDITION;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionsFragment extends AppBaseFragment implements
  ConditionsAdapter.ConditionsAdapterListener {

  @BindView(R.id.fragment_conditions_recycler_view)
  RecyclerView conditionsRecyclerView;

  private NavigationManager navigationManager;
  private ConditionsContract.Presenter presenter;
  private CustomToolbar customToolbar;

  protected ConditionsAdapter conditionsAdapter;
  protected LinearLayoutManager linearLayoutManager;
  protected List<ListingsViewModel> conditionsList;
  private Integer currentGestationWeek;
  private Subscription checkUserDataSubscription;
  private Subscription conditionsListSubscription;

  public static ConditionsFragment newInstance() {
    return new ConditionsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_conditions, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupRecyclerView();
    fetchUserData();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_conditions_title));
    }
  }

  private void setupRecyclerView() {
    conditionsList = new ArrayList<>();
    linearLayoutManager = new LinearLayoutManager(getContext());
    conditionsRecyclerView.setLayoutManager(linearLayoutManager);
    conditionsAdapter = new ConditionsAdapter(conditionsList);
    conditionsAdapter.setListener(this);
    conditionsRecyclerView.setAdapter(conditionsAdapter);
    conditionsRecyclerView.getRecycledViewPool().setMaxRecycledViews(CONDITION, 0);
  }

  private void fetchUserData() {
    if (checkUserDataSubscription != null) {
      checkUserDataSubscription.unsubscribe();
    }
    showLoading();
    checkUserDataSubscription = presenter.fetchUserData()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchUserDataSuccess,
        this::onFetchUserDataFailure);
    compositeSubscription.add(checkUserDataSubscription);
  }

  private void onFetchUserDataSuccess(UserViewModel userDataViewModel) {
    this.currentGestationWeek = userDataViewModel.getGestationWeeks();
    fetchListConditions(currentGestationWeek);
  }

  private void onFetchUserDataFailure(Throwable throwable) {
    hideLoading();
    conditionsRecyclerView.setVisibility(View.GONE);
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void fetchListConditions(Integer gestationWeeks) {
    showLoading();
    if (conditionsListSubscription != null) {
      conditionsListSubscription.unsubscribe();
    }
    conditionsListSubscription = presenter.listConditionsUseCase(gestationWeeks)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onListConditionsSuccess, this::onListConditionsFailure, this::hideLoading);
    compositeSubscription.add(conditionsListSubscription);
  }

  private void onListConditionsFailure(Throwable throwable) {
    this.hideLoading();
    conditionsRecyclerView.setVisibility(View.GONE);
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void onListConditionsSuccess(List<ListingsViewModel> clinicalConditionItemViewModels) {
    this.conditionsList.clear();
    this.conditionsList.addAll(clinicalConditionItemViewModels);
    conditionsAdapter.notifyDataSetChanged();
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(ConditionsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void onConditionCellClicked(ClinicalConditionItemViewModel clinicalConditionItemViewModel) {
    navigationManager.showClinicalConditionsDetailsUI(clinicalConditionItemViewModel,
      this::returningFromDetails);
  }

  private void returningFromDetails(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      fetchListConditions(currentGestationWeek);
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void showLoading() {
    super.showLoading();
    hidePlaceholder();
    conditionsRecyclerView.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    hidePlaceholder();
    conditionsRecyclerView.setVisibility(View.VISIBLE);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchUserData();
  }

  @Override
  public void onConditionHistoryCellClicked(int conditionId) {
    // do nothing by now
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_conditions_placeholder;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_condition),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
