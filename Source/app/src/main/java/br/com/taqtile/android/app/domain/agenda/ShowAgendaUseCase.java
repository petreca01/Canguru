package br.com.taqtile.android.app.domain.agenda;

import java.util.List;

import br.com.taqtile.android.app.data.agenda.AgendaRepository;
import br.com.taqtile.android.app.domain.agenda.mappers.AgendaItemResultToViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class ShowAgendaUseCase extends BaseUseCase<List<AgendaItemViewModel>, Void,AgendaRepository> {

  public ShowAgendaUseCase(AgendaRepository agendaRepository) {
    super(agendaRepository);
  }

  @Override
  public Observable<List<AgendaItemViewModel>> execute(Void aVoid) {
    return getRepository().list()
      .map(AgendaItemResultToViewModelMapper::perform);
  }
}
