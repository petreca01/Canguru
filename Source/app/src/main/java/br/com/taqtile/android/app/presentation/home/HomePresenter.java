package br.com.taqtile.android.app.presentation.home;


import java.util.Arrays;
import java.util.List;

import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.domain.forum.LikeForumPostItemUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import br.com.taqtile.android.app.domain.timeline.FetchTimelineUseCase;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.presentation.home.mappers.TimelineModelToSocialFeedViewModelMapper;
import br.com.taqtile.android.app.support.Injection;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import rx.Observable;

/**
 * Created by taqtile on 7/6/16.
 */

public class HomePresenter implements HomeContract.Presenter {

  private FetchTimelineUseCase timelineUseCase;
  private LikeForumPostItemUseCase likeForumPostItemUseCase;
  private LikeChannelPostUseCase likeChannelPostUseCase;

  public HomePresenter() {
    this.timelineUseCase = Injection.provideFetchTimelineUseCase();
    this.likeForumPostItemUseCase = Injection.provideLikeForumPostReplyUseCase();
    this.likeChannelPostUseCase = Injection.provideLikeChannelPostUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<ListingsViewModel>> likeCommunityPost(Integer postId, boolean alreadyLiked) {
    return likeForumPostItemUseCase.execute(new PostParams(postId, null, null, alreadyLiked ? 1 : 0));
  }

  @Override
  public Observable<List<ListingsViewModel>> likeChannelPost(Integer postId, boolean alreadyLiked) {

    return likeChannelPostUseCase.execute(new ChannelPostParams(postId, null, !alreadyLiked));
  }

  @Override
  public Observable<List<SocialFeedViewModel>> fetchCards(boolean reload) {

    return Toolbox.getInstance().getMainTutorial()
      .flatMap(alreadySeen -> {
        if (alreadySeen) {
          return this.timelineUseCase.execute(reload)
            .map(TimelineModelToSocialFeedViewModelMapper::perform)
            .map(HomePresenter::sortByDate);
        } else {
          // This mock refers to tutorial data DON`T REMOVE IT
          return createMockData();
        }
      });
  }

  private static List<SocialFeedViewModel> sortByDate(List<SocialFeedViewModel> list) {
    return StreamSupport
      .stream(list)
      .sorted((card1, card2) -> card1.getDateInMilliseconds() > card2.getDateInMilliseconds() ? 1 : 0)
      .collect(Collectors.toList());
  }

  private Observable<List<SocialFeedViewModel>> createMockData(){
    return Observable.just(
      Arrays.asList(
        new SocialFeedCommunityImplViewModel(
          1, 1, "", "Amanda", "23/08/2016", "Nausea", "Estou com 6 meses e ainda sinto nauseas. É normal?", 1, 1, true, false
        ),
        new SocialFeedCommunityImplViewModel(
          1, 1, "", "Patricia", "13/07/2016", "Meu príncipe nasceu!", "Meu filho acabou de nascer!", 1, 0, false, false
        ),
        new SocialFeedCommunityImplViewModel(
          1, 1, "", "Carolina", "01/07/2016", "Sexo do bebe", "Com quantos meses consigo saber o sexo do meu bebe?", 1, 1, false, false
        )
      )
    );
  }
}
