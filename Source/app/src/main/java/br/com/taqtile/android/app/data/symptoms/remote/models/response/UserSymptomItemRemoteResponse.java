package br.com.taqtile.android.app.data.symptoms.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class UserSymptomItemRemoteResponse {
    @SerializedName("date")
    private String date;

    @SerializedName("symptom_id")
    private Integer symptomId;

    @SerializedName("symptom_name")
    private String symptomName;

    @SerializedName("ga_weeks")
    private Integer gestationAgeWeeks;

    @SerializedName("ga_days")
    private Integer gestationAgeDays;

    public String getDate() {
        return date;
    }

    public Integer getSymptomId() {
        return symptomId;
    }

    public String getSymptomName() {
        return symptomName;
    }

    public Integer getGestationAgeWeeks() {
        return gestationAgeWeeks;
    }

    public Integer getGestationAgeDays() {
        return gestationAgeDays;
    }
}
