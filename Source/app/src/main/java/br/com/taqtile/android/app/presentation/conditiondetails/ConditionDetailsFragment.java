package br.com.taqtile.android.app.presentation.conditiondetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.misc.general.CustomFlag;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.condition.models.ReportClinicalConditionViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.CLINICAL_CONDITIONS_TAG;

/**
 * Created by taqtile on 5/9/17.
 */

public class ConditionDetailsFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  @BindView(R.id.fragment_condition_details_header)
  DescriptionHeaderWithImage conditionDetailHeader;
  @BindView(R.id.fragment_condition_detail_check_condition)
  ProgressBarButton conditionCheckButton;

  private NavigationManager navigationManager;
  private ConditionDetailsContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private Subscription registerConditionSubscription;

  public static ConditionDetailsFragment newInstance() {
    return new ConditionDetailsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_condition_details, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupView();
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  private void setupView() {
    setupConditionDetailView();
  }

  private void setupConditionDetailView() {
    ClinicalConditionItemViewModel condition =
      getActivity().getIntent().getParcelableExtra(CLINICAL_CONDITIONS_TAG);

    if (condition != null) {
      conditionDetailHeader.setTitle(condition.getName());
      conditionDetailHeader.setDescription(condition.getContent());
      conditionCheckButton.setVisibility(View.VISIBLE);
      conditionCheckButton.setButtonListener(() ->
        setupRegisteredConditionDetailView(condition.getId()));
    } else {
      showPlaceholder();
    }
  }

  private void setupRegisteredConditionDetailView(Integer id) {
    if (registerConditionSubscription != null) {
      registerConditionSubscription.unsubscribe();
    }
    showLoading();
    registerConditionSubscription = presenter.reportCondition(new ReportConditionParams(id))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onReportConditionSuccess,
        this::onReportConditionFailure,
        this::hideLoading);
    compositeSubscription.add(registerConditionSubscription);

  }


  private void onReportConditionFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    this.hideLoading();
  }

  private void onReportConditionSuccess(ReportClinicalConditionViewModel reportClinicalCondition) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_condition),
      getString(R.string.analytics_event_registration),
      getString(R.string.analytics_label_user_registered_condition)
    ));
    conditionDetailHeader.setFlagTypeAndText(CustomFlag.FlagType.CallToAction ,String.format(getString(R.string.fragment_conditions_report_date_flag),
      DateFormatterHelper.formatDateWithHours(reportClinicalCondition.getUserCondition().getDate(),
        getContext())));
    conditionCheckButton.setVisibility(View.GONE);

    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);

  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_conditions_title));
    }
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(ConditionDetailsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override

  public void showLoading() {
    conditionCheckButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    conditionCheckButton.showDefaultState();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void onButtonClicked() {
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_condition_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
