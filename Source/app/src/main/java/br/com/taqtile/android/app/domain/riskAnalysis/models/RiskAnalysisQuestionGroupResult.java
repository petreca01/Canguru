package br.com.taqtile.android.app.domain.riskAnalysis.models;

/**
 * Created by felipesabino on 5/17/17.
 */

public class RiskAnalysisQuestionGroupResult {

  public RiskAnalysisQuestionGroupResult(String name) {
    this.name = name;
  }

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
