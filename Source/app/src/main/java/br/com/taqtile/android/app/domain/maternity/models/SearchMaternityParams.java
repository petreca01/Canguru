package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class SearchMaternityParams {
  private String city;
  private String search;

  public SearchMaternityParams(String city, String search) {
    this.city = city;
    this.search = search;
  }

  public String getCity() {
    return city;
  }

  public String getSearch() {
    return search;
  }
}
