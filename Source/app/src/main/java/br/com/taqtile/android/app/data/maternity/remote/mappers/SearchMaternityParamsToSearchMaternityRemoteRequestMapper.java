package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.request.SearchMaternityRemoteRequest;
import br.com.taqtile.android.app.domain.maternity.models.SearchMaternityParams;

/**
 * Created by taqtile on 06/04/17.
 */

public class SearchMaternityParamsToSearchMaternityRemoteRequestMapper {

  public static SearchMaternityRemoteRequest perform(SearchMaternityParams searchMaternityParams) {
    SearchMaternityRemoteRequest searchMaternityRemoteRequest = new SearchMaternityRemoteRequest();

    searchMaternityRemoteRequest.setCity(searchMaternityParams.getCity());
    searchMaternityRemoteRequest.setSearch(searchMaternityParams.getSearch());

    return searchMaternityRemoteRequest;
  }
}
