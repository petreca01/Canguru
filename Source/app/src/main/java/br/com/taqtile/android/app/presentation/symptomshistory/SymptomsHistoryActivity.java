package br.com.taqtile.android.app.presentation.symptomshistory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.profilemenu.ProfileMenuActivity;
import br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 08/05/17.
 */

public class SymptomsHistoryActivity extends TemplateBackActivity {

  private SymptomsHistoryFragment fragment;

  @Override
  public Fragment getFragment() {
    SymptomsHistoryFragment symptomsHistoryFragment = (SymptomsHistoryFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (symptomsHistoryFragment == null) {
      symptomsHistoryFragment = SymptomsHistoryFragment.newInstance();
    }
    fragment = symptomsHistoryFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    SymptomsHistoryPresenter symptomsHistoryPresenter = new SymptomsHistoryPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(symptomsHistoryPresenter);

    return symptomsHistoryPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, SymptomsHistoryActivity.class);
    context.startActivity(intent);
  }

}
