package br.com.taqtile.android.app.domain.weeklycontent.models;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentResult {

  private Integer week;
  private String imageUrl;
  private String content;

  public WeeklyContentResult(Integer week, String imageUrl, String content) {
    this.week = week;
    this.imageUrl = imageUrl;
    this.content = content;
  }

  public Integer getWeek() {
    return week;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public String getContent() {
    return content;
  }
}
