package br.com.taqtile.android.app.presentation.diary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper.REQUEST_CODE;

/**
 * Created by taqtile on 04/05/17.
 */

public class DiaryActivity extends TemplateBackActivity {

  private DiaryFragment fragment;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public Fragment getFragment() {
    DiaryFragment diaryFragment = (DiaryFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (diaryFragment == null) {
      diaryFragment = DiaryFragment.newInstance();
    }
    fragment = diaryFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    DiaryPresenter diaryPresenter = new DiaryPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(diaryPresenter);
    return diaryPresenter;
  }

  public static void navigate(Context context) {
    context.startActivity(new Intent(context, DiaryActivity.class));
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }

}
