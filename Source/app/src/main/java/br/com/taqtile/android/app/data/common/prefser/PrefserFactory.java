package br.com.taqtile.android.app.data.common.prefser;

import android.content.Context;
import android.content.SharedPreferences;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.support.TemplateApplication;
import com.github.pwittchen.prefser.library.Prefser;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 18/04/17.
 */

public class PrefserFactory {

  public static Prefser build() {
    Context context = checkNotNull(TemplateApplication.getContext());
    String fileName =
      TemplateApplication.getContext().getString(R.string.shared_preferences_file_name);
    SharedPreferences sharedPreferences =
      context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    return new Prefser(sharedPreferences);
  }
}
