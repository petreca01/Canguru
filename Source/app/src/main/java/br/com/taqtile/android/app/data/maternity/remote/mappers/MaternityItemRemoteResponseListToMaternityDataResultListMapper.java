package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityItemRemoteResponse;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityItemRemoteResponseListToMaternityDataResultListMapper {

  public static List<MaternityDataResult> perform(List<MaternityItemRemoteResponse> maternityItemRemoteResponseList) {
    return StreamSupport.stream(maternityItemRemoteResponseList)
      .map(MaternityItemRemoteResponseToMaternityDataResultMapper::perform)
      .collect(Collectors.toList());
  }
}
