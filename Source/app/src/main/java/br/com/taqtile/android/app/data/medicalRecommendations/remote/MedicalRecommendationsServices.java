package br.com.taqtile.android.app.data.medicalRecommendations.remote;

import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.medicalRecommendations.remote.models.MedicalRecommendationItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface MedicalRecommendationsServices {
    @POST("recommendations/all")
    Observable<Result<BaseRemoteResponse<List<MedicalRecommendationItemRemoteResponse>>>> list();

    @POST("recommendations/{recommendationId}/acknowledged")
    Observable<Result<BaseRemoteResponse<MedicalRecommendationItemRemoteResponse>>> markAsRead(@Path("recommendationId") Integer recommendationId);
}
