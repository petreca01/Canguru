package br.com.taqtile.android.app.presentation.profilepublic.viewmodel;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;

/**
 * Created by taqtile on 4/27/17.
 */

public class PublicProfileViewModel {

  private ProfileDetailsViewModel profileDetailsViewModel;
  private List<SocialFeedViewModel> socialFeedViewModels;

  public PublicProfileViewModel(ProfileDetailsViewModel profileDetailsViewModel,
                             List<SocialFeedViewModel> socialFeedViewModels) {
    this.profileDetailsViewModel = profileDetailsViewModel;
    this.socialFeedViewModels = socialFeedViewModels;
  }

  public ProfileDetailsViewModel getProfileDetailsViewModel() {
    return profileDetailsViewModel;
  }

  public List<SocialFeedViewModel> getSocialFeedViewModels() {
    return socialFeedViewModels;
  }

}
