package br.com.taqtile.android.app.presentation.manageprofessionals;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.professional.AuthorizeProfessionalUseCase;
import br.com.taqtile.android.app.domain.professional.RevokeProfessionalUseCase;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalAuthorizationParams;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 25/04/17.
 */

public class ManageProfessionalsPresenter implements ManageProfessionalsContract.Presenter {

  private AuthorizeProfessionalUseCase authorizeProfessionalUseCase;
  private RevokeProfessionalUseCase revokeProfessionalUseCase;

  public ManageProfessionalsPresenter() {
    authorizeProfessionalUseCase = Injection.provideAuthorizeProfessionalUseCase();
    revokeProfessionalUseCase = Injection.provideRevokeProfessionalUseCase();
  }

  @Override public void start() {

  }

  @Override public void resume() {

  }

  public Observable<String> authorizeProfessional(String professionalId) {
    return authorizeProfessionalUseCase.execute(
      getProfessionalAuthorizationParams(professionalId, false));
  }

  public Observable<EmptyResult> revokeProfessional(String professionalId) {
    return revokeProfessionalUseCase.execute(
      getProfessionalAuthorizationParams(professionalId, false));
  }

  private ProfessionalAuthorizationParams getProfessionalAuthorizationParams(String professionalId,
    Boolean questionnaireOnly) {
    Integer professionalIdInteger = Integer.parseInt(professionalId);
    return new ProfessionalAuthorizationParams(professionalIdInteger, questionnaireOnly);
  }
}
