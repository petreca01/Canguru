package br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.healthcaredetails.HealthcareDetailsActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 20/04/17.
 */

public class HealthcareUserAdditionalInfoActivity extends TemplateBackActivity {

  public static int EDIT_USER_DATA_REQUEST_CODE = 1234;
  private HealthcareUserAdditionalInfoFragment fragment;
  private static HealthOperatorViewModel healthOperatorViewModel;

  @Override
  public Fragment getFragment() {

    fragment = (HealthcareUserAdditionalInfoFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      fragment = HealthcareUserAdditionalInfoFragment.newInstance();
    }
    fragment.setHealthOperatorViewModel(healthOperatorViewModel);

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    HealthcareUserAdditionalInfoPresenter presenter = new HealthcareUserAdditionalInfoPresenter();
    fragment.setPresenter(presenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return presenter;
  }

  public static void navigate(Context context, HealthOperatorViewModel healthOperator, CustomNavigationResultListener listener) {

    healthOperatorViewModel = healthOperator;

    Intent intent = new Intent(context, HealthcareUserAdditionalInfoActivity.class);

    if (context instanceof HealthcareDetailsActivity) {
      ((HealthcareDetailsActivity) context).setResultListener(listener,
        EDIT_USER_DATA_REQUEST_CODE);
      ((HealthcareDetailsActivity) context).startActivityForResult(intent,
        EDIT_USER_DATA_REQUEST_CODE);
    }
  }

}
