package br.com.taqtile.android.app.data.agenda.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.request.FinishAppointmentRemoteRequest;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;

/**
 * Created by taqtile on 4/7/17.
 */

public class FinishAppointmentParamsToFinishAppointmentRemoteRequestMapper {

  public static FinishAppointmentRemoteRequest perform(FinishAppointmentParams finishAppointmentParams) {
    return new FinishAppointmentRemoteRequest(finishAppointmentParams.getAgendaId(),
      finishAppointmentParams.getIsAppointment(),
      finishAppointmentParams.getDone());
  }
}
