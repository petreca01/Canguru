package br.com.taqtile.android.app.data.common.Toolbox;

import br.com.taqtile.android.app.data.common.prefser.PrefserFactory;
import br.com.taqtile.android.app.data.common.prefser.PrefserWrapper;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.support.GlobalConstants;
import rx.Observable;

/**
 * Created by taqtile on 3/9/17.
 */

public class Toolbox {

  private static Toolbox instance;
  private PrefserWrapper prefserWrapper;

  public static Toolbox getInstance() {
    if (instance == null) {
      instance = new Toolbox();
    }
    return instance;
  }

  public Toolbox() {
    prefserWrapper = new PrefserWrapper(PrefserFactory.build());
  }

  public Observable<String> getUserToken() {
    return prefserWrapper.getString(GlobalConstants.USER_TOKEN_KEY);
  }

  public Observable<String> getUserProfilePictureURL() {
    return prefserWrapper.getString(GlobalConstants.USER_PROFILE_PICTURE_URL_KEY);
  }

  public Observable<Integer> getUserId() {
    return prefserWrapper.getInteger(GlobalConstants.USER_ID_KEY);
  }

  public Observable<String> getUserName() {
    return prefserWrapper.getString(GlobalConstants.USER_NAME_KEY);
  }

  public Observable<String> getUserEmail() {
    return prefserWrapper.getString(GlobalConstants.USER_EMAIL_KEY);
  }

  public Observable<String> getUserZipCode() {
    return prefserWrapper.getString(GlobalConstants.USER_ZIP_CODE_KEY);
  }

  public Observable<Boolean> getMainTutorial() {
    return prefserWrapper.getBoolean(GlobalConstants.MAIN_TUTORIAL_KEY);
  }

  public Observable<Boolean> getDiaryTutorial() {
    return prefserWrapper.getBoolean(GlobalConstants.DIARY_TUTORIAL_KEY);
  }

  public Observable<Boolean> userHasCPFAndPhoneNumber() {
    return prefserWrapper.getBoolean(GlobalConstants.USER_HAS_CPF_AND_PHONE_NUMBER_KEY);
  }

  public Observable<Boolean> isLoggedIn() {
    return prefserWrapper.getString(GlobalConstants.USER_TOKEN_KEY)
      .map(result -> !result.isEmpty());
  }

  // TODO: 5/30/17 remove put methods from toolbox (they are used inside frameLayouts components)
  public Observable<EmptyResult> saveMainTutorial() {
    return prefserWrapper.putBoolean(GlobalConstants.MAIN_TUTORIAL_KEY, true);
  }

  public Observable<EmptyResult> saveDiaryTutorial() {
    return prefserWrapper.putBoolean(GlobalConstants.DIARY_TUTORIAL_KEY, true);
  }

  public Observable<EmptyResult> saveProfilePicture(String profilePictureUrl) {
    return prefserWrapper.putString(GlobalConstants.USER_PROFILE_PICTURE_URL_KEY, profilePictureUrl);
  }
}
