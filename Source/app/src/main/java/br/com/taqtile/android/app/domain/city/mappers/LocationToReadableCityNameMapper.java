package br.com.taqtile.android.app.domain.city.mappers;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import br.com.taqtile.android.app.support.TemplateApplication;

/**
 * Created by taqtile on 4/24/17.
 */

public class LocationToReadableCityNameMapper {
  public static LocationDiscoveryResult getReverseGeocode(Location location) {
    Geocoder geocoder = new Geocoder(TemplateApplication.getContext(), Locale.getDefault());
    List<Address> addresses;

    try {
      addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
      Address address = addresses.get(0);

      return new LocationDiscoveryResult(
        address.getPostalCode(),
        address.getLocality(),
        getState(address),
        String.valueOf(address.getLatitude()),
        String.valueOf(address.getLongitude()));

    } catch (IOException e) {
      return new LocationDiscoveryResult("","","","","");
    }
  }

  private static String getState(Address address) {
    if (address != null) {
      if(address.getAddressLine(1) != null) {
        if(address.getAddressLine(1).length() >= 2) {
          return address.getAddressLine(1).substring(address.getAddressLine(1).length() - 2);
        }
      }
    }
    return "";
  }
}
