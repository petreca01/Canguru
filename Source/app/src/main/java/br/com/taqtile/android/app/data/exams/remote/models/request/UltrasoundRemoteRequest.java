package br.com.taqtile.android.app.data.exams.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/22/17.
 */

public class UltrasoundRemoteRequest extends BaseRemoteRequest {
  @RequestName("codigo")
  private String code;

  @RequestName("dias")
  private Integer days;

  @RequestName("semanas")
  private Integer weeks;

  @RequestName("data")
  private String date;

  private static final String EXAM_CODE = "ultra1";

  public UltrasoundRemoteRequest(Integer days, Integer weeks, String date) {
    this.code = EXAM_CODE;
    this.days = days;
    this.weeks = weeks;
    this.date = date;
  }
}
