package br.com.taqtile.android.app.presentation.symptomdetail;

import android.os.Bundle;

import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.presentation.common.BundleAccessor;

import static br.com.taqtile.android.app.presentation.symptomdetail.SymptomDetailBundleConstants.SYMPTOM_KEY;

/**
 * Created by taqtile on 10/05/17.
 */

public class SymptomDetailAccessor extends BundleAccessor {

  public SymptomDetailAccessor(Bundle bundle) {
    super(bundle);
  }


  public SubSymptomViewModel getSymptom() {
    if (bundle != null) {
      return bundle.getParcelable(SYMPTOM_KEY);
    }
    return null;
  }

}
