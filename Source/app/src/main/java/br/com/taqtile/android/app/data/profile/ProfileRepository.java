package br.com.taqtile.android.app.data.profile;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.forum.remote.mappers.PostItemResponseToPostMapper;
import br.com.taqtile.android.app.data.profile.local.ImageDataSource;
import br.com.taqtile.android.app.data.profile.local.mappers.EditUserParamsToUserImagesRequestMapper;
import br.com.taqtile.android.app.data.profile.remote.ProfileRemoteDataSource;
import br.com.taqtile.android.app.data.profile.remote.mappers.EditUserParamsToEditPublicProfileRemoteRequestMapper;
import br.com.taqtile.android.app.data.profile.remote.mappers.ProfileDetailsRemoteResponseToProfileDetailsResultMapper;
import br.com.taqtile.android.app.data.profile.remote.mappers.UpdateProfileDetailsRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.profile.remote.mappers.UserParamsToGetProfileDetailsRemoteRequestMapper;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;
import br.com.taqtile.android.app.domain.profile.models.ProfileDetailsResult;
import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;
import br.com.taqtile.android.app.domain.profile.models.UserProfileParams;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfileRepository extends BaseRepository<ProfileRemoteDataSource, ImageDataSource> {

  public ProfileRepository(ProfileRemoteDataSource profileRemoteDataSource,
    ImageDataSource imageDataSource) {
    super(profileRemoteDataSource, imageDataSource);
  }

  public Observable<ProfileDetailsResult> getProfileDetails(UserProfileParams userProfileParams) {
    return getRemoteDataSource().getProfileDetails(
      UserParamsToGetProfileDetailsRemoteRequestMapper.perform(userProfileParams))
      .map(result -> ProfileDetailsRemoteResponseToProfileDetailsResultMapper.perform(
        result.getResult()));
  }

  public Observable<UpdateProfileDetailsResult> editProfile(EditProfileParams editProfileParams) {
    return getLocalDataSource().getFile(
      EditUserParamsToUserImagesRequestMapper.perform(editProfileParams))
      .map(userImagesResponse -> EditUserParamsToEditPublicProfileRemoteRequestMapper.perform(
        editProfileParams, userImagesResponse))
      .flatMap(editPublicProfileRemoteRequest -> getRemoteDataSource().editPublicProfile(
        editPublicProfileRemoteRequest))
      .map(result -> UpdateProfileDetailsRemoteResponseToResultMapper.perform(
        result.getResult()));
  }

  public Observable<List<PostResult>> getPosts(UserProfileParams userProfileParams) {
    return getRemoteDataSource().listPosts(userProfileParams.getProfileId())
      .map(result -> PostItemResponseToPostMapper.perform(result.getResult()));
  }
}
