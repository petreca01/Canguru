package br.com.taqtile.android.app.data.common.geocoding;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.taqtile.android.app.data.city.geocondingAPI.LocationDiscoveryServices;
import br.com.taqtile.android.app.data.common.remote.RequestInterceptor;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import rx.Emitter;
import rx.Observable;

import static br.com.taqtile.android.app.listings.Constants.GOOGLE_API_KEY;

/**
 * Created by taqtile on 6/2/17.
 */

public class GeocodingServiceFactory {

  public static Observable<LocationDiscoveryServices> getGeocodingServiceFactory() {
    GeocodingService service = new GeocodingService();
    return service.getGeocodingService();
  }
}

class GeocodingService {

  public static final int TIMEOUT = 30;

  protected Observable<LocationDiscoveryServices> getGeocodingService() {

    return Observable.just(null)
      .map(any -> GeocodingHelper.getRetrofitBuilder()
        .client(new OkHttpClient.Builder()
          .readTimeout(TIMEOUT, TimeUnit.SECONDS)
          .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
          .addInterceptor(new GeocodingInterceptor())
          .build()))
      .map(Retrofit.Builder::build)
      .map(retrofit -> retrofit.create(LocationDiscoveryServices.class));
  }
}

class GeocodingInterceptor implements Interceptor {

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request original = chain.request();
    HttpUrl originalHttpUrl = original.url();

    HttpUrl url = originalHttpUrl.newBuilder()
      .addQueryParameter("key", GOOGLE_API_KEY)
      .build();

    Request.Builder requestBuilder = original.newBuilder()
      .url(url);

    Request request = requestBuilder.build();
    return chain.proceed(request);
  }
}
