package br.com.taqtile.android.app.data.professional.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.professional.remote.models.response.ProfessionalInfoItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public interface ProfessionalServices {
    @POST("professionals")
    Observable<Result<BaseRemoteResponse<List<ProfessionalInfoItemRemoteResponse>>>> list();

    @FormUrlEncoded
    @POST("professional/authorize")
    Observable<Result<BaseRemoteResponse<ProfessionalInfoItemRemoteResponse>>> authorizedAccess(
      @FieldMap Map<String, String> authorizedAccessRemoteRequest);

    @FormUrlEncoded
    @POST("professional/revoke")
    Observable<Result<BaseRemoteResponse<ProfessionalInfoItemRemoteResponse>>> revokeAccess(
      @FieldMap Map<String, String> revokeAccessRemoteRequest);
}
