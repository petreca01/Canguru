package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardUltrasoundsRemoteResponse {

  @SerializedName("id")
  private Integer id;

  @SerializedName("consultation_id")
  private Integer consultationId;

  @SerializedName("date")
  private String date;

  @SerializedName("period_pregnancy_weeks")
  private Integer periodPregnancyWeeks;

  @SerializedName("period_pregnancy_days")
  private Integer periodPregnancyDays;

  @SerializedName("ultrasound_pregnancy_weeks")
  private Integer ultrasoundPregnancyWeeks;

  @SerializedName("ultrasound_pregnancy_days")
  private Integer ultrasoundPregnancyDays;

  @SerializedName("fetal_weight")
  private Integer fetalWeight;

  @SerializedName("placenta")
  private String placenta;

  @SerializedName("liquid")
  private String liquid;

  @SerializedName("pbf")
  private String pbf;

  @SerializedName("created_at")
  private String createdAt;

  @SerializedName("doctor_id")
  private Integer doctorId;

  @SerializedName("doctor_name")
  private String doctorName;

  public Integer getId() {
    return id;
  }

  public Integer getConsultationId() {
    return consultationId;
  }

  public String getDate() {
    return date;
  }

  public Integer getPeriodPregnancyWeeks() {
    return periodPregnancyWeeks;
  }

  public Integer getPeriodPregnancyDays() {
    return periodPregnancyDays;
  }

  public Integer getUltrasoundPregnancyWeeks() {
    return ultrasoundPregnancyWeeks;
  }

  public Integer getUltrasoundPregnancyDays() {
    return ultrasoundPregnancyDays;
  }

  public Integer getFetalWeight() {
    return fetalWeight;
  }

  public String getPlacenta() {
    return placenta;
  }

  public String getLiquid() {
    return liquid;
  }

  public String getPbf() {
    return pbf;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public String getDoctorName() {
    return doctorName;
  }
}
