package br.com.taqtile.android.app.domain.channel.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.common.ProfessionalValidator;
import br.com.taqtile.android.app.domain.forum.models.PostAndRepliesResult;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.domain.forum.models.ReplyResult;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedCommunityImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/20/17.
 */

public class ForumPostAndRepliesResultToListingsViewModelMapper {

  public static List<ListingsViewModel> perform(PostAndRepliesResult postAndRepliesResult) {
    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.add(map(postAndRepliesResult.getPost()));
    listingsViewModels.addAll(map(postAndRepliesResult.getReplies()));
    return listingsViewModels;
  }

  private static SocialFeedCommunityImplViewModel map(PostResult postResult) {
    return new SocialFeedCommunityImplViewModel(postResult.getId(), postResult.getUserId(),
      postResult.getUserPicture(), postResult.getUsername(), postResult.getCreatedAt(),
      postResult.getTitle(), postResult.getContent(), postResult.getLikes(),
      postResult.getCountReplies(),
      postResult.getLiked(),
      ProfessionalValidator.isProfessional(postResult.getUserType()));
  }

  private static List<SocialFeedCommunityImplViewModel> map(List<ReplyResult> replyResults) {
    return StreamSupport.stream(replyResults)
      .map(ForumPostAndRepliesResultToListingsViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static SocialFeedCommunityImplViewModel map(ReplyResult replyResult) {
    return new SocialFeedCommunityImplViewModel(replyResult.getId(), replyResult.getUserId(),
      replyResult.getUserPicture(), replyResult.getUsername(), replyResult.getCreatedAt(),
      replyResult.getReply(), replyResult.getLikes(),
      replyResult.getLiked());
  }
}
