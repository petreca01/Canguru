package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/21/17.
 */

public class NewPostRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("operator_id")
    private Integer operatorId;

    @SerializedName("usuario_id")
    private Integer userId;

    @SerializedName("title")
    private String title;

    @SerializedName("content")
    private String content;

    @SerializedName("active")
    private Integer active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Integer getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
