package br.com.taqtile.android.app.domain.channel.models;

import java.util.List;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelWithCategoryResult {

  private String category;
  private List<ChannelResult> channelList;

  public ChannelWithCategoryResult(String category, List<ChannelResult> channelList) {
    this.category = category;
    this.channelList = channelList;
  }

  public String getCategory(){
    return category;
  }

  public List<ChannelResult> getChannels(){
    return channelList;
  }

}
