package br.com.taqtile.android.app.data.deviceregistration;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.deviceregistration.remote.DeviceRemoteDataSource;
import br.com.taqtile.android.app.data.deviceregistration.remote.mappers.DeviceInfoParamsToDeviceInfoRemoteRequestMapper;
import br.com.taqtile.android.app.domain.deviceregistration.models.DeviceInfoParams;
import rx.Observable;

/**
 * Created by taqtile on 7/11/16.
 */

public class DeviceInfoRepository extends BaseRepository<DeviceRemoteDataSource, Void> {
  public DeviceInfoRepository(DeviceRemoteDataSource deviceRemoteDataSource) {
    super(deviceRemoteDataSource, null);
  }

  public Observable<EmptyResult> register(DeviceInfoParams data) {
    return getRemoteDataSource().register(DeviceInfoParamsToDeviceInfoRemoteRequestMapper.perform(data))
      .map(BaseRemoteResponse::getResult);
  }
}
