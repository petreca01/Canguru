package br.com.taqtile.android.app.domain.professional.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfessionalResult {

  private Integer id;

  private Integer professionalId;

  private String professionalName;

  private String authorizedAt;

  private Boolean riskQuestionnaireAuthorized;

  private String revokedAt;

  private String createdAt;

  private String updatedAt;

  public ProfessionalResult(Integer id, Integer professionalId, String professionalName,
    String authorizedAt, Boolean riskQuestionnaireAuthorized, String revokedAt, String createdAt,
    String updatedAt) {
    this.id = id;
    this.professionalId = professionalId;
    this.professionalName = professionalName;
    this.authorizedAt = authorizedAt;
    this.riskQuestionnaireAuthorized = riskQuestionnaireAuthorized;
    this.revokedAt = revokedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public Integer getId() {
    return id;
  }

  public Integer getProfessionalId() {
    return professionalId;
  }

  public String getProfessionalName() {
    return professionalName;
  }

  public String getAuthorizedAt() {
    return authorizedAt;
  }

  public Boolean getRiskQuestionnaireAuthorized() {
    return riskQuestionnaireAuthorized;
  }

  public String getRevokedAt() {
    return revokedAt;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }
}
