package br.com.taqtile.android.app.domain.riskAnalysis;

import br.com.taqtile.android.app.data.riskAnalysis.RiskAnalysisRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.riskAnalysis.mappers.RiskAnalysisQuestionsResultToRiskAnalysisQuestionResultMapper;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import rx.Observable;

/**
 * Created by felipesabino on 5/17/17.
 */

public class FirstQuestionForGroupUseCase extends BaseUseCase<RiskAnalysisQuestionResult,
  RiskAnalysisQuestionGroupResult, RiskAnalysisRepository> {

  public FirstQuestionForGroupUseCase(RiskAnalysisRepository riskAnalysisRepository) {
    super(riskAnalysisRepository);
  }

  @Override
  public Observable<RiskAnalysisQuestionResult> execute(RiskAnalysisQuestionGroupResult params) {

    return getRepository().list(new ViewModeParams())
      .flatMap(result ->
        RiskAnalysisQuestionsResultToRiskAnalysisQuestionResultMapper.perform(result, params, -1));
  }
}
