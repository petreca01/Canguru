package br.com.taqtile.android.app.data.professional.remote.models.response;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfessionalInfoItemRemoteResponse {
    @SerializedName("loaded")
    private Boolean loaded;

    @SerializedName("id")
    private Integer id;

    @SerializedName("professional_id")
    private Integer professionalId;

    @SerializedName("professional_name")
    private String professionalName;

    @SerializedName("professional_email")
    private String professionalEmail;

    @SerializedName("patient_id")
    private Integer patientId;

    @SerializedName("usuario")
    private UserRemoteResponse user;

    @SerializedName("usuario_id")
    private Integer userId;

    @SerializedName("authorized_at")
    private String authorizedAt;

    @SerializedName("risk_questionnaire_authorized")
    private Boolean riskQuestionnaireAuthorized;

    @SerializedName("revoked_at")
    private String revokedAt;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    public Boolean getLoaded() {
        return loaded;
    }

    public Integer getId() {
        return id;
    }

    public Integer getProfessionalId() {
        return professionalId;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public String getProfessionalEmail() {
        return professionalEmail;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public UserRemoteResponse getUser() {
        return user;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getAuthorizedAt() {
        return authorizedAt;
    }

    public Boolean getRiskQuestionnaireAuthorized() {
        return riskQuestionnaireAuthorized;
    }

    public String getRevokedAt() {
        return revokedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
