package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.view.View;

/**
 * Created by taqtile on 5/30/17.
 */

public interface SurroundingShape extends Shape {

  void focusIn(View target);
}
