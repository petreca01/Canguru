package br.com.taqtile.android.app.domain.city;

import br.com.taqtile.android.app.data.city.LocationDiscoveryRepository;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.presentation.common.PresenterError;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class LocationDiscoveryByZipCodeUseCase extends BaseUseCase<LocationDiscoveryResult,
  LocationDiscoveryByZipCodeParams, LocationDiscoveryRepository> {

  public LocationDiscoveryByZipCodeUseCase(LocationDiscoveryRepository locationDiscoveryRepository) {
    super(locationDiscoveryRepository);
  }

  @Override
  public Observable<LocationDiscoveryResult> execute(LocationDiscoveryByZipCodeParams params) {
    if (params.getZipCode() != null) {
      return getRepository().searchCityByZipCode(params);
    } else {
      PresenterError error = new PresenterError();
      error.setErrorType(PresenterError.INVALID_DATA);
      return Observable.error(error);
    }
  }
}
