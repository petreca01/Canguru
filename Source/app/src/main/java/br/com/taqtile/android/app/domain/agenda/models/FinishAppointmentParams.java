package br.com.taqtile.android.app.domain.agenda.models;

/**
 * Created by taqtile on 4/7/17.
 */

public class FinishAppointmentParams {

  public static final Integer TODO = 0;
  public static final Integer DONE = 1;

  private Integer agendaId;
  private Integer isAppointment;
  private Integer done;

  public FinishAppointmentParams(Integer agendaId, Integer isAppointment, Integer done) {
    this.agendaId = agendaId;
    this.isAppointment = isAppointment;
    this.done = done;
  }

  public Integer getAgendaId() {
    return agendaId;
  }

  public Integer getIsAppointment() {
    return isAppointment;
  }

  public Integer getDone() {
    return done;
  }
}
