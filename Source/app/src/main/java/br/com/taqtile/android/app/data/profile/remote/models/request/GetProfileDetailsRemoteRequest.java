package br.com.taqtile.android.app.data.profile.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class GetProfileDetailsRemoteRequest extends BaseRemoteRequest {
    @RequestName("profile_id")
    private Integer profileId;

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }
}
