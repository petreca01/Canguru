package br.com.taqtile.android.app.presentation.healthcaredetails;

import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public interface HealthcareDetailsContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> checkUserData(String healthOperatorDocumentType);

    Observable<UserViewModel> addHealthOperator(EditUserDataParams editUserDataParams);
  }

}
