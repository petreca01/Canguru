package br.com.taqtile.android.app.domain.questionnaires.models;

/**
 * Created by felipesabino on 5/18/17.
 */

public class QuestionItemUnlockResult {

  public QuestionItemUnlockResult(Integer morethan) {
    this.moreThan = morethan;
    this.isMoreThan = true;
  }

  private Integer moreThan;
  private Boolean isMoreThan;

  public Integer getMoreThan() {
    return moreThan;
  }

  public Boolean isMoreThan() {
    return isMoreThan;
  }

}
