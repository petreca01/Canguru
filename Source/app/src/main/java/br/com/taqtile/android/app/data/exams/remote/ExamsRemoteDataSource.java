package br.com.taqtile.android.app.data.exams.remote;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.exams.remote.models.request.UltrasoundRemoteRequest;
import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsGetRemoteResponse;
import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsInfoItemRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 3/22/17.
 */

public class ExamsRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ExamsServices> services;

  public ExamsRemoteDataSource() {
    super();
    this.services = ServiceFactory.getObservableAuthenticated(ExamsServices.class);
  }

  public Observable<BaseRemoteResponse<ExamsGetRemoteResponse>> fetchExam() {
    return performRequest(services.flatMap(ExamsServices::list));
  }

  // TODO: 6/20/17 Will be removed later. It is an easy and fast approach to cover an APIs issue
  public Observable<BaseRemoteResponse<Boolean>> fetchExamLegacy() {
    return performRequest(services.flatMap(ExamsServices::legacyList));
  }

  public Observable<BaseRemoteResponse<List<ExamsInfoItemRemoteResponse>>> createUltrasound(
    UltrasoundRemoteRequest request) {
    return performRequest(
      services.flatMap(services -> services.createUltrasound(request.getFormData())));
  }
}
