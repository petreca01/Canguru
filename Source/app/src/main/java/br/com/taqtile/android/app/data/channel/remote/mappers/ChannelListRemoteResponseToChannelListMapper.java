package br.com.taqtile.android.app.data.channel.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.channel.mappers.ChannelCategoryRemoteResponseToChannelCategoryResultMapper;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelCategoryListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelCategoryResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 3/24/17.
 */

public class ChannelListRemoteResponseToChannelListMapper {
  static public ChannelCategoryResult map(ChannelCategoryListRemoteResponse response) {

    return ChannelCategoryRemoteResponseToChannelCategoryResultMapper.perform(response);

  }

}
