package br.com.taqtile.android.app.data.healthOperators;

import java.util.List;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.healthOperators.remote.HealthOperatorsRemoteDataSource;
import br.com.taqtile.android.app.data.healthOperators.remote.mappers.HealthOperatorItemRemoteResponseToHealtOperatorItemResultMapper;
import br.com.taqtile.android.app.data.healthOperators.remote.mappers.SearchHealthOperatorParamsToSearchHealthOperatorRemoteRequestMapper;
import br.com.taqtile.android.app.domain.healthOperators.models.HealthOperatorItemResult;
import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class HealthOperatorsRepository extends BaseRepository<HealthOperatorsRemoteDataSource, Void> {
  public HealthOperatorsRepository(HealthOperatorsRemoteDataSource healthOperatorsRemoteDataSource) {
    super(healthOperatorsRemoteDataSource, null);
  }

  public Observable<List<HealthOperatorItemResult>> list() {
    return getRemoteDataSource()
      .list()
      .map(result ->
        HealthOperatorItemRemoteResponseToHealtOperatorItemResultMapper.perform(result.getResult()));
  }

  public Observable<List<HealthOperatorItemResult>> search(SearchHealthOperatorParams params) {
    return getRemoteDataSource()
      .search(SearchHealthOperatorParamsToSearchHealthOperatorRemoteRequestMapper.perform(params))
      .map(result ->
        HealthOperatorItemRemoteResponseToHealtOperatorItemResultMapper.perform(result.getResult()));
  }
}
