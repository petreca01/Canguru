package br.com.taqtile.android.app.presentation.additionalinfo;

import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by Renato on 4/20/17.
 */

public interface AdditionalInfoContract {

  interface Presenter extends BasePresenter {
    Observable<AdditionalInfoUserViewModel> fetchUserData();

    Observable<UserViewModel> updateLocation(EditUserDataParams editUserDataParams);
    Observable<UserViewModel> editUserData(EditUserDataParams editUserDataParams);


  }

}
