package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.forum.remote.models.request.ContentSuggestionRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionParams;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionParamsToContentSuggestionRemoteRequestMapper {

  public static ContentSuggestionRemoteRequest perform(ContentSuggestionParams params){
    return new ContentSuggestionRemoteRequest(
      params.getSearch(),
      params.getPage(),
      params.getPerPage()
      );
  }

}
