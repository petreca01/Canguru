package br.com.taqtile.android.app.presentation.gestationbabydevelopmentdetails;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 12/05/17.
 */

public interface GestationBabyDevelopmentDetailsContract {
  interface Presenter extends BasePresenter {
  }
}
