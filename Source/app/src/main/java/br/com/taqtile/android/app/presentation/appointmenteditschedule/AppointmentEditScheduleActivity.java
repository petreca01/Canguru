package br.com.taqtile.android.app.presentation.appointmenteditschedule;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.appointmentdetail.AppointmentDetailActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.presentation.diary.DiaryActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.EDIT_APPOINTMENT_REQUEST_CODE;

/**
 * Created by taqtile on 27/04/17.
 */

public class AppointmentEditScheduleActivity extends TemplateBackActivity {

  public static final String APPOINTMENT_MODEL_KEY = "appointmentTitleKey";
  public static final String EDITED_APPOINTMENT_MODEL_KEY = "editedAppointmentTitleKey";
  private AppointmentEditScheduleFragment fragment;

  @Override
  public Fragment getFragment() {
    AppointmentEditScheduleFragment appointmentEditScheduleFragment =
      (AppointmentEditScheduleFragment) getSupportFragmentManager()
        .findFragmentById(getFragmentContainerId());
    if (appointmentEditScheduleFragment == null) {
      appointmentEditScheduleFragment = AppointmentEditScheduleFragment.newInstance();
    }
    fragment = appointmentEditScheduleFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    AppointmentEditSchedulePresenter appointmentEditSchedulePresenter =
      new AppointmentEditSchedulePresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(appointmentEditSchedulePresenter);

    return appointmentEditSchedulePresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {
    AppointmentEditScheduleActivity.navigate(context, listener, new AgendaItemViewModel());
  }

  public static void navigate(Context context, CustomNavigationResultListener listener,
                              AgendaItemViewModel agendaItemViewModel) {
    Intent intent = new Intent(context, AppointmentEditScheduleActivity.class);
    intent.putExtra(APPOINTMENT_MODEL_KEY, agendaItemViewModel);

    if (context instanceof MainActivity){
      ((MainActivity) context).setResultListener(listener,
        EDIT_APPOINTMENT_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        EDIT_APPOINTMENT_REQUEST_CODE);
    } else if (context instanceof AppointmentDetailActivity){
      ((AppointmentDetailActivity) context).setResultListener(listener,
        EDIT_APPOINTMENT_REQUEST_CODE);
      ((AppointmentDetailActivity) context).startActivityForResult(intent,
        EDIT_APPOINTMENT_REQUEST_CODE);
    }
  }
}
