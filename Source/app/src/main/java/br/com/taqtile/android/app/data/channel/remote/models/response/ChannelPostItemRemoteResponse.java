package br.com.taqtile.android.app.data.channel.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/17/17.
 */

public class ChannelPostItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("type")
    private String type;

    @SerializedName("channel_id")
    private Integer channelId;

    @SerializedName("channel_name")
    private String channelName;

    @SerializedName("channel_author")
    private String channelAuthor;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("post")
    private String post;

    @SerializedName("image")
    private String image;

    @SerializedName("video")
    private String video;

    @SerializedName("schedule")
    private String schedule;

    @SerializedName("visualizations")
    private Integer visualizations;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("replies")
    private Integer replies;

    @SerializedName("likes")
    private Integer likes;

    @SerializedName("liked")
    private Boolean liked;

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getChannelAuthor() {
        return channelAuthor;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPost() {
        return post;
    }

    public String getImage() {
        return image;
    }

    public String getVideo() {
        return video;
    }

    public String getSchedule() {
        return schedule;
    }

    public Integer getVisualizations() {
        return visualizations;
    }

    public Boolean getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Integer getReplies() {
        return replies;
    }

    public Integer getLikes() {
        return likes;
    }

    public Boolean getLiked() {
        return liked;
    }
}
