package br.com.taqtile.android.app.data.weeklycontent.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.weeklycontent.remote.models.request.WeeklyContentRemoteRequest;
import br.com.taqtile.android.app.data.weeklycontent.remote.models.response.WeeklyContentRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentDataSource extends BaseRemoteDataSource {

  private Observable<WeeklyContentServices> services;

  public WeeklyContentDataSource() {
    super();
    this.services = ServiceFactory.getObservableAuthenticated(WeeklyContentServices.class);
  }

  public Observable<BaseRemoteResponse<WeeklyContentRemoteResponse>>
  fetchWeeklyContent(WeeklyContentRemoteRequest weeklyContentRemoteRequest) {
    return performRequest(services
      .flatMap(services -> services.fetchContent(weeklyContentRemoteRequest.getWeek())));
  }
}
