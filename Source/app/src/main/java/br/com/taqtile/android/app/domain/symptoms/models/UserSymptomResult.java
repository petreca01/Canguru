package br.com.taqtile.android.app.domain.symptoms.models;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class UserSymptomResult {

  private String date;
  private Integer symptomId;
  private String symptomName;
  private Integer gestationAgeWeeks;
  private Integer gestationAgeDays;

  public UserSymptomResult(String date, Integer symptomId, String symptomName,
                           Integer gestationAgeWeeks, Integer gestationAgeDays) {
    this.date = date;
    this.symptomId = symptomId;
    this.symptomName = symptomName;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.gestationAgeDays = gestationAgeDays;
  }

  public String getDate() {
    return date;
  }

  public Integer getSymptomId() {
    return symptomId;
  }

  public String getSymptomName() {
    return symptomName;
  }

  public Integer getGestationAgeWeeks() {
    return gestationAgeWeeks;
  }

  public Integer getGestationAgeDays() {
    return gestationAgeDays;
  }
}
