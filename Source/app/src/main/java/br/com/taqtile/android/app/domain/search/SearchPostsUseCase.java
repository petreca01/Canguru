package br.com.taqtile.android.app.domain.search;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.domain.forum.models.PostsListParams;
import br.com.taqtile.android.app.domain.search.models.PostSearchParams;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 28/03/17.
 */

public class SearchPostsUseCase extends BaseUseCase<List<PostResult>, PostSearchParams, ForumRepository> {
  private Integer currentPage;

  public SearchPostsUseCase(ForumRepository forumRepository) {
    super(forumRepository);
    currentPage = 1;
  }

  @Override
  public Observable<List<PostResult>> execute(PostSearchParams postSearchParams) {

    currentPage = postSearchParams.isReload() && currentPage > 1 ? 1 : currentPage;

    PostsListParams postsListParams = new PostsListParams(
        currentPage,
        PostsListParams.DEFAULT_PER_PAGE,
        PostsListParams.ORDER_DIRECTION_DESC,
        PostsListParams.ORDER_BY_CREATED_AT,
        postSearchParams.getSearchKeyword());

    return getRepository().listPosts(postsListParams)
        .doOnNext(forumPostResults -> currentPage++);

  }
}
