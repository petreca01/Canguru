package br.com.taqtile.android.app.data.symptoms.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/23/17.
 */

public class SubSymptomItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("parent_id")
    private Integer parentId;

    @SerializedName("name")
    private String name;

    @SerializedName("ciap")
    private String ciap;

    @SerializedName("condition")
    private String condition;

    @SerializedName("condition_trigger")
    private String conditionTrigger;

    @SerializedName("explanation")
    private String explanation;

    @SerializedName("how_to_avoid")
    private String howToAvoid;

    @SerializedName("when_to_worry")
    private String whenToWorry;

    @SerializedName("dangerous")
    private Boolean dangerous;

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getCiap() {
        return ciap;
    }

    public String getCondition() {
        return condition;
    }

    public String getConditionTrigger() {
        return conditionTrigger;
    }

    public String getExplanation() {
        return explanation;
    }

    public String getHowToAvoid() {
        return howToAvoid;
    }

    public String getWhenToWorry() {
        return whenToWorry;
    }

    public Boolean getDangerous() {
        return dangerous;
    }
}
