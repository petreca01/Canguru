package br.com.taqtile.android.app.data.forum.remote.mappers;

import br.com.taqtile.android.app.data.forum.remote.models.request.PostRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.PostParams;

/**
 * Created by taqtile on 4/17/17.
 */

public class PostParamsToRemoteRequestMapper {
  public static PostRemoteRequest perform(PostParams params) {
    return new PostRemoteRequest(
      params.getPostId(),
      params.getReplyId(),
      params.getReply(),
      params.getDislike());
  }
}
