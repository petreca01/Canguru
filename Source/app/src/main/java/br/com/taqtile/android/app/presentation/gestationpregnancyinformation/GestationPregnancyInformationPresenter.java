package br.com.taqtile.android.app.presentation.gestationpregnancyinformation;

import br.com.taqtile.android.app.domain.gestation.FetchExamUseCase;
import br.com.taqtile.android.app.domain.gestation.StartOrEditGestationUseCase;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model.ExamViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 12/05/17.
 */

public class GestationPregnancyInformationPresenter implements GestationPregnancyInformationContract.Presenter {

  private StartOrEditGestationUseCase startOrEditGestationUseCase;
  private FetchExamUseCase fetchExamUseCase;

  public GestationPregnancyInformationPresenter() {
    this.startOrEditGestationUseCase = Injection.provideStartGestationUseCase();
    this.fetchExamUseCase = Injection.provideFetchExamUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> startOrEditGestation(StartOrEditGestationParams startOrEditGestationParams) {
    return startOrEditGestationUseCase.execute(startOrEditGestationParams);
  }

  @Override
  public Observable<ExamViewModel> fetchExam() {
    return fetchExamUseCase.execute(null);
  }
}
