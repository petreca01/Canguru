package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.medicalRecommendations.MarkAsReadUseCase;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationParams;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 08/05/17.
 */

public class MedicalRecommendationDetailPresenter implements MedicalRecommendationDetailContract.Presenter {

  private MarkAsReadUseCase markAsReadUseCase;

  public MedicalRecommendationDetailPresenter() {
    markAsReadUseCase = Injection.provideMarkAsReadUseCase();
  }

  @Override public Observable<EmptyResult> markAsRead(String id) {
    return markAsReadUseCase.execute(new MedicalRecommendationParams(Integer.parseInt(id)));
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }
}
