package br.com.taqtile.android.app.data.weeklycontent.remote.mappers;

import br.com.taqtile.android.app.data.weeklycontent.remote.models.request.WeeklyContentRemoteRequest;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentParamsToRemoteRequestMapper {

  public static WeeklyContentRemoteRequest perform(WeeklyContentParams weeklyContentParams) {
    return new WeeklyContentRemoteRequest(weeklyContentParams.getWeek());
  }
}
