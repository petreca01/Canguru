package br.com.taqtile.android.app.data.common.remote;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by taqtile on 3/20/17.
 */

public class BaseRemoteDataSource {

  private ResponseValidator responseValidator;

  public BaseRemoteDataSource() {
    responseValidator = new ResponseValidator();
  }

  protected Observable<ResultError> asResultError(Throwable throwable) {
    return Observable.error(new ResultError(throwable));
  }

  protected <T> Observable<T> asResponseModel(Result<T> result) {
    return responseValidator.convertResultToModel(result);
  }

  protected <T> Observable<BaseRemoteResponse<T>> performRequest(
    Observable<Result<BaseRemoteResponse<T>>> observableResponse) {
    return RemoteDataSourceInterceptor.intercept(observableResponse.flatMap(ResponseValidator::convertResultToModel)
      .subscribeOn(Schedulers.io()));
  }

}
