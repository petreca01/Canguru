package br.com.taqtile.android.app.domain.medicalRecommendations.models;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class MedicalRecommendationParams {
  private Integer id;

  public MedicalRecommendationParams(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }
}
