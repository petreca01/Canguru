package br.com.taqtile.android.app.domain.common;

/**
 * Created by taqtile on 04/05/17.
 */

public class ProfessionalValidator {

  public static final String PROFESSIONAL = "professional";

  public static boolean isProfessional(String userType) {
    return userType.equalsIgnoreCase(PROFESSIONAL);
  }
}
