package br.com.taqtile.android.app.domain.professional;

import br.com.taqtile.android.app.domain.professional.mappers.ProfessionalResultListToConnectedProfessionalViewModelListMapper;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import java.util.List;

import br.com.taqtile.android.app.data.professional.ProfessionalRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class ListProfessionalsUseCase extends BaseUseCase<List<ConnectedProfessionalViewModel>, Void,
  ProfessionalRepository> {

  public ListProfessionalsUseCase(ProfessionalRepository professionalRepository) {
    super(professionalRepository);
  }

  @Override
  public Observable<List<ConnectedProfessionalViewModel>> execute(Void aVoid) {
    return getRepository().list().map(
      ProfessionalResultListToConnectedProfessionalViewModelListMapper::perform);
  }
}
