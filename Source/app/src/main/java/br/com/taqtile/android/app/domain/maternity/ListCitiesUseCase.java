package br.com.taqtile.android.app.domain.maternity;

import java.util.List;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class ListCitiesUseCase extends BaseUseCase<List<String>, Void, MaternityRepository> {

  public ListCitiesUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<List<String>> execute(Void aVoid) {
    return getRepository().listCities();
  }
}
