package br.com.taqtile.android.app.presentation.tutorial.showcase.builders;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import br.com.taqtile.android.app.presentation.tutorial.showcase.shapes.Rectangle;

/**
 * Created by taqtile on 5/23/17.
 */

public class FullScreenRectangleBuilder {

  public static Rectangle build(Context context, @ColorInt int color) {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    windowManager.getDefaultDisplay().getMetrics(displayMetrics);

    Rectangle.DrawingParams drawingParams = new Rectangle.DrawingParams(displayMetrics.widthPixels, displayMetrics.heightPixels, color);
    return new Rectangle(drawingParams);

  }
}
