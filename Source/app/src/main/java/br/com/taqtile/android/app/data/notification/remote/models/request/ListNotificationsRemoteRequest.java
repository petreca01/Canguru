package br.com.taqtile.android.app.data.notification.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class ListNotificationsRemoteRequest extends BaseRemoteRequest {
    @RequestName("last_id")
    private Integer lastId;

    public ListNotificationsRemoteRequest(Integer lastId) {
      this.lastId = lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }
}
