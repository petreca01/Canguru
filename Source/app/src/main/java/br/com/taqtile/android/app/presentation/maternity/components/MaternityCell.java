package br.com.taqtile.android.app.presentation.maternity.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.components.BirthsProgressBarComponent;
import br.com.taqtile.android.app.listings.components.RatingViewComponent;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by taqtile on 5/9/17.
 */

public class MaternityCell extends FrameLayout {

  @BindView(R.id.component_maternity_cell_root)
  LinearLayout root;
  @BindView(R.id.component_maternity_cell_image)
  CircleImageView maternityImage;
  @BindView(R.id.component_maternity_cell_title)
  CustomTextView title;
  @BindView(R.id.component_maternity_cell_rating)
  RatingViewComponent ratingComponent;
  @BindView(R.id.component_maternity_cell_births_progressbar)
  BirthsProgressBarComponent birthsProgressBarComponent;

  private MaternityCellListener listener;

  public MaternityCell(Context context) {
    super(context);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_maternity_cell, this, true);
    ButterKnife.bind(this, v);
  }

  public void setData(MaternityViewModel maternity) {
    PicassoHelper.loadImageFromUrl(this.maternityImage,
      maternity.getImage(),
      getContext(),
      R.drawable.ic_maternidade_placeholder);
    this.title.setText(maternity.getName());
    if (maternity.getRate() != null) {
      ratingComponent.setRating(maternity.getRate().intValue(), maternity.getTotalRates());
    }
    birthsProgressBarComponent.setNaturalBirthPercent(maternity.getCurrentPercentNormalBirth().intValue());
    birthsProgressBarComponent.setCesareanBirthPercent(maternity.getCurrentPercentCsections().intValue());
    root.setOnClickListener(view -> {
      if (listener != null) {
        listener.onMaternityCellClicked(maternity.getId());
      }
    });
  }

  public void setListener(MaternityCellListener listener) {
    this.listener = listener;
  }

  public interface MaternityCellListener {
    void onMaternityCellClicked(int id);
  }
}
