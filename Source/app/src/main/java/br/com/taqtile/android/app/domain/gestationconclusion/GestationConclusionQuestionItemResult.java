package br.com.taqtile.android.app.domain.gestationconclusion;

import android.util.Log;

import java.util.List;

import br.com.taqtile.android.app.domain.questionnaires.models.AnswerItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionQuestionItemResult {

  private static final String TAG = "GesConQuesItRes";
  private Integer id;

  private String question;

  private String answerType;

  private List<String> alternatives;

  private RangedAnswerResult range;

  private boolean hasNextQuestion;

  private List<Integer> nextQuestionRules;

  private AnswerItemResult answer;

  public GestationConclusionQuestionItemResult(Integer id, String question, String answerType,
                                               List<String> alternatives, RangedAnswerResult range,
                                               boolean hasNextQuestion, List<Integer> nextQuestionRules,
                                               AnswerItemResult answer) {
    this.id = id;
    this.question = question;
    this.answerType = answerType;
    this.alternatives = alternatives;
    this.range = range;
    this.hasNextQuestion = hasNextQuestion;
    this.nextQuestionRules = nextQuestionRules;
    this.answer = answer;
  }

  public Integer getId() {
    return id;
  }

  public String getQuestion() {
//    Log.e(TAG, "getQuestion("+question+")");
    return question;
  }

  public String getAnswerType() {
//    Log.e(TAG, "getAnswerType("+answerType+")");
    return answerType;
  }

  public AnswerItemResult getAnswer() {
//    Log.e(TAG, "getAnswer("+answer+")");
    return answer;
  }

  public List<String> getAlternatives() {
//    Log.e(TAG, "getAlternatives("+alternatives+")");
    return alternatives;
  }

  public RangedAnswerResult getRange() {
    return range;
  }

  public boolean isHasNextQuestion() {
    return hasNextQuestion;
  }

  public List<Integer> getNextQuestionRules() {
//    Log.e(TAG, "getNextQuestionRules("+nextQuestionRules+")");
    return nextQuestionRules;
  }
}
