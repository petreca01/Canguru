package br.com.taqtile.android.app.domain.agenda;

import br.com.taqtile.android.app.data.agenda.AgendaRepository;
import br.com.taqtile.android.app.domain.agenda.mappers.AgendaItemResultToViewModelMapper;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class FinishAgendaItemUseCase extends BaseUseCase<AgendaItemViewModel, FinishAppointmentParams,
  AgendaRepository> {

  public FinishAgendaItemUseCase(AgendaRepository agendaRepository) {
    super(agendaRepository);
  }

  @Override
  public Observable<AgendaItemViewModel> execute(FinishAppointmentParams finishAppointmentParams) {
    return getRepository().finish(finishAppointmentParams)
      .map(AgendaItemResultToViewModelMapper::map);
  }
}
