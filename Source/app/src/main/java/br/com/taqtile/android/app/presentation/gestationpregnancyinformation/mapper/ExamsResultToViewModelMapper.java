package br.com.taqtile.android.app.presentation.gestationpregnancyinformation.mapper;

import br.com.taqtile.android.app.domain.exams.models.ExamsResult;
import br.com.taqtile.android.app.domain.exams.models.ExamsValuesResult;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model.ExamViewModel;

/**
 * Created by taqtile on 5/16/17.
 */

public class ExamsResultToViewModelMapper {

  public static ExamViewModel perform(ExamsResult examsResult) {
    ExamsValuesResult examsDetail = examsResult.getExamsValuesResult();
    return new ExamViewModel(examsResult.getDate(),
      examsDetail != null ? examsDetail.getDays() : "",
      examsDetail != null ? examsDetail.getWeeks() : "");
  }
}
