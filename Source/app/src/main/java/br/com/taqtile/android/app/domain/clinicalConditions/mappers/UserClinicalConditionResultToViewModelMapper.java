package br.com.taqtile.android.app.domain.clinicalConditions.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.clinicalConditions.models.UserClinicalConditionResult;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/14/17.
 */

public class UserClinicalConditionResultToViewModelMapper {
  public static List<UserClinicalConditionViewModel> perform(
    List<UserClinicalConditionResult> userClinicalConditionResultList) {
    return StreamSupport.stream(userClinicalConditionResultList)
      .map(UserClinicalConditionResultToViewModelMapper::perform)
      .collect(Collectors.toList());
  }

  public static UserClinicalConditionViewModel perform(
    UserClinicalConditionResult userClinicalConditionResult) {
    return new UserClinicalConditionViewModel(
      userClinicalConditionResult.getPregnancyConditionId(), userClinicalConditionResult.getName(),
      userClinicalConditionResult.getDate(), userClinicalConditionResult.getGestationAgeWeeks(),
      userClinicalConditionResult.getGestationAgeDays(), userClinicalConditionResult.getRisk());
  }
}
