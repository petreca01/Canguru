package br.com.taqtile.android.app.presentation.profilemenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by Renato on 4/18/17.
 */

public class ProfileMenuActivity extends TemplateBackActivity {

  private ProfileMenuFragment fragment;

  private final static String REQUEST_CODE = "requestCode";
  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override
  public ProfileMenuFragment getFragment() {
    ProfileMenuFragment fragment = (ProfileMenuFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      fragment = ProfileMenuFragment.newInstance();
    }
    this.fragment = fragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ProfileMenuPresenter profileMenuPresenter = new ProfileMenuPresenter();
    fragment.setPresenter(profileMenuPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return profileMenuPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, ProfileMenuActivity.class);
    context.startActivity(intent);
  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
