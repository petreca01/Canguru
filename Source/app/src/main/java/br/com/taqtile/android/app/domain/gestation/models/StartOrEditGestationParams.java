package br.com.taqtile.android.app.domain.gestation.models;

import static br.com.taqtile.android.app.support.GlobalConstants.PREGNANT;

/**
 * Created by taqtile on 19/04/17.
 */

public class StartOrEditGestationParams {
  private String dataForLastPeriod;
  private String babyName;
  private Integer gestationAgeDays;
  private Integer gestationAgeWeeks;
  private String ultrasoundDate;
  private String dolpUpdatedAt;

  public StartOrEditGestationParams(String dataForLastPeriod, String dolpUpdatedAt, String babyName, Integer gestationAgeDays,
                                    Integer gestationAgeWeeks, String ultrasoundDate) {
    this.dataForLastPeriod = dataForLastPeriod;
    this.babyName = babyName;
    this.gestationAgeDays = gestationAgeDays;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.ultrasoundDate = ultrasoundDate;
    this.dolpUpdatedAt = dolpUpdatedAt;
  }

  public StartOrEditGestationParams(String dataForLastPeriod, String babyName, Integer gestationAgeDays,
                                    Integer gestationAgeWeeks, String ultrasoundDate) {
    this.dataForLastPeriod = dataForLastPeriod;
    this.babyName = babyName;
    this.gestationAgeDays = gestationAgeDays;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.ultrasoundDate = ultrasoundDate;
  }

  public StartOrEditGestationParams(String babyName, Integer gestationAgeDays,
                                    Integer gestationAgeWeeks, String ultrasoundDate) {
    this.babyName = babyName;
    this.gestationAgeDays = gestationAgeDays;
    this.gestationAgeWeeks = gestationAgeWeeks;
    this.ultrasoundDate = ultrasoundDate;
  }

  public String getUserType() {
    return PREGNANT;
  }

  public String getDataForLastPeriod() {
    return dataForLastPeriod;
  }

  public String getBabyName() {
    return babyName;
  }

  public Integer getGestationAgeDays() {
    return gestationAgeDays;
  }

  public Integer getGestationAgeWeeks() {
    return gestationAgeWeeks;
  }

  public String getUltrasoundDate() {
    return ultrasoundDate;
  }

  public String getDolpUpdatedAt() {
    return dolpUpdatedAt;
  }
}
