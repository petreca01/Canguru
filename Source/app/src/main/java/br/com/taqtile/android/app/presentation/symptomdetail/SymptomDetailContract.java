package br.com.taqtile.android.app.presentation.symptomdetail;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.SubSymptomViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 28/04/17.
 */

public interface SymptomDetailContract {

  interface Presenter extends BasePresenter {
    Observable<EmptyResult> reportSymptom(SubSymptomViewModel subSymptomViewModel);
    Observable<UserViewModel> fetchUserData();
    Observable<List<SymptomViewModel>> listSymptoms(Integer gestationWeeks);
  }

}
