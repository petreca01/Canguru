package br.com.taqtile.android.app.presentation.maternity.components;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/11/17.
 */

public class MaternityHeaderCell extends LinearLayout {

  public interface Listener {
    void onFilterSubmitted(String text);
    void onChangeCityClicked();
  }

  @BindView(R.id.component_maternity_header_cell_filter)
  GenericLabelEditTextCaption filterByText;
  @BindView(R.id.component_maternity_header_cell_city)
  SectionHeader cityCustomTextView;

  Listener listener;

  public MaternityHeaderCell(Context context) {
    super(context);
    init(context);
  }

  public MaternityHeaderCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public MaternityHeaderCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_maternity_header_cell, this, true);
    ButterKnife.bind(this, v);
    filterByText.setKeyListener(() -> {
      if (listener != null) {
        listener.onFilterSubmitted(filterByText.getText());
      }
    });
    cityCustomTextView.setSectionHeaderListener(() -> listener.onChangeCityClicked());
    filterByText.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  public void setCityName(String cityName) {
    this.cityCustomTextView.setText(cityName);
  }

  public void resetFilter() {
    this.filterByText.setText("");
  }

}
