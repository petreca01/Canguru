package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.account.remote.models.request.EditUserRemoteRequest;
import br.com.taqtile.android.app.domain.account.models.EditUserPasswordParams;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditUserPasswordParamsToEditUserDataRemoteResquestMapper {

  public static EditUserRemoteRequest perform(EditUserPasswordParams editUserPasswordParams) {
    return new EditUserRemoteRequest(editUserPasswordParams.getPassword());
  }
}
