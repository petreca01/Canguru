package br.com.taqtile.android.app.domain.maternity;

import java.util.List;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.maternity.mappers.MaternityCommentResultToMaternityCommentViewModelMapper;
import br.com.taqtile.android.app.domain.maternity.models.DeleteCommentParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import rx.Observable;

/**
 * Created by taqtile on 5/24/17.
 */

public class DeleteMaternityCommentUseCase extends BaseUseCase<List<MaternityCommentViewModel>,
  DeleteCommentParams, MaternityRepository> {

  public DeleteMaternityCommentUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<List<MaternityCommentViewModel>> execute(
    DeleteCommentParams deleteCommentParams) {
    return getRepository().deleteComment(deleteCommentParams).map(MaternityCommentResultToMaternityCommentViewModelMapper::map);
  }
}
