package br.com.taqtile.android.app.data.channel.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/20/17.
 */

public class ChannelReplyRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("channel_post_id")
    private Integer channelPostId;

    @SerializedName("usuario_id")
    private Integer usuarioId;

    @SerializedName("reply")
    private String reply;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("username")
    private String username;

    @SerializedName("user_picture")
    private String userPicture;

    @SerializedName("likes")
    private Integer likes;

    @SerializedName("liked")
    private Boolean liked;

    public Integer getId() {
        return id;
    }

    public Integer getChannelPostId() {
        return channelPostId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public String getReply() {
        return reply;
    }

    public Boolean getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public Integer getLikes() {
        return likes;
    }

    public Boolean getLiked() {
        return liked;
    }
}
