package br.com.taqtile.android.app.presentation.maternitycitysearch;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.cells.TextCell;
import br.com.taqtile.android.app.presentation.maternitycitysearch.components.CityCell;
import br.com.taqtile.android.app.presentation.maternitycitysearch.viewholders.CityCellViewHolder;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternitySearchCitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<String> cities;
  private CityCell.CityCellListener cityCellListener;

  public MaternitySearchCitiesAdapter(List<String> cities, CityCell.CityCellListener cityCellListener) {
    this.cities = cities;
    this.cityCellListener = cityCellListener;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new CityCellViewHolder(new CityCell(parent.getContext(), cityCellListener));
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    ((CityCellViewHolder) holder).setCity(cities.get(position));
  }

  @Override
  public int getItemCount() {
    return cities.size();
  }
}
