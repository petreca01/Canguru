package br.com.taqtile.android.app.data.clinicalcondition.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ClinicalConditionItemRemoteResponse;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionItemResult;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ClinicalConditionResult;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/10/17.
 */

public class ClinicalConditionItemRemoteResponseToResultMapper {
  public static List<ClinicalConditionResult> perform(List<ClinicalConditionItemRemoteResponse> response) {
    return StreamSupport.stream(response)
      .filter(Objects::nonNull)
      .map(ClinicalConditionItemRemoteResponseToResultMapper::map)
      .collect(Collectors.toList());
  }

  private static ClinicalConditionResult map(
    ClinicalConditionItemRemoteResponse clinicalConditionItemRemoteResponse) {
    return new ClinicalConditionResult(
      ClinicalConditionItemRemoteResponseToResultMapper
        .mapFromDataSource(clinicalConditionItemRemoteResponse),
      ClinicalConditionItemRemoteResponseToResultMapper
        .mapFromDataSource(clinicalConditionItemRemoteResponse.getChilds()));
  }

  private static List<ClinicalConditionItemResult> mapFromDataSource(
    List<ClinicalConditionItemRemoteResponse> response) {
    return StreamSupport.stream(response)
      .filter(Objects::nonNull)
      .map(ClinicalConditionItemRemoteResponseToResultMapper::mapFromDataSource)
      .collect(Collectors.toList());
  }

  private static ClinicalConditionItemResult mapFromDataSource(
    ClinicalConditionItemRemoteResponse clinicalConditionItemRemoteResponse) {
    return new ClinicalConditionItemResult(
      clinicalConditionItemRemoteResponse.getId(),
      clinicalConditionItemRemoteResponse.getParentId(),
      clinicalConditionItemRemoteResponse.getName(),
      clinicalConditionItemRemoteResponse.getLogic(),
      clinicalConditionItemRemoteResponse.getLogicTrigger(),
      clinicalConditionItemRemoteResponse.getRisk(),
      clinicalConditionItemRemoteResponse.getContent());
  }
}
