package br.com.taqtile.android.app.presentation.personalriskform;

import android.support.v4.util.Pair;

import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by Renato on 4/19/17.
 */

public interface PersonalRiskFormContract {

  interface Presenter extends BasePresenter {
    Observable<RiskAnalysisQuestionResult> fetchFirstRiskAnalysisQuestionnaire(
      RiskAnalysisQuestionGroupResult params);

    Observable<RiskAnalysisQuestionResult> saveAnswers(Pair<AnswerParams,
      RiskAnalysisQuestionGroupResult> params);
  }

}
