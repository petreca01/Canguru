package br.com.taqtile.android.app.data.questionnaires.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class RangedAnswerRemoteResponse {
    @SerializedName("min")
    private Integer min;

    @SerializedName("max")
    private Integer max;

    public Integer getMin() {
        return min;
    }

    public Integer getMax() {
        return max;
    }
}
