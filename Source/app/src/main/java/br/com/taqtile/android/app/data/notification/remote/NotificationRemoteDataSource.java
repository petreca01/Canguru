package br.com.taqtile.android.app.data.notification.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.notification.remote.models.request.ListNotificationsRemoteRequest;
import br.com.taqtile.android.app.data.notification.remote.models.request.SetVisualizedNotificationRemoteRequest;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationCountRemoteResponse;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class NotificationRemoteDataSource extends BaseRemoteDataSource {
  private Observable<NotificationServices> services;

  public NotificationRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(NotificationServices.class);
  }

  public Observable<BaseRemoteResponse<NotificationCountRemoteResponse>> count() {
    return performRequest(services.flatMap(NotificationServices::count));
  }

  public Observable<BaseRemoteResponse<List<NotificationItemRemoteResponse>>> listAfterId(
    ListNotificationsRemoteRequest listRequestModel) {
    return performRequest(services.flatMap(services ->
      services.list(listRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<NotificationItemRemoteResponse>>> listAll() {
    return performRequest(services.flatMap(NotificationServices::list));
  }

  public Observable<BaseRemoteResponse<Boolean>> setVisualized(
    SetVisualizedNotificationRemoteRequest setVisualizedRequestModel) {
    return performRequest(services.flatMap(services ->
        services.setVisualized(setVisualizedRequestModel.getFormData())));
  }
}
