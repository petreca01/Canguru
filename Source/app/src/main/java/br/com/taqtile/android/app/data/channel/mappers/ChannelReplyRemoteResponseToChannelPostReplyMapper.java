package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelReplyRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReply;
import java.util.List;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelReplyRemoteResponseToChannelPostReplyMapper {

  public static List<ChannelPostReply> perform(List<ChannelReplyRemoteResponse> replies) {
    return StreamSupport.stream(replies)
      .map(ChannelReplyRemoteResponseToChannelPostReplyMapper::perform)
      .collect(Collectors.toList());
  }

  public static ChannelPostReply perform(ChannelReplyRemoteResponse channelReplyRemoteResponse) {
    ChannelPostReply channelPostReply = new ChannelPostReply();

    channelPostReply.setId(channelReplyRemoteResponse.getId());
    channelPostReply.setActive(channelReplyRemoteResponse.getActive());
    channelPostReply.setChannelPostId(channelReplyRemoteResponse.getChannelPostId());
    channelPostReply.setUserId(channelReplyRemoteResponse.getUsuarioId());
    channelPostReply.setReply(channelReplyRemoteResponse.getReply());
    channelPostReply.setCreatedAt(channelReplyRemoteResponse.getCreatedAt());
    channelPostReply.setUpdatedAt(channelReplyRemoteResponse.getUpdatedAt());
    channelPostReply.setUsername(channelReplyRemoteResponse.getUsername());
    channelPostReply.setUserPicture(channelReplyRemoteResponse.getUserPicture());
    channelPostReply.setLiked(channelReplyRemoteResponse.getLiked());
    channelPostReply.setLikes(channelReplyRemoteResponse.getLikes());

    return channelPostReply;
  }
}
