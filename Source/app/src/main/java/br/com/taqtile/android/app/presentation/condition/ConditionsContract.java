package br.com.taqtile.android.app.presentation.condition;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/9/17.
 */

public interface ConditionsContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> fetchUserData();

    Observable<List<ListingsViewModel>> listConditionsUseCase(Integer gestationWeeks);

  }
}
