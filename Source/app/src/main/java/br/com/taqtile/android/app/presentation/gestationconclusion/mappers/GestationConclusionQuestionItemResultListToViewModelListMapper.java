package br.com.taqtile.android.app.presentation.gestationconclusion.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionQuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.GestationConclusionQuestionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.presentation.questionnaires.mappers.QuestionItemResultToQuestionViewModelMapper;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 7/11/17.
 */

public class GestationConclusionQuestionItemResultListToViewModelListMapper {
  public static List<GestationConclusionQuestionViewModel> perform(List<GestationConclusionQuestionItemResult> questions) throws DomainError {

    return StreamSupport.stream(questions)
      .filter(Objects::nonNull)
      .map(item -> {
        try {
          return GestationConclusionQuestionItemResultListToViewModelListMapper.map(item ,
            QuestionItemResultToQuestionViewModelMapper
            .perform(new QuestionItemResult(item.getId(), item.getQuestion(), "",
              item.getAnswerType(), item.getAlternatives(), item.getRange(), false, null, null, null))
          );
        } catch (DomainError e) {
          throw new RuntimeException(e);
        }
      })
      .collect(Collectors.toList());

  }

  private static GestationConclusionQuestionViewModel map(GestationConclusionQuestionItemResult item,
                                                          QuestionViewModel questionViewModel) {
    return new GestationConclusionQuestionViewModel(questionViewModel.getQuestionId(),
      questionViewModel.getType(), questionViewModel.getQuestion(), questionViewModel.getAnswer(),
      questionViewModel.getAlternatives(), questionViewModel.getAnswers(),
      item.isHasNextQuestion(), item.getNextQuestionRules());
  }
}
