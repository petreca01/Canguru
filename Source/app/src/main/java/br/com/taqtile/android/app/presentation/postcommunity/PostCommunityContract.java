package br.com.taqtile.android.app.presentation.postcommunity;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 11/04/17.
 */

public interface PostCommunityContract {

  interface Presenter extends BasePresenter {
    Observable<EmptyResult> post(String topic, String message);
    Observable<List<SearchListingViewModel>> getContentSuggestion(String search, boolean reload);
  }
}
