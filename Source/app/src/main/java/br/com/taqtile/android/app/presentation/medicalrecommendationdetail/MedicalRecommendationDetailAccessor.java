package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import android.os.Bundle;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.app.presentation.common.BundleAccessor;

/**
 * Created by taqtile on 09/05/17.
 */

public class MedicalRecommendationDetailAccessor extends BundleAccessor {

  public MedicalRecommendationDetailAccessor(Bundle bundle) {
    super(bundle);
  }

  public MedicalRecommendationViewModel getMedicalRecommendationViewModel() {
    if (bundle == null) {
      return null;
    }
    return bundle.getParcelable(MedicalRecommendationDetailBundleConstants.MEDICAL_RECOMMENDATION_KEY);
  }
}
