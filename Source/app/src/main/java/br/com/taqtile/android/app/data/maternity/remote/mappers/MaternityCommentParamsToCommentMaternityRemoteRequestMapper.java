package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.maternity.remote.models.request.CommentMaternityRemoteRequest;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityCommentParamsToCommentMaternityRemoteRequestMapper {

  public static CommentMaternityRemoteRequest perform(MaternityCommentParams maternityCommentParams) {
    CommentMaternityRemoteRequest commentMaternityRemoteRequest = new CommentMaternityRemoteRequest();

    commentMaternityRemoteRequest.setComment(maternityCommentParams.getComment());

    return commentMaternityRemoteRequest;
  }
}
