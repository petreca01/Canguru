package br.com.taqtile.android.app.presentation.maternitycitysearch;

/**
 * Created by taqtile on 5/12/17.
 */


  import android.content.Context;
  import android.content.Intent;
  import android.support.v4.app.Fragment;

  import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
  import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
  import br.com.taqtile.android.app.presentation.main.MainActivity;
  import br.com.taqtile.android.app.presentation.maternity.MaternityFragment;
  import br.com.taqtile.android.app.support.NavigationHelper;
  import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

  import static br.com.taqtile.android.app.support.GlobalConstants.MATERNITY_CITY_SEARCH_REQUEST_CODE;

public class MaternityCitySearchActivity extends TemplateBackActivity{

  private  MaternityCitySearchFragment fragment;
  private  MaternityCitySearchPresenter presenter;

  @Override
  public Fragment getFragment() {
    MaternityCitySearchFragment fragment = (MaternityCitySearchFragment)
      getSupportFragmentManager().findFragmentById(this.getFragmentContainerId());
    if (fragment == null) {
      fragment = MaternityCitySearchFragment.newInstance();
    }
    this.fragment = fragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(getMaternityCitySearchPresenter());
    return presenter;
  }

  private  MaternityCitySearchPresenter getMaternityCitySearchPresenter() {
    if (presenter == null) {
      presenter = new MaternityCitySearchPresenter();
    }
    return presenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {

    Intent intent = new Intent(context, MaternityCitySearchActivity.class);
    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        MATERNITY_CITY_SEARCH_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        MATERNITY_CITY_SEARCH_REQUEST_CODE);
    }
  }
}
