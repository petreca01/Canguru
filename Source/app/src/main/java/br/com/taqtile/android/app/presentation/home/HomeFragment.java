package br.com.taqtile.android.app.presentation.home;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.adapters.SocialFeedPaginatedAdapter;
import br.com.taqtile.android.app.listings.cells.ChannelCard;
import br.com.taqtile.android.app.listings.cells.CommunityCard;
import br.com.taqtile.android.app.listings.cells.HomeHeaderCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.misc.general.FABMenu;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.VerticalSpaceItemDecoration;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.PresenterError;
import br.com.taqtile.android.app.presentation.common.TemplateBottomNavActivity;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.design.FABHelper;
import br.com.taqtile.android.utils.animations.RotationEffectHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.Optional;
import java8.util.stream.StreamSupport;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.listings.Constants.CHANNEL_CELL;
import static br.com.taqtile.android.app.listings.Constants.COMMUNITY_CELL;
import static br.com.taqtile.android.app.presentation.communitypostdetail.CommunityPostDetailFragment.DELETE_POST;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_POST_ID_KEY;

/**
 * Created by taqtile on 7/6/16.
 */

public class HomeFragment extends AppBaseFragment implements MenuItem.OnMenuItemClickListener,
  ChannelCard.ChannelCardListener, CommunityCard.CommunityCardListener, FABMenu.Listener, FABHelper.FABHelperListener {

  @BindView(R.id.fragment_home_recycler_view)
  RecyclerView socialFeedList;

  @BindView(R.id.fragment_home)
  HomeHeaderCell headerCell;

  @BindView(R.id.fragment_home_loading)
  ProgressBarLoading loadingLayout;

  @BindView(R.id.fragment_home_swipe_layout)
  SwipeRefreshLayout swipeRefreshLayout;

  private HomeContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  private SocialFeedPaginatedAdapter socialFeedAdapter;
  private ArrayList<SocialFeedViewModel> socialFeedViewModels;
  private LinearLayoutManager linearLayoutManager;

  private RecyclerView.OnScrollListener onScrollListener;

  private boolean loadMoreTriggered;

  private boolean isShowingOverlay;
  private FABMenu fabMenu;
  private FloatingActionButton fab;
  private final static int FAB_ROTATION = 45;
  private RotationEffectHelper rotationEffectHelper;
  private boolean resetTimeline;
  private Subscription refreshDataSubscription;
  private Subscription loadListSubscription;
  private Subscription updateProfilePictureSubscription;
  private Subscription loginSubscription;
  private Bundle bundle;

  public static HomeFragment newInstance() {
    return new HomeFragment();
  }

  public HomeFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_home, container, false);

    //This has to be called so that the fragment knows that a menu is going to be inflated
    //Because of an unknown reason, this only happens in the homefragment
    setHasOptionsMenu(true);
    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupRecyclerView();
    setupListeners();
    setupSwipeRefreshLayout();
    setupFAB();
    setupFABOverlay();
    resetTimeline = true;
  }

  private void setupRecyclerView() {
    socialFeedViewModels = new ArrayList<>();
    socialFeedAdapter = new SocialFeedPaginatedAdapter(getContext(), socialFeedViewModels, this, this);

    linearLayoutManager = new LinearLayoutManager(getContext());
    socialFeedList.addItemDecoration(
      new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.margin_medium)));
    socialFeedList.setAdapter(socialFeedAdapter);
    socialFeedList.setLayoutManager(linearLayoutManager);
  }

  private void setupSwipeRefreshLayout() {
    swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.color_primary));
  }

  private void setupListeners() {
    headerCell.setListener(this::onHomeHeaderCellClicked);
    swipeRefreshLayout.setOnRefreshListener(this::onSwipeRefresh);
  }

  private void setupFABOverlay() {
    isShowingOverlay = false;
  }

  private void setupFAB() {
    fabMenu = ((TemplateBottomNavActivity) getActivity()).getFABMenu();
    fab = ((TemplateBottomNavActivity) getActivity()).getFAB();
    fabMenu.setListener(this);
    getFABHelper().setFABListener(this);
    getFABHelper().showFAB();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_home_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchCards(true);
    hidePlaceholder();
    showFAB();
  }

  private void onHomeHeaderCellClicked() {
    navigationManager.showPostCommunity(this::returningFromPostCreation);
  }

  private void returningFromPostCreation(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      resetTimeline = true;
      fetchCards(true);
    }
  }

  public void onSwipeRefresh(){
    resetTimeline = true;
    fetchCards(true);
  }

  private void fetchCards(boolean reload) {
    fetchCards(reload, false);
  }
  private void fetchCards(boolean reload, boolean fromDetail) {
    if (reload) {
      showLoading();
    }
    refreshDataSubscription = presenter.fetchCards(reload)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        result -> {
          onFetchCardsSuccessful(result, reload, fromDetail);
        },
        throwable ->
          onFetchCardsFailure(throwable, reload));
    compositeSubscription.add(refreshDataSubscription);
  }

  private void onFetchCardsSuccessful(List<SocialFeedViewModel> result, boolean reload, boolean fromDetail) {

    if(reload){
      reloadTimelineSuccessful(result, fromDetail);
    }else{
      nextPageTimelineSuccessful(result);
    }
  }

  private void onFetchCardsFailure(Throwable throwable, boolean reload) {
    if(reload){
      // show placeholder
      handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
      hideFAB();
      hideLoading();
      showPlaceholder();
    }else {
      // show snackbar
      loadMoreTriggered = false;
      socialFeedAdapter.setFooterButtonVisible(false);
      socialFeedAdapter.notifyDataSetChanged();
      showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
    }
  }

  private void reloadTimelineSuccessful(List<SocialFeedViewModel> result, boolean fromDetail){
    resetSocialFeedViewModels();
    socialFeedViewModels.addAll(result);
    socialFeedAdapter.notifyDataSetChanged();
    if (fromDetail){ returnFromDetail();}
    else if (resetTimeline){
      resetTimeline = false;
      socialFeedList.scrollToPosition(0);
    }
    hideLoading();
  }

  private void nextPageTimelineSuccessful(List<SocialFeedViewModel> result){
    socialFeedViewModels.addAll(result);
    socialFeedAdapter.notifyDataSetChanged();
    loadMoreTriggered = false;
    if (result.isEmpty()) {
      socialFeedAdapter.setFooterButtonVisible(false);
    } else {
      socialFeedAdapter.setFooterButtonVisible(true);
    }
  }

  private void returnFromDetail(){
    if (this.bundle != null){
      if (this.bundle.getInt(POST_DETAIL_ACTION_KEY, -1) == (DELETE_POST)) {
        showSnackbar(getString(R.string.fragment_home_delete_post_success_message), Snackbar.LENGTH_LONG);
        socialFeedList.scrollToPosition(0);
      } else {
        int postId = this.bundle.getInt(POST_DETAIL_ACTION_POST_ID_KEY);
        Optional<SocialFeedViewModel> post = StreamSupport.stream(socialFeedViewModels)
          .filter(item -> item.getPostId().equals(postId))
          .findFirst();
        if (post != null && post.isPresent()) {
          int postIndex = socialFeedViewModels.indexOf(post.get());
          socialFeedList.scrollToPosition(postIndex);
        }
      }
    }
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_profile, view -> navigationManager.showProfileMenu());
      customToolbar.inflateOptionsMenu(R.menu.menu_home, this);
      customToolbar.hideTitle();
      ViewCompat.setElevation(getToolbar(), 0);
    }
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(HomeContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public boolean onMenuItemClick(MenuItem menuItem) {
    if (menuItem.getItemId() == R.id.home_channel) {
      navigationManager.showChannelListUI();
    } else if (menuItem.getItemId() == R.id.home_search) {
      navigationManager.showSearchUI();
    }
    return false;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    setupView();
  }

  private void setupView() {
    setupToolbar();
    setupRecyclerviewListener();
  }

  @Override
  public void onProfileRegionClick(Integer profileId, Integer postId, int type) {
    if (type == CHANNEL_CELL) {
      navigationManager.showChannelDetailUI(profileId, this::returningFromPostDetail);
    } else if (type == COMMUNITY_CELL) {
      navigationManager.showPublicProfileUI(profileId, this::returningFromPostDetail);
    }
  }

  @Override
  public void onCardClick(Integer id, int type) {
    if (type == CHANNEL_CELL) {
      navigationManager.showChannelPostDetailUI(this::returningFromPostDetail, id, false);
    } else if (type == COMMUNITY_CELL) {
      navigationManager.showCommunityPostDetailUI(this::returningFromPostDetail, id, false);
    }
  }

  @Override
  public void onCommentsCounterClick(Integer id, int type) {
    if (type == CHANNEL_CELL) {
      navigationManager.showChannelPostDetailUI(this::returningFromPostDetail, id, false);
    } else if (type == COMMUNITY_CELL) {
      navigationManager.showCommunityPostDetailUI(this::returningFromPostDetail, id, false);
    }
  }

  @Override
  public void onLikeClick(boolean isCounterActivated, Integer postId, int postType, CounterWithDrawable counterWithDrawable) {

    Subscription likeChannelPostSubscription = likePostObservable(postType, postId, isCounterActivated)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onItemLikedSuccess,
        error -> this.onItemLikedFailure(error, counterWithDrawable, isCounterActivated)
      );
    compositeSubscription.add(likeChannelPostSubscription);

  }

  private Observable<List<ListingsViewModel>> likePostObservable (int postType, Integer postId, boolean alreadyLiked) {

    if (postType == CHANNEL_CELL) {
      return presenter.likeChannelPost(postId, alreadyLiked);
    } else { // (typePost == COMMUNITY_CELL) {
      return presenter.likeCommunityPost(postId, alreadyLiked);
    }
  }

  protected void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels) {
//    no action is required
  }

  protected void onItemLikedFailure(Throwable throwable, CounterWithDrawable counterWithDrawable, boolean alreadyLiked) {
    counterWithDrawable. changeDrawable();
    showSnackbar(throwable.getLocalizedMessage(), Snackbar.LENGTH_SHORT);
  }

  @Override
  public void onOverflowMenuItemSelected(String item) {
    //nothing
  }

  @Override
  public void onBodyClick(Integer id, int type) {
    if (type == CHANNEL_CELL) {
      navigationManager.showChannelPostDetailUI(this::returningFromPostDetail, id, false);
    } else if (type == COMMUNITY_CELL) {
      navigationManager.showCommunityPostDetailUI(this::returningFromPostDetail, id, false);
    }
  }

  private void returningFromPostDetail(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      this.bundle = bundle;
      fetchCards(true, true);
    }
  }

  public void setupRecyclerviewListener() {
    loadMoreTriggered = false;
    onScrollListener = new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (hasRecyclerViewReachedTheEnd() && !loadMoreTriggered) {
          loadMoreTriggered = true;
          fetchCardsOnScroll();
        }
      }
    };
    socialFeedList.addOnScrollListener(onScrollListener);
  }

  private boolean hasRecyclerViewReachedTheEnd() {
    int visibleItemCount = linearLayoutManager.getChildCount();
    int totalItemCount = linearLayoutManager.getItemCount();
    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
    return pastVisibleItems + visibleItemCount >= totalItemCount;
  }

  private void fetchCardsOnScroll() {
    fetchCards(false);
  }

  @Override
  public void showLoading() {
    socialFeedList.setVisibility(View.GONE);
    FadeEffectHelper.fadeInView(loadingLayout);
  }

  @Override
  public void hideLoading() {
    FadeEffectHelper.fadeInView(socialFeedList);
    FadeEffectHelper.fadeOutView(loadingLayout);
    swipeRefreshLayout.setRefreshing(false);
  }

  @Override
  public void onCreatePostButtonClick() {
    navigationManager.showPostCommunity(this::returningFromPostCreation);
  }

  @Override
  public void onAddSymptomButtonClick() {
    navigationManager.showSymptomGuideUI();
  }

  @Override
  public void onScheduleAppointmentClick() {
    navigationManager.showAppointmentCreateUI(null);
  }

  @Override
  public void onFABClick() {
    fabMenu.translateButton();
    if (isShowingOverlay) {
      hideFABMenu();
    } else {
      showFABMenu();
    }
  }

  private void hideFABMenu() {
    ((TemplateBottomNavActivity) getActivity()).hideFABOverlay();
    ((TemplateBottomNavActivity) getActivity()).getFABOverlay().setClickable(false);
    RotationEffectHelper.rotateView(fab, 0, -FAB_ROTATION);
    isShowingOverlay = false;
  }

  private void showFABMenu() {
    ((TemplateBottomNavActivity) getActivity()).showFABOverlay();
    ((TemplateBottomNavActivity) getActivity()).getFABOverlay().setClickable(true);
    RotationEffectHelper.rotateView(fab, -FAB_ROTATION, 0);
    isShowingOverlay = true;
  }

  @Override
  public void onFABShown() {

  }

  @Override
  public void onFABHidden() {

  }

  private void hideFAB() {
    ((TemplateBottomNavActivity) getActivity()).getFAB().setClickable(false);
    getFABHelper().hideFAB();
    hideFABMenu();
    fabMenu.translateButton();
  }

  private void showFAB() {
    ((TemplateBottomNavActivity) getActivity()).getFAB().setClickable(true);
    getFABHelper().showFAB();
  }

  private void hideToolbarMenu() {
    if (customToolbar != null) {
      customToolbar.hideOptionsMenu();
      customToolbar.hideLeftButton();
    }
  }

  @Override
  public void willDisappear() {
    super.unsubscribeAll();
    //It was necessary to call it here, or it would appear after changing to one of the
    //other bottom nav fragments
    hideFAB();
    hideToolbarMenu();
  }

  @Override
  public void onResume() {
    super.onResume();
    checkUserLogged();
    updateProfilePicture();
    if (resetTimeline || socialFeedViewModels.isEmpty()) {
      fetchCards(true);
    }
  }

  @Override
  public void willAppear() {
    //It was necessary to call it here, or it would never appear after changing to one of the
    //other bottom nav fragments
    super.willAppear();
    setupToolbar();
    showFAB();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_home),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void checkUserLogged(){
    loginSubscription = Toolbox.getInstance()
      .isLoggedIn()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        isLoggedIn -> {
          if (!isLoggedIn) {
            userNotLogged();
          }},
        this::onLoginFailure);
    compositeSubscription.add(loginSubscription);
  }

  public void userNotLogged(){
    resetTimeline = true;
    this.navigationManager.showInitialLoginUI();
    PresenterError error = new PresenterError();
    error.setErrorType(PresenterError.NOT_LOGGED_IN);
    hideFAB();
    hideLoading();
  }

  private void onLoginFailure(Throwable throwable) {
    userNotLogged();
  }

  private void updateProfilePicture() {
    if (updateProfilePictureSubscription != null) {
      updateProfilePictureSubscription.unsubscribe();
    }
    updateProfilePictureSubscription = fetchProfilePicture()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchProfilePictureSuccess,
        this::onFetchProfilePictureFailure);
    compositeSubscription.add(updateProfilePictureSubscription);
  }

  private Observable<EmptyResult> fetchProfilePicture() {
    return Toolbox.getInstance()
      .getUserProfilePictureURL()
      .map(profilePictureURL -> {
        headerCell.setImage(profilePictureURL);
        return new EmptyResult();
      });
  }

  private void onFetchProfilePictureSuccess(EmptyResult emptyResult) {
    // do nothing
  }
  private void onFetchProfilePictureFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  public void refreshDataAfterTutorial() {
    resetSocialFeedViewModels();
    fetchCards(true);
  }

  private void resetSocialFeedViewModels() {
    if (socialFeedViewModels != null) {
      socialFeedViewModels.clear();
    }
  }

}
