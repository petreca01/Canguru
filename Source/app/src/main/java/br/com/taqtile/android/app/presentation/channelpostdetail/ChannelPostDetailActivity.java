package br.com.taqtile.android.app.presentation.channelpostdetail;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.VIEW_POST_CODE;

/**
 * Created by taqtile on 12/04/17.
 */

public class ChannelPostDetailActivity extends TemplateBackActivity {

  private ChannelPostDetailFragment fragment;
  public final static String POST_ID_TAG = "postId";
  public final static String FROM_PROFILE_TAG = "formProfileTag";

  @Override
  public Fragment getFragment() {
    ChannelPostDetailFragment channelPostDetailFragment = (ChannelPostDetailFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (channelPostDetailFragment == null) {
      channelPostDetailFragment = ChannelPostDetailFragment.newInstance();
    }
    fragment = channelPostDetailFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ChannelPostDetailPresenter channelPostDetailPresenter = new ChannelPostDetailPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(channelPostDetailPresenter);
    fragment.setChannelPresenter(channelPostDetailPresenter);

    return channelPostDetailPresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener, int id, boolean fromProfile) {

    Intent intent = new Intent(context, ChannelPostDetailActivity.class);
    intent.putExtra(POST_ID_TAG, id);
    intent.putExtra(FROM_PROFILE_TAG, fromProfile);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener, VIEW_POST_CODE);
      ((MainActivity) context).startActivityForResult(intent, VIEW_POST_CODE);
    } else {
      context.startActivity(intent);
    }
  }

}
