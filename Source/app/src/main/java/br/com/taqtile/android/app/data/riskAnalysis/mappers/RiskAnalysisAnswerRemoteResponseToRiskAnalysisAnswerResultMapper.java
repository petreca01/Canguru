package br.com.taqtile.android.app.data.riskAnalysis.mappers;

import br.com.taqtile.android.app.data.common.mappers.UserRemoteResponseToUserResultMapper;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisAnswerRemoteResponse;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisAnswerResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionsResult;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class RiskAnalysisAnswerRemoteResponseToRiskAnalysisAnswerResultMapper {

  public static RiskAnalysisAnswerResult perform(RiskAnalysisAnswerRemoteResponse riskAnalysisAnswerRemoteResponse) {
    UserResult userResult = UserRemoteResponseToUserResultMapper
      .perform(riskAnalysisAnswerRemoteResponse.getUser());

    RiskAnalysisQuestionsResult riskAnalysisQuestionsResult =
      RiskAnalysisQuestionsRemoteResponseToRiskAnalysisQuestionsResultMapper
        .perform(riskAnalysisAnswerRemoteResponse.getQuestions());

    return new RiskAnalysisAnswerResult(userResult, riskAnalysisQuestionsResult);
  }
}
