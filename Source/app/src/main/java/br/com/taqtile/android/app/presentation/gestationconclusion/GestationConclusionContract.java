package br.com.taqtile.android.app.presentation.gestationconclusion;

import java.util.List;

import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionViewModel;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 07/07/17.
 */

public interface GestationConclusionContract {
  interface Presenter extends BasePresenter {

    Observable<GestationConclusionViewModel> fetchGestationConclusionQuestionnaire();

    Observable<GestationConclusionViewModel> sendGestationConclusionQuestionnaireEmail();

    Observable<GestationConclusionViewModel> saveAnswers(List<AnswerParams> answerParams, boolean finishPregnancy);

  }
}
