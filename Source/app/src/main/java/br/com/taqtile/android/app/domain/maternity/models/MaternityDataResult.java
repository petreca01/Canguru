package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityDataResult {

  private Integer id;

  private Integer channelId;

  private String state;

  private String city;

  private String name;

  private String address;

  private String website;

  private String phone;

  private String logo;

  private String image;

  private String about;

  private String birthSchool;

  private String visitRules;

  private String availableMidwife;

  private String bathBirth;

  private String pppRoom;

  private String crowdRoom;

  private String accreditations;

  private Float rate;

  private Integer totalRates;

  private String showImageOnTop;

  private String showMap;

  private String currentBirthsSource;

  private Integer currentTotalBirths;

  private Float currentPercentNormalBirth;

  private Float currentPercentCsections;

  private boolean highlight;

  private boolean active;

  private String createdAt;

  private String updatedAt;

  private Integer oneRates;

  private Integer twoRates;

  private Integer fourRates;

  private Integer threeRates;

  private Integer fiveRates;

  private Integer myRate;

  private boolean isFollowing;

  public MaternityDataResult(Integer id, Integer channelId, String state, String city, String name,
    String about, String address, String website, String phone, String logo, String image,
    String birthSchool, String visitRules, String availableMidwife, String bathBirth,
    String pppRoom, String crowdRoom, String accreditations, Float rate, Integer totalRates,
    String showImageOnTop, String showMap, String currentBirthsSource, Integer currentTotalBirths,
    Float currentPercentNormalBirth, Float currentPercentCsections, boolean highlight,
    boolean active, String createdAt, String updatedAt, Integer oneRates, Integer twoRates,
    Integer fourRates, Integer threeRates, Integer fiveRates, Integer myRate, boolean isFollowing) {
    this.id = id;
    this.channelId = channelId;
    this.state = state;
    this.city = city;
    this.name = name;
    this.about = about;
    this.address = address;
    this.website = website;
    this.phone = phone;
    this.logo = logo;
    this.image = image;
    this.birthSchool = birthSchool;
    this.visitRules = visitRules;
    this.availableMidwife = availableMidwife;
    this.bathBirth = bathBirth;
    this.pppRoom = pppRoom;
    this.crowdRoom = crowdRoom;
    this.accreditations = accreditations;
    this.rate = rate;
    this.totalRates = totalRates;
    this.showImageOnTop = showImageOnTop;
    this.showMap = showMap;
    this.currentBirthsSource = currentBirthsSource;
    this.currentTotalBirths = currentTotalBirths;
    this.currentPercentNormalBirth = currentPercentNormalBirth;
    this.currentPercentCsections = currentPercentCsections;
    this.highlight = highlight;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.oneRates = oneRates;
    this.twoRates = twoRates;
    this.fourRates = fourRates;
    this.threeRates = threeRates;
    this.fiveRates = fiveRates;
    this.myRate = myRate;
    this.isFollowing = isFollowing;
  }

  public Integer getId() {
    return id;
  }

  public Integer getChannelId() {
    return channelId;
  }

  public String getState() {
    return state;
  }

  public String getCity() {
    return city;
  }

  public String getName() {
    return name;
  }

  public String getAbout() {
    return about;
  }

  public String getAddress() {
    return address;
  }

  public String getWebsite() {
    return website;
  }

  public String getPhone() {
    return phone;
  }

  public String getLogo() {
    return logo;
  }

  public String getImage() {
    return image;
  }

  public String getBirthSchool() {
    return birthSchool;
  }

  public String getVisitRules() {
    return visitRules;
  }

  public String getAvailableMidwife() {
    return availableMidwife;
  }

  public String getBathBirth() {
    return bathBirth;
  }

  public String getPppRoom() {
    return pppRoom;
  }

  public String getCrowdRoom() {
    return crowdRoom;
  }

  public String getAccreditations() {
    return accreditations;
  }

  public Float getRate() {
    return rate;
  }

  public Integer getTotalRates() {
    return totalRates;
  }

  public String getShowImageOnTop() {
    return showImageOnTop;
  }

  public String getShowMap() {
    return showMap;
  }

  public String getCurrentBirthsSource() {
    return currentBirthsSource;
  }

  public Integer getCurrentTotalBirths() {
    return currentTotalBirths;
  }

  public Float getCurrentPercentNormalBirth() {
    return currentPercentNormalBirth;
  }

  public Float getCurrentPercentCsections() {
    return currentPercentCsections;
  }

  public boolean isHighlight() {
    return highlight;
  }

  public boolean isActive() {
    return active;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public Integer getOneRates() {
    return oneRates;
  }

  public Integer getTwoRates() {
    return twoRates;
  }

  public Integer getFourRates() {
    return fourRates;
  }

  public Integer getThreeRates() {
    return threeRates;
  }

  public Integer getFiveRates() {
    return fiveRates;
  }

  public Integer getMyRate() {
    return myRate;
  }

  public boolean isFollowing() {
    return isFollowing;
  }
}
