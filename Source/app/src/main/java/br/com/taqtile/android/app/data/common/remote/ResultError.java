package br.com.taqtile.android.app.data.common.remote;

/**
 * Created by taqtile on 3/20/17.
 */

public class ResultError extends Throwable {
    public enum ErrorType {
        BadResponse,
        NoConnection,
        Unknown
    }

    private ErrorType errorType;

    private String message;

    public ResultError(ErrorType errorType, String message) {
        this.errorType = errorType;
        this.message = message;
    }

    public ResultError(Throwable throwable) {
        this.errorType = ErrorType.Unknown;
        this.message = throwable.getMessage();
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    @Override
    public String getMessage() {
        return message;
    }
}