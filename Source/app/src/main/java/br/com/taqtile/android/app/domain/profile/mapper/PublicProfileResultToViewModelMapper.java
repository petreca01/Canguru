package br.com.taqtile.android.app.domain.profile.mapper;

import br.com.taqtile.android.app.domain.profile.models.PublicProfileResult;
import br.com.taqtile.android.app.presentation.home.mappers.ForumPostToSocialFeedViewModelMapper;
import br.com.taqtile.android.app.presentation.profilepublic.viewmodel.PublicProfileViewModel;

/**
 * Created by taqtile on 4/27/17.
 */

public class PublicProfileResultToViewModelMapper {

  public static PublicProfileViewModel perform(PublicProfileResult publicProfileResult) {
    return new PublicProfileViewModel(
      ProfileDetailsResultToViewModelMapper.perform(publicProfileResult.getProfileDetailsResult()),
      ForumPostToSocialFeedViewModelMapper.perform(publicProfileResult.getPostResults()));
  }
}
