package br.com.taqtile.android.app.data.account.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class SignUpRemoteRequest extends BaseRemoteRequest {
  private String email;

  private String password;

  private String name;

  @RequestName("user_birthdate")
  private String birthDate;

  @RequestName("user_type") private String userType;

  @RequestName("last_period")
  private String lastPeriod;

  public SignUpRemoteRequest(String email, String password, String name, String userType) {
    this.email = email;
    this.password = password;
    this.name = name;
    this.userType = userType;
  }

  public SignUpRemoteRequest(String email, String password, String name, String userType, String lastPeriod) {

    this.email = email;
    this.password = password;
    this.name = name;
    this.userType = userType;
    this.lastPeriod = lastPeriod;
  }

  public SignUpRemoteRequest addBirthDate(String birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getName() {
    return name;
  }

  public String getUserType() {
    return userType;
  }

  public String getLastPeriod() {
    return lastPeriod;
  }

  public String getBirthDate() {
    return birthDate;
  }
}
