package br.com.taqtile.android.app.support.helpers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.location.Address;
import android.location.Location;
import android.location.Geocoder;
import android.os.Bundle;

import java.io.IOException;
import java.util.Locale;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by taqtile on 4/19/17.
 */

public class LocationHelper implements GoogleApiClient.ConnectionCallbacks,
  GoogleApiClient.OnConnectionFailedListener, LocationListener {

  private GoogleApiClient mGoogleApiClient;
  private LocationRequest mLocationRequest;
  private Location mLastLocation;

  private Fragment fragment;
  private Context context;

  private static final int REQUEST_PERMISSIONS_LOCATION = 1;

  private static String[] PERMISSIONS_REQUIRED = {
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.ACCESS_COARSE_LOCATION};

  private RequestLocationResponse requestLocationResponse;

  public interface RequestLocationResponse {
    void onRequestLocationResponse(@Nullable Location location);
  }

  public LocationHelper(Fragment fragment, RequestLocationResponse requestLocationResponse){
    this.fragment = fragment;
    this.context = fragment.getContext();
    this.requestLocationResponse = requestLocationResponse;
  }

  public void getCurrentLocation(){

    if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
      != PackageManager.PERMISSION_GRANTED) {
      fragment.requestPermissions(PERMISSIONS_REQUIRED, REQUEST_PERMISSIONS_LOCATION);
    } else {
      connectGoogleAPI();
    }
  }

  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
    switch (requestCode) {
      case REQUEST_PERMISSIONS_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
          && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          connectGoogleAPI();
        }else{
          sendLocation(null);
        }
      }
    }
  }

  private void connectGoogleAPI(){

      mGoogleApiClient = new GoogleApiClient.Builder(context)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();

      mGoogleApiClient.connect();
  }

//  method used for async call
  private void sendLocation(@Nullable Location location){

    try {
      if (mGoogleApiClient.isConnected()) {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
      }
    }
    catch (Exception ex) {}
    finally {
      this.requestLocationResponse.onRequestLocationResponse(location);
    }
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {

      mLocationRequest = LocationRequest.create()
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        .setNumUpdates(1);

      LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
        .addLocationRequest(mLocationRequest);

      PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
        .checkLocationSettings(mGoogleApiClient, builder.build());

      result.setResultCallback(result1 -> getLocation(result1, mLocationRequest));
  }

  private void getLocation(LocationSettingsResult result, LocationRequest locationRequest) {

    Status status = result.getStatus();
    switch (status.getStatusCode()) {

      case LocationSettingsStatusCodes.SUCCESS:
        try {
          LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }catch (SecurityException ex){
          sendLocation(null);
        }
        break;

      // the case LocationSettingsStatusCodes.RESOLUTION_REQUIRED and others:
      default:
        sendLocation(null);
        break;
    }
  }

  @Override
  public void onConnectionSuspended(int i) {

  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    sendLocation(null);
  }

  @Override
  public void onLocationChanged(Location location) {
    sendLocation(location);
  }

//  static methods
  public static Address getAddressByLatLng(Context context, double lat, double lng){

    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
    List<Address> addresses = null;

    try{
      addresses = geocoder.getFromLocation(lat,lng,1);
      if (addresses != null && addresses.size() > 0) {
        return addresses.get(0);
      } else{
        return new Address(new Locale(""));
      }
    } catch (IOException ioException) {
      return new Address(new Locale(""));
    }
  }

  public static Address getAddressByLocation(Context context, Location location){
    try{
      return getAddressByLatLng(context, location.getLatitude(), location.getLongitude());
    } catch (Exception ex) {
      return new Address(new Locale(""));
    }
  }
}
