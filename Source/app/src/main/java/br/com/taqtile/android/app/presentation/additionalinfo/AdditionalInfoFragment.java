package br.com.taqtile.android.app.presentation.additionalinfo;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.IconButtonWithLabel;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.listings.cells.FormDetailCell;
import br.com.taqtile.android.app.listings.cells.HealthcareWithPartnerCell;
import br.com.taqtile.android.app.listings.cells.HealthcareWithoutPartnerCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.LocationHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.ADDITIONAL_INFO_TAG;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoFragment extends AppBaseFragment implements IconButtonWithLabel.Listener, LocationHelper.RequestLocationResponse {

  @BindView(R.id.fragment_additional_info_layout)
  LinearLayout additionalInfoLayout;

  @BindView(R.id.fragment_additional_info_email)
  FormDetailCell email;

  @BindView(R.id.fragment_additional_info_telephone)
  FormDetailCell telephone;

  @BindView(R.id.fragment_additional_info_birth_date)
  FormDetailCell birthdate;

  @BindView(R.id.fragment_additional_info_cpf)
  FormDetailCell cpf;

  @BindView(R.id.fragment_additional_info_cep)
  FormDetailCell cep;

  @BindView(R.id.fragment_additional_info_partner_heath_operator_detail)
  HealthcareWithPartnerCell healthcareWithPartner;

  @BindView(R.id.fragment_additional_info_heath_operator_detail)
  HealthcareWithoutPartnerCell healthcareWithoutPartner;

  @BindView(R.id.fragment_additional_info_heath_operator_edit_detail)
  SectionHeader sectionHeader;

  @BindView(R.id.fragment_additional_info_scroll_view)
  ScrollView scrollView;

  @BindView(R.id.fragment_additional_info_location_agreement)
  IconButtonWithLabel locationAgreement;

  private AdditionalInfoContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;
  private AdditionalInfoUserViewModel additionalInfoUserData;
  private LocationHelper locationHelper;

  public static AdditionalInfoFragment newInstance() {
    return new AdditionalInfoFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_additional_info, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();

    boolean getUserLocation = true;
    fetchUserData(getUserLocation);
  }

  private void setupView(){
    setupListeners();
  }

  private void setupListeners(){
    setupLinkButton();
    setupLocationAgreementButton();
  }

  private void setupLocationAgreementButton(){
    locationAgreement.setListener(this);
    locationAgreement.setLabel(getString(R.string.fragment_additional_info_edit_location));
  }

  private void setupLinkButton() {
    sectionHeader.setSectionHeaderListener(() ->
      navigationManager.showEditAdditionalInfoUserData(additionalInfoUserData, this::returningFormEditUserData));
  }

  private void returningFormEditUserData(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      fetchUserData();
    }
  }

  private void fetchUserData() {
    boolean getUserLocation = false;
    fetchUserData(getUserLocation);
  }

  private void fetchUserData(boolean getUserLocation) {
    showLoading();
    Subscription userDataSubscription = presenter.fetchUserData()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        additionalInfoUserData ->
          onFetchUserDataSuccess(additionalInfoUserData, getUserLocation),
        this::onFetchUserDataFailure);
    compositeSubscription.add(userDataSubscription);
  }

  private void onFetchUserDataSuccess(AdditionalInfoUserViewModel additionalInfoUserData, Boolean getUserLocation) {
    this.additionalInfoUserData = additionalInfoUserData;
    showUserInformation();

    if (this.additionalInfoUserData.getZipCode().isEmpty() && getUserLocation) {
      getUserLocation();
    } else{
      hideLoading();
    }
  }

  private void onFetchUserDataFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void showUserInformation(){
    setHealthOperatorInformation();
    setupLocationAgreement();
    setBasicInformation();
  }

  private void setHealthOperatorInformation() {
    if (additionalInfoUserData.getOperatorId() != null && additionalInfoUserData.getOperatorName() != null &&
      !additionalInfoUserData.getOperatorName().isEmpty()) {
      if (additionalInfoUserData.isPartner()) {
        healthcareWithPartner.setHeader(additionalInfoUserData.getOperatorName());
        healthcareWithPartner.setImage(additionalInfoUserData.getHealthOperatorIcon());
        healthcareWithPartner.setVisibility(View.VISIBLE);
        healthcareWithoutPartner.setVisibility(View.GONE);
        healthcareWithPartner.setListener(this::navigateToHealthcareDetails);
      } else {
        healthcareWithoutPartner.setHeader(additionalInfoUserData.getOperatorName());
        healthcareWithoutPartner.setImage(additionalInfoUserData.getHealthOperatorIcon());
        healthcareWithoutPartner.setVisibility(View.VISIBLE);
        healthcareWithPartner.setVisibility(View.GONE);
        healthcareWithoutPartner.setListener(this::navigateToHealthcareDetails);
      }
    } else {
      healthcareWithoutPartner.setHeader(getString(R.string.fragment_additional_info_subsection_add_health_care));
      healthcareWithoutPartner.setImage(null);
      healthcareWithoutPartner.setVisibility(View.VISIBLE);
      healthcareWithPartner.setVisibility(View.GONE);

      healthcareWithoutPartner.setListener(() -> navigationManager.showSearchHealthOperatorsUI(null));
    }
  }

  private void setupLocationAgreement(){
    locationAgreement.setButtonActive(additionalInfoUserData.getLocationPrivacyAgreed());
  }

  private void setBasicInformation() {
    email.setupValue(this.additionalInfoUserData.getEmail());
    telephone.setupValue(setValue(this.additionalInfoUserData.getTelephone()));
    birthdate.setupValue(setValue(DateFormatterHelper
      .formatDateWithoutHours(this.additionalInfoUserData.getBirthDate())));
    cpf.setupValue(setValue(this.additionalInfoUserData.getCpf()));
    cep.setupValue(setValue(this.additionalInfoUserData.getZipCode()));
  }

  private void navigateToHealthcareDetails() {
    navigationManager
      .showHealthOperatorsDetailUI(new HealthOperatorViewModel(additionalInfoUserData.getOperatorId(),
        additionalInfoUserData.getOperatorName(), additionalInfoUserData.healthOperatorAbout,
        additionalInfoUserData.getHealthOperatorIcon(), additionalInfoUserData.isPartner(), additionalInfoUserData.getHealthOperatorDocumentType()), false,
        null);
  }

  public void setPresenter(@NonNull AdditionalInfoContract.Presenter presenter) {
    this.presenter = checkNotNull(presenter);
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_additional_info),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources().getString(R.string.fragment_additional_info_title), Gravity.LEFT);
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
    }
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    this.unsubscribeAll();
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchUserData();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void showLoading() {
    super.showLoading();
    hidePlaceholder();
    additionalInfoLayout.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    hidePlaceholder();
    additionalInfoLayout.setVisibility(View.VISIBLE);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_additional_info_placeholder;
  }

  @Override
  public void onActiveButtonClick() {
    locationAgreementClicked();
  }

  @Override
  public void onInactiveButtonClick() {
    locationAgreementClicked();
  }

  private void locationAgreementClicked() {
    toggleLocationAgreementButton();
    showButtonLoading();
    compositeSubscription.add(updateLocationAgreement());
  }

  private void toggleLocationAgreementButton(){
    locationAgreement.setButtonActive(!locationAgreement.isButtonActive());
  }

  private void showButtonLoading(){
    locationAgreement.setEnabled(false);
  }

  private void hideButtonLoading(){
    locationAgreement.setEnabled(true);
  }

  private Subscription updateLocationAgreement() {
    return presenter.updateLocation(new EditUserDataParams(locationAgreement.isButtonActive()))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onLocationAgreementUpdateSuccess, this::onLocationAgreementUpdateFailure);
  }

  private void onLocationAgreementUpdateSuccess(UserViewModel userViewModel) {
    hideButtonLoading();
  }

  private void onLocationAgreementUpdateFailure(Throwable throwable) {
    locationAgreement.setButtonActive(!locationAgreement.isButtonActive());
    hideButtonLoading();
  }

  private String setValue(String value) {
    return (value == null || value.isEmpty()) ? getString(R.string.fragment_additional_info_subsection_empty_field) :
      value;
  }

  private void getUserLocation(){
    locationHelper = new LocationHelper(this, this);
    locationHelper.getCurrentLocation();
    // the continuation this is in onRequestLocationResponse method, because depends on user response
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  @Override
  public void onRequestLocationResponse(@Nullable Location location) {

    if (location != null) {
      Address address = LocationHelper.getAddressByLocation(getContext(), location);
      if (address != null) {

        EditUserDataParams params = new EditUserDataParams(
          address.getPostalCode(), //zip code
          address.getSubAdminArea(), //city
          address.getAdminArea(), //state
          String.valueOf(address.getLatitude()), //lat
          String.valueOf(address.getLongitude()),//lng
          true); // show location on public profile

        Subscription saveUserLocation = presenter.editUserData(params)
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(
            this::saveUserLocationSuccessful,
            this::saveUserLocationFailure);
        compositeSubscription.add(saveUserLocation);

      } else {
        showErrorLocation();
      }
    } else {
      showErrorLocation();
    }
  }

  private void saveUserLocationSuccessful(UserViewModel userViewModel) {
    // update additionalInfoUserData object
    fetchUserData();
    hideLoading();
  }

  private void saveUserLocationFailure(Throwable throwable) {
    showSnackbar(getResources().getString(R.string.forms_save_location_error), Snackbar.LENGTH_LONG);
    hideLoading();
  }

  private void showErrorLocation(){
    int custom_duration = 1000 * 4; // 4 seconds
    showSnackbar(getResources().getString(R.string.forms_location_error), custom_duration);
    hideLoading();
  }
}
