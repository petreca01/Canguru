package br.com.taqtile.android.app.support;

/**
 * Created by taqtile on 3/6/17.
 */

public interface BuildConstants {

  String getBaseUrl();

  String getGoogleAPIUrl();

  String getGoogleAnalyticsId();

  String getSenderId();

}
