package br.com.taqtile.android.app.domain.forum;

import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.forum.models.CreatePostParams;
import br.com.taqtile.android.app.domain.forum.models.NewPostResult;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */


public class CreatePostUseCase extends BaseUseCase<NewPostResult, CreatePostParams, ForumRepository>{

  public CreatePostUseCase(ForumRepository forumRepository) {
    super(forumRepository);
  }

  @Override
  public Observable<NewPostResult> execute(CreatePostParams createPostParams) {
    return getRepository().createPost(createPostParams);
  }
}
