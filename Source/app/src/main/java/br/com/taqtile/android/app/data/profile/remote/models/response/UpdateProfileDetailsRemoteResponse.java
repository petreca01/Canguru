package br.com.taqtile.android.app.data.profile.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class UpdateProfileDetailsRemoteResponse {
  @SerializedName("id")
  private Integer id;

  @SerializedName("name")
  private String name;

  @SerializedName("baby_name")
  private String babyName;

  @SerializedName("about")
  private String about;

  @SerializedName("profile_picture")
  private String profilePicture;

  @SerializedName("cover_picture")
  private String coverPicture;

  @SerializedName("pregnancy_weeks")
  private Integer pregnancyWeeks;

  @SerializedName("pregnancy_days")
  private Integer pregnancyDays;

  @SerializedName("city")
  private String city;

  @SerializedName("state")
  private String state;

  @SerializedName("location_privacy_agreed")
  private Boolean locationPrivacyAgreed;

  @SerializedName("created_at")
  private String createdAt;

  @SerializedName("updated_at")
  private String updatedAt;

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getAbout() {
    return about;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public Integer getPregnancyWeeks() {
    return pregnancyWeeks;
  }

  public Integer getPregnancyDays() {
    return pregnancyDays;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public Boolean getLocationPrivacyAgreed() {
    return locationPrivacyAgreed;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }
}
