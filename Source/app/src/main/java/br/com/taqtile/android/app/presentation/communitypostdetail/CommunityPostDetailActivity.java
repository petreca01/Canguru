package br.com.taqtile.android.app.presentation.communitypostdetail;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity.FROM_PROFILE_TAG;
import static br.com.taqtile.android.app.presentation.channelpostdetail.ChannelPostDetailActivity.POST_ID_TAG;
import static br.com.taqtile.android.app.support.GlobalConstants.VIEW_POST_CODE;

/**
 * Created by taqtile on 19/04/17.
 */

public class CommunityPostDetailActivity extends TemplateBackActivity {
  private CommunityPostDetailFragment fragment;

  @Override
  public Fragment getFragment() {
    CommunityPostDetailFragment communityPostDetailFragment = (CommunityPostDetailFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (communityPostDetailFragment == null) {
      communityPostDetailFragment = CommunityPostDetailFragment.newInstance();
    }
    fragment = communityPostDetailFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    CommunityPostDetailPresenter communityPostDetailPresenter = new CommunityPostDetailPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(communityPostDetailPresenter);
    fragment.setCommunityPresenter(communityPostDetailPresenter);

    return communityPostDetailPresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener, Integer id,
                              boolean fromProfile) {
    Intent intent = new Intent(context, CommunityPostDetailActivity.class);
    intent.putExtra(POST_ID_TAG, id);
    intent.putExtra(FROM_PROFILE_TAG, fromProfile);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener, VIEW_POST_CODE);
      ((MainActivity) context).startActivityForResult(intent, VIEW_POST_CODE);
    } else {
      context.startActivity(intent);
    }

  }
}
