package br.com.taqtile.android.app.presentation.maternitydetail;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.DeleteMaternityCommentUseCase;
import br.com.taqtile.android.app.domain.maternity.GetMaternityDetailsUseCase;
import br.com.taqtile.android.app.domain.maternity.mappers.MaternityDetailsResultToMaternityDetailsViewModelMapper;
import br.com.taqtile.android.app.domain.maternity.models.DeleteCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityDetailsViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 09/05/17.
 */

public class MaternityDetailPresenter implements MaternityDetailContract.Presenter {

  private GetMaternityDetailsUseCase getMaternityDetailsUseCase;
  private DeleteMaternityCommentUseCase deleteMaternityCommentUseCase;

  public MaternityDetailPresenter() {
    this.getMaternityDetailsUseCase = Injection.provideGetMaternityDetailsUseCase();
    this.deleteMaternityCommentUseCase = Injection.provideDeleteMaternityUseCase();
  }

  @Override public void resume() {

  }

  @Override public void start() {

  }

  @Override
  public Observable<MaternityDetailsViewModel> getMaternityDetailsUseCase(MaternityParams maternityParams) {
    return getMaternityDetailsUseCase.execute(maternityParams)
      .map(MaternityDetailsResultToMaternityDetailsViewModelMapper::map);
  }

  @Override
  public Observable<List<MaternityCommentViewModel>> deleteMaternityCommentUseCase(DeleteCommentParams deleteCommentParams) {
    return deleteMaternityCommentUseCase.execute(deleteCommentParams);
  }
}
