package br.com.taqtile.android.app.domain.questionnaires.models;

import java.util.List;

/**
 * Created by taqtile on 07/04/17.
 */

public class AnswerQuestionnairesParams {
  private List<AnswerParams> answers;
  private boolean finishPregnancy;

  public AnswerQuestionnairesParams(List<AnswerParams> answers) {
    this.answers = answers;
    this.finishPregnancy = false;
  }

  public AnswerQuestionnairesParams(List<AnswerParams> answers, boolean finishPregnancy) {
    this.answers = answers;
    this.finishPregnancy = finishPregnancy;
  }

  public List<AnswerParams> getAnswers() {
    return answers;
  }
  public boolean getFinishPregnancy() {
    return finishPregnancy;
  }

}
