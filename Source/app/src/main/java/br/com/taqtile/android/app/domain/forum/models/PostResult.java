package br.com.taqtile.android.app.domain.forum.models;

import java.util.List;

/**
 * Created by taqtile on 3/21/17.
 */

public class PostResult {
    private Integer id;
    private Integer operatorId;
    private Integer userId;
    private String title;
    private String content;
    private Boolean active;
    private String createdAt;
    private String updatedAt;
    private String userType;
    private String username;
    private String userPicture;
    private Integer likes;
    private Integer countReplies;
    private Boolean liked;
    private List<CategoryItem> categories;

  public PostResult(Integer id, Integer operatorId, Integer userId, String title, String content,
                    Boolean active, String createdAt, String updatedAt, String userType,
                    String username, String userPicture, Integer likes, Integer countReplies,
                    Boolean liked, List<CategoryItem> categories) {
    this.id = id;
    this.operatorId = operatorId;
    this.userId = userId;
    this.title = title;
    this.content = content;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.userType = userType;
    this.username = username;
    this.userPicture = userPicture;
    this.likes = likes;
    this.countReplies = countReplies;
    this.liked = liked;
    this.categories = categories;
  }

  public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getCountReplies() {
        return countReplies;
    }

    public void setCountReplies(Integer countReplies) {
        this.countReplies = countReplies;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public List<CategoryItem> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryItem> categories) {
        this.categories = categories;
    }
}
