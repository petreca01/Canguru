package br.com.taqtile.android.app.data.profile.local.models;

/**
 * Created by taqtile on 17/04/17.
 */

public class UserImagesRequest {
  private String coverPhotoPath;
  private String profilePicturePath;

  public UserImagesRequest(String coverPhotoPath, String profilePicturePath) {
    this.coverPhotoPath = coverPhotoPath;
    this.profilePicturePath = profilePicturePath;
  }

  public String getCoverPhotoPath() {
    return coverPhotoPath;
  }

  public String getProfilePicturePath() {
    return profilePicturePath;
  }
}
