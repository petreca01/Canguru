package br.com.taqtile.android.app.data.channel.remote.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostsListRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsParams;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelPostsParamsToChannelPostsListRemoteRequestMapper {

  public static ChannelPostsListRemoteRequest perform(ChannelPostsParams channelPostsParams) {
    ChannelPostsListRemoteRequest channelPostsListRemoteRequest = new ChannelPostsListRemoteRequest();

    channelPostsListRemoteRequest.setChannelId(channelPostsParams.getChannelId());
    channelPostsListRemoteRequest.setPerPage(channelPostsParams.getPerPage());
    channelPostsListRemoteRequest.setPage(channelPostsParams.getPage());

    return channelPostsListRemoteRequest;
  }
}
