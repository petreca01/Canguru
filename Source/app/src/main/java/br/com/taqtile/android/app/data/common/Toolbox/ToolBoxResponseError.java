package br.com.taqtile.android.app.data.common.Toolbox;

import br.com.taqtile.android.app.data.common.model.DataSourceResponseError;

/**
 * Created by taqtile on 3/17/17.
 */

public class ToolBoxResponseError extends DataSourceResponseError {
    public enum ErrorType {
        InvalidData
    }

    private ErrorType errorType;

    public ToolBoxResponseError() {
        this.errorType = ErrorType.InvalidData;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}
