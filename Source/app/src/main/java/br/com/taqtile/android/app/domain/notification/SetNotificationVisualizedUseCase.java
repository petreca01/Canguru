package br.com.taqtile.android.app.domain.notification;

import br.com.taqtile.android.app.data.notification.NotificationRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.notification.models.SetVisualizedNotificationParams;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public class SetNotificationVisualizedUseCase
  extends BaseUseCase<Boolean, SetVisualizedNotificationParams, NotificationRepository> {

  public SetNotificationVisualizedUseCase(NotificationRepository notificationRepository) {
    super(notificationRepository);
  }

  @Override public Observable<Boolean> execute(
    SetVisualizedNotificationParams setVisualizedNotificationParams) {
    return getRepository().setVisualized(setVisualizedNotificationParams);
  }
}
