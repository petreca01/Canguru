package br.com.taqtile.android.app.presentation.signup;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.SignUpUseCase;
import br.com.taqtile.android.app.domain.account.models.SignUpParams;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 11/10/16.
 */

public class SignUpPresenter implements SignUpContract.Presenter {

  private SignUpUseCase signUp;

    public SignUpPresenter() {
       signUp = Injection.provideSignUpUseCase();
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public Observable<EmptyResult> createAccount(String email, String password, String name,
      String birthDate) {
      return signUp.execute(new SignUpParams(email, password, name, birthDate));
    }
}
