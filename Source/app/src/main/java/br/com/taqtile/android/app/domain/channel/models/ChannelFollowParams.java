package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelFollowParams {

  private Integer channelId;
  private boolean follow;

  public ChannelFollowParams(Integer channelId, boolean follow) {
    this.channelId = channelId;
    this.follow = follow;
  }

  public Integer getChannelId() {
    return channelId;
  }

  public void setChannelId(Integer channelId) {
    this.channelId = channelId;
  }

  public boolean isFollow() {
    return follow;
  }

  public void setFollow(boolean follow) {
    this.follow = follow;
  }

}
