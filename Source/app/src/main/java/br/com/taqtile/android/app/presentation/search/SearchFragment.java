package br.com.taqtile.android.app.presentation.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.SearchPaginatedAdapter;

import br.com.taqtile.android.app.listings.cells.SearchPlaceholderCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.misc.search.CustomSearchView;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.SkeletonLoadingGenerator;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 3/23/17.
 */

public class SearchFragment extends AppBaseFragment implements SearchPaginatedAdapter.SearchAdapterListener,
  CustomSearchView.SubmitSearchListener {

  @BindView(R.id.fragment_search_search_view)
  CustomSearchView searchView;

  @BindView(R.id.fragment_search_search_list_header)
  CustomTextView searchListHeader;
  @BindView(R.id.fragment_search_search_list_header_container)
  FrameLayout searchListHeaderContainer;

  @BindView(R.id.fragment_search_search_view_layout)
  LinearLayout searchViewLayout;

  @BindView(R.id.fragment_search_recycler_view)
  RecyclerView searchRecyclerView;

  @BindView(R.id.fragment_search_loading_layout)
  LinearLayout loadingLayout;

  @BindView(R.id.fragment_search_placeholder)
  LinearLayout placeholder;

  private SkeletonLoadingGenerator loadingGenerator;

  private CustomToolbar customToolbar;
  private SearchContract.Presenter presenter;

  private NavigationManager navigationManager;
  private List<SearchListingViewModel> searchResults;
  private LinearLayoutManager linearLayoutManager;
  private boolean shouldTriggerLoadMore;

  private SearchPaginatedAdapter searchPaginatedAdapter;

  public static SearchFragment newInstance() {
    return new SearchFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_search, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupRecyclerView();
    setupListeners();
    setupLoading();
  }

  private void setupView() {
    setupToolbar();
  }

  private void setupRecyclerView() {
    shouldTriggerLoadMore = false;
    searchResults = new ArrayList<>();
    searchPaginatedAdapter = new SearchPaginatedAdapter(searchResults, this.getContext());

    searchRecyclerView.setAdapter(searchPaginatedAdapter);
    linearLayoutManager = new LinearLayoutManager(getContext());
    searchRecyclerView.setLayoutManager(linearLayoutManager);
    searchPaginatedAdapter.setFooterButtonVisible(false);
  }

  @Override
  public void showLoading() {
    super.showLoading();
    loadingLayout.setVisibility(View.VISIBLE);
    searchRecyclerView.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    loadingLayout.setVisibility(View.GONE);
    searchRecyclerView.setVisibility(View.VISIBLE);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_search_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  public void setPresenter(SearchContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  //region Search

  @Override
  public void onSubmit(String text) {
    searchResults.clear();
    hideNotFoundPlaceholder();
    showLoading();
    SoftKeyboardHelper.hideKeyboard(getContext());
    searchListHeaderContainer.setVisibility(View.VISIBLE);

    fetchPost(text, true);

  }

  private void fetchPost(String text, boolean reload) {
    Subscription subscription = presenter.search(text, reload)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onPostSearchResultsFetched,
        this::onPostSearchResultsFetchedFailure,
        this::hideLoading);
    compositeSubscription.add(subscription);
  }

  private void onPostSearchResultsFetched(List<ForumListingViewModel> posts) {
    if (posts.size() > 0) {
      shouldTriggerLoadMore = true;
      searchResults.addAll(posts);
      searchPaginatedAdapter.setFooterButtonVisible(true);
      hideNotFoundPlaceholder();
    } else {
      shouldTriggerLoadMore = false;
      searchPaginatedAdapter.setFooterButtonVisible(false);
      if (searchResults.isEmpty()) {
        showNotFoundPlaceholder();
      }

    }

    searchPaginatedAdapter.notifyDataSetChanged();
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_forum),
      getString(R.string.analytics_event_search),
      getString(R.string.analytics_label_user_did_search)
    ));
  }

  private void showNotFoundPlaceholder() {
    placeholder.setVisibility(View.VISIBLE);
    searchListHeaderContainer.setVisibility(View.GONE);
  }

  private void hideNotFoundPlaceholder() {
    placeholder.setVisibility(View.GONE);
    searchListHeaderContainer.setVisibility(View.VISIBLE);
  }

  private void onPostSearchResultsFetchedFailure(Throwable throwable) {
    shouldTriggerLoadMore = true;
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_SHORT);
    hideLoading();
  }
  //endregion

  //region Search Results Click
  @Override
  public void onForumCellClick(String id) {
    navigationManager.showCommunityPostDetailUI(null, Integer.valueOf(id), false);
  }

  @Override
  public void onChannelCellClick(String id) {
    //TODO integrate
  }

  @Override
  public void onSymptomCellClick(String id) {
    //TODO integrate
  }
  //endregion

  private void setupListeners() {
    searchPaginatedAdapter.setListener(this);
    searchView.setSubmitSearchListener(this);

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (hasRecyclerViewReachedTheEnd() && shouldTriggerLoadMore) {
          shouldTriggerLoadMore = false;
          fetchPost(searchView.getText(), false);
        }
      }
    };

    searchRecyclerView.addOnScrollListener(onScrollListener);

  }

  private boolean hasRecyclerViewReachedTheEnd() {
    int visibleItemCount = linearLayoutManager.getChildCount();
    int totalItemCount = linearLayoutManager.getItemCount();
    int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
    return pastVisibleItems + visibleItemCount >= totalItemCount;
  }


  private void setupLoading() {
    loadingGenerator = new SkeletonLoadingGenerator(loadingLayout, new SearchPlaceholderCell(getContext()));
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_search),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
