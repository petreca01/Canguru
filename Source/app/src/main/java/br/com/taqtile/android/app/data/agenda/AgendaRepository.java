package br.com.taqtile.android.app.data.agenda;

import br.com.taqtile.android.app.data.agenda.remote.AgendaRemoteDataSource;
import br.com.taqtile.android.app.data.common.BaseRepository;
import java.util.List;

import br.com.taqtile.android.app.data.agenda.remote.mappers.AgendaItemRemoteResponseToAgendaItemResultMapper;
import br.com.taqtile.android.app.data.agenda.remote.mappers.AppointmentParamsToAppointmentRemoteRequestMapper;
import br.com.taqtile.android.app.data.agenda.remote.mappers.FinishAppointmentParamsToFinishAppointmentRemoteRequestMapper;
import br.com.taqtile.android.app.data.agenda.remote.mappers.ListAgendaItemRemoteResponseToListAgendaItemResultMapper;
import br.com.taqtile.android.app.data.agenda.remote.mappers.RemoveAppointmentParamsToRemoveAppointmentRemoteRequestMapper;
import br.com.taqtile.android.app.data.agenda.remote.mappers.ScheaduleParamsToScheaduleRemoteRequestMapper;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.domain.agenda.models.AgendaItemResult;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import rx.Observable;

/**
 * Created by taqtile on 3/21/17.
 */

public class AgendaRepository extends BaseRepository<AgendaRemoteDataSource, Void> {

  public AgendaRepository(AgendaRemoteDataSource agendaRemoteDataSource) {
    super(agendaRemoteDataSource, null);
  }

  public Observable<List<AgendaItemResult>> list() {
    return getRemoteDataSource().list()
      .map(result -> ListAgendaItemRemoteResponseToListAgendaItemResultMapper.perform(result.getResult()));
  }

  public Observable<AgendaItemResult> schedule(ScheduleParams params) {
    return getRemoteDataSource()
      .schedule(ScheaduleParamsToScheaduleRemoteRequestMapper.perform(params))
      .map(result -> AgendaItemRemoteResponseToAgendaItemResultMapper.perform(result.getResult()));
  }

  public Observable<AgendaItemResult> createAppointment(AppointmentParams params) {
    return getRemoteDataSource()
      .createAppointment(AppointmentParamsToAppointmentRemoteRequestMapper.perform(params))
      .map(result -> AgendaItemRemoteResponseToAgendaItemResultMapper.perform(result.getResult()));
  }

  public Observable<AgendaItemResult> finish(FinishAppointmentParams request) {
    return getRemoteDataSource()
      .finish(FinishAppointmentParamsToFinishAppointmentRemoteRequestMapper.perform(request))
      .map(result -> AgendaItemRemoteResponseToAgendaItemResultMapper.perform(result.getResult()));
  }

  public Observable<String> remove(RemoveAppointmentParams request) {
    return getRemoteDataSource()
      .remove(RemoveAppointmentParamsToRemoveAppointmentRemoteRequestMapper.perform(request))
      .map(BaseRemoteResponse::getResult);
  }
}
