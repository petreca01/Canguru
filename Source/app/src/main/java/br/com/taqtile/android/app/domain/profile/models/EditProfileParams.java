package br.com.taqtile.android.app.domain.profile.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class EditProfileParams {
  private String name;

  private String babyName;

  private String about;

  private String profilePicture;

  private String coverPicture;

  private String conclusionDate;

  private String conclusionMethod;

  private String conclusionText;

  private String removeCover;

  private String removeProfilePicture;

  public EditProfileParams(String name, String about, String profilePicture, String coverPicture, Boolean removeCover, Boolean removeProfilePicture) {
    this.name = name;
    this.about = about;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.removeCover = removeCover ? "1" : "0";
    this.removeProfilePicture = removeProfilePicture ? "1" : "0";
  }

  public String getName() {
    return name;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getAbout() {
    return about;
  }

  public String getProfilePicture() {
    return profilePicture;
  }

  public String getCoverPicture() {
    return coverPicture;
  }

  public String getConclusionDate() {
    return conclusionDate;
  }

  public String getConclusionMethod() { return conclusionMethod; }

  public String getConclusionText() { return conclusionText; }

  public String getRemoveCover() { return removeCover; }

  public String getRemoveProfilePicture() { return removeProfilePicture; }
}
