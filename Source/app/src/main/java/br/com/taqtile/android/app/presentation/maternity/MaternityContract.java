package br.com.taqtile.android.app.presentation.maternity;

import java.util.List;

import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.helpers.LocationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public interface MaternityContract {

  interface Presenter extends BasePresenter {

    Observable<List<MaternityViewModel>> fetchMaternitiesByCity(String city);

    Observable<UserViewModel> fetchUserData();
  }

}
