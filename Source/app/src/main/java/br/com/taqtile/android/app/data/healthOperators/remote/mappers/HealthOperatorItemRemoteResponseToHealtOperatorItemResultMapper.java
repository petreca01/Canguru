package br.com.taqtile.android.app.data.healthOperators.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.healthOperators.remote.models.response.HealthOperatorItemRemoteResponse;
import br.com.taqtile.android.app.domain.healthOperators.models.HealthOperatorItemResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/10/17.
 */

public class HealthOperatorItemRemoteResponseToHealtOperatorItemResultMapper {

  public static List<HealthOperatorItemResult> perform(List<HealthOperatorItemRemoteResponse> response) {
    return StreamSupport.stream(response)
      .map(HealthOperatorItemRemoteResponseToHealtOperatorItemResultMapper::map)
      .collect(Collectors.toList());
  }

  public static HealthOperatorItemResult map(HealthOperatorItemRemoteResponse response) {
    return new HealthOperatorItemResult(
      response.getLoaded(),
      response.getId(),
      response.getName(),
      response.getAbout(),
      response.getLogo(),
      response.getHealthPlanActive(),
      response.getActive(),
      response.getDocumentType());
  }

}
