package br.com.taqtile.android.app.domain.professional.models;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfessionalAuthorizationParams {

  private Integer professionalId;
  private Boolean questionnaireOnly;

  public Integer getProfessionalId() {
    return professionalId;
  }

  public Boolean getQuestionnaireOnly() {
    return questionnaireOnly;
  }

  public ProfessionalAuthorizationParams(Integer professionalId, Boolean questionnaireOnly) {
    this.professionalId = professionalId;
    this.questionnaireOnly = questionnaireOnly;
  }
}
