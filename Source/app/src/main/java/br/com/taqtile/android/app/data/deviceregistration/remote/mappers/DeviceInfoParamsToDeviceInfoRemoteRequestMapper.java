package br.com.taqtile.android.app.data.deviceregistration.remote.mappers;

import android.support.annotation.NonNull;

import br.com.taqtile.android.app.data.deviceregistration.remote.models.DeviceInfoRemoteRequest;
import br.com.taqtile.android.app.domain.deviceregistration.models.DeviceInfoParams;

/**
 * Created by taqtile on 3/30/17.
 */

public class DeviceInfoParamsToDeviceInfoRemoteRequestMapper {
  public static DeviceInfoRemoteRequest perform(@NonNull DeviceInfoParams params) {
    return new DeviceInfoRemoteRequest(params.getToken());
  }
}
