package br.com.taqtile.android.app.domain.channel;

import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelFollowParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 31/03/17.
 */

public class FollowChannelUseCase extends BaseUseCase<EmptyResult, ChannelFollowParams, ChannelRepository> {

  public FollowChannelUseCase(ChannelRepository channelRepository) {
    super(channelRepository);
  }

  @Override
  public Observable<EmptyResult> execute(ChannelFollowParams channelFollowParams) {
    return getRepository().follow(channelFollowParams);
  }
}
