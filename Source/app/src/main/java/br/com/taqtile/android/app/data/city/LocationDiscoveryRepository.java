package br.com.taqtile.android.app.data.city;

import br.com.taqtile.android.app.data.city.geocondingAPI.LocationDiscoveryGeocodingAPIDataSource;
import br.com.taqtile.android.app.data.city.geocondingAPI.mappers.LocationDiscoveryGeocodingAPIResponseToLocationDiscoveryResultMapper;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class LocationDiscoveryRepository extends BaseRepository<LocationDiscoveryGeocodingAPIDataSource, Void> {

  public LocationDiscoveryRepository(LocationDiscoveryGeocodingAPIDataSource cityDiscoveryDataSource) {
    super(cityDiscoveryDataSource, null);
  }

  public Observable<LocationDiscoveryResult> searchCityByZipCode(
    LocationDiscoveryByZipCodeParams locationDiscoveryByZipCodeParams) {
    return getRemoteDataSource().searchCityByZipCode(locationDiscoveryByZipCodeParams.getZipCode())
      .map(LocationDiscoveryGeocodingAPIResponseToLocationDiscoveryResultMapper::perform);
  }
}
