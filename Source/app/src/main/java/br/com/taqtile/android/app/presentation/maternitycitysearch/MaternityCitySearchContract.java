package br.com.taqtile.android.app.presentation.maternitycitysearch;

import java.util.List;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/12/17.
 */

public interface MaternityCitySearchContract {

  interface Presenter extends BasePresenter {
    Observable<List<String>> fetchCities();
  }
}
