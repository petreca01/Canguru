package br.com.taqtile.android.app.presentation.additionalinfoedit;

import android.content.Context;
import android.content.Intent;

import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo.HealthcareUserAdditionalInfoActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoEditActivity extends TemplateBackActivity {

  private AdditionalInfoEditFragment fragment;
  private static AdditionalInfoUserViewModel additionalInfoUserData;

  @Override
  public AdditionalInfoEditFragment getFragment() {
    AdditionalInfoEditFragment fragment = (AdditionalInfoEditFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (fragment == null) {
      fragment = AdditionalInfoEditFragment.newInstance();
    }
    this.fragment = fragment;
    this.fragment.setAdditionalInfoUserData(additionalInfoUserData);

    return this.fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    AdditionalInfoEditPresenter presenter = new AdditionalInfoEditPresenter();
    fragment.setPresenter(presenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return presenter;
  }

  public static void navigate(Context context,
                              AdditionalInfoUserViewModel additionalInfoUserDataViewModel,
                              CustomNavigationResultListener listener) {
    additionalInfoUserData = additionalInfoUserDataViewModel;
    Intent intent = new Intent(context, AdditionalInfoEditActivity.class);

    if (context instanceof AdditionalInfoActivity) {
      ((AdditionalInfoActivity) context).setResultListener(listener,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
      ((AdditionalInfoActivity) context).startActivityForResult(intent,
        HealthcareUserAdditionalInfoActivity.EDIT_USER_DATA_REQUEST_CODE);
    }
  }

}
