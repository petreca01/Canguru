package br.com.taqtile.android.app.presentation.prenatalcard;

import java.util.List;

import br.com.taqtile.android.app.domain.followcard.FollowCardConsultationsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardExamsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardUltrasoundsUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardUseCase;
import br.com.taqtile.android.app.domain.followcard.FollowCardVaccinesUseCase;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardConsultationsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardExamsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardUltrasoundsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardVaccinesViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/22/17.
 */

public class PrenatalCardPresenter implements PrenatalCardContract.Presenter {

  private FollowCardUseCase followCardUseCase;
  private FollowCardConsultationsUseCase followCardConsultationsUseCase;
  private FollowCardUltrasoundsUseCase followCardUltrasoundsUseCase;
  private FollowCardExamsUseCase followCardExamsUseCase;
  private FollowCardVaccinesUseCase followCardVaccinesUseCase;

  public PrenatalCardPresenter() {
    this.followCardUseCase = Injection.provideFollowCardUseCase();
    this.followCardConsultationsUseCase = Injection.provideFollowCardConsultationsUseCase();
    this.followCardUltrasoundsUseCase = Injection.provideFollowCardUltrasoundsUseCase();
    this.followCardExamsUseCase = Injection.provideFollowCardExamsUseCase();
    this.followCardVaccinesUseCase = Injection.provideFollowCardVaccinesUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<FollowCardViewModel> fetchAntecedentsCard() {
    return followCardUseCase.execute(null);
  }

  @Override
  public Observable<List<FollowCardConsultationsViewModel>> fetchMedicalAppointments() {
    return followCardConsultationsUseCase.execute(null);
  }

  @Override
  public Observable<List<FollowCardExamsViewModel>> fetchExamsCard() {
    return followCardExamsUseCase.execute(null);
  }

  @Override
  public Observable<List<FollowCardUltrasoundsViewModel>> fetchUltraSoundCard() {
    return followCardUltrasoundsUseCase.execute(null);
  }

  @Override
  public Observable<List<FollowCardVaccinesViewModel>> fetchVaccinesCard() {
    return followCardVaccinesUseCase.execute(null);
  }
}
