package br.com.taqtile.android.app.presentation.initiallogin;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.domain.signin.Models.FacebookLoginResult;
import br.com.taqtile.android.app.domain.signin.Models.FacebookUserResult;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 11/11/16.
 */

public class InitialLoginFragment extends AppBaseFragment
  implements InitialLoginContract.View {

  private static final String TAG = "InitialLoginFragment";
  @BindView(R.id.fragment_initial_login_sign_up_button)
  TemplateButton signUpButton;

  @BindView(R.id.fragment_initial_login_login_button)
  TemplateButton loginButton;

//  @BindView(R.id.fragment_initial_login_create_account_button)
//  TemplateButton facebookButton;

  private CustomToolbar customToolbar;
  private InitialLoginContract.Presenter presenter;
  private NavigationManager navigationManager;
  private Subscription subscription;

  public static InitialLoginFragment newInstance() {
    return new InitialLoginFragment();
  }

  public InitialLoginFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_initial_login, container, false);
    ButterKnife.bind(this, root);
    setupButtons();
    setupToolbar();
    setupAppbar();
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupToolbar();
    setupAppbar();
  }

  private void setupButtons() {
    loginButton.setOnClickListener(v -> navigationManager.showSignInUI(this::returningFromSignInOrSignUp));
    signUpButton.setOnClickListener(v -> navigationManager.showSignUpUI(this::returningFromSignInOrSignUp));
  }

  private void returningFromSignInOrSignUp(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      getActivity().finish();
    }
  }

  private void enableButtons() {
    loginButton.setEnabled(true);
    signUpButton.setEnabled(true);
//    facebookButton.setEnabled(true);
    if (subscription != null) {
      subscription.unsubscribe();
    }
  }

  private void disableButtons() {
    loginButton.setEnabled(false);
    signUpButton.setEnabled(false);
//    facebookButton.setEnabled(false);
  }

  private void setupToolbar() {
    if (getActivity() != null && getToolbar() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }

      ViewCompat.setElevation(getToolbar(), 0);
      customToolbar.hide();
    }
  }

  private void setupAppbar() {
    if (getActivity() != null && getAppBarLayout() != null) {
      getAppBarLayout().setVisibility(View.GONE);
    }
  }

  @Override
  public void setPresenter(InitialLoginContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_initial_login_placeholder;
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  private void handleFacebookCallback(FacebookLoginResult facebookLoginResult) {

      this.analytics.trackUser(facebookLoginResult.getEmail());
      this.analytics.trackEvent(new AnalyticsEvent(
        getString(R.string.analytics_category_login),
        getString(R.string.analytics_event_facebook),
        getString(R.string.analytics_label_login_facebook)
      ));

    getActivity().finish();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_initial_login),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
