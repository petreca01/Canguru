package br.com.taqtile.android.app.presentation.maternityownermaternity;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by duh on 3/12/18.
 */

public interface MaternityOwnerMaternityContract {

  interface Presenter extends BasePresenter {

  }

}
