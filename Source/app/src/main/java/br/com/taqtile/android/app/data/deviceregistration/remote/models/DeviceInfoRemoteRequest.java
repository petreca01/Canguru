package br.com.taqtile.android.app.data.deviceregistration.remote.models;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 7/11/16.
 */

public class DeviceInfoRemoteRequest extends BaseRemoteRequest {
  private String token;
  private String type;

  public DeviceInfoRemoteRequest(String token) {
    this.token = token;
    this.type = "A";
  }

}
