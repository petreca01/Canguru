package br.com.taqtile.android.app.domain.riskAnalysis;

import android.support.v4.util.Pair;

import br.com.taqtile.android.app.data.riskAnalysis.RiskAnalysisRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import br.com.taqtile.android.app.domain.riskAnalysis.mappers.RiskAnalysisQuestionsResultToRiskAnalysisQuestionResultMapper;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisAnswerResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class SendAnswersUseCase extends BaseUseCase<RiskAnalysisQuestionResult,
  Pair<AnswerParams, RiskAnalysisQuestionGroupResult>, RiskAnalysisRepository> {

  public SendAnswersUseCase(RiskAnalysisRepository riskAnalysisRepository) {
    super(riskAnalysisRepository);
  }

  @Override
  public Observable<RiskAnalysisQuestionResult> execute(
    Pair<AnswerParams, RiskAnalysisQuestionGroupResult> params) {
    return getRepository().answer(params.first)
      .flatMap(result ->
        RiskAnalysisQuestionsResultToRiskAnalysisQuestionResultMapper
          .perform(result.getQuestions(), params.second, params.first.getQuestionId()
          )
      );
  }
}
