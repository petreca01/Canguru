package br.com.taqtile.android.app.presentation.debugpanel;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.channel.ChannelActivity;
import br.com.taqtile.android.app.presentation.channel.ChannelFragment;
import br.com.taqtile.android.app.presentation.channel.ChannelPresenter;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 5/5/17.
 */

public class DebugPanelActivity extends TemplateBackActivity {


  private DebugPanelFragment fragment;

  @Override
  public DebugPanelFragment getFragment() {
    DebugPanelFragment debugPanelFragment = (DebugPanelFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (debugPanelFragment == null) {
      debugPanelFragment = DebugPanelFragment.newInstance();
    }
    fragment = debugPanelFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    DebugPanelPresenter debugPanelPresenter = new DebugPanelPresenter();
    fragment.setPresenter(debugPanelPresenter);

    return debugPanelPresenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, DebugPanelActivity.class);
    context.startActivity(intent);
  }
}
