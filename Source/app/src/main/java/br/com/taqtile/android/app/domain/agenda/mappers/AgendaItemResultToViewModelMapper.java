package br.com.taqtile.android.app.domain.agenda.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.agenda.models.AgendaItemResult;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/8/17.
 */

public class AgendaItemResultToViewModelMapper {

  public static List<AgendaItemViewModel> perform(List<AgendaItemResult> agendaItemResults) {
    return StreamSupport.stream(agendaItemResults)
      .map(AgendaItemResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  public static AgendaItemViewModel map(AgendaItemResult agendaItemResult) {
    return new AgendaItemViewModel(agendaItemResult.getLoaded(), agendaItemResult.getId(),
      agendaItemResult.getOperatorHighlight(), agendaItemResult.getQuarter(),
      agendaItemResult.getName(), agendaItemResult.getCode(), agendaItemResult.getTypeId(),
      agendaItemResult.getType(), agendaItemResult.getWeekStart(), agendaItemResult.getWeekEnd(),
      agendaItemResult.getDayEnd(), agendaItemResult.getDescription(),
      agendaItemResult.getDescriptionImageUrl(), agendaItemResult.getCompleteDescription(),
      agendaItemResult.getObservation(), agendaItemResult.getDone(),
      agendaItemResult.getCreatedAt(), agendaItemResult.getUpdatedAt(),
      agendaItemResult.getMarkingDate(), agendaItemResult.getMarkingObservation(),
      agendaItemResult.getIsAppointment());
  }
}
