package br.com.taqtile.android.app.presentation.additionalinfo;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.EditUserDataUseCase;
import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.healthOperators.SearchHealthOperatorsUseCase;
import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.presentation.additionalinfo.mapper.UserDataAndHealthOperatorToAdditionalInfoUserDataViewModelMapper;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import java8.util.stream.StreamSupport;
import rx.Observable;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoPresenter implements AdditionalInfoContract.Presenter {

  private UserDataUseCase userDataUseCase;
  private EditUserDataUseCase editUserDataUseCase;
  private SearchHealthOperatorsUseCase searchHealthOperatorsUseCase;

  public AdditionalInfoPresenter() {
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.searchHealthOperatorsUseCase = Injection.provideSearchHealthOperatorsUseCase();
    this.editUserDataUseCase = Injection.provideEditUserDataUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<AdditionalInfoUserViewModel> fetchUserData() {
    return userDataUseCase.execute(null)
      .flatMap(userDataViewModel -> {
        if (isHealthOperatorAtive(userDataViewModel)) {
          return getActiveHealthOperator(userDataViewModel);
        } else {
          return Observable.just(userDataViewModel)
            .map(healthOperator -> UserDataAndHealthOperatorToAdditionalInfoUserDataViewModelMapper
              .perform(userDataViewModel, null));
        }
      });
  }

  @Override
  public Observable<UserViewModel> updateLocation(EditUserDataParams editUserDataParams) {
    return editUserDataUseCase.execute(editUserDataParams);
  }

  @Override
  public Observable<UserViewModel> editUserData(EditUserDataParams editUserDataParams) {
    return editUserDataUseCase.execute(editUserDataParams);
  }

  private boolean isHealthOperatorAtive(UserViewModel userViewModel) {
    return userViewModel.getOperatorName() != null &&
      !userViewModel.getOperatorName().isEmpty();
  }

  private Observable<AdditionalInfoUserViewModel> getActiveHealthOperator(
    UserViewModel userViewModel) {
    return searchHealthOperatorsUseCase.execute(
      new SearchHealthOperatorParams(userViewModel.getOperatorName()))
      .map(healthOperators -> UserDataAndHealthOperatorToAdditionalInfoUserDataViewModelMapper
        .perform(userViewModel, getOption(healthOperators, userViewModel)));
  }

  private HealthOperatorViewModel getOption(List<HealthOperatorViewModel> healthOperatorViewModels,
                                            UserViewModel userViewModel) {
    return StreamSupport.stream(healthOperatorViewModels)
      .filter(healthOperatorViewModel ->
        healthOperatorViewModel.getId().equals(userViewModel.getOperatorId()) &&
          healthOperatorViewModel.getName().equals(userViewModel.getOperatorName()))
      .findFirst()
      .orElse(null);
  }

}
