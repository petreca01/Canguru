package br.com.taqtile.android.app.presentation.channelpostdetail;

import java.util.List;

import br.com.taqtile.android.app.domain.channel.ChannelPostDataUseCase;
import br.com.taqtile.android.app.domain.channel.DeleteChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostReplyUseCase;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.ReplyChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostLikeReplyParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyDeleteParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyParams;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.common.post.PostDetailContract;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 12/04/17.
 */

public class ChannelPostDetailPresenter implements PostDetailContract.Presenter,
  ChannelPostDetailContract.Presenter {

  private ChannelPostDataUseCase channelPostDataUseCase;
  private DeleteChannelPostUseCase deleteChannelPostUseCase;
  private LikeChannelPostReplyUseCase likeChannelPostReplyUseCase;
  private LikeChannelPostUseCase likeChannelPostUseCase;
  private ReplyChannelPostUseCase replyChannelPostUseCase;

  public ChannelPostDetailPresenter() {
    this.channelPostDataUseCase = Injection.provideChannelPostDataUseCase();
    this.deleteChannelPostUseCase = Injection.provideDeleteChannelPostUseCase();
    this.likeChannelPostReplyUseCase = Injection.provideLikeChannelPostReplyUseCase();
    this.likeChannelPostUseCase = Injection.provideLikeChannelPostUseCase();
    this.replyChannelPostUseCase = Injection.provideReplyChannelPostUseCase();
  }

  @Override
  public Observable<List<ListingsViewModel>> fetchPostModels(Integer postId) {
    return channelPostDataUseCase.execute(new ChannelPostParams(postId, null, null));
  }

  @Override
  public Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId,
                                                      boolean alreadyLiked) {

    if (replyId == null) {
      return likeChannelPostUseCase.execute(new ChannelPostParams(postId, null, !alreadyLiked));
    } else {
      return likeChannelPostReplyUseCase.execute(new ChannelPostLikeReplyParams(postId, replyId,
        !alreadyLiked));
    }

  }

  @Override
  public Observable<List<ListingsViewModel>> postComment(Integer postId, String message) {

    return replyChannelPostUseCase.execute(new ChannelPostReplyParams(postId, message));
  }

  @Override
  public Observable<List<ListingsViewModel>> deleteComment(Integer postId, Integer commentId) {

    return deleteChannelPostUseCase.execute(new ChannelPostReplyDeleteParams(postId, commentId));
  }

  @Override
  public void start() {
  }

  @Override
  public void resume() {
  }
}
