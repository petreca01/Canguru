package br.com.taqtile.android.app.data.forum.remote;

import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.ContentSuggestionRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.NewPostRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostAndRepliesRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostListResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public interface ForumServices {

  @FormUrlEncoded
  @POST("forum")
  Observable<Result<BaseRemoteResponse<PostListResponse>>> listPosts(
    @FieldMap Map<String, String> listPostsRequestModel);

  @FormUrlEncoded
  @POST("forum/post/new")
  Observable<Result<BaseRemoteResponse<NewPostRemoteResponse>>> createPost(
    @FieldMap Map<String, String> createPostRequestModel);

  @FormUrlEncoded
  @POST("forum/post")
  Observable<Result<BaseRemoteResponse<PostAndRepliesRemoteResponse>>> getPostAndReplies(
    @FieldMap Map<String, String> getPostAndRepliesRequestModel);

  @FormUrlEncoded
  @POST("forum/post/like")
  Observable<Result<BaseRemoteResponse<PostAndRepliesRemoteResponse>>> likePost(
    @FieldMap Map<String, String> likePostRequestModel);

  @FormUrlEncoded
  @POST("forum/post/reply")
  Observable<Result<BaseRemoteResponse<PostAndRepliesRemoteResponse>>> replyPost(
    @FieldMap Map<String, String> replyPostRequestModel);

  @FormUrlEncoded
  @POST("forum/post/destroy")
  Observable<Result<BaseRemoteResponse<String>>> delete(
    @FieldMap Map<String, String> deleteRequestModel);

  @FormUrlEncoded
  @POST("forum/post/warn")
  Observable<Result<BaseRemoteResponse<PostAndRepliesRemoteResponse>>> warn(
    @FieldMap Map<String, String> warnPostRequestModel);

  @FormUrlEncoded
  @POST("search")
  Observable<Result<BaseRemoteResponse<ContentSuggestionRemoteResponse>>> getSuggestedContent(
    @FieldMap Map<String, String> suggestedContentModel);

}
