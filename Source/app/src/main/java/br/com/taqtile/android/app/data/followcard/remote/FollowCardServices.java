package br.com.taqtile.android.app.data.followcard.remote;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardConsultationsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardExamsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardUltrasoundsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardVaccinesRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public interface FollowCardServices {

  @GET("follow_card")
  Observable<Result<BaseRemoteResponse<FollowCardRemoteResponse>>> fetchFollowCard();

  @GET("follow_card/consultations")
  Observable<Result<BaseRemoteResponse<List<FollowCardConsultationsRemoteResponse>>>> fetchFollowCardConsultations();

  @GET("follow_card/exams")
  Observable<Result<BaseRemoteResponse<List<FollowCardExamsRemoteResponse>>>> fetchFollowCardExams();

  @GET("follow_card/vaccines")
  Observable<Result<BaseRemoteResponse<List<FollowCardVaccinesRemoteResponse>>>> fetchFollowCardVaccines();

  @GET("follow_card/ultrasounds")
  Observable<Result<BaseRemoteResponse<List<FollowCardUltrasoundsRemoteResponse>>>> fetchFollowCardUltrasounds();


}
