package br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.edittexts.HealthOperatorRegisterNumberEditTextCaption;
import br.com.taqtile.android.app.edittexts.PhoneCellPhoneLabelEditTextCaption;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FocusHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FormTextHelper;
import br.com.taqtile.android.app.misc.utils.helpers.StringListFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.ValidateAndScrollHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.components.CPFLabelEditTextCaption;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.validatable.Validatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 20/04/17.
 */

public class HealthcareUserAdditionalInfoFragment extends AppBaseFragment implements
  ProgressBarButton.ProgressBarButtonButtonListener {

  private static final String TAG = "HealthcareUserAddInfoF";
  @BindView(R.id.fragment_healthcare_user_additional_info_header)
  CustomTextView header;

  @BindView(R.id.fragment_healthcare_user_additional_info_body)
  CustomTextView body;

  @BindView(R.id.fragment_healthcare_user_additional_info_cpf_form)
  CPFLabelEditTextCaption cpfEditText;

  @BindView(R.id.fragment_healthcare_user_additional_info_register_number_form)
  HealthOperatorRegisterNumberEditTextCaption registerNumberEditText;

  @BindView(R.id.fragment_healthcare_user_additional_info_phone_or_cellphone_form)
  PhoneCellPhoneLabelEditTextCaption cellphoneEditText;

  @BindView(R.id.fragment_healthcare_user_additional_info_save_button)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_healthcare_user_additional_scroll_layout)
  ScrollView scrollView;

  @BindView(R.id.fragment_healthcare_user_additional_content_layout)
  FrameLayout contentLayout;

  private CustomToolbar customToolbar;
  private HealthcareUserAdditionalInfoContract.Presenter presenter;
  private NavigationManager navigationManager;

  private HealthOperatorViewModel healthOperatorViewModel;

  private Validatable[] forms;
  private List<Validatable> errorForms;
  private String errorMessage;

  public static HealthcareUserAdditionalInfoFragment newInstance() {
    return new HealthcareUserAdditionalInfoFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_healthcare_user_additional_info, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupFormsList();
  }

  private void setupView() {
    setupButtons();
    setupHeader();
    setupBody();
    setupCpfRegister();
  }

  private String getDocument() {
    if (healthOperatorViewModel != null && healthOperatorViewModel.getDocumentType().equals("cpf")) {
      return cpfEditText.getText();
    } else {
      return registerNumberEditText.getText();
    }
  }

  private void setupFormsList() {
    if (healthOperatorViewModel != null && healthOperatorViewModel.getDocumentType().equals("cpf")) {
      forms = new Validatable[]{cellphoneEditText, cpfEditText};
    } else {
      forms = new Validatable[]{cellphoneEditText, registerNumberEditText};
    }

    errorForms = new ArrayList<Validatable>();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupHeader() {
    header.setText(getResources().getString(R.string.fragment_healthcare_user_additional_info_disclaimer_text));
  }

  private void setupBody() {
    if (healthOperatorViewModel != null && healthOperatorViewModel.getDocumentType().equals("cpf")) {
      body.setText(getResources().getString(R.string.fragment_healthcare_user_additional_info_disclaimer_body_text_cpf));
    } else {
      body.setText(getResources().getString(R.string.fragment_healthcare_user_additional_info_disclaimer_body_text_register_number));
    }
  }

  private void setupCpfRegister() {
    if (healthOperatorViewModel != null && healthOperatorViewModel.getDocumentType().equals("cpf")) {
      cpfEditText.setVisibility(View.VISIBLE);
      registerNumberEditText.setVisibility(View.GONE);
    } else {
      cpfEditText.setVisibility(View.GONE);
      registerNumberEditText.setVisibility(View.VISIBLE);
    }
  }

  private void setupButtons() {
    saveButton.setButtonListener(this::onButtonClicked);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_healthcare_user_additional_info_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

//  XXX
  @Override
  public void onButtonClicked() {
    if (locallyValidateForms()) {
      showLoading();
      Subscription editUserAditionalInfo = presenter.updateUserData(healthOperatorViewModel.getId(), cellphoneEditText.getText(), getDocument(), healthOperatorViewModel.getDocumentType())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::finishFromEditUserData, this::onEditUserDataFailure, this::hideLoading);
      compositeSubscription.add(editUserAditionalInfo);
    }
  }

  private void finishFromEditUserData(UserViewModel userViewModel) {
    Log.d(TAG, "finishFromEditUserData()");
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    getActivity().finish();

    navigationManager.showAdditionalInfo();

  }

  private void onEditUserDataFailure(Throwable throwable) {
    Log.d(TAG, "onEditUserDataFailure(): "+throwable.getMessage());
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    hideLoading();
  }

  public boolean locallyValidateForms() {
    boolean isValid = true;

    for (Validatable form : forms) {
      FocusHelper.clearViewFocus(form.getView(), contentLayout);
    }

    errorForms.clear();
    errorForms = ValidateAndScrollHelper.validateForms(forms);

    //Display feedback with all views with an error
    if (!errorForms.isEmpty()) {
      showFormValidationError();
      ValidateAndScrollHelper.scrollToView(errorForms.get(0).getView(), scrollView);
      isValid = false;
    }
    return isValid;
  }

  public void showFormValidationError() {
    errorMessage = "";
    showSnackbar(getResources().getString(R.string.forms_generic_error, buildErrorMessage(errorForms)), Snackbar.LENGTH_SHORT);
  }

  private String buildErrorMessage(List<Validatable> errorViews) {
    String[] errorForms = FormTextHelper.getFormsInputPlaceholderText(errorViews);
    return StringListFormatterHelper.buildStringList(errorForms, getResources().getString(R.string.required_symbol));
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setHealthOperatorViewModel(HealthOperatorViewModel healthOperatorViewModel) {
    Log.d(TAG, "setHealthOperatorViewModel(). healthOperatorViewModel.getName(): "+healthOperatorViewModel.getName());
    this.healthOperatorViewModel = healthOperatorViewModel;
  }

  public void setPresenter(HealthcareUserAdditionalInfoContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void showLoading() {
    saveButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    saveButton.showDefaultState();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_healthcare_additional_user_information),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
