package br.com.taqtile.android.app.presentation.signin;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/23/16.
 */

public interface SignInContract {

    interface Presenter extends BasePresenter{
        Observable<EmptyResult> signIn(String email, String password);
    }


}
