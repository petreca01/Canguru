package br.com.taqtile.android.app.presentation.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBottomNavActivity;
import br.com.taqtile.android.app.presentation.diary.DiaryContract;
import br.com.taqtile.android.app.presentation.diary.DiaryFragment;
import br.com.taqtile.android.app.presentation.diary.DiaryPresenter;
import br.com.taqtile.android.app.presentation.gestation.GestationContract;
import br.com.taqtile.android.app.presentation.gestation.GestationFragment;
import br.com.taqtile.android.app.presentation.gestation.GestationPresenter;
import br.com.taqtile.android.app.presentation.home.HomeContract;
import br.com.taqtile.android.app.presentation.home.HomeFragment;
import br.com.taqtile.android.app.presentation.home.HomePresenter;
import br.com.taqtile.android.app.presentation.maternity.MaternityContract;
import br.com.taqtile.android.app.presentation.maternity.MaternityFragment;
import br.com.taqtile.android.app.presentation.maternity.MaternityPresenter;
import br.com.taqtile.android.app.presentation.notifications.NotificationsContract;
import br.com.taqtile.android.app.presentation.notifications.NotificationsFragment;
import br.com.taqtile.android.app.presentation.notifications.NotificationsPresenter;
import br.com.taqtile.android.app.presentation.tutorial.TutorialComponent;
import br.com.taqtile.android.app.presentation.tutorial.TutorialViewPagerCellModel;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.app.support.deviceregistration.service.DeviceRegistrationIntentService;
import br.com.taqtile.android.cleanbase.presentation.activity.model.BottomNavItem;
import br.com.taqtile.android.cleanbase.presentation.util.ActivityUtils;
import io.fabric.sdk.android.Fabric;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.tutorial.TutorialViewPagerCellModel.INVALID_INDEX;

/**
 * Created by taqtile on 7/4/16.
 */

public class MainActivity extends TemplateBottomNavActivity implements TutorialComponent.TutorialPageListener {

  public static final int HOME_INDEX = 0,
    SCHEDULE_INDEX = 1,
    GESTATION_INDEX = 2,
    MATERNITY_INDEX = 3,
    NOTIFICATION_INDEX = 4;

  private static final int BOTTOM_NAV_ANIMATION_TIME = 200;

  HomeFragment homeView;
  DiaryFragment diaryView;
  GestationFragment gestationView;
  MaternityFragment maternityView;
  NotificationsFragment notificationsView;

  TutorialComponent tutorialComponent;
  List<TutorialViewPagerCellModel> tutorialContent;

  private final static String REQUEST_CODE = "requestCode";
  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  HomeContract.Presenter homePresenter;
  NotificationsContract.Presenter notificationPresenter;
  GestationContract.Presenter gestationPresenter;
  MaternityContract.Presenter maternityPresenter;
  DiaryContract.Presenter diaryPresenter;

  private Handler handler;
  private Subscription checkSubscription;
  private int parentIndex;
  private ViewGroup parent;

  private static final String AF_DEV_KEY = "HD8g5HK3BfTqWxoEy3oou6";
  private AppsFlyerConversionListener conversionDataListener;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      restartApp();
      super.onCreate(null);
    } else {
      super.onCreate(savedInstanceState);
      Fabric.with(this, new Crashlytics());
      setupPushNotifications();
      appsFlyerSetup();
      handler = new Handler();
    }
  }

  private void appsFlyerSetup(){

    conversionDataListener = new AppsFlyerConversionListener() {
      @Override
      public void onInstallConversionDataLoaded(Map<String, String> map) {

      }

      @Override
      public void onInstallConversionFailure(String s) {

      }

      @Override
      public void onAppOpenAttribution(Map<String, String> map) {

      }

      @Override
      public void onAttributionFailure(String s) {

      }
    };

    AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionDataListener, getApplicationContext());
    AppsFlyerLib.getInstance().startTracking(getApplication());

  }

  private void setupPushNotifications() {
    if (ActivityUtils.checkPlayServices(this)) {
      Intent intent = new Intent(this, DeviceRegistrationIntentService.class);
      startService(intent);
    }
  }

  @Override
  protected void onStart() {
    super.onStart();

    setParentPosition();
    checkTutorial();
  }

  private void setParentPosition() {
    parent = (ViewGroup) this.findViewById(android.R.id.content);
    parentIndex = parent.getChildCount();
  }

  private void checkTutorial() {
    checkSubscription = Toolbox.getInstance().getMainTutorial()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onCheckTutorialSuccess, this::onCheckTutorialFailure);
  }

  private void onCheckTutorialFailure(Throwable throwable) {
    // do nothing
  }

  private void onCheckTutorialSuccess(Boolean alreadySeen) {
    if (!alreadySeen && tutorialComponent == null) {
      tutorialContent = getTutorialContent();
      tutorialComponent = new TutorialComponent(this, tutorialContent);
      parent.addView(tutorialComponent, parentIndex);
      tutorialComponent.setTutorialPageListener(this);
    }
  }


  @Override
  public ArrayList<BottomNavItem> getNavigationItems() {
    ArrayList<BottomNavItem> navItems = new ArrayList<>();

    homeView = HomeFragment.newInstance();
    diaryView = DiaryFragment.newInstance();
    gestationView = GestationFragment.newInstance();
    maternityView = MaternityFragment.newInstance();
    notificationsView = NotificationsFragment.newInstance();

    setupHomeFragment();
    setupGestationFragment();
    setupMaternityFragment();
    setupNotificationFragment();
    setupDiaryFragment();
    setupGestationFragment();

    navItems.add(new BottomNavItem(
      getResources().getString(R.string.bottom_nav_home),
      ContextCompat.getDrawable(this, R.drawable.ic_inicio),
      homeView, homePresenter));
    navItems.add(new BottomNavItem(
      getResources().getString(R.string.bottom_nav_schedule),
      ContextCompat.getDrawable(this, R.drawable.ic_agenda),
      diaryView,
      diaryPresenter));
    navItems.add(new BottomNavItem(
      getResources().getString(R.string.bottom_nav_gestation),
      ContextCompat.getDrawable(this, R.drawable.ic_gestacao),
      gestationView,
      gestationPresenter));
    navItems.add(new BottomNavItem(
      getResources().getString(R.string.bottom_nav_maternity),
      ContextCompat.getDrawable(this, R.drawable.ic_maternidade),
      maternityView, maternityPresenter));
    navItems.add(new BottomNavItem(
      getResources().getString(R.string.bottom_nav_notifications),
      ContextCompat.getDrawable(this, R.drawable.ic_notification),
      notificationsView,
      notificationPresenter));

    return navItems;
  }

  private void setupHomeFragment() {
    homePresenter = new HomePresenter();
    homeView.setNavigationManager(new NavigationHelper(this));
    homeView.setPresenter(homePresenter);
  }

  private void setupMaternityFragment() {
    maternityPresenter = new MaternityPresenter();
    maternityView.setNavigationManager(new NavigationHelper(this));
    maternityView.setPresenter(maternityPresenter);
  }

  private void setupNotificationFragment() {
    notificationPresenter = new NotificationsPresenter();
    notificationsView.setNavigationManager(new NavigationHelper(this));
    notificationsView.setPresenter(notificationPresenter);
  }

  private void setupGestationFragment() {
    gestationPresenter = new GestationPresenter();
    gestationView.setNavigationManager(new NavigationHelper(this));
    gestationView.setPresenter(gestationPresenter);
  }

  private void setupDiaryFragment() {
    diaryPresenter = new DiaryPresenter();
    diaryView.setNavigationManager(new NavigationHelper(this));
    diaryView.setPresenter(diaryPresenter);
  }

  @Override
  public boolean isBehaviorTranslationEnabled() {
    return false;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  public void onActivityReenter(int resultCode, Intent data) {
    super.onActivityReenter(resultCode, data);
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null && onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
      observedRequestCode = 0;
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }


  private List<TutorialViewPagerCellModel> getTutorialContent() {
    return Arrays.asList(
      getTutorialInitialContent(),
      getTutorialHomeContent(),
      getTutorialScheduleContent(),
      getTutorialGestationContent(),
      getTutorialMaternityContent(),
      getTutorialNotificatonContext());
  }

  private TutorialViewPagerCellModel getTutorialInitialContent() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_inital,
      R.string.tutorial_initial_title,
      R.string.tutorial_initial_body,
      INVALID_INDEX);
  }

  private TutorialViewPagerCellModel getTutorialHomeContent() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_home,
      R.string.tutorial_home_title,
      R.string.tutorial_home_body,
      HOME_INDEX);
  }

  private TutorialViewPagerCellModel getTutorialScheduleContent() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_agenda,
      R.string.tutorial_agenda_title,
      R.string.tutorial_agenda_body,
      SCHEDULE_INDEX);
  }

  private TutorialViewPagerCellModel getTutorialGestationContent() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_gestation,
      R.string.tutorial_gestation_title,
      R.string.tutorial_gestation_body,
      GESTATION_INDEX);
  }

  private TutorialViewPagerCellModel getTutorialMaternityContent() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_maternity,
      R.string.tutorial_maternity_title,
      R.string.tutorial_maternity_body,
      MATERNITY_INDEX);
  }

  private TutorialViewPagerCellModel getTutorialNotificatonContext() {
    return buildTutorialViewPagerCellModel(R.drawable.ic_tutorial_notification,
      R.string.tutorial_notification_title,
      R.string.tutorial_notitication_body,
      NOTIFICATION_INDEX);
  }

  @NonNull
  private TutorialViewPagerCellModel buildTutorialViewPagerCellModel(@DrawableRes int icon,
                                                                     @StringRes int title,
                                                                     @StringRes int body,
                                                                     int index) {
    return new TutorialViewPagerCellModel(icon,
      getResources().getString(title),
      getResources().getString(body),
      index);
  }

  @Override
  public void onPageChanged(int position) {
    tutorialContent.get(position);
    if (tutorialContent.get(position).targetIndex != INVALID_INDEX) {
      this.bottomNav.setActiveItem(tutorialContent.get(position).targetIndex);
      // This is a workaround to wait the Tab animation to finish since the lib doesn't expose an observable for that.
      handler.postDelayed(() -> tutorialComponent.focusOnView(bottomNav.getActiveView()), BOTTOM_NAV_ANIMATION_TIME);
    } else {
      tutorialComponent.setNoFocus();
    }
  }

  @Override
  public void onTutorialClose() {
    ViewGroup parent = (ViewGroup) this.findViewById(android.R.id.content);
    parent.removeView(tutorialComponent);
    setActiveItem(HOME_INDEX);
    homeView.refreshDataAfterTutorial();
  }

  private void restartApp() {
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
    finish();
  }

}
