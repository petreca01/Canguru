package br.com.taqtile.android.app.presentation.forgotpassword;

import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import br.com.taqtile.android.cleanbase.presentation.view.BaseView;
import br.com.taqtile.android.app.support.NavigationManager;
import rx.Observable;

/**
 * Created by taqtile on 11/10/16.
 */

public interface ForgotPasswordContract {

    interface View extends BaseView<ForgotPasswordContract.Presenter, NavigationManager> {

    }

    interface Presenter extends BasePresenter {
        Observable<ForgotPasswordResult> recoverPassword(String email);
    }

}
