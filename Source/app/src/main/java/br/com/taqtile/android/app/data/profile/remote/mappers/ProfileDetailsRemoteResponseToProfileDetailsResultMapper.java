package br.com.taqtile.android.app.data.profile.remote.mappers;

import br.com.taqtile.android.app.data.profile.remote.models.response.ProfileDetailsRemoteResponse;
import br.com.taqtile.android.app.domain.profile.models.ProfileDetailsResult;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfileDetailsRemoteResponseToProfileDetailsResultMapper {

  public static ProfileDetailsResult perform(
    ProfileDetailsRemoteResponse profileDetailsRemoteResponse) {
    return new ProfileDetailsResult(
      getId(profileDetailsRemoteResponse.getId()),
      getUserType(profileDetailsRemoteResponse.getUserType()),
      getName(profileDetailsRemoteResponse.getName()),
      getBabyName(profileDetailsRemoteResponse.getBabyName()),
      getAbout(profileDetailsRemoteResponse.getAbout()),
      getProfilePicture(profileDetailsRemoteResponse.getProfilePicture()),
      getCoverPicture(profileDetailsRemoteResponse.getCoverPicture()),
      getPregnancyWeeks(profileDetailsRemoteResponse.getPregnancyWeeks()),
      getPregnancyDays(profileDetailsRemoteResponse.getPregnancyDays()),
      getFollowing(profileDetailsRemoteResponse.getFollowing()),
      getCity(profileDetailsRemoteResponse.getCity()),
      getState(profileDetailsRemoteResponse.getState()),
      getLocationPrivacyAgreed(profileDetailsRemoteResponse.getLocationPrivacyAgreed()),
      getCreatedAt(profileDetailsRemoteResponse.getCreatedAt()),
      getUpdatedAt(profileDetailsRemoteResponse.getUpdatedAt()));
  }

  private static Integer getId(Integer id) {
    return id;
  }

  private static String getUserType(String userType) {
    return userType != null ? userType : "";
  }

  private static String getName(String name) {
    return name != null ? name : "";
  }

  private static String getBabyName(String babyName) {
    return babyName != null ? babyName : "";
  }

  private static String getAbout(String about) {
    return about != null ? about : "";
  }

  private static String getProfilePicture(String profilePicture) {
    return profilePicture != null ? profilePicture : "";
  }

  private static String getCoverPicture(String coverPicture) {
    return coverPicture != null ? coverPicture : "";
  }

  private static Integer getPregnancyWeeks(Integer pregnancyWeeks) {
    return pregnancyWeeks != null ? pregnancyWeeks : 0;
  }

  private static Integer getPregnancyDays(Integer pregnancyDays) {
    return pregnancyDays != null ? pregnancyDays : 0;
  }

  private static boolean getFollowing(Boolean following) {
    return following != null ? following : false;
  }

  private static String getCity(String city) {
    return city != null ? city : "";
  }

  private static String getState(String state) {
    return state != null ? state : "";
  }

  private static boolean getLocationPrivacyAgreed(Boolean locationPrivacyAgreed) {
    return locationPrivacyAgreed != null ? locationPrivacyAgreed : false;
  }

  private static String getCreatedAt(String createdAt) {
    return createdAt != null ? createdAt : "";
  }

  private static String getUpdatedAt(String updatedAt) {
    return updatedAt != null ? updatedAt : "";
  }
}
