package br.com.taqtile.android.app.domain.symptoms;

import br.com.taqtile.android.app.data.symptoms.SymptomsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.symptoms.mapper.SymptomResultToViewModelMapper;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.SymptomViewModel;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ListSymptomsUseCase extends BaseUseCase<List<SymptomViewModel>, Integer, SymptomsRepository> {

  public ListSymptomsUseCase(SymptomsRepository symptomsRepository) {
    super(symptomsRepository);
  }

  @Override
  public Observable<List<SymptomViewModel>> execute(Integer gestationWeeks) {
    return getRepository().list()
      .map(result -> SymptomResultToViewModelMapper.perform(result, gestationWeeks));
  }
}
