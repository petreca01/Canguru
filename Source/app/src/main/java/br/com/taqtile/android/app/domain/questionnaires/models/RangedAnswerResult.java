package br.com.taqtile.android.app.domain.questionnaires.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class RangedAnswerResult {

  private Integer min;

  private Integer max;

  public RangedAnswerResult(Integer min, Integer max) {
    this.min = min;
    this.max = max;
  }

  public Integer getMin() {
    return min;
  }

  public Integer getMax() {
    return max;
  }
}
