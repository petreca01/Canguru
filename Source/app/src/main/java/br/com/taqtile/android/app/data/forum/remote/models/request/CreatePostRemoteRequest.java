package br.com.taqtile.android.app.data.forum.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 4/17/17.
 */

public class CreatePostRemoteRequest extends BaseRemoteRequest {

  @RequestName("title")
  private String title;

  @RequestName("content")
  private String content;

  public CreatePostRemoteRequest(String title, String content) {
    this.title = title;
    this.content = content;
  }
}
