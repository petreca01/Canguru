package br.com.taqtile.android.app.presentation.communitypostdetail;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.forum.DeleteForumPostUseCase;
import br.com.taqtile.android.app.domain.forum.ForumPostDataUseCase;
import br.com.taqtile.android.app.domain.forum.LikeForumPostItemUseCase;
import br.com.taqtile.android.app.domain.forum.ReplyForumPostUseCase;
import br.com.taqtile.android.app.domain.forum.WarnPostUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import br.com.taqtile.android.app.domain.forum.models.WarnPostParams;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.common.post.PostDetailContract;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class CommunityPostDetailPresenter implements PostDetailContract.Presenter,
  CommunityPostDetailContract.Presenter {

  private ForumPostDataUseCase forumPostDataUseCase;
  private DeleteForumPostUseCase deleteForumPostUseCase;
  private LikeForumPostItemUseCase likeForumPostItemUseCase;
  private ReplyForumPostUseCase replyForumPostUseCase;
  private WarnPostUseCase warnPostUseCase;

  public CommunityPostDetailPresenter() {
    this.forumPostDataUseCase = Injection.provideForumPostDataUseCase();
    this.deleteForumPostUseCase = Injection.provideDeleteForumPostUseCase();
    this.likeForumPostItemUseCase = Injection.provideLikeForumPostReplyUseCase();
    this.replyForumPostUseCase = Injection.provideReplyForumPostUseCase();
    this.warnPostUseCase = Injection.provideWarnPostUseCase();
  }

  @Override
  public void start() {
  }

  @Override
  public void resume() {
  }

  @Override
  public Observable<List<ListingsViewModel>> fetchPostModels(Integer postId) {
    return forumPostDataUseCase.execute(new PostParams(postId));
  }

  @Override
  public Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked) {
    return likeForumPostItemUseCase.execute(new PostParams(postId, replyId, null, alreadyLiked ? 1 : 0));
  }

  @Override
  public Observable<List<ListingsViewModel>> postComment(Integer postId, String message) {
    return replyForumPostUseCase.execute(new PostParams(postId, null, message, null));
  }

  @Override
  public Observable<String> deleteComment(Integer postId, Integer commentId) {
    return deleteForumPostUseCase.execute(new PostParams(postId, commentId, null, null));
  }

  @Override
  public Observable<String> deletePost(Integer postId) {
    return deleteForumPostUseCase.execute(new PostParams(postId));
  }

  @Override
  public Observable<EmptyResult> reportPost(Integer postId, String reason) {
    return warnPostUseCase.execute(new WarnPostParams(postId, reason))
      .map(any -> new EmptyResult());
  }

}
