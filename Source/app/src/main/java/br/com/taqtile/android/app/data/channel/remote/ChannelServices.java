package br.com.taqtile.android.app.data.channel.remote;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelCategoryListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostItemRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostsRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import java.util.List;
import java.util.Map;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public interface ChannelServices {
  @POST("channels/category") Observable<Result<BaseRemoteResponse<ChannelCategoryListRemoteResponse>>> list();

  @POST("channels/following")
  Observable<Result<BaseRemoteResponse<List<ChannelPostItemRemoteResponse>>>> listFollowingChannelsPosts();

  @FormUrlEncoded
  @POST("channel")
  Observable<Result<BaseRemoteResponse<ChannelPostsRemoteResponse>>> listPostsOfAChannel(
    @FieldMap Map<String, String> listPostsRequestModel);

  @FormUrlEncoded
  @POST("channel/post")
  Observable<Result<BaseRemoteResponse<ChannelPostRemoteResponse>>> post(
    @FieldMap Map<String, String> postRequestModel);

  @FormUrlEncoded
  @POST("channel/post/reply")
  Observable<Result<BaseRemoteResponse<ChannelPostRemoteResponse>>> postReply(
    @FieldMap Map<String, String> replyRequestModel);

  @FormUrlEncoded
  @POST("channel/post/reply/like")
  Observable<Result<BaseRemoteResponse<ChannelPostRemoteResponse>>> likeReply(
    @FieldMap Map<String, String> likeRequestModel);

  @FormUrlEncoded
  @POST("channel/post/reply/destroy")
  Observable<Result<BaseRemoteResponse<ChannelPostRemoteResponse>>> deleteReply(
    @FieldMap Map<String, String> deleteReplyRequestModel);

  @FormUrlEncoded
  @POST("channel")
  Observable<Result<BaseRemoteResponse<EmptyResult>>> follow(
    @FieldMap Map<String, String> deleteReplyRequestModel);

}
