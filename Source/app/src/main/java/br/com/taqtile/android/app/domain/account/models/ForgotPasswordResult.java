package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordResult {

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
