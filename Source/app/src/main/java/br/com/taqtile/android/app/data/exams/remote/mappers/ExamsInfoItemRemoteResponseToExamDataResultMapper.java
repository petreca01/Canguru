package br.com.taqtile.android.app.data.exams.remote.mappers;

import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsInfoItemRemoteResponse;
import br.com.taqtile.android.app.domain.exams.models.ExamDataResult;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsInfoItemRemoteResponseToExamDataResultMapper {

  public static ExamDataResult perform(ExamsInfoItemRemoteResponse examsInfoItemRemoteResponse) {
    return new ExamDataResult(examsInfoItemRemoteResponse.getLoaded(),
      examsInfoItemRemoteResponse.getId(),
      examsInfoItemRemoteResponse.getUserType(),
      examsInfoItemRemoteResponse.getOperatorId(),
      examsInfoItemRemoteResponse.getOperator(),
      examsInfoItemRemoteResponse.getCanOperatorSeeRiskAnswers(),
      examsInfoItemRemoteResponse.getName(),
      examsInfoItemRemoteResponse.getAbout(),
      examsInfoItemRemoteResponse.getProfilePicture(),
      examsInfoItemRemoteResponse.getCoverPicture(),
      examsInfoItemRemoteResponse.getBabyName(),
      examsInfoItemRemoteResponse.getCpf(),
      examsInfoItemRemoteResponse.getUserBirthdate(),
      examsInfoItemRemoteResponse.getEmail(),
      examsInfoItemRemoteResponse.getCellphone(),
      examsInfoItemRemoteResponse.getState(),
      examsInfoItemRemoteResponse.getZipCode(),
      examsInfoItemRemoteResponse.getLatitude(),
      examsInfoItemRemoteResponse.getLongitude(),
      examsInfoItemRemoteResponse.getToken(),
      examsInfoItemRemoteResponse.getDum(),
      examsInfoItemRemoteResponse.getDolpUpdatedAt(),
      examsInfoItemRemoteResponse.getSure(),
      examsInfoItemRemoteResponse.getIgWeaks(),
      examsInfoItemRemoteResponse.getIgDays(),
      examsInfoItemRemoteResponse.getScheduleEngagement(),
      examsInfoItemRemoteResponse.getRisk(),
      examsInfoItemRemoteResponse.getActive(),
      examsInfoItemRemoteResponse.getDeactivated_at(),
      examsInfoItemRemoteResponse.getDeactivation_reason(),
      examsInfoItemRemoteResponse.getConclusion_date(),
      examsInfoItemRemoteResponse.getConclusion_text(),
      examsInfoItemRemoteResponse.getConclusion_method(),
      examsInfoItemRemoteResponse.getPregnancy_dates_updated_at(),
      examsInfoItemRemoteResponse.getLocation_blocked_at(),
      examsInfoItemRemoteResponse.getLocationPrivacyAgreedAt(),
      examsInfoItemRemoteResponse.getCreated_at(),
      examsInfoItemRemoteResponse.getUpdated_at(),
      examsInfoItemRemoteResponse.getOperatorHighlight(),
      examsInfoItemRemoteResponse.getQuarter(),
      examsInfoItemRemoteResponse.getCode(),
      examsInfoItemRemoteResponse.getIdType(),
      examsInfoItemRemoteResponse.getType(),
      examsInfoItemRemoteResponse.getWeakBegin(),
      examsInfoItemRemoteResponse.getWeakEnd(),
      examsInfoItemRemoteResponse.getDayEnd(),
      examsInfoItemRemoteResponse.getDescription(),
      examsInfoItemRemoteResponse.getDescriptionImagemUrl(),
      examsInfoItemRemoteResponse.getFullDescription(),
      examsInfoItemRemoteResponse.getOptional(),
      examsInfoItemRemoteResponse.getObservation(),
      examsInfoItemRemoteResponse.getAccomplished(),
      examsInfoItemRemoteResponse.getDataMark(),
      examsInfoItemRemoteResponse.getObservationMark(),
      examsInfoItemRemoteResponse.getIsAppointment());
  }
}
