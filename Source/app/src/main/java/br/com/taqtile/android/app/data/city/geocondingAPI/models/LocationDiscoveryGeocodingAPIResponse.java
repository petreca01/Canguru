package br.com.taqtile.android.app.data.city.geocondingAPI.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by taqtile on 6/2/17.
 */

public class LocationDiscoveryGeocodingAPIResponse {

  @SerializedName("address_components")
  private List<AddressComponentGeocodingAPIResponse> addressComponents;
  @SerializedName("geometry")
  private GeometryGeocodingAPIResponse geometry;

  public List<AddressComponentGeocodingAPIResponse> getAddressComponents() {
    return addressComponents;
  }

  public GeometryGeocodingAPIResponse getGeometry() {
    return geometry;
  }
}
