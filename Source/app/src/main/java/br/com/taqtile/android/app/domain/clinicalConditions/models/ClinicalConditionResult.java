package br.com.taqtile.android.app.domain.clinicalConditions.models;

import java.util.List;

/**
 * Created by taqtile on 4/10/17.
 */

public class ClinicalConditionResult {
  private ClinicalConditionItemResult clinicalConditionResult;
  private List<ClinicalConditionItemResult> children;

  public ClinicalConditionResult(ClinicalConditionItemResult clinicalConditionResult,
                                 List<ClinicalConditionItemResult> children) {
    this.clinicalConditionResult = clinicalConditionResult;
    this.children = children;
  }

  public ClinicalConditionItemResult getClinicalConditionResult() {
    return clinicalConditionResult;
  }

  public List<ClinicalConditionItemResult> getChildren() {
    return children;
  }
}
