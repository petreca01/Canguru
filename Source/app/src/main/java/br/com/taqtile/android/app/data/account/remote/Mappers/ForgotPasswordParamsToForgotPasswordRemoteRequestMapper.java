package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.account.remote.models.request.ForgotPasswordRemoteRequest;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordParams;

/**
 * Created by taqtile on 3/6/17.
 */

public class ForgotPasswordParamsToForgotPasswordRemoteRequestMapper {

    public static ForgotPasswordRemoteRequest perform(ForgotPasswordParams params) {
        ForgotPasswordRemoteRequest request = new ForgotPasswordRemoteRequest();

        request.setEmail(params.getEmail());

        return request;
    }
}
