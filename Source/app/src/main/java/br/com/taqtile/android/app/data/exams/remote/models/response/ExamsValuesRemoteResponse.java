package br.com.taqtile.android.app.data.exams.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/22/17.
 */

public class ExamsValuesRemoteResponse {
    @SerializedName("dias")
    private String days;

    @SerializedName("semanas")
    private String weeks;

    public String getDays() {
        return days;
    }

    public String getWeeks() {
        return weeks;
    }
}
