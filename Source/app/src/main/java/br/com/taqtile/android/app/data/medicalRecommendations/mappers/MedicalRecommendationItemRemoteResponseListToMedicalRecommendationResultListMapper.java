package br.com.taqtile.android.app.data.medicalRecommendations.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.medicalRecommendations.remote.models.MedicalRecommendationItemRemoteResponse;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class MedicalRecommendationItemRemoteResponseListToMedicalRecommendationResultListMapper {
  public static List<MedicalRecommendationResult> perform(List<MedicalRecommendationItemRemoteResponse> medicalRecommendationItemRemoteResponseList) {
    return StreamSupport.stream(medicalRecommendationItemRemoteResponseList)
      .map(MedicalRecommendationItemRemoteResponseToMedicalRecommendationResultMapper::perform)
      .collect(Collectors.toList());
  }
}
