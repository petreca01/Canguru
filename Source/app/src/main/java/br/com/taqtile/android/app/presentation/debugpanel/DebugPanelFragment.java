package br.com.taqtile.android.app.presentation.debugpanel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.remote.RemoteDataSourceInterceptor;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/5/17.
 */

public class DebugPanelFragment extends Fragment {

  private DebugPanelContract.Presenter presenter;

  @BindView(R.id.fragment_debug_panel_textview_error)
  TextView textviewError;
  @BindView(R.id.fragment_debug_panel_switch_error)
  Switch switchButtonError;
  @BindView(R.id.fragment_debug_panel_textview_wait)
  TextView textviewWait;
  @BindView(R.id.fragment_debug_panel_switch_wait)
  Switch switchButtonWait;
  @BindView(R.id.fragment_debug_panel_edittext_wait)
  EditText edittext;

  public void setPresenter(DebugPanelContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public static DebugPanelFragment newInstance() {
    return new DebugPanelFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_debug_panel, container, false);

    ButterKnife.bind(this, root);
    setupComponents();
    return root;
  }

  public void setupComponents() {
    String panic = new String(Character.toChars(0x1F631));
    String noEntry = new String(Character.toChars(0x1F6AB));
    String alarm = new String(Character.toChars(0x23F0));
    String sleepFace = new String(Character.toChars(0x1F634));
    textviewError.setText("Fazer as respostas das requests falharem  " + noEntry + panic);
    textviewWait.setText("Fazer as requests esperarem  " + alarm + sleepFace);
    edittext.setText(String.valueOf(RemoteDataSourceInterceptor.getDelaySeconds()));
    switchButtonError.setChecked(RemoteDataSourceInterceptor.getMakeResquestReturnError());
    switchButtonWait.setChecked(RemoteDataSourceInterceptor.getMakeRequestWait());
    switchButtonError.setOnCheckedChangeListener(this::onToogleErrorChecked);
    switchButtonWait.setOnCheckedChangeListener(this::onToogleWaitChecked);
  }

  private void onToogleErrorChecked(CompoundButton buttonView, boolean isChecked) {
    RemoteDataSourceInterceptor.setMakeResquestReturnError(isChecked);
  }

  private void onToogleWaitChecked(CompoundButton buttonView, boolean isChecked) {
    RemoteDataSourceInterceptor.setMakeRequestWait(isChecked, Integer.parseInt(edittext.getText().toString()));
  }
}
