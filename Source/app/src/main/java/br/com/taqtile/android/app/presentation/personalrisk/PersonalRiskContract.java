package br.com.taqtile.android.app.presentation.personalrisk;

import java.util.List;

import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by Renato on 4/17/17.
 */

public interface PersonalRiskContract {

  interface Presenter extends BasePresenter {
    Observable<List<RiskAnalysisQuestionGroupResult>> fetchRiskAnalysisQuestionAnswersList();

    Observable<List<RiskAnalysisQuestionGroupResult>> sendRiskAnalysisQuestionAnswersList(String email);
  }
}
