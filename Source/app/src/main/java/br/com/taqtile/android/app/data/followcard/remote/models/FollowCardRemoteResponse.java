package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardRemoteResponse {

  @SerializedName("name")
  private String name;

  @SerializedName("age")
  private String age;

  @SerializedName("gpa")
  private GPARemoteResponse gpa;

  @SerializedName("risks")
  private RisksRemoteResponse risks;

  public String getName() {
    return name;
  }

  public String getAge() {
    return age;
  }

  public GPARemoteResponse getGpa() {
    return gpa;
  }

  public RisksRemoteResponse getRisks() {
    return risks;
  }
}
