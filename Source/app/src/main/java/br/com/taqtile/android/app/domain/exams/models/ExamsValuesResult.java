package br.com.taqtile.android.app.domain.exams.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsValuesResult {

  private String days;

  private String weeks;

  public ExamsValuesResult() {
  }

  public ExamsValuesResult(String days, String weeks) {
    this.days = days;
    this.weeks = weeks;
  }

  public String getDays() {
    return days;
  }

  public String getWeeks() {
    return weeks;
  }
}
