package br.com.taqtile.android.app.presentation.channeldetail;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 4/5/17.
 */

public interface ChannelDetailsContract {

  interface Presenter extends BasePresenter {
    Observable<List<ListingsViewModel>> fetchChannelDetailsModels(Integer id, boolean withHeader);

    Observable<EmptyResult> followChannel(Integer id, boolean isFollowing);

    Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked);
  }

}
