package br.com.taqtile.android.app.presentation.profilemenu;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.cells.ProfileHeaderCell;
import br.com.taqtile.android.app.listings.cells.TextCell;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.listings.cells.TextImageCellCenter;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Renato on 4/18/17.
 */

public class ProfileMenuFragment extends AppBaseFragment {

  private ProfileMenuContract.Presenter presenter;
  private NavigationManager navigationManager;
  CustomToolbar customToolbar;

  @BindView(R.id.fragment_profile_menu_profile_header_cell)
  ProfileHeaderCell profileHeader;
  @BindView(R.id.fragment_profile_menu_additional_info)
  TextImageCellCenter additionalInfo;
  @BindView(R.id.fragment_profile_menu_change_password)
  TextImageCellCenter changePassword;
  @BindView(R.id.fragment_profile_menu_connected_professionals)
  TextImageCellCenter connectedProfessionals;
  @BindView(R.id.fragment_profile_menu_about_canguru)
  TextCell aboutCanguru;
  @BindView(R.id.fragment_profile_menu_use_terms)
  TextCell useTerms;
  @BindView(R.id.fragment_profile_menu_privacy_policy)
  TextCell privacyPolice;
  @BindView(R.id.fragment_profile_menu_logout)
  TextCell logout;

  public static ProfileMenuFragment newInstance() {
    return new ProfileMenuFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_profile_menu, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  public void setPresenter(@NonNull ProfileMenuContract.Presenter presenter) {
    this.presenter = checkNotNull(presenter);
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }


  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupListeners();
    setupData();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupListeners() {
    profileHeader.setListener(() -> navigationManager.showPublicProfileUI(null, this::returningFromEditPublicProfile));
    additionalInfo.setListener( () -> navigationManager.showAdditionalInfo());
    changePassword.setListener(() -> navigationManager.showChangePasswordUI());
    connectedProfessionals.setListener(() -> navigationManager.showConnectedProfessionals());
    aboutCanguru.setListener((view, text) -> navigationManager.showAboutCanguruUI());
    useTerms.setListener((view, text) -> navigationManager.showUseTermsUI());
    privacyPolice.setListener((view, text) -> navigationManager.showPrivacyPolicyUI());
    logout.setListener((view, text) -> this.confirmLogout());
  }

  private void returningFromEditPublicProfile(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      setupData();
    }
  }

  private void confirmLogout() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setMessage(getString(R.string.fragment_profile_menu_logout_message))
      .setPositiveButton(getString(R.string.yes), (dialog, which) -> logout())
      .setNegativeButton(getString(R.string.no), (dialog, which) -> {
      });
    builder.create().show();
  }

  private void logout() {
    Subscription subscription = presenter.logout()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onLogoutSuccess,
        this::onLogoutFailure);
    compositeSubscription.add(subscription);
  }

  private void onLogoutSuccess(EmptyResult emptyResult) {

    getActivity().finish();
    navigationManager.showInitialLoginUI();

    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_logout),
      getString(R.string.analytics_event_logout),
      getString(R.string.analytics_label_logout)
    ));
    this.analytics.trackUser(null);
    getActivity().onBackPressed();

  }

  private void onLogoutFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  // XXX: Thats a good deal
  private void setupData() {
    Subscription subscription = Toolbox.getInstance()
      .getUserName()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::setUserName,
        this::handleError
      );

    Subscription subscription1 = Toolbox.getInstance()
      .getUserProfilePictureURL()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::setUserProfilePicture,
        this::handleError
      );

    compositeSubscription.add(subscription);
    compositeSubscription.add(subscription1);
  }

  private void setUserProfilePicture(String pictureURL) {
    profileHeader.setProfileImage(pictureURL);
  }

  private void setUserName(String name) {
    profileHeader.setProfileName(name);
  }

  private void handleError(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_profile_menu_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_menu_profile),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
