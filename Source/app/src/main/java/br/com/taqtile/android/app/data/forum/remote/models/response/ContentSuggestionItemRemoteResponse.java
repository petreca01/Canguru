package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionItemRemoteResponse {

  @SerializedName("content_id")
  private Integer contentId;

  @SerializedName("content_parent_id")
  private Integer contentParentId;

  @SerializedName("id")
  private Integer id;

  @SerializedName("type")
  private String type;

  @SerializedName("title")
  private String title;

  @SerializedName("resume")
  private String resume;

  @SerializedName("likes")
  private Integer likes;

  @SerializedName("count_replies")
  private Integer countReplies;

  public Integer getContentId() {
    return contentId;
  }

  public Integer getContentParentId() {
    return contentParentId;
  }

  public Integer getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getResume() {
    return resume;
  }

  public Integer getLikes() {
    return likes;
  }

  public Integer getCountReplies() {
    return countReplies;
  }

  public String getTitle() {
    return title;
  }

}
