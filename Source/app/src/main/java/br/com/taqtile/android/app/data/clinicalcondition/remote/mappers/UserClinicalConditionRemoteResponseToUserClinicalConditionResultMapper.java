package br.com.taqtile.android.app.data.clinicalcondition.remote.mappers;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.UserClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.domain.clinicalConditions.models.UserClinicalConditionResult;

/**
 * Created by taqtile on 4/12/17.
 */

public class UserClinicalConditionRemoteResponseToUserClinicalConditionResultMapper {
  public static UserClinicalConditionResult perform(UserClinicalConditionRemoteResponse response) {
    return new UserClinicalConditionResult(
      response.getPregnancyConditionId(),
      response.getName(),
      response.getDate(),
      response.getGestationAgeWeeks(),
      response.getGestationAgeDays(),
      response.getRisk());
  }
}
