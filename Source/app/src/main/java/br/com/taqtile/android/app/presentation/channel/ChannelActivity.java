package br.com.taqtile.android.app.presentation.channel;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.channel.ChannelFragment;
import br.com.taqtile.android.app.presentation.channel.ChannelPresenter;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.presentation.signin.SignInFragment;
import br.com.taqtile.android.app.presentation.signin.SignInPresenter;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.activity.BaseActivityFragment;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 3/22/17.
 */

public class ChannelActivity extends TemplateBackActivity {

    private ChannelFragment fragment;

    @Override
    public ChannelFragment getFragment() {
        ChannelFragment channelFragment = (ChannelFragment) getSupportFragmentManager()
                .findFragmentById(getFragmentContainerId());
        if (channelFragment == null) {
            channelFragment = ChannelFragment.newInstance();
        }
        fragment = channelFragment;

        return fragment;
    }

    @Override
    public BasePresenter getPresenter() {
        ChannelPresenter channelPresenter = new ChannelPresenter();
        fragment.setNavigationManager(new NavigationHelper(this));
        fragment.setPresenter(channelPresenter);

        return channelPresenter;
    }

    public static void navigate(Context context) {
        Intent intent = new Intent(context, ChannelActivity.class);
        context.startActivity(intent);
    }
}
