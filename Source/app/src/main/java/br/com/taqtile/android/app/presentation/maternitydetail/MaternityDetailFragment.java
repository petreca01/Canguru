package br.com.taqtile.android.app.presentation.maternitydetail;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.maternity.models.DeleteCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityParams;
import br.com.taqtile.android.app.listings.adapters.MaternityReviewCommentsAdapter;
import br.com.taqtile.android.app.listings.cells.CommentCell;
import br.com.taqtile.android.app.listings.cells.MaternityDetailHeaderCell;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityDetailsViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.maternityownermaternity.MaternityOwnerMaternityActivity;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.maternitydetail.MaternityDetailActivity.MATERNITY_ID_TAG;
import static br.com.taqtile.android.app.support.GlobalConstants.RATING_MATERNITY_KEY;

/**
 * Created by taqtile on 09/05/17.
 */

public class MaternityDetailFragment extends AppBaseFragment
  implements MaternityDetailHeaderCell.MaternityDetailHeaderCellListener, CommentCell.CommentCellListener {

  @BindView(R.id.maternity_recycler_view)
  RecyclerView maternityCommentsRecyclerView;

  private NavigationManager navigationManager;
  private MaternityDetailContract.Presenter presenter;
  private MaternityReviewCommentsAdapter adapter;
  private CustomToolbar customToolbar;

  private int maternityId;
  private MaternityDetailsViewModel maternityDetails;

  public static MaternityDetailFragment newInstance() {
    return new MaternityDetailFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);

    View root = inflater.inflate(R.layout.fragment_maternity_detail, container, false);
    this.maternityId = getActivity().getIntent().getIntExtra(MATERNITY_ID_TAG, 0);
    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupRecyclerView();
    setupToolbar();
  }

  @Override
  public void willAppear() {
    super.willAppear();
    getMaternityDetails();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_hospital_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void getMaternityDetails() {
    showLoading();
    Subscription subscription = Toolbox.getInstance().getUserId()
      .flatMap(userId -> {
        adapter.setUserId(userId);
        return presenter.getMaternityDetailsUseCase(new MaternityParams(maternityId));
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onGetMaternityDetailsSuccess, this::onGetMaternityDetailsFailure,
        this::hideLoading);
    compositeSubscription.add(subscription);
  }

  private void onGetMaternityDetailsSuccess(MaternityDetailsViewModel maternityDetails) {
    this.maternityDetails = maternityDetails;
    adapter.setMaternityDetails(maternityDetails);
    adapter.notifyDataSetChanged();
  }

  private void onGetMaternityDetailsFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupRecyclerView() {
    adapter = new MaternityReviewCommentsAdapter(this, this);
    maternityCommentsRecyclerView.setAdapter(adapter);
    maternityCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  public void setPresenter(MaternityDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_maternity_detail_placeholder;
  }

  @Override
  public void onRatingMaternityClicked() {
    navigationManager.showMaternityRating(maternityDetails.getMaternity(), this::returningFromRating);
  }

  private void returningFromRating(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      showSnackbar(bundle.getString(RATING_MATERNITY_KEY, ""), Snackbar.LENGTH_LONG);
    }
  }

  @Override
  public void onMaternityPhoneNumberClicked() {
    startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse(getResources()
      .getString(R.string.fragment_maternity_detail_phone) + maternityDetails.getMaternity().getPhone())));
  }

  @Override
  public void onMaternityOwnerClicked() {
    Intent intent = new Intent(getContext(), MaternityOwnerMaternityActivity.class);
    startActivity(intent);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources()
        .getString(R.string.fragment_maternity_detail_title), Gravity.LEFT);
    }
  }

  @Override
  public void onMaternityMapClicked() {
    Uri gmmIntentUri = Uri.parse("geo::0,0?q=" + maternityDetails.getMaternity().getAddress());
    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
      startActivity(mapIntent);
    }
  }

  @Override
  public void showLoading() {
    super.showLoading();
    maternityCommentsRecyclerView.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    maternityCommentsRecyclerView.setVisibility(View.VISIBLE);
  }

  @Override
  public void onPlaceholderButtonClick() {
    getMaternityDetails();
  }

  private void onDeleteCommentSuccess(List<MaternityCommentViewModel> maternityCommentViewModelList) {
    this.maternityDetails.setComments(maternityCommentViewModelList);
    adapter.notifyDataSetChanged();
  }

  private void onDeleteCommentFailure(Throwable throwable) {
    hideLoading();
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  @Override
  public void onCellLongClick(Integer commentId) {
    showLoading();
    DeleteCommentParams params = new DeleteCommentParams(commentId, maternityId);
    Subscription subscription = presenter.deleteMaternityCommentUseCase(params)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onDeleteCommentSuccess, this::onDeleteCommentFailure, this::hideLoading);
    compositeSubscription.add(subscription);
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void onLikeClick(Integer id, boolean isLikeActivated) {
  }

  @Override
  public void onProfileRegionClick(Integer id) {
    navigationManager.showPublicProfileUI(id, null);
  }
}
