package br.com.taqtile.android.app.domain.search.models;

/**
 * Created by taqtile on 28/03/17.
 */

public class PostSearchParams {
  private String searchKeyword;
  private boolean reload;

  public PostSearchParams(String searchKeyword, boolean reload) {
    this.searchKeyword = searchKeyword;
    this.reload = reload;
  }

  public String getSearchKeyword() {
    return searchKeyword;
  }

  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }

  public boolean isReload() {
    return reload;
  }

}
