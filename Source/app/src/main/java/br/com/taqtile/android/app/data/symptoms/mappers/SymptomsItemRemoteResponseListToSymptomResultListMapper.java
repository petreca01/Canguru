package br.com.taqtile.android.app.data.symptoms.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.SymptomsItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.SymptomResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class SymptomsItemRemoteResponseListToSymptomResultListMapper {
  public static List<SymptomResult> perform(List<SymptomsItemRemoteResponse> symptomsItemRemoteResponseList) {
    return StreamSupport.stream(symptomsItemRemoteResponseList)
      .map(SymptomsItemRemoteResponseToSymptomResultMapper::perform)
      .collect(Collectors.toList());
  }
}
