package br.com.taqtile.android.app.presentation.maternitynotfound;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionItemViewModel;
import br.com.taqtile.android.app.presentation.additionalinfo.AdditionalInfoActivity;
import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.condition.ConditionsActivity;
import br.com.taqtile.android.app.presentation.healthcaredetails.HealthcareDetailsActivity;
import br.com.taqtile.android.app.presentation.searchhealthcare.SearchHealthcareActivity;
import br.com.taqtile.android.app.presentation.searchhealthcare.SearchHealthcarePresenter;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import static br.com.taqtile.android.app.support.GlobalConstants.CLINICAL_CONDITIONS_DETAILS_REQUEST_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.CLINICAL_CONDITIONS_TAG;

public class MaternityNotFoundActivity extends TemplateBackActivity {

  private MaternityNotFoundFragment fragment;

  @Override
  public Fragment getFragment() {

    MaternityNotFoundFragment maternityNotFoundFragment = (MaternityNotFoundFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());

    if (maternityNotFoundFragment == null) {
      maternityNotFoundFragment = MaternityNotFoundFragment.newInstance();
    }
    fragment = maternityNotFoundFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    MaternityNotFoundPresenter maternityNotFoundPresenter = new MaternityNotFoundPresenter();
    fragment.setPresenter(maternityNotFoundPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return maternityNotFoundPresenter;
  }

  public static void navigate(Context context) {
    context.startActivity(new Intent(context, MaternityNotFoundActivity.class));
  }

}
