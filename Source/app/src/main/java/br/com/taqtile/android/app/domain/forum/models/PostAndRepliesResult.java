package br.com.taqtile.android.app.domain.forum.models;

import java.util.List;

/**
 * Created by taqtile on 4/4/17.
 */

public class PostAndRepliesResult {
  private PostResult post;
  private List<ReplyResult> replies;

  public PostAndRepliesResult(PostResult post, List<ReplyResult> replies) {
    this.post = post;
    this.replies = replies;
  }

  public PostResult getPost() {
    return post;
  }

  public List<ReplyResult> getReplies() {
    return replies;
  }
}
