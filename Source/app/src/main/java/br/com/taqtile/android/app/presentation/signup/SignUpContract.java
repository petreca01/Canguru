package br.com.taqtile.android.app.presentation.signup;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 11/10/16.
 */

public interface SignUpContract {

    interface Presenter extends BasePresenter {
        Observable<EmptyResult> createAccount(String email, String password, String name,
          String birthdate);
    }

}
