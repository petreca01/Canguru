package br.com.taqtile.android.app.data.followcard.remote.mappers;

import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.GPARemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.RisksRemoteResponse;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardResult;
import br.com.taqtile.android.app.domain.followcard.models.GestationHistoryResult;
import br.com.taqtile.android.app.domain.followcard.models.RisksResult;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardRemoteResponseToResultMapper {

  public static FollowCardResult perform(FollowCardRemoteResponse followCardRemoteResponse) {
    return new FollowCardResult(followCardRemoteResponse.getName(),
      followCardRemoteResponse.getAge(),
      FollowCardRemoteResponseToResultMapper.map(followCardRemoteResponse.getGpa()),
      FollowCardRemoteResponseToResultMapper.map(followCardRemoteResponse.getRisks()));
  }

  private static GestationHistoryResult map(GPARemoteResponse gpa) {
    return new GestationHistoryResult(gpa.getG(), gpa.getPn(), gpa.getPc(), gpa.getA());
  }

  private static RisksResult map(RisksRemoteResponse risks) {
    return new RisksResult(risks.getAlert(), risks.getRisk());
  }
}
