package br.com.taqtile.android.app.presentation.initiallogin;

import android.os.Bundle;

import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.EMAIL_KEY;
import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.FB_TOKEN_KEY;
import static br.com.taqtile.android.app.presentation.initiallogin.InitialLoginBundleConstants.NAME_KEY;

/**
 * Created by taqtile on 26/04/17.
 */

public class InitialLoginBundleCreator {

  public static Bundle getBundle(String name, String email, String token) {
    Bundle bundle = new Bundle(3);
    bundle.putString(EMAIL_KEY, email);
    bundle.putString(NAME_KEY, name);
    bundle.putString(FB_TOKEN_KEY, token);

    return bundle;
  }

}
