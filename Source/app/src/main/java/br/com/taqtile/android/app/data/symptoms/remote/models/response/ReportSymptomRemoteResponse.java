package br.com.taqtile.android.app.data.symptoms.remote.models.response;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;

/**
 * Created by taqtile on 3/23/17.
 */

public class ReportSymptomRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("user")
    private UserRemoteResponse user;

    @SerializedName("user_id")
    private Integer userId;

    @SerializedName("symptom")
    private SubSymptomItemRemoteResponse symptom;

    @SerializedName("symptom_id")
    private Integer symptomId;

    @SerializedName("description")
    private String description;

    @SerializedName("date")
    private String date;

    @SerializedName("ga_weeks")
    private Integer gestationAgeWeeks;

    @SerializedName("ga_days")
    private Integer gestationAgeDays;

    public Integer getId() {
        return id;
    }

    public UserRemoteResponse getUser() {
        return user;
    }

    public Integer getUserId() {
        return userId;
    }

    public SubSymptomItemRemoteResponse getSymptom() {
        return symptom;
    }

    public Integer getSymptomId() {
        return symptomId;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public Integer getGestationAgeWeeks() {
        return gestationAgeWeeks;
    }

    public Integer getGestationAgeDays() {
        return gestationAgeDays;
    }
}
