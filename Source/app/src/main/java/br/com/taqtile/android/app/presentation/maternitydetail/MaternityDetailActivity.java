package br.com.taqtile.android.app.presentation.maternitydetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.REQUEST_CODE;

/**
 * Created by taqtile on 09/05/17.
 */

public class MaternityDetailActivity extends TemplateBackActivity {

  private MaternityDetailFragment fragment;
  public static final String MATERNITY_ID_TAG = "maternity_id";

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private Bundle onActivityResultBundle;
  private int onActivityResultCode;
  private int onActivityRequestCode;

  @Override public Fragment getFragment() {
    MaternityDetailFragment medicalRecommendationsFragment =
      (MaternityDetailFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (medicalRecommendationsFragment == null) {
      medicalRecommendationsFragment = MaternityDetailFragment.newInstance();
    }
    fragment = medicalRecommendationsFragment;

    return fragment;
  }

  @Override public BasePresenter getPresenter() {
    MaternityDetailPresenter medicalRecommendationsPresenter = new MaternityDetailPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(medicalRecommendationsPresenter);

    return medicalRecommendationsPresenter;
  }

  public static void navigate(Context context, int id) {
    Intent intent = new Intent(context, MaternityDetailActivity.class);
    intent.putExtra(MATERNITY_ID_TAG, id);
    context.startActivity(intent);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == observedRequestCode
      && resultListener != null) {
      if (data == null) {
        data = new Intent();
      }
      data.putExtra(REQUEST_CODE, requestCode);
      onActivityResultBundle = data.getExtras();
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
      resultListener = null;
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
