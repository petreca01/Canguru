package br.com.taqtile.android.app.presentation.common.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by taqtile on 4/26/17.
 */

public class UserViewModel implements Parcelable {

  private String email;
  private String telephone;
  private String birthDate;
  private String cpf;
  private String zipCode;

  private Integer operatorId;
  private String operatorName;
  private String operatorDocument;

  private String userType;
  private Integer gestationTrimesters;
  private Integer gestationMonths;
  private Integer gestationWeeks;
  private Integer gestationDays;
  private String babyName;
  private String userDUM;
  private String childbirthEstimatedDate;
  private String childbirthEstimatedDateBase;

  private String city;
  private String state;
  private String latitude;
  private String longitude;
  private boolean locationPrivacyAgreed;
  private String pregnancyDatesUpdatedAt;
  private String dolpUpdatedAt;

  public UserViewModel(String email, String telephone, String birthDate, String cpf,
                       String zipCode, Integer operatorId, String operatorName,
                       Boolean locationPrivacyAgreed) {
    this.email = email;
    this.telephone = telephone;
    this.birthDate = birthDate;
    this.cpf = cpf;
    this.zipCode = zipCode;
    this.operatorId = operatorId;
    this.operatorName = operatorName;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
  }

  public UserViewModel(String email, String telephone, String birthDate, String cpf, String operatorDocument,
                       String zipCode, Integer operatorId, String operatorName,
                       String userType, Integer gestationTrimesters, Integer gestationMonths,
                       Integer gestationWeeks, Integer gestationDays, String babyName,
                       String userDUM, String childbirthEstimatedDate,
                       String childbirthEstimatedDateBase, String city, String state,
                       String latitude, String longitude, Boolean locationPrivacyAgreed, String pregnancyDatesUpdatedAt, String dolpUpdatedAt) {
    this.email = email;
    this.telephone = telephone;
    this.birthDate = birthDate;
    this.cpf = cpf;
    this.zipCode = zipCode;
    this.operatorId = operatorId;
    this.operatorName = operatorName;
    this.operatorDocument = operatorDocument;
    this.userType = userType;
    this.gestationTrimesters = gestationTrimesters;
    this.gestationMonths = gestationMonths;
    this.gestationWeeks = gestationWeeks;
    this.gestationDays = gestationDays;
    this.babyName = babyName;
    this.userDUM = userDUM;
    this.childbirthEstimatedDate = childbirthEstimatedDate;
    this.childbirthEstimatedDateBase = childbirthEstimatedDateBase;
    this.city = city;
    this.state = state;
    this.latitude = latitude;
    this.longitude = longitude;
    this.locationPrivacyAgreed = locationPrivacyAgreed;
    this.pregnancyDatesUpdatedAt = pregnancyDatesUpdatedAt;
    this.dolpUpdatedAt = dolpUpdatedAt;
  }

  public String getEmail() {
    return email;
  }

  public String getTelephone() {
    return telephone;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getCpf() {
    return cpf;
  }

  public String getZipCode() {
    return zipCode;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public String getOperatorName() {
    return operatorName;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public Integer getGestationTrimesters() {
    return gestationTrimesters;
  }

  public Integer getGestationMonths() {
    return gestationMonths;
  }

  public Integer getGestationWeeks() {
    return gestationWeeks;
  }

  public Integer getGestationDays() {
    return gestationDays;
  }

  public String getUserType() {
    return userType;
  }

  public String getBabyName() {
    return babyName;
  }

  public String getUserDUM() {
    return userDUM;
  }

  public String getChildbirthEstimatedDate() {
    return childbirthEstimatedDate;
  }

  public String getChildbirthEstimatedDateBase() {
    return childbirthEstimatedDateBase.toUpperCase();
  }

  public Boolean getLocationPrivacyAgreed() {
    return locationPrivacyAgreed;
  }

  public String getPregnancyDatesUpdatedAt() {
    return pregnancyDatesUpdatedAt;
  }

  public String getDolpUpdatedAt() {
    return dolpUpdatedAt;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.email);
    dest.writeString(this.telephone);
    dest.writeString(this.birthDate);
    dest.writeString(this.cpf);
    dest.writeString(this.operatorDocument);
    dest.writeString(this.zipCode);
    dest.writeValue(this.operatorId);
    dest.writeString(this.operatorName);
    dest.writeString(this.userType);
    dest.writeValue(this.gestationTrimesters);
    dest.writeValue(this.gestationMonths);
    dest.writeValue(this.gestationWeeks);
    dest.writeValue(this.gestationDays);
    dest.writeString(this.babyName);
    dest.writeString(this.userDUM);
    dest.writeString(this.childbirthEstimatedDate);
    dest.writeString(this.childbirthEstimatedDateBase);
    dest.writeString(this.city);
    dest.writeString(this.state);
    dest.writeString(this.latitude);
    dest.writeString(this.longitude);
    dest.writeByte(this.locationPrivacyAgreed ? (byte) 1 : (byte) 0);
    dest.writeString(this.pregnancyDatesUpdatedAt);
    dest.writeString(this.dolpUpdatedAt);
  }

  protected UserViewModel(Parcel in) {
    this.email = in.readString();
    this.telephone = in.readString();
    this.birthDate = in.readString();
    this.cpf = in.readString();
    this.operatorDocument = in.readString();
    this.zipCode = in.readString();
    this.operatorId = (Integer) in.readValue(Integer.class.getClassLoader());
    this.operatorName = in.readString();
    this.userType = in.readString();
    this.gestationTrimesters = (Integer) in.readValue(Integer.class.getClassLoader());
    this.gestationMonths = (Integer) in.readValue(Integer.class.getClassLoader());
    this.gestationWeeks = (Integer) in.readValue(Integer.class.getClassLoader());
    this.gestationDays = (Integer) in.readValue(Integer.class.getClassLoader());
    this.babyName = in.readString();
    this.userDUM = in.readString();
    this.childbirthEstimatedDate = in.readString();
    this.childbirthEstimatedDateBase = in.readString();
    this.city = in.readString();
    this.state = in.readString();
    this.latitude = in.readString();
    this.longitude = in.readString();
    this.locationPrivacyAgreed = in.readByte() != 0;
    this.pregnancyDatesUpdatedAt = in.readString();
    this.dolpUpdatedAt = in.readString();
  }

  public static final Creator<UserViewModel> CREATOR = new Creator<UserViewModel>() {
    @Override
    public UserViewModel createFromParcel(Parcel source) {
      return new UserViewModel(source);
    }

    @Override
    public UserViewModel[] newArray(int size) {
      return new UserViewModel[size];
    }
  };
}
