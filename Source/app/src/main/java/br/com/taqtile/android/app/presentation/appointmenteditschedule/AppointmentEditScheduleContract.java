package br.com.taqtile.android.app.presentation.appointmenteditschedule;

import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 27/04/17.
 */

public interface AppointmentEditScheduleContract {

  interface Presenter extends BasePresenter {

    Observable<AgendaItemViewModel> scheduleAppointment(ScheduleParams scheduleParams);

    Observable<AgendaItemViewModel> createAppointment(AppointmentParams appointmentParams);

  }
}
