package br.com.taqtile.android.app.presentation.debugpanel;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelListingViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/5/17.
 */

public class DebugPanelContract {

  interface Presenter extends BasePresenter {
  }
}
