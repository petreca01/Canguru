package br.com.taqtile.android.app.domain.maternity.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDetailsResult;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityDetailsResultToMaternityDetailsViewModelMapper {
  public static MaternityDetailsViewModel map(MaternityDetailsResult maternityDataResult) {
    MaternityViewModel maternity = map(maternityDataResult.getMaternity());
    List<MaternityCommentViewModel> maternityComments =
      MaternityCommentResultToMaternityCommentViewModelMapper.map(maternityDataResult.getComments());

    return new MaternityDetailsViewModel(maternity, maternityComments);
  }

  private static MaternityViewModel map(MaternityDataResult maternityDataResult) {
    List<MaternityDataResult> maternityViewModelList = new ArrayList<MaternityDataResult>();
    maternityViewModelList.add(maternityDataResult);
    List<MaternityViewModel> maternityViewModelsList =
      MaternityDataResultToMaternityViewModelMapper.map(maternityViewModelList);
    return maternityViewModelsList.get(0);
  }
}
