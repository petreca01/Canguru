package br.com.taqtile.android.app.data.channel.remote.mappers;

import br.com.taqtile.android.app.data.channel.mappers.ChannelPostsItemsRemoteResponseToChannelPostsMapper;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostsRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsResult;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import java.util.List;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelPostsRemoteResponseToChannelPostsResultMapper {

  public static ChannelPostsResult perform(ChannelPostsRemoteResponse channelPostsRemoteResponse) {
    ChannelPostsResult channelPostsResult = new ChannelPostsResult();

    channelPostsResult.setPostsInPage(channelPostsRemoteResponse.getPostsInPage());
    channelPostsResult.setTotalPosts(channelPostsRemoteResponse.getTotalPosts());
    ChannelResult channelResult = getChannelResult(channelPostsRemoteResponse);
    channelPostsResult.setChannelResult(channelResult);
    List<ChannelPost> channelPosts = getChannelPosts(channelPostsRemoteResponse);
    channelPostsResult.setChannelPosts(channelPosts);

    return channelPostsResult;
  }

  private static ChannelResult getChannelResult(ChannelPostsRemoteResponse channelPostsRemoteResponse) {
    return ChannelItemRemoteResponseToChannelResultMapper
      .perform(channelPostsRemoteResponse.getChannel());
  }

  private static List<ChannelPost> getChannelPosts(ChannelPostsRemoteResponse channelPostsRemoteResponse) {
    return ChannelPostsItemsRemoteResponseToChannelPostsMapper
      .perform(channelPostsRemoteResponse.getPosts());
  }
}
