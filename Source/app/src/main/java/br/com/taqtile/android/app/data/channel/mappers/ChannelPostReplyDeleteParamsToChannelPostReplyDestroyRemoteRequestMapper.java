package br.com.taqtile.android.app.data.channel.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostReplyDestroyRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyDeleteParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostReplyDeleteParamsToChannelPostReplyDestroyRemoteRequestMapper {

  public static ChannelPostReplyDestroyRemoteRequest perform(
    ChannelPostReplyDeleteParams channelPostReplyDeleteParams) {
    ChannelPostReplyDestroyRemoteRequest channelPostReplyDestroyRemoteRequest =
      new ChannelPostReplyDestroyRemoteRequest();

    channelPostReplyDestroyRemoteRequest.setPostId(channelPostReplyDeleteParams.getPostId());
    channelPostReplyDestroyRemoteRequest.setReplyId(channelPostReplyDeleteParams.getReplyId());

    return channelPostReplyDestroyRemoteRequest;
  }
}
