package br.com.taqtile.android.app.data.facebook.remote.mappers;

import br.com.taqtile.android.app.data.facebook.remote.models.FacebookUserResponse;
import br.com.taqtile.android.app.domain.signin.Models.FacebookUserResult;

/**
 * Created by taqtile on 26/04/17.
 */

public class FacebookUserResponseToFacebookUserResultMapper {

  public static FacebookUserResult perform(FacebookUserResponse facebookUserResponse) {
    return new FacebookUserResult(facebookUserResponse.getName(), facebookUserResponse.getEmail(),
      facebookUserResponse.getToken());
  }
}
