package br.com.taqtile.android.app.presentation.tutorial;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by taqtile on 5/19/17.
 */

public class TutorialViewPagerCell extends FrameLayout {

  @BindView(R.id.component_tutorial_view_pager_cell_icon)
  ImageView icon;
  @BindView(R.id.component_tutorial_view_pager_cell_title)
  CustomTextView title;
  @BindView(R.id.component_tutorial_view_pager_cell_body)
  CustomTextView body;

  public TutorialViewPagerCell(Context context) {
    super(context);
    init(context);
  }

  public TutorialViewPagerCell(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public TutorialViewPagerCell(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context context) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View v = inflater.inflate(R.layout.component_tutorial_view_pager_cell, this, true);
    ButterKnife.bind(this, v);
  }

  public void setData(TutorialViewPagerCellModel model) {
    icon.setImageDrawable(ContextCompat.getDrawable(getContext(), model.icon));
    title.setText(model.title);
    body.setText(model.body);
  }

}
