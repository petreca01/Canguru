package br.com.taqtile.android.app.domain.riskAnalysis.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionsResult;
import br.com.taqtile.android.app.support.GlobalConstants;

/**
 * Created by felipesabino on 5/17/17.
 */

public class RiskAnalysisQuestionsResultToRiskAnalysisQuestionGroupMapper {
  public static List<RiskAnalysisQuestionGroupResult> perform(RiskAnalysisQuestionsResult questions) {
    ArrayList<RiskAnalysisQuestionGroupResult> result = new ArrayList<>();
    if (questions.getAboutYou() != null && questions.getAboutYou().size() > 0) {
      result.add(new RiskAnalysisQuestionGroupResult(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOU));
    }
    if (questions.getAboutYourFamily() != null && questions.getAboutYourFamily().size() > 0) {
      result.add(new RiskAnalysisQuestionGroupResult(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_FAMILY));
    }
    if (questions.getAboutYourHealth() != null && questions.getAboutYourHealth().size() > 0) {
      result.add(new RiskAnalysisQuestionGroupResult(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_HEALTH));
    }
    if (questions.getAboutYourObstetricalPast() != null && questions.getAboutYourObstetricalPast().size() > 0) {
      result.add(new RiskAnalysisQuestionGroupResult(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_OBSTETRICAL_PAST));
    }
    return result;
  }
}
