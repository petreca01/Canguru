package br.com.taqtile.android.app.data.questionnaires.mappers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemUnlockResult;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class QuestionItemRemoteResponseToQuestionItemResultMapper {

  public static QuestionItemResult perform(QuestionItemRemoteResponse response) {

    AnswerItemResult answerItemResult = null;
    if (response.getAnswer() != null) {
      answerItemResult = AnswerItemRemoteResponseToAnswerItemResultMapper
        .perform(response.getAnswer());
    } else {
      answerItemResult = new AnswerItemResult(null, null);
    }

    QuestionItemUnlockResult unlockResult =
      AnswerUnlockTriggerRemoteResponseToQuestionItemUnlockResultMapper
        .perform(response.getAnswerUnlockTrigger());

    List<String> alternatives = null;
    if (response.getAnswers().isJsonArray()) {
      Type listType = new TypeToken<List<String>>() {}.getType();
      alternatives = new Gson().fromJson(response.getAnswers().getAsJsonArray(), listType);
    }

    RangedAnswerResult range = null;
    if (response.getAnswers().isJsonObject() &&
      response.getAnswers().getAsJsonObject().get("min").getAsNumber() != null &&
      response.getAnswers().getAsJsonObject().get("max").getAsNumber() != null) {
      Type rangeType = new TypeToken<RangedAnswerResult>() {}.getType();
      range = new Gson().fromJson(response.getAnswers().getAsJsonObject(), rangeType);
    }

    return new QuestionItemResult(
      response.getId(),
      response.getQuestion(),
      response.getDescription(),
      response.getAnswerType(),
      alternatives,
      range,
      response.getUnlocksQuestions(),
      response.getDependsOnQuestionId(),
      unlockResult,
      answerItemResult);
  }
}
