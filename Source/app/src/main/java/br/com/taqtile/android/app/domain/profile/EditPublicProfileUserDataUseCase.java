package br.com.taqtile.android.app.domain.profile;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.profile.ProfileRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.profile.mapper.UpdateProfileDetailsResultToUserRemoteResponseMapper;
import br.com.taqtile.android.app.domain.profile.mapper.UpdateProfileDetailsResultToViewModelMapper;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;
import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditPublicProfileUserDataUseCase extends BaseUseCase<ProfileDetailsViewModel, EditProfileParams,
  ProfileRepository> {

  private AccountRepository accountRepository;

  public EditPublicProfileUserDataUseCase(ProfileRepository profileRepository, AccountRepository accountRepositoryInjection) {
    super(profileRepository);
    this.accountRepository = accountRepositoryInjection;
  }

  @Override
  public Observable<ProfileDetailsViewModel> execute(EditProfileParams editProfileParams) {
    return getRepository().editProfile(editProfileParams)
      .flatMap(this::updateLocalStorage)
      .map(UpdateProfileDetailsResultToViewModelMapper::perform);
  }

  private Observable<UpdateProfileDetailsResult> updateLocalStorage(UpdateProfileDetailsResult result) {
    return accountRepository.savePublicUserData(UpdateProfileDetailsResultToUserRemoteResponseMapper.perform(result))
      .map(any -> result);
  }

}
