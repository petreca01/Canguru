package br.com.taqtile.android.app.data.channel.remote.mappers;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelItemRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelItemRemoteResponseToChannelResultMapper {

  public static ChannelResult perform(ChannelItemRemoteResponse channel) {
    return new ChannelResult(channel.getId(),
      channel.getName(),
      channel.getAuthor(),
      channel.getDescription(),
      channel.getLogo(),
      channel.getCover(),
      channel.getActive(),
      channel.getTotalPosts(),
      channel.getFollowers(),
      channel.getFollowing());
  }

}
