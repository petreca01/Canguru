package br.com.taqtile.android.app.presentation.manageprofessionals;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.cells.CellFactory;
import br.com.taqtile.android.app.listings.cells.ConnectedProfessionalsCell;
import br.com.taqtile.android.app.listings.viewmodels.ConnectedProfessionalViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 25/04/17.
 */

public class ManageProfessionalsFragment extends AppBaseFragment
  implements ConnectedProfessionalsCell.Listener {

  @BindView(R.id.fragment_manage_professionals_professionals_layout)
  LinearLayout
    manageProfessionalsLayout;

  private NavigationManager navigationManager;
  private ManageProfessionalsContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private List<ConnectedProfessionalViewModel> connectedProfessionalViewModelList;
  private ManagedProfessionalsBundleAccessor managedProfessionalsBundleAccessor;

  public static ManageProfessionalsFragment newInstance() {
    return new ManageProfessionalsFragment();
  }

  public ManageProfessionalsFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);

    View root = inflater.inflate(R.layout.fragment_manage_professionals, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    loadProfessionals();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_manage_professionals_title),
        Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void loadProfessionals() {
    connectedProfessionalViewModelList = managedProfessionalsBundleAccessor.getConnectedProfessionalViewModelList();

    if (connectedProfessionalViewModelList != null
      && !connectedProfessionalViewModelList.isEmpty()) {
      addProfessionals();
    } else {
      placeholder.setPlaceholderType(CustomPlaceholder.CONNECTED_PROFESSIONALS_NOT_FOUND);
      showPlaceholder();
    }
  }

  private void addProfessionals() {
    for (ConnectedProfessionalViewModel connectedProfessionalViewModel : connectedProfessionalViewModelList) {
      addConnectedProfessionalWithPermission(connectedProfessionalViewModel);
    }

    hideLastBottomDivider(manageProfessionalsLayout);
  }

  //Use this method to populate the screen
  private void addConnectedProfessionalWithPermission(
    ConnectedProfessionalViewModel connectedProfessionalViewModel) {

    ConnectedProfessionalsCell connectedProfessionalsCell =
      CellFactory.getConnectedProfessionalsCell(getContext(), connectedProfessionalViewModel);
    connectedProfessionalsCell.showAccessPermission();
    connectedProfessionalsCell.setListener(this);

    manageProfessionalsLayout.addView(connectedProfessionalsCell);
  }

  private void hideLastBottomDivider(LinearLayout layout) {
    int childCount = layout.getChildCount();
    ConnectedProfessionalsCell lastChild =
      (ConnectedProfessionalsCell) layout.getChildAt(childCount - 1);
    lastChild.hideSeparator();
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(ManageProfessionalsContract.Presenter presenter) {
    this.presenter = presenter;
  }


  @Override
  public void onActiveButtonClick(String id) {
    Subscription subscription = presenter.revokeProfessional(id)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        emptyResult -> handleSuccess(id, false, null),
        error -> handleError(error, id)
      );
    compositeSubscription.add(subscription);
  }

  @Override
  public void onInactiveButtonClick(String id) {

    Subscription subscription = presenter.authorizeProfessional(id)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(dateAuthorized -> handleSuccess(id, true, dateAuthorized),
        error -> handleError(error, id)
      );

    compositeSubscription.add(subscription);
  }

  private void handleSuccess(String id, boolean isAuthorized, String dateAuthorized) {
    ConnectedProfessionalsCell connectedProfessionalsCell =
      ManageProfessionalsViewUtility.getConnectedProfessionalsCell(manageProfessionalsLayout, id);

    if (connectedProfessionalsCell != null) {
      toggleButton(connectedProfessionalsCell, isAuthorized, dateAuthorized);
    }
    if (isAuthorized) {
      this.analytics.trackEvent(new AnalyticsEvent(
        getString(R.string.analytics_category_professional),
        getString(R.string.analytics_event_authorization),
        getString(R.string.analytics_label_user_authorized_professional)
      ));
    } else {
      this.analytics.trackEvent(new AnalyticsEvent(
        getString(R.string.analytics_category_professional),
        getString(R.string.analytics_event_authorization),
        getString(R.string.analytics_label_user_revoked_professional)
      ));
    }
  }

  private void handleError(Throwable throwable, String id) {
    // TODO show error
    ConnectedProfessionalsCell connectedProfessionalsCell =
      ManageProfessionalsViewUtility.getConnectedProfessionalsCell(manageProfessionalsLayout, id);

    if (connectedProfessionalsCell != null) {
      connectedProfessionalsCell.rollbackButton();
      showSnackbar(getResources().getString(R.string.error_generic_message), Snackbar.LENGTH_SHORT);
    }
  }

  private void toggleButton(ConnectedProfessionalsCell connectedProfessionalsCell,
                            boolean isAuthorized, String dateAuthorized) {
    if (isAuthorized) {
      connectedProfessionalsCell.setAuthorized(dateAuthorized);
    } else {
      connectedProfessionalsCell.setUnauthorized();
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_manage_professionals_placeholder;
  }

  public void setManagedProfessionalsBundleAccessor(ManagedProfessionalsBundleAccessor
                                                      managedProfessionalsBundleAccessor) {
    this.managedProfessionalsBundleAccessor = managedProfessionalsBundleAccessor;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_manage_professional),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
