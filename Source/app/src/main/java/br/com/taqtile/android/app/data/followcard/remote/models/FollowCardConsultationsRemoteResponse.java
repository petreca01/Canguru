package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardConsultationsRemoteResponse {

  @SerializedName("id")
  private Integer id;

  @SerializedName("doctor_id")
  private Integer doctorId;

  @SerializedName("patient_id")
  private Integer patientId;

  @SerializedName("usuario_id")
  private Integer usuarioId;

  @SerializedName("date")
  private String date;

  @SerializedName("pregnancy_weeks")
  private Integer pregnancyWeeks;

  @SerializedName("pregnancy_days")
  private Integer pregnancyDays;

  @SerializedName("height")
  private Integer height;

  @SerializedName("weight")
  private Integer weight;

  @SerializedName("reason")
  private String reason;

  @SerializedName("blood_pressure_systolic")
  private Integer bloodPressureSystolic;

  @SerializedName("blood_pressure_diastolic")
  private Integer bloodPressureDiastolic;

  @SerializedName("using_ferric_sulfate")
  private Integer usingFerricSulfate;

  @SerializedName("using_folic_acid")
  private Integer usingFolicAcid;

  @SerializedName("uterine_height")
  private Integer uterineHeight;

  @SerializedName("fetal_presentation")
  private String fetalPresentation;

  @SerializedName("fetal_heart_rate")
  private Integer fetalHeartRate;

  @SerializedName("fetal_movement")
  private Integer fetalMovement;

  @SerializedName("edema")
  private Integer edema;

  @SerializedName("toque")
  private String toque;

  @SerializedName("created_at")
  private String createdAt;

  @SerializedName("updated_at")
  private String updatedAt;

  @SerializedName("doctor_name")
  private String doctorName;

  public Integer getId() {
    return id;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public Integer getUsuarioId() {
    return usuarioId;
  }

  public String getDate() {
    return date;
  }

  public Integer getPregnancyWeeks() {
    return pregnancyWeeks;
  }

  public Integer getPregnancyDays() {
    return pregnancyDays;
  }

  public Integer getHeight() {
    return height;
  }

  public Integer getWeight() {
    return weight;
  }

  public String getReason() {
    return reason;
  }

  public Integer getBloodPressureSystolic() {
    return bloodPressureSystolic;
  }

  public Integer getBloodPressureDiastolic() {
    return bloodPressureDiastolic;
  }

  public Integer getUsingFerricSulfate() {
    return usingFerricSulfate;
  }

  public Integer getUsingFolicAcid() {
    return usingFolicAcid;
  }

  public Integer getUterineHeight() {
    return uterineHeight;
  }

  public String getFetalPresentation() {
    return fetalPresentation;
  }

  public Integer getFetalHeartRate() {
    return fetalHeartRate;
  }

  public Integer getFetalMovement() {
    return fetalMovement;
  }

  public Integer getEdema() {
    return edema;
  }

  public String getToque() {
    return toque;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getDoctorName() {
    return doctorName;
  }
}
