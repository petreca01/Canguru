package br.com.taqtile.android.app.presentation.healthcaredetails;

import br.com.taqtile.android.app.domain.account.EditUserDataUseCase;
import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class HealthcareDetailsPresenter implements HealthcareDetailsContract.Presenter {

  private UserDataUseCase userDataUseCase;
  private EditUserDataUseCase editUserDataUseCase;

  public HealthcareDetailsPresenter() {
    this.userDataUseCase = Injection.provideUserDataUseCase();
    this.editUserDataUseCase = Injection.provideEditUserDataUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> checkUserData(String healthOperatorDocumentType) {
    return userDataUseCase.execute(null);
  }

  @Override
  public Observable<UserViewModel> addHealthOperator(EditUserDataParams editUserDataParams) {
    return editUserDataUseCase.execute(editUserDataParams);
  }

}
