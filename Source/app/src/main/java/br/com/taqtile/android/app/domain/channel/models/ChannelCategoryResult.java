package br.com.taqtile.android.app.domain.channel.models;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelCategoryListRemoteResponse;

/**
 * Created by taqtile on 10/07/17.
 */

public class ChannelCategoryResult {

  private ChannelListResult section;

  public ChannelCategoryResult(ChannelListResult section) {
    this.section = section;
  }

  public ChannelListResult getSection(){
    return section;
  }

}
