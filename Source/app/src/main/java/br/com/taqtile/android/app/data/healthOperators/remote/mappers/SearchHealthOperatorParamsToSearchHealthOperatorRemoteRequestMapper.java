package br.com.taqtile.android.app.data.healthOperators.remote.mappers;

import br.com.taqtile.android.app.data.agenda.remote.models.request.RemoveAppointmentRemoteRequest;
import br.com.taqtile.android.app.data.healthOperators.remote.models.request.SearchHealthOperatorRemoteRequest;
import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;

/**
 * Created by taqtile on 4/10/17.
 */

public class SearchHealthOperatorParamsToSearchHealthOperatorRemoteRequestMapper {
  public static SearchHealthOperatorRemoteRequest perform(SearchHealthOperatorParams params) {
    return new SearchHealthOperatorRemoteRequest(params.getSearch());
  }
}
