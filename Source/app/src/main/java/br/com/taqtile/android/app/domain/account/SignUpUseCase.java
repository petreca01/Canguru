package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.models.SignUpParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class SignUpUseCase extends BaseUseCase<EmptyResult, SignUpParams, AccountRepository> {

  public SignUpUseCase(AccountRepository accountRepository) {
    super(accountRepository);
  }

  @Override
  public Observable<EmptyResult> execute(SignUpParams signUpParams) {
    return getRepository().signUp(signUpParams)
      .map(any -> new EmptyResult());
  }
}
