package br.com.taqtile.android.app.domain.channel.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.domain.channel.models.ChannelPostsResult;
import br.com.taqtile.android.app.listings.Constants;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsHeaderImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ChannelDetailsSectionImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SocialFeedChannelImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.presentation.home.mappers.ChannelPostItemResponseToForumPostMapper;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class ChannelPostsResultToListingsViewModelListMapper {
  public static List<ListingsViewModel> perform(ChannelPostsResult channelPostsResult) {
    ChannelDetailsHeaderImplViewModel channelDetailsHeaderImplViewModel =
      ChannelResultToChannelDetailsHeaderImplViewModelMapper.
        perform(channelPostsResult.getChannelResult());

    ChannelDetailsSectionImplViewModel channelDetailsSectionImplViewModel =
      new ChannelDetailsSectionImplViewModel();

    List<SocialFeedViewModel> socialFeedChannelImplViewModels =
      ChannelPostItemResponseToForumPostMapper
        .perform(channelPostsResult.getChannelPosts());

    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.add(channelDetailsHeaderImplViewModel);
    listingsViewModels.add(channelDetailsSectionImplViewModel);
    listingsViewModels.addAll(socialFeedChannelImplViewModels);

    return listingsViewModels;
  }

  public static List<ListingsViewModel> performWithoutHeader(ChannelPostsResult channelPostsResult) {
    List<SocialFeedViewModel> socialFeedChannelImplViewModels =
      ChannelPostItemResponseToForumPostMapper
      .perform(channelPostsResult.getChannelPosts());

    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.addAll(socialFeedChannelImplViewModels);

    return listingsViewModels;
  }
}
