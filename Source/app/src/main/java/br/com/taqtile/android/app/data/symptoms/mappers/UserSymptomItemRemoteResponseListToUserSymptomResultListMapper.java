package br.com.taqtile.android.app.data.symptoms.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.UserSymptomItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.UserSymptomResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class UserSymptomItemRemoteResponseListToUserSymptomResultListMapper {

  public static List<UserSymptomResult> perform(List<UserSymptomItemRemoteResponse> userSymptomItemRemoteResponse) {
    return StreamSupport.stream(userSymptomItemRemoteResponse)
      .map(UserSymptomItemRemoteResponseToUserSymptomResultMapper::perform)
      .collect(Collectors.toList());
  }
}
