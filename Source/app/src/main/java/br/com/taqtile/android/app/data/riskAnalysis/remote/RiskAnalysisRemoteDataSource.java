package br.com.taqtile.android.app.data.riskAnalysis.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.models.AnswerQuestionnairesRemoteRequest;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisAnswerRemoteResponse;
import br.com.taqtile.android.app.data.riskAnalysis.remote.models.RiskAnalysisQuestionsRemoteResponse;
import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class RiskAnalysisRemoteDataSource extends BaseRemoteDataSource {
  private Observable<RiskAnalysisServices> services;

  public RiskAnalysisRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(RiskAnalysisServices.class);
  }

  public Observable<BaseRemoteResponse<RiskAnalysisQuestionsRemoteResponse>> list(
    ViewModeRemoteRequest viewModeRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.list(viewModeRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<RiskAnalysisAnswerRemoteResponse>> answer(
    AnswerQuestionnairesRemoteRequest answerQuestionnairesRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.answer(answerQuestionnairesRemoteRequest.getFormData())));
  }
}
