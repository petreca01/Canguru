package br.com.taqtile.android.app.data.gestationconclusion.remote.models;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerItemRemoteResponse;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionQuestionItemRemoteResponse {

  @SerializedName("id")
  private Integer id;

  @SerializedName("question")
  private String question;

  @SerializedName("answer_type")
  private String answerType;

  @SerializedName("answers")
  private JsonElement answers;

  @SerializedName("next_question")
  private Boolean nextQuestion;

  @SerializedName("next_question_rules")
  private List<Integer> nextQuestionRules;

  @SerializedName("answer")
  private AnswerItemRemoteResponse answer;

  public Integer getId() {
    return id;
  }

  public String getQuestion() {
    return question;
  }

  public String getAnswerType() {
    return answerType;
  }

  public JsonElement getAnswers() {
    return answers;
  }

  public Boolean getNextQuestion() {
    return nextQuestion;
  }

  public List<Integer> getNextQuestionRules() {
    return nextQuestionRules;
  }

  public AnswerItemRemoteResponse getAnswer() {
    return answer;
  }

}
