package br.com.taqtile.android.app.presentation.changepassword;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.support.NavigationHelper;

/**
 * Created by taqtile on 11/15/16.
 */

public class ChangePasswordActivity extends TemplateBackActivity {

  private ChangePasswordFragment fragment;

  @Override
  public Fragment getFragment() {
    ChangePasswordFragment changePasswordFragment = (ChangePasswordFragment) getSupportFragmentManager()
            .findFragmentById(getFragmentContainerId());
    if (changePasswordFragment == null) {
        changePasswordFragment = ChangePasswordFragment.newInstance();
    }

    fragment = changePasswordFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    ChangePasswordPresenter changePasswordPresenter = new ChangePasswordPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(changePasswordPresenter);
    return changePasswordPresenter;
  }

  public static void navigate(Context context){
    Intent intent = new Intent(context, ChangePasswordActivity.class);
    context.startActivity(intent);
  }
}
