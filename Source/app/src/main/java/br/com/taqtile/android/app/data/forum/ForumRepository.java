package br.com.taqtile.android.app.data.forum;

import br.com.taqtile.android.app.data.forum.remote.mappers.ContentSuggestionParamsToContentSuggestionRemoteRequestMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.ContentSuggestionRemoteRequestToContentSuggestionResultMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.CreatePostParamsToRemoteRequestModel;
import br.com.taqtile.android.app.data.forum.remote.mappers.PostParamsToRemoteRequestMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.WarnPostParamsToWarnPostRemoteRequestMapper;
import br.com.taqtile.android.app.data.forum.remote.models.request.WarnPostRemoteRequest;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionParams;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionResult;
import br.com.taqtile.android.app.domain.forum.models.CreatePostParams;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import br.com.taqtile.android.app.domain.forum.models.WarnPostParams;
import java.util.List;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.ForumRemoteDataSource;
import br.com.taqtile.android.app.data.forum.remote.mappers.PostAndRepliesRemoteResponseToPostAndRepliesResultMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.PostItemResponseToPostMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.PostsListParamsToPostRemoteRequestMapper;
import br.com.taqtile.android.app.data.forum.remote.mappers.NewPostRemoteResponseToNewPostResultMapper;
import br.com.taqtile.android.app.domain.forum.models.NewPostResult;
import br.com.taqtile.android.app.domain.forum.models.PostAndRepliesResult;
import br.com.taqtile.android.app.domain.forum.models.PostsListParams;
import br.com.taqtile.android.app.domain.forum.models.PostResult;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class ForumRepository extends BaseRepository<ForumRemoteDataSource, Void> {
  public ForumRepository(ForumRemoteDataSource forumRemoteDataSource) {
    super(forumRemoteDataSource, null);
  }

  public Observable<List<PostResult>> listPosts(PostsListParams postsListParams) {
    return getRemoteDataSource()
      .listPosts(PostsListParamsToPostRemoteRequestMapper.perform(postsListParams))
      .map(PostItemResponseToPostMapper::perform);
  }

  public Observable<NewPostResult> createPost(CreatePostParams createPostParams) {
    return getRemoteDataSource()
      .createPost(CreatePostParamsToRemoteRequestModel.perform(createPostParams))
      .map(NewPostRemoteResponseToNewPostResultMapper::perform);
  }


  public Observable<PostAndRepliesResult> getPostAndReplies(PostParams postsListParams) {
    return getRemoteDataSource()
      .getPostAndReplies(PostParamsToRemoteRequestMapper.perform(postsListParams))
      .map(PostAndRepliesRemoteResponseToPostAndRepliesResultMapper::perform);
  }


  public Observable<PostAndRepliesResult> likePost(PostParams postsListParams) {
    return getRemoteDataSource()
      .likePost(PostParamsToRemoteRequestMapper.perform(postsListParams))
      .map(PostAndRepliesRemoteResponseToPostAndRepliesResultMapper::perform);
  }


  public Observable<PostAndRepliesResult> replyPost(PostParams postsListParams) {
    return getRemoteDataSource()
      .replyPost(PostParamsToRemoteRequestMapper.perform(postsListParams))
      .map(PostAndRepliesRemoteResponseToPostAndRepliesResultMapper::perform);
  }

  public Observable<PostAndRepliesResult> likeReply(PostParams postsListParams) {
    return getRemoteDataSource()
      .likePost(PostParamsToRemoteRequestMapper.perform(postsListParams))
      .map(PostAndRepliesRemoteResponseToPostAndRepliesResultMapper::perform);
  }

  public Observable<String> delete(PostParams postsListParams) {
    return getRemoteDataSource()
      .delete(PostParamsToRemoteRequestMapper.perform(postsListParams))
      .map(BaseRemoteResponse::getResult);
  }

  public Observable<PostAndRepliesResult> warnPost(WarnPostParams warnPostParams) {

    return getRemoteDataSource().warnPost(
      WarnPostParamsToWarnPostRemoteRequestMapper.perform(warnPostParams))
      .map(PostAndRepliesRemoteResponseToPostAndRepliesResultMapper::perform);
  }

  public Observable<ContentSuggestionResult> getContentSuggestion(ContentSuggestionParams contentSuggestionParams){
    return getRemoteDataSource().
      getContentSuggestion(ContentSuggestionParamsToContentSuggestionRemoteRequestMapper.perform(contentSuggestionParams))
      .map(ContentSuggestionRemoteRequestToContentSuggestionResultMapper::perform);
  }

}
