package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.domain.account.mappers.UserDataResultToViewModelMapper;
import br.com.taqtile.android.app.domain.account.mappers.UserResultToUserRemoteResponseMapper;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.domain.profile.mapper.UpdateProfileDetailsResultToUserRemoteResponseMapper;
import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditUserDataUseCase extends BaseUseCase<UserViewModel, EditUserDataParams, AccountRepository> {

  private AccountRepository accountRepository;

  public EditUserDataUseCase(AccountRepository accountRepository, AccountRepository accountRepositoryInjection) {
    super(accountRepository);
    this.accountRepository = accountRepositoryInjection;
  }

  @Override
  public Observable<UserViewModel> execute(EditUserDataParams editUserDataParams) {
    return getRepository().editUserData(editUserDataParams)
      .flatMap(this::updateLocalStorage)
      .map(UserDataResultToViewModelMapper::perform);
  }

  private Observable<UserResult> updateLocalStorage(UserResult result) {
    return accountRepository
      .savePrivateUserData(UserResultToUserRemoteResponseMapper.perform(result))
      .map(any -> result);
  }

}
