package br.com.taqtile.android.app.presentation.gestationpregnancyinformation;

import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model.ExamViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 12/05/17.
 */

public interface GestationPregnancyInformationContract {

  interface Presenter extends BasePresenter {

    Observable<UserViewModel> startOrEditGestation(StartOrEditGestationParams startOrEditGestationParams);

    Observable<ExamViewModel> fetchExam();

  }
}
