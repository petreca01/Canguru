package br.com.taqtile.android.app.data.common.remote;

import retrofit2.adapter.rxjava.Result;
import rx.Observable;

/**
 * Created by taqtile on 3/6/17.
 */

public class ResponseValidator {

    public static <T> Observable<T> convertResultToModel(Result<T> result) {

        if (!result.isError() && result.response().isSuccessful()) {
            return Observable.just(result.response().body());
        }

        RemoteErrorDecisionMaker errorDecisionMaker = new RemoteErrorDecisionMaker(result);
        errorDecisionMaker.decide();

        return Observable.error(errorDecisionMaker.getRemoteError());
    }
}
