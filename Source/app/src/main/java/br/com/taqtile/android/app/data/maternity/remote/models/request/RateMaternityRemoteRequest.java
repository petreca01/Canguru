package br.com.taqtile.android.app.data.maternity.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class RateMaternityRemoteRequest extends BaseRemoteRequest {
    private Integer rate;

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
