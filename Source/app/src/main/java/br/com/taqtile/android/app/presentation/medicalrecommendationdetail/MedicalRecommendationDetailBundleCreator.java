package br.com.taqtile.android.app.presentation.medicalrecommendationdetail;

import android.os.Bundle;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;

import static br.com.taqtile.android.app.presentation.medicalrecommendationdetail.MedicalRecommendationDetailBundleConstants.MEDICAL_RECOMMENDATION_KEY;

/**
 * Created by taqtile on 09/05/17.
 */

public class MedicalRecommendationDetailBundleCreator {

  public static Bundle getBundle(MedicalRecommendationViewModel medicalRecommendationViewModel) {
    Bundle bundle = new Bundle(1);
    bundle.putParcelable(MEDICAL_RECOMMENDATION_KEY, medicalRecommendationViewModel);

    return bundle;
  }
}
