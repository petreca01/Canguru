package br.com.taqtile.android.app.presentation.maternitydetail;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.models.DeleteCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.MaternityParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityDetailsViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 09/05/17.
 */

public interface MaternityDetailContract {
  interface Presenter extends BasePresenter {
    Observable<MaternityDetailsViewModel> getMaternityDetailsUseCase(MaternityParams maternityParams);
    Observable<List<MaternityCommentViewModel>> deleteMaternityCommentUseCase(DeleteCommentParams deleteCommentParams);
  }
}
