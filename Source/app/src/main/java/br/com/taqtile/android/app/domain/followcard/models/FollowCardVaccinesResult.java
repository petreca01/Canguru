package br.com.taqtile.android.app.domain.followcard.models;


/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardVaccinesResult {

  private Integer id;
  private String name;
  private String result;
  private String createdAt;

  public FollowCardVaccinesResult(Integer id, String name, String result, String createdAt) {
    this.id = id;
    this.name = name;
    this.result = result;
    this.createdAt = createdAt;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getResult() {
    return result;
  }

  public String getCreatedAt() {
    return createdAt;
  }

}
