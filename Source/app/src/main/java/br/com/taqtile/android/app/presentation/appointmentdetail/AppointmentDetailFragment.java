package br.com.taqtile.android.app.presentation.appointmentdetail;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.IconButtonWithLabel;
import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.misc.general.CustomFlag;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CustomTextViewWithHTML;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams.DONE;
import static br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams.TODO;
import static br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity.APPOINTMENT_MODEL_KEY;
import static br.com.taqtile.android.app.presentation.appointmenteditschedule.AppointmentEditScheduleActivity.EDITED_APPOINTMENT_MODEL_KEY;

/**
 * Created by taqtile on 26/04/17.
 */

public class AppointmentDetailFragment extends AppBaseFragment implements IconButtonWithLabel.Listener {

  private final static int FIRST_MENU_ITEM_POSITION = 0;
  private static final Integer CUSTOM_APPOINTMENT = 1;

  @BindView(R.id.fragment_appointment_detail_appointment_title)
  CustomTextView appointmentTitle;
  @BindView(R.id.fragment_appointment_detail_appointment_description)
  CustomTextViewWithHTML appointmentDescription;
  @BindView(R.id.fragment_appointment_detail_flag)
  CustomFlag appointmentFlag;
  @BindView(R.id.fragment_appointment_detail_edit_appointment)
  TemplateButton editAppointmentButton;
  @BindView(R.id.fragment_appointment_detail_schedule_appointment)
  ProgressBarButton scheduleAppointmentButton;
  @BindView(R.id.fragment_appointment_detail_observations_container)
  LinearLayout observationsContainer;
  @BindView(R.id.fragment_appointment_detail_observations)
  CustomTextView observations;
  @BindView(R.id.fragment_appointment_detail_attended_check)
  IconButtonWithLabel attendedCheck;

  private NavigationManager navigationManager;
  private AppointmentDetailContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private List<AgendaItemViewModel> agendaItems;
  private AgendaItemViewModel appointment;
  private boolean isChecked;
  private boolean needsReload;
  private boolean showOverflowMenu;

  public static AppointmentDetailFragment newInstance() {
    return new AppointmentDetailFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_appointment_detail, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupListeners();
    setupView();
  }

  private void setupListeners() {
    attendedCheck.setListener(this);
    editAppointmentButton.setOnClickListener(view -> editScheduleAppointment());
    scheduleAppointmentButton.setButtonListener(this::editScheduleAppointment);
  }

  private void setupView() {
    this.agendaItems = new ArrayList<>();
    showOverflowMenu = false;
    setAppointmentData(getActivity().getIntent().getParcelableExtra(APPOINTMENT_MODEL_KEY));
  }

  private void setAppointmentData(AgendaItemViewModel agendaItemViewModel) {
    this.appointment = agendaItemViewModel;

    appointmentTitle.setText(appointment.getName());
    String description;
    if (appointment.thereIsFullDescription() || appointment.thereIsDescription()) {
      description = appointment.thereIsFullDescription() ?
        appointment.getCompleteDescription() : appointment.getDescription();
    } else {
      description = "";
    }

    appointmentDescription.setTextWithHTML(description);
    isChecked = appointment.getDone().equals(1);

    if (appointment.isAppointmentAttended() && appointment.isAppointmentScheduled()) {
      setAppointmentAttendedAndScheduled(appointment.getMarkingDate(),
        appointment.getMarkingObservation());
    } else if (!appointment.isAppointmentAttended() && appointment.isAppointmentScheduled()) {
      setAppointmentNotAttendedAndScheduled(appointment.getMarkingDate(),
        appointment.getMarkingObservation());
    } else if (!appointment.isAppointmentAttended() && appointment.isAppointmentNotScheduled()) {
      setAppointmentNotAttendedAndNotScheduled();
    } else if (appointment.isAppointmentAttended() && appointment.isAppointmentNotScheduled()) {
      setAppointmentAttendedAndNotScheduled();
    }
  }

  private void setAppointmentNotAttendedAndNotScheduled() {
    setFlagForNotScheduled();
    setButtonsToggle();
    setObservations(null);
    showOverflowButton();
    setAttendedNotChecked();
  }

  private void setAppointmentNotAttendedAndScheduled(String date, String observations) {
    setFlagForScheduled(date);
    setButtonsToggle();
    setObservations(observations);
    showOverflowButton();
    isChecked = false;
    setAttendedNotChecked();
  }

  private void setAttendedNotChecked() {
    isChecked = false;
    attendedCheck.setButtonActive(isChecked);
    attendedCheck.setLabel(getResources().getString(R.string.fragment_appointment_detail_mark_as_done));
  }

  private void setAttendedChecked() {
    isChecked = true;
    attendedCheck.setButtonActive(isChecked);
    attendedCheck.setLabel(getResources().getString(R.string.fragment_appointment_detail_marked_as_done));
  }

  private void setAppointmentAttendedAndNotScheduled() {
    setFlagForNotScheduled();
    setButtonsToggle();
    setObservations(null);
    showOverflowButton();
    isChecked = true;
    setAttendedChecked();
  }

  private void setAppointmentAttendedAndScheduled(String date, String observations) {
    setFlagForScheduled(date);
    setButtonsToggle();
    setObservations(observations);
    showOverflowButton();
    isChecked = true;
    setAttendedChecked();
  }

  private void setFlagForScheduled(String date) {
    String formattedDate = DateFormatterHelper.formatDateWithHours(date, getContext());
    appointmentFlag.setFlagText(getString(R.string.fragment_appointment_detail_flag_scheduled, formattedDate));
    appointmentFlag.setFlagType(CustomFlag.FlagType.CallToAction);
  }

  private void setFlagForNotScheduled() {
    appointmentFlag.setFlagText(getString(R.string.fragment_appointment_detail_flag_not_scheduled));
    appointmentFlag.setFlagType(CustomFlag.FlagType.Neutral);
  }

  private void setButtonsToggle() {
    if (this.appointment.getIsAppointment().equals(CUSTOM_APPOINTMENT)) {
      scheduleAppointmentButton.setVisibility(View.GONE);
      editAppointmentButton.setVisibility(View.VISIBLE);
      editAppointmentButton.setEnabled(this.appointment.getDone().equals(TODO));
    } else {
      editAppointmentButton.setVisibility(View.GONE);
      scheduleAppointmentButton.setVisibility(View.VISIBLE);
      if (this.appointment.getDone().equals(TODO)) {
        scheduleAppointmentButton.enableClick();
      } else {
        scheduleAppointmentButton.disableClick();
      }
    }
  }

  private void setObservations(String observations) {
    if (observations == null || observations.isEmpty()) {
      observationsContainer.setVisibility(View.GONE);
    } else {
      observationsContainer.setVisibility(View.VISIBLE);
      this.observations.setText(observations);
    }
  }

  @Override
  public void showLoading() {
    scheduleAppointmentButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    scheduleAppointmentButton.showDefaultState();
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_appointment_detail_title));
      if (showOverflowMenu) {
        customToolbar.inflateOptionsMenu(R.menu.menu_overflow, menuItem -> false);
        customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION).setActionView(getOverflowButton());
      }
    }
  }

  private void showOverflowButton() {
    showOverflowMenu = true;
  }

  private OverflowButton getOverflowButton() {
    OverflowButton overflowButton = new OverflowButton(getContext());
    setupOverflowButton(overflowButton);
    return overflowButton;
  }

  private void setupOverflowButton(OverflowButton overflowButton) {
    List<String> overflowOptions = new ArrayList<>();
    String overFlowText;

    if (appointment.getIsAppointment().equals(0)) {
      overFlowText = getResources().getString(R.string.fragment_appointment_detail_overflow_button_clean_schedule_text);
      overflowButton.onMenuItemSelected(item -> cleanScheduleAppointment());
    } else {
      overFlowText = getResources().getString(R.string.fragment_appointment_detail_overflow_button_delete_text);
      overflowButton.onMenuItemSelected(item -> deleteAppointment());
    }
    overflowOptions.add(overFlowText);

    overflowButton.setMenuList(overflowOptions);
  }

  private void cleanScheduleAppointment() {
    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_appointment_detail_clean_schedule_appointment_dialog_title))
      .setMessage(getString(R.string.fragment_appointment_detail_clean_schedule_appointment_dialog_message))
      .setPositiveButton(getString(R.string.continues), (dialog, which) -> this.performAppointmentScheduleClear())
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
        // do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(getResources().getColor(R.color.color_gray_dark));
  }

  private void performAppointmentScheduleClear() {
    Subscription resetScheduleSubscription = presenter
      .clearScheduleAppointment(new ScheduleParams(appointment.getId(),
        appointment.getIsAppointment(), null, appointment.getObservation(),
        appointment.getName(), appointment.getTypeId()))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onResetScheduleSuccess,
        this::onFailure);
    compositeSubscription.add(resetScheduleSubscription);
  }

  private void onResetScheduleSuccess(AgendaItemViewModel agendaItemViewModel) {
    setAppointmentData(agendaItemViewModel);
    needsReload = true;
  }

  private void deleteAppointment() {
    AlertDialog alert = new AlertDialog.Builder(getContext())
      .setTitle(getString(R.string.fragment_appointment_detail_delete_appointment_dialog_title))
      .setMessage(getString(R.string.fragment_appointment_detail_delete_appointment_dialog_message))
      .setPositiveButton(getString(R.string.delete), (dialog, which) -> this.performAppointmentDelete())
      .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
        // do nothing
      })
      .show();
    Button button = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
    button.setTextColor(getResources().getColor(R.color.color_gray_dark));
  }

  private void performAppointmentDelete() {
    Subscription removeSubscription = presenter
      .removeAppointment(new RemoveAppointmentParams(appointment.getId()))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onRemoveSuccess,
        this::onFailure);
    compositeSubscription.add(removeSubscription);
  }

  private void onRemoveSuccess(String successMessage) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_remove),
      getString(R.string.analytics_label_user_removed_schedule)
    ));
    needsReload = true;
    finishForResult();
  }

  private void attemptCheckClicked(boolean isChecked) {
    Subscription finishSubscription = presenter.finishAppointment(
      new FinishAppointmentParams(appointment.getId(), appointment.getIsAppointment(),
        isChecked ? DONE : TODO))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFinishSuccess,
        this::onFailure);
    compositeSubscription.add(finishSubscription);
  }

  private void onFinishSuccess(AgendaItemViewModel agendaItemViewModel) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_update),
      getString(R.string.analytics_label_user_updated_schedule)
    ));
    needsReload = true;
    setAppointmentData(agendaItemViewModel);
  }

  private void onFailure(Throwable throwable) {
    showSnackBar(throwable.getMessage());
    attendedCheck.setButtonActive(isChecked);

  }

  private void editScheduleAppointment() {
    navigationManager.showAppointmentEditScheduleUI(this::returningFromEditAppointment, appointment);
  }

  private void returningFromEditAppointment(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      setAppointmentData(bundle.getParcelable(EDITED_APPOINTMENT_MODEL_KEY));
      needsReload = true;
    }
  }

  @Override
  public void onActiveButtonClick() {
    attemptCheckClicked(false);
  }

  @Override
  public void onInactiveButtonClick() {
    attemptCheckClicked(true);
  }

  private void finishForResult() {
    Intent intent = new Intent();
    if (needsReload) {
      getActivity().setResult(Activity.RESULT_OK, intent);
    }
    getActivity().finish();
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  public void setPresenter(AppointmentDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_appointment_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
