package br.com.taqtile.android.app.domain.healthOperators.models;

import android.util.Log;

/**
 * Created by taqtile on 4/10/17.
 */

public class HealthOperatorItemResult {
  private static final String TAG = "HOperatorItemResult";
  private Boolean loaded;
  private Integer id;
  private String name;
  private String about;
  private String logo;
  private Boolean healthPlanActive;
  private Boolean active;
  private String documentType;

  public HealthOperatorItemResult(Boolean loaded, Integer id, String name, String about,
                                  String logo, Boolean healthPlanActive, Boolean active, String documentType) {
    this.loaded = loaded;
    this.id = id;
    this.name = name;
    this.about = about;
    this.logo = logo;
    this.healthPlanActive = healthPlanActive;
    this.active = active;
    this.documentType = documentType;
//    Log.e(TAG, "HealthOperatorItemResult(). name: "+name);
//    Log.e(TAG, "HealthOperatorItemResult(). documentType: "+documentType);
  }

  public Boolean getLoaded() {
    return loaded;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAbout() {
    return about;
  }

  public String getLogo() {
    return logo;
  }

  public Boolean getHealthPlanActive() {
    return healthPlanActive;
  }

  public Boolean getActive() {
    return active;
  }

  public String getDocumentType() {
    return documentType;
  }
}
