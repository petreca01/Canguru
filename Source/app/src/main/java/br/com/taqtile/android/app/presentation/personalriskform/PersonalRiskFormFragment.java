package br.com.taqtile.android.app.presentation.personalriskform;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionGroupResult;
import br.com.taqtile.android.app.domain.riskAnalysis.models.RiskAnalysisQuestionResult;
import br.com.taqtile.android.app.listings.adapters.QuestionnaireAdapter;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorBuilder;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorColors;
import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import br.com.taqtile.android.app.listings.questionnairecomponents.AnswerListener;
import br.com.taqtile.android.app.listings.questionnairecomponents.QuestionnaireResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.NoSwipeViewPager;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.questionnaires.mappers.QuestionItemResultListToQuestionViewModelListMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.RISK_ANALYSIS_QUESTIONNAIRE_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Renato on 4/19/17.
 */

public class PersonalRiskFormFragment extends AppBaseFragment implements
  ViewPager.OnPageChangeListener, AnswerListener, ProgressBarButton.ProgressBarButtonButtonListener {

  private final static String FIRST_QUESTION = "0";

  private PersonalRiskFormContract.Presenter presenter;

  private NavigationManager navigationManager;

  private final static int QUESTION_NUMBER_OFFSET = 1;

  @BindView(R.id.fragment_personal_risk_form_view_pager)
  NoSwipeViewPager questionsViewPager;
  QuestionnaireAdapter questionsAdapter;
  HorizontalSelectorBuilder questionMarkerBuilder;

  @BindView(R.id.fragment_question_marker_container)
  LinearLayout questionMarkerContainer;

  @BindView(R.id.fragment_personal_risk_form_category_caption)
  CustomTextView categoryCaption;

  @BindView(R.id.fragment_personal_risk_form_save_button)
  ProgressBarButton saveButton;

  @BindView(R.id.fragment_personal_risk_form_loading)
  ProgressBarLoading loading;

  HorizontalSelectorColors questionsMarkerColors;
  private List<String> questionsNumbers;
  private List<String> answersList;
  private String questionId;
  private int currentQuestionIndex;

  CustomToolbar customToolbar;
  private RiskAnalysisQuestionResult question;
  private String questionnaireName;
  private Subscription saveSubscription;

  public static PersonalRiskFormFragment newInstance() {
    return new PersonalRiskFormFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_personal_risk_form, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    showLoading();
    setupQuestionsAdapter();
    setupButtons();
    setupAnswersList();

    getQuestionnaireType();
  }

  private void setupQuestionsAdapter() {
    questionsAdapter = new QuestionnaireAdapter(this::onAnswerSelected);
    questionsViewPager.setAdapter(questionsAdapter);
    questionsViewPager.addOnPageChangeListener(this);
  }

  private void setupAnswersList() {
    answersList = new ArrayList<>();
  }

  private void setupButtons() {
    saveButton.setButtonListener(this);
  }

  private void saveQuestion() {
    showButtonLoading();

    Pair<AnswerParams, RiskAnalysisQuestionGroupResult> params = new
      Pair<>(new AnswerParams(this.answersList, Integer.valueOf(this.getQuestionId())),
      new RiskAnalysisQuestionGroupResult(this.questionnaireName));
    if (saveSubscription != null) {
      saveSubscription.unsubscribe();
    }
    saveSubscription = presenter.saveAnswers(params)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onSaveQuestionSuccess, this::onSaveQuestionFailure);
    compositeSubscription.add(saveSubscription);
  }

  private void onSaveQuestionSuccess(RiskAnalysisQuestionResult riskAnalysisQuestionResult) {
    answersList.clear();
    hideButtonLoading();
    if (riskAnalysisQuestionResult.getHasNextQuestion()) {
      onFetchQuestionSuccess(riskAnalysisQuestionResult);
      selectQuestionPage(riskAnalysisQuestionResult.getCurrentQuestionIndex());
    } else {
      getActivity().finish();
    }
  }

  private void getQuestionnaireType() {
    Bundle bundle = getActivity().getIntent().getExtras();
    this.questionnaireName = bundle.getString(RISK_ANALYSIS_QUESTIONNAIRE_KEY, "");
    setCategoryCaption(this.questionnaireName);
    fetchQuestionnaire();
  }

  private void setCategoryCaption(String categoryCaption) {
    this.categoryCaption.setText(categoryCaption);
  }

  private void fetchQuestionnaire() {
    showLoading();
    Subscription subscription = presenter.fetchFirstRiskAnalysisQuestionnaire(
      new RiskAnalysisQuestionGroupResult(questionnaireName))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchQuestionSuccess, this::onFetchQuestionsFailure);
    compositeSubscription.add(subscription);
  }

  private void onFetchQuestionSuccess(RiskAnalysisQuestionResult riskAnalysisQuestionResult) {
    hideLoading();
    this.question = riskAnalysisQuestionResult;

    if (this.question != null) {
      List<QuestionViewModel> currentQuestion = new ArrayList<>();
      try {
        currentQuestion.addAll(QuestionItemResultListToQuestionViewModelListMapper
          .perform(new ArrayList<QuestionItemResult>() {{
            add(question.getQuestion());
          }}));
      } catch (DomainError domainError) {
        domainError.printStackTrace();
      }
      questionsAdapter.addQuestionViewModels(currentQuestion);
      questionsAdapter.notifyDataSetChanged();
      setupQuestionsMarker(this.question.getTotalQuestionsForGroup());
      setButtonEnabledOrDisabled(getQuestionResult());
    }
  }

  private void onFetchQuestionsFailure(Throwable throwable){
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupQuestionsMarker(Integer totalQuestionsForGroup) {
    if (this.questionsNumbers == null) {
      setupQuestionsMarkerColors();

      questionsNumbers = new ArrayList<>();
      for (int i = 1; i <= totalQuestionsForGroup; i++) {
        questionsNumbers.add(String.valueOf(i));
      }

      questionMarkerBuilder = new HorizontalSelectorBuilder(questionMarkerContainer, getContext());
      addQuestions(questionsNumbers,
        questionsMarkerColors.getTextActiveColor(),
        questionsMarkerColors.getTextInactiveColor(),
        questionsMarkerColors.getBackgroundSelectedColor());

      questionMarkerBuilder.setSelectedCell(questionsNumbers.get(0));
      questionsViewPager.setOffscreenPageLimit(questionsNumbers.size());
    }
  }

  private void addQuestions(List<String> questions, int textActiveColor, int textInactiveColor,
                            int backgroundSelectedColor) {
    for (String selectorText : questions) {
      questionMarkerBuilder.addSelectorCell(selectorText,
        null,
        textActiveColor,
        textInactiveColor,
        backgroundSelectedColor);
    }
  }

  private void setupQuestionsMarkerColors() {
    int textActiveColor = ContextCompat.getColor(getContext(), R.color.color_brand_orange);
    int textInactiveColor = ContextCompat.getColor(getContext(), R.color.color_questionnaire_number_default_color);
    int backgroundSelectedColor = Color.TRANSPARENT;
    questionsMarkerColors = new HorizontalSelectorColors(textActiveColor, textInactiveColor,
      backgroundSelectedColor);
  }

  private void onSaveQuestionFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    hideButtonLoading();
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  public void setPresenter(@NonNull PersonalRiskFormContract.Presenter presenter) {
    this.presenter = checkNotNull(presenter);
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_close, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_personal_risk_form_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  //Use this method to set the specific question page you want to go to
  private void selectQuestionPage(Integer nextQuestion) {
    if (!String.valueOf(nextQuestion).equals(FIRST_QUESTION)) {
      currentQuestionIndex = nextQuestion;
      questionsViewPager.setCurrentItem(currentQuestionIndex + QUESTION_NUMBER_OFFSET);
      questionMarkerBuilder.paintPreviousCells(
        String.valueOf(currentQuestionIndex + QUESTION_NUMBER_OFFSET));
    }
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
  }

  @Override
  public void onPageSelected(int position) {
    HorizontalSelectorCell selectedQuestion = (HorizontalSelectorCell) questionMarkerContainer.getChildAt(currentQuestionIndex);
    selectedQuestion.setTextColor(questionsMarkerColors.getTextActiveColor());

    questionId = getQuestionId();
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  private String getQuestionId() {
    QuestionnaireResult answer = (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
    return answer.getQuestionId();
  }

  private QuestionnaireResult getQuestionResult() {
    QuestionnaireResult answer = (QuestionnaireResult) questionsViewPager.getChildAt(questionsViewPager.getCurrentItem());
    return answer;
  }

  private void setButtonEnabledOrDisabled(QuestionnaireResult questionnaireResult) {
    if (questionnaireResult == null) {
      saveButton.disableClick();
      return;
    }
    if (!questionnaireResult.getResult().isEmpty() &&
      !questionnaireResult.getResult().get(0).isEmpty()) {
      saveButton.enableClick();
    } else {
      saveButton.disableClick();
    }
  }

  @Override
  public void onPageScrollStateChanged(int state) {
  }

  @Override
  public void onAnswerSelected(QuestionnaireResult result) {
    if (saveButton.isLoading()) {
      return;
    }
    if (result.isResultValid()) {
      saveButton.enableClick();
      answersList.clear();
      answersList.addAll(result.getResult());
    } else {
      saveButton.disableClick();
    }
  }

  //end region

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
  }

  private void showButtonLoading() {
    saveButton.showLoadingState();
  }

  private void hideButtonLoading() {
    saveButton.showDefaultState();
    setButtonEnabledOrDisabled(getQuestionResult());
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_personal_risk_form_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    hidePlaceholder();
    fetchQuestionnaire();
  }

  @Override
  public void onButtonClicked() {
    saveQuestion();
  }
}
