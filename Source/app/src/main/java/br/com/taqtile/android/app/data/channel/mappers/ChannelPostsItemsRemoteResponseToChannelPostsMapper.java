package br.com.taqtile.android.app.data.channel.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostItemRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 3/21/17.
 */

public class ChannelPostsItemsRemoteResponseToChannelPostsMapper {
    public static List<ChannelPost> perform(List<ChannelPostItemRemoteResponse> response) {

        if (response == null) {
            return new ArrayList<>();
        }

        return StreamSupport.stream(response)
                .filter(ChannelPostItemRemoteResponseToChannelPostMapper::isValidChannelPost)
                .map(ChannelPostItemRemoteResponseToChannelPostMapper::perform)
                .collect(Collectors.toList());
    }
}
