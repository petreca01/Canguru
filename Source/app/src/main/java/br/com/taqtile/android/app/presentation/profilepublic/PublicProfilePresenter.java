package br.com.taqtile.android.app.presentation.profilepublic;

import java.util.List;

import br.com.taqtile.android.app.domain.forum.LikeForumPostItemUseCase;
import br.com.taqtile.android.app.domain.forum.models.PostParams;
import br.com.taqtile.android.app.domain.profile.EditPublicProfileUserDataUseCase;
import br.com.taqtile.android.app.domain.profile.PublicProfileUserDataUseCase;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;
import br.com.taqtile.android.app.domain.profile.models.UserProfileParams;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.profilepublic.viewmodel.PublicProfileViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 20/04/17.
 */

public class PublicProfilePresenter implements PublicProfileContract.Presenter {

  private PublicProfileUserDataUseCase publicProfileUserDataUseCase;
  private EditPublicProfileUserDataUseCase editPublicProfileUserDataUseCase;
  private LikeForumPostItemUseCase likeForumPostItemUseCase;

  public PublicProfilePresenter() {
    this.publicProfileUserDataUseCase = Injection.providePublicProfileUserDataUseCase();
    this.editPublicProfileUserDataUseCase = Injection.provideEditPublicProfileUserDataUseCase();
    this.likeForumPostItemUseCase = Injection.provideLikeForumPostReplyUseCase();

  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<PublicProfileViewModel> fetchPublicProfileData(Integer userId) {
    return publicProfileUserDataUseCase.execute(new UserProfileParams(userId));
  }

  @Override
  public Observable<ProfileDetailsViewModel> updatePublicProfileData(
    EditProfileParams editProfileParams) {
    return editPublicProfileUserDataUseCase.execute(editProfileParams);
  }

  @Override
  public Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked) {
    return likeForumPostItemUseCase.execute(new PostParams(postId, replyId, null, alreadyLiked ? 1 : 0));
  }
}
