package br.com.taqtile.android.app.presentation.profilepublic;

import java.util.List;

import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.profilepublic.viewmodel.PublicProfileViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 20/04/17.
 */

public interface PublicProfileContract {
  interface Presenter extends BasePresenter {
    Observable<PublicProfileViewModel> fetchPublicProfileData(Integer userId);

    Observable<ProfileDetailsViewModel> updatePublicProfileData(EditProfileParams editProfileParams);

    Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked);
  }
}
