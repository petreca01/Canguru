package br.com.taqtile.android.app.data.clinicalcondition.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ClinicalConditionItemRemoteResponse;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ReportClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.UserClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.ReportSymptomRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface ClinicalConditionsServices {
    @POST("conditions")
    Observable<Result<BaseRemoteResponse<List<ClinicalConditionItemRemoteResponse>>>> list();

    @FormUrlEncoded
    @POST("user/condition")
    Observable<Result<BaseRemoteResponse<ReportClinicalConditionRemoteResponse>>> reportCondition(@FieldMap Map<String, String> reportConditionRequestModel);

    @POST("user/conditions")
    Observable<Result<BaseRemoteResponse<List<UserClinicalConditionRemoteResponse>>>> listUserConditions();
}
