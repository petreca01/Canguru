package br.com.taqtile.android.app.domain.common.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class ViewModeParams {
  private String view;
  private String email;

  private static final String JSON_VIEW_TYPE = "json";
  private static final String EMAIL_VIEW_TYPE = "email";

  public ViewModeParams() {
    this.view = JSON_VIEW_TYPE;
  }

  public ViewModeParams(String email) {
    this.view = EMAIL_VIEW_TYPE;
    this.email = email;
  }

  public ViewModeParams(String view, String email) {
    this.view = view;
    this.email = email;
  }

  public String getView() {
    return view;
  }

  public String getEmail() {
    return email;
  }

  public static String getJsonViewType() {
    return JSON_VIEW_TYPE;
  }

  public static String getEmailViewType() {
    return EMAIL_VIEW_TYPE;
  }
}
