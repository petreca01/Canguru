package br.com.taqtile.android.app.data.professional.remote.mappers;

import br.com.taqtile.android.app.data.professional.remote.models.request.ProfessionalAuthorizationRemoteRequest;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalAuthorizationParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfessionalAuthorizationParamsToProfessionalAuthorizationRemoteRequestMapper {

  public static ProfessionalAuthorizationRemoteRequest perform(
    ProfessionalAuthorizationParams professionalAuthorizationParams) {
    return new ProfessionalAuthorizationRemoteRequest(
      professionalAuthorizationParams.getProfessionalId(),
      professionalAuthorizationParams.getQuestionnaireOnly() ? 1 : 0);
  }
}
