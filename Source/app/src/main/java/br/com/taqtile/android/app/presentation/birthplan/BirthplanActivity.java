package br.com.taqtile.android.app.presentation.birthplan;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.presentation.maternitycitysearch.MaternityCitySearchActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_REQUEST_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.MATERNITY_CITY_SEARCH_REQUEST_CODE;

/**
 * Created by taqtile on 10/05/17.
 */

public class BirthplanActivity extends TemplateBackActivity {

  private BirthplanFragment fragment;

  @Override
  public Fragment getFragment() {
    BirthplanFragment birthplanFragment = (BirthplanFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (birthplanFragment == null){
      birthplanFragment = BirthplanFragment.newInstance();
    }
    fragment = birthplanFragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    BirthplanPresenter birthplanPresenter = new BirthplanPresenter();
    fragment.setPresenter(birthplanPresenter);
    fragment.setNavigationManager(new NavigationHelper(this));
    return birthplanPresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {

    Intent intent = new Intent(context, BirthplanActivity.class);
    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        BIRTH_PLAN_REQUEST_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        BIRTH_PLAN_REQUEST_CODE);
    }
  }

}
