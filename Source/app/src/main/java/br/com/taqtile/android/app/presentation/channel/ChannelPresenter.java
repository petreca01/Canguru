package br.com.taqtile.android.app.presentation.channel;

import br.com.taqtile.android.app.domain.channel.ChannelListUseCase;
import br.com.taqtile.android.app.domain.channel.mappers.ChannelResultListToChannelListingViewModelListMapper;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 3/22/17.
 */

public class ChannelPresenter implements ChannelContract.Presenter {

  private ChannelListUseCase channelListUseCase;

  public ChannelPresenter() {
    channelListUseCase = Injection.provideChannelListUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<SearchListingViewModel>> fetchChannelList() {
    return channelListUseCase.execute(null)
      .map(ChannelResultListToChannelListingViewModelListMapper::map);
  }
}
