package br.com.taqtile.android.app.domain.clinicalConditions;

import br.com.taqtile.android.app.data.clinicalcondition.ClinicalConditionRepository;
import br.com.taqtile.android.app.domain.clinicalConditions.mappers.ReportClinicalConditionResultToViewModelMapper;
import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.presentation.condition.models.ReportClinicalConditionViewModel;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ReportConditionUseCase
  extends BaseUseCase<ReportClinicalConditionViewModel, ReportConditionParams, ClinicalConditionRepository> {

  public ReportConditionUseCase(ClinicalConditionRepository clinicalConditionRepository) {
    super(clinicalConditionRepository);
  }

  @Override
  public Observable<ReportClinicalConditionViewModel> execute(ReportConditionParams reportConditionParams) {
    return getRepository().reportClinicalCondition(reportConditionParams)
      .map(ReportClinicalConditionResultToViewModelMapper::perform);
  }
}
