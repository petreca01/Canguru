package br.com.taqtile.android.app.data.clinicalcondition.remote;

import br.com.taqtile.android.app.data.clinicalcondition.remote.models.request.ReportConditionRemoteRequest;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ClinicalConditionItemRemoteResponse;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.ReportClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.data.clinicalcondition.remote.models.response.UserClinicalConditionRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class ClinicalConditionRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ClinicalConditionsServices> services;

  public ClinicalConditionRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(ClinicalConditionsServices.class);
  }

  public Observable<BaseRemoteResponse<List<ClinicalConditionItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(ClinicalConditionsServices::list));
  }

  public Observable<BaseRemoteResponse<ReportClinicalConditionRemoteResponse>> reportClinicalCondition(
    ReportConditionRemoteRequest reportSymptomRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.reportCondition(reportSymptomRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<List<UserClinicalConditionRemoteResponse>>> listUserClinicalCondition() {
    return performRequest(services.flatMap(ClinicalConditionsServices::listUserConditions));
  }
}
