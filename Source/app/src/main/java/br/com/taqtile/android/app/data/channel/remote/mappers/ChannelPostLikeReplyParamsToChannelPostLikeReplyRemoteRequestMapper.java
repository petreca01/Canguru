package br.com.taqtile.android.app.data.channel.remote.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostLikeReplyRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostLikeReplyParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostLikeReplyParamsToChannelPostLikeReplyRemoteRequestMapper {

  public static ChannelPostLikeReplyRemoteRequest perform(ChannelPostLikeReplyParams channelPostLikeReplyParams) {
    ChannelPostLikeReplyRemoteRequest channelPostLikeReplyRemoteRequest = new ChannelPostLikeReplyRemoteRequest();

    channelPostLikeReplyRemoteRequest.setReplyId(channelPostLikeReplyParams.getReplyId());
    channelPostLikeReplyRemoteRequest.setPostId(channelPostLikeReplyParams.getPostId());

    if (channelPostLikeReplyParams.isLike()) {
      channelPostLikeReplyRemoteRequest.setLike(1);
    } else {
      channelPostLikeReplyRemoteRequest.setDislike(1);
    }

    return channelPostLikeReplyRemoteRequest;
  }
}
