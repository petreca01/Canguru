package br.com.taqtile.android.app.data.common.remote.models;

import java.util.Map;

/**
 * Created by taqtile on 3/17/17.
 */

public interface RemoteRequestModel {
  Map<String, String> getFormData();
}
