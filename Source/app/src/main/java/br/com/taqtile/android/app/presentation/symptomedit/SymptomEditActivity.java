package br.com.taqtile.android.app.presentation.symptomedit;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.symptomsguide.SymptomsGuideActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.SYMPTOM_DETAILS_REQUEST_CODE;

/**
 * Created by taqtile on 27/04/17.
 */

public class SymptomEditActivity extends TemplateBackActivity {

  private SymptomEditFragment fragment;

  @Override
  public Fragment getFragment() {
    SymptomEditFragment symptomEditFragment =
      (SymptomEditFragment) getSupportFragmentManager()
        .findFragmentById(getFragmentContainerId());
    if (symptomEditFragment == null) {
      symptomEditFragment = SymptomEditFragment.newInstance();
    }
    fragment = symptomEditFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    SymptomEditPresenter symptomEditPresenter =
      new SymptomEditPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(symptomEditPresenter);

    return symptomEditPresenter;
  }

  public static void navigate(Context context, CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, SymptomEditActivity.class);

    ((SymptomsGuideActivity) context).setResultListener(listener,
      SYMPTOM_DETAILS_REQUEST_CODE);
    ((SymptomsGuideActivity) context).startActivityForResult(intent,
      SYMPTOM_DETAILS_REQUEST_CODE);
  }
}
