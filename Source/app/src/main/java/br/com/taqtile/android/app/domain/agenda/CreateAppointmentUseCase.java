package br.com.taqtile.android.app.domain.agenda;

import br.com.taqtile.android.app.data.agenda.AgendaRepository;
import br.com.taqtile.android.app.domain.agenda.mappers.AgendaItemResultToViewModelMapper;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class CreateAppointmentUseCase extends BaseUseCase<AgendaItemViewModel, AppointmentParams,AgendaRepository> {

  public CreateAppointmentUseCase(AgendaRepository agendaRepository) {
    super(agendaRepository);
  }

  @Override
  public Observable<AgendaItemViewModel> execute(AppointmentParams appointmentParams) {
    return getRepository().createAppointment(appointmentParams)
      .map(AgendaItemResultToViewModelMapper::map);
  }
}
