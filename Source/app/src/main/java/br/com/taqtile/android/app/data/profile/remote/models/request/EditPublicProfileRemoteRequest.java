package br.com.taqtile.android.app.data.profile.remote.models.request;

import java.io.File;

import br.com.taqtile.android.app.data.common.remote.RequestFile;
import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.MultiPartBaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class EditPublicProfileRemoteRequest extends MultiPartBaseRemoteRequest {
  @RequestName("nome") private String name;

  @RequestName("baby_name") private String babyName;

  @RequestName("about") private String about;

  @RequestName("conclusion_date") private String conclusionDate;

  @RequestName("conclusion_method") private String conclusionMethod;

  @RequestName("conclusion_text") private String conclusionText;

  @RequestFile("profile_picture") private File profilePicture;

  @RequestFile("cover_picture") private File coverPicture;

  @RequestName("delete_cover_picture") private String deleteCoverPicture;

  @RequestName("delete_profile_picture") private String deleteProfilePicture;


  public EditPublicProfileRemoteRequest(String name, String babyName, String about,
                                        String conclusionDate, String conclusionMethod,
                                        String conclusionText, File profilePicture,
                                        File coverPicture, String deleteCoverPicture,
                                        String deleteProfilePicture) {
    this.name = name;
    this.babyName = babyName;
    this.about = about;
    this.conclusionDate = conclusionDate;
    this.conclusionMethod = conclusionMethod;
    this.conclusionText = conclusionText;
    this.profilePicture = profilePicture;
    this.coverPicture = coverPicture;
    this.deleteCoverPicture = deleteCoverPicture;
    this.deleteProfilePicture = deleteProfilePicture;
  }
}
