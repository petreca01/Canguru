package br.com.taqtile.android.app.data.common.remote.models;

import com.google.common.io.Files;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.data.common.remote.RequestFile;
import br.com.taqtile.android.app.data.common.remote.RequestName;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by taqtile on 5/2/17.
 */

public class MultiPartBaseRemoteRequest extends BaseRemoteRequest {

  @Override
  protected String getFormDataKey(Field property) {

    if (property == null) {
      return null;
    }

    Annotation annotation = property.getAnnotation(RequestName.class);
    if (annotation == null && checkIfIsAFile(property)) {
      return null;
    }

    RequestName customName = (RequestName) annotation;
    return customName != null ? customName.value() : property.getName();
  }

  private boolean checkIfIsAFile(Field property) {
    Annotation annotation = property.getAnnotation(RequestFile.class);
    return annotation != null;
  }

  public List<MultipartBody.Part> getFiles() {
    List<MultipartBody.Part> parts = new ArrayList<>();
    Field[] properties = this.getClass().getDeclaredFields();
    for (Field property : properties) {
      property.setAccessible(true);
      String key = getFileDataKey(property);
      File value = getFileDataValue(property);

      if (key == null || value == null || key.isEmpty() || !value.exists()) {
        continue;
      }

      parts.add(prepareFilePart(key, value));
    }
    return parts;
  }

  private String getFileDataKey(Field property) {

    if (property == null) {
      return null;
    }

    Annotation annotation = property.getAnnotation(RequestFile.class);
    RequestFile customName = (RequestFile) annotation;
    return customName != null ? customName.value() : property.getName();
  }

  private File getFileDataValue(Field property) {
    if (property == null || !property.getType().equals(File.class)) {
      return null;
    }

    File formDataValue;
    try {
      formDataValue = (File) property.get(this);
    } catch (Exception e) {
      formDataValue = null;
    }
    return formDataValue;
  }

  private static MultipartBody.Part prepareFilePart(String partName, File file) {
    if (file == null) {
      return null;
    }
    RequestBody requestFile =
      RequestBody.create(
        MediaType.parse(Files.getFileExtension(file.getName())), file);

    return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
  }

}
