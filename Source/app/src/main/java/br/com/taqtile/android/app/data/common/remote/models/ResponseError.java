package br.com.taqtile.android.app.data.common.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/6/17.
 */

public class ResponseError {

   @SerializedName("error")
    private String errorMessage;

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
