package br.com.taqtile.android.app.data.medicalRecommendations;

import java.util.List;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.medicalRecommendations.mappers.MedicalRecommendationItemRemoteResponseListToMedicalRecommendationResultListMapper;
import br.com.taqtile.android.app.data.medicalRecommendations.mappers.MedicalRecommendationItemRemoteResponseToMedicalRecommendationResultMapper;
import br.com.taqtile.android.app.data.medicalRecommendations.remote.MedicalRecommendationsDataSource;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationParams;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationResult;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class MedicalRecommendationsRepository extends BaseRepository<MedicalRecommendationsDataSource, Void> {

  public MedicalRecommendationsRepository(MedicalRecommendationsDataSource medicalRecommendationsDataSource) {
    super(medicalRecommendationsDataSource, null);
  }

  public Observable<List<MedicalRecommendationResult>> list() {
    return getRemoteDataSource().list()
      .map(result ->
        MedicalRecommendationItemRemoteResponseListToMedicalRecommendationResultListMapper
          .perform(result.getResult()));
  }

  public Observable<MedicalRecommendationResult> markAsRead(MedicalRecommendationParams params) {
    return getRemoteDataSource().markAsRead(params.getId())
      .map(result -> MedicalRecommendationItemRemoteResponseToMedicalRecommendationResultMapper
        .perform(result.getResult()));
  }
}
