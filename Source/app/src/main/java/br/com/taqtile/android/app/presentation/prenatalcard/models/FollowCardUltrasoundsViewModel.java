package br.com.taqtile.android.app.presentation.prenatalcard.models;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardUltrasoundsViewModel {

  private Integer id;
  private Integer consultationId;
  private String date;
  private Integer periodPregnancyWeeks;
  private Integer periodPregnancyDays;
  private Integer ultrasoundPregnancyWeeks;
  private Integer ultrasoundPregnancyDays;
  private Integer fetalWeight;
  private String placenta;
  private String liquid;
  private String pbf;
  private String createdAt;
  private Integer doctorId;
  private String doctorName;

  public FollowCardUltrasoundsViewModel(Integer id, Integer consultationId, String date,
                                     Integer periodPregnancyWeeks, Integer periodPregnancyDays,
                                     Integer ultrasoundPregnancyWeeks,
                                     Integer ultrasoundPregnancyDays, Integer fetalWeight,
                                     String placenta, String liquid, String pbf, String createdAt,
                                     Integer doctorId, String doctorName) {
    this.id = id;
    this.consultationId = consultationId;
    this.date = date;
    this.periodPregnancyWeeks = periodPregnancyWeeks;
    this.periodPregnancyDays = periodPregnancyDays;
    this.ultrasoundPregnancyWeeks = ultrasoundPregnancyWeeks;
    this.ultrasoundPregnancyDays = ultrasoundPregnancyDays;
    this.fetalWeight = fetalWeight;
    this.placenta = placenta;
    this.liquid = liquid;
    this.pbf = pbf;
    this.createdAt = createdAt;
    this.doctorId = doctorId;
    this.doctorName = doctorName;
  }

  public Integer getId() {
    return id;
  }

  public Integer getConsultationId() {
    return consultationId;
  }

  public String getDate() {
    return date;
  }

  public Integer getPeriodPregnancyWeeks() {
    return periodPregnancyWeeks;
  }

  public Integer getPeriodPregnancyDays() {
    return periodPregnancyDays;
  }

  public Integer getUltrasoundPregnancyWeeks() {
    return ultrasoundPregnancyWeeks;
  }

  public Integer getUltrasoundPregnancyDays() {
    return ultrasoundPregnancyDays;
  }

  public Integer getFetalWeight() {
    return fetalWeight;
  }

  public String getPlacenta() {
    return placenta;
  }

  public String getLiquid() {
    return liquid;
  }

  public String getPbf() {
    return pbf;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public Integer getDoctorId() {
    return doctorId;
  }

  public String getDoctorName() {
    return doctorName;
  }

}
