package br.com.taqtile.android.app.domain.maternity.models;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityCommentResult {

  private Integer id;

  private Integer userId;

  private Integer hospitalId;

  private String comment;

  private Integer active;

  private String createdAt;

  private String updatedAt;

  private String username;

  private String userPicture;

  public MaternityCommentResult(Integer id, Integer userId, Integer hospitalId, String comment,
    Integer active, String createdAt, String updatedAt, String username, String userPicture) {
    this.id = id;
    this.userId = userId;
    this.hospitalId = hospitalId;
    this.comment = comment;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.username = username;
    this.userPicture = userPicture;
  }

  public Integer getId() {
    return id;
  }

  public Integer getUserId() {
    return userId;
  }

  public Integer getHospitalId() {
    return hospitalId;
  }

  public String getComment() {
    return comment;
  }

  public Integer getActive() {
    return active;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public String getUsername() {
    return username;
  }

  public String getUserPicture() {
    return userPicture;
  }
}
