package br.com.taqtile.android.app.presentation.prenatalcard.models;

/**
 * Created by taqtile on 5/23/17.
 */

public class GestationHistoryViewModel {

  private Integer pregnancy;
  private Integer normalDelivery;
  private Integer caesarean;
  private Integer miscarriage;

  public GestationHistoryViewModel(Integer pregnancy, Integer normalDelivery, Integer caesarean,
                                   Integer miscarriage) {
    this.pregnancy = pregnancy;
    this.normalDelivery = normalDelivery;
    this.caesarean = caesarean;
    this.miscarriage = miscarriage;
  }

  public Integer getPregnancy() {
    return pregnancy;
  }

  public Integer getNormalDelivery() {
    return normalDelivery;
  }

  public Integer getCaesarean() {
    return caesarean;
  }

  public Integer getMiscarriage() {
    return miscarriage;
  }

}
