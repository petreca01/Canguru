package br.com.taqtile.android.app.data.notification.remote.models.request;

import com.google.gson.annotations.SerializedName;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class SetVisualizedNotificationRemoteRequest extends BaseRemoteRequest {

    @SerializedName("id")
    private String id;

    public SetVisualizedNotificationRemoteRequest(String id) {
      this.id = id;
    }

}
