package br.com.taqtile.android.app.presentation.prenatalcard;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.signup.SignUpPresenter;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 5/22/17.
 */

public class PrenatalCardActivity extends TemplateBackActivity {

  PrenatalCardFragment fragment;

  @Override
  public Fragment getFragment() {
    PrenatalCardFragment prenatalCardFragment = (PrenatalCardFragment)
      getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (prenatalCardFragment == null) {
      prenatalCardFragment = PrenatalCardFragment.newInstance();
    }
    fragment = prenatalCardFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    PrenatalCardPresenter presenter = new PrenatalCardPresenter();
    fragment.setPresenter(presenter);
    NavigationHelper navigationHelper = new NavigationHelper(this);
    fragment.setNavigationManager(navigationHelper);
    return presenter;
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, PrenatalCardActivity.class);
    context.startActivity(intent);
  }

}
