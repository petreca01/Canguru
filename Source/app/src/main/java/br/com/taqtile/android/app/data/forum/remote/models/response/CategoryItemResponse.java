package br.com.taqtile.android.app.data.forum.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/17/17.
 */

public class CategoryItemResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
