package br.com.taqtile.android.app.domain.symptoms.models;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class SubSymptomResult {

  private Integer id;
  private Integer parentId;
  private String name;
  private String ciap;
  private String condition;
  private String conditionTrigger;
  private String explanation;
  private String howToAvoid;
  private String whenToWorry;
  private boolean dangerous;

  public SubSymptomResult(Integer id, Integer parentId, String name, String ciap, String condition,
                          String conditionTrigger, String explanation, String howToAvoid,
                          String whenToWorry, boolean dangerous) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
    this.ciap = ciap;
    this.condition = condition;
    this.conditionTrigger = conditionTrigger;
    this.explanation = explanation;
    this.howToAvoid = howToAvoid;
    this.whenToWorry = whenToWorry;
    this.dangerous = dangerous;
  }

  public Integer getId() {
    return id;
  }

  public Integer getParentId() {
    return parentId;
  }

  public String getName() {
    return name;
  }

  public String getCiap() {
    return ciap;
  }

  public String getCondition() {
    return condition;
  }

  public String getConditionTrigger() {
    return conditionTrigger;
  }

  public String getExplanation() {
    return explanation;
  }

  public String getHowToAvoid() {
    return howToAvoid;
  }

  public String getWhenToWorry() {
    return whenToWorry;
  }

  public boolean isDangerous() {
    return dangerous;
  }
}
