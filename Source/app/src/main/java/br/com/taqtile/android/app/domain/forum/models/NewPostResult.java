package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 4/3/17.
 */

public class NewPostResult {
  private Integer id;
  private Integer operatorId;
  private Integer userId;
  private String title;
  private String content;
  private Integer active;
  private String createdAt;
  private String updatedAt;

  public NewPostResult(Integer id, Integer operatorId, Integer userId, String title, String content, Integer active, String createdAt, String updatedAt) {
    this.id = id;
    this.operatorId = operatorId;
    this.userId = userId;
    this.title = title;
    this.content = content;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public Integer getId() {
    return id;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public Integer getUserId() {
    return userId;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }

  public Integer getActive() {
    return active;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }
}
