package br.com.taqtile.android.app.presentation.gestationpregnancyinformation.model;

/**
 * Created by taqtile on 5/16/17.
 */

public class ExamViewModel {

  private String examDate;
  private String days;
  private String weeks;

  public ExamViewModel(String examDate, String days, String weeks) {
    this.examDate = examDate;
    this.days = days;
    this.weeks = weeks;
  }

  public String getExamDate() {
    return examDate;
  }

  public String getDays() {
    return days;
  }

  public String getWeeks() {
    return weeks;
  }
}
