package br.com.taqtile.android.app.presentation.questionnaires.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by felipesabino on 5/23/17.
 */

public class QuestionItemResultListToQuestionViewModelListMapper {
  public static List<QuestionViewModel> perform(List<QuestionItemResult> questions) throws DomainError {

    return StreamSupport.stream(questions)
      .filter(Objects::nonNull)
      .map(item -> {
        try {
          return QuestionItemResultToQuestionViewModelMapper.perform(item);
        } catch (DomainError e) {
          throw new RuntimeException(e);
        }
      })
      .collect(Collectors.toList());

  }
}
