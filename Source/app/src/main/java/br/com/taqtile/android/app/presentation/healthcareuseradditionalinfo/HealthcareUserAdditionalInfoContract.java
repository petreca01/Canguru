package br.com.taqtile.android.app.presentation.healthcareuseradditionalinfo;

import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 20/04/17.
 */

public interface HealthcareUserAdditionalInfoContract {

  interface Presenter extends BasePresenter {
    Observable<UserViewModel> updateUserData(Integer operatorId, String telephone, String cdf, String documentType);
  }
}
