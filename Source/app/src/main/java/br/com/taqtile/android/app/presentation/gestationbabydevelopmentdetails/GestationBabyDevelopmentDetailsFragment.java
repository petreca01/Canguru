package br.com.taqtile.android.app.presentation.gestationbabydevelopmentdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.GestationTextFormatterHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

import static br.com.taqtile.android.app.support.GlobalConstants.USER_DATA_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.WEEKLY_CONTENT_DATA_KEY;

/**
 * Created by taqtile on 12/05/17.
 */

public class GestationBabyDevelopmentDetailsFragment extends AppBaseFragment {

  @BindView(R.id.fragment_gestation_baby_development_details_description)
  DescriptionHeaderWithImage babyDevelopmentDescription;

  private CustomToolbar customToolbar;

  private String babyName;

  public static GestationBabyDevelopmentDetailsFragment newInstance(){ return new GestationBabyDevelopmentDetailsFragment(); }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_gestation_baby_development_details, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    fetchUserData();
  }

  private void fetchUserData() {
    UserViewModel userData = getActivity().getIntent().getParcelableExtra(USER_DATA_KEY);
    WeeklyContentViewModel weeklyContent = getActivity().getIntent().getParcelableExtra(WEEKLY_CONTENT_DATA_KEY);

    if (userData != null) {
      setBabyName(userData.getBabyName());
    }
    if (weeklyContent != null) {
      setBabyDevelopmentDetails(GestationTextFormatterHelper.composeDevelopmentWeek(getContext(), Integer.toString(weeklyContent.getWeek())),
        weeklyContent.getWeekContentBody());
    }

    if (userData == null || weeklyContent == null){
      if (userData == null){
        setBabyName(getResources().getString(R.string.fragment_gestation_baby_development_details_baby_name_placeholder));
      }
      placeholder.setPlaceholderType(CustomPlaceholder.UNEXPECTED_ERROR);
      showPlaceholder();
    }

  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_gestation_baby_development_details_title, babyName), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  private void setBabyDevelopmentDetails(String developmentDetailsTitle, String developmentDetailsDescription){
    babyDevelopmentDescription.setTitle(developmentDetailsTitle);
    babyDevelopmentDescription.setDescription(developmentDetailsDescription);
  }

  private void setBabyName(String babyName){
    this.babyName = babyName;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_gestation_baby_development_details_placeholder;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_gestation_development_details),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
