package br.com.taqtile.android.app.presentation.maternitynotfound;

import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;


public interface MaternityNotFoundContract {

  interface Presenter extends BasePresenter {

  }
}
