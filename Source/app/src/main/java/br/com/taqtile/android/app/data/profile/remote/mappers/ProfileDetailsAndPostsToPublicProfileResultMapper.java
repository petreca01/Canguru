package br.com.taqtile.android.app.data.profile.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.forum.models.PostResult;
import br.com.taqtile.android.app.domain.profile.models.ProfileDetailsResult;
import br.com.taqtile.android.app.domain.profile.models.PublicProfileResult;

/**
 * Created by taqtile on 4/18/17.
 */

public class ProfileDetailsAndPostsToPublicProfileResultMapper {

  public static PublicProfileResult perform(ProfileDetailsResult profileDetailsResult,
                                            List<PostResult> postResults) {
    return new PublicProfileResult(profileDetailsResult, postResults);
  }
}
