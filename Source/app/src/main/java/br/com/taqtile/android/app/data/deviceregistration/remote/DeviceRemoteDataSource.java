package br.com.taqtile.android.app.data.deviceregistration.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.common.remote.models.RemoteRequestModel;
import rx.Observable;

/**
 * Created by taqtile on 3/30/17.
 */

public class DeviceRemoteDataSource extends BaseRemoteDataSource {
  private Observable<DeviceServices> authenticatedServices;

  public DeviceRemoteDataSource() {
    super();
    this.authenticatedServices = ServiceFactory.getObservableAuthenticated(DeviceServices.class);
  }

  public Observable<BaseRemoteResponse<EmptyResult>> register(
    RemoteRequestModel deviceRemoteRequest) {
    return performRequest(authenticatedServices.flatMap(
      services -> services.register(deviceRemoteRequest.getFormData())));
  }
}
