package br.com.taqtile.android.app.data.notification.local;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import br.com.taqtile.android.app.data.common.prefser.PrefserFactory;
import br.com.taqtile.android.app.data.common.prefser.PrefserWrapper;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.notification.remote.models.response.NotificationItemRemoteResponse;
import br.com.taqtile.android.app.support.GlobalConstants;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public class NotificationLocalDataSource {
  private PrefserWrapper prefserWrapper;

  public NotificationLocalDataSource() {
    prefserWrapper = new PrefserWrapper(PrefserFactory.build());
  }

  public Observable<EmptyResult> saveLastSeenNotificationId(Integer id) {
    return prefserWrapper.putInteger(GlobalConstants.LAST_SEEN_NOTIFICATION_ID_KEY, id);
  }

  public Observable<Integer> getLastSeenNotificationId() {
    return prefserWrapper.getInteger(GlobalConstants.LAST_SEEN_NOTIFICATION_ID_KEY);
  }

  public Observable<? extends List<NotificationItemRemoteResponse>> getNotifications() {
    Type listType = new TypeToken<List<NotificationItemRemoteResponse>>() {}.getType();
    return prefserWrapper.<List<NotificationItemRemoteResponse>>getFromJsonString(GlobalConstants.NOTIFICATION_CACHE_KEY, listType)
      //All error handling bellow are on purpose to returning an empty list
      // any error or problem parsing data from shared pref will result in returning an empty list
      .onErrorResumeNext(Observable.from(Collections.emptyList()))
      .onExceptionResumeNext(Observable.from(Collections.emptyList()))
      .map(result -> {
        if (result == null) {
          return Collections.emptyList();
        } else {
          return result;
        }
      });

  }

  public Observable<EmptyResult> saveNotifications(List<NotificationItemRemoteResponse> notifications) {
    return prefserWrapper.putAsJsonString(GlobalConstants.NOTIFICATION_CACHE_KEY, notifications);
  }
}
