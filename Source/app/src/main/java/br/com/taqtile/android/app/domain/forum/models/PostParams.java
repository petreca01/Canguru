package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 4/17/17.
 */

public class PostParams {

  private String  postId;
  private String  replyId;
  private String  reply;
  private Integer dislike;

  public PostParams(Integer postId) {
    this.postId = postId != null ? String.valueOf(postId) : "";
  }

  public PostParams(Integer postId, Integer replyId, String reply, Integer dislike) {
    this.postId = postId != null ? String.valueOf(postId) : "";
    this.replyId = replyId != null ? String.valueOf(replyId) : "";
    this.reply = reply != null ? String.valueOf(reply) : "";
    this.dislike = dislike;
  }

  public String getPostId() {
    return postId;
  }

  public String getReplyId() {
    return replyId;
  }

  public String getReply() {
    return reply;
  }

  public Integer getDislike() {
    return dislike;
  }
}
