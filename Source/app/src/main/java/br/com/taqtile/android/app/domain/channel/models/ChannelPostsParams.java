package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 31/03/17.
 */

public class ChannelPostsParams {

  private Integer channelId;
  private Integer page;
  private Integer perPage;

  public ChannelPostsParams(Integer channelId) {
    this.channelId = channelId;
  }

  public Integer getChannelId() {
    return channelId;
  }

  public void setChannelId(Integer channelId) {
    this.channelId = channelId;
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }
}
