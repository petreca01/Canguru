package br.com.taqtile.android.app.presentation.maternity;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityCell;
import br.com.taqtile.android.app.presentation.maternity.components.MaternityHeaderCell;
import br.com.taqtile.android.app.presentation.maternity.viewholders.MaternityCellViewHolder;
import br.com.taqtile.android.app.presentation.maternity.viewholders.MaternityHeaderViewHolder;

/**
 * Created by taqtile on 5/11/17.
 */

public class MaternityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private MaternityHeaderCell.Listener listener;
  private MaternityCell.MaternityCellListener maternityCellListener;

  private List<MaternityViewModel> maternityViewModelList;

  private static final int MATERNITY_HEADER = 0, MATERNITY_CELL = 1;
  private static final int HEADER_OFFSET = 1;

  public MaternityAdapter(List<MaternityViewModel> maternityViewModelList, MaternityCell.MaternityCellListener maternityAdapterListener) {
    this.maternityViewModelList = maternityViewModelList;
    this.maternityCellListener = maternityAdapterListener;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == MATERNITY_HEADER) {
      return new MaternityHeaderViewHolder(new MaternityHeaderCell(parent.getContext()));
    } else {
      return new MaternityCellViewHolder(new MaternityCell(parent.getContext()));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if (holder.getItemViewType() == MATERNITY_CELL) {
      ((MaternityCellViewHolder) holder).setData(maternityViewModelList.get(position - HEADER_OFFSET));
      ((MaternityCellViewHolder) holder).getMaternityCell().setListener(maternityCellListener);
    } else if (holder.getItemViewType() == MATERNITY_HEADER) {
      ((MaternityHeaderViewHolder) holder).resetFilter();
      ((MaternityHeaderViewHolder) holder).setFileInputListener(listener);
      if (this.maternityViewModelList.size() > 0) {
        ((MaternityHeaderViewHolder) holder).setCityName(maternityViewModelList.get(0).getCity());
      }
    }
  }

  @Override
  public int getItemCount() {
    return maternityViewModelList.size() + HEADER_OFFSET;
  }

  @Override
  public int getItemViewType(int position) {
    return position == 0 ? MATERNITY_HEADER : MATERNITY_CELL;
  }

  public void setListener(MaternityHeaderCell.Listener listener) {
    this.listener = listener;
  }

}
