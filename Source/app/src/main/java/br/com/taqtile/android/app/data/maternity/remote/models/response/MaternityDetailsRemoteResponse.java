package br.com.taqtile.android.app.data.maternity.remote.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostItemRemoteResponse;

/**
 * Created by taqtile on 3/23/17.
 */

public class MaternityDetailsRemoteResponse {
    @SerializedName("hospital")
    private MaternityItemRemoteResponse hospital;

    @SerializedName("comments")
    private List<MaternityCommentItemRemoteResponse> comments;

    @SerializedName("channel_posts")
    private List<ChannelPostItemRemoteResponse> channelPosts;

    public MaternityItemRemoteResponse getHospital() {
        return hospital;
    }

    public List<MaternityCommentItemRemoteResponse> getComments() {
        return comments;
    }

    public List<ChannelPostItemRemoteResponse> getChannelPosts() {
        return channelPosts;
    }
}
