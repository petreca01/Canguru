package br.com.taqtile.android.app.data.channel.remote.mappers;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostReplyRemoteRequest;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostReplyParams;

/**
 * Created by taqtile on 05/04/17.
 */

public class ChannelPostReplyParamsToChannelPostReplyRemoteRequestMapper {

  public static ChannelPostReplyRemoteRequest perform(ChannelPostReplyParams channelPostReplyParams) {
    ChannelPostReplyRemoteRequest channelPostReplyRemoteRequest = new ChannelPostReplyRemoteRequest();

    channelPostReplyRemoteRequest.setPostId(channelPostReplyParams.getPostId());
    channelPostReplyRemoteRequest.setReply(channelPostReplyParams.getReply());

    return channelPostReplyRemoteRequest;
  }
}
