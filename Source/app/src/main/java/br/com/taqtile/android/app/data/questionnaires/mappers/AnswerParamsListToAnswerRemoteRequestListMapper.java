package br.com.taqtile.android.app.data.questionnaires.mappers;

import br.com.taqtile.android.app.data.questionnaires.models.AnswerRemoteRequest;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import java.util.List;

/**
 * Created by taqtile on 07/04/17.
 */

public class AnswerParamsListToAnswerRemoteRequestListMapper {
  public static List<AnswerRemoteRequest> perform(List<AnswerParams> answerParamsList) {

    return StreamSupport.stream(answerParamsList)
      .map(AnswerParamsToAnswerRemoteRequestMapper::perform)
      .collect(Collectors.toList());
  }
}
