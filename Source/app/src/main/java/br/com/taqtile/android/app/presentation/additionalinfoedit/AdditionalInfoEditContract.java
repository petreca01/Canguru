package br.com.taqtile.android.app.presentation.additionalinfoedit;

import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by Renato on 4/20/17.
 */

public interface AdditionalInfoEditContract {

  interface Presenter extends BasePresenter {
    Observable<UserViewModel> editUserData(EditUserDataParams editUserDataParams);

    Observable<LocationDiscoveryResult> getLocationDiscovery(LocationDiscoveryByZipCodeParams params);
  }

}
