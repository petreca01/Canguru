package br.com.taqtile.android.app.domain.agenda.models;

/**
 * Created by taqtile on 4/7/17.
 */

public class AppointmentParams {
  private Integer typeId;
  private String name;
  private String date;
  private String observation;

  public AppointmentParams(Integer typeId, String name, String date, String observation) {
    this.typeId = typeId;
    this.name = name;
    this.date = date;
    this.observation = observation;
  }

  public Integer getTypeId() {
    return typeId;
  }

  public String getName() {
    return name;
  }

  public String getDate() {
    return date;
  }

  public String getObservation() {
    return observation;
  }

}
