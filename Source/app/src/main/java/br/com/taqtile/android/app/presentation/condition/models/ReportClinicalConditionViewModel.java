package br.com.taqtile.android.app.presentation.condition.models;

import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;

/**
 * Created by taqtile on 5/19/17.
 */

public class ReportClinicalConditionViewModel {
  private UserViewModel user;
  private UserClinicalConditionViewModel userCondition;

  public ReportClinicalConditionViewModel(UserViewModel user, UserClinicalConditionViewModel userCondition) {
    this.user = user;
    this.userCondition = userCondition;
  }

  public UserViewModel getUser() {
    return user;
  }

  public UserClinicalConditionViewModel getUserCondition() {
    return userCondition;
  }

}
