package br.com.taqtile.android.app.presentation.maternityrating;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentParams;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.maternityrating.components.RatingStarComponent;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.GenericTextAreaLabelEditTextCaption;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.presentation.maternityrating.MaternityRatingActivity.MATERNITY_TAG;
import static br.com.taqtile.android.app.support.GlobalConstants.RATING_MATERNITY_KEY;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityRatingFragment extends AppBaseFragment {

  private static final int STAR_ARRAY_INDEX_TO_RATE_OFFSET = 1;
  @BindView(R.id.fragment_maternity_rating_logo)
  ImageView maternityLogo;
  @BindView(R.id.fragment_maternity_rating_name)
  CustomTextView maternityName;
  @BindView(R.id.fragment_maternity_rate_first_star)
  RatingStarComponent firstStar;
  @BindView(R.id.fragment_maternity_rate_second_star)
  RatingStarComponent secondStar;
  @BindView(R.id.fragment_maternity_rate_third_star)
  RatingStarComponent thirdStar;
  @BindView(R.id.fragment_maternity_rate_fourth_star)
  RatingStarComponent fourthStar;
  @BindView(R.id.fragment_maternity_rate_fifth_star)
  RatingStarComponent fifthStar;
  @BindView(R.id.fragment_maternity_rating_comment_body)
  GenericTextAreaLabelEditTextCaption commentBody;
  @BindView(R.id.fragment_maternity_rating_submit_button)
  ProgressBarButton submitButton;
  @BindView(R.id.fragment_maternity_rating_container)
  LinearLayout container;

  private List<RatingStarComponent> stars;
  private int currentRating;
  private MaternityRatingContract.Presenter presenter;
  private NavigationManager navigationManager;
  private MaternityViewModel maternityViewModel;
  private CustomToolbar customToolbar;

  public static MaternityRatingFragment newInstance() {
    return new MaternityRatingFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_maternity_rating, container, false);
    ButterKnife.bind(this, root);
    maternityViewModel = getActivity().getIntent().getParcelableExtra(MATERNITY_TAG);
    setupMaternityInfo(maternityViewModel);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupRatingComponent();
    setupSubmitButton();
    setupToolbar();

    commentBody.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getResources()
        .getString(R.string.fragment_maternity_rating_title), Gravity.LEFT);
    }
  }

  private void setupMaternityInfo(MaternityViewModel maternityViewModel) {
    PicassoHelper.loadImageFromUrl(
      maternityLogo,
      maternityViewModel.getLogo(),
      getContext(),
      ContextCompat.getDrawable(getContext(), R.drawable.ic_maternidade_placeholder));
    maternityName.setText(maternityViewModel.getName());
  }

  private void setupRatingComponent() {
    currentRating = 0;
    firstStar.setOnClickListener(v -> {
      if (currentRating == 1) {
        this.setRating(0);
      } else {
        this.setRating(1);
      }
    });
    stars = initAndPopulateStars();
    for (int i = 1; i < stars.size(); i++) {
      final int index = i;
      stars.get(i).setOnClickListener(v -> setRating(index + STAR_ARRAY_INDEX_TO_RATE_OFFSET));
    }
    setRating(currentRating);
  }

  private void setupSubmitButton() {
    submitButton.setButtonListener(this::onSubmitButtonClicked);
  }

  private void onSubmitButtonClicked() {
    rateMaternity();
    comment(commentBody.getText());
  }

  private List<RatingStarComponent> initAndPopulateStars() {
    ArrayList<RatingStarComponent> stars = new ArrayList<>();
    stars.add(firstStar);
    stars.add(secondStar);
    stars.add(thirdStar);
    stars.add(fourthStar);
    stars.add(fifthStar);
    return stars;
  }

  private void setRating(int selectedStar) {
    currentRating = selectedStar;
    for (int i = 0; i < stars.size(); i++) {
      if (i < selectedStar) {
        stars.get(i).setStarFull();
      } else {
        stars.get(i).setStarEmpty();
      }
    }
  }

  private void rateMaternity() {
    if (currentRating > 0) {
      showLoading();
      RateMaternityParams params = new RateMaternityParams(currentRating, maternityViewModel.getId());
      Subscription subscription = presenter.rateMaternity(params)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onRateMaternitySuccess, this::onRateMaternityFailure, this::hideLoading);
      compositeSubscription.add(subscription);
    }
  }

  private void onRateMaternityFailure(Throwable throwable) {
    hideLoading();
    showSnackBar(throwable.getMessage());
  }

  private void onRateMaternitySuccess(MaternityViewModel maternityViewModel) {
    Intent intent = new Intent();
    getActivity().setResult(Activity.RESULT_OK, intent);
    intent.putExtra(RATING_MATERNITY_KEY, getString(R.string.fragment_maternity_rating_success_rating));
    getActivity().finish();
  }

  private void comment(String text) {
    if (!text.equals("")) {
      MaternityCommentParams params = new MaternityCommentParams(text, maternityViewModel.getId());
      Subscription subscription = presenter.comment(params)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onCommentSuccess, this::onCommentFailure, this::hideLoading);
      compositeSubscription.add(subscription);
    }
    commentBody.setText("");
  }

  private void onCommentFailure(Throwable throwable) {
    hideLoading();
    showSnackBar(throwable.getMessage());
  }

  private void showSnackBar(String message) {
    showSnackbar(message, Snackbar.LENGTH_LONG);
  }

  private void onCommentSuccess(List<MaternityCommentViewModel> maternityCommentViewModel) {
    hideLoading();
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_hospital),
      getString(R.string.analytics_event_comment),
      getString(R.string.analytics_label_user_commented_hospital)
    ));
  }

  public void setPresenter(MaternityRatingContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void showLoading() {
    submitButton.showLoadingState();
  }

  @Override
  public void hideLoading() {
    submitButton.showDefaultState();
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_hospital_rating),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
