package br.com.taqtile.android.app.domain.followcard.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.followcard.models.FollowCardUltrasoundsResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardUltrasoundsViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardUltrasoundsResultToViewModelMapper {

  public static List<FollowCardUltrasoundsViewModel> perform(
    List<FollowCardUltrasoundsResult> followCardUltrasoundsResults) {
    return StreamSupport.stream(followCardUltrasoundsResults)
      .filter(Objects::nonNull)
      .map(FollowCardUltrasoundsResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardUltrasoundsViewModel map(
    FollowCardUltrasoundsResult followCardUltrasoundsResult) {
    return new FollowCardUltrasoundsViewModel(
      followCardUltrasoundsResult.getId(),
      followCardUltrasoundsResult.getConsultationId(),
      followCardUltrasoundsResult.getDate(),
      followCardUltrasoundsResult.getPeriodPregnancyWeeks(),
      followCardUltrasoundsResult.getPeriodPregnancyDays(),
      followCardUltrasoundsResult.getUltrasoundPregnancyWeeks(),
      followCardUltrasoundsResult.getUltrasoundPregnancyDays(),
      followCardUltrasoundsResult.getFetalWeight(),
      followCardUltrasoundsResult.getPlacenta(),
      followCardUltrasoundsResult.getLiquid(),
      followCardUltrasoundsResult.getPbf(),
      followCardUltrasoundsResult.getCreatedAt(),
      followCardUltrasoundsResult.getDoctorId(),
      followCardUltrasoundsResult.getDoctorName()
    );
  }
}
