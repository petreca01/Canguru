package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.domain.account.mappers.UserDataResultToViewModelMapper;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class UserDataUseCase extends BaseUseCase<UserViewModel, Void, AccountRepository> {

  public UserDataUseCase(AccountRepository accountRepository) {
    super(accountRepository);
  }

  @Override
  public Observable<UserViewModel> execute(Void aVoid) {
    return getRepository().getUserData()
      .map(UserDataResultToViewModelMapper::perform);
  }
}
