package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.view.View;

/**
 * Created by taqtile on 5/23/17.
 */

public class TransparentCircle implements  SurroundingShape {

  private int centerX;
  private int centerY;

  public static class DrawingParams {
    public int radius;
    public Paint paint;

    public DrawingParams(int radius, Paint paint) {
      this.radius = radius;
      this.paint = paint;
    }
  }

  private TransparentCircle.DrawingParams drawingParams;

  public TransparentCircle(TransparentCircle.DrawingParams drawingParams) {
    this.drawingParams = drawingParams;
    this.centerX = 0;
    this.centerY = 0;
  }

  public void drawOnCanvas(Canvas canvas) {
   canvas.drawCircle(centerX, centerY, drawingParams.radius, drawingParams.paint);
  }

  @Override
  public void focusIn(View target) {
    int[] location = new int[2];

    target.getLocationInWindow(location);
    int left = location[0];
    int top = location[1];

    centerX =  left + target.getWidth() / 2;
    centerY = top + target.getHeight() / 2;
  }

  public void setCenter(int centerX, int centerY) {
    this.centerX = centerX;
    this.centerY = centerY;
  }

}
