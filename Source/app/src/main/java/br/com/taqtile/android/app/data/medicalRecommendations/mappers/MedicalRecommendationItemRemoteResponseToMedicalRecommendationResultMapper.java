package br.com.taqtile.android.app.data.medicalRecommendations.mappers;

import br.com.taqtile.android.app.data.medicalRecommendations.remote.models.MedicalRecommendationItemRemoteResponse;
import br.com.taqtile.android.app.domain.medicalRecommendations.models.MedicalRecommendationResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class MedicalRecommendationItemRemoteResponseToMedicalRecommendationResultMapper {
  public static MedicalRecommendationResult perform(MedicalRecommendationItemRemoteResponse medicalRecommendationItemRemoteResponse) {
    return new MedicalRecommendationResult(
      medicalRecommendationItemRemoteResponse.getLoaded(),
      medicalRecommendationItemRemoteResponse.getId(),
      medicalRecommendationItemRemoteResponse.getDoctorId(),
      medicalRecommendationItemRemoteResponse.getDoctorName(),
      medicalRecommendationItemRemoteResponse.getName(),
      medicalRecommendationItemRemoteResponse.getRecommendedAt(),
      medicalRecommendationItemRemoteResponse.getAcknowledgedAt(),
      medicalRecommendationItemRemoteResponse.getContent());
  }
}
