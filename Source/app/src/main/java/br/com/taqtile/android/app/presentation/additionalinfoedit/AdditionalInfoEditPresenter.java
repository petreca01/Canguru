package br.com.taqtile.android.app.presentation.additionalinfoedit;

import br.com.taqtile.android.app.domain.account.EditUserDataUseCase;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.city.LocationDiscoveryByZipCodeUseCase;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryByZipCodeParams;
import br.com.taqtile.android.app.domain.city.models.LocationDiscoveryResult;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by Renato on 4/20/17.
 */

public class AdditionalInfoEditPresenter implements AdditionalInfoEditContract.Presenter {

  private EditUserDataUseCase editUserDataUseCase;
  private LocationDiscoveryByZipCodeUseCase locationDiscoveryUseCase;

  public AdditionalInfoEditPresenter() {
    this.editUserDataUseCase = Injection.provideEditUserDataUseCase();
    this.locationDiscoveryUseCase = Injection.provideDiscoveryByZipCodeUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> editUserData(EditUserDataParams editUserDataParams) {
    return editUserDataUseCase.execute(editUserDataParams);
  }

  @Override
  public Observable<LocationDiscoveryResult> getLocationDiscovery(LocationDiscoveryByZipCodeParams params) {
    return locationDiscoveryUseCase.execute(params);
  }
}
