package br.com.taqtile.android.app.data.exams.remote.mappers;

import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsValuesRemoteResponse;
import br.com.taqtile.android.app.domain.exams.models.ExamsValuesResult;

/**
 * Created by taqtile on 06/04/17.
 */

public class ExamsValuesRemoteResponseToExamsValuesResultMapper {

  public static ExamsValuesResult perform(ExamsValuesRemoteResponse examsValuesRemoteResponse) {
    if (examsValuesRemoteResponse == null) {
      return new ExamsValuesResult();
    }
    return new ExamsValuesResult(examsValuesRemoteResponse.getDays(),
      examsValuesRemoteResponse.getWeeks());
  }
}
