package br.com.taqtile.android.app.presentation.channelpostdetail;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.presentation.postdetail.PostDetailFragment;
import br.com.taqtile.android.app.presentation.common.post.PostDetailContract;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 12/04/17.
 */

public class ChannelPostDetailFragment extends PostDetailFragment {

  protected ChannelPostDetailContract.Presenter channelPresenter;
  public static ChannelPostDetailFragment newInstance() {
    return new ChannelPostDetailFragment();
  }
  public void setPresenter(PostDetailContract.Presenter presenter) {
    this.presenter = presenter;
  }
  public void setChannelPresenter(ChannelPostDetailContract.Presenter channelPresenter) {
    this.channelPresenter = channelPresenter;
  }

  @Override
  public void onProfileClicked(Integer profileId, Integer postId) {
    if (postId.equals(this.postId) && fromProfile) {
      this.getActivity().finish();
    } else if (!postId.equals(this.postId)) {
      navigationManager.showPublicProfileUI(profileId, null);
    } else {
      navigationManager.showChannelDetailUI(profileId, null);
    }
  }

  @Override
  protected void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels, Integer itemId, boolean alreadyLiked) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel),
      getString(R.string.analytics_event_like),
      getString(R.string.analytics_label_user_liked)
    ));

    super.onItemLikedSuccess(listingsViewModels, itemId, alreadyLiked);
  }

  @Override
  protected void onPostNewCommentSuccess(List<ListingsViewModel> listingsViewModels) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel),
      getString(R.string.analytics_event_reply),
      getString(R.string.analytics_label_user_replied_post)
    ));

    super.onPostNewCommentSuccess(listingsViewModels);
  }
  @Override
  public void onDeleteComment(Integer commentId){
    Subscription deleteCommentSubscription = channelPresenter.deleteComment(this.postId, commentId)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onDeleteCommentSuccess,
        this::onDeleteCommentFailure);
    compositeSubscription.add(deleteCommentSubscription);
  }
  protected void onDeleteCommentSuccess(List<ListingsViewModel> listingsViewModels) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel),
      getString(R.string.analytics_event_reply_delete),
      getString(R.string.analytics_label_user_deleted_reply)
    ));

    super.onDeleteCommentSuccess();
  }

  @Override
  public void onReportPost(Integer postId) {
    // functionality not available for channel
  }
  @Override
  protected void onReportPostSuccess(EmptyResult emptyResult){
    // functionality not available for channel
  }

  @Override
  public void onDeletePost(Integer postId) {
    // functionality not available for channel
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel_post_detail),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
