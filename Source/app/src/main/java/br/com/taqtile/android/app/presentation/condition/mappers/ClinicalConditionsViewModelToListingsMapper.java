package br.com.taqtile.android.app.presentation.condition.mappers;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionSectionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ClinicalConditionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionSectionHeaderViewModel;
import br.com.taqtile.android.app.listings.viewmodels.UserClinicalConditionViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;

/**
 * Created by taqtile on 5/18/17.
 */

public class ClinicalConditionsViewModelToListingsMapper {

  public static List<ListingsViewModel> perform(List<ClinicalConditionViewModel> allConditions,
    List<UserClinicalConditionViewModel> userHistoryConditions) {
    List<ListingsViewModel> clinicalConditionsViewModel = new ArrayList<>();

    clinicalConditionsViewModel.add(new ClinicalConditionHeaderViewModel());
    if (!userHistoryConditions.isEmpty()) {
      clinicalConditionsViewModel.add(new UserClinicalConditionSectionHeaderViewModel());
      clinicalConditionsViewModel.addAll(ClinicalConditionsViewModelToListingsMapper.mapHistoryConditions(userHistoryConditions));
    }
    clinicalConditionsViewModel.add(new ClinicalConditionSectionHeaderViewModel());
    clinicalConditionsViewModel.addAll(ClinicalConditionsViewModelToListingsMapper.mapAllConditions(allConditions));


    return clinicalConditionsViewModel;
  }

  private static List<ListingsViewModel> mapHistoryConditions(
    List<UserClinicalConditionViewModel> userClinicalConditionViewModels) {
    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.addAll(userClinicalConditionViewModels);
    return listingsViewModels;
  }

  private static List<ListingsViewModel> mapAllConditions(
    List<ClinicalConditionViewModel> clinicalConditionViewModels) {
    List<ListingsViewModel> listingsViewModels = new ArrayList<>();
    listingsViewModels.addAll(clinicalConditionViewModels);
    return listingsViewModels;
  }
}
