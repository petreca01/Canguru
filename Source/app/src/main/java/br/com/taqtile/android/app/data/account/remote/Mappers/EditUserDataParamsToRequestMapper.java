package br.com.taqtile.android.app.data.account.remote.Mappers;

import br.com.taqtile.android.app.data.account.remote.models.request.EditUserRemoteRequest;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;

/**
 * Created by taqtile on 4/17/17.
 */

public class EditUserDataParamsToRequestMapper {

  public static EditUserRemoteRequest perform(EditUserDataParams editUserDataParams) {
    return new EditUserRemoteRequest(
      editUserDataParams.getCpf(),
      editUserDataParams.getBirthDate(),
      editUserDataParams.getTelephone(),
      editUserDataParams.getZipCode(),
      editUserDataParams.getOperator(),
      editUserDataParams.getCity(),
      editUserDataParams.getState(),
      editUserDataParams.getLatitude(),
      editUserDataParams.getLongitude(),
      editUserDataParams.getLocationPolicyAgreed(),
      editUserDataParams.getOperatorDocument());
  }
}
