package br.com.taqtile.android.app.data.forum.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.request.ContentSuggestionRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.request.CreatePostRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.request.PostRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.request.PostsListRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.request.WarnPostRemoteRequest;
import br.com.taqtile.android.app.data.forum.remote.models.response.ContentSuggestionRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.NewPostRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostAndRepliesRemoteResponse;
import br.com.taqtile.android.app.data.forum.remote.models.response.PostListResponse;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class ForumRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ForumServices> services;

  public ForumRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(ForumServices.class);
  }

  public Observable<BaseRemoteResponse<PostListResponse>> listPosts(PostsListRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.listPosts(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<NewPostRemoteResponse>> createPost(
    CreatePostRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.createPost(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<PostAndRepliesRemoteResponse>> getPostAndReplies(
    PostRemoteRequest request) {
    return performRequest(
      services.flatMap(services -> services.getPostAndReplies(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<PostAndRepliesRemoteResponse>> likePost(
    PostRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.likePost(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<PostAndRepliesRemoteResponse>> replyPost(
    PostRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.replyPost(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<String>> delete(PostRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.delete(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<PostAndRepliesRemoteResponse>> warnPost(
    WarnPostRemoteRequest request) {
    return performRequest(services.flatMap(services -> services.warn(request.getFormData())));
  }

  public Observable<BaseRemoteResponse<ContentSuggestionRemoteResponse>> getContentSuggestion(
    ContentSuggestionRemoteRequest request){
    return performRequest(services.flatMap(services -> services.getSuggestedContent(request.getFormData())));
  }

}
