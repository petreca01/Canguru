package br.com.taqtile.android.app.support.analytics;

/**
 * Created by felipesabino on 5/24/17.
 */

public class AnalyticsEvent {
  private String category;
  private String action;
  private String label;

  public AnalyticsEvent(String category) {
    this(category, null, null);
  }

  public AnalyticsEvent(String category, String action) {
    this(category, action, null);
  }

  public AnalyticsEvent(String category, String action, String label) {
    this.category = category;
    this.action = action;
    this.label = label;
  }

  public String getCategory() {
    return category;
  }

  public String getAction() {
    return action;
  }

  public String getLabel() {
    return label;
  }
}
