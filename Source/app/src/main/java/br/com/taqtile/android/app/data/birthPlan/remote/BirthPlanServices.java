package br.com.taqtile.android.app.data.birthPlan.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public interface BirthPlanServices {
    @FormUrlEncoded
    @POST("birth_plan")
    Observable<Result<BaseRemoteResponse<List<QuestionItemRemoteResponse>>>> listQuestionsAndAnswers(@FieldMap Map<String, String> listQuestionsAndAnswersRequestModel);

    @FormUrlEncoded
    @POST("birth_plan/answer")
    Observable<Result<BaseRemoteResponse<List<QuestionItemRemoteResponse>>>> answer(@FieldMap Map<String, String> answersRequestModel);

    @FormUrlEncoded
    @POST("birth_plan/answer/remove")
    Observable<Result<BaseRemoteResponse<List<QuestionItemRemoteResponse>>>> removeAnswer(@FieldMap Map<String, String> removeAnswerRequestModel);
}
