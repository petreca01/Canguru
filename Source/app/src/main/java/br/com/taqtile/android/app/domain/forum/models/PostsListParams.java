package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 4/3/17.
 */

public class PostsListParams {
  public static String ORDER_BY_CREATED_AT = "created_at";
  public static String ORDER_DIRECTION_DESC = "DESC";
  public static Integer DEFAULT_PER_PAGE = 10;

  private Integer page;
  private Integer perPage;
  private String  orderBy;
  private String  orderDirection;
  private String  search;

  public PostsListParams(Integer page, Integer perPage, String orderBy, String orderDirection, String search) {
    this.page = page;
    this.perPage = perPage;
    this.orderBy = orderBy;
    this.orderDirection = orderDirection;
    this.search = search;
  }

  public Integer getPage() {
    return page;
  }

  public Integer getPerPage() {
    return perPage;
  }

  public String getOrderBy() {
    return orderBy;
  }

  public String getOrderDirection() {
    return orderDirection;
  }

  public String getSearch() {
    return search;
  }
}
