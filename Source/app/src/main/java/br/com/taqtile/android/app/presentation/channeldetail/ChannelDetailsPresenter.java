package br.com.taqtile.android.app.presentation.channeldetail;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.channel.ChannelPostsUseCase;
import br.com.taqtile.android.app.domain.channel.FollowChannelUseCase;
import br.com.taqtile.android.app.domain.channel.LikeChannelPostUseCase;
import br.com.taqtile.android.app.domain.channel.mappers.ChannelPostsResultToListingsViewModelListMapper;
import br.com.taqtile.android.app.domain.channel.models.ChannelFollowParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostsResult;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 4/5/17.
 */

public class ChannelDetailsPresenter implements ChannelDetailsContract.Presenter {

  private FollowChannelUseCase followChannelUseCase;
  private ChannelPostsUseCase channelPostsUseCase;
  private LikeChannelPostUseCase likeChannelPostUseCase;

  public ChannelDetailsPresenter() {
    this.followChannelUseCase = Injection.provideFollowChannelUseCase();
    this.channelPostsUseCase = Injection.provideChannelPostsUseCase();
    this.likeChannelPostUseCase = Injection.provideLikeChannelPostUseCase();

  }

  @Override public void start() {

  }

  @Override public void resume() {

  }

  @Override
  public Observable<List<ListingsViewModel>> fetchChannelDetailsModels(Integer id,
    boolean withHeader) {

    return channelPostsUseCase.execute(new ChannelPostsParams(id))
      .map(channelPostsResult -> getList(channelPostsResult, withHeader));
  }

  @Override
  public Observable<EmptyResult> followChannel(Integer id, boolean isFollowing) {

    return followChannelUseCase.execute(new ChannelFollowParams(id, isFollowing));
  }

  private List<ListingsViewModel> getList(ChannelPostsResult channelPostsResult,
    boolean withHeader) {
    return withHeader ? ChannelPostsResultToListingsViewModelListMapper.perform(channelPostsResult)
      : ChannelPostsResultToListingsViewModelListMapper.performWithoutHeader(channelPostsResult);
  }

  @Override
  public Observable<List<ListingsViewModel>> likeItem(Integer postId, Integer replyId, boolean alreadyLiked) {
    return likeChannelPostUseCase.execute(new ChannelPostParams(postId, null, !alreadyLiked));
  }
}
