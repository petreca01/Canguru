package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by taqtile on 5/23/17.
 */

public class RectangleWithShape implements Shape {

  private Rectangle rectangle;
  private Shape shape;

  public RectangleWithShape(Rectangle rectangle, Shape shape) {
    this.rectangle = rectangle;
    this.shape = shape;
  }

  public Bitmap draw() {
    Bitmap bitmap = Bitmap.createBitmap(rectangle.getWidth(), rectangle.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);

    drawOnCanvas(canvas);

    return bitmap;
  }

  public void drawOnCanvas(Canvas canvas) {

    rectangle.drawOnCanvas(canvas);
    shape.drawOnCanvas(canvas);

  }
}
