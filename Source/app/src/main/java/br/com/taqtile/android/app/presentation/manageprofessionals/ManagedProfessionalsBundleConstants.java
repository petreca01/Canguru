package br.com.taqtile.android.app.presentation.manageprofessionals;

/**
 * Created by taqtile on 27/04/17.
 */

class ManagedProfessionalsBundleConstants {
  static final String PROFESSIONALS_LIST_KEY = "professionalsListKey";
}
