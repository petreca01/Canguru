package br.com.taqtile.android.app.presentation.initiallogin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.CallbackManager;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.signin.SignInActivity;
import br.com.taqtile.android.app.presentation.signup.SignUpActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

/**
 * Created by taqtile on 11/11/16.
 */

public class InitialLoginActivity extends TemplateBackActivity {

  private static final String TAG = "InitialLoginActivity";
  private InitialLoginFragment fragment;
  private CallbackManager callbackManager;

  private CustomNavigationResultListener resultListener;
  private int observedRequestCode;
  private int onActivityRequestCode;
  private int onActivityResultCode;
  private Bundle onActivityResultBundle;

  @Override
  public Fragment getFragment() {
    InitialLoginFragment initialLoginFragment =
      (InitialLoginFragment) getSupportFragmentManager().findFragmentById(getFragmentContainerId());
    if (initialLoginFragment == null) {
      initialLoginFragment = InitialLoginFragment.newInstance();
    }

    fragment = initialLoginFragment;
    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    callbackManager = CallbackManager.Factory.create();

    return new InitialLoginPresenter(fragment, new NavigationHelper(this), this, callbackManager);
  }

  public static void navigate(Context context) {
    Intent intent = new Intent(context, InitialLoginActivity.class);
    context.startActivity(intent);
  }

  @Override
  public void onBackPressed() {
    this.finishAffinity();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    Log.e(TAG, "onActivityResult() resultCode: " + resultCode);

    if (callbackManager != null) {
      callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    if (requestCode == observedRequestCode
      && resultListener != null) {
      onActivityResultCode = resultCode;
      onActivityRequestCode = requestCode;
      onActivityResultBundle = data != null ? data.getExtras() : new Bundle();

      this.finish();
    }
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (resultListener != null &&
      onActivityRequestCode == observedRequestCode) {
      resultListener.onNavigationResult(onActivityResultBundle, onActivityResultCode);
    }
  }

  public void setResultListener(CustomNavigationResultListener listener, int requestCode) {
    resultListener = listener;
    observedRequestCode = requestCode;
  }
}
