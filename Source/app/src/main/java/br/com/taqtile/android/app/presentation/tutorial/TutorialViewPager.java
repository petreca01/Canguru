package br.com.taqtile.android.app.presentation.tutorial;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by taqtile on 5/19/17.
 */

public class TutorialViewPager extends ViewPager {
  public TutorialViewPager(Context context) {
    super(context);
  }

  public TutorialViewPager(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

//  Reference: http://stackoverflow.com/questions/8394681/android-i-am-unable-to-have-viewpager-wrap-content
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int height = 0;
    for(int i = 0; i < getChildCount(); i++) {
      View child = getChildAt(i);
      child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
      int h = child.getMeasuredHeight();
      if(h > height) height = h;
    }

    heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }
}
