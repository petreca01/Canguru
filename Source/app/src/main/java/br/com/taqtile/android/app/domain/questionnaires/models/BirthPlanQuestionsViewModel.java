package br.com.taqtile.android.app.domain.questionnaires.models;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.QuestionViewModel;

/**
 * Created by taqtile on 4/19/17.
 */

public class BirthPlanQuestionsViewModel {

  private List<QuestionViewModel> questionViewModels;
  private List<QuestionViewModel> answeredQuestionViewModels;

  public BirthPlanQuestionsViewModel(List<QuestionViewModel> questionViewModels,
                                     List<QuestionViewModel> answeredQuestionViewModels) {
    this.questionViewModels = questionViewModels;
    this.answeredQuestionViewModels = answeredQuestionViewModels;
  }

  public List<QuestionViewModel> getQuestionViewModels() {
    return questionViewModels;
  }

  public List<QuestionViewModel> getAnsweredQuestionViewModels() {
    return answeredQuestionViewModels;
  }

}
