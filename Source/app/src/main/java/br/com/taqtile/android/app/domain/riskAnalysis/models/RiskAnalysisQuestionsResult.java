package br.com.taqtile.android.app.domain.riskAnalysis.models;

import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.RangedAnswerResult;
import java.util.List;

/**
 * Created by taqtile on 07/04/17.
 */

public class RiskAnalysisQuestionsResult {

  private List<QuestionItemResult> aboutYou;

  private List<QuestionItemResult> aboutYourObstetricalPast;

  private List<QuestionItemResult> aboutYourFamily;

  private List<QuestionItemResult> aboutYourHealth;

  public RiskAnalysisQuestionsResult(List<QuestionItemResult> aboutYou,
    List<QuestionItemResult> aboutYourObstetricalPast,
    List<QuestionItemResult> aboutYourFamily,
    List<QuestionItemResult> aboutYourHealth) {
    this.aboutYou = aboutYou;
    this.aboutYourObstetricalPast = aboutYourObstetricalPast;
    this.aboutYourFamily = aboutYourFamily;
    this.aboutYourHealth = aboutYourHealth;
  }

  public List<QuestionItemResult> getAboutYou() {
    return aboutYou;
  }
  public List<QuestionItemResult> getAboutYourObstetricalPast() { return aboutYourObstetricalPast; }
  public List<QuestionItemResult> getAboutYourFamily() {
    return aboutYourFamily;
  }
  public List<QuestionItemResult> getAboutYourHealth() {
    return aboutYourHealth;
  }
}
