package br.com.taqtile.android.app.data.questionnaires.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class QuestionItemRemoteResponseListToQuestionItemResultListMapper {
  public static List<QuestionItemResult> perform(List<QuestionItemRemoteResponse> questionItemRemoteResponseList) {
    return StreamSupport.stream(questionItemRemoteResponseList)
      .filter(Objects::nonNull)
      .map(QuestionItemRemoteResponseToQuestionItemResultMapper::perform)
      .collect(Collectors.toList());
  }
}
