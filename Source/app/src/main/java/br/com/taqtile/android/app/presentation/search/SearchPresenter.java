package br.com.taqtile.android.app.presentation.search;

import br.com.taqtile.android.app.domain.search.SearchPostsUseCase;
import br.com.taqtile.android.app.domain.search.models.PostSearchParams;
import br.com.taqtile.android.app.domain.search.mappers.ForumPostResultListToForumListingViewModelListMapper;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ForumListingViewModel;
import br.com.taqtile.android.app.support.Injection;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class SearchPresenter implements SearchContract.Presenter {

    private SearchPostsUseCase searchPostsUseCase;


    public SearchPresenter() {
        searchPostsUseCase = Injection.provideSearchPostsUseCase();
    }

    @Override
    public Observable<List<ForumListingViewModel>> search(String searchKeyword, boolean reload) {
        PostSearchParams postSearchParams = new PostSearchParams(searchKeyword, reload);

        return searchPostsUseCase.execute(postSearchParams)
            .map(ForumPostResultListToForumListingViewModelListMapper::perform);
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }
}
