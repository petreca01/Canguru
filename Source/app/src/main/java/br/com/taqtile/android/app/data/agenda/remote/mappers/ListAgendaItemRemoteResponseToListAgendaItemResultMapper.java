package br.com.taqtile.android.app.data.agenda.remote.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.agenda.remote.models.response.AgendaItemRemoteResponse;
import br.com.taqtile.android.app.domain.agenda.models.AgendaItemResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/7/17.
 */

public class ListAgendaItemRemoteResponseToListAgendaItemResultMapper {
  public static List<AgendaItemResult> perform(List<AgendaItemRemoteResponse> response) {

    return StreamSupport.stream(response)
      .map(AgendaItemRemoteResponseToAgendaItemResultMapper::perform)
      .collect(Collectors.toList());
  }
}
