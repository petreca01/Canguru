package br.com.taqtile.android.app.presentation.maternitycitysearch;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.ListCitiesUseCase;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityCitySearchPresenter implements MaternityCitySearchContract.Presenter {

  private ListCitiesUseCase listCitiesUseCase;

  public MaternityCitySearchPresenter() {
    this.listCitiesUseCase = Injection.provideListCitiesUseCase();
  }

  @Override
  public void start() {}

  @Override
  public void resume() {}

  @Override
  public Observable<List<String>> fetchCities() {
    return listCitiesUseCase.execute(null);
  }
}
