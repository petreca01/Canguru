package br.com.taqtile.android.app.presentation.channel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.ChannelAdapter;
import br.com.taqtile.android.app.listings.cells.SearchPlaceholderCell;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.app.misc.utils.helpers.SkeletonLoadingGenerator;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 3/22/17.
 */

public class ChannelFragment extends AppBaseFragment {

  @BindView(R.id.fragment_channel_recycler_view)
  RecyclerView channelsRecyclerView;

  @BindView(R.id.fragment_channel_loading_layout)
  LinearLayout loadingLayout;

  ChannelContract.Presenter presenter;
  NavigationManager navigationManager;
  CustomToolbar customToolbar;

  ChannelAdapter channelAdapter;

  List<SearchListingViewModel> channelsList;
  private SkeletonLoadingGenerator skeletonGenerator;

  public static ChannelFragment newInstance() {
    return new ChannelFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_channel, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupView();
    setupLoading();
    showLoading();
    fetchChannels();
  }

  private void fetchChannels() {
    Subscription subscription = presenter.fetchChannelList()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onChannelListFetched, this::onChannelListFetchedFailure);
    compositeSubscription.add(subscription);
  }

  private void setupView() {
    setupToolbar();
    setupRecyclerView();
    setupListeners();
  }

  private void setupListeners() {
    channelAdapter.setListener(id -> navigationManager.showChannelDetailUI((id != null ?
      Integer.parseInt(id) : 0), null));
  }

  private void setupRecyclerView() {
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    channelsRecyclerView.setLayoutManager(linearLayoutManager);
    setupAdapter();
  }

  private void setupAdapter() {
    channelsList = new ArrayList<>();
    channelAdapter = new ChannelAdapter(channelsList);
    channelsRecyclerView.setAdapter(channelAdapter);
  }

  private void updateChannelList(List<SearchListingViewModel> channelListingViewModelsList) {
    channelsList.clear();
    channelsList.addAll(channelListingViewModelsList);
    channelAdapter.notifyDataSetChanged();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> getActivity().finish());
      customToolbar.setTitle(getResources().getString(R.string.fragment_channel_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), getResources().getDimension(R.dimen.toolbar_elevation));
    }
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void showLoading() {
    channelsRecyclerView.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    skeletonGenerator.stopShimmering();
    FadeEffectHelper.fadeInView(channelsRecyclerView);
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    hideLoading();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_channel_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchChannels();
    hidePlaceholder();
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  public void setPresenter(ChannelContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  private void onChannelListFetched(List<SearchListingViewModel> channelListingViewModelsList) {
    updateChannelList(channelListingViewModelsList);
    hideLoading();
  }

  private void onChannelListFetchedFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void setupLoading() {
    skeletonGenerator = new SkeletonLoadingGenerator(loadingLayout, new SearchPlaceholderCell(getContext()), 10);
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_channel_list),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
