package br.com.taqtile.android.app.presentation.birthplan;

import java.util.List;

import br.com.taqtile.android.app.domain.birthPlan.ListBirthPlanQuestionAnswersUseCase;
import br.com.taqtile.android.app.domain.birthPlan.RestartBirthPlanAnswersUseCase;
import br.com.taqtile.android.app.domain.common.models.DomainError;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.BirthPlanQuestionsViewModel;
import br.com.taqtile.android.app.presentation.questionnaires.mappers.QuestionItemResultListToQuestionViewModelListMapper;
import br.com.taqtile.android.app.support.Injection;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import rx.Observable;

/**
 * Created by taqtile on 10/05/17.
 */

public class BirthplanPresenter implements BirthplanContract.Presenter {

  private ListBirthPlanQuestionAnswersUseCase listBirthPlanQuestionAnswersUseCase;
  private RestartBirthPlanAnswersUseCase restartBirthPlanAnswersUseCase;

  public BirthplanPresenter() {
    this.listBirthPlanQuestionAnswersUseCase = Injection.provideListBirthPlanQuestionAnswersUseCase();
    this.restartBirthPlanAnswersUseCase = Injection.provideRestartBirthPlanAnswersUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<BirthPlanQuestionsViewModel> fetchBirthPlanQuestionAnswersList() {
    return this.map(listBirthPlanQuestionAnswersUseCase.execute(null));
  }

  @Override
  public Observable<BirthPlanQuestionsViewModel> sendBirthPlanQuestionAnswersList(String email) {
    return this.map(listBirthPlanQuestionAnswersUseCase.execute(email));
  }

  @Override
  public Observable<BirthPlanQuestionsViewModel> restartBirthPlanAnswers(String id) {
    return this.map(restartBirthPlanAnswersUseCase.execute(id));
  }

  private Observable<BirthPlanQuestionsViewModel> map(Observable<List<QuestionItemResult>> questionItemsResult){
    return questionItemsResult
      .map(result -> {
        try {
          return new BirthPlanQuestionsViewModel(
            QuestionItemResultListToQuestionViewModelListMapper.perform(result),
            QuestionItemResultListToQuestionViewModelListMapper.perform(answeredQuestions(result)));
        } catch (DomainError domainError) {
          domainError.printStackTrace();
          return null;
        }
      });
  }

  private List<QuestionItemResult> answeredQuestions(List<QuestionItemResult> questionItemResults) {
    return StreamSupport.stream(questionItemResults)
      .filter(question -> question.getAnswer() != null && question.getAnswer().getAnswer() != null)
      .collect(Collectors.toList());
  }

}
