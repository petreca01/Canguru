package br.com.taqtile.android.app.presentation.prenatalcard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.cells.AntecedentsAccordion;
import br.com.taqtile.android.app.listings.cells.ExamsAccordion;
import br.com.taqtile.android.app.listings.cells.MedicalAppointmentsAccordion;
import br.com.taqtile.android.app.listings.cells.UltraSoundAccordion;
import br.com.taqtile.android.app.listings.cells.VaccinesAccordion;
import br.com.taqtile.android.app.misc.general.DescriptionHeaderWithImage;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardConsultationsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardExamsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardUltrasoundsViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardVaccinesViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardViewModel;
import br.com.taqtile.android.app.presentation.prenatalcard.models.RisksViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 5/22/17.
 */

public class PrenatalCardFragment extends AppBaseFragment {

  @BindView(R.id.fragment_prenatal_header)
  DescriptionHeaderWithImage prenatalInfoHeader;

  @BindView(R.id.fragment_prenatal_antecedents)
  AntecedentsAccordion antecedentsCard;

  @BindView(R.id.fragment_prenatal_medical_appointments)
  MedicalAppointmentsAccordion medicalAppointmentsCard;

  @BindView(R.id.fragment_prenatal_exams)
  ExamsAccordion examsCard;

  @BindView(R.id.fragment_prenatal_ultra_sound)
  UltraSoundAccordion ultraSoundCard;

  @BindView(R.id.fragment_prenatal_vaccines)
  VaccinesAccordion vaccinesCard;

  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;
  private PrenatalCardContract.Presenter presenter;
  private Subscription antecedentsCardSubscription;
  private Subscription medicalAppointmentCardSubscription;
  private Subscription examsCardSubscription;
  private Subscription ultraSoundCardSubscription;
  private Subscription vaccinesCardSubscription;

  public static PrenatalCardFragment newInstance() {
    return new PrenatalCardFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_prenatal, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  public void willAppear() {
    fetchCardsData();
  }

  private void fetchCardsData() {
    loadAntecedentesCard();
    loadMedicalAppointmentCard();
    loadExamsCard();
    loadUltraSoundCard();
    loadVaccinesCard();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_prenatal),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void loadAntecedentesCard() {
    if (antecedentsCardSubscription == null) {
      antecedentsCard.showSkeleton();
      antecedentsCardSubscription = presenter.fetchAntecedentsCard()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onAntecedentsSuccess, this::onFetchAntecedentsFailure, this::successState);
      compositeSubscription.add(antecedentsCardSubscription);
    } else {
      compositeSubscription.remove(antecedentsCardSubscription);
    }
  }

  private void onAntecedentsSuccess(FollowCardViewModel followCardViewModel) {
    antecedentsCard.setAge(followCardViewModel.getAge());

    int amountOfPregnancies = followCardViewModel.getGpa().getPregnancy();
    antecedentsCard.setNumberOfGestations(
      getResources().getString(R.string.fragment_prenatal_pregnancies,
        amountOfPregnancies));

    int amountOfNormalDelivery = followCardViewModel.getGpa().getNormalDelivery();
    int amountOfCaesareans = followCardViewModel.getGpa().getCaesarean();
    int amountOfMiscarriages = followCardViewModel.getGpa().getMiscarriage();

    antecedentsCard.setChildbirthTypes(String.format(getString(R.string.fragment_prenatal_antecedents_pregnancies),
      getResources().getString(R.string.fragment_prenatal_normal_delivery, amountOfNormalDelivery),
      getResources().getString(R.string.fragment_prenatal_caesareans, amountOfCaesareans),
      getResources().getString(R.string.fragment_prenatal_miscarriages, amountOfMiscarriages)));

    setFlags(followCardViewModel.getRisks());

    antecedentsCard.showChildCellContent();
  }

  private void setFlags(RisksViewModel risks) {
    for (String alert : risks.getAlert()) {
      antecedentsCard.addAlertFlag(alert);
    }
    for (String risk : risks.getRisk()) {
      antecedentsCard.addRiskFlag(risk);
    }
  }

  private void loadMedicalAppointmentCard() {
    if (medicalAppointmentCardSubscription == null) {
      medicalAppointmentsCard.showSkeleton();
      medicalAppointmentCardSubscription = presenter.fetchMedicalAppointments()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onMedicalAppointmentSuccess, this::onFetchMedicalAppointmentsFailure, this::successState);
      compositeSubscription.add(medicalAppointmentCardSubscription);
    }
  }

  private void onMedicalAppointmentSuccess(List<FollowCardConsultationsViewModel> followCardConsultations) {
    if (!followCardConsultations.isEmpty()) {
      medicalAppointmentsCard.showChildCellContent();
      for (FollowCardConsultationsViewModel medicalAppointment : followCardConsultations) {
        medicalAppointmentsCard.addChildCellText(
          DateFormatterHelper.formatDateWithoutHours(medicalAppointment.getDate()),
          medicalAppointment.getDoctorName(),
          String.format(getResources().getString(
            R.string.fragment_prenatal_medical_appointments_weight_height),
            medicalAppointment.getWeight(), medicalAppointment.getHeight()),
          String.valueOf(medicalAppointment.getEdema()),
          String.format(getResources().getString(
            R.string.fragment_prenatal_medical_appointments_weight_height),
            medicalAppointment.getBloodPressureDiastolic(),
            medicalAppointment.getBloodPressureSystolic()),
          String.format(getResources().getString(
            R.string.fragment_prenatal_medical_appointments_arterial_uterine_height),
            medicalAppointment.getUterineHeight()),
          medicalAppointment.getFetalPresentation(),
          String.format(getString(R.string.fragment_prenatal_medical_appointments_fetal_heart_rate),
            medicalAppointment.getFetalHeartRate()),
          verify(medicalAppointment.getFetalMovement()),
          verify(medicalAppointment.getUsingFerricSulfate()),
          verify(medicalAppointment.getUsingFolicAcid()),
          medicalAppointment.getToque(), medicalAppointment.getReason());
      }
    } else {
      medicalAppointmentsCard.showNoContentPlaceholder();
    }
  }

  private String verify(Integer fetalMovement) {
    if (fetalMovement == null) {
      return "";
    }
    return fetalMovement.equals(0) ? getString(R.string.no) : getString(R.string.yes);
  }

  private void loadExamsCard() {
    if (examsCardSubscription == null) {
      examsCard.showSkeleton();
      examsCardSubscription = presenter.fetchExamsCard()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onExamsSuccess, this::onFetchExamsFailure, this::successState);
      compositeSubscription.add(examsCardSubscription);
    }
  }

  private void onExamsSuccess(List<FollowCardExamsViewModel> followCardExamsViewModels) {
    if (!followCardExamsViewModels.isEmpty()) {
      examsCard.showChildCellContent();
      for (FollowCardExamsViewModel followCardExams : followCardExamsViewModels) {
        examsCard.addChildCellText(followCardExams.getName(),
          DateFormatterHelper.formatDateWithoutHours(followCardExams.getDate()),
          followCardExams.getResult());
      }
    } else {
      examsCard.showNoContentPlaceholder();
    }
  }

  private void loadUltraSoundCard() {
    if (ultraSoundCardSubscription == null) {
      ultraSoundCard.showSkeleton();
      ultraSoundCardSubscription = presenter.fetchUltraSoundCard()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onUltraSoundSuccess, this::onFetchUltrasoundFailure, this::successState);
      compositeSubscription.add(ultraSoundCardSubscription);
    }
  }

  private void onUltraSoundSuccess(List<FollowCardUltrasoundsViewModel> followCardUltrasoundsViewModels) {
    if (!followCardUltrasoundsViewModels.isEmpty()) {
      ultraSoundCard.showChildCellContent();
      for (FollowCardUltrasoundsViewModel followCardUltrasounds : followCardUltrasoundsViewModels) {
        ultraSoundCard.addChildCellText(
          DateFormatterHelper.formatDateWithoutHours(followCardUltrasounds.getDate()),
          followCardUltrasounds.getDoctorName(),
          String.valueOf(followCardUltrasounds.getPeriodPregnancyWeeks()),
          String.valueOf(followCardUltrasounds.getUltrasoundPregnancyWeeks()),
          String.valueOf(followCardUltrasounds.getFetalWeight()),
          followCardUltrasounds.getPlacenta(), followCardUltrasounds.getLiquid(),
          followCardUltrasounds.getPbf());
      }
    } else {
      ultraSoundCard.showNoContentPlaceholder();
    }
  }

  private void loadVaccinesCard() {
    if (vaccinesCardSubscription == null) {
      vaccinesCard.showSkeleton();
      vaccinesCardSubscription = presenter.fetchVaccinesCard()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onVaccinesSuccess, this::onFetchVaccinesFailure, this::successState);
      compositeSubscription.add(vaccinesCardSubscription);
    }
  }


  private void onVaccinesSuccess(List<FollowCardVaccinesViewModel> followCardVaccinesViewModels) {
    if (!followCardVaccinesViewModels.isEmpty()) {
      vaccinesCard.showChildCellContent();
      for (FollowCardVaccinesViewModel followCardVaccines : followCardVaccinesViewModels) {
        vaccinesCard.addChildCellText(followCardVaccines.getName(),
          DateFormatterHelper.formatDateWithoutHours(followCardVaccines.getCreatedAt()));
      }
    } else {
      vaccinesCard.showNoContentPlaceholder();
    }
  }

  private void onFetchAntecedentsFailure(Throwable throwable) {
    antecedentsCard.showErrorPlaceholder();
    showOnFetchFailureSnackbar(throwable);
  }

  private void onFetchMedicalAppointmentsFailure(Throwable throwable) {
    medicalAppointmentsCard.showErrorPlaceholder();
    showOnFetchFailureSnackbar(throwable);
  }

  private void onFetchExamsFailure(Throwable throwable) {
    examsCard.showErrorPlaceholder();
    showOnFetchFailureSnackbar(throwable);
  }

  private void onFetchUltrasoundFailure(Throwable throwable) {
    ultraSoundCard.showErrorPlaceholder();
    showOnFetchFailureSnackbar(throwable);
  }

  private void onFetchVaccinesFailure(Throwable throwable) {
    vaccinesCard.showErrorPlaceholder();
    showOnFetchFailureSnackbar(throwable);
  }

  private void showOnFetchFailureSnackbar(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  private void successState() {
  }

  @Override
  public void onResume() {
    super.onResume();
    setupToolbar();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_prenatal_toolbar_title));
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(PrenatalCardContract.Presenter presenter) {
    this.presenter = presenter;
  }
}
