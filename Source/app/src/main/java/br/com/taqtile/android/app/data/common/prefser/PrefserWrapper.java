package br.com.taqtile.android.app.data.common.prefser;

import android.location.Location;

import com.fernandocejas.arrow.optional.Optional;
import com.github.pwittchen.prefser.library.Prefser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

import br.com.taqtile.android.app.data.common.Toolbox.ToolBoxResponseError;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import rx.Emitter;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public class PrefserWrapper {
  private Prefser prefser;

  public PrefserWrapper(Prefser prefser) {
    this.prefser = prefser;
  }

  public Observable<Boolean> getBoolean(String key) {
    return emitBooleanObservable(key, false);
  }

  public Observable<String> getString(String key) {
    return emitStringObservable(key, "");
  }

  public Observable<Integer> getInteger(String key) {
    return emitIntegerObservable(key, 0);
  }

  public Observable<EmptyResult> putBoolean(String key, boolean value) {
    return putObject(key, value);
  }

  public Observable<EmptyResult> putString(String key, String value) {
    return putObject(key, value);
  }

  public Observable<EmptyResult> putInteger(String key, Integer value) {
    return putObject(key, value);
  }

  public Observable<EmptyResult> putLocation(String key, Location value) {
    return putObject(key, value);
  }

  public Observable<EmptyResult> putOptionalString(String key, Optional<String> optionalString) {
    return putOptionalObject(key, optionalString);
  }

  public <S> Observable<S> getFromJsonString(String key, Type objectType) {
    // Can not create Type using generics as if <S> is something like List<T>,
    //  Type will be wrongly create as List<LinkedTreeMap>, ignoring T completely.
    //  So the solution is to receive a Type instance created without generics usage.
    //  http://stackoverflow.com/a/23018567/429521
    return getString(key)
      .map((json) -> new Gson().fromJson(json, objectType));
  }

  public Observable<EmptyResult> putAsJsonString(String key, Object value) {
    GsonBuilder builder = new GsonBuilder();
    return putString(key, builder.create().toJson(value));
  }

  private Observable<Boolean> emitBooleanObservable(String key, Boolean defaultValue) {
    return getObject(key, defaultValue, Boolean.class);
  }

  private Observable<String> emitStringObservable(String key, String defaultValue) {
    return getObject(key, defaultValue, String.class);
  }

  private Observable<Integer> emitIntegerObservable(String key, Integer defaultValue) {
    return getObject(key, defaultValue, Integer.class);
  }

  public Observable<EmptyResult> remove(String key) {
    return removeObject(key);
  }

  public Observable<EmptyResult> clear() {
    return Observable.fromEmitter(emitter -> {
      prefser.clear();

      emitter.onNext(new EmptyResult());
      emitter.onCompleted();
    }, Emitter.BackpressureMode.NONE);
  }

  //region Generic methods

  private <S> Observable<EmptyResult> putOptionalObject(String key, Optional<S> optionalValue) {
    return Observable.fromEmitter(emitter -> {
      if (optionalValue.isPresent()) {
        prefser.put(key, optionalValue.get());
        emitter.onNext(new EmptyResult());
        emitter.onCompleted();
      } else {
        emitter.onError(getError());
      }
    }, Emitter.BackpressureMode.NONE);
  }

  private <S> Observable<EmptyResult> putObject(String key, S value) {
    return Observable.fromEmitter(emitter -> {
      prefser.put(key, value);

      emitter.onNext(new EmptyResult());
      emitter.onCompleted();
    }, Emitter.BackpressureMode.NONE);
  }

  private <S> Observable<S> getObject(String key, S defaultValue, Class<S> classOfT) {
    return Observable.fromEmitter(emitter -> {
      S value = prefser.get(key, classOfT, defaultValue);

      emitter.onNext(value);
      emitter.onCompleted();
    }, Emitter.BackpressureMode.NONE);
  }

  private Observable<EmptyResult> removeObject(String key) {
    return Observable.fromEmitter(emitter -> {
      prefser.remove(key);

      emitter.onNext(new EmptyResult());
      emitter.onCompleted();
    }, Emitter.BackpressureMode.NONE);
  }

  //endregion
  private Throwable getError() {
    return new Throwable(ToolBoxResponseError.ErrorType.InvalidData.name());
  }
}
