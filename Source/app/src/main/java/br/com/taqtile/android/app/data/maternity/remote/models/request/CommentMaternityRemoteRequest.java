package br.com.taqtile.android.app.data.maternity.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/23/17.
 */

public class CommentMaternityRemoteRequest extends BaseRemoteRequest {
    private String comment;

    public void setComment(String comment) {
        this.comment = comment;
    }
}
