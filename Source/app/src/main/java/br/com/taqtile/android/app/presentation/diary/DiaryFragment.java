package br.com.taqtile.android.app.presentation.diary;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.RemoveAppointmentParams;
import br.com.taqtile.android.app.listings.adapters.AppointmentFeedAdapter;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorBuilder;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorColors;
import br.com.taqtile.android.app.listings.builders.VerticalDividerItemDecorationBuilder;
import br.com.taqtile.android.app.listings.cells.AppointmentFeedCell;
import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import br.com.taqtile.android.app.listings.cells.SectionHeader;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemSeparatorViewModel;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.AgendaFeedViewModel;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.diary.model.DiaryViewModel;
import br.com.taqtile.android.app.presentation.tutorial.TutorialDiaryComponent;
import br.com.taqtile.android.app.presentation.tutorial.TutorialDiaryPageViewModel;
import br.com.taqtile.android.app.support.ExternalIntent;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java8.util.stream.StreamSupport;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams.DONE;
import static br.com.taqtile.android.app.domain.agenda.models.FinishAppointmentParams.TODO;

/**
 * Created by taqtile on 04/05/17.
 */

public class DiaryFragment extends AppBaseFragment implements AppointmentFeedCell.Listener,
  TutorialDiaryComponent.Listener, MenuItem.OnMenuItemClickListener,
  SectionHeader.SectionHeaderListener, HorizontalSelectorCell.Listener, ViewTreeObserver.OnGlobalLayoutListener {

  @BindView(R.id.fragment_diary_container)
  LinearLayout diaryContainer;

  @BindView(R.id.fragment_diary_weeks_selector_container)
  LinearLayout weekSelectorContainer;

  @BindView(R.id.fragment_diary_appointment_feed)
  RecyclerView appointmentFeedRecyclerview;

  @BindView(R.id.fragment_diary_diary_percentage_chart)
  PieChart diaryPercentageChart;

  @BindView(R.id.fragment_diary_scheduled_appointments_chart)
  PieChart scheduleAppointmentsChart;

  @BindView(R.id.fragment_diary_appointments_chart_text)
  CustomTextView scheduleAppointmentChartText;

  @BindView(R.id.fragment_diary_diary_percentage_chart_text)
  CustomTextView diaryPercentageChartText;

  @BindView(R.id.fragment_diary_scroll_view)
  NestedScrollView scrollView;

  @BindView(R.id.fragment_diary_appointment_feed_section_header)
  SectionHeader appointmentFeedSectionHeader;

  @BindView(R.id.fragment_diary_pie_charts_container)
  LinearLayout pieChartsContainer;

  @BindView(R.id.fragment_diary_no_appointment_placeholder)
  CustomTextView noAppointmentPlaceholder;

  @BindView(R.id.fragment_diary_loading)
  ProgressBarLoading loading;

  @BindView(R.id.fragment_diary_weeks_selector_scroll_view)
  HorizontalScrollView weeksSelectorScrollView;

  private NavigationManager navigationManager;
  private DiaryContract.Presenter presenter;
  private CustomToolbar customToolbar;
  private ViewTreeObserver.OnGlobalLayoutListener toolbarGlobalLayoutListener;

  private AppointmentFeedAdapter appointmentFeedAdapter;

  private HorizontalSelectorBuilder weekSelectorBuilder;
  private HorizontalSelectorColors trimesterColors;

  private List<AgendaFeedViewModel> agendaItems;
  private String currentWeek;
  private String gestationWeek;
  private int totalAppointments;
  private int totalAppointmentsAccomplishedOrScheduled;
  private double appointmentsScheduledPercentage;
  private double appointmentsAccomplishedPercentage;
  private TreeMap<Integer, List<AgendaItemViewModel>> weekAppointments;
  private Subscription checkUserDataSubscription;
  private Subscription listSubscription;
  private Subscription finishAppointmentSubscription;

  private TutorialDiaryComponent tutorialDiaryComponent;
  private Boolean tutorialFinished = true;
  private static final Integer MAX_WEEK = 42;
  private boolean autoScroll;
  private boolean reloadWeekSelector;

  public static DiaryFragment newInstance() {
    return new DiaryFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_diary, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setupRecyclerview();
    setupListeners();
    setupButtons();

  }

  @Override
  public void willAppear() {
    autoScroll = true;
    setupToolbar();

    hideNoAppointmentPlaceholder();
    checkUserData();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setupButtons() {
  }

  private void showNoAppointmentPlaceholder() {
    noAppointmentPlaceholder.setVisibility(View.VISIBLE);
    appointmentFeedRecyclerview.setVisibility(View.GONE);
  }

  private void hideNoAppointmentPlaceholder() {
    noAppointmentPlaceholder.setVisibility(View.GONE);
    appointmentFeedRecyclerview.setVisibility(View.VISIBLE);
  }

  private void setupRecyclerview() {
    this.reloadWeekSelector = true;
    this.autoScroll = true;
    this.weekAppointments = new TreeMap<>();
    this.agendaItems = new ArrayList<>();
    appointmentFeedAdapter = new AppointmentFeedAdapter(this, agendaItems);
    appointmentFeedRecyclerview.setAdapter(appointmentFeedAdapter);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    appointmentFeedRecyclerview.setLayoutManager(linearLayoutManager);
    appointmentFeedRecyclerview.setNestedScrollingEnabled(false);
  }

  private void setupListeners() {
    appointmentFeedSectionHeader.setSectionHeaderListener(this);
  }

  private void checkUserData() {
    showLoading();

    if (checkUserDataSubscription != null) {
      checkUserDataSubscription.unsubscribe();
    }

    checkUserDataSubscription = presenter.checkUserData()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onFetchUserDataSuccess,
        this::onFetchUserDataFailure);
    compositeSubscription.add(checkUserDataSubscription);

  }

  private void onFetchUserDataSuccess(UserViewModel userViewModel) {
    this.currentWeek = userViewModel.getGestationWeeks() != null ?
      String.valueOf(userViewModel.getGestationWeeks()) : "0";
    validateCurrentWeek();

    loadData();
  }

  private void validateCurrentWeek() {
    if (weekAppointments != null && !weekAppointments.isEmpty()) {
      this.currentWeek = getValidWeek();
    }
    this.gestationWeek = this.currentWeek;
  }

  private String getValidWeek() {

    Integer currentWeek = Integer.valueOf(this.currentWeek);

    while (validCurrentWeek() && weekWithoutAppointment(currentWeek)) {
      currentWeek++;
      reloadWeekSelector = true;
    }

    return String.valueOf(currentWeek);
  }

  private boolean weekWithoutAppointment(Integer currentWeek) {
    return !weekAppointments.containsKey(currentWeek) && currentWeek < MAX_WEEK;
  }

  private boolean validCurrentWeek() {
    return weekAppointments != null && weekAppointments.size() > 1;
  }

  private void onFetchUserDataFailure(Throwable throwable) {
    handlePresenterError(throwable);
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.hideLeftButton();
      customToolbar.setTitle(getResources().getString(R.string.fragment_diary_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), 0);
      customToolbar.inflateOptionsMenu(R.menu.menu_agenda, this::onMenuItemClick);
    }
  }

  private void loadData() {
    showLoading();

    if (listSubscription != null) {
      listSubscription.unsubscribe();
    }

    listSubscription = presenter.listAppointments()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onListSuccess, this::onListFailure, this::hideLoading);
    compositeSubscription.add(listSubscription);
  }

  private void onListFailure(Throwable throwable) {
    handlePresenterError(throwable);
  }

  private void handlePresenterError(Throwable throwable) {
    hideLoading();
    if (weekAppointments == null || weekAppointments.isEmpty()) {
      handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    } else {
      showSnackBar(throwable.getMessage());
    }
  }

  private void onListSuccess(DiaryViewModel diaryViewModel) {
    resetWeekAppointments();
    this.weekAppointments.putAll(diaryViewModel.getWeeksAppointments());

    validateCurrentWeek();

    showAppointments(diaryViewModel.showDiaryTutorial());

    calculateDiaryStatistics();
    showCharts();
    populateWeekSelector();

    if (diaryViewModel.showDiaryTutorial()) {
      final TutorialDiaryComponent.Listener listener = this;
      setupToolbarGlobalLayoutListener(listener);
      addToolbarGlobalLayoutListener();
    }
  }

  private void resetWeekAppointments() {
    if (this.weekAppointments != null) {
      this.weekAppointments.clear();
    }
  }

  private void setupToolbarGlobalLayoutListener(TutorialDiaryComponent.Listener listener) {
    toolbarGlobalLayoutListener = () -> {
      if (tutorialDiaryComponent == null) {
        removeToolbarGlobalLayoutListener();

        ViewGroup parent = (ViewGroup) getActivity().findViewById(android.R.id.content);
        int parentIndex = parent.getChildCount();
        tutorialDiaryComponent = new TutorialDiaryComponent(getContext(), buildTutorialContent(customToolbar), listener);
        parent.addView(tutorialDiaryComponent, parentIndex);
      }
    };
  }

  private void addToolbarGlobalLayoutListener() {
    getToolbar().getViewTreeObserver().addOnGlobalLayoutListener(toolbarGlobalLayoutListener);
  }

  private void removeToolbarGlobalLayoutListener() {
    getToolbar().getViewTreeObserver().removeOnGlobalLayoutListener(toolbarGlobalLayoutListener);
  }

  private void showAppointments(boolean showTutorial) {

    resetAppointments();

    if (weekAppointments.containsKey(Integer.valueOf(currentWeek)) || showTutorial) {
      this.agendaItems.addAll(weekAppointments.get(showTutorial ? weekAppointments.firstKey() :
        Integer.valueOf(currentWeek)));

      hideNoAppointmentPlaceholder();
      agendaItems = VerticalDividerItemDecorationBuilder.getListWithSeparators(agendaItems,
        new AgendaItemSeparatorViewModel());
    } else {
      showNoAppointmentPlaceholder();
    }

    appointmentFeedAdapter.setAgendaItems(agendaItems);
    appointmentFeedAdapter.notifyDataSetChanged();

  }

  private void showCharts() {
    setupPieCharts();
    animateCharts();
  }

  private void calculateDiaryStatistics() {
    this.totalAppointments = 0;
    List<AgendaFeedViewModel> totalAppointmentUntilCurrentWeek = new ArrayList<>();

    for (Map.Entry<Integer, List<AgendaItemViewModel>> entry : weekAppointments.entrySet()) {
      Integer week = entry.getKey();
      List<AgendaItemViewModel> appointments = entry.getValue();
      if (week <= Integer.valueOf(gestationWeek)) {
        this.totalAppointments += appointments.size();
        totalAppointmentUntilCurrentWeek.addAll(appointments);
      }
    }

    this.totalAppointmentsAccomplishedOrScheduled = presenter.calculateAccomplishedOrScheduledTotal(totalAppointmentUntilCurrentWeek);
    int totalAppointmentsAccomplished = presenter.calculateAccomplishedTotal(totalAppointmentUntilCurrentWeek);
    this.appointmentsAccomplishedPercentage = presenter.calculatePercentage (totalAppointmentUntilCurrentWeek,
      totalAppointmentsAccomplished);
    this.appointmentsScheduledPercentage = presenter.calculatePercentage (totalAppointmentUntilCurrentWeek,
      totalAppointmentsAccomplishedOrScheduled);
  }

  private void resetAppointments() {
    this.agendaItems.clear();
    this.totalAppointments = 0;
    this.totalAppointmentsAccomplishedOrScheduled = 0;
    this.appointmentsAccomplishedPercentage = 0;
    this.appointmentsScheduledPercentage = 0;
  }

  private void setupPieCharts() {
    ArrayList<Double> pieChartData = getPieChartShare(appointmentsAccomplishedPercentage);
    setPieChartData(pieChartData, diaryPercentageChart);
    pieChartData = getPieChartShare(appointmentsScheduledPercentage);
    setPieChartData(pieChartData, scheduleAppointmentsChart);
    setScheduleAppointmentChartText(String.format(
      getString(R.string.fragment_diary_piechart_absolute_value),
      totalAppointmentsAccomplishedOrScheduled, totalAppointments));
    setDiaryPercentageChartText(String.format(
      getString(R.string.fragment_diary_piechart_percent_value), appointmentsAccomplishedPercentage));
  }

  //Use this to populate the selector
  private void populateWeekSelector() {
    if (weekSelectorBuilder == null || reloadWeekSelector) {
      setupTrimesterColors();

      //The last argument is the mother' pregnancy week, which should always appear with a different color
      resetWeekSelector();
      weekSelectorBuilder = new HorizontalSelectorBuilder(weekSelectorContainer,
        trimesterColors.getTextActiveColor(), gestationWeek, getContext());

      addTrimesterWeeks(this.weekAppointments.keySet(),
        trimesterColors.getTextActiveColor(), trimesterColors.getTextInactiveColor(),
        trimesterColors.getBackgroundSelectedColor());

      weekSelectorBuilder.setDefaultTextPaintedCell();

    }

    weekSelectorBuilder.setSelectedCell(Integer.valueOf(currentWeek) > MAX_WEEK ?
      String.valueOf(MAX_WEEK) : currentWeek);

    if (autoScroll) {
      weekSelectorBuilder.getCellToSelect(currentWeek).requestFocus();
      weeksSelectorScrollView.getViewTreeObserver().addOnGlobalLayoutListener(this);
      this.autoScroll = false;
    }
  }

  @Override
  public void onGlobalLayout() {
    weeksSelectorScrollView.smoothScrollTo(
      ((int) weekSelectorBuilder.getCellToSelect(currentWeek).getX()), 0);
    weeksSelectorScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
  }

  private void setupTrimesterColors() {
    int textDefaultColor = ContextCompat.getColor(getContext(), R.color.color_gray_dark);
    trimesterColors = new HorizontalSelectorColors(ContextCompat.getColor(getContext(),
      R.color.color_week_text_selected_color_first_trimester),
      textDefaultColor, ContextCompat.getColor(getContext(),
      R.color.color_week_background_selected_color_first_trimester));
  }

  private void addTrimesterWeeks(Set<Integer> weeks, int textActiveColor, int textInactiveColor,
                                 int backgroundSelectedColor) {
    for (Integer selectorText : weeks) {
      weekSelectorBuilder.addSelectorCell(String.valueOf(selectorText), this, textActiveColor,
        textInactiveColor, backgroundSelectedColor);
    }
  }

  @Override
  public void onSelectorClick(boolean state, String number) {
    this.currentWeek = number;
    weekSelectorBuilder.setSelectedCell(number);
    showAppointments(false);
    this.autoScroll = false;
  }

  private void animateCharts() {
    animatePieChart(diaryPercentageChart);
    animatePieChart(scheduleAppointmentsChart);
    resetPieChartAnimation(scheduleAppointmentsChart);
    resetPieChartAnimation(diaryPercentageChart);
  }

  //region Pie chart

  private ArrayList<Double> getPieChartShare(Double filledPercentage) {
    ArrayList<Double> pieChartMock = new ArrayList<>();
    pieChartMock.add(filledPercentage);
    pieChartMock.add(100 - filledPercentage);
    return pieChartMock;
  }

  public void setPieChartData(ArrayList<Double> data, PieChart pieChart) {
    ArrayList<PieEntry> chartValues = new ArrayList<>();
    ArrayList<Integer> colors = getColors();
    if (data.size() == 0) {
      chartValues.add(new PieEntry(.0f));
    } else {
      for (double pieChartValue : data) {
        chartValues.add(new PieEntry(((float) pieChartValue)));
      }
    }
    setupPieChart(chartValues, colors, pieChart);
  }

  private void setScheduleAppointmentChartText(String text) {
    scheduleAppointmentChartText.setText(text);
  }

  private void setDiaryPercentageChartText(String text) {
    diaryPercentageChartText.setText(text);
  }

  private void setupPieChart(ArrayList<PieEntry> chartValues, ArrayList<Integer> colors, PieChart pieChart) {

    // TODO: 08/05/17 Pass the value of the holeRadius to dps
    TypedValue typeHoleRadius = new TypedValue();
    getResources().getValue(R.dimen.pie_chart_default_hole_radius, typeHoleRadius, true);
    float holeRadius = typeHoleRadius.getFloat();

    PieDataSet pieDataSet = new PieDataSet(chartValues, "");

    pieDataSet.setColors(colors);
    pieDataSet.setDrawValues(false);

    PieData pieData = new PieData(pieDataSet);

    Description description = new Description();
    description.setText("");
    pieChart.setVisibility(View.VISIBLE);
    pieChart.setDescription(description);
    pieChart.setDrawCenterText(false);
    pieChart.setDrawEntryLabels(false);
    pieChart.setHoleRadius(holeRadius);
    pieChart.setHoleColor(Color.TRANSPARENT);
    pieChart.setTransparentCircleRadius(0);
    pieChart.getLegend().setEnabled(false);
    pieChart.setTouchEnabled(false);

    pieChart.setData(pieData);
    pieChart.invalidate();
    pieChart.notifyDataSetChanged();
  }

  private ArrayList<Integer> getColors() {
    ArrayList<Integer> colors = new ArrayList<>();
    colors.add(ContextCompat.getColor(getContext(), R.color.color_white));
    colors.add(ContextCompat.getColor(getContext(), R.color.color_agenda_pie_chart_empty));
    return colors;
  }

  public void animatePieChart(PieChart pieChart) {
    pieChart.animateXY(0, 700);
  }

  public void resetPieChartAnimation(PieChart pieChart) {
    pieChart.animateXY(700, 0);
  }

  //end region

  @Override
  public void onFinishButtonClick(int id, AppointmentFeedCell appointmentFeedCell) {
    performAppointmentFinish(id, appointmentFeedCell, DONE);
  }

  @Override
  public void onUndoButtonClick(int id, AppointmentFeedCell appointmentFeedCell) {
    if(tutorialFinished) {
      performAppointmentFinish(id, appointmentFeedCell, TODO);
    }
    tutorialFinished = true;
  }

  @Override
  public void onUndoExamStateExpired(int id, AppointmentFeedCell appointmentFeedCell) {
    appointmentFeedAdapter.enableClickableView(appointmentFeedCell);
  }

  private void performAppointmentFinish(int id, AppointmentFeedCell appointmentFeedCell, int done) {
    finishAppointmentSubscription = presenter.finishAppointment(
      new FinishAppointmentParams(id, getAppointmentById(id).getIsAppointment(), done))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(success -> this.onFinishSuccess(success, appointmentFeedCell),
        error -> this.onFinishFailure(error, appointmentFeedCell),
        this::hideLoading);
    compositeSubscription.add(finishAppointmentSubscription);
  }

  @Override
  public void onCellDeleted(int id, AppointmentFeedCell appointmentFeedCell) {
    performAppointmentDelete(id);
  }

  private void performAppointmentDelete(int id) {
    Subscription removeSubscription = presenter
      .removeAppointment(new RemoveAppointmentParams(id))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onRemoveSuccess,
        this::onRemoveFailure);
    compositeSubscription.add(removeSubscription);
  }

  private void onRemoveSuccess(String successMessage) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_remove),
      getString(R.string.analytics_label_user_removed_schedule)
    ));
    loadData();
  }

  private void onRemoveFailure(Throwable throwable) {
    showSnackBar(throwable.getMessage());
  }

  private void onFinishSuccess(AgendaItemViewModel agendaItemViewModel, AppointmentFeedCell appointmentFeedCell) {
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_schedule),
      getString(R.string.analytics_event_update),
      getString(R.string.analytics_label_user_updated_schedule)
    ));
    List<AgendaItemViewModel> currentWeekAppointments = this.weekAppointments.get(Integer.valueOf(currentWeek));
    StreamSupport.stream(currentWeekAppointments)
      .forEach(appointment -> {
        if (Objects.equals(appointment.getId(), agendaItemViewModel.getId())) {
          appointment.setDone(agendaItemViewModel.getDone());
        }
      });
    appointmentFeedAdapter.enableClickableView(appointmentFeedCell);

    calculateDiaryStatistics();
    showCharts();
  }

  private void onFinishFailure(Throwable throwable, AppointmentFeedCell appointmentFeedCell) {
    showSnackBar(throwable.getMessage());
    appointmentFeedAdapter.setNormalCellState(appointmentFeedCell, false);
    appointmentFeedAdapter.enableClickableView(appointmentFeedCell);
    hideLoading();
  }

  private void showSnackBar(String message) {
    showSnackbar(message != null && !message.isEmpty() ? message :
      getString(R.string.error_generic_message), Snackbar.LENGTH_LONG);
  }

  @Override
  public void onExamCellClick(int id) {
    navigationManager.showAppointmentDetailUI(this::returningFromDetail, getAppointmentById(id));
  }

  private void returningFromDetail(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      loadData();
    }
  }

  private AgendaItemViewModel getAppointmentById(int id) {
    return (AgendaItemViewModel) StreamSupport.stream(agendaItems)
      .filter(item -> item instanceof AgendaItemViewModel)
      .filter(item -> ((AgendaItemViewModel) item).getId() == id)
      .findFirst()
      .orElse(new AgendaItemViewModel());
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    openCalendarApp();
    return true;
  }

  @Override
  public void onLinkButtonClick() {
    navigationManager.showAppointmentCreateUI(this::returningFromCreateAppointment);
  }

  private void returningFromCreateAppointment(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      resetWeekSelector();
      loadData();
    }
  }

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
    diaryContainer.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
    diaryContainer.setVisibility(View.VISIBLE);
    hidePlaceholder();
  }

  @Override
  public void hidePlaceholder() {
    super.hidePlaceholder();
    appointmentFeedRecyclerview.setVisibility(View.VISIBLE);
  }

  @Override
  public void showPlaceholder() {
    super.showPlaceholder();
    appointmentFeedRecyclerview.setVisibility(View.GONE);
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    showLoading();
    checkUserData();
    hidePlaceholder();
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  public void setPresenter(DiaryContract.Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    this.unsubscribeAll();
    if (customToolbar != null) {
      customToolbar.hideOptionsMenu();
    }
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_diary_placeholder;
  }

  private void openCalendarApp() {
    startActivity(ExternalIntent.getCalendarIntent());
  }

  private List<TutorialDiaryPageViewModel> buildTutorialContent(CustomToolbar customToolbar) {
    tutorialFinished = false;
    return Arrays.asList(buildPageWithFocusOnMenu(customToolbar), buildPageWithFocusOnWeeksSelector(), buildPageWithFocusOnAppointment());
  }

  private TutorialDiaryPageViewModel buildPageWithFocusOnMenu(CustomToolbar customToolbar) {
    return new TutorialDiaryPageViewModel(
      getToolbar().findViewById((customToolbar.getMenuItem(0).getItemId())),
      getResources().getString(R.string.tutorial_diary_menu), TutorialDiaryPageViewModel.CIRCLE, false);
  }

  private TutorialDiaryPageViewModel buildPageWithFocusOnWeeksSelector() {
    return new TutorialDiaryPageViewModel(
      ((HorizontalSelectorCell) weekSelectorContainer.getChildAt(0)).getSelectorTextView(),
      getResources().getString(R.string.tutorial_diary_weeks),
      TutorialDiaryPageViewModel.CIRCLE,
      false);
  }

  private TutorialDiaryPageViewModel buildPageWithFocusOnAppointment() {
    return new TutorialDiaryPageViewModel(
      ((AppointmentFeedCell) appointmentFeedRecyclerview.getChildAt(0)).getInfoLayout(),
      getResources().getString(R.string.tutorial_diary_appointment),
      TutorialDiaryPageViewModel.SEMI_CIRCLE_RETANGLE,
      true
    );
  }

  @Override
  public void onPageChange(int index) {
    ((AppointmentFeedCell) appointmentFeedRecyclerview.getChildAt(0)).triggerTutorialAnimation();
  }

  @Override
  public void onCloseTutorial() {
    ViewGroup parent = (ViewGroup) getActivity().findViewById(android.R.id.content);
    parent.removeView(tutorialDiaryComponent);
    saveTutorialSeen();
    appointmentFeedAdapter.setNormalCellState(((AppointmentFeedCell) appointmentFeedRecyclerview.getChildAt(0)));
  }

  private void saveTutorialSeen() {
    showLoading();
    resetAppointments();
    resetWeekAppointments();
    resetWeekSelector();
    Subscription saveSubscription = Toolbox.getInstance().saveDiaryTutorial()
      .flatMap(res -> presenter.checkUserData())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchUserDataSuccess, this::onFetchUserDataFailure);
    compositeSubscription.add(saveSubscription);
  }

  private void resetWeekSelector() {
    if (weekSelectorBuilder != null) {
      weekSelectorBuilder.clearSelector();
      weekSelectorBuilder = null;
    }
  }

}
