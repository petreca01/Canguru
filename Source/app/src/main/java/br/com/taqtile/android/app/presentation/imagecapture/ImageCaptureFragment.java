package br.com.taqtile.android.app.presentation.imagecapture;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.cleanbase.presentation.view.BaseFragment;
import id.zelory.compressor.Compressor;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 4/28/17.
 */

public class ImageCaptureFragment extends BaseFragment {

  private String currentPhotoPath;
  private ImageCaptureCallback imageCaptureCallback;
  private boolean dismissActivity = false;

  private static final int TAKE_PICTURE = 0;
  private static final int CHOOSE_PICTURE = 1;
  private static final int CHOOSE_OR_TAKE = 2;

  private static final int MAX_IMAGE_SIZE = 500;
  private static final int MAX_IMAGE_SIZE_IN_BYTES = 500000;

  private Uri contentUri = null;
  private static File photoFile;
  private static File f;

  private int actionId;
  private int requestCode;

  public static ImageCaptureFragment newInstance(ImageCaptureCallback callback) {
    ImageCaptureFragment fragment = new ImageCaptureFragment();
    fragment.imageCaptureCallback = callback;
    return fragment;
  }

  @Override
  public void onStart() {
    super.onStart();

    if (dismissActivity) {
      getActivity().finish();
    } else {
      start();
    }

    dismissActivity = true;


  }

  @Override
  public void onResume() {
    super.onResume();

  }

  private void start() {

    Intent galleryIntent = buildGalleryIntent();
    List<Intent> cameraIntentsList = buildCameraIntents();
    if (actionId == CHOOSE_OR_TAKE) {
      final Intent chooserIntent = Intent.createChooser(galleryIntent, "");
      chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntentsList.toArray(new Parcelable[cameraIntentsList.size()]));
      getActivity().startActivityForResult(chooserIntent, requestCode);
    } else if (actionId == CHOOSE_PICTURE) {
      final Intent chooserIntent = Intent.createChooser(galleryIntent, "");
      startActivityForResult(chooserIntent, requestCode);
    } else if (actionId == TAKE_PICTURE) {
      Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
      cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, getFileURI());
      startActivityForResult(cameraIntent, requestCode);
    }

  }

  private Intent buildGalleryIntent() {
    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
    return intent;

  }

  private List<Intent> buildCameraIntents() {
    List<Intent> cameraIntentsList = new ArrayList<>();

    photoFile = createTempImageFile();

    if (photoFile != null) {
      cameraIntentsList = buildCameraIntentsList(photoFile);
    }

    return cameraIntentsList;
  }

  private List<Intent> buildCameraIntentsList(File photoFile) {
    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
      Uri.fromFile(photoFile));

    return putIntentsInList(cameraIntent);
  }

  private List<Intent> putIntentsInList(Intent cameraIntent) {
    List<ResolveInfo> listCam = getActivity().getPackageManager().queryIntentActivities(cameraIntent, PackageManager.GET_META_DATA);

    return StreamSupport.stream(listCam)
      .map(res -> {
        final Intent finalIntent = new Intent(cameraIntent);
        finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
        return finalIntent;
      })
      .collect(Collectors.toList());

  }

  private File createTempImageFile() {
    File photoFile = null;
    try {
      photoFile = createImageFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return photoFile;
  }

  private File createImageFile() throws IOException {
    String imageFileName = createImageFileNamePrefix();
    File storageDir = getStorageDirectory();
    buildStorageDirectory(storageDir);

    File image = File.createTempFile(
      imageFileName,
      ".jpg",
      storageDir
    );
    image.deleteOnExit();
    this.currentPhotoPath = image.getAbsolutePath();
    return image;
  }

  private String createImageFileNamePrefix() {
    String timestamp = new SimpleDateFormat(getString(R.string.fragment_profile_menu_image_date)).format(new Date());
    return String.format("JPEG_$1%s_", timestamp);
  }

  private File getStorageDirectory() {
    return Environment.getExternalStoragePublicDirectory(
      Environment.DIRECTORY_PICTURES);
  }

  private void buildStorageDirectory(File storageDir) {
    if (!storageDir.exists()) {
      storageDir.mkdirs();
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    String imagePath;
    if (resultCode == Activity.RESULT_OK) {
      if (isFromCamera(data)) {
        galleryAddPic();
        imagePath = this.currentPhotoPath;
      } else {
        imagePath = getImagePathFromGallery(data);
      }

      File picture = new File(imagePath);
      if (picture.length() > MAX_IMAGE_SIZE_IN_BYTES) {
        imagePath = compressImage(picture);
      }

      if (this.imageCaptureCallback != null) {
        this.imageCaptureCallback.onImagePathReady(imagePath);
      }
    } else {
      deleteFiles();
      getActivity().finish();
    }
  }

  private String compressImage(File picture) {

    File compressedImage = new Compressor.Builder(getContext())
      .setMaxWidth(MAX_IMAGE_SIZE)
      .setMaxHeight(MAX_IMAGE_SIZE)
      .setQuality(100)
      .setCompressFormat(Bitmap.CompressFormat.JPEG)
      .setDestinationDirectoryPath(picture.getParent())
      .build()
      .compressToFile(picture);
    return compressedImage.getPath();
  }

  // Data from camera intent return null as it saved the image in the uri provided in the intent
  private boolean isFromCamera(Intent data) {
    Boolean bool = true;

    // Some android versions return a null data object and others return a data.getData() null
    // the following prevent crash on both cases.

    try {
      bool = (data == null);
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      bool = (data.getData() == null);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return bool;
  }

  private Uri getFileURI() {
    f = new File(this.currentPhotoPath);
    this.contentUri = Uri.fromFile(f);
    return contentUri;
  }

  private void galleryAddPic() {
    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
    if (this.contentUri == null) {
      getFileURI();
    }
    mediaScanIntent.setData(this.contentUri);
    getActivity().sendBroadcast(mediaScanIntent);
  }


  private String getImagePathFromGallery(Intent data) {
    Uri selectedImageUri = data.getData();
    return getRealPathFromURI(selectedImageUri);
  }

  private String getRealPathFromURI(Uri uri) {
    String[] projection = {MediaStore.Images.Media.DATA};
    Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

    if (cursor == null) {
      return uri.toString().replace("file://", "");
    } else {
      int columnIndex = cursor
        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      String cursorString = cursor.getString(columnIndex);
      cursor.close();
      return cursorString;
    }
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {

  }

  @Override
  public void willDisappear() {

  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }


  public interface ImageCaptureCallback {
    void onImagePathReady(String imagePath);
  }

  public void setActionId(int actionId) {
    this.actionId = actionId;
  }

  public void setRequestCode(int requestCode) {
    this.requestCode = requestCode;
  }

  public static void deleteFiles() {
    if (f != null) f.delete();
    if (photoFile != null) photoFile.delete();
  }
}

