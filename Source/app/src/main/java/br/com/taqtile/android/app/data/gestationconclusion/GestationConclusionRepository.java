package br.com.taqtile.android.app.data.gestationconclusion;

import java.util.List;
import br.com.taqtile.android.app.data.birthPlan.mappers.RemoveAnswerParamsToRemoveAnswerRemoteRequestMapper;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.gestationconclusion.mappers.GestationConclusionQuestionItemRemoteResponseListToResultMapper;
import br.com.taqtile.android.app.data.gestationconclusion.remote.GestationConclusionRemoteDataSource;
import br.com.taqtile.android.app.data.gestationconclusion.remote.models.GestationConclusionQuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.mappers.AnswerQuestionnairesParamsToAnswerQuestionnairesRemoteRequestMapper;
import br.com.taqtile.android.app.data.view.mappers.ViewModeParamsToViewModeRemoteRequestMapper;
import br.com.taqtile.android.app.domain.birthPlan.models.RemoveAnswerParams;
import br.com.taqtile.android.app.domain.common.models.ViewModeParams;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionQuestionItemResult;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import rx.Observable;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionRepository extends BaseRepository<GestationConclusionRemoteDataSource, Void> {

  public GestationConclusionRepository(GestationConclusionRemoteDataSource birthPlanRemoteDataSource) {
    super(birthPlanRemoteDataSource, null);
  }

  public Observable<List<GestationConclusionQuestionItemResult>> listQuestionsAndAnswers(ViewModeParams viewModeParams) {
    return performMap(getRemoteDataSource().listQuestionsAndAnswers(ViewModeParamsToViewModeRemoteRequestMapper.perform(viewModeParams)));
  }

  public Observable<List<GestationConclusionQuestionItemResult>> answer(AnswerQuestionnairesParams answerQuestionnairesParams) {
    return performMap(getRemoteDataSource().answer(AnswerQuestionnairesParamsToAnswerQuestionnairesRemoteRequestMapper
      .perform(answerQuestionnairesParams)));
  }

  public Observable<List<GestationConclusionQuestionItemResult>> removeAnswer(RemoveAnswerParams removeAnswerParams) {

    return performMap(getRemoteDataSource().removeAnswer(RemoveAnswerParamsToRemoveAnswerRemoteRequestMapper.perform(removeAnswerParams)));
  }

  private Observable<List<GestationConclusionQuestionItemResult>> performMap(
    Observable<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>> remoteResponseObservable) {
    return remoteResponseObservable
      .map(result -> GestationConclusionQuestionItemRemoteResponseListToResultMapper.perform(result.getResult()));
  }
}
