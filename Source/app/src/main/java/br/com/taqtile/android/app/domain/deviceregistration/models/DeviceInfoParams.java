package br.com.taqtile.android.app.domain.deviceregistration.models;

/**
 * Created by taqtile on 3/30/17.
 */

public class DeviceInfoParams {
  private String token;

  public DeviceInfoParams(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }
}
