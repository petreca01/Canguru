package br.com.taqtile.android.app.data.riskAnalysis.remote.models;

import br.com.taqtile.android.app.data.questionnaires.models.QuestionItemRemoteResponse;
import br.com.taqtile.android.app.data.questionnaires.models.RangedAnswerRemoteResponse;
import br.com.taqtile.android.app.support.GlobalConstants;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by taqtile on 3/24/17.
 */

public class RiskAnalysisQuestionsRemoteResponse {
  @SerializedName(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOU)
  private List<QuestionItemRemoteResponse>
    aboutYou;

  @SerializedName(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_OBSTETRICAL_PAST)
  private List<QuestionItemRemoteResponse>
    aboutYourObstetricalPast;

  @SerializedName(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_FAMILY)
  private List<QuestionItemRemoteResponse>
    aboutYourFamily;

  @SerializedName(GlobalConstants.RISK_ANALYSIS_GROUP_ABOUT_YOUR_HEALTH)
  private List<QuestionItemRemoteResponse>
    aboutYourHealth;

  public List<QuestionItemRemoteResponse> getAboutYou() {
    return aboutYou;
  }

  public List<QuestionItemRemoteResponse> getAboutYourObstetricalPast() {
    return aboutYourObstetricalPast;
  }

  public List<QuestionItemRemoteResponse> getAboutYourFamily() {
    return aboutYourFamily;
  }

  public List<QuestionItemRemoteResponse> getAboutYourHealth() {
    return aboutYourHealth;
  }
}
