package br.com.taqtile.android.app.presentation.medicalrecommendations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.listings.adapters.MedicalRecommendationsAdapter;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.medicalrecommendationdetail.MedicalRecommendationDetailBundleCreator;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsFragment extends AppBaseFragment {

  @BindView(R.id.fragment_medical_recommendations_recycler_view)
  RecyclerView
    medicalRecommendationsRecyclerView;

  private NavigationManager navigationManager;
  private MedicalRecommendationsContract.Presenter presenter;
  private CustomToolbar customToolbar;

  protected MedicalRecommendationsAdapter medicalRecommendationsAdapter;
  protected LinearLayoutManager linearLayoutManager;
  protected List<MedicalRecommendationViewModel> medicalRecommendationsList = new ArrayList<>();
  private Subscription recommendationSubscription;

  public static MedicalRecommendationsFragment newInstance() {
    return new MedicalRecommendationsFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_medical_recommendations, container, false);

    ButterKnife.bind(this, root);

    return root;
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();
    setupRecyclerView();
    fetchRecommendations();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.setTitle(getString(R.string.fragment_medical_recommendations_title));
    }
  }

  private void setupRecyclerView() {
    linearLayoutManager = new LinearLayoutManager(getContext());
    medicalRecommendationsRecyclerView.setLayoutManager(linearLayoutManager);
    medicalRecommendationsAdapter = new MedicalRecommendationsAdapter(medicalRecommendationsList);
    medicalRecommendationsAdapter.setListener(this::onRecommendationClicked);
    medicalRecommendationsRecyclerView.setAdapter(medicalRecommendationsAdapter);
  }

  private void fetchRecommendations() {
    if (recommendationSubscription == null) {
      medicalRecommendationsRecyclerView.setVisibility(View.GONE);
      showLoading();
      Subscription subscription = presenter.fetchRecommendations()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
          this::onFetchRecommendationsSuccess,
          this::onFetchRecommendationsFailure
        );
      compositeSubscription.add(subscription);
    }
  }

  private void onFetchRecommendationsSuccess(List<MedicalRecommendationViewModel> medicalRecommendationViewModelList) {
    hideLoading();
    if (!medicalRecommendationViewModelList.isEmpty()) {
      medicalRecommendationsList.clear();
      medicalRecommendationsList.addAll(medicalRecommendationViewModelList);
      medicalRecommendationsAdapter.notifyDataSetChanged();
      medicalRecommendationsRecyclerView.setVisibility(View.VISIBLE);
    } else {
      showNoMedicalRecommendationsPlaceholder();
    }
  }

  private void showNoMedicalRecommendationsPlaceholder(){
    placeholder.setPlaceholderType(CustomPlaceholder.NO_MEDICAL_RECOMMENDATIONS);
    showPlaceholder();
  }

  private void onFetchRecommendationsFailure(Throwable error) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(error));
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void onPlaceholderButtonClick() {
    hidePlaceholder();
    fetchRecommendations();
  }

  private void onRecommendationClicked(
    MedicalRecommendationViewModel medicalRecommendationViewModel) {
    Bundle bundle =
      MedicalRecommendationDetailBundleCreator.getBundle(medicalRecommendationViewModel);
    navigationManager.showMedicalRecommendationDetail(bundle);
  }

  public void setPresenter(MedicalRecommendationsContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_medical_recommendations_placeholder;
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_medical_recommendation),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
