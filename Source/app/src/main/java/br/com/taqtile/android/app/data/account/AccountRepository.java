package br.com.taqtile.android.app.data.account;

import com.fernandocejas.arrow.optional.Optional;

import br.com.taqtile.android.app.data.account.remote.AccountRemoteDataSource;
import br.com.taqtile.android.app.data.account.remote.Mappers.AccountParamsToAccountRemoteResquestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.AccountSignInWithFacebookToFacebookAccountRemoteRequestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.EditUserDataParamsToRequestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.EditUserPasswordParamsToEditUserDataRemoteResquestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.ForgotPasswordParamsToForgotPasswordRemoteRequestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.ForgotPasswordRemoteResponseToForgotPasswordMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.SignUpParamsToSignUpRemoteRequestMapper;
import br.com.taqtile.android.app.data.account.remote.Mappers.StartGestationParamsToEditUserRemoteRequestMapper;
import br.com.taqtile.android.app.data.account.sharedpreferences.AccountSharedPreferencesDataSource;
import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.common.mappers.UserRemoteResponseToUserResultMapper;
import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.models.EditUserDataParams;
import br.com.taqtile.android.app.domain.account.models.EditUserPasswordParams;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordParams;
import br.com.taqtile.android.app.domain.account.models.ForgotPasswordResult;
import br.com.taqtile.android.app.domain.account.models.SignUpParams;
import br.com.taqtile.android.app.domain.common.models.UserResult;
import br.com.taqtile.android.app.domain.gestation.models.StartOrEditGestationParams;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInParams;
import br.com.taqtile.android.app.domain.signin.Models.AccountSignInWithFacebookParams;
import rx.Observable;

/**
 * Created by taqtile on 3/19/17.
 */

public class AccountRepository
  extends BaseRepository<AccountRemoteDataSource, AccountSharedPreferencesDataSource> {

  public AccountRepository(AccountRemoteDataSource accountRemoteDataSource,
                           AccountSharedPreferencesDataSource accountSharedPreferencesDataSource) {
    super(accountRemoteDataSource, accountSharedPreferencesDataSource);
  }

  public Observable<EmptyResult> login(AccountSignInParams data) {
    return getRemoteDataSource().signIn(AccountParamsToAccountRemoteResquestMapper.perform(data))
      .flatMap(result -> this.saveUserData(result.getResult()))
      .map(any -> new EmptyResult());
  }

  private Observable<UserResult> saveUserData(UserRemoteResponse response) {
    return getLocalDataSource().saveToken(Optional.of(response.getToken()))
      .flatMap(emptyResult -> this.saveUserId(response, emptyResult))
      .flatMap(emptyResult -> this.savePrivateUserData(response))
      .flatMap(emptyResult -> this.savePublicUserData(response))
      .map(result -> UserRemoteResponseToUserResultMapper.perform(response));
  }

  public Observable<EmptyResult> savePrivateUserData(UserRemoteResponse response) {
    return this.saveUserHasCPFAndPhoneNumber(response)
      .flatMap(emptyResult -> this.saveUserZipCode(response, emptyResult));
  }

  public Observable<EmptyResult> savePublicUserData(UserRemoteResponse response) {
    return this.saveProfilePictureURL(response, new EmptyResult())
      .flatMap(emptyResult -> this.saveUserName(response, emptyResult))
      .flatMap(emptyResult -> this.saveUserEmail(response, emptyResult));
  }

  private Observable<EmptyResult> saveProfilePictureURL(UserRemoteResponse response,
                                                        EmptyResult emptyResult) {
    if (response.getProfilePicture() != null) {
      return getLocalDataSource().saveProfilePictureURL(response.getProfilePicture());
    }
    return Observable.just(emptyResult);
  }

  private Observable<EmptyResult> saveUserId(UserRemoteResponse response,
                                             EmptyResult emptyResult) {
    if (response.getId() != null) {
      return getLocalDataSource().saveUserId(response.getId());
    }
    return Observable.just(emptyResult);
  }

  private Observable<EmptyResult> saveUserName(UserRemoteResponse response,
                                               EmptyResult emptyResult) {
    if (response.getName() != null) {
      return getLocalDataSource().saveUserName(response.getName());
    }
    return Observable.just(emptyResult);
  }

  private Observable<EmptyResult> saveUserEmail(UserRemoteResponse response,
                                               EmptyResult emptyResult) {
    if (response.getEmail() != null) {
      return getLocalDataSource().saveUserEmail(response.getEmail());
    }
    return Observable.just(emptyResult);
  }

  private Observable<EmptyResult> saveUserHasCPFAndPhoneNumber(UserRemoteResponse response) {
    return getLocalDataSource().saveUserHasCPFAndPhoneNumber(
      response.getCpf() != null && response.getCellphoneNumber() != null);
  }

  private Observable<EmptyResult> saveUserZipCode(UserRemoteResponse response, EmptyResult emptyResult) {
    if (response.getZipCode() != null) {
      return getLocalDataSource().saveUserZipCode(response.getZipCode());
    }
    return Observable.just(emptyResult);
  }

  public Observable<ForgotPasswordResult> passwordRecovery(
    ForgotPasswordParams forgotPasswordParams) {
    return getRemoteDataSource().passwordRecovery(
      ForgotPasswordParamsToForgotPasswordRemoteRequestMapper.perform(forgotPasswordParams))
      .map(ForgotPasswordRemoteResponseToForgotPasswordMapper::perform);
  }

  public Observable<UserResult> signUp(SignUpParams signUpParams) {
    return getRemoteDataSource().signUp(
      SignUpParamsToSignUpRemoteRequestMapper.perform(signUpParams))
      .flatMap(result -> this.saveUserData(result.getResult()));
  }

  public Observable<UserResult> getUserData() {
    return getRemoteDataSource().getUserData()
      .flatMap(result -> this.saveUserData(result.getResult()).map(any -> result))
      .map(result -> UserRemoteResponseToUserResultMapper.perform(result.getResult()));
  }

  public Observable<UserResult> editUserData(EditUserDataParams editUserDataParams) {
    return getRemoteDataSource().edit(EditUserDataParamsToRequestMapper.perform(editUserDataParams))
      .map(result -> UserRemoteResponseToUserResultMapper.perform(result.getResult()));
  }

  public Observable<UserResult> editUserDataToStartGestation(StartOrEditGestationParams startOrEditGestationParams) {
    return getRemoteDataSource().edit(
      StartGestationParamsToEditUserRemoteRequestMapper.perform(startOrEditGestationParams))
      .map(result -> UserRemoteResponseToUserResultMapper.perform(result.getResult()));
  }

  public Observable<EmptyResult> editUserPassword(EditUserPasswordParams editUserPasswordParams) {
    return getRemoteDataSource().edit(
      EditUserPasswordParamsToEditUserDataRemoteResquestMapper.perform(editUserPasswordParams))
      .flatMap(result -> Observable.just(new EmptyResult()));
  }

  public Observable<UserResult> loginWithFacebook(AccountSignInWithFacebookParams data) {
    return getRemoteDataSource().signInWithFacebook(
      AccountSignInWithFacebookToFacebookAccountRemoteRequestMapper.map(data))
      .flatMap(result -> this.saveUserData(result.getResult()));
  }

  public Observable<EmptyResult> logout() {
    return getLocalDataSource().logout();
  }
}
