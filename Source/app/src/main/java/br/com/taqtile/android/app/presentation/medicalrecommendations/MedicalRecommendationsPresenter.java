package br.com.taqtile.android.app.presentation.medicalrecommendations;

import br.com.taqtile.android.app.domain.medicalRecommendations.ListRecommendationsUseCase;
import br.com.taqtile.android.app.listings.viewmodels.MedicalRecommendationViewModel;
import br.com.taqtile.android.app.support.Injection;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 28/04/17.
 */

public class MedicalRecommendationsPresenter implements MedicalRecommendationsContract.Presenter {

  private ListRecommendationsUseCase listRecommendationsUseCase;

  public MedicalRecommendationsPresenter() {
    listRecommendationsUseCase = Injection.provideListRecommendationsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<MedicalRecommendationViewModel>> fetchRecommendations() {
    return listRecommendationsUseCase.execute(null);
  }
}
