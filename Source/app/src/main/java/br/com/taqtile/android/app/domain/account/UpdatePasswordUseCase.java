package br.com.taqtile.android.app.domain.account;

import br.com.taqtile.android.app.data.account.AccountRepository;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.account.models.EditUserPasswordParams;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class UpdatePasswordUseCase extends BaseUseCase<EmptyResult, EditUserPasswordParams, AccountRepository> {

  public UpdatePasswordUseCase(AccountRepository accountRepository) {
    super(accountRepository);
  }

  @Override
  public Observable<EmptyResult> execute(EditUserPasswordParams editUserPasswordParams) {
    return getRepository().editUserPassword(editUserPasswordParams);
  }
}
