package br.com.taqtile.android.app.data.healthOperators.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.healthOperators.remote.models.request.SearchHealthOperatorRemoteRequest;
import br.com.taqtile.android.app.data.healthOperators.remote.models.response.HealthOperatorItemRemoteResponse;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/24/17.
 */

public class HealthOperatorsRemoteDataSource extends BaseRemoteDataSource {
  private Observable<HealthOperatorsServices> services;

  public HealthOperatorsRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(HealthOperatorsServices.class);
  }

  public Observable<BaseRemoteResponse<List<HealthOperatorItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(HealthOperatorsServices::list));
  }

  public Observable<BaseRemoteResponse<List<HealthOperatorItemRemoteResponse>>> search(
    SearchHealthOperatorRemoteRequest searchRequestModel) {
    return performRequest(
      services.flatMap(services -> services.search(searchRequestModel.getFormData())));
  }
}
