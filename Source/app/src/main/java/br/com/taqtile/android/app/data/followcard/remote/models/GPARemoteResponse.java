package br.com.taqtile.android.app.data.followcard.remote.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 5/19/17.
 */

public class GPARemoteResponse {

  @SerializedName("g")
  private Integer g;

  @SerializedName("pn")
  private Integer pn;

  @SerializedName("pc")
  private Integer pc;

  @SerializedName("a")
  private Integer a;

  public Integer getG() {
    return g;
  }

  public Integer getPn() {
    return pn;
  }

  public Integer getPc() {
    return pc;
  }

  public Integer getA() {
    return a;
  }
}
