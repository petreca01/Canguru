package br.com.taqtile.android.app.data.followcard;

import java.util.List;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.followcard.remote.FollowCardRemoteDataSource;
import br.com.taqtile.android.app.data.followcard.remote.mappers.FollowCardConsultationsRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.followcard.remote.mappers.FollowCardExamsRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.followcard.remote.mappers.FollowCardRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.followcard.remote.mappers.FollowCardUltrasoundsRemoteResponseToResultMapper;
import br.com.taqtile.android.app.data.followcard.remote.mappers.FollowCardVaccinesRemoteResponseToResultMapper;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardConsultationsResult;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardExamsResult;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardResult;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardUltrasoundsResult;
import br.com.taqtile.android.app.domain.followcard.models.FollowCardVaccinesResult;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardRepository extends BaseRepository<FollowCardRemoteDataSource, Void> {

  public FollowCardRepository(FollowCardRemoteDataSource followCardRemoteDataSource) {
    super(followCardRemoteDataSource, null);
  }


  public Observable<FollowCardResult> fetchFollowCard() {
    return getRemoteDataSource().fetchFollowCard()
      .map(result -> FollowCardRemoteResponseToResultMapper.perform(result.getResult()));
  }

  public Observable<List<FollowCardConsultationsResult>> fetchFollowCardConsultations() {
    return getRemoteDataSource().fetchFollowCardConsultations()
      .map(result -> FollowCardConsultationsRemoteResponseToResultMapper.perform(result.getResult()));
  }

  public Observable<List<FollowCardExamsResult>> fetchFollowCardExams() {
    return getRemoteDataSource().fetchFollowCardExams()
      .map(result -> FollowCardExamsRemoteResponseToResultMapper.perform(result.getResult()));
  }

  public Observable<List<FollowCardVaccinesResult>> fetchFollowCardVaccines() {
    return getRemoteDataSource().fetchFollowCardVaccines()
      .map(result -> FollowCardVaccinesRemoteResponseToResultMapper.perform(result.getResult()));
  }

  public Observable<List<FollowCardUltrasoundsResult>> fetchFollowCardUltrasounds() {
    return getRemoteDataSource().fetchFollowCardUltrasounds()
      .map(result -> FollowCardUltrasoundsRemoteResponseToResultMapper.perform(result.getResult()));
  }
}
