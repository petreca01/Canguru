package br.com.taqtile.android.app.presentation.common;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.data.common.remote.models.RemoteError;
import br.com.taqtile.android.app.misc.placeholders.CustomPlaceholder;
import br.com.taqtile.android.app.support.Injection;
import br.com.taqtile.android.app.support.analytics.AnalyticsToolBox;
import br.com.taqtile.android.cleanbase.presentation.view.BaseFragmentForResult;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by taqtile on 4/25/17.
 */

public class AppBaseFragment extends BaseFragmentForResult implements CustomPlaceholder.PlaceholderListener {

  protected CompositeSubscription compositeSubscription;
  protected CustomPlaceholder placeholder;
  protected AnalyticsToolBox analytics;

  @Nullable
  @Override
  @CallSuper
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    compositeSubscription = new CompositeSubscription();
    analytics = Injection.provideAnalyticsToolBox();
    return null;
  }

  @Override
  @CallSuper
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (getPlaceholderViewContainerId() != 0) {
      setupPlaceholder();
    }
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    hidePlaceholder();
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void willAppear() {

  }

  @Override
  public void willDisappear() {
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    this.unsubscribeAll();
  }

  public void unsubscribeAll() {
    if (compositeSubscription.hasSubscriptions()) {
      compositeSubscription.clear();
    }
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return 0;
  }

  private void setupPlaceholder() {
    if (getPlaceholderViewContainerId() != 0) {
      placeholder = getPlaceholder(CustomPlaceholder.class);
      placeholder.setListener(this);
    }
  }

  protected void handleError(PresenterError error) {
    switch (error.getErrorType()) {
      case PresenterError.NO_CONNECTION:
        showNoConnectionPlaceholder();
        break;
      case PresenterError.NOT_LOGGED_IN:
        break;
      case PresenterError.NOT_FOUND:
        showNotFoundPlaceholder();
        break;
      case PresenterError.TIMEOUT:
        showServerErrorPlaceholder(getString(R.string.error_timeout_message));
        break;
      default:
        showServerErrorPlaceholder(getString(R.string.error_generic_message));
        break;
    }
  }

  protected String mapSnackBarErrorString(String error) {
    if(error == null) { return getString(R.string.error_generic_message); }
    switch (error) {
      case "":
        return getString(R.string.error_generic_message);
      case RemoteError.NO_CONNECTION:
        return getString(R.string.error_connection_message);
      case RemoteError.TIMEOUT:
        return getString(R.string.error_timeout_message);
      default:
        return error;
    }
  }

  protected void showPlaceholderByType(@CustomPlaceholder.PlaceholderType int type) {
    if (placeholder != null) {
      placeholder.setPlaceholderType(type);
      showPlaceholder();
    }
  }

  private void showNoConnectionPlaceholder() {
    if (placeholder != null) {
      placeholder.setPlaceholderType(CustomPlaceholder.CONNECTION);
      showPlaceholder();
    }
  }

  private void showServerErrorPlaceholder(String message) {
    if (placeholder != null) {
      placeholder.setPlaceholderUnexpectedError(message);
      showPlaceholder();
    }
  }

  private void showNotFoundPlaceholder() {
    if (placeholder != null) {
      placeholder.setPlaceholderType(CustomPlaceholder.NOT_FOUND);
      showPlaceholder();
    }
  }

  @Override
  public void onPlaceholderButtonClick() {
  }

  protected void showSnackbar(@StringRes int text, int duration) {
    showSnackbar(getResources().getString(text), duration);
  }

  protected void showSnackbar(@StringRes int text, @StringRes int actionText, @ColorRes int color, View.OnClickListener listener, int duration) {
    Resources resources = getResources();
    showSnackbar(
      resources.getString(text),
      resources.getString(actionText),
      ContextCompat.getColor(getContext(), color),
      listener,
      duration);
  }
}
