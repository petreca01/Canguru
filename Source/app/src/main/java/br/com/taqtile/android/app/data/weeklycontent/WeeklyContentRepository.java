package br.com.taqtile.android.app.data.weeklycontent;

import br.com.taqtile.android.app.data.common.BaseRepository;
import br.com.taqtile.android.app.data.weeklycontent.remote.WeeklyContentDataSource;
import br.com.taqtile.android.app.data.weeklycontent.remote.mappers.WeeklyContentParamsToRemoteRequestMapper;
import br.com.taqtile.android.app.data.weeklycontent.remote.mappers.WeeklyContentRemoteRequestToResultMapper;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentResult;
import rx.Observable;

/**
 * Created by taqtile on 5/16/17.
 */

public class WeeklyContentRepository extends BaseRepository<WeeklyContentDataSource, Void> {

  public WeeklyContentRepository(WeeklyContentDataSource weeklyContentDataSource) {
    super(weeklyContentDataSource, null);
  }

  public Observable<WeeklyContentResult> fetchWeeklyContent(WeeklyContentParams weeklyContentParams) {
    return getRemoteDataSource()
      .fetchWeeklyContent(WeeklyContentParamsToRemoteRequestMapper.perform(weeklyContentParams))
      .map(result -> WeeklyContentRemoteRequestToResultMapper.perform(result.getResult()));
  }
}
