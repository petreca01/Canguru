package br.com.taqtile.android.app.domain.birthPlan;

import java.util.List;

import br.com.taqtile.android.app.data.birthPlan.BirthPlanRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import rx.Observable;

/**
 * Created by taqtile on 4/19/17.
 */

public class SendBirthPlanQAUseCase extends BaseUseCase<List<QuestionItemResult>,
  AnswerQuestionnairesParams, BirthPlanRepository> {

  public SendBirthPlanQAUseCase(BirthPlanRepository birthPlanRepository) {
    super(birthPlanRepository);
  }

  @Override
  public Observable<List<QuestionItemResult>> execute(AnswerQuestionnairesParams
                                                                  answerQuestionnairesParams) {
    return getRepository().answer(answerQuestionnairesParams);
  }

}
