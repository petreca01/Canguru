package br.com.taqtile.android.app.domain.maternity;

import br.com.taqtile.android.app.data.maternity.MaternityRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import br.com.taqtile.android.app.domain.maternity.models.RateMaternityParams;
import rx.Observable;

/**
 * Created by taqtile on 4/18/17.
 */

public class RateMaternityUseCase extends BaseUseCase<MaternityDataResult, RateMaternityParams,
  MaternityRepository> {

  public RateMaternityUseCase(MaternityRepository maternityRepository) {
    super(maternityRepository);
  }

  @Override
  public Observable<MaternityDataResult> execute(RateMaternityParams rateMaternityParams) {
    return getRepository().rate(rateMaternityParams);
  }
}
