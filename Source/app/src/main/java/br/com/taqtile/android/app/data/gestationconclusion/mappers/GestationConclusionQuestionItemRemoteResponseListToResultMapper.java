package br.com.taqtile.android.app.data.gestationconclusion.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.gestationconclusion.remote.models.GestationConclusionQuestionItemRemoteResponse;
import br.com.taqtile.android.app.domain.gestationconclusion.GestationConclusionQuestionItemResult;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 7/10/17.
 */

public class GestationConclusionQuestionItemRemoteResponseListToResultMapper {

  public static List<GestationConclusionQuestionItemResult> perform(List<GestationConclusionQuestionItemRemoteResponse> questionItemRemoteResponseList) {
    return StreamSupport.stream(questionItemRemoteResponseList)
      .map(GestationConclusionQuestionItemRemoteResponseToResultMapper::perform)
      .collect(Collectors.toList());
  }

}
