package br.com.taqtile.android.app.data.channel.remote;

import java.util.List;

import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostFollowRemoteRequest;
import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostLikeReplyRemoteRequest;
import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostReplyDestroyRemoteRequest;
import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostReplyRemoteRequest;
import br.com.taqtile.android.app.data.channel.remote.models.request.ChannelPostsListRemoteRequest;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelCategoryListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelListRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostItemRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostRemoteResponse;
import br.com.taqtile.android.app.data.channel.remote.models.response.ChannelPostsRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class ChannelRemoteDataSource extends BaseRemoteDataSource {
  private Observable<ChannelServices> services;

  public ChannelRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(ChannelServices.class);
  }

  public Observable<BaseRemoteResponse<ChannelCategoryListRemoteResponse>> list() {
    return performRequest(services.flatMap(ChannelServices::list));
  }

  public Observable<BaseRemoteResponse<List<ChannelPostItemRemoteResponse>>> listPostsOfFollowedChannels() {
    return performRequest(services.flatMap(ChannelServices::listFollowingChannelsPosts));
  }

  public Observable<BaseRemoteResponse<ChannelPostsRemoteResponse>> listPosts(
    ChannelPostsListRemoteRequest listPostsRequestModel) {
    return performRequest(
      services.flatMap(services -> services.listPostsOfAChannel(listPostsRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<ChannelPostRemoteResponse>> post(
    ChannelPostLikeReplyRemoteRequest postRequestModel) {
    return performRequest(services.flatMap(services -> services.post(postRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<ChannelPostRemoteResponse>> postReply(
    ChannelPostReplyRemoteRequest postReplyRequestModel) {
    return performRequest(
      services.flatMap(services -> services.postReply(postReplyRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<ChannelPostRemoteResponse>> likeReply(
    ChannelPostLikeReplyRemoteRequest likeRequestModel) {
    return performRequest(
      services.flatMap(services -> services.likeReply(likeRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<ChannelPostRemoteResponse>> deleteReply(
    ChannelPostReplyDestroyRemoteRequest deleteRequestModel) {
    return performRequest(
      services.flatMap(services -> services.deleteReply(deleteRequestModel.getFormData())));
  }

  public Observable<BaseRemoteResponse<EmptyResult>> follow(
    ChannelPostFollowRemoteRequest channelPostFollowRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.follow(channelPostFollowRemoteRequest.getFormData())));
  }

}
