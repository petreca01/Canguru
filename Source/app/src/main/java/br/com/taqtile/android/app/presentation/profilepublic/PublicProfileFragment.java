package br.com.taqtile.android.app.presentation.profilepublic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.buttons.ProgressBarButton;
import br.com.taqtile.android.app.data.common.Toolbox.Toolbox;
import br.com.taqtile.android.app.domain.profile.models.EditProfileParams;
import br.com.taqtile.android.app.edittexts.AboutYouLabelEditTextCaption;
import br.com.taqtile.android.app.edittexts.CustomNameLabelEditTextCaption;
import br.com.taqtile.android.app.listings.adapters.PublicProfileAdapter;
import br.com.taqtile.android.app.listings.cells.CommunityCard;
import br.com.taqtile.android.app.listings.viewmodels.ProfileDetailsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SocialFeedViewModel;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.CounterWithDrawable;
import br.com.taqtile.android.app.misc.utils.helpers.HeterogeneousRecyclerViewItemDecoration;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.app.misc.utils.helpers.PictureArcView;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.imagecapture.ImageCaptureActivity;
import br.com.taqtile.android.app.presentation.profilepublic.viewmodel.PublicProfileViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.CallMediaFragmentHelper;
import br.com.taqtile.android.textviews.CustomTextView;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static android.view.View.VISIBLE;
import static br.com.taqtile.android.app.presentation.communitypostdetail.CommunityPostDetailFragment.LIKE_POST;
import static br.com.taqtile.android.app.presentation.imagecapture.ImageCaptureActivity.REQUEST_PROFILE_IMAGE;
import static br.com.taqtile.android.app.presentation.profilepublic.PublicProfileActivity.PUBLIC_PROFILE_ID_TAG;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.POST_DETAIL_ACTION_POST_ID_KEY;

/**
 * Created by taqtile on 20/04/17.
 */


public class PublicProfileFragment extends AppBaseFragment implements
  CommunityCard.CommunityCardListener, ProgressBarButton.ProgressBarButtonButtonListener, OverflowButton.OverflowButtonListener {

  @BindView(R.id.fragment_public_profile_frameview_container)
  FrameLayout framelayoutContainer;
  @BindView(R.id.fragment_public_profile_background_picture)
  PictureArcView backgroundPicture;
  @BindView(R.id.fragment_public_profile_background_picture_button)
  ImageButton backgroundPictureButton;
  @BindView(R.id.fragment_public_profile_profile_picture)
  CircleImageView profilePicture;
  @BindView(R.id.fragment_public_profile_profile_picture_button)
  ImageButton profilePictureButton;
  @BindView(R.id.fragment_public_profile_recycler_view)
  RecyclerView recyclerView;
  @BindView(R.id.fragment_public_profile_edit_container)
  ScrollView profileEditContainer;
  @BindView(R.id.fragment_public_profile_name_form)
  CustomNameLabelEditTextCaption name;
  @BindView(R.id.fragment_public_profile_about_you_form)
  AboutYouLabelEditTextCaption aboutYou;
  @BindView(R.id.fragment_public_profile_edit_profile_button)
  ProgressBarButton saveProfileButton;
  @BindView(R.id.fragment_public_profile_content_container)
  LinearLayout contentContainer;
  @BindView(R.id.fragment_public_profile_no_posts_placeholder)
  CustomTextView noPostsPlaceholderText;
  @BindView(R.id.fragment_public_profile_posts_container)
  FrameLayout recyclerViewContainer;
  @BindView(R.id.fragment_public_profile_no_posts_placeholder_container)
  LinearLayout noPostsPlaceholderContainer;

  private PublicProfileContract.Presenter presenter;
  private NavigationManager navigationManager;
  private CustomToolbar customToolbar;

  private List<SocialFeedViewModel> socialFeedViewModels;
  private ProfileDetailsViewModel profileDetailsViewModel;
  private PublicProfileAdapter publicProfileAdapter;
  private LinearLayoutManager linearLayoutManager;

  private Integer profileId;
  private String profilePicturePath;
  private String backgroundPicturePath;

  private boolean reloadData;
  private boolean dataEdited;

  private boolean removeCover = false;
  private boolean removePhoto = false;

  private Integer globalRequestCode;

  Intent intent;

  CallMediaFragmentHelper mediaHelper = new CallMediaFragmentHelper(this);

  public static PublicProfileFragment newInstance() {
    return new PublicProfileFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View root = inflater.inflate(R.layout.fragment_public_profile, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    profileId = getActivity().getIntent().getIntExtra(PUBLIC_PROFILE_ID_TAG, 0);

    setupRecyclerView();
    setupListeners();
    setupView();
  }

  @Override
  public void onResume() {
    super.onResume();

    setupToolbar();

    if (reloadData) {
      fetchSocialFeedModelsWithHeader();
    }
  }

  private void setupToolbar() {
    if (customToolbar == null) {
      customToolbar = getToolbar(CustomToolbar.class);
    }
    customToolbar.hideTitle();
    customToolbar.setBackgroundDrawable(R.drawable.bkg_toolbar_gradient);
    setLeftButton();
  }

  private void showDefaultToolbar() {
    if (customToolbar == null) {
      customToolbar = getToolbar(CustomToolbar.class);
    }
    customToolbar.hideTitle();
    customToolbar.setToolbarColor(ContextCompat.getColor(getContext(), R.color.color_primary));
    customToolbar.setLeftButton(R.drawable.ic_arrow_back, v -> getActivity().finish());
    customToolbar.hideOptionsMenu();
  }

  public void setupView() {
    setEditModeOn(false);
    backgroundPicture.invalidate();
    reloadData = true;
    if (android.os.Build.VERSION.SDK_INT == 19) {
      framelayoutContainer.setFitsSystemWindows(false);
    }

    name.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
    aboutYou.getEditText().setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
  }

  private void setupRecyclerView() {
    intent = new Intent();
    linearLayoutManager = new LinearLayoutManager(getContext());
    recyclerView.setLayoutManager(linearLayoutManager);
    recyclerView.addItemDecoration(new HeterogeneousRecyclerViewItemDecoration((int)
      getResources().getDimension(R.dimen.margin_medium)) {

    });
    setupViewModelsLists();
    setupAdapter();
  }

  private void setupRecyclerviewScrollListener() {
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (customToolbar == null) {
          return;
        }
        setupToolbarTitleBehavior();
      }
    });
  }

  private void setupToolbarTitleBehavior() {
    if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
      customToolbar.setTitle(profileDetailsViewModel.getName());
    } else {
      customToolbar.hideTitle();
    }
  }

  private void setupAdapter() {
    publicProfileAdapter = new PublicProfileAdapter(socialFeedViewModels, profileDetailsViewModel);
    recyclerView.setAdapter(publicProfileAdapter);
    publicProfileAdapter.setListener(this);
  }

  private void setupViewModelsLists() {
    socialFeedViewModels = new ArrayList<>();
    profileDetailsViewModel = new ProfileDetailsViewModel();
  }

  private void setupListeners() {
    saveProfileButton.setButtonListener(this);
    backgroundPictureButton.setOnClickListener(view -> changeBackgroundPicture());
    profilePictureButton.setOnClickListener(view -> changeProfilePicture());
    setupRecyclerviewScrollListener();
  }

  private void onUpdateUserClick() {
    if (!name.validate()) {
      showFormValidationError();
    } else {
      updateUserData();
    }
  }

  private void updateUserData() {
    saveProfileButton.showLoadingState();
    Subscription updateUserDatasubscription = presenter
      .updatePublicProfileData(new EditProfileParams(name.getText(),
        aboutYou.getText(), profilePicturePath, backgroundPicturePath, removeCover, removePhoto))
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onUpdateSuccess, this::onUpdateFailure,
        () -> saveProfileButton.showDefaultState());
    compositeSubscription.add(updateUserDatasubscription);
  }

  private void onUpdateSuccess(ProfileDetailsViewModel profileDetailsViewModel) {
    this.dataEdited = true;
    this.profileDetailsViewModel = profileDetailsViewModel;
    setEditModeOn(false);

    publicProfileAdapter.setProfileDetailsViewModel(profileDetailsViewModel);
    publicProfileAdapter.notifyDataSetChanged();
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_update_profile),
      getString(R.string.analytics_event_update),
      getString(R.string.analytics_label_user_updated_profile)
    ));

  }

  private void onUpdateFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    saveProfileButton.showDefaultState();
  }

  private void setLeftButton() {
    if (customToolbar != null && getActivity() != null) {
      if (profileEditContainer.getVisibility() == View.GONE) {
        customToolbar.setLeftButton(R.drawable.ic_arrow_back, view -> this.finishFromEditUserData());
      } else {
        customToolbar.setLeftButton(R.drawable.ic_close, view -> {
          setEditModeOn(false);
          loadImagesFromUrl(profileDetailsViewModel.getProfilePicture(),
            profileDetailsViewModel.getCoverPicture());
        });
      }
    }
  }

  private void finishFromEditUserData() {

    getActivity().setResult(dataEdited ? Activity.RESULT_OK : Activity.RESULT_CANCELED, intent);
    getActivity().finish();
  }

  private void enableEdit(boolean shouldEnable) {
    if (customToolbar == null) {
      return;
    }
    if (shouldEnable) {
      customToolbar.inflateOptionsMenu(R.menu.menu_edit, menuItem -> {
        setEditModeOn(true);
        return false;
      });
    } else {
      customToolbar.hideOptionsMenu();
    }
  }

  private void setEditModeOn(boolean isEditModeOn) {
    if (isEditModeOn) {
      name.setText(profileDetailsViewModel.getName());
      aboutYou.setText(profileDetailsViewModel.getAbout());
    } else {
      SoftKeyboardHelper.hideKeyboard(getContext());
    }
    backgroundPictureButton.setVisibility(isEditModeOn ? VISIBLE : View.GONE);
    profilePictureButton.setVisibility(isEditModeOn ? VISIBLE : View.GONE);
    profileEditContainer.setVisibility(isEditModeOn ? VISIBLE : View.GONE);

    recyclerViewContainer.setVisibility(isEditModeOn ? View.GONE : VISIBLE);
    setLeftButton();
    enableEdit(shouldShowEditButton(isEditModeOn));
    if (isEditModeOn) {
      customToolbar.inflateOptionsMenu(R.menu.menu_overflow, menuItem -> true);
      customToolbar.getMenuItem(0).setActionView(getOverflowButton());
    }
  }

  private boolean shouldShowEditButton(boolean isEditModeOn) {
    return isUserAbleToEdit() && !isEditModeOn;
  }

  private OverflowButton getOverflowButton() {
    OverflowButton overflowButton = new OverflowButton(getContext());
    setupOverflowButton(overflowButton);
    return overflowButton;
  }

  private void setupOverflowButton(OverflowButton overflowButton) {
    List<String> overflowOptions = new ArrayList<>();
    overflowOptions.add(getResources().getString(
      R.string.fragment_public_profile_overflow_button_remove_photo_text));
    overflowOptions.add(getResources().getString(
      R.string.fragment_public_profile_overflow_button_remove_background_text));
    overflowButton.setIconColor(R.color.color_white);
    overflowButton.onMenuItemSelected(this);
    overflowButton.setMenuList(overflowOptions);
  }

  private void changeBackgroundPicture() {
    globalRequestCode = ImageCaptureActivity.REQUEST_COVER_IMAGE;
    mediaHelper.setActionId(CallMediaFragmentHelper.CHOOSE_OR_TAKE);
    mediaHelper.callMediaActivity(ImageCaptureActivity.REQUEST_COVER_IMAGE);
  }

  private void changeProfilePicture() {
    globalRequestCode = ImageCaptureActivity.REQUEST_PROFILE_IMAGE;
    mediaHelper.setActionId(CallMediaFragmentHelper.CHOOSE_OR_TAKE);
    mediaHelper.callMediaActivity(ImageCaptureActivity.REQUEST_PROFILE_IMAGE);
  }

  private void fetchSocialFeedModelsWithHeader() {
    showLoading();
    if (isUserAbleToEdit()) {
      Subscription fetchUserIdSubscription = Toolbox.getInstance()
        .getUserId()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onFetchUserIdSuccessful,
          this::onFetchUserIdFailure);
      compositeSubscription.add(fetchUserIdSubscription);
    } else {
      onFetchUserIdSuccessful(profileId);
    }
  }

  @Override
  public void showLoading() {
    super.showLoading();
    showDefaultToolbar();
    framelayoutContainer.setVisibility(View.GONE);
  }

  private boolean isUserAbleToEdit() {
    return profileId == 0;
  }

  private void onFetchUserIdSuccessful(Integer userId) {
    Subscription fetchPublicProfileData = presenter.fetchPublicProfileData(userId)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchDataSuccess, this::onFetchDataFailure, this::hideLoading);
    compositeSubscription.add(fetchPublicProfileData);
  }

  private void onFetchUserIdFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
  }

  private void onFetchDataSuccess(PublicProfileViewModel publicProfileViewModel) {
    updateAdapterModels(publicProfileViewModel);

    loadImagesFromUrl(profileDetailsViewModel.getProfilePicture(),
      profileDetailsViewModel.getCoverPicture());

    publicProfileAdapter.notifyDataSetChanged();
  }

  private void updateAdapterModels(PublicProfileViewModel publicProfileViewModel) {
    socialFeedViewModels.clear();
    socialFeedViewModels.addAll(publicProfileViewModel.getSocialFeedViewModels());

    showPostListPlaceholder(socialFeedViewModels.isEmpty());
    profileDetailsViewModel = publicProfileViewModel.getProfileDetailsViewModel();
    publicProfileAdapter.setProfileDetailsViewModel(profileDetailsViewModel);
  }

  private void showPostListPlaceholder(boolean shouldShowPostListPlaceholder) {
    if (shouldShowPostListPlaceholder) {
      noPostsPlaceholderContainer.setVisibility(VISIBLE);
    } else {
      noPostsPlaceholderContainer.setVisibility(View.GONE);

    }
  }

  private void loadImagesFromUrl(String profileImage, String coverImage) {
    shouldReloadCover(isCoverImageEmpty(coverImage), coverImage);
    shouldReloadProfile(isProfileImageEmpty(profileImage), profileImage);
    redrawProfilePictureAndCloseEditMode();
  }

  private void redrawProfilePictureAndCloseEditMode() {
    redrawProfilePicture();
    setEditModeOn(false);
  }

  private void shouldReloadProfile(boolean shouldReloadProfile, String profileImage) {
    if (shouldReloadProfile) {
      PicassoHelper.loadImageFromUrl(profilePicture, profileImage, getContext(), R.drawable.ic_profile_placeholder);
    }
  }

  private void shouldReloadCover(boolean shouldReloadCover, String coverImage) {
    if (shouldReloadCover) {
      backgroundPicture.setImageUrl(coverImage);
    }
  }

  private boolean isProfileImageEmpty(String profileImage) {
    return profileImage != null && !profileImage.isEmpty();
  }

  private boolean isCoverImageEmpty(String coverImage) {
    return coverImage != null && !coverImage.isEmpty();
  }

  private void onFetchDataFailure(Throwable throwable) {
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    mediaHelper.callMediaOnPermissionRequestResult(globalRequestCode);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    reloadData = false;
    if (requestCode == REQUEST_PROFILE_IMAGE && resultCode == Activity.RESULT_OK) {
      profilePicturePath = data.getStringExtra(ImageCaptureActivity.IMAGE_PATH);
      PicassoHelper.loadImageFromPath(profilePicture, profilePicturePath, getContext());
      new Handler().postDelayed(this::redrawProfilePicture, 500);
      removePhoto = false;

    } else if (requestCode == ImageCaptureActivity.REQUEST_COVER_IMAGE &&
      resultCode == Activity.RESULT_OK) {
      backgroundPicturePath = data.getStringExtra(ImageCaptureActivity.IMAGE_PATH);
      backgroundPicture.setImagePath(backgroundPicturePath);
      removeCover = false;
    }
  }

  private void redrawProfilePicture() {
    profilePicture.setImageDrawable(profilePicture.getDrawable());
    profilePicture.invalidate();
  }

  public void setPresenter(PublicProfileContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public void hideLoading() {
    super.hideLoading();
    setupToolbar();
    framelayoutContainer.setVisibility(VISIBLE);
  }

  @Override
  public void showPlaceholder() {
    super.showPlaceholder();
    contentContainer.setVisibility(View.GONE);
    framelayoutContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_white));
    showDefaultToolbar();
  }

  @Override
  public void hidePlaceholder() {
    super.hidePlaceholder();
    contentContainer.setVisibility(VISIBLE);
    framelayoutContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_background));
  }

  private void showFormValidationError() {
    showSnackbar(getResources().getString(R.string.fragment_public_profile_edit_data_form_error_message), Snackbar.LENGTH_SHORT);
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_public_profile_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchSocialFeedModelsWithHeader();
    hidePlaceholder();
  }

  @Override
  public void onProfileRegionClick(Integer profileId, Integer postId, int type) {

  }

  @Override
  public void onCardClick(Integer id, int type) {
    navigationManager.showCommunityPostDetailUI(null, id, true);
  }

  @Override
  public void onLikeClick(boolean isCounterActivated, Integer id, int type, CounterWithDrawable counterWithDrawable) {
    Subscription likePostSubscription = presenter.likeItem(Integer.valueOf(id), null, isCounterActivated)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        success -> this.onItemLikedSuccess(success, id),
        this::onItemLikedFailure
      );
    compositeSubscription.add(likePostSubscription);
  }

  private void onItemLikedSuccess(List<ListingsViewModel> listingsViewModels, Integer id) {
    // do nothing
    this.dataEdited = true;
    intent.putExtra(POST_DETAIL_ACTION_POST_ID_KEY, id);
    intent.putExtra(POST_DETAIL_ACTION_KEY, LIKE_POST);
    getActivity().setResult(Activity.RESULT_OK, intent);
  }

  private void onItemLikedFailure(Throwable throwable) {
    showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
  }

  @Override
  public void onCommentsCounterClick(Integer id, int type) {
  }

  @Override
  public void onButtonClicked() {
    onUpdateUserClick();
  }

  @Override
  public void onItemSelected(String item) {
    if (item.equals(getString(R.string.fragment_public_profile_overflow_button_remove_photo_text))) {
      profilePicturePath = null;
      shouldReloadProfile(true, null);
      removePhoto = true;
    } else {
      backgroundPicturePath = null;
      shouldReloadCover(true, null);
      removeCover = true;
    }
  }

  @Override
  public void willAppear() {
    super.willAppear();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_public_profile),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }
}
