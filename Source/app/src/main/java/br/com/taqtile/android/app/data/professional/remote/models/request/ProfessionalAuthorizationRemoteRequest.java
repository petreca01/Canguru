package br.com.taqtile.android.app.data.professional.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 3/24/17.
 */

public class ProfessionalAuthorizationRemoteRequest extends BaseRemoteRequest {
  @RequestName("professional_id") private Integer professionalId;

  @RequestName("questionnaire_only") private Integer questionnaireOnly;

  public ProfessionalAuthorizationRemoteRequest(Integer professionalId, Integer questionnaireOnly) {
    this.professionalId = professionalId;
    this.questionnaireOnly = questionnaireOnly;
  }
}
