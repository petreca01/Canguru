package br.com.taqtile.android.app.presentation.conditiondetails;

import br.com.taqtile.android.app.domain.clinicalConditions.models.ReportConditionParams;
import br.com.taqtile.android.app.presentation.condition.models.ReportClinicalConditionViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import rx.Observable;

/**
 * Created by taqtile on 5/9/17.
 */

public interface ConditionDetailsContract {

  interface Presenter extends BasePresenter {
    Observable<ReportClinicalConditionViewModel> reportCondition(ReportConditionParams reportConditionParams);
  }

}
