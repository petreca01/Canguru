package br.com.taqtile.android.app.domain.channel.models;

/**
 * Created by taqtile on 03/04/17.
 */

public class ChannelPostParams {
  private Integer postId;

  private Integer replyId;

  private Boolean like;

  public ChannelPostParams(Integer postId, Integer replyId, Boolean like) {
    this.postId = postId;
    this.replyId = replyId;
    this.like = like;
  }

  public Integer getPostId() {
    return postId;
  }

  public Integer getReplyId() {
    return replyId;
  }

  public Boolean isLike() {
    return like;
  }

}
