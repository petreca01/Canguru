package br.com.taqtile.android.app.presentation.additionalinfo.mapper;

import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.presentation.additionalinfo.model.AdditionalInfoUserViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;

/**
 * Created by taqtile on 4/26/17.
 */

public class UserDataAndHealthOperatorToAdditionalInfoUserDataViewModelMapper {

  public static AdditionalInfoUserViewModel perform(UserViewModel userViewModel,
                                                    HealthOperatorViewModel healthOperatorViewModel) {
    AdditionalInfoUserViewModel additionalInfoUserDataViewModel;
    if (healthOperatorViewModel != null) {
      additionalInfoUserDataViewModel = new AdditionalInfoUserViewModel(
        getEmail(userViewModel.getEmail()),
        getTelephone(userViewModel.getTelephone()),
        getBirthDate(userViewModel.getBirthDate()),
        getCpf(userViewModel.getCpf()),
        getZipCode(userViewModel.getZipCode()),
        getOperatorId(userViewModel.getOperatorId()),
        getOperatorName(userViewModel.getOperatorName()),
        getLocationPrivacyAgreed(userViewModel.getLocationPrivacyAgreed()),
        getLogo(healthOperatorViewModel.getLogo()),
        getAbout(healthOperatorViewModel.getAbout()),
        getHealthPlanPartner(healthOperatorViewModel.getHealthPlanPartner()),
        getOperatorDocumentType(healthOperatorViewModel.getDocumentType()));
    } else {
      additionalInfoUserDataViewModel = new AdditionalInfoUserViewModel(
        getEmail(userViewModel.getEmail()),
        getTelephone(userViewModel.getTelephone()),
        getBirthDate(userViewModel.getBirthDate()),
        getCpf(userViewModel.getCpf()),
        getZipCode(userViewModel.getZipCode()),
        getOperatorId(userViewModel.getOperatorId()),
        getOperatorName(userViewModel.getOperatorName()),
        getLocationPrivacyAgreed(userViewModel.getLocationPrivacyAgreed()));
    }
    return additionalInfoUserDataViewModel;
  }

  private static String getLogo(String logo) {
    return logo != null ? logo : "";
  }

  private static String getAbout(String about) {
    return about != null ? about : "";
  }

  private static Boolean getHealthPlanPartner(Boolean healthPlanPartner) {
    return healthPlanPartner != null ? healthPlanPartner : false;
  }

  private static String getEmail(String email) {
    return email != null ? email : "";
  }

  private static String getTelephone(String telephone) {
    return telephone != null ? telephone : "";
  }

  private static String getBirthDate(String birthDate) {
    return birthDate != null ? birthDate : "";
  }

  private static String getCpf(String cpf) {
    return cpf != null ? cpf : "";
  }

  private static String getZipCode(String zipCode) {
    return zipCode != null ? zipCode : "";
  }

  private static Integer getOperatorId(Integer operatorId) {
    return operatorId != null ? operatorId : 0;
  }

  private static String getOperatorName(String operatorName) {
    return operatorName != null ? operatorName : "";
  }

  private static Boolean getLocationPrivacyAgreed(Boolean locationPrivacyAgreed) {
    return locationPrivacyAgreed != null ? locationPrivacyAgreed : false;
  }

  private static String getOperatorDocumentType(String operatorDocumentType) {
    return operatorDocumentType != null ? operatorDocumentType : "";
  }
}
