package br.com.taqtile.android.app.presentation.tutorial.showcase.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;

/**
 * Created by taqtile on 5/23/17.
 */

public class Rectangle implements Shape {

  public static class DrawingParams {
    public int width;
    public int height;
    public Paint paint;

    public DrawingParams(int width, int height, int color) {
      this.width = width;
      this.height = height;
      this.paint = new Paint();
      paint.setColor(color);
      paint.setStyle(Paint.Style.FILL);
    }

    public DrawingParams(int width, int height, Paint paint) {
      this.width = width;
      this.height = height;
      this.paint = paint;
    }
  }

  private DrawingParams drawingParams;
  private int top, left;

  public Rectangle(DrawingParams drawingParams) {
    this.drawingParams = drawingParams;
    this.top = 0;
    this.left = 0;
  }

  public void drawOnCanvas(Canvas canvas) {
    canvas.drawRect(left, top, left + drawingParams.width, top + drawingParams.height, drawingParams.paint);
  }

  public int getWidth() {
    return drawingParams.width;
  }

  public int getHeight() {
    return drawingParams.height;
  }

  public void setPosition(int left, int top) {
    this.top = top;
    this.left = left;
  }
}
