package br.com.taqtile.android.app.domain.channel;

import java.util.List;

import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.domain.channel.mappers.ChannelPostResultToListingsViewModelMapper;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostParams;
import br.com.taqtile.android.app.domain.channel.models.ChannelPostResult;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.ListingsViewModel;
import rx.Observable;

/**
 * Created by taqtile on 4/17/17.
 */

public class LikeChannelPostUseCase extends BaseUseCase<List<ListingsViewModel>, ChannelPostParams, ChannelRepository> {

  public LikeChannelPostUseCase(ChannelRepository channelRepository) {
    super(channelRepository);
  }

  @Override
  public Observable<List<ListingsViewModel>> execute(ChannelPostParams channelPostParams) {
    return getRepository().post(channelPostParams)
      .map(ChannelPostResultToListingsViewModelMapper::perform);
  }
}
