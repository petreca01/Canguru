package br.com.taqtile.android.app.support;

import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import br.com.taqtile.android.app.domain.agenda.models.AppointmentParams;
import br.com.taqtile.android.app.domain.agenda.models.ScheduleParams;
import br.com.taqtile.android.app.listings.viewmodels.AgendaItemViewModel;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;

/**
 * Created by taqtile on 22/05/17.
 */

public class ExternalIntent {


  public static Intent getCalendarIntent() {
    Uri calendarUri = CalendarContract.CONTENT_URI
      .buildUpon()
      .appendPath("time")
      .build();

    return new Intent(Intent.ACTION_VIEW, calendarUri);
  }

  public static Intent getCalendarInsertEventIntent(AppointmentParams appointmentParams) {
    long begin = DateFormatterHelper.getDateInMilliseconds(appointmentParams.getDate());
    long end = begin + DateFormatterHelper.getOneHour();

    Intent intent = new Intent(Intent.ACTION_INSERT)
      .setData(CalendarContract.Events.CONTENT_URI)
      .putExtra(CalendarContract.Events.TITLE, appointmentParams.getName())
      .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin)
      .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end);

    return intent;
  }

  public static Intent getCalendarInsertEventIntent(ScheduleParams scheduleParams, AgendaItemViewModel agendaItemViewModel) {
    long begin = DateFormatterHelper.getDateInMilliseconds(scheduleParams.getDate());
    long end = begin + DateFormatterHelper.getOneHour();

    Intent intent = new Intent(Intent.ACTION_INSERT)
      .setData(CalendarContract.Events.CONTENT_URI)
      .putExtra(CalendarContract.Events.TITLE, agendaItemViewModel.getName())
      .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin)
      .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end);

    return intent;
  }
}
