package br.com.taqtile.android.app.data.professional.remote.mappers;

import br.com.taqtile.android.app.data.professional.remote.models.response.ProfessionalInfoItemRemoteResponse;
import br.com.taqtile.android.app.domain.professional.models.ProfessionalResult;

/**
 * Created by taqtile on 05/04/17.
 */

public class ProfessionalInfoItemRemoteResponseToProfessionalResultMapper {
  public static ProfessionalResult perform(ProfessionalInfoItemRemoteResponse professionalInfoItemRemoteResponse) {
    ProfessionalResult professionalResult = new ProfessionalResult(
      professionalInfoItemRemoteResponse.getId(),
      professionalInfoItemRemoteResponse.getProfessionalId(),
      professionalInfoItemRemoteResponse.getProfessionalName(),
      professionalInfoItemRemoteResponse.getAuthorizedAt(),
      professionalInfoItemRemoteResponse.getRiskQuestionnaireAuthorized(),
      professionalInfoItemRemoteResponse.getRevokedAt(),
      professionalInfoItemRemoteResponse.getCreatedAt(),
      professionalInfoItemRemoteResponse.getUpdatedAt());

    return professionalResult;
  }
}
