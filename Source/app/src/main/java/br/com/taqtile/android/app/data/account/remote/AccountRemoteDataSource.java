package br.com.taqtile.android.app.data.account.remote;

import br.com.taqtile.android.app.data.account.remote.models.request.EditUserRemoteRequest;
import br.com.taqtile.android.app.data.account.remote.models.request.FacebookSignInRemoteRequest;
import br.com.taqtile.android.app.data.account.remote.models.request.ForgotPasswordRemoteRequest;
import br.com.taqtile.android.app.data.account.remote.models.request.SignInRemoteRequest;
import br.com.taqtile.android.app.data.account.remote.models.request.SignUpRemoteRequest;
import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 3/15/17.
 */

public class AccountRemoteDataSource extends BaseRemoteDataSource {

  private Observable<AccountServices> services;
  private Observable<AccountServices> authenticatedServices;

  public AccountRemoteDataSource() {
    super();
    this.services = ServiceFactory.getObservable(AccountServices.class);
    this.authenticatedServices = ServiceFactory.getObservableAuthenticated(AccountServices.class);
  }

  public Observable<BaseRemoteResponse<UserRemoteResponse>> signIn(
    SignInRemoteRequest signInRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.signIn(signInRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<UserRemoteResponse>> signInWithFacebook(
    FacebookSignInRemoteRequest facebookRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.signInWithFacebook(facebookRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<UserRemoteResponse>> signUp(
    SignUpRemoteRequest signUpRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.signUp(signUpRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<UserRemoteResponse>> edit(
    EditUserRemoteRequest editRemoteRequest) {
    return performRequest(
      authenticatedServices.flatMap(services -> services.edit(editRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<UserRemoteResponse>> getUserData() {
    return performRequest(authenticatedServices.flatMap(AccountServices::getUserData));
  }

  public Observable<BaseRemoteResponse<String>> passwordRecovery(
    ForgotPasswordRemoteRequest forgotPasswordRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.passwordRecovery(forgotPasswordRemoteRequest.getFormData())));
  }
}
