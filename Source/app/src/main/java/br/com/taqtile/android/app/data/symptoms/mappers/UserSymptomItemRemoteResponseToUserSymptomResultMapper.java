package br.com.taqtile.android.app.data.symptoms.mappers;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.UserSymptomItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.UserSymptomResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class UserSymptomItemRemoteResponseToUserSymptomResultMapper {

  public static UserSymptomResult perform(UserSymptomItemRemoteResponse userSymptomItemRemoteResponse) {
    return new UserSymptomResult(
      userSymptomItemRemoteResponse.getDate(),
      userSymptomItemRemoteResponse.getSymptomId(),
      userSymptomItemRemoteResponse.getSymptomName(),
      userSymptomItemRemoteResponse.getGestationAgeWeeks(),
      userSymptomItemRemoteResponse.getGestationAgeDays()
    );
  }
}
