package br.com.taqtile.android.app.domain.followcard.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.followcard.models.FollowCardExamsResult;
import br.com.taqtile.android.app.presentation.prenatalcard.models.FollowCardExamsViewModel;
import java8.util.Objects;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/23/17.
 */

public class FollowCardExamsResultToViewModelMapper {

  public static List<FollowCardExamsViewModel> perform(List<FollowCardExamsResult> followCardExamsResults) {
    return StreamSupport.stream(followCardExamsResults)
      .filter(Objects::nonNull)
      .map(FollowCardExamsResultToViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static FollowCardExamsViewModel map(FollowCardExamsResult followCardExamsResult) {
    return new FollowCardExamsViewModel(
      followCardExamsResult.getId(),
      followCardExamsResult.getName(),
      followCardExamsResult.getResult(),
      followCardExamsResult.getDate(),
      followCardExamsResult.getCreatedAt()
    );
  }
}
