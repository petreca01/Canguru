package br.com.taqtile.android.app.presentation.gestationpregnancyinformation;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import br.com.taqtile.android.app.presentation.common.CustomNavigationResultListener;
import br.com.taqtile.android.app.presentation.common.TemplateBackActivity;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.main.MainActivity;
import br.com.taqtile.android.app.support.NavigationHelper;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;

import static br.com.taqtile.android.app.support.GlobalConstants.EDIT_GESTATION_CODE;
import static br.com.taqtile.android.app.support.GlobalConstants.USER_DATA_KEY;

/**
 * Created by taqtile on 12/05/17.
 */

public class GestationPregnancyInformationActivity extends TemplateBackActivity {


  private GestationPregnancyInformationFragment fragment;

  @Override
  public Fragment getFragment() {
    GestationPregnancyInformationFragment gestationPregnancyInformationFragment =
      (GestationPregnancyInformationFragment) getSupportFragmentManager()
      .findFragmentById(getFragmentContainerId());
    if (gestationPregnancyInformationFragment == null) {
      gestationPregnancyInformationFragment = GestationPregnancyInformationFragment.newInstance();
    }
    fragment = gestationPregnancyInformationFragment;

    return fragment;
  }

  @Override
  public BasePresenter getPresenter() {
    GestationPregnancyInformationPresenter presenter = new GestationPregnancyInformationPresenter();
    fragment.setNavigationManager(new NavigationHelper(this));
    fragment.setPresenter(presenter);

    return presenter;
  }

  public static void navigate(Context context, UserViewModel userData,
                              CustomNavigationResultListener listener) {
    Intent intent = new Intent(context, GestationPregnancyInformationActivity.class);
    intent.putExtra(USER_DATA_KEY, userData);

    if (context instanceof MainActivity) {
      ((MainActivity) context).setResultListener(listener,
        EDIT_GESTATION_CODE);
      ((MainActivity) context).startActivityForResult(intent,
        EDIT_GESTATION_CODE);
    }
  }
}
