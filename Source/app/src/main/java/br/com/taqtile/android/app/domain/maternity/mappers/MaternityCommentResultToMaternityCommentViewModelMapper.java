package br.com.taqtile.android.app.domain.maternity.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import br.com.taqtile.android.app.listings.viewmodels.MaternityCommentViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 5/12/17.
 */

public class MaternityCommentResultToMaternityCommentViewModelMapper {
  public static List<MaternityCommentViewModel> map(List<MaternityCommentResult> maternityDataResult) {
    return StreamSupport.stream(maternityDataResult)
      .map(MaternityCommentResultToMaternityCommentViewModelMapper::map)
      .collect(Collectors.toList());
  }

  public static MaternityCommentViewModel map(MaternityCommentResult maternityDataResult) {
    return new MaternityCommentViewModel(
      maternityDataResult.getId(),
      maternityDataResult.getUserId(),
      maternityDataResult.getComment(),
      maternityDataResult.getCreatedAt(),
      maternityDataResult.getUsername(),
      maternityDataResult.getUserPicture()
    );
  }
}
