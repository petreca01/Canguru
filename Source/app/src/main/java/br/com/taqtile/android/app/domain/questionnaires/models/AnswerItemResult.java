package br.com.taqtile.android.app.domain.questionnaires.models;

/**
 * Created by filipe.mp on 4/10/17.
 */

public class AnswerItemResult {
  private String answer;

  private String createdAt;

  public AnswerItemResult() {
    this.answer = "";
    this.createdAt = "";
  }

  public AnswerItemResult(String answer, String createdAt) {
    this.answer = answer;
    this.createdAt = createdAt;
  }

  public String getAnswer() {
    return answer;
  }

  public String getCreatedAt() {
    return createdAt;
  }
}
