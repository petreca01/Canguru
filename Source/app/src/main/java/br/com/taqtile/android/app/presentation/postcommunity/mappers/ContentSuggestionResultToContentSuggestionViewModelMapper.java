package br.com.taqtile.android.app.presentation.postcommunity.mappers;

import java.util.List;

import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionItemResult;
import br.com.taqtile.android.app.domain.forum.models.ContentSuggestionResult;
import br.com.taqtile.android.app.listings.viewmodels.ChannelListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ChannelSearchPostImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ForumListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.ForumReplyListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.SymptomListingImplViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionResultToContentSuggestionViewModelMapper {

  private final static String FORUM = "forum";
  private final static String CHANNEL_POST = "channel-post";
  private final static String CHANNEL = "channel";
  private final static String SYMPTOMS = "symptom";
  private final static String FORUM_REPLY = "forum-reply";

  private static int totalContents;

  public static List<SearchListingViewModel> perform(ContentSuggestionResult result) {
    List<ContentSuggestionItemResult> contentSuggestionItemResults = result.getContents();
    totalContents = result.getTotalContents();
    return ContentSuggestionResultToContentSuggestionViewModelMapper.map(contentSuggestionItemResults);
  }

  public static List<SearchListingViewModel> map(List<ContentSuggestionItemResult> contentSuggestionItemResults) {
    return StreamSupport.stream(contentSuggestionItemResults)
      .filter(contentSuggestionItemResult -> !isChannelCell(contentSuggestionItemResult))
      .map(ContentSuggestionResultToContentSuggestionViewModelMapper::map)
      .collect(Collectors.toList());
  }

  private static SearchListingViewModel map(ContentSuggestionItemResult contentSuggestionItemResults) {
    switch (contentSuggestionItemResults.getType()) {
      case FORUM:
        return new ForumListingImplViewModel(String.valueOf(contentSuggestionItemResults.getId()),
          contentSuggestionItemResults.getTitle(),
          contentSuggestionItemResults.getLikes(),
          contentSuggestionItemResults.getCountReplies(),
          contentSuggestionItemResults.getResume(),
          totalContents,
          contentSuggestionItemResults.getContentId(),
          contentSuggestionItemResults.getContentParentId());
      case FORUM_REPLY:
        return new ForumReplyListingImplViewModel(totalContents,
          String.valueOf(contentSuggestionItemResults.getId()),
          contentSuggestionItemResults.getTitle(),
          contentSuggestionItemResults.getLikes(),
          contentSuggestionItemResults.getCountReplies(),
          contentSuggestionItemResults.getResume(),
          contentSuggestionItemResults.getContentId(),
          contentSuggestionItemResults.getContentParentId());
      case CHANNEL_POST:
        return new ChannelSearchPostImplViewModel(String.valueOf(contentSuggestionItemResults.getId()),
          contentSuggestionItemResults.getTitle(),
          contentSuggestionItemResults.getLikes(),
          contentSuggestionItemResults.getCountReplies(),
          contentSuggestionItemResults.getResume(),
          totalContents,
          contentSuggestionItemResults.getContentId(),
          contentSuggestionItemResults.getContentParentId());
      case SYMPTOMS:
        return new SymptomListingImplViewModel(String.valueOf(contentSuggestionItemResults.getId()),
          contentSuggestionItemResults.getTitle(),
          contentSuggestionItemResults.getResume(),
          totalContents,
          contentSuggestionItemResults.getContentId(),
          contentSuggestionItemResults.getContentParentId());
      default:
        return null;
    }
  }

  private static boolean isChannelCell(ContentSuggestionItemResult contentSuggestionItemResult){
    return contentSuggestionItemResult.getType().equals(CHANNEL);
  }


}
