package br.com.taqtile.android.app.data.healthOperators.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class HealthOperatorIdentifierRemoteResponse {
    @SerializedName("label")
    private String label;

    @SerializedName("type")
    private String type;

    @SerializedName("placeholder")
    private String placeholder;

    @SerializedName("required")
    private Boolean required;

    public String getLabel() {
        return label;
    }

    public String getType() {
        return type;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public Boolean getRequired() {
        return required;
    }
}
