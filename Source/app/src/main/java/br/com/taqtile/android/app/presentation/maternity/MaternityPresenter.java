package br.com.taqtile.android.app.presentation.maternity;

import java.util.List;

import br.com.taqtile.android.app.domain.account.UserDataUseCase;
import br.com.taqtile.android.app.domain.maternity.ListByCityUseCase;
import br.com.taqtile.android.app.domain.maternity.mappers.MaternityDataResultToMaternityViewModelMapper;
import br.com.taqtile.android.app.listings.viewmodels.MaternityViewModel;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 3/17/17.
 */

public class MaternityPresenter implements MaternityContract.Presenter {

  private ListByCityUseCase listByCityUseCase;
  private UserDataUseCase userDataUseCase;

  public MaternityPresenter() {
    this.listByCityUseCase = Injection.provideListByCityUseCase();
    this.userDataUseCase = Injection.provideUserDataUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<UserViewModel> fetchUserData() {
    return userDataUseCase.execute(null);
  }

  @Override
  public Observable<List<MaternityViewModel>> fetchMaternitiesByCity(String city) {
    return this.listByCityUseCase.execute(city).map(MaternityDataResultToMaternityViewModelMapper::map);
  }
}
