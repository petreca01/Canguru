package br.com.taqtile.android.app.data.symptoms.mappers;

import java.util.List;

import br.com.taqtile.android.app.data.symptoms.remote.models.response.SymptomsItemRemoteResponse;
import br.com.taqtile.android.app.domain.symptoms.models.SubSymptomResult;
import br.com.taqtile.android.app.domain.symptoms.models.SymptomResult;

/**
 * Created by filipe.mp on 4/11/17.
 */

public class SymptomsItemRemoteResponseToSymptomResultMapper {

  public static SymptomResult perform(SymptomsItemRemoteResponse symptomsItemRemoteResponse) {
    List<SubSymptomResult> subSymptomResultList =
      SubSymptomItemRemoteResponseListToSubSymptomResultListMapper
        .perform(symptomsItemRemoteResponse.getChilds());

    return new SymptomResult(
      symptomsItemRemoteResponse.getId(),
      symptomsItemRemoteResponse.getParentId(),
      symptomsItemRemoteResponse.getName(),
      symptomsItemRemoteResponse.getCiap(),
      symptomsItemRemoteResponse.getCondition(),
      symptomsItemRemoteResponse.getConditionTrigger(),
      symptomsItemRemoteResponse.getExplanation(),
      symptomsItemRemoteResponse.getHowToAvoid(),
      symptomsItemRemoteResponse.getWhenToWorry(),
      symptomsItemRemoteResponse.getDangerous(),
      subSymptomResultList
    );
  }
}
