package br.com.taqtile.android.app.domain.followcard.models;

/**
 * Created by taqtile on 5/19/17.
 */

public class GestationHistoryResult {

  private Integer pregnancy;
  private Integer normalDelivery;
  private Integer caesarean;
  private Integer miscarriage;

  public GestationHistoryResult(Integer pregnancy, Integer normalDelivery, Integer caesarean,
                                Integer miscarriage) {
    this.pregnancy = pregnancy;
    this.normalDelivery = normalDelivery;
    this.caesarean = caesarean;
    this.miscarriage = miscarriage;
  }

  public Integer getPregnancy() {
    return pregnancy;
  }

  public Integer getNormalDelivery() {
    return normalDelivery;
  }

  public Integer getCaesarean() {
    return caesarean;
  }

  public Integer getMiscarriage() {
    return miscarriage;
  }

}
