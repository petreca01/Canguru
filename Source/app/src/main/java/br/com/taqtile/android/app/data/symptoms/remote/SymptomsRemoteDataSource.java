package br.com.taqtile.android.app.data.symptoms.remote;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.request.ReportSymptomRemoteRequest;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.ReportSymptomRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.SymptomsItemRemoteResponse;
import br.com.taqtile.android.app.data.symptoms.remote.models.response.UserSymptomItemRemoteResponse;
import br.com.taqtile.android.app.data.view.models.request.ViewModeRemoteRequest;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public class SymptomsRemoteDataSource extends BaseRemoteDataSource {
  private Observable<SymptomsServices> services;

  public SymptomsRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(SymptomsServices.class);
  }

  public Observable<BaseRemoteResponse<List<SymptomsItemRemoteResponse>>> list() {
    return performRequest(services.flatMap(SymptomsServices::list));
  }

  public Observable<BaseRemoteResponse<List<UserSymptomItemRemoteResponse>>> listUserSymptoms(
    ViewModeRemoteRequest listUserSymptomsRemoteRequest) {
    return performRequest(services.flatMap(
      services -> services.listUserSymptoms(listUserSymptomsRemoteRequest.getFormData())));
  }

  public Observable<BaseRemoteResponse<ReportSymptomRemoteResponse>> reportSymptom(
    ReportSymptomRemoteRequest reportSymptomRemoteRequest) {
    return performRequest(
      services.flatMap(services -> services.reportSymptom(reportSymptomRemoteRequest.getFormData())));
  }
}
