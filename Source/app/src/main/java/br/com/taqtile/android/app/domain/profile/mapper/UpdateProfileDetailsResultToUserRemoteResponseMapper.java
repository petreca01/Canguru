package br.com.taqtile.android.app.domain.profile.mapper;

import br.com.taqtile.android.app.data.common.model.response.UserRemoteResponse;
import br.com.taqtile.android.app.domain.profile.models.UpdateProfileDetailsResult;

/**
 * Created by taqtile on 6/7/17.
 */

public class UpdateProfileDetailsResultToUserRemoteResponseMapper {

  public static UserRemoteResponse perform(UpdateProfileDetailsResult updateProfileDetailsResult) {
    return new UserRemoteResponse(
      updateProfileDetailsResult.getProfilePicture(), updateProfileDetailsResult.getName());
  }
}
