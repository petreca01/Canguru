package br.com.taqtile.android.app.domain.symptoms;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.data.symptoms.SymptomsRepository;
import br.com.taqtile.android.app.domain.common.BaseUseCase;
import br.com.taqtile.android.app.domain.symptoms.models.ReportSymptomParams;
import rx.Observable;

/**
 * Created by taqtile on 19/04/17.
 */

public class ReportSymptomUseCase
  extends BaseUseCase<EmptyResult, ReportSymptomParams, SymptomsRepository> {

  public ReportSymptomUseCase(SymptomsRepository symptomsRepository) {
    super(symptomsRepository);
  }

  @Override
  public Observable<EmptyResult> execute(ReportSymptomParams reportSymptomParams) {
    return getRepository().reportSymptom(reportSymptomParams)
      .map(reportSymptomResult -> new EmptyResult());
  }
}
