package br.com.taqtile.android.app.domain.agenda.models;

/**
 * Created by taqtile on 4/7/17.
 */

public class ScheduleParams {

  private Integer agendaId;
  private Integer isAppointment;
  private String date;
  private String observation;
  private String name;
  private Integer typeId;

  public ScheduleParams(Integer agendaId, String date, String observation) {
    this.agendaId = agendaId;
    this.date = date;
    this.observation = observation;
  }

  public ScheduleParams(Integer agendaId, Integer isAppointment, String date, String observation,
                        String name, Integer typeId) {
    this.agendaId = agendaId;
    this.isAppointment = isAppointment;
    this.date = date;
    this.observation = observation;
    this.name = name;
    this.typeId = typeId;
  }

  public Integer getAgendaId() {
    return agendaId;
  }

  public Integer getIsAppointment() {
    return isAppointment;
  }

  public String getDate() {
    return date;
  }

  public String getObservation() {
    return observation;
  }

  public String getName() {
    return name;
  }

  public Integer getTypeId() {
    return typeId;
  }
}
