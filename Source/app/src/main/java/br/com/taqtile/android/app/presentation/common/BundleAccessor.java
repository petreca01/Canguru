package br.com.taqtile.android.app.presentation.common;

import android.os.Bundle;

/**
 * Created by taqtile on 09/05/17.
 */

public class BundleAccessor {

  protected Bundle bundle;

  public BundleAccessor(Bundle bundle) {
    this.bundle = bundle;
  }
}
