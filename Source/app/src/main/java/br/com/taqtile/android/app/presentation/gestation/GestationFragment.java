package br.com.taqtile.android.app.presentation.gestation;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.app.R;
import br.com.taqtile.android.app.buttons.OverflowButton;
import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.domain.weeklycontent.models.WeeklyContentParams;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorBuilder;
import br.com.taqtile.android.app.listings.builders.HorizontalSelectorColors;
import br.com.taqtile.android.app.listings.cells.HorizontalSelectorCell;
import br.com.taqtile.android.app.listings.cells.TextImageCellCenter;
import br.com.taqtile.android.app.misc.general.BalloonWithText;
import br.com.taqtile.android.app.misc.general.ProgressBarLoading;
import br.com.taqtile.android.app.misc.toolbar.CustomToolbar;
import br.com.taqtile.android.app.misc.utils.helpers.DateFormatterHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.app.misc.utils.helpers.FloatToDpConverter;
import br.com.taqtile.android.app.misc.utils.helpers.PicassoHelper;
import br.com.taqtile.android.app.presentation.common.AppBaseFragment;
import br.com.taqtile.android.app.presentation.common.mappers.DataSourceErrorToPresenterErrorMapper;
import br.com.taqtile.android.app.presentation.common.user.UserViewModel;
import br.com.taqtile.android.app.presentation.gestation.models.WeeklyContentViewModel;
import br.com.taqtile.android.app.support.NavigationManager;
import br.com.taqtile.android.app.support.analytics.AnalyticsEvent;
import br.com.taqtile.android.app.support.helpers.GestationTextFormatterHelper;
import br.com.taqtile.android.design.ArcView;
import br.com.taqtile.android.textviews.CustomTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static br.com.taqtile.android.app.support.GlobalConstants.BIRTH_PLAN_ANSWERED_PERCENTAGE_KEY;
import static br.com.taqtile.android.app.support.GlobalConstants.PREGNANT;
import static br.com.taqtile.android.app.support.GlobalConstants.USER_DATA_KEY;

/**
 * Created by taqtile on 3/17/17.
 */

public class GestationFragment extends AppBaseFragment implements HorizontalSelectorCell.Listener,
  OverflowButton.OverflowButtonListener, MenuItem.OnMenuItemClickListener, ViewTreeObserver.OnGlobalLayoutListener {

  private final static int FIRST_MENU_ITEM_POSITION = 0;
  private Integer MIN_WEEK = 4;
  private Integer MAX_WEEK = 42;
  private Integer GESTATION_FINISHED_CARD_MAX_LINES = 10;
  private Integer GESTATION_CARD_BODY_DEFAULT_LINES = 4;

  @BindView(R.id.fragment_gestation_scroll_view)
  ScrollView scrollView;

  @BindView(R.id.fragment_gestation_container)
  LinearLayout gestationContainer;

  @BindView(R.id.fragment_gestation_weekly_content_container)
  LinearLayout weeklyContentContainer;

  @BindView(R.id.fragment_gestation_weekly_content_selector_container)
  LinearLayout weeklyContentSelectorContainer;

  @BindView(R.id.fragment_gestation_initial_screen_container)
  LinearLayout initialScreenContainer;

  @BindView(R.id.fragment_gestation_start_gestation_button)
  TemplateButton startGestationButton;

  @BindView(R.id.fragment_gestation_gestation_information_container)
  LinearLayout gestationInformationContainer;

  @BindView(R.id.fragment_gestation_gestation_information)
  LinearLayout gestationDateInformationContainer;

  @BindView(R.id.fragment_gestation_gestation_information_arc_view)
  ArcView gestationInformationArcView;

  @BindView(R.id.fragment_gestation_baby_information)
  CustomTextView babyInformation;

  @BindView(R.id.fragment_gestation_baby_probable_birthdate_header)
  CustomTextView babyProbableBirthdateHeader;

  @BindView(R.id.fragment_gestation_birthplan_button)
  TextImageCellCenter birthplanButton;

  @BindView(R.id.fragment_gestation_end_gestation_button)
  TextImageCellCenter endGestationButton;

  @BindView(R.id.fragment_gestation_clinical_condition_button)
  TextImageCellCenter clinicalConditionsButton;

  @BindView(R.id.fragment_profile_menu_personal_risk)
  TextImageCellCenter personalRisk;

  @BindView(R.id.fragment_gestation_medical_recommendations_button)
  TextImageCellCenter medicalRecommendationsButton;

  @BindView(R.id.fragment_gestation_prenatal_card_button)
  TextImageCellCenter prenatalCardButton;

  @BindView(R.id.fragment_gestation_symptom_guide_button)
  TextImageCellCenter symptomGuideButton;

  @BindView(R.id.fragment_gestation_symptom_history_button)
  TextImageCellCenter symptomHistoryButton;

  @BindView(R.id.fragment_gestation_development_title)
  CustomTextView babyDevelopmentTitle;

  @BindView(R.id.fragment_gestation_development_body)
  CustomTextView babyDevelopmentBody;

  @BindView(R.id.fragment_gestation_development_image)
  ImageView babyDevelopmentImage;

  @BindView(R.id.fragment_gestation_loading)
  ProgressBarLoading loading;

  @BindView(R.id.fragment_gestation_development_card)
  CardView babyDevelopmentCard;

  @BindView(R.id.fragment_gestation_card_loading)
  ProgressBar cardLoading;

  @BindView(R.id.fragment_gestation_card_loading_container)
  FrameLayout cardLoadingContainer;

  @BindView(R.id.fragment_gestation_development_card_container)
  FrameLayout cardContainer;

  @BindView(R.id.fragment_gestation_weeks_selector_scroll_view)
  HorizontalScrollView weeksSelectorScrollView;

  @BindView(R.id.fragment_gestation_baby_probable_birthdate)
  CustomTextView babyProbableBirthdate;

  @BindView(R.id.fragment_gestation_probable_birthdate_container)
  LinearLayout babyProbableBirthdateContainer;

  @BindView(R.id.fragment_gestation_weeks_selector_container)
  LinearLayout weeksSelectorContainer;
  private HorizontalSelectorBuilder weekSelectorBuilder;
  private HorizontalSelectorColors firstTrimesterColors;

  private boolean userIsPregnant;

  private CustomToolbar customToolbar;
  private GestationContract.Presenter presenter;
  private NavigationManager navigationManager;
  private UserViewModel userData;
  private String currentWeek;
  private ArrayList<String[]> weeks;
  private WeeklyContentViewModel currentWeeklyContent;
  private Subscription userDataSubscription;
  private Subscription weeklyContentSubscription;
  private boolean reloadWeekSelector;
  private String defaultWeek;
  private String gestationWeek;
  private View root;
  private boolean autoScroll;

  public static GestationFragment newInstance() {
    return new GestationFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    root = inflater.inflate(R.layout.fragment_gestation, container, false);

    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setupButtons();
    setupBabyDevelopmentCard();
    setupView();
  }

  private void setupView() {
    this.autoScroll = true;
  }

  private void setupButtons() {

    startGestationButton.setOnClickListener(v -> onStartGestationButtonClick());
    babyDevelopmentCard.setOnClickListener(v -> onWeeklyContentClick());
    symptomGuideButton.setListener(this::onSymptomGuideButtonClick);
    symptomHistoryButton.setListener(this::onSymptomHistoryButtonClick);
    clinicalConditionsButton.setListener(this::onClinicalConditionsButtonClick);
    medicalRecommendationsButton.setListener(this::onMedicalRecommendationsButtonClick);
    prenatalCardButton.setListener(this::onPreNatalButtonClick);
    birthplanButton.setListener(this::onBirthPlanButtonClick);
    endGestationButton.setListener(this::onEndGestationButtonClick);
    personalRisk.setListener(() -> navigationManager.showPersonalRisk());
    babyProbableBirthdateContainer.setOnClickListener(v -> navigationManager.showProbableBirthdateUI());
    babyProbableBirthdateHeader.setOnClickListener(v -> navigationManager.showProbableBirthdateUI());
    babyProbableBirthdate.setOnClickListener(v -> navigationManager.showProbableBirthdateUI());
    gestationDateInformationContainer.setOnClickListener(v -> navigationManager.showProbableBirthdateUI());
  }

  @Override
  public void willAppear() {
    autoScroll = true;
    fetchGestationData();
    setupToolbar();

    // page view
    this.analytics.trackEvent(new AnalyticsEvent(
      getString(R.string.analytics_category_gestation),
      getString(R.string.analytics_event_view),
      getString(R.string.analytics_label_user_view_page)
    ));
  }

  private void setupBabyDevelopmentCard() {
    float cornerRadius = FloatToDpConverter.convertFromDpToFloat(getResources().getDimension(br.com.taqtile.android.app.listings.R.dimen.card_corner_radius), getContext());
    babyDevelopmentCard.setRadius(cornerRadius);
    babyDevelopmentCard.setClipChildren(false);
    babyDevelopmentCard.setClipToPadding(false);
    setupBabyDevelopmentPlaceholderCard();
  }

  private void setupBabyDevelopmentPlaceholderCard() {
    cardLoading.getIndeterminateDrawable().setColorFilter(
      ContextCompat.getColor(getContext(), R.color.color_primary),
      android.graphics.PorterDuff.Mode.SRC_IN);
  }

  private void fetchGestationData() {
    showLoading();
    if (userDataSubscription != null) {
      userDataSubscription.unsubscribe();
    }
    userDataSubscription = presenter.fetchUserData()
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(this::onFetchSuccess,
        this::onFetchGestationDataFailure);
    compositeSubscription.add(userDataSubscription);
  }

  private void onFetchGestationDataFailure(Throwable throwable) {
    hideLoading();
    handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
  }

  private void onFetchSuccess(UserViewModel userViewModel) {
    this.userData = userViewModel;

    this.currentWeek = getValidWeek();
    this.gestationWeek = this.currentWeek;
    babyDevelopmentImage.setImageDrawable(getBabyDevelopmentImage(Integer.valueOf(currentWeek)));
    this.userIsPregnant = verifyUserType();

    if (userIsPregnant) {

      setBabyInformation(GestationTextFormatterHelper.composeFullChildbirth(getContext(), userViewModel));

      setProbableBirthdate(String.format(
        getString(R.string.fragment_gestation_information_gestation_header_childbirth),
        getChildbirth(userViewModel), userViewModel.getChildbirthEstimatedDateBase()));

      setBabyDevelopmentTitle(userViewModel.getBabyName());
      this.babyDevelopmentBody.setMaxLines(GESTATION_CARD_BODY_DEFAULT_LINES);

      this.reloadWeekSelector = true;
      setupToolbarMenu();

      fetchWeeklyContentData();
    }

    setupToolbar();
    setGestationInformationVisibility();
  }

  private void showCardLoading() {
    babyDevelopmentCard.setVisibility(View.INVISIBLE);
    cardLoadingContainer.setVisibility(View.VISIBLE);
  }

  private void hideCardLoading() {
    cardLoadingContainer.setVisibility(View.GONE);
    FadeEffectHelper.fadeInView(babyDevelopmentCard);
  }

  private String getValidWeek() {
    Integer currentWeek = userData.getGestationWeeks() != null ?
      userData.getGestationWeeks() : 0;
    if (currentWeek < MIN_WEEK) {
      return String.valueOf(MIN_WEEK);
    } else {
      return String.valueOf(currentWeek);
    }
  }

  private String getChildbirth(UserViewModel userViewModel) {
    return DateFormatterHelper.formatDateWithoutHours(userViewModel.getChildbirthEstimatedDate());
  }

  private boolean verifyUserType() {
    return userData.getUserType() != null && !userData.getUserType().isEmpty() &&
      userData.getUserType().equals(PREGNANT);
  }

  private void onFetchWeeklyContentFailure(Throwable throwable) {
    if (currentWeeklyContent == null) {
      hideLoading();
      handleError(DataSourceErrorToPresenterErrorMapper.map(throwable));
    } else {
      hideCardLoading();
      showSnackbar(mapSnackBarErrorString(throwable.getMessage()), Snackbar.LENGTH_LONG);
    }
  }

  private void setBabyDevelopmentTitle(String babyName) {
    if (babyName == null || babyName.isEmpty() || babyName.equals(getString(R.string.baby_name_default_text))) {
      babyName = getResources().getString(R.string.fragment_gestation_baby_development_details_baby_name_placeholder);
    }
    this.babyDevelopmentTitle.setText(getResources()
        .getString(R.string.fragment_gestation_development_title_text, babyName));
  }

  private void setBabyDevelopmentBody(String body) {
    this.babyDevelopmentBody.setText(body);
  }

  private void setGestationInformationVisibility() {
    if (userIsPregnant) {
      initialScreenContainer.setVisibility(View.GONE);
      gestationInformationContainer.setVisibility(View.VISIBLE);
    } else {
      initialScreenContainer.setVisibility(View.VISIBLE);
      gestationInformationContainer.setVisibility(View.GONE);
      hideLoading();
      hideOptionsMenu();
    }
  }

  private void hideOptionsMenu() {
    if (customToolbar != null) {
      customToolbar.hideOptionsMenu();
    }
  }

  private void setBirthplanProgress(String progress) {
    birthplanButton.setRightText(getResources()
      .getString(R.string.fragment_gestation_birthplan_progress_text, progress));
  }

  private void fetchWeeklyContentData() {
    if (weeklyContentSubscription != null) {
      weeklyContentSubscription.unsubscribe();
    }

    if (gestationPeriodExceeded()) {
      showLastGestationWeek();
    } else {
      weeklyContentSubscription = presenter.fetchWeeklyContent(new WeeklyContentParams(currentWeek))
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::onFetchWeeklyContentSuccess, this::onFetchWeeklyContentFailure);
      compositeSubscription.add(weeklyContentSubscription);
    }
  }

  private void showLastGestationWeek() {
    this.babyDevelopmentTitle.setGravity(Gravity.CENTER_HORIZONTAL);
    this.babyDevelopmentTitle.setText(getString(R.string.fragment_gestation_baby_development_last_week_header));
    babyDevelopmentBody.setMaxLines(GESTATION_FINISHED_CARD_MAX_LINES);
    setBabyDevelopmentBody(getString(R.string.fragment_gestation_baby_development_last_week_text));
    babyDevelopmentImage.setVisibility(View.GONE);
    weeklyContentSelectorContainer.setVisibility(View.GONE);
    gestationDateInformationContainer.setVisibility(View.GONE);
    gestationInformationArcView.setVisibility(View.GONE);

    setCardPosition();

    showWeeklyContentCard();
  }

  private void setCardPosition() {
    LinearLayout.LayoutParams layoutParams = ((LinearLayout.LayoutParams) cardContainer.getLayoutParams());
    layoutParams.topMargin = (int) getResources().getDimension(R.dimen.margin_large);
    cardContainer.setLayoutParams(layoutParams);

  }

  private void onFetchWeeklyContentSuccess(WeeklyContentViewModel weeklyContentViewModel) {
    this.currentWeeklyContent = weeklyContentViewModel;
    setBabyDevelopmentBody(weeklyContentViewModel.getWeekContentBody());
    PicassoHelper.loadImageFromPath(babyDevelopmentImage, weeklyContentViewModel.getWeekContentImage(), getContext());
    babyDevelopmentImage.setVisibility(View.VISIBLE);
    weeklyContentSelectorContainer.setVisibility(View.VISIBLE);
    gestationDateInformationContainer.setVisibility(View.VISIBLE);
    gestationInformationArcView.setVisibility(View.VISIBLE);
    populateWeekSelector();

    showWeeklyContentCard();
  }

  private void showWeeklyContentCard() {
    hideLoading();
    hideCardLoading();
  }

  private void setupToolbar() {
    if (getActivity() != null) {
      if (customToolbar == null) {
        customToolbar = getToolbar(CustomToolbar.class);
      }
      customToolbar.hideLeftButton();
      customToolbar.setTitle(getResources()
        .getString(R.string.fragment_gestation_title), Gravity.LEFT);
      ViewCompat.setElevation(getToolbar(), 0);
    }
  }

  private void setupToolbarMenu() {
    customToolbar.inflateOptionsMenu(R.menu.menu_overflow, this);
    customToolbar.getMenuItem(FIRST_MENU_ITEM_POSITION).setActionView(getOverflowButton());
  }

  private OverflowButton getOverflowButton() {
    OverflowButton overflowButton = new OverflowButton(getContext());
    setupOverflowButton(overflowButton);
    return overflowButton;
  }

  private void setupOverflowButton(OverflowButton overflowButton) {
    List<String> overflowOptions = new ArrayList<>();
    overflowOptions.add(getResources().getString(
      R.string.fragment_gestation_overflow_menu_option_text));
    overflowButton.onMenuItemSelected(this);
    overflowButton.setMenuList(overflowOptions);
  }

  private void setBabyInformation(String babyInformation) {
    this.babyInformation.setText(babyInformation);
  }

  private void setProbableBirthdate(String birthdate) {
    this.babyProbableBirthdate.setText(birthdate);
  }

  //Use this to populate the selector
  private void populateWeekSelector() {

    if (weeks != null) {
      weeks.clear();
      weekSelectorBuilder.clearSelector();
    }

    setupTrimesterColors();

    if (reloadWeekSelector) {
      defaultWeek = this.currentWeek;
    }

    //The last argument is the mother' pregnancy week, which should always appear with a different color
    weekSelectorBuilder = new HorizontalSelectorBuilder(weeksSelectorContainer,
      firstTrimesterColors.getTextActiveColor(), defaultWeek, getContext());

    buildSemesterWeeks();

    ArrayList<HorizontalSelectorColors> colors = buildWeekSelectorStyles();

    for (int i = 0; i < weeks.size(); i++) {
      addTrimesterWeeks(weeks.get(i), colors.get(i).getTextActiveColor(),
        colors.get(i).getTextInactiveColor(), colors.get(i).getBackgroundSelectedColor());
    }

    weekSelectorBuilder.setSelectedCell(currentWeek);
    weekSelectorBuilder.setDefaultTextPaintedCell();

    if (autoScroll) {
      weekSelectorBuilder.getCellToSelect(currentWeek).requestFocus();
      weeksSelectorScrollView.getViewTreeObserver().addOnGlobalLayoutListener(this);
      this.autoScroll = false;
    }
  }

  @Override
  public void onGlobalLayout() {
    weeksSelectorScrollView.smoothScrollTo(
      ((int) weekSelectorBuilder.getCellToSelect(currentWeek).getX()), 0);
    weeksSelectorScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
  }

  private void buildSemesterWeeks() {
    ArrayList<String[]> trimesterWeeks = new ArrayList<>();
    weeks = new ArrayList<>();
    trimesterWeeks.add(getResources()
      .getStringArray(R.array.fragment_gestation_first_trimester_weeks));
    trimesterWeeks.add(getResources()
      .getStringArray(R.array.fragment_gestation_second_trimester_weeks));
    trimesterWeeks.add(getResources()
      .getStringArray(R.array.fragment_gestation_third_trimester_weeks));
    weeks.add(trimesterWeeks.get(0));
    weeks.add(trimesterWeeks.get(1));
    weeks.add(trimesterWeeks.get(2));
  }

  private ArrayList<HorizontalSelectorColors> buildWeekSelectorStyles() {
    ArrayList<HorizontalSelectorColors> colors = new ArrayList<>();
    colors.add(firstTrimesterColors);
    colors.add(firstTrimesterColors);
    colors.add(firstTrimesterColors);
    return colors;
  }

  private void addTrimesterWeeks(String[] weeks, int textActiveColor, int textInactiveColor,
                                 int backgroundSelectedColor) {
    for (int i = 0; i < weeks.length; i++) {
      weekSelectorBuilder.addSelectorCell(weeks[i], getListener(weeks[i]), textActiveColor,
        getInactiveColor(weeks[i], textInactiveColor), backgroundSelectedColor);
    }
  }

  private int getInactiveColor(String week, int textInactiveColor) {
    return Integer.valueOf(week) <= Integer.valueOf(this.gestationWeek) ? textInactiveColor :
      ContextCompat.getColor(getContext(), R.color.color_gray);
  }

  private HorizontalSelectorCell.Listener getListener(String week) {
    return Integer.valueOf(week) <= Integer.valueOf(this.gestationWeek) ? this : null;
  }

  private void setupTrimesterColors() {
    int textDefaultColor = ContextCompat.getColor(getContext(), R.color.color_gray_dark);
    firstTrimesterColors = new HorizontalSelectorColors(ContextCompat.getColor(getContext(),
      R.color.color_week_text_selected_color_first_trimester),
      textDefaultColor, ContextCompat.getColor(getContext(),
      R.color.color_week_background_selected_color_first_trimester));
  }

  private void onWeeklyContentClick() {
    if (gestationPeriodExceeded()) {
      onEndGestationConfirmationButtonClick();
    } else {
      navigationManager.showWeeklyContentDetailsUI(userData, currentWeeklyContent);
    }
  }

  private boolean gestationPeriodExceeded() {
    return Integer.valueOf(currentWeek) > MAX_WEEK;
  }

  private void onStartGestationButtonClick() {
    if (userHasPreviousGestationInformation()) {
      showStartGestationConfirmationDialog();
    } else {
      showStartGestationForm();
    }
  }

  private boolean userHasPreviousGestationInformation() {
    // TODO: 5/17/17 ADD VERIFICATION LOGIC AFTER CLIENT DEFINITION (IT WILL BE A FUTURE FEATURE)
    return false;
  }

  private void showStartGestationForm() {
    setGestationInformationVisibility();
    navigationManager.showGestationEditUI(userData, this::returningFromEditGestationData);
  }

  private void showStartGestationConfirmationDialog() {
    new AlertDialog.Builder(getContext())
      .setTitle(getResources().getString(R.string.fragment_gestation_start_gestation_dialog_title))
      .setMessage(getResources().getString(R.string.fragment_gestation_start_gestation_dialog_body))
      .setPositiveButton(R.string.conclude, (dialog, which) -> showStartGestationForm())
      .setNegativeButton(R.string.cancel, (dialog, which) -> {
        // do nothing
      })
      .show();
  }

  private void onSymptomGuideButtonClick() {
    navigationManager.showSymptomGuideUI();
  }

  private void onSymptomHistoryButtonClick() {
    navigationManager.showSymptomHistoryUI();
  }

  private void onClinicalConditionsButtonClick() {
    navigationManager.showClinicalConditionsUI();
  }

  private void onMedicalRecommendationsButtonClick() {
    navigationManager.showMedicalRecommendations();
  }

  private void onBirthPlanButtonClick() {
    navigationManager.showBirthPlanUI(this::returningFromBirthPlan);
  }

  private void returningFromBirthPlan(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      setBirthplanProgress(bundle.getString(BIRTH_PLAN_ANSWERED_PERCENTAGE_KEY, "?"));
    }
  }

  private void onPreNatalButtonClick() {
    navigationManager.showPrenatalCard();
  }

  private void onEndGestationButtonClick() {
    if(gestationPeriodExceeded()) {
      onEndGestationConfirmationButtonClick();
    }
    else {
      showEndGestationConfirmationDialog();
    }
  }

  private void showEndGestationConfirmationDialog() {
    new AlertDialog.Builder(getContext())
      .setTitle(getResources().getString(R.string.fragment_gestation_end_gestation_dialog_title))
      .setMessage(getResources().getString(R.string.fragment_gestation_end_gestation_dialog_body))
      .setPositiveButton(R.string.conclude, (dialog, which) -> onEndGestationConfirmationButtonClick())
      .setNegativeButton(R.string.cancel, (dialog, which) -> {
        // do nothing
      })
      .show();
  }

  private void onEndGestationConfirmationButtonClick() {
    navigationManager.showGestationConclusionUI(this::returningFromEndGestation);
  }

  private void returningFromEndGestation(Bundle bundle, int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      fetchGestationData();
    }
  }

  public void setPresenter(GestationContract.Presenter presenter) {
    this.presenter = presenter;
  }

  public void setNavigationManager(NavigationManager navigationManager) {
    this.navigationManager = navigationManager;
  }

  @Override
  public boolean refresh() {
    return false;
  }

  @Override
  public void onPause() {
    super.onPause();
    this.unsubscribeAll();
  }

  @Override
  public void willDisappear() {
    super.willDisappear();
    this.unsubscribeAll();
    this.hideOptionsMenu();
  }

  @Override
  public int getPlaceholderViewContainerId() {
    return R.id.fragment_gestation_placeholder;
  }

  @Override
  public void onPlaceholderButtonClick() {
    super.onPlaceholderButtonClick();
    fetchGestationData();
  }

  @Override
  public void onSelectorClick(boolean state, String number) {
    showCardLoading();
    weekSelectorBuilder.setSelectedCell(number);
    currentWeek = number;
    this.reloadWeekSelector = false;
    this.autoScroll = false;
    fetchWeeklyContentData();
    babyDevelopmentImage.setImageDrawable(getBabyDevelopmentImage(Integer.valueOf(currentWeek)));
  }

  @Override
  public void onItemSelected(String item) {
    navigationManager.showGestationEditUI(userData, this::returningFromEditGestationData);
  }

  private void returningFromEditGestationData(Bundle bundle, int responseCode) {
    if (responseCode == Activity.RESULT_OK) {
      onFetchSuccess(bundle.getParcelable(USER_DATA_KEY));
    }
  }

  private Drawable getBabyDevelopmentImage(int week) {
    if (0 < week && week <= 6) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_1);
    } else if (7 <= week && week <= 10) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_2);
    } else if (11 <= week && week <= 14) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_3);
    } else if (15 <= week && week <= 18) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_4);
    } else if (19 <= week && week <= 23) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_5);
    } else if (24 <= week && week <= 28) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_6);
    } else if (29 <= week && week <= 33) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_7);
    } else if (34 <= week && week <= 37) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_8);
    } else if (38 <= week && week <= 42) {
      return ContextCompat.getDrawable(getContext(), R.drawable.ic_gestation_development_9);
    } else {
      return null;
    }
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    return false;
  }

  @Override
  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
    hidePlaceholder();
    gestationContainer.setVisibility(View.GONE);
  }

  @Override
  public void hideLoading() {
    loading.setVisibility(View.GONE);
    gestationContainer.setVisibility(View.VISIBLE);
  }

}
