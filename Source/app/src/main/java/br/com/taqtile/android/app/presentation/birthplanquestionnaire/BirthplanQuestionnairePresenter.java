package br.com.taqtile.android.app.presentation.birthplanquestionnaire;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.models.EmptyResult;
import br.com.taqtile.android.app.domain.birthPlan.SendBirthPlanQAUseCase;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerParams;
import br.com.taqtile.android.app.domain.questionnaires.models.AnswerQuestionnairesParams;
import br.com.taqtile.android.app.domain.questionnaires.models.QuestionItemResult;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 11/05/17.
 */

public class BirthplanQuestionnairePresenter implements BirthplanQuestionnaireContract.Presenter {

  private SendBirthPlanQAUseCase sendBirthPlanQAUseCase;

  public BirthplanQuestionnairePresenter() {
    this.sendBirthPlanQAUseCase = Injection.provideSendBirthPlanQAUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<EmptyResult> sendBirthPlanQuestionAnswers(List<AnswerParams>
                                                                             answerParams) {
    return sendBirthPlanQAUseCase.execute(new AnswerQuestionnairesParams(answerParams))
      .map(any -> new EmptyResult());
  }
}
