package br.com.taqtile.android.app.domain.forum.models;

/**
 * Created by taqtile on 12/07/17.
 */

public class ContentSuggestionItemResult {

  private Integer id;
  private Integer contentId;
  private Integer contentParentId;
  private String type;
  private String title;
  private String resume;
  private Integer likes;
  private Integer countReplies;

  public ContentSuggestionItemResult(Integer id, String type, String title, String resume, Integer likes, Integer countReplies, Integer contentParentId, Integer contentId) {
    this.id = id;
    this.type = type;
    this.title = title;
    this.resume = resume;
    this.likes = likes;
    this.countReplies = countReplies;
    this.contentId = contentId;
    this.contentParentId = contentParentId;
  }

  public Integer getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }

  public String getResume() {
    return resume;
  }

  public Integer getLikes() {
    return likes;
  }

  public Integer getCountReplies() {
    return countReplies;
  }

  public Integer getContentId() {
    return contentId;
  }

  public Integer getContentParentId() {
    return contentParentId;
  }

}
