package br.com.taqtile.android.app.data.forum.remote.models.request;

import br.com.taqtile.android.app.data.common.remote.RequestName;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;

/**
 * Created by taqtile on 12/04/17.
 */

public class WarnPostRemoteRequest extends BaseRemoteRequest {

  @RequestName("post_id")
  private Integer postId;

  private String content;

  public WarnPostRemoteRequest(Integer postId, String content) {
    this.postId = postId;
    this.content = content;
  }

}
