package br.com.taqtile.android.app.data.followcard.remote;

import java.util.List;

import br.com.taqtile.android.app.data.common.remote.BaseRemoteDataSource;
import br.com.taqtile.android.app.data.common.remote.ServiceFactory;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardConsultationsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardExamsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardUltrasoundsRemoteResponse;
import br.com.taqtile.android.app.data.followcard.remote.models.FollowCardVaccinesRemoteResponse;
import rx.Observable;

/**
 * Created by taqtile on 5/19/17.
 */

public class FollowCardRemoteDataSource extends BaseRemoteDataSource {

  private Observable<FollowCardServices> services;

  public FollowCardRemoteDataSource() {
    this.services = ServiceFactory.getObservableAuthenticated(FollowCardServices.class);
  }

  public Observable<BaseRemoteResponse<FollowCardRemoteResponse>> fetchFollowCard() {
    return performRequest(services.flatMap(FollowCardServices::fetchFollowCard));
  }

  public Observable<BaseRemoteResponse<List<FollowCardConsultationsRemoteResponse>>> fetchFollowCardConsultations() {
    return performRequest(services.flatMap(FollowCardServices::fetchFollowCardConsultations));
  }

  public Observable<BaseRemoteResponse<List<FollowCardExamsRemoteResponse>>> fetchFollowCardExams() {
    return performRequest(services.flatMap(FollowCardServices::fetchFollowCardExams));
  }

  public Observable<BaseRemoteResponse<List<FollowCardVaccinesRemoteResponse>>> fetchFollowCardVaccines() {
    return performRequest(services.flatMap(FollowCardServices::fetchFollowCardVaccines));
  }

  public Observable<BaseRemoteResponse<List<FollowCardUltrasoundsRemoteResponse>>> fetchFollowCardUltrasounds() {
    return performRequest(services.flatMap(FollowCardServices::fetchFollowCardUltrasounds));
  }
}
