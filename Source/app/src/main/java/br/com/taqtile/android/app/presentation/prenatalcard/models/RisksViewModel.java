package br.com.taqtile.android.app.presentation.prenatalcard.models;

import java.util.ArrayList;

/**
 * Created by taqtile on 5/23/17.
 */

public class RisksViewModel {

  private ArrayList<String> alert;
  private ArrayList<String> risk;

  public RisksViewModel(ArrayList<String> alert, ArrayList<String> risk) {
    this.alert = alert;
    this.risk = risk;
  }

  public ArrayList<String> getAlert() {
    return alert;
  }

  public ArrayList<String> getRisk() {
    return risk;
  }

}
