package br.com.taqtile.android.app.data.city.geocondingAPI;

import java.util.List;

import br.com.taqtile.android.app.data.city.geocondingAPI.models.LocationDiscoveryGeocodingAPIResponse;
import br.com.taqtile.android.app.data.common.geocoding.GeocodingServiceFactory;
import br.com.taqtile.android.app.data.common.geocoding.models.BaseGeocodingResponse;
import br.com.taqtile.android.app.data.common.remote.ResponseValidator;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by taqtile on 6/2/17.
 */

public class LocationDiscoveryGeocodingAPIDataSource {

  private Observable<LocationDiscoveryServices> services;

  public LocationDiscoveryGeocodingAPIDataSource() {
    this.services = GeocodingServiceFactory.getGeocodingServiceFactory();
  }

  public Observable<BaseGeocodingResponse<List<LocationDiscoveryGeocodingAPIResponse>>> searchCityByZipCode(String zipCode) {
    return performRequest(services.flatMap(services -> services.searchCityByZipCode(zipCode)));
  }

  private <T> Observable<BaseGeocodingResponse<T>> performRequest(
    Observable<Result<BaseGeocodingResponse<T>>> observableResponse) {
    return observableResponse.flatMap(ResponseValidator::convertResultToModel)
      .subscribeOn(Schedulers.io());
  }
}
