package br.com.taqtile.android.app.data.exams.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsGetRemoteResponse;
import br.com.taqtile.android.app.data.exams.remote.models.response.ExamsInfoItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 3/22/17.
 */

public interface ExamsServices {

  @POST("agenda/ultrassom/result")
  Observable<Result<BaseRemoteResponse<ExamsGetRemoteResponse>>> list();

  // TODO: 6/20/17 Will be removed later. It is an easy and fast approach to cover an APIs issue
  @POST("agenda/ultrassom/result")
  Observable<Result<BaseRemoteResponse<Boolean>>> legacyList();

  @FormUrlEncoded
  @POST("agenda/ultrassom")
  Observable<Result<BaseRemoteResponse<List<ExamsInfoItemRemoteResponse>>>> createUltrasound(@FieldMap Map<String, String> request);
}
