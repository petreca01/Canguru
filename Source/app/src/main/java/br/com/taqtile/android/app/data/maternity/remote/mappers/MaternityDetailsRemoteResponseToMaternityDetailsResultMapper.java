package br.com.taqtile.android.app.data.maternity.remote.mappers;

import br.com.taqtile.android.app.data.channel.mappers.ChannelPostsItemsRemoteResponseToChannelPostsMapper;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityDetailsRemoteResponse;
import br.com.taqtile.android.app.domain.channel.models.ChannelPost;
import br.com.taqtile.android.app.domain.maternity.models.MaternityCommentResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDataResult;
import br.com.taqtile.android.app.domain.maternity.models.MaternityDetailsResult;
import java.util.List;

/**
 * Created by taqtile on 06/04/17.
 */

public class MaternityDetailsRemoteResponseToMaternityDetailsResultMapper {

  public static MaternityDetailsResult perform(
    MaternityDetailsRemoteResponse maternityDetailsRemoteResponse) {

    MaternityDataResult maternityDataResult =
      MaternityItemRemoteResponseToMaternityDataResultMapper.perform(
        maternityDetailsRemoteResponse.getHospital());

    List<MaternityCommentResult> comments =
      MaternityCommentItemRemoteResponseListToMaternityCommentResultListMapper.perform(
        maternityDetailsRemoteResponse.getComments());

    List<ChannelPost> posts = ChannelPostsItemsRemoteResponseToChannelPostsMapper.perform(
      maternityDetailsRemoteResponse.getChannelPosts());

    return new MaternityDetailsResult(maternityDataResult, comments, posts);
  }
}
