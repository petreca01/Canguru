package br.com.taqtile.android.app.presentation.channel;

import br.com.taqtile.android.app.listings.viewmodels.interfaces.ChannelListingViewModel;
import br.com.taqtile.android.app.listings.viewmodels.interfaces.SearchListingViewModel;
import br.com.taqtile.android.cleanbase.presentation.presenter.BasePresenter;
import java.util.List;
import rx.Observable;

/**
 * Created by taqtile on 3/22/17.
 */

public interface ChannelContract {

    interface Presenter extends BasePresenter {
        Observable<List<SearchListingViewModel>> fetchChannelList();
    }

}
