package br.com.taqtile.android.app.presentation.initiallogin;

import android.app.Activity;
import br.com.taqtile.android.app.domain.signin.FacebookLoginUseCase;
import br.com.taqtile.android.app.domain.signin.Models.FacebookLoginResult;
import br.com.taqtile.android.app.support.Injection;
import br.com.taqtile.android.app.support.NavigationManager;
import com.facebook.CallbackManager;
import rx.Observable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by taqtile on 11/11/16.
 */

public class InitialLoginPresenter implements InitialLoginContract.Presenter {

  private InitialLoginContract.View view;
  private FacebookLoginUseCase facebookLoginUseCase;

  public InitialLoginPresenter(InitialLoginContract.View initialLoginFragment,
    NavigationManager navigationManager, Activity activity, CallbackManager callbackManager) {
    view = checkNotNull(initialLoginFragment, "Initial login fragment cannot be null");

    view.setPresenter(this);
    view.setNavigationManager(navigationManager);

    facebookLoginUseCase = Injection.provideFacebookLoginUseCase(activity, callbackManager);
  }

  @Override public void start() {

  }

  @Override public void resume() {

  }

  @Override
  public Observable<FacebookLoginResult> facebookLoginButtonTapped() {
    return facebookLoginUseCase.execute(null);
  }
}
