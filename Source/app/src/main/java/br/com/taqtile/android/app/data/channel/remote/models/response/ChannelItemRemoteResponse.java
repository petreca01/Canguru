package br.com.taqtile.android.app.data.channel.remote.models.response;

import android.os.IBinder;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/17/17.
 */

public class ChannelItemRemoteResponse {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("author")
    private String author;

    @SerializedName("description")
    private String description;

    @SerializedName("logo")
    private String logo;

    @SerializedName("cover")
    private String cover;

    @SerializedName("active")
    private Boolean active;

    @SerializedName("total_posts")
    private Integer totalPosts;

    @SerializedName("followers")
    private Integer followers;

    @SerializedName("following")
    private Boolean following;

    public Integer getId() {
        return id;
    };

    public String getName() {
        return name;
    };

    public String getAuthor() {
        return author;
    };

    public String getDescription() {
        return description;
    };

    public String getLogo() {
        return logo;
    };

    public String getCover() {
        return cover;
    };

    public Boolean getActive() {
        return active;
    };

    public Integer getTotalPosts() {
        return totalPosts;
    };

    public Integer getFollowers() {
        return followers;
    };

    public Boolean getFollowing() {
        return following;
    };

}
