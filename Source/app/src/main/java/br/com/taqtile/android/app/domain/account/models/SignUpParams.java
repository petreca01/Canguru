package br.com.taqtile.android.app.domain.account.models;

/**
 * Created by taqtile on 31/03/17.
 */

public class SignUpParams {

  private String email;
  private String password;
  private String name;
  private String userType;
  private String lastPeriod;
  private String birthDate;

  public SignUpParams(String email, String password, String name, String birthDate) {
    this.email = email;
    this.password = password;
    this.name = name;
    this.userType = UserType.planning;
    this.birthDate = birthDate;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getName() {
    return name;
  }

  public String getUserType() {
    return userType;
  }

  public String getLastPeriod() {
    return lastPeriod;
  }

  public String getBirthDate() {
    return birthDate;
  }
}
