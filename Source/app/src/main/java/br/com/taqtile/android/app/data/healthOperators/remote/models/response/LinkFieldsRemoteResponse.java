package br.com.taqtile.android.app.data.healthOperators.remote.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by taqtile on 3/24/17.
 */

public class LinkFieldsRemoteResponse {
    @SerializedName("identifier")
    private HealthOperatorIdentifierRemoteResponse identifier;

    public HealthOperatorIdentifierRemoteResponse getIdentifier() {
        return identifier;
    }
}
