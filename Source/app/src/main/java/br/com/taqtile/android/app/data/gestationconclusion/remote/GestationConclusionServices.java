package br.com.taqtile.android.app.data.gestationconclusion.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.gestationconclusion.remote.models.GestationConclusionQuestionItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by taqtile on 7/10/17.
 */

public interface GestationConclusionServices {
  @FormUrlEncoded
  @POST("after_birth")
  Observable<Result<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>>>
  listQuestionsAndAnswers(@FieldMap Map<String, String> listQuestionsAndAnswersRequestModel);

  @FormUrlEncoded
  @POST("after_birth/answer")
  Observable<Result<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>>>
  answer(@FieldMap Map<String, String> answersRequestModel);

  @FormUrlEncoded
  @POST("after_birth/answer/remove")
  Observable<Result<BaseRemoteResponse<List<GestationConclusionQuestionItemRemoteResponse>>>>
  removeAnswer(@FieldMap Map<String, String> removeAnswerRequestModel);
}
