package br.com.taqtile.android.app.domain.gestationconclusion;

import java.util.List;

import br.com.taqtile.android.app.listings.viewmodels.questionsviewmodels.GestationConclusionQuestionViewModel;

/**
 * Created by taqtile on 7/11/17.
 */

public class GestationConclusionViewModel {


  private List<GestationConclusionQuestionViewModel> questionViewModels;
  private List<GestationConclusionQuestionViewModel> answeredQuestionViewModels;

  public GestationConclusionViewModel(List<GestationConclusionQuestionViewModel> questionViewModels,
                                     List<GestationConclusionQuestionViewModel> answeredQuestionViewModels) {
    this.questionViewModels = questionViewModels;
    this.answeredQuestionViewModels = answeredQuestionViewModels;
  }

  public List<GestationConclusionQuestionViewModel> getQuestionViewModels() {
    return questionViewModels;
  }

  public List<GestationConclusionQuestionViewModel> getAnsweredQuestionViewModels() {
    return answeredQuestionViewModels;
  }

}
