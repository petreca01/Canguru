package br.com.taqtile.android.app.presentation.searchhealthcare;

import java.util.List;

import br.com.taqtile.android.app.domain.healthOperators.ListHealthOperatorsUseCase;
import br.com.taqtile.android.app.domain.healthOperators.SearchHealthOperatorsUseCase;
import br.com.taqtile.android.app.domain.healthOperators.models.SearchHealthOperatorParams;
import br.com.taqtile.android.app.listings.viewmodels.HealthOperatorViewModel;
import br.com.taqtile.android.app.support.Injection;
import rx.Observable;

/**
 * Created by taqtile on 18/04/17.
 */

public class SearchHealthcarePresenter implements SearchHealthcareContract.Presenter {

  private ListHealthOperatorsUseCase listHealthOperatorsUseCase;
  private SearchHealthOperatorsUseCase searchHealthOperatorsUseCase;

  public SearchHealthcarePresenter() {
    this.listHealthOperatorsUseCase = Injection.provideListHealthOperatorsUseCase();
    this.searchHealthOperatorsUseCase = Injection.provideSearchHealthOperatorsUseCase();
  }

  @Override
  public void start() {

  }

  @Override
  public void resume() {

  }

  @Override
  public Observable<List<HealthOperatorViewModel>> fetchListHealthOperators() {
    return listHealthOperatorsUseCase.execute(null);
  }

  @Override
  public Observable<List<HealthOperatorViewModel>> fetchSearchHealthOperators(SearchHealthOperatorParams searchTerm) {
    return searchHealthOperatorsUseCase.execute(searchTerm);
  }
}
