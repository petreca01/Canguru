package br.com.taqtile.android.app.domain.notification.models;

/**
 * Created by taqtile on 07/04/17.
 */

public class NotificationCountResult {

  private Integer notifications;

  private Integer inboxes;

  public NotificationCountResult(Integer notifications, Integer inboxes) {
    this.notifications = notifications;
    this.inboxes = inboxes;
  }

  public Integer getNotifications() {
    return notifications;
  }

  public Integer getInboxes() {
    return inboxes;
  }
}
