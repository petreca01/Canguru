package br.com.taqtile.android.app.data.maternity.remote;

import java.util.List;
import java.util.Map;

import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteRequest;
import br.com.taqtile.android.app.data.common.remote.models.BaseRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityCommentItemRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityDetailsRemoteResponse;
import br.com.taqtile.android.app.data.maternity.remote.models.response.MaternityItemRemoteResponse;
import retrofit2.adapter.rxjava.Result;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by taqtile on 3/23/17.
 */

public interface MaternityServices {
    @POST("hospitals/cities")
    Observable<Result<BaseRemoteResponse<List<String>>>> listCities();

    @POST("hospitals/city/{cityName}")
    Observable<Result<BaseRemoteResponse<List<MaternityItemRemoteResponse>>>> listByCity(@Path("cityName") String cityName);

    @POST("hospitals/{maternityId}")
    Observable<Result<BaseRemoteResponse<MaternityDetailsRemoteResponse>>> getDetails(@Path("maternityId") Integer maternityId);

    @FormUrlEncoded
    @POST("hospitals/{maternityId}/rate")
    Observable<Result<BaseRemoteResponse<MaternityItemRemoteResponse>>> rate(@FieldMap Map<String, String> rateRequestModel, @Path("maternityId") Integer maternityId);

    @FormUrlEncoded
    @POST("hospitals/{maternityId}/comment")
    Observable<Result<BaseRemoteResponse<List<MaternityCommentItemRemoteResponse>>>> comment(@FieldMap Map<String, String> commentRequestModel, @Path("maternityId") Integer maternityId);

    @DELETE("hospitals/{maternityId}/comment/{commentId}")
    Observable<Result<BaseRemoteResponse<List<MaternityCommentItemRemoteResponse>>>> deleteComment(@Path("maternityId") Integer maternityId, @Path("commentId") Integer commentId);

    @POST("hospitals/{maternityId}/follow_unfollow")
    Observable<Result<BaseRemoteResponse<MaternityItemRemoteResponse>>> followOrUnfollow(@Path("maternityId") Integer maternityId);
}
