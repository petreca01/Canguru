package br.com.taqtile.android.app.domain.timeline;

import android.support.annotation.Nullable;

import br.com.taqtile.android.app.data.channel.ChannelRepository;
import br.com.taqtile.android.app.data.forum.ForumRepository;
import br.com.taqtile.android.app.domain.forum.models.PostsListParams;
import br.com.taqtile.android.app.domain.timeline.models.TimelineModel;
import rx.Observable;

/**
 * Created by taqtile on 3/21/17.
 */

public class FetchTimelineUseCase {

  protected ForumRepository forumRepository;
  protected ChannelRepository channelRepository;
  protected Integer currentPage;

  public FetchTimelineUseCase(ForumRepository forumRepository, ChannelRepository channelRepository) {
    this.forumRepository = forumRepository;
    this.channelRepository = channelRepository;
    currentPage = 1;
  }

  public Observable<TimelineModel> execute(boolean reload) {

    int page = this.currentPage;
    int size = PostsListParams.DEFAULT_PER_PAGE;
    boolean fetchChannel = true;

    if(reload) {
      //  get updated data for current timeline, keeping all pages already loaded
      page = 1;
      size = this.currentPage * PostsListParams.DEFAULT_PER_PAGE;
    } else{
      // get only the next page
      this.currentPage++;
      fetchChannel = false;
    }
    return execute(page, size, fetchChannel);
  }

  private Observable<TimelineModel> execute(int page, int size, boolean fetchChannel){

    TimelineModel timeline = new TimelineModel();
    return this.forumRepository
      .listPosts(getForumRequestParams(page, size))
      .flatMap(forumPostResults -> {
        timeline.setPostResults(forumPostResults);
        if (fetchChannel){
          return fetchPostsFromChannels(timeline);
        }else {
          return Observable.just(timeline);
        }
      });
  }

  private PostsListParams getForumRequestParams(int page, int size) {

    return new PostsListParams(
      page,
      size,
      PostsListParams.ORDER_DIRECTION_DESC,
      PostsListParams.ORDER_BY_CREATED_AT,
      null);
  }

  protected Observable<TimelineModel> fetchPostsFromChannels(TimelineModel timeline) {

    return this.channelRepository
      .listPostsOfFollowedChannels()
      .map(channelPosts -> {
        timeline.setChannelPosts(channelPosts);
        return timeline;
      }).onErrorReturn(t -> timeline);
  }
}
