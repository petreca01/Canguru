package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 3/13/17.
 */

public class ObservationLabelEditTextCaption extends GenericLabelEditTextCaption {

    public ObservationLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ObservationLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservationLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.observations_input_placeholder;
    }
}
