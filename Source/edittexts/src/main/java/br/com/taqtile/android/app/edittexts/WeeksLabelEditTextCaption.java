package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by taqtile on 12/05/17.
 */

public class WeeksLabelEditTextCaption extends GenericNumberLabelEditTextCaption {
  public WeeksLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public WeeksLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public WeeksLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.weeks_input_placeholder;
  }
}
