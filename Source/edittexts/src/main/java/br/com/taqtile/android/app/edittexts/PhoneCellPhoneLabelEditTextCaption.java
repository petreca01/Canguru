package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.TelephoneLabelEditTextCaption;

/**
 * Created by taqtile on 20/04/17.
 */

public class PhoneCellPhoneLabelEditTextCaption extends TelephoneLabelEditTextCaption {

  public PhoneCellPhoneLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setup();
  }

  public PhoneCellPhoneLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
    setup();
  }

  public PhoneCellPhoneLabelEditTextCaption(Context context) {
    super(context);
    setup();
  }

  public void setup(){
    this.setMaskFormat(getResources().getString(R.string.mask_phone_or_cell_phone));
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.phone_cellphone_input_placeholder;
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_phone_cell_phone_regex;
  }

  @Override
  public int getClearBtnDrawableId() {
    return 0;
  }
}
