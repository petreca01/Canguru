package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 3/13/17.
 */

public class MessageBodyLabelEditTextCaption extends GenericLabelEditTextCaption {

    public MessageBodyLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MessageBodyLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageBodyLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.message_body_input_placeholder;
    }

    @Override
    public int getFormType() {
        return TEXTAREA;
    }

}
