package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;

/**
 * Created by taqtile on 11/15/16.
 */

public class OldEmailLabelEditTextCaption extends EmailLabelEditTextCaption {
    public OldEmailLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OldEmailLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OldEmailLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.old_email_input_placeholder;
    }
}
