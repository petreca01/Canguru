package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.BirthdateLabelEditTextCaption;

/**
 * Created by taqtile on 12/05/17.
 */

public class ExamDateLabelEditTextCaption extends BirthdateLabelEditTextCaption{
  public ExamDateLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public ExamDateLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ExamDateLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.exam_date_input_placeholder;
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_birth_date_error_caption;
  }
}
