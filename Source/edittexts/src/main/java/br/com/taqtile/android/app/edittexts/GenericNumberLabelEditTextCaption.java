package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;

/**
 * Created by taqtile on 12/05/17.
 */

public class GenericNumberLabelEditTextCaption extends CustomLabelEditTextCaption {
  public GenericNumberLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public GenericNumberLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public GenericNumberLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.generic_form_input_placeholder;
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_generic_regex;
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_generic_form_error_caption;
  }

  @Override
  public int getFormType() {
    return NUMBER;
  }

  @Override
  public int getClearBtnDrawableId() {
    return 0;
  }

  @Override
  public int getCheckDrawableId() {
    return 0;
  }

  @Override
  public int getErrorCaptionDrawableId() {
    return 0;
  }
}
