package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.BirthdateLabelEditTextCaption;

/**
 * Created by taqtile on 12/05/17.
 */

public class LastPeriodLabelEditTextCaption extends BirthdateLabelEditTextCaption {

  private long MIN_PERIOD_DAYS = 28;
  private long MAX_PERIOD_DAYS = 294;
  private final String ddMMyyyy = "dd/MM/yyyy";

  public LastPeriodLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public LastPeriodLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public LastPeriodLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.last_period_input_placeholder;
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_birth_date_error_caption;
  }

  @Override
  public int additionalValidation() {

    int state = CustomLabelEditTextCaption.NO_STATE;

    // reset the messages
    this.setErrorCaption(this.getResources().getString(R.string.validation_birth_date_error_caption));
    this.setupErrorCaption(this.errorCaption);

    SimpleDateFormat parser = new SimpleDateFormat(ddMMyyyy);
    Date date;

    try {
      date = parser.parse(this.getText());

      if (!isDatePeriodValid(date)) {

        this.setErrorCaption(this.getResources().getString(R.string.validation_dum_error_caption));
        this.setupErrorCaption(this.errorCaption);
        state = CustomLabelEditTextCaption.STATE_INVALID;
      }

    } catch (Exception e) {
      state = CustomLabelEditTextCaption.STATE_INVALID;
    }

    return state;
  }

  private boolean isDatePeriodValid(Date date) {

    try {

      long dateInMilliseconds = date.getTime();
      long currentDateInMillisecond = System.currentTimeMillis();
      long dateDifference = currentDateInMillisecond - dateInMilliseconds;

      return dateDifference >= TimeUnit.DAYS.toMillis(MIN_PERIOD_DAYS) && dateDifference <= TimeUnit.DAYS.toMillis(MAX_PERIOD_DAYS);

    }
    catch (Exception e){
      return false;
    }
  }
}
