package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;

/**
 * Created by taqtile on 10/11/16.
 */

public class CreditCardExpirationDateLabelEditTextCaption extends CustomLabelEditTextCaption {

    public CreditCardExpirationDateLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public CreditCardExpirationDateLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public CreditCardExpirationDateLabelEditTextCaption(Context context) {
        super(context);
        setup();
    }

    private void setup(){
        setMaskFormat(getResources().getString(R.string.mask_cc_valid_date));
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.cc_valid_date_input_placeholder;
    }

    @Override
    public int getValidationRegexRes() {
        return R.string.validation_cc_valid_date_regex;
    }

    @Override
    public int getErrorCaptionTextRes() {
        return R.string.validation_cc_valid_date_error_caption;
    }

    @Override
    public int getFormType() {
        return 2;
    }

    @Override
    public int getClearBtnDrawableId() {
        return 0;
    }

    @Override
    public int getCheckDrawableId() {
        return 0;
    }

    @Override
    public int getErrorCaptionDrawableId() {
        return 0;
    }
}
