package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by Renato on 4/19/17.
 */

public class WeightLabelEditTextCaption  extends GenericLabelEditTextCaption {

  public WeightLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public WeightLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public WeightLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.weight_input_placeholder;
  }

  @Override
  public int getFormType() {
    return NUMBER;
  }

}
