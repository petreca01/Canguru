package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by taqtile on 12/05/17.
 */

public class BabyNameEditTextCaption extends GenericTextLabelEditTextCaption {
  public BabyNameEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public BabyNameEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public BabyNameEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.baby_name_input_placeholder;
  }

}
