package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 3/13/17.
 */

public class MessageTopicLabelEditTextCaption extends GenericLabelEditTextCaption {

  private static int MAX_SIZE = 75;
  private MessageTopicLabelEditTextCaptionListener listener;

  public MessageTopicLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setup();
  }

  public MessageTopicLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
    setup();
  }

  public MessageTopicLabelEditTextCaption(Context context) {
    super(context);
    setup();
  }

  private void setup(){
    setMaxLength(MAX_SIZE);
    setTextChangedListener(getTextChangedListener());
  }

  public TextWatcher getTextChangedListener() {
    return new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (listener != null) {
          listener.onTextChanged(charSequence.toString());
        }
        if (charSequence.length() < MAX_SIZE) {
          setHintCaption("");
        } else {
          setHintCaption(getResources().getString(R.string.validation_topic_max_size_error_caption, MAX_SIZE));
        }
      }

      @Override
      public void afterTextChanged(Editable editable) {}
    };
  }

  @Override
    public int getFormType() {
      return TEXTAREA;
    }

  @Override
  public int getInputPlaceholderTextRes() {
      return R.string.message_topic_input_placeholder;
  }

  public interface MessageTopicLabelEditTextCaptionListener {
    void onTextChanged(String text);
  }

  public void onTextChanged(MessageTopicLabelEditTextCaptionListener listener) {
    this.listener = listener;
  }
}
