package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.PasswordLabelEditTextCaption;

/**
 * Created by taqtile on 11/15/16.
 */

public class OldPasswordLabelEditTextCaption extends PasswordLabelEditTextCaption {
    public OldPasswordLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OldPasswordLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OldPasswordLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.old_password_input_placeholder;
    }
}
