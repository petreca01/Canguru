package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 3/13/17.
 */

public class AboutYouLabelEditTextCaption extends GenericLabelEditTextCaption {

    public AboutYouLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AboutYouLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AboutYouLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public @StringRes int getInputPlaceholderTextRes() {
        return R.string.about_you_input_placeholder;
    }

  @Override
  public int getFormType() {
    return TEXTAREA;
  }
}
