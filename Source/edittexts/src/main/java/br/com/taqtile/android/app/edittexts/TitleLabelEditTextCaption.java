package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 27/04/17.
 */

public class TitleLabelEditTextCaption extends GenericLabelEditTextCaption {
  public TitleLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public TitleLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public TitleLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.title_input_placeholder;
  }

}
