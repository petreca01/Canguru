package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;

/**
 * Created by taqtile on 4/3/17.
 */

public class PasswordNoValidationLabelEditTextCaption extends GenericLabelEditTextCaption {

  public PasswordNoValidationLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public PasswordNoValidationLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public PasswordNoValidationLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.password_input_placeholder;
  }

  @Override
  public int getFormType() {
    return PASSWORD;
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_generic_regex;
  }

}
