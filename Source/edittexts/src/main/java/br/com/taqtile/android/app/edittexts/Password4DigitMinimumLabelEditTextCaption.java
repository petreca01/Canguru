package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.PasswordLabelEditTextCaption;

/**
 * Created by taqtile on 27/06/17.
 */

public class Password4DigitMinimumLabelEditTextCaption extends PasswordLabelEditTextCaption {
  public Password4DigitMinimumLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public Password4DigitMinimumLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public Password4DigitMinimumLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_password_error_caption;
  }
}
