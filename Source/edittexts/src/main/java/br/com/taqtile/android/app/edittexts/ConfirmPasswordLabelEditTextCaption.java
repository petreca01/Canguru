package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.PasswordLabelEditTextCaption;

/**
 * Created by taqtile on 20/04/17.
 */

public class ConfirmPasswordLabelEditTextCaption extends PasswordLabelEditTextCaption {

  public ConfirmPasswordLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public ConfirmPasswordLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ConfirmPasswordLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public @StringRes
  int getInputPlaceholderTextRes() {
    return R.string.confirm_password_input_placeholder;
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_password_regex;
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_password_error_caption;
  }
}
