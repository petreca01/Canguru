package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by taqtile on 12/05/17.
 */

public class DaysLabelEditTextCaption extends GenericNumberLabelEditTextCaption {
  public DaysLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public DaysLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public DaysLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.days_input_placeholder;
  }
}
