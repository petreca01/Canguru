package br.com.taqtile.android.app.edittexts;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;

/**
 * Created by taqtile on 27/04/17.
 */

public class HourLabelEditTextCaption extends GenericLabelEditTextCaption {

  private int hour;
  private int minute;

  public HourLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  public HourLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public HourLabelEditTextCaption(Context context) {
    super(context);
    init();
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.hour_input_placeholder;
  }

  private void init() {
    GregorianCalendar calendar = new GregorianCalendar();
    hour = calendar.get(Calendar.HOUR_OF_DAY);
    minute = calendar.get(Calendar.MINUTE);

    this.getEditText().setFocusable(false);
    this.setFormType(TEXTAREA);
    this.getEditText().setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {

        SoftKeyboardHelper.hideKeyboard(getContext());

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
          @Override
          public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            hour = selectedHour;
            minute = selectedMinute;
            getEditText().setText(getFormattedValue(hour) + ":" + getFormattedValue(minute));
          }
        }, hour, minute, true);
        timePickerDialog.show();
      }
    });
  }

  private String getFormattedValue(int value) {
    String stringValue = String.valueOf(value);
    return stringValue.length() > 1 ? stringValue : String.format("0%s",stringValue);
  }

  public void setTime(int hour, int minute) {
    this.hour = hour;
    this.minute = minute;
    getEditText().setText(getFormattedValue(hour) + ":" + getFormattedValue(minute));
  }
}
