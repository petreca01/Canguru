package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.edittexts.components.NameLabelEditTextCaption;

/**
 * Created by taqtile on 7/5/17.
 */

public class CustomNameLabelEditTextCaption extends NameLabelEditTextCaption {

  public CustomNameLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public CustomNameLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CustomNameLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_generic_regex;
  }

  @Override
  public int getFormType() {
    return CustomLabelEditTextCaption.TEXTAREA;
  }

}
