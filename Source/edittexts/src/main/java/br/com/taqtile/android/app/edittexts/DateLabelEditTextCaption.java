package br.com.taqtile.android.app.edittexts;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import br.com.taqtile.android.edittexts.GenericLabelEditTextCaption;
import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;

/**
 * Created by taqtile on 27/04/17.
 */

public class DateLabelEditTextCaption extends GenericLabelEditTextCaption {

  private int selectedDay;
  private int selectedMonth;
  private int selectedYear;

  public DateLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  public DateLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public DateLabelEditTextCaption(Context context) {
    super(context);
    init();
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.date_input_placeholder;
  }

  private void init() {

    GregorianCalendar calendar = new GregorianCalendar();
    selectedDay = calendar.get(Calendar.DAY_OF_MONTH);
    selectedMonth = calendar.get(Calendar.MONTH) + 1;
    selectedYear = calendar.get(Calendar.YEAR);

    this.getEditText().setFocusable(false);
    this.setFormType(TEXTAREA);
    this.getEditText().setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {

        SoftKeyboardHelper.hideKeyboard(getContext());

        DatePickerDialog dialog = new DatePickerDialog(getContext(), getDateListener(), selectedYear, selectedMonth - 1, selectedDay);
        dialog.show();
      }
    });
  }

  private DatePickerDialog.OnDateSetListener getDateListener() {
    return new DatePickerDialog.OnDateSetListener() {
      @Override
      public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        selectedDay = dayOfMonth;
        selectedMonth = monthOfYear + 1;
        selectedYear = year;
        getEditText().setText(String.format("%s/%s/%s", getFormattedValue(selectedDay),
          getFormattedValue(selectedMonth), getFormattedValue(selectedYear)));
      }
    };
  }

  private String getFormattedValue(int value) {
    String stringValue = String.valueOf(value);
    return stringValue.length() > 1 ? stringValue : String.format("0%s",stringValue);
  }

  public void setDate(int day, int month, int year) {
    this.selectedDay = day;
    this.selectedMonth = month;
    this.selectedYear = year;
    getEditText().setText(String.format("%s/%s/%s", getFormattedValue(selectedDay), getFormattedValue(selectedMonth), getFormattedValue(selectedYear)));

  }

}
