package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;

/**
 * Created by taqtile on 20/07/17.
 */

public class LimitedTextLabelEditTextCaption extends CustomLabelEditTextCaption {

  public LimitedTextLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public LimitedTextLabelEditTextCaption(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public LimitedTextLabelEditTextCaption(Context context) {
    super(context);
  }

  @Override
  public int getInputPlaceholderTextRes() {
    return R.string.generic_form_input_placeholder;
  }

  @Override
  public int getValidationRegexRes() {
    return R.string.validation_generic_regex;
  }

  @Override
  public int getErrorCaptionTextRes() {
    return R.string.validation_generic_form_error_caption;
  }

  @Override
  public int getFormType() {
    return CustomLabelEditTextCaption.TEXTAREA;
  }

  @Override
  public int getClearBtnDrawableId() {
    return 0;
  }

  @Override
  public int getCheckDrawableId() {
    return 0;
  }

  @Override
  public int getErrorCaptionDrawableId() {
    return 0;
  }
}
