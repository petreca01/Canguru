package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;

/**
 * Created by taqtile on 3/13/17.
 */

public class ConfirmEmailLabelEditTextCaption extends EmailLabelEditTextCaption {

    public ConfirmEmailLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ConfirmEmailLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ConfirmEmailLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public @StringRes int getInputPlaceholderTextRes() {
        return R.string.confirm_email_input_placeholder;
    }
}
