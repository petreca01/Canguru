package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.List;

import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/19/17.
 */

public class CustomDropDown extends LinearLayout {

  public interface CustomDropDownListener {
    void onItemSelectedListener(int position);
  }

  Spinner spinner;
  private CustomDropDownListener listener;

  CustomTextView labelView;
  View separator;
  CustomTextView errorCaption;
  private String label;

  public CustomDropDown(Context context) {
    super(context);
    init(context);
  }

  public CustomDropDown(Context context, AttributeSet attrs) {
    super(context, attrs);
    getXmlAttrs(attrs, 0);
    init(context);
  }

  public CustomDropDown(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    getXmlAttrs(attrs, defStyleAttr);
    init(context);
  }

  public void getXmlAttrs(AttributeSet attrs, int defStyleAttr) {
    TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomDropDown, defStyleAttr, 0);

    label = typedArray.getString(R.styleable.CustomDropDown_label);

    typedArray.recycle();
  }

  public void init(Context context) {
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    View v = layoutInflater.inflate(R.layout.component_custom_drop_down, this, true);

    setupViews();

    if (label != null) {
      labelView.setText(label);
    }

    setupListeners();
  }

  private void setupListeners(){
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
          listener.onItemSelectedListener(position);
        }
        showDefaultState();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  public void setupViews(){
    spinner = (Spinner) findViewById(R.id.component_custom_dropdown_spinner);
    labelView = (CustomTextView) findViewById(R.id.component_custom_dropdown_label);
    separator = (View) findViewById(R.id.component_custom_dropdown_separator);
    errorCaption = (CustomTextView) findViewById(R.id.component_custom_dropdown_error_caption);
  }

  public void setValues(List<String> values) {
    ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(), R.layout.component_spinner_form_item, values);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(adapter);
  }

  public void setLabel(String label){
    this.labelView.setText(label);
  }

  public String getLabel() {
    return label;
  }

  public void setSelectedValue(int position){
    spinner.setSelection(position);
  }

  public String getSelectedValue(){
    return spinner.getSelectedItem().toString();
  }

  public int getSelectedValueIndex(){
    return spinner.getSelectedItemPosition();
  }

  public void setListener(CustomDropDownListener listener) {
    this.listener = listener;
  }

  public void showDefaultState() {
    separator.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));
    errorCaption.setVisibility(GONE);
  }

  public void showErrorState() {
    separator.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_error));
    errorCaption.setVisibility(VISIBLE);
  }
}
