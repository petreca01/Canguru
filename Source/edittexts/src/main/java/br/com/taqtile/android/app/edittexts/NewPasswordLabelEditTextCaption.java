package br.com.taqtile.android.app.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import br.com.taqtile.android.edittexts.components.PasswordLabelEditTextCaption;

/**
 * Created by taqtile on 11/15/16.
 */

public class NewPasswordLabelEditTextCaption extends PasswordLabelEditTextCaption {
    public NewPasswordLabelEditTextCaption(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NewPasswordLabelEditTextCaption(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewPasswordLabelEditTextCaption(Context context) {
        super(context);
    }

    @Override
    public int getInputPlaceholderTextRes() {
        return R.string.new_password_input_placeholder;
    }

    @Override
    public int getValidationRegexRes() {
      return R.string.validation_password_regex;
    }

    @Override
    public int getErrorCaptionTextRes() {
      return R.string.validation_password_error_caption;
    }

}
