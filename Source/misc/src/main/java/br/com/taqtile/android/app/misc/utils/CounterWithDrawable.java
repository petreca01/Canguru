package br.com.taqtile.android.app.misc.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 3/16/17.
 */

public class CounterWithDrawable extends LinearLayout {

  private ImageView icon;
  private CustomTextView text;
  private FrameLayout root;
  private boolean isLiked;

  private int counter;
  private CounterWithDrawableListener listener;

  public interface CounterWithDrawableListener {
    void onCounterClick(boolean isCounterActivated, CounterWithDrawable counterWithDrawable);
  }

  public CounterWithDrawable(Context context) {
    super(context);
    init();
  }

  public CounterWithDrawable(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CounterWithDrawable(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_counter_with_drawable, this, true);

    bindViews();
    resetCounter();
    setupButtons();
  }

  private void bindViews() {
    icon = (ImageView) findViewById(R.id.component_counter_with_drawable_icon);
    text = (CustomTextView) findViewById(R.id.component_counter_with_drawable_count);
    root = (FrameLayout) findViewById(R.id.component_counter_with_drawable_root);
  }

  private void setupButtons() {
    root.setOnClickListener(view -> {
      if (listener != null) {
        if (isLiked()) {
          decreaseCounter();
        } else {
          increaseCounter();
        }
        listener.onCounterClick(isLiked(), this);
        text.setText(convertCounterToString());
        setLiked(!this.isLiked());
      }
    });
  }

  public void changeLikeState(){
    this.isLiked = !this.isLiked;
    if (this.isLiked){
      icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.color_success), PorterDuff.Mode.SRC_IN);
      counter++;
    }else{
      icon.clearColorFilter();
      if (counter > 0) {
        counter--;
      }
    }
    text.setText(convertCounterToString());
  }

  private void paintLike(){
    icon.setColorFilter(ContextCompat.getColor(getContext(), R.color.color_success), PorterDuff.Mode.SRC_IN);
  }

  public void unpaintLike(){
    icon.clearColorFilter();
  }

  public void increaseCounter() {
    counter++;
  }

  public void decreaseCounter() {
    if (counter > 0) {
      counter--;
    }
  }

  public void resetCounter() {
    counter = 0;
    text.setText(convertCounterToString());
  }

  private String convertCounterToString() {
    return String.valueOf(counter);
  }

  public void setIcon(Drawable icon) {
    this.icon.setImageDrawable(icon);
  }

  public boolean isLiked() {
    return this.isLiked;
  }

  public void setLiked(boolean alreadyLiked) {
    this.isLiked = alreadyLiked;
    changeDrawable();
  }

  public void setCounter(int counter) {
    if (counter < 0) {
      this.counter = 0;
    } else{
      this.counter = counter;
    }
    this.text.setText(convertCounterToString());
  }

  public void changeDrawable() {
    if (isLiked){
      paintLike();
    } else {
      unpaintLike();
    }
  }

  public String getText() {
    return (String) text.getText();
  }

  public int getValueOfText() {
    return Integer.valueOf((String) text.getText());
  }

  public void setListener(CounterWithDrawableListener counterWithDrawableListener) {
    listener = counterWithDrawableListener;
  }

}
