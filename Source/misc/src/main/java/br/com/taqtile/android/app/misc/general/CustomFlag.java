package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 26/04/17.
 */

public class CustomFlag extends FrameLayout {

  public enum FlagType {
    Neutral,
    CallToAction,
    Alert
  }

  private CustomTextView flagText;
  private FrameLayout container;

  public CustomFlag(Context context) {
    super(context);
    init();
  }

  public CustomFlag(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CustomFlag(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_custom_flag, this, true);

    bindView();
  }

  private void bindView() {
    flagText = (CustomTextView) findViewById(R.id.component_custom_flag_text);
    container = (FrameLayout) findViewById(R.id.component_custom_flag_container);
  }

  public void setFlagText(String text) {
    flagText.setText(text);
  }

  public void setFlagType(FlagType type) {
    switch (type) {
      case CallToAction:
        setFlagBackgroundColor(R.color.color_call_to_action);
        setFlagTextColor(R.color.color_white);
        break;
      case Alert:
        setFlagBackgroundColor(R.color.color_alert);
        setFlagTextColor(R.color.color_white);
        break;
      case Neutral:
        setFlagBackgroundColor(R.color.color_gray);
        setFlagTextColor(R.color.color_black);
        break;
      default:
        setFlagBackgroundColor(R.color.color_gray);
        setFlagTextColor(R.color.color_black);
        break;
    }
  }

  private void setFlagBackgroundColor(int color) {
    Drawable background = container.getBackground();

    if (background instanceof ShapeDrawable) {
      ((ShapeDrawable)background).getPaint().setColor(ContextCompat.getColor(getContext(), color));
    } else if (background instanceof GradientDrawable) {
      ((GradientDrawable)background).setColor(ContextCompat.getColor(getContext(), color));
    } else if (background instanceof ColorDrawable) {
      ((ColorDrawable)background).setColor(ContextCompat.getColor(getContext(), color));
    }
  }

  private void setFlagTextColor(int color) {
    flagText.setTextColor(ContextCompat.getColor(getContext(), color));
  }
}
