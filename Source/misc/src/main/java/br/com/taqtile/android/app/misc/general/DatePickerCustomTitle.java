package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 20/06/17.
 */

public class DatePickerCustomTitle extends FrameLayout {

  public DatePickerCustomTitle(@NonNull Context context) {
    super(context);
    init();
  }

  public DatePickerCustomTitle(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public DatePickerCustomTitle(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_date_picker_custom_title, this, true);

  }

}
