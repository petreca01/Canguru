package br.com.taqtile.android.app.misc.utils.helpers;

import android.view.View;

import java.util.List;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.utils.validatable.Validatable;

/**
 * Created by taqtile on 11/16/16.
 */

public abstract class FormTextHelper {

    public static String[] getFormsInputPlaceholderText(List<Validatable> validatables){
        String[] forms = new String[validatables.size()];
        for (int i = 0; i < forms.length; i++){
            View validatable = validatables.get(i).getView();
            forms[i] = validatable.getResources().getString(((CustomLabelEditTextCaption) validatable).getInputPlaceholderTextRes());
        }
        return forms;
    }
}
