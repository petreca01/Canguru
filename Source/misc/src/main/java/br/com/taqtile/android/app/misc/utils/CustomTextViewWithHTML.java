package br.com.taqtile.android.app.misc.utils;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;

import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 02/05/17.
 */

public class CustomTextViewWithHTML extends CustomTextView {
  public CustomTextViewWithHTML(Context context) {
    super(context);
  }

  public CustomTextViewWithHTML(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CustomTextViewWithHTML(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  public void setTextWithHTML(String textWithHTML){
    if (textWithHTML == null) {
      return;
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      setText(Html.fromHtml(textWithHTML, Html.FROM_HTML_MODE_COMPACT));
    } else {
      //This method is not deprecated for APIs below 24, but the compiler shows erroneously a deprecated warning
      setText(Html.fromHtml(textWithHTML));
    }

    setMovementMethod(LinkMovementMethod.getInstance());
  }
}
