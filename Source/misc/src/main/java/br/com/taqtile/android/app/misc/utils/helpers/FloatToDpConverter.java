package br.com.taqtile.android.app.misc.utils.helpers;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by taqtile on 3/16/17.
 */

public class FloatToDpConverter {

    public static float convertFromDpToFloat(float valueInDp, Context context){
        float valueInFloat = TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, valueInDp,
                context.getResources().getDisplayMetrics() );
        return valueInFloat;
    }
}
