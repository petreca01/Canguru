package br.com.taqtile.android.app.misc.dialog;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.edittexts.components.EmailLabelEditTextCaption;

/**
 * Created by taqtile on 19/07/17.
 */

public class EmailFormInDialog extends FrameLayout {

  private EmailLabelEditTextCaption emailForm;

  public EmailFormInDialog(@NonNull Context context) {
    super(context);
    init();
  }


  public EmailFormInDialog(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public EmailFormInDialog(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_email_form_in_dialog, this, true);

    bindViews();
  }

  private void bindViews(){
    emailForm = (EmailLabelEditTextCaption) findViewById(R.id.component_email_form_in_dialog);
  }

  public String getFormText(){
    return emailForm.getText();
  }

  public void setText(String text){
    emailForm.setText(text);
  }

}
