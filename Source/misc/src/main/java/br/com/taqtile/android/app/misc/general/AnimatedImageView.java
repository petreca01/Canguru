package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by taqtile on 27/04/17.
 */

public class AnimatedImageView extends android.support.v7.widget.AppCompatImageView {

  private Listener listener;

  public AnimatedImageView(Context context) {
    super(context);
  }

  public AnimatedImageView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public AnimatedImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected void onAnimationEnd() {
    super.onAnimationEnd();
    if (listener != null){
      listener.onAnimationFinished(this);
    }

  }

  public interface Listener{
    void onAnimationFinished(AnimatedImageView animatedImageView);
  }

  public void setListener(Listener listener){ this.listener = listener; }

}
