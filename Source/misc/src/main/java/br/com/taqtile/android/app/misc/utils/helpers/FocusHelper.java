package br.com.taqtile.android.app.misc.utils.helpers;

import android.view.View;

import br.com.taqtile.android.utils.keyboard.SoftKeyboardHelper;

/**
 * Created by taqtile on 11/15/16.
 */

public abstract class FocusHelper {

    public static void clearViewFocus(View focusedView, View focuseableView){
        SoftKeyboardHelper.hideKeyboard(focusedView.getContext());
        if (focusedView.hasFocus()){
            focusedView.clearFocus();
        }
        focuseableView.requestFocus();
    }

    public static void clearViewFocus(View focusedView){
        SoftKeyboardHelper.hideKeyboard(focusedView.getContext());
        if (focusedView.hasFocus()){
            focusedView.clearFocus();
        }
    }

}
