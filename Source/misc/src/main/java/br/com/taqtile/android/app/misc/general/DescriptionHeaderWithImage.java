package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by Renato on 4/17/17.
 */

public class DescriptionHeaderWithImage extends LinearLayout {
  public ImageView image;
  public CustomTextView title;
  public CustomTextView description;
  public CustomFlag symptomRegistrationFlag;

  int imageResAttr;
  String titleResAttr;
  String descriptionResAttr;
  boolean isImageVisibleAttr;

  public DescriptionHeaderWithImage(Context context) {
    super(context);
    init();
  }

  public DescriptionHeaderWithImage(Context context, AttributeSet attrs) {
    super(context, attrs);

    setupAttrs(attrs);
    init();
  }

  public DescriptionHeaderWithImage(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_description_header_with_image, this, true);

    setupView();
  }

  private void setupAttrs(AttributeSet attrs) {
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.DescriptionHeader);
    imageResAttr = a.getResourceId(R.styleable.DescriptionHeader_image, 0);
    titleResAttr = a.getString(R.styleable.DescriptionHeader_title);
    descriptionResAttr = a.getString(R.styleable.DescriptionHeader_description);
    isImageVisibleAttr = a.getBoolean(R.styleable.DescriptionHeader_isImageVisible, true);

    a.recycle();

  }

  private void setupView() {
    bindViews();
  }

  private void bindViews(){
    image = (ImageView) findViewById(R.id.component_description_header_with_image_image);
    if (imageResAttr != 0) {
      image.setImageResource(imageResAttr);
    }

    if (!isImageVisibleAttr){
      hideImage();
    }

    title = (CustomTextView) findViewById(R.id.component_description_header_with_image_title);
    if(titleResAttr != null){
      title.setText(titleResAttr);
    }

    description = (CustomTextView) findViewById(R.id.component_description_header_with_image_description);
    if(descriptionResAttr != null){
      setDescription(descriptionResAttr);
    }
    symptomRegistrationFlag = (CustomFlag) findViewById(R.id.component_description_header_with_image_custom_flag);
  }

  public void hideImage(){
    image.setVisibility(GONE);
  }

  public void setTitle(String title){
    this.title.setText(title);
  }

  public void setDescription(String description){
    if (description != null && !description.isEmpty()) {
      this.description.setVisibility(VISIBLE);
    }
    this.description.setText(description);
  }

  public void setFlagTypeAndText(CustomFlag.FlagType flagType, String flagText){
    if (flagText != null && !flagText.isEmpty()) {
      symptomRegistrationFlag.setVisibility(VISIBLE);
    }
    symptomRegistrationFlag.setFlagType(flagType);
    symptomRegistrationFlag.setFlagText(flagText);
  }

  public void setImage(int resourceId) {
    image.setImageResource(resourceId);
  }
}
