package br.com.taqtile.android.app.misc.dialog;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import br.com.taqtile.android.app.misc.R;

public class TextViewInDialog extends FrameLayout {

  private TextView textForm;

  public TextViewInDialog(@NonNull Context context) {
    super(context);
    init();
  }

  public TextViewInDialog(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public TextViewInDialog(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_text_form_in_dialog, this, true);

    bindViews();
  }

  private void bindViews(){
    textForm = (TextView) findViewById(R.id.component_text_form_in_dialog);
  }

  public void setText(String text){
    textForm.setText(text);
  }

}
