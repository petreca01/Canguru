package br.com.taqtile.android.app.misc.toolbar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.view.MenuItem;

import br.com.taqtile.android.utils.drawable.DrawableHelper;

/**
 * Created by indigo on 8/11/16.
 */
public class OptionsMenuHelper {

    public static void setMenuIcon(MenuItem menuItem, @ColorRes int tintColorResId,
                                   Context context) {
        Drawable sourceDrawable = menuItem.getIcon();
        if (sourceDrawable != null) {
            Drawable tintedDrawable = DrawableHelper.tintDrawable(sourceDrawable, context,
                    tintColorResId);
            menuItem.setIcon(tintedDrawable);
        }
    }
}
