package br.com.taqtile.android.app.misc.utils.helpers;

/**
 * Created by taqtile on 30/05/17.
 */

public class IntegerToStringFormatter {

  public static String integerToString(Integer integer){
    return String.valueOf(integer);
  }
}
