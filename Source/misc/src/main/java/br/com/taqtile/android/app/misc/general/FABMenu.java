package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.app.misc.utils.helpers.FadeEffectHelper;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 27/04/17.
 */

// TODO: 28/04/17 Refactor this component to use observables
public class FABMenu extends LinearLayout implements AnimatedRelativeLayout.Listener {

  private final static int TRANSLATION_ANIMATION_DURATION = 250;

  private int Y_AXIS_DEFAULT_TRANSLATION = (int) getResources().getDimension(R.dimen.fab_menu_y_axis_default_translation);
  private int Y_AXIS_SCHEDULE_TRANSLATION = (int) getResources().getDimension(R.dimen.fab_menu_y_axis_schedule_translation);
  private int X_AXIS_POST_TRANSLATION = (int) getResources().getDimension(R.dimen.fab_menu_x_axis_post_translation);
  private int X_AXIS_SCHEDULE_TRANSLATION = (int) getResources().getDimension(R.dimen.fab_menu_x_axis_schedule_translation);

  private FrameLayout root;
  private FloatingActionButton floatingActionButton;

  private Listener listener;

  private AnimatedRelativeLayout createPostButton;
  private CustomTextView createPostButtonText;
  private AnimatedImageView createPostButtonIcon;
  private TranslateAnimation createPostShowAnimation;
  private TranslateAnimation createPostHideAnimation;
  private boolean createPostAnimationFinished;

  private AnimatedRelativeLayout addSymptomButton;
  private CustomTextView addSymptomButtonText;
  private AnimatedImageView addSymptomButtonIcon;
  private TranslateAnimation addSymptomShowAnimation;
  private TranslateAnimation addSymptomHideAnimation;
  private boolean addSymptomAnimationFinished;

  private AnimatedRelativeLayout scheduleAppointmentButton;
  private CustomTextView scheduleAppointmentButtonText;
  private AnimatedImageView scheduleAppointmentButtonIcon;
  private TranslateAnimation scheduleAppointmentShowAnimation;
  private TranslateAnimation scheduleAppointmentHideAnimation;
  private boolean scheduleAppointmentAnimationFinished;

  private boolean isShowingButtons;


  public FABMenu(Context context) {
    super(context);
    init();
  }

  public FABMenu(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public FABMenu(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_fab_menu, this, true);

    bindViews();
    setupView();
  }

  private void setupView(){
    setupAnimations();
    setupButtons();
    isShowingButtons = false;
    addSymptomAnimationFinished = false;
    createPostAnimationFinished = false;
    scheduleAppointmentAnimationFinished = false;
  }

  private void bindViews() {
    root = (FrameLayout) findViewById(R.id.component_fab_menu_root);
    createPostButtonIcon = (AnimatedImageView) findViewById(R.id.component_fab_menu_create_button_icon);
    scheduleAppointmentButtonIcon = (AnimatedImageView) findViewById(R.id.component_fab_menu_schedule_button_icon);
    addSymptomButtonIcon = (AnimatedImageView) findViewById(R.id.component_fab_menu_add_symptom_button_icon);
    floatingActionButton = (FloatingActionButton) findViewById(R.id.component_fab_menu_fab);
    createPostButton = (AnimatedRelativeLayout) findViewById(R.id.component_fab_menu_create_button);
    addSymptomButton = (AnimatedRelativeLayout) findViewById(R.id.component_fab_menu_add_symptom_button);
    scheduleAppointmentButton = (AnimatedRelativeLayout) findViewById(R.id.component_fab_menu_schedule_button);
    createPostButtonText = (CustomTextView) findViewById(R.id.component_fab_create_button_text);
    addSymptomButtonText= (CustomTextView) findViewById(R.id.component_fab_menu_add_symptom_button_icon_text);
    scheduleAppointmentButtonText = (CustomTextView) findViewById(R.id.component_fab_schedule_button_text);
  }

  private void setupAnimations() {
    setupShowAnimation();
    setupHideAnimation();
    setupListeners();
  }

  private void setupShowAnimation(){
    createPostShowAnimation = animationFactory(0, -X_AXIS_POST_TRANSLATION, TRANSLATION_ANIMATION_DURATION);
    addSymptomShowAnimation = animationFactory(-2*Y_AXIS_DEFAULT_TRANSLATION, 0, TRANSLATION_ANIMATION_DURATION);
    scheduleAppointmentShowAnimation = animationFactory(-Y_AXIS_SCHEDULE_TRANSLATION, -X_AXIS_SCHEDULE_TRANSLATION,  TRANSLATION_ANIMATION_DURATION);
  }

  private void setupHideAnimation(){
    createPostHideAnimation = animationFactory(0, X_AXIS_POST_TRANSLATION, TRANSLATION_ANIMATION_DURATION);
    addSymptomHideAnimation = animationFactory(2*Y_AXIS_DEFAULT_TRANSLATION, 0, TRANSLATION_ANIMATION_DURATION);
    scheduleAppointmentHideAnimation = animationFactory(Y_AXIS_SCHEDULE_TRANSLATION, X_AXIS_SCHEDULE_TRANSLATION, TRANSLATION_ANIMATION_DURATION);
  }

  private void setupListeners(){
    createPostButton.setListener(this);
    addSymptomButton.setListener(this);
    scheduleAppointmentButton.setListener(this);
  }

  private TranslateAnimation animationFactory(int yTranslation, int xTranslation, int duration) {
    TranslateAnimation translateAnimation = new TranslateAnimation(0, xTranslation, 0, yTranslation);
    translateAnimation.setDuration(duration);
    return translateAnimation;
  }

  private void setupButtons() {
    floatingActionButton.setOnClickListener(v -> translateButton());
    createPostButtonIcon.setOnClickListener(v -> {
      if (listener != null) {
        listener.onCreatePostButtonClick();
      }
    });
    addSymptomButtonIcon.setOnClickListener(v -> {
      if (listener != null) {
        listener.onAddSymptomButtonClick();
      }
    });
    scheduleAppointmentButtonIcon.setOnClickListener(v -> {
      if (listener != null) {
        listener.onScheduleAppointmentClick();
      }
    });

  }

  public void translateButton() {
    floatingActionButton.setEnabled(false);
    if (isShowingButtons) {
      hideButtonsText();
      startHideTranslationAnimation();
    } else {
      startShowTranslationAnimation();
    }
  }

  private void startShowTranslationAnimation(){
    createPostButton.startAnimation(createPostShowAnimation);
    addSymptomButton.startAnimation(addSymptomShowAnimation);
    scheduleAppointmentButton.startAnimation(scheduleAppointmentShowAnimation);
  }

  private void startHideTranslationAnimation(){
    createPostButton.startAnimation(createPostHideAnimation);
    addSymptomButton.startAnimation(addSymptomHideAnimation);
    scheduleAppointmentButton.startAnimation(scheduleAppointmentHideAnimation);
  }

  @Override
  public void onAnimationFinished(AnimatedRelativeLayout animatedRelativeLayout) {
    if (!isShowingButtons) {
      if (animatedRelativeLayout == createPostButton) {
        setNewHorizontalPosition(-getPostRightMargin() + X_AXIS_POST_TRANSLATION, createPostButton);
        createPostAnimationFinished = true;
      }
      if (animatedRelativeLayout == addSymptomButton) {
        setNewVerticalPosition(getLargeMargin() + (isShowingButtons ? 0 : 2*Y_AXIS_DEFAULT_TRANSLATION), addSymptomButton);
        addSymptomAnimationFinished = true;
      }

      if (animatedRelativeLayout == scheduleAppointmentButton) {
        setNewVerticalPosition(getLargeMargin() + (isShowingButtons ? 0 : Y_AXIS_SCHEDULE_TRANSLATION), scheduleAppointmentButton);
        setNewHorizontalPosition(-getMediumMargin() + X_AXIS_SCHEDULE_TRANSLATION, scheduleAppointmentButton);
        scheduleAppointmentAnimationFinished = true;
      }
      if (createPostAnimationFinished && scheduleAppointmentAnimationFinished && addSymptomAnimationFinished) {
        isShowingButtons = true;
        resetAnimationFinishedFlags();
        showButtonsText();
        floatingActionButton.setEnabled(true);
      }
    } else {
      if (animatedRelativeLayout == createPostButton) {
        setNewVerticalPosition((int) getResources().getDimension(R.dimen.margin_small), createPostButton);
        setNewHorizontalPosition(0, createPostButton);
        createPostAnimationFinished = true;
      }

      if (animatedRelativeLayout == addSymptomButton) {
        setNewVerticalPosition(getLargeMargin(), addSymptomButton);
        setNewHorizontalPosition(0, addSymptomButton);
        addSymptomAnimationFinished = true;
      }

      if (animatedRelativeLayout == scheduleAppointmentButton) {
        setNewVerticalPosition(getLargeMargin(), scheduleAppointmentButton);
        setNewHorizontalPosition(0, scheduleAppointmentButton);
        scheduleAppointmentAnimationFinished = true;
      }
    }
    if (createPostAnimationFinished && scheduleAppointmentAnimationFinished && addSymptomAnimationFinished) {
      isShowingButtons = false;
      resetAnimationFinishedFlags();
      floatingActionButton.setEnabled(true);
    }
  }

  private int getLargeMargin() {
    return (int) getResources().getDimension(R.dimen.margin_large);
  }

  private int getMediumMargin(){ return (int) getResources().getDimension(R.dimen.margin_medium); }

  private int getPostRightMargin(){ return (int) getResources().getDimension(R.dimen.fab_menu_x_axis_post_right_margin); }

  private void resetAnimationFinishedFlags() {
    createPostAnimationFinished = false;
    scheduleAppointmentAnimationFinished = false;
    addSymptomAnimationFinished = false;
  }

  private void setNewVerticalPosition(int newPosition, AnimatedRelativeLayout button) {
    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) button.getLayoutParams();
    layoutParams.bottomMargin = newPosition;
    button.setLayoutParams(layoutParams);
  }

  private void setNewHorizontalPosition(int newPosition, AnimatedRelativeLayout button){
    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) button.getLayoutParams();
    layoutParams.rightMargin = newPosition;
    button.setLayoutParams(layoutParams);
  }

  private void showButtonsText(){
    FadeEffectHelper.fadeInView(createPostButtonText);
    FadeEffectHelper.fadeInView(addSymptomButtonText);
    FadeEffectHelper.fadeInView(scheduleAppointmentButtonText);
  }

  private void hideButtonsText(){
    createPostButtonText.setVisibility(INVISIBLE);
    addSymptomButtonText.setVisibility(INVISIBLE);
    scheduleAppointmentButtonText.setVisibility(INVISIBLE);
  }

  public interface Listener {
    void onCreatePostButtonClick();

    void onAddSymptomButtonClick();

    void onScheduleAppointmentClick();
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }
}
