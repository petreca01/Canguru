package br.com.taqtile.android.app.misc.utils.helpers;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 19/04/17.
 */

public class SlideEffectAnimator {

  private final static int SLIDE_ANIMATION_DURATION = 500;

  static public void slideView(View view) {
    if (view != null && view.getVisibility() != View.VISIBLE) {
      setupAnimation(view, R.anim.slide_up, View.VISIBLE);
    } else {
      setupAnimation(view, R.anim.slide_down, View.GONE);
    }
  }

  private static void setupAnimation(View view, int animationFileId, int finalVisibility) {
    Animation slide_down = AnimationUtils.loadAnimation(view.getContext(), animationFileId);
    slide_down.setDuration(SLIDE_ANIMATION_DURATION);

    view.setVisibility(finalVisibility);
    view.startAnimation(slide_down);
  }
}
