package br.com.taqtile.android.app.misc.utils;

/**
 * Created by taqtile on 1/22/16.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 1/22/16.
 *
 * This ImageView accepts a ratio (width : height) and it sets
 * its height using this ratio and its width (height = width / ratio)
 */
public class RatioImageView extends ImageView {
    private float mRatio;

    //region Setter
    public void setRatio(float mRatio) {
        this.mRatio = mRatio;
    }
    //endregion

    //region Constructor
    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getXmlAttrs(context, attrs, 0);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getXmlAttrs(context, attrs, defStyleAttr);
    }
    //endregion

    //region Setup methods
    private void getXmlAttrs(Context context, AttributeSet attrs, int defStyleAttr) {
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RatioImageView, defStyleAttr, 0);
        mRatio = a.getFloat(R.styleable.RatioImageView_ratio, 1);
        a.recycle();
    }
    //endregion

    @Override
    protected void onMeasure(int widthMeasureSpec,int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int)((float)getMeasuredWidth() / mRatio)); //Snap to width
    }
}
