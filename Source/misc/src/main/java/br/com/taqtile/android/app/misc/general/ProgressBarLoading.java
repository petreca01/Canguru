package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 07/06/17.
 */

public class ProgressBarLoading extends FrameLayout {

  private ProgressBar progressBar;

  public ProgressBarLoading(@NonNull Context context) {
    super(context);
    init();
  }

  public ProgressBarLoading(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ProgressBarLoading(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init(){
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_progress_bar_loading, this, true);

    bindViews();
    setupProgressBar();
  }

  private void bindViews(){
    progressBar = (ProgressBar) findViewById(R.id.component_progress_bar_loading);
  }

  private void setupProgressBar(){
    progressBar.getIndeterminateDrawable().setColorFilter(
      ContextCompat.getColor(getContext(), R.color.color_primary),
      android.graphics.PorterDuff.Mode.SRC_IN);
  }

}
