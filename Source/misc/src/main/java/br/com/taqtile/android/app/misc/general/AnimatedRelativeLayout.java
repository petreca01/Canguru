package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by taqtile on 28/04/17.
 */

public class AnimatedRelativeLayout extends RelativeLayout {

  private Listener listener;

  public AnimatedRelativeLayout(Context context) {
    super(context);
  }

  public AnimatedRelativeLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public AnimatedRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected void onAnimationEnd() {
    super.onAnimationEnd();
    if (listener != null){
      listener.onAnimationFinished(this);
    }

  }

  public interface Listener{
    void onAnimationFinished(AnimatedRelativeLayout animatedRelativeLayout);
  }

  public void setListener(Listener listener){ this.listener = listener; }
}
