package br.com.taqtile.android.app.misc.toolbar;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.MenuRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by indigo on 8/10/16.
 */
public class CustomToolbar {

  private final Toolbar toolbar;
  private final Context context;

  public CustomToolbar(View rootView) {
    toolbar = (Toolbar) rootView;
    context = rootView.getContext();
  }

  public void setBackgroundDrawable(@DrawableRes int drawableRes) {
    toolbar.setBackground(ContextCompat.getDrawable(context, drawableRes));
  }

  // region Toolbar visibility

  public void show() {
    toolbar.setVisibility(View.VISIBLE);
  }

  public void hide() {
    toolbar.setVisibility(View.GONE);
  }

  // endregion

  // region Logo
  public void showLogo() {
    hideTitle();
    toolbar.findViewById(R.id.toolbar_logo).setVisibility(View.VISIBLE);
  }

  public void hideLogo() {
    toolbar.findViewById(R.id.toolbar_logo).setVisibility(View.GONE);
  }

  // endregion

  // region Title

  public void setTitle(String text) {
    setTitle(text, Gravity.LEFT);
  }

  public void setTitle(String text, int layoutGravity) {
    CustomTextView textView = (CustomTextView)
      toolbar.findViewById(R.id.toolbar_title_text_view);

    hideLogo();

    textView.setText(text);
    textView.setVisibility(View.VISIBLE);
    toolbar.setTitle("");

    Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) textView.getLayoutParams();
    layoutParams.gravity = layoutGravity;

    textView.setLayoutParams(layoutParams);
  }

  public void hideTitle() {
    CustomTextView textView = (CustomTextView)
      toolbar.findViewById(R.id.toolbar_title_text_view);
    textView.setText("");
    textView.setVisibility(View.GONE);
    toolbar.setTitle("");
  }

  // endregion

  // region Left button

  public void setLeftButton(@DrawableRes int drawableResId, View.OnClickListener listener) {
    toolbar.setNavigationIcon(drawableResId);
    toolbar.setNavigationOnClickListener(listener);
  }

  public void hideLeftButton() {
    toolbar.setNavigationIcon(null);
    toolbar.setNavigationOnClickListener(null);
  }

  // endregion

  public void setToolbarTransparent() {
    toolbar.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
  }

  public void setToolbarColor(@ColorInt int color){
    toolbar.setBackgroundColor(color);
  }

  // region Right buttons

  public void inflateOptionsMenu(@MenuRes int menuResId,
                                 MenuItem.OnMenuItemClickListener itemClickListener) {
    Menu menu = toolbar.getMenu();

    if (menu != null) {
      menu.clear();
      toolbar.inflateMenu(menuResId);
      for (int i = 0; i < menu.size(); i++) {
        MenuItem menuItem = menu.getItem(i);
        menuItem.setOnMenuItemClickListener(itemClickListener);
        OptionsMenuHelper.setMenuIcon(menuItem, R.color.color_white, context);
      }

      toolbar.requestLayout();
    }

  }

  public void hideOptionsMenu() {
    Menu menu = toolbar.getMenu();
    menu.clear();
    toolbar.requestLayout();
  }

  public MenuItem getMenuItem(int position) {
    return toolbar.getMenu().getItem(position);
  }

  // endregion

}
