package br.com.taqtile.android.app.misc.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.buttons.TemplateButton;
import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 1/22/16.
 * This class should be used with BaseProductListAdapter
 */
public class AsyncRequestErrorPlaceholder extends LinearLayout {
    public static final String ASYNC_REQUEST_ERROR_PLACEHOLDER = AsyncRequestErrorPlaceholder.class.getName();

    TemplateButton mButton;

    public AsyncRequestErrorPlaceholder(Context context) {
        super(context);
        initialSetup(context);
    }

    public AsyncRequestErrorPlaceholder(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialSetup(context);
    }

    public AsyncRequestErrorPlaceholder(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialSetup(context);
    }

    private void bindViews(){
        mButton = (TemplateButton) findViewById(R.id.component_async_request_error_placeholder_button);
    }

    private void initialSetup(Context context) {
        this.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_gray_light));
        this.setOrientation(VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_async_request_error_placeholder, this, true);

        bindViews();
    }

    public void setListener(OnClickListener listener){
        mButton.setOnClickListener(listener);
    }
}
