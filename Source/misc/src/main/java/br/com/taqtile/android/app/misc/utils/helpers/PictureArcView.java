package br.com.taqtile.android.app.misc.utils.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

/**
 * Created by taqtile on 20/04/17.
 */

public class PictureArcView extends View {
  private Drawable drawable;
  private int width = 0;
  private int height = 0;

  private Paint paint;

  public PictureArcView(Context context) {
    super(context);
  }

  public PictureArcView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public PictureArcView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  protected void onDraw(Canvas canvas) {
    width = getWidth();
    height = getHeight();

    if (width == 0 || height == 0) {
      return;
    }

    setupPaint();

    if (drawable == null) {

      paint.setColor(Color.rgb(146, 105, 191));
      canvas.drawArc(getOvalRect(), 0.0F, 180.0F, true, paint);

    } else {

      Bitmap b = ((BitmapDrawable) drawable).getBitmap();
      Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

      Bitmap croppedBitmap = getCroppedBitmap(bitmap);
      canvas.drawBitmap(croppedBitmap, 0, 0, null);

    }
  }

  private Bitmap getCroppedBitmap(Bitmap bitmap) {

    if (isHeightWidthPictureRatioBiggerThanPlaceholder(bitmap)) {
      float ratio = (float) bitmap.getWidth() / width;
      bitmap = Bitmap.createScaledBitmap(bitmap, width, (int) (bitmap.getHeight() / ratio), false);
    } else {
      float ratio = (float) bitmap.getHeight() / height;
      bitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() / ratio), height, false);
    }

    int x = 0;
    int y = 0;

    if (bitmap.getHeight() > height) {
      y = (bitmap.getHeight() - height) / 2;
    } else if (bitmap.getWidth() > width) {
      x = (bitmap.getWidth() - width) / 2;
    }


    Bitmap finalBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);

    Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    // TODO review
    Paint paint2 = new Paint();
    paint2.setAntiAlias(true);
    canvas.drawArc(getOvalRect(), 0.0F, 180.0F, true, paint2);

    Paint paint1 = new Paint();
    paint1.setAntiAlias(true);
    paint1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    paint1.setAlpha(145);
    final Rect rect = new Rect(0, 0, width, height);
    canvas.drawBitmap(finalBitmap, rect, rect, paint1);

//    Paint paint3 = new Paint();
//    paint3.setAntiAlias(true);
//    paint3.setColor(Color.rgb(127, 96, 166));
//    paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
//    canvas.drawPaint(paint3);

    return output;
  }

  private boolean isHeightWidthPictureRatioBiggerThanPlaceholder(Bitmap bitmap) {
    return calculatePictureRation(bitmap.getHeight(), bitmap.getWidth()) >= calculatePlaceholderRation();
  }

  private float calculatePlaceholderRation() {
    Float height = (float) this.height;
    Float width = (float) this.width;
    return height / width;
  }

  private float calculatePictureRation(int originalHeight, int originalWidth) {
    Float height = (float) originalHeight;
    Float width = (float) originalWidth;
    return height / width;
  }

  public void setImageUrl(String imageUrl) {
    ImageView imageView = new ImageView(getContext());
    Target target = new Target() {
      @Override
      public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        imageView.setImageBitmap(bitmap);
        reload(imageView);
      }

      @Override
      public void onBitmapFailed(Drawable errorDrawable) {
      }

      @Override
      public void onPrepareLoad(Drawable placeHolderDrawable) {
      }
    };
    imageView.setTag(target);
    Picasso.with(getContext()).load(imageUrl).into(target);
  }

  public void setImagePath(String imagePath) {
    ImageView imageView = new ImageView(getContext());
    Picasso.with(getContext()).load(new File(imagePath)).into(imageView);
    reload(imageView);
  }

  public void reload(ImageView imageView) {
    int delay = 500;
    new Handler().postDelayed(() -> {
      drawable = imageView.getDrawable();
      this.invalidate();
    }, delay);
  }

  private void setupPaint() {
    paint = new Paint();
    paint.setAntiAlias(true);
  }

  private RectF getOvalRect() {
    int radius = 2000;
    RectF ovalRect = new RectF();

    float centerX = (float) (width / 2);
    float ovalRectLeft = -((float) radius - centerX);
    float ovalRectRight = (float) width + ((float) radius - centerX);

    float ovalRectTop = (float) (-(2 * radius) + height);
    ovalRect.set(ovalRectLeft, ovalRectTop, ovalRectRight, (float) height);

    return ovalRect;
  }
}
