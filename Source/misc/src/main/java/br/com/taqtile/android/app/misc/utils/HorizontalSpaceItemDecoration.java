package br.com.taqtile.android.app.misc.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by taqtile on 1/22/16.
 */
public class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int mHorizontalSpaceHeight;

    public HorizontalSpaceItemDecoration(int mHorizontalSpaceHeight) {
        this.mHorizontalSpaceHeight = mHorizontalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        final boolean isFirstItem = parent.getChildAdapterPosition(view) == 0;
        final boolean isLastItem = parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1;
        if (isFirstItem) {
            outRect.left = mHorizontalSpaceHeight * 2;
        } else if (isLastItem) {
            outRect.left = mHorizontalSpaceHeight;
            outRect.right = mHorizontalSpaceHeight * 2;
        } else {
            outRect.left = mHorizontalSpaceHeight;
        }
    }
}
