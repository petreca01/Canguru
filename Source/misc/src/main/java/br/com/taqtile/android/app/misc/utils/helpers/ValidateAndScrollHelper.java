package br.com.taqtile.android.app.misc.utils.helpers;

import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import br.com.taqtile.android.edittexts.CustomLabelEditTextCaption;
import br.com.taqtile.android.utils.validatable.Validatable;

/**
 * Created by taqtile on 11/15/16.
 */

public abstract class ValidateAndScrollHelper {

  public static List<Validatable> validateForms(Validatable[] forms) {

    List<Validatable> errorForms = new ArrayList<>();
    //Iterate through all forms passed to the method to find out which ones are not valid
    for (Validatable form : forms) {
      if (formInvalid(form) || formRequired(form)) {
        errorForms.add(form);
      }
    }

    return errorForms;
  }

  private static boolean formInvalid(Validatable form) {
    return !form.validate() && !formEmpty(form);
  }

  private static boolean formRequired(Validatable form) {
    if (!(form instanceof CustomLabelEditTextCaption)) {
      return true;
    }
    return ((CustomLabelEditTextCaption) form).getRequired() && formEmpty(form);
  }
  private static boolean formEmpty(Validatable form) {
    if (!(form instanceof CustomLabelEditTextCaption)) {
      return true;
    }
    return ((CustomLabelEditTextCaption) form).getText().isEmpty();
  }

  //Change ScrollView parameter to Class<? extends ScrollView>
  public static void scrollToView(final View viewToScroll, final ScrollView scrollView) {
    scrollView.post(() -> scrollView.smoothScrollTo(0, viewToScroll.getTop()));
  }

  public static void scrollToView(final View viewToScroll, final NestedScrollView scrollView) {
    scrollView.post(() -> scrollView.smoothScrollTo(0, viewToScroll.getTop()));
  }

}
