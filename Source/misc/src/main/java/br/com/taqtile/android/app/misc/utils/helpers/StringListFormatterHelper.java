package br.com.taqtile.android.app.misc.utils.helpers;

/**
 * Created by taqtile on 11/15/16.
 */

public abstract class StringListFormatterHelper {

    public static String buildStringList(String[] unformattedStringList){
        String formattedStringList = "";
        for (int i = 0; i < unformattedStringList.length; i++){

            formattedStringList += unformattedStringList[i];

            if(i != unformattedStringList.length - 1){
                formattedStringList += ", ";
            }
        }
        return formattedStringList;
    }

    public static String buildStringList(String[] unformattedStringList, String unwantedString){
        String formattedStringList = "";
        for (int i = 0; i < unformattedStringList.length; i++){

            formattedStringList += removeCharacterFromString(unformattedStringList[i], unwantedString);

            if(i != unformattedStringList.length - 1){
                formattedStringList += ", ";
            }
        }
        return  formattedStringList;
    }



    public static String removeCharacterFromString(String str, String specialCharacter){
        str = str.replace(specialCharacter, "");
        return str;
    }

}
