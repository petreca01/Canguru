package br.com.taqtile.android.app.misc.utils.helpers;

import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/**
 * Created by indigo on 1/27/16.
 */
public class FadeEffectHelper {

    private final static int FADE_ANIMATION_DURATION = 500;

    static public void fadeInView(View view) {
        fadeInView(view,null);
    }

    static public void fadeInView(final View view, final Animation.AnimationListener listener) {
        fadeInView(view, listener, FADE_ANIMATION_DURATION);
    }

    static public void fadeOutView(final View view) {
        fadeOutView(view, null);
    }

    static public void fadeOutView(final View view, final Animation.AnimationListener listener) {
        fadeOutView(view, listener, FADE_ANIMATION_DURATION);
    }

    static public void fadeInView(final View view, final Animation.AnimationListener listener, final long duration) {
        if (view != null && view.getVisibility() != View.VISIBLE) {
            AlphaAnimation fadeIn = new AlphaAnimation(0, 1f);
            fadeIn.setDuration(duration);
            fadeIn.setInterpolator(new LinearOutSlowInInterpolator());

            fadeIn.setAnimationListener(listener);

            view.setVisibility(View.VISIBLE);
            view.startAnimation(fadeIn);
        }
    }

    static public void fadeOutView(final View view, final Animation.AnimationListener listener, final long duration) {
        if (view != null && view.getVisibility() == View.VISIBLE) {
            AlphaAnimation fadeOut = new AlphaAnimation(1f, 0);
            fadeOut.setDuration(duration);
            fadeOut.setInterpolator(new FastOutLinearInInterpolator());

            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    if (listener != null) {
                        listener.onAnimationStart(animation);
                    }
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);

                    if (listener != null) {
                        listener.onAnimationEnd(animation);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    if (listener != null) {
                        listener.onAnimationRepeat(animation);
                    }
                }
            });

            view.startAnimation(fadeOut);
        }
    }
}
