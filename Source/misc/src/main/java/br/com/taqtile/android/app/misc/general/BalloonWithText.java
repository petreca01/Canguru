package br.com.taqtile.android.app.misc.general;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 03/07/17.
 */

public class BalloonWithText extends FrameLayout {

  private CustomTextView title;
  private String titleAttr;
  private CustomTextView description;
  private String descriptionAttr;
  private RelativeLayout balloonBackground;
  private Listener listener;
  private ImageView closeBtn;

  public BalloonWithText(@NonNull Context context) {
    super(context);
    init();
  }

  public BalloonWithText(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupAttrs(attrs);
    init();
  }

  public BalloonWithText(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setupAttrs(attrs);
    init();
  }

  private void init() {
    LayoutInflater factory = LayoutInflater.from(getContext());
    final View view = factory.inflate(R.layout.component_balloon_with_text, this, true);

    bindView();

    setupView();

    if (titleAttr != null){
      title.setText(titleAttr);
    }
    if (descriptionAttr != null){
      description.setText(descriptionAttr);
    }
  }

  private void bindView(){
    title = (CustomTextView) findViewById(R.id.component_balloon_with_text_title);
    description = (CustomTextView) findViewById(R.id.component_balloon_with_text_body);
    balloonBackground = (RelativeLayout) findViewById(R.id.component_balloon_with_text_balloon_bkg);
    closeBtn = (ImageView) findViewById(R.id.component_balloon_with_text_close_button);
  }

  private void setupView(){
    paintBackground();
    paintCloseBtn();
    setupListener();
  }

  private void paintBackground(){
    balloonBackground.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.color_balloon_background), PorterDuff.Mode.SRC_ATOP);
  }

  private void paintCloseBtn(){
    closeBtn.setColorFilter(ContextCompat.getColor(getContext(), R.color.color_white), PorterDuff.Mode.SRC_ATOP);
  }

  private void setupListener(){
    setOnClickListener(v -> {
      if (listener != null){
        listener.onBalloonClick();
      }
    });
  }

  private void setupAttrs(AttributeSet attributeSet){
    TypedArray a = getContext().obtainStyledAttributes(attributeSet, R.styleable.BalloonWithText);

    titleAttr = a.getString(R.styleable.BalloonWithText_balloonTitle);
    descriptionAttr = a.getString(R.styleable.BalloonWithText_balloonDescription);

    a.recycle();
  }

  public void setTitle(String title){
    this.title.setText(title);
  }

  public void setDescription(String description){
    this.description.setText(description);
  }

  public interface Listener{
    void onBalloonClick();
  }

  public void setListener(Listener listener){ this.listener = listener; }

}
