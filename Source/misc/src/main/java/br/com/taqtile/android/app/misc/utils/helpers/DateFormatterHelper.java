package br.com.taqtile.android.app.misc.utils.helpers;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import br.com.taqtile.android.app.misc.R;

/**
 * Created by taqtile on 3/22/17.
 */

public class DateFormatterHelper {

  private final static String MIDNIGHT = "00";
  private final static String HOUR_ONE = "01";
  private final static String dd = "dd";
  private final static String MM = "MM";
  private final static String yyyy = "yyyy";
  private final static String HH = "HH";
  private final static String mm = "mm";
  private final static String HHmm = "HH:mm";
  private final static String dateFormat = "yyyy-MM-dd HH:mm:ss";
  private final static String dateFormat2 = "yyyy-MM-dd";
  private final static String dateFormat3 = "dd/MM/yyyy HH:mm";
  private final static String ddMMyyyy = "dd/MM/yyyy";
  private final static String TZ = "UTC";
  private final static long ONE_HOUR = 3600000;

  public static String formatDateWithHours(String date, Context context) {
    if (isInvalidString(date)) {
      return "";
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    long dateInMilliseconds = getTimeSafely(dateFromString);
    return dateWithHoursFormatter(dateInMilliseconds, context);
  }

  public static String formatDateWithoutHours(String date) {
    if (isInvalidString(date)) {
      return "";
    }
    Date dateFromString = formatServerDate(date, dateFormat2);
    long dateInMilliseconds = getTimeSafely(dateFromString);
    return formatDate(dateInMilliseconds);
  }

  public static long getDateInMilliseconds(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    long dateInMilliseconds = getTimeSafely(dateFromString);
    return dateInMilliseconds;
  }

  public static long getOneHour() {
    return ONE_HOUR;
  }

  public static String formatDateForAPIWithHour(String time, String date) {
    if (isInvalidString(time) && isInvalidString(date)) {
      return "";
    }

    String fullDateString = date + " " + time;
    Date dateddMMyyyyHHmm = formatServerDate(fullDateString, dateFormat3);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    return simpleDateFormat.format(dateddMMyyyyHHmm);
  }

  public static String formatDateForAPIWithoutHour(String date) {
    if (isInvalidString(date)) {
      return "";
    }
    Date dateFromString = formatServerDate(date, ddMMyyyy);
    long dateInMilliseconds = getTimeSafely(dateFromString);
    return new SimpleDateFormat(dateFormat2).format(new Date(dateInMilliseconds));
  }

  public static String stringFromDateWithAPIFormat(Date date) {
    return new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(date);
  }

  public static int getHour(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    return Integer.parseInt(new SimpleDateFormat(HH)
      .format(new Date(getTimeSafely(dateFromString))));
  }

  public static int getMinutes(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    return Integer.parseInt(new SimpleDateFormat(mm)
      .format(new Date(getTimeSafely(dateFromString))));
  }

  public static int getDay(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    return Integer.parseInt(new SimpleDateFormat(dd)
      .format(new Date(getTimeSafely(dateFromString))));
  }

  public static int getMonth(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    return Integer.parseInt(new SimpleDateFormat(MM)
      .format(new Date(getTimeSafely(dateFromString))));
  }

  public static int getYear(String date) {
    if (isInvalidString(date)) {
      return 0;
    }
    Date dateFromString = formatServerDate(date, dateFormat);
    return Integer.parseInt(new SimpleDateFormat(yyyy)
      .format(new Date(getTimeSafely(dateFromString))));
  }

  private static long getTimeSafely(Date dateFromString) {
    if (dateFromString == null) {
      return 0;
    }
    return dateFromString.getTime();
  }

  private static String dateWithHoursFormatter(long dateInMilliseconds, Context context) {
    String formattedddMMyyyy = formatDate(dateInMilliseconds);
    String formattedHH = new SimpleDateFormat(HH).format(new Date(dateInMilliseconds));
    String formatedmm = new SimpleDateFormat(mm).format(new Date(dateInMilliseconds));
    if (formattedHH.equals(MIDNIGHT) || formattedHH.equals(HOUR_ONE)) {
      return context.getResources()
        .getString(R.string.social_feed_cards_hours_indicator_singular, formattedddMMyyyy,
          formattedHH, formatedmm);
    } else {
      return context.getResources()
        .getString(R.string.social_feed_cards_hours_indicator_plural, formattedddMMyyyy,
          formattedHH, formatedmm);
    }
  }

  private static String formatDate(long dateInMilliseconds) {
    return new SimpleDateFormat(ddMMyyyy).format(new Date(dateInMilliseconds));
  }

  private static Date formatServerDate(String dateString, String format) {
    if (isInvalidString(dateString) && isInvalidString(format)) {
      return new Date();
    }
    SimpleDateFormat parser = new SimpleDateFormat(format);
    Date date;
    try {
      date = parser.parse(dateString);
    } catch (ParseException e) {
      date = null;
    }
    return date;
  }

  private static boolean isInvalidString(String date) {
    return date == null || date.isEmpty();
  }

}
