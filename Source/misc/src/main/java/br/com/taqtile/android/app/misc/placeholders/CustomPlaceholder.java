package br.com.taqtile.android.app.misc.placeholders;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.lang.annotation.Retention;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.buttons.CustomButton;
import br.com.taqtile.android.textviews.CustomTextView;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by indigo on 8/5/16.
 */
public class CustomPlaceholder {

  private final Context context;

  private PlaceholderListener listener;

  private int placeholderType;

  private final LinearLayout rootView;
  private final ImageView image;
  private final CustomTextView title;
  private final FrameLayout titleContainer;
  private final CustomTextView message;
  private final FrameLayout messageContainer;
  private final CustomButton button;

  public static final int CONNECTION = 0;
  public static final int NOT_FOUND = 1;
  public static final int CITY_NOT_DEFINED = 2;
  public static final int UNEXPECTED_ERROR = 3;
  public static final int CONNECTED_PROFESSIONALS_NOT_FOUND = 4;
  public static final int NO_NOTIFICATIONS = 5;
  public static final int NO_MEDICAL_RECOMMENDATIONS = 6;
  public static final int NO_SYMPTOM_HISTORY = 7;
  public static final int NO_HEALTHCARE_FOUND = 8;

  @Retention(SOURCE)
  @IntDef({
    CONNECTION,
    NOT_FOUND,
    CITY_NOT_DEFINED,
    UNEXPECTED_ERROR,
    CONNECTED_PROFESSIONALS_NOT_FOUND,
    NO_NOTIFICATIONS,
    NO_MEDICAL_RECOMMENDATIONS,
    NO_SYMPTOM_HISTORY,
    NO_HEALTHCARE_FOUND
  })
  public @interface PlaceholderType {}

  //region Lifecycle
  public CustomPlaceholder(View rootView) {
    context = rootView.getContext();
    this.rootView = (LinearLayout) rootView;
    image = (ImageView) this.rootView.findViewById(R.id.component_placeholder_image);
    title = (CustomTextView) this.rootView.findViewById(R.id.component_placeholder_title);
    titleContainer = (FrameLayout) this.rootView.findViewById(R.id.component_custom_placeholder_title_container);
    message = (CustomTextView) this.rootView.findViewById(R.id.component_placeholder_message);
    messageContainer = (FrameLayout) this.rootView.findViewById(R.id.component_custom_placeholder_message_container);
    button = (CustomButton) this.rootView.findViewById(R.id.component_placeholder_button);
    init();
  }

  private void init() {
    setupLinearLayout();
    button.setOnClickListener(v -> {
      if (listener != null)
        listener.onPlaceholderButtonClick();
    });
  }
  //endregion

  //region Setup
  private void setupLinearLayout() {
    rootView.setOrientation(LinearLayout.VERTICAL);
    rootView.setGravity(Gravity.CENTER);
  }

  private void bindViews() {
  }

  private void setupPlaceholder(Drawable imageDrawable, String title, String message,
                                String buttonText) {
    setPlaceholderImage(imageDrawable);
    setPlaceholderTitle(title);
    setPlaceholderMessage(message);
    setPlaceholderButtonText(buttonText);
  }
  //endregion

  //region Public
  public void setListener(PlaceholderListener listener) {
    this.listener = listener;
  }

  public void setPlaceholderImage(Drawable imageDrawable) {
    image.setImageDrawable(imageDrawable);
  }

  public void hideButton() {
    button.setVisibility(View.GONE);
  }

  public void showButton() {
    button.setVisibility(View.VISIBLE);
  }

  public void setPlaceholderTitle(String title) {
    this.title.setText(title);
  }

  public void setPlaceholderMessage(String message) {
    this.message.setText(message);
  }

  public void setPlaceholderButtonText(String text) {
    button.setText(text);
  }

  public int getPlaceHolderType() {
    return placeholderType;
  }

  public void setPlaceholderUnexpectedError(String message) {

    Resources r = context.getResources();
    Drawable image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
    String title = r.getString(R.string.placeholder_unexpected_error_title);
    showButton();
    String buttonText = r.getString(R.string.placeholder_button_try_again);

    setupPlaceholder(image, title, message, buttonText);

  }

  public void setPlaceholderType(@PlaceholderType int type) {
    placeholderType = type;
    Resources r = context.getResources();
    Drawable image;
    String title;
    String message;
    String buttonText = "";

    switch (type) {
      case CONNECTION:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_no_connection);
        title = r.getString(R.string.placeholder_connection_title);
        message = r.getString(R.string.placeholder_connection_message);
        showButton();
        buttonText = r.getString(R.string.placeholder_button_try_again);
        break;
      case NOT_FOUND:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_no_results);
        title = r.getString(R.string.placeholder_not_found_title);
        message = r.getString(R.string.placeholder_not_found_message);
        showButton();
        buttonText = r.getString(R.string.placeholder_button_try_again);
        break;
      case CITY_NOT_DEFINED:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
        title = r.getString(R.string.placeholder_city_not_defined_title);
        message = r.getString(R.string.placeholder_city_not_defined_message);
        showButton();
        buttonText = r.getString(R.string.placeholder_city_not_defined_button);
        break;
      case UNEXPECTED_ERROR:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
        title = r.getString(R.string.placeholder_unexpected_error_title);
        message = r.getString(R.string.placeholder_unexpected_error_message);
        showButton();
        buttonText = r.getString(R.string.placeholder_button_try_again);
        break;
      case CONNECTED_PROFESSIONALS_NOT_FOUND:
        image = ContextCompat.getDrawable(context, R.drawable.ic_recomendacoes_estetoscopio);
        title = r.getString(R.string.placeholder_connected_professionals_not_defined_title);
        message = r.getString(R.string.placeholder_connected_professionals_not_defined_message);
        hideButton();
        break;
      case NO_NOTIFICATIONS:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
        title = r.getString(R.string.placeholder_no_notifications_title);
        message = r.getString(R.string.placeholder_no_notifications_message);
        this.titleContainer.setVisibility(View.GONE);
        adjustBodyMargin();
        hideButton();
        break;
      case NO_MEDICAL_RECOMMENDATIONS:
        image = ContextCompat.getDrawable(context, R.drawable.ic_recomendacoes_estetoscopio);
        title = r.getString(R.string.placeholder_no_medical_recommendations_title);
        message = r.getString(R.string.placeholder_no_medical_recommendations_message);
        this.titleContainer.setVisibility(View.GONE);
        adjustBodyMargin();
        hideButton();
        break;
      case NO_SYMPTOM_HISTORY:
        image = ContextCompat.getDrawable(context, R.drawable.ic_historico_sintomas_placeholder);
        title = r.getString(R.string.placeholder_no_symptom_history_title);
        message = r.getString(R.string.placeholder_no_symptom_history_message);
        adjustBodyMargin();
        hideButton();
        break;
      case NO_HEALTHCARE_FOUND:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
        title = r.getString(R.string.placeholder_healthcare_found_title);
        message = r.getString(R.string.placeholder_healthcare_found_message);
        adjustBodyMargin();
        hideButton();
        break;
      default:
        image = ContextCompat.getDrawable(context, R.drawable.ic_placeholder_empty_state);
        title = r.getString(R.string.placeholder_unexpected_error_title);
        message = r.getString(R.string.placeholder_unexpected_error_message);
        showButton();
        buttonText = r.getString(R.string.placeholder_button_try_again);
        break;
    }

    setupPlaceholder(image, title, message, buttonText);
  }

  private void adjustBodyMargin(){
    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) messageContainer.getLayoutParams();
    layoutParams.topMargin = (int) context.getResources().getDimension(R.dimen.margin_x_small);
    messageContainer.setLayoutParams(layoutParams);
  }

  public interface PlaceholderListener {
    void onPlaceholderButtonClick();
  }
}
