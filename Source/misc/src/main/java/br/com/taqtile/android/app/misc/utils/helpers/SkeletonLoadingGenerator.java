package br.com.taqtile.android.app.misc.utils.helpers;

import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by taqtile on 3/31/17.
 */

public class SkeletonLoadingGenerator<T extends View & SkeletonLoadingGenerator.SkeletonLoadingWithShimmer> {

  private final static int DEFAULT_AMOUNT_OF_SKELETON_CELLS = 5;
  private int amountOfSkeletonCells;

  private LinearLayout loadingViewsLayout;

  public SkeletonLoadingGenerator(LinearLayout loadingViewsLayout, T loadingView) {
    this.loadingViewsLayout = loadingViewsLayout;
    this.amountOfSkeletonCells = DEFAULT_AMOUNT_OF_SKELETON_CELLS;
    generateLoadingViewsList(loadingView);
  }

  public SkeletonLoadingGenerator(LinearLayout loadingViewsLayout, T loadingView, int amountOfSkeletonCells) {
    this.loadingViewsLayout = loadingViewsLayout;
    this.amountOfSkeletonCells = amountOfSkeletonCells;
    generateLoadingViewsList(loadingView);
  }

  private void generateLoadingViewsList(T loadingWithShimmer) {
    for (int i = 0; i < amountOfSkeletonCells; i++) {
      loadingViewsLayout.addView(loadingWithShimmer.instantiate());
    }
  }

  public void stopShimmering() {
    FadeEffectHelper.fadeOutView(loadingViewsLayout);
  }

  public interface SkeletonLoadingWithShimmer {
    View instantiate();
  }
}
