package br.com.taqtile.android.app.misc.utils.helpers;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by taqtile on 18/04/17.
 */

public class HeterogeneousRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{

  // TODO: 18/04/17 Refactor this component later to allow it to specify with more flexibility the view that should receive spacing
  private int FIRST_VIEW_POSITION = 0;
  private int SECOND_VIEW_POSITION = 1;

  int verticalSpacing;

  public HeterogeneousRecyclerViewItemDecoration(int verticalSpacing) {
    this.verticalSpacing = verticalSpacing;
  }

  @Override
  public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                             RecyclerView.State state) {
    boolean isFirstPosition = parent.getChildAdapterPosition(view) == FIRST_VIEW_POSITION;
    boolean isSecondPosition = parent.getChildAdapterPosition(view) == SECOND_VIEW_POSITION;
    if (!isFirstPosition && !isSecondPosition) {
      outRect.bottom = verticalSpacing;
    }
  }

}
