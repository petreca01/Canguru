package br.com.taqtile.android.app.misc.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import xyz.santeri.wvp.WrappingViewPager;

/**
 * Created by taqtile on 19/05/17.
 */


public class NoSwipeViewPager extends WrappingViewPager {
  public NoSwipeViewPager(Context context) {
    super(context);
  }

  public NoSwipeViewPager(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public boolean onInterceptTouchEvent(MotionEvent event) {
    // Never allow swiping to switch between pages
    return false;
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    // Never allow swiping to switch between pages
    return false;
  }

}
