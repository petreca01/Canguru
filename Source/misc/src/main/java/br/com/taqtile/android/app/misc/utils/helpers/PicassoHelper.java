package br.com.taqtile.android.app.misc.utils.helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import br.com.taqtile.android.app.misc.R;


/**
 * Created by taqtile on 12/7/15.
 */
public class PicassoHelper {

  public PicassoHelper() {
  }

  public static void loadImageFromUrl(final ImageView imageView, final String imageUrl,
                                      final Context context) {
    if (isImageSourceValid(imageUrl)) {
      Picasso.with(context)
        .load(imageUrl)
        .placeholder(R.drawable.bkg_placeholder)
        .error(R.drawable.bkg_placeholder)
        .into(imageView);
    }
  }

  public static void loadImageFromUrl(final ImageView imageView, final String imageUrl,
                                      final Context context, Drawable placeholder) {
    if (isImageSourceValid(imageUrl)) {
      Picasso.with(context)
        .load(imageUrl)
        .placeholder(placeholder)
        .error(placeholder)
        .into(imageView);
    } else {
      imageView.setImageDrawable(placeholder);
    }
  }


  public static void loadImageFromUrl(final ImageView imageView, final String imageUrl,
                                      final Context context, @DrawableRes int placeholder) {
    loadImageFromUrl(imageView, imageUrl, context, ContextCompat.getDrawable(context, placeholder));
  }

  public static void loadImageFromUrlAndChangeVisibility(ImageView imageView, String imageUrl, Context context) {
    Picasso.with(context)
      .load(imageUrl)
      .into(imageView,
        new com.squareup.picasso.Callback() {
          @Override
          public void onSuccess() {
            imageView.setVisibility(View.VISIBLE);
          }

          @Override
          public void onError() {
            imageView.setVisibility(View.GONE);
          }
        });
  }

  public static void loadImageFromUrlFitCenter(final ImageView imageView, final String imageUrl,
                                               final Context context, Drawable placeholder) {
    if (isImageSourceValid(imageUrl)) {
      Picasso.with(context)
        .load(imageUrl)
        .placeholder(placeholder)
        .error(placeholder)
        .fit().centerInside()
        .into(imageView);
    } else {
      imageView.setImageDrawable(placeholder);
    }
  }

  public static void loadImageFromPath(final ImageView imageView, final String imagePath, final Context context) {
    if (isImageSourceValid(imagePath)) {
      Picasso.with(context)
        .load(new File(imagePath))
        .placeholder(R.drawable.bkg_placeholder)
        .error(R.drawable.bkg_placeholder)
        .into(imageView);
    }
  }

  private static boolean isImageSourceValid(String imageURL) {
    return imageURL != null && !imageURL.isEmpty();
  }
}
