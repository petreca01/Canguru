package br.com.taqtile.android.app.misc.search;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import br.com.taqtile.android.app.misc.R;
import br.com.taqtile.android.edittexts.CustomSimpleEditText;
import br.com.taqtile.android.textviews.CustomTextView;

/**
 * Created by taqtile on 11/6/15.
 */
public class CustomSearchView extends LinearLayout {

  protected CustomSimpleEditText textField;

  private SubmitSearchListener listener;

  private ImageView clearButton;

  private View bottomDivider;

  public CustomSearchView(Context context) {
    super(context);
    init();
  }

  public CustomSearchView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CustomSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  public CustomSimpleEditText getEditText(){
    return textField;
  }

  private void init() {
    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    inflater.inflate(R.layout.component_custom_search_view, this, true);

    bindViews();

    setupClearButton();
    setupEditText();
    setupFocusListener();
    textField.requestFocus();
  }

  //TODO implement different font later http://stackoverflow.com/questions/23629122/how-to-set-custom-fonts-on-edittext-android

  private void bindViews() {
    textField = (CustomSimpleEditText) findViewById(R.id.component_custom_search_view_edit_text);
    clearButton = (ImageView) findViewById(R.id.component_custom_search_view_clear_button);
    bottomDivider = findViewById(R.id.component_custom_search_view_bottom_divider);
  }

  private void setupClearButton() {
    clearButton.setOnClickListener(view -> eraseText());
  }

  private void eraseText() {
    textField.setText("");

  }

  private void setupEditText() {

    textField.setOnKeyListener((v, keyCode, event) -> {
      if (wasEnterPressed(keyCode, event) && listener != null) {
        listener.onSubmit(getText());
        return true;
      }

      return false;
    });
  }

  private void setupFocusListener(){
    textField.setOnFocusChangeListener((v, hasFocus) -> {
      if (hasFocus){
        bottomDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_primary));
      } else {
        bottomDivider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_gray_dark));
      }
    });
  }

  private boolean wasEnterPressed(int keyCode, KeyEvent event) {
    return (event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER);
  }

  public void setTextWatcher(TextWatcher textWatcher) {
    textField.addTextChangedListener(textWatcher);
  }

  public void setText(String text) {
    textField.setText(text);
    textField.setSelection(text.length());
  }

  public String getText() {
    return textField.getText().toString();
  }

  public void setSubmitSearchListener(SubmitSearchListener listener) {
    this.listener = listener;
  }

  public interface SubmitSearchListener {
    void onSubmit(String text);
  }

  public void setHint(@StringRes int hint) {
    setHint(getResources().getString(hint));
  }

  public void setHint(String hint) {
    textField.setHint(hint);
  }

  public void requestEditTextFocus() {
    textField.requestFocus();
  }
}
