# Canguru - Android

## Environment

<details>
<summary>Environment Setup</summary>

__Credentials for setup can be found on [Passpack](https://www.passpack.com/)__

- Setup environment as described in [fastlane's repo](https://github.com/indigotech/fastlane/blob/4.1.2/docs/SETUP.md#general-local-environment)
- **Android Studio 2.3**
- **Gradle 3.3**
- [Install Editor Config](docs/assets/EditorConfigInstall.png)
</details>

<details>
<summary>Updating docs</summary>

Doc image generation uses node.js environment with mermaid [[1]](https://github.com/knsv/mermaid) [[2]](https://knsv.github.io/mermaid/live_editor/) to handle text-to-image diagrams.

To update image diagams at [`docs/assets`](docs/assets) you can edit them at [`docs/diagrams-src`](docs/diagrams-src) and use the folowing commads to generate the images again:

```bash
$ yarn docs:update
````

To configure your environment to do so, follow the steps bellow

```bash 
$ nvm use
$ npm install -g yarn
$ yarn install
```

</details>

## Integration Docs

<details>
<summary>Notifications List</summary>

API has no pagination ant can only deal with "return all after a certain id", so we implemented a cache on the app side to handle this limitation.

![Notifications list](./docs/assets/notifications-list.txt.png)
</details>

<details>
<summary>Notifications Status</summary>
To set notification status correctly cache needs to be taken into consideration

![Notification status](./docs/assets/notifications-status.txt.png)

</details>


## Troubleshooting

<details>
<summary>Android Studio build fails with errors related to `R` class</summary>

If the build fails with issues related to the `R` class, probably the refactor step missed some files. Check the `AndroidManifest.xml` of each module (buttons, recyclerviews,...) and verify that the package name inside of it matches the refactored packages.

</details>

## API Tests

Credentials for access can be found on __[Passpack](https://www.passpack.com/)__, with this entry: __`Runscope - Public`__.


<details>
<summary>Autenticação</summary>

- __[Auth](https://www.runscope.com/radar/1b9gvi4cizjk/3fbd470f-f58c-4ebb-a041-b0e502316341/overview)__
</details>

<details>
<summary>Agenda</summary>

- __[Agendar](https://www.runscope.com/radar/1b9gvi4cizjk/2df03f4e-684e-4763-8209-c17e93360f24/overview)__
- __[Cadastrar compromisso](https://www.runscope.com/radar/1b9gvi4cizjk/12102c7f-e6ad-4aa7-a82f-885b71414c4c/overview)__
- __[Listar itens](https://www.runscope.com/radar/1b9gvi4cizjk/d36a7e91-67ad-40d3-a0e8-e6aaa699446d/overview)__
- __[Marcar como realizado](https://www.runscope.com/radar/1b9gvi4cizjk/052546f3-6f5d-420c-a577-f7ea60824e68/overview)__
- __[Remover compromisso](https://www.runscope.com/radar/1b9gvi4cizjk/f11e1ea8-5f6e-4606-9ebd-34a5586ac584/overview)__
</details>

<details>
<summary>Análise de risco</summary>

- __[Enviar Respostas](https://www.runscope.com/radar/1b9gvi4cizjk/6bf0c0b8-4d4a-4a03-915e-9b38c92f7d66/overview)__
- __[Listar perguntas e respostas](https://www.runscope.com/radar/1b9gvi4cizjk/5f07dd19-b40f-40f7-b360-2bb202657c96/overview)__
</details>

<details>
<summary>Canais</summary>

- __[Comentar em um post](https://www.runscope.com/radar/1b9gvi4cizjk/358a1320-bbc3-4f40-a883-3717a7adb9c1/overview)__
- __[Curtir/Descurtir um comentário em um post](https://www.runscope.com/radar/1b9gvi4cizjk/89846b47-c75c-4017-82b9-813078664734/overview)__
- __[Curtir/Descurtir um post](https://www.runscope.com/radar/1b9gvi4cizjk/9e75abe8-dd08-4941-af21-268f8617b139/overview)__
- __[Excluir comentário](https://www.runscope.com/radar/1b9gvi4cizjk/458bee97-8592-4528-b9e9-412567264980/overview)__
- __[Listar canais](https://www.runscope.com/radar/1b9gvi4cizjk/c1a99b78-a75d-4eff-8dcd-2ab4570e65f0/overview)__
- __[Listar últimos posts de canais seguidos](https://www.runscope.com/radar/1b9gvi4cizjk/79f500d4-c5f0-4bdc-9f01-efff1512e1da/overview)__
- __[Seguir/Parar de seguir um canal](https://www.runscope.com/radar/1b9gvi4cizjk/72e771b6-809d-48f0-bfd2-52dc7b034fab/overview)__
- __[Ver um post e seus comentarios](https://www.runscope.com/radar/1b9gvi4cizjk/b439ff04-b2b3-4afe-aff1-47ae93c94f81/overview)__
</details>

<details>
<summary>Devices</summary>

- __[Adicionar novo device](https://www.runscope.com/radar/1b9gvi4cizjk/d4ce00cb-dcdb-4344-b985-165fe4624240/overview)__
</details>

<details>
<summary>Exames</summary>

- __[Informar ultrassom](https://www.runscope.com/radar/1b9gvi4cizjk/a4082b48-9b7d-4ee2-8321-1aff9213396f/overview)__
- __[Ver ultrassom](https://www.runscope.com/radar/1b9gvi4cizjk/1f77e789-833a-4683-98d3-61f4afb53e72/overview)__
</details>

<details>
<summary>Forum</summary>

- __[Comentar em um post](https://www.runscope.com/radar/1b9gvi4cizjk/077a4c87-3bb4-4720-aa81-788ea54a3faa/overview)__
- __[Curtir/Descurtir um comentário](https://www.runscope.com/radar/1b9gvi4cizjk/4e13b784-d819-4625-b1af-1aacbb7f99d4/overview)__
- __[Curtir/Descurtir um post](https://www.runscope.com/radar/1b9gvi4cizjk/8adca01a-6a05-43bf-94e9-6e41b7dea704/overview)__
- __[Excluir comentário](https://www.runscope.com/radar/1b9gvi4cizjk/55759289-cce0-4a20-9441-6d37275eaa02/overview)__
- __[Listar posts](https://www.runscope.com/radar/1b9gvi4cizjk/f0468b73-b97f-497e-ac49-994ec30023ad/overview)__
- __[Novo post](https://www.runscope.com/radar/1b9gvi4cizjk/91cbf390-e5a0-4678-8854-5d58c8116988/overview)__
- __[Ver post e comentários](https://www.runscope.com/radar/1b9gvi4cizjk/d97fd162-c748-4d65-9fc3-932cf0c2299f/overview)__
</details>

<details>
<summary>Maternidades</summary>

- __[Avaliar maternidade](https://www.runscope.com/radar/1b9gvi4cizjk/a6631f3a-d04a-418f-8d80-d394f8b2735e/overview)__
- __[Buscar](https://www.runscope.com/radar/1b9gvi4cizjk/cfb87b05-efc8-477b-b7f6-88ccd5bf49f4/overview)__
- __[Comentar em uma maternidade](https://www.runscope.com/radar/1b9gvi4cizjk/8f4cf9b0-7109-4e13-854a-93539ce98f72/overview)__
- __[Excluir comentário](https://www.runscope.com/radar/1b9gvi4cizjk/3507fb2f-a339-4261-8a01-6f04432235d4/overview)__
- __[Listar cidades](https://www.runscope.com/radar/1b9gvi4cizjk/b7e49925-ce2a-4473-9026-98f868499bbe/overview)__
- __[Listar por cidades](https://www.runscope.com/radar/1b9gvi4cizjk/12c35f0c-bbf6-4239-a904-8a7d462ce803/overview)__
- __[Seguir/Deixar de seguir uma maternidade](https://www.runscope.com/radar/1b9gvi4cizjk/5fd53d94-7fba-42e4-9fd6-9165fdb57217/overview)__
- __[Ver maternidade](https://www.runscope.com/radar/1b9gvi4cizjk/d1f1d4ca-7408-4ee8-98ba-ecee1c11edb4/overview)__
</details>

<details>
<summary>Notificações</summary>

- __[Count](https://www.runscope.com/radar/1b9gvi4cizjk/a493b73c-f5d9-484d-81b6-053ea89aae3f/overview)__
- __[Listar notificações](https://www.runscope.com/radar/1b9gvi4cizjk/f0ae1ccc-1838-456d-a969-6e80fb2a8d08/overview)__
- __[Marcar como lida](https://www.runscope.com/radar/1b9gvi4cizjk/d36f6b95-8af8-4496-9d3c-d47128532ac1/overview)__
</details>

<details>
<summary>Operadores de saúde</summary>

- __[Buscar operadora](https://www.runscope.com/radar/1b9gvi4cizjk/6e51faed-fcab-4969-ada6-cbac36ed94a9/overview)__
- __[Listar operadoras](https://www.runscope.com/radar/1b9gvi4cizjk/0f599f8f-b7c0-450f-b221-a18f0cc8083e/overview)__
</details>

<details>
<summary>Perfil</summary>

- __[Atualizar perfil público](https://www.runscope.com/radar/1b9gvi4cizjk/1219b81a-6be1-4918-afd1-280a2f0823a9/overview)__
- __[Exibir perfil](https://www.runscope.com/radar/1b9gvi4cizjk/fcd111d3-f0c2-4fc1-8f59-4938d35c3929/overview)__
- __[Lista últimos posts](https://www.runscope.com/radar/1b9gvi4cizjk/deec277b-5ae2-4778-a78e-bf6822696c5b/overview)__
</details>

<details>
<summary>Plano de parto</summary>

- __[Enviar Respostas](https://www.runscope.com/radar/1b9gvi4cizjk/f1d0c345-df29-4190-9751-5dfd7af29c15/overview)__
- __[Listar perguntas e respostas](https://www.runscope.com/radar/1b9gvi4cizjk/e9879fec-5919-4e6e-8a9f-2089805e12e6/overview)__
- __[Remover resposta](https://www.runscope.com/radar/1b9gvi4cizjk/c638dae3-88d7-4b0c-a276-2b9ce7afb282/overview)__
</details>

<details>
<summary>Profissionais</summary>

- __[Autorizar acesso](https://www.runscope.com/radar/1b9gvi4cizjk/d44e6f47-26c1-4cb7-bc46-1b913151cf92/overview)__
- __[Listar profissionais](https://www.runscope.com/radar/1b9gvi4cizjk/7e223185-d3c9-4c09-afff-4dd440608489/overview)__
- __[Revogar acesso](https://www.runscope.com/radar/1b9gvi4cizjk/76df4103-0f94-4a4d-9c4d-2568377de1c8/overview)__
</details>

<details>
<summary>Quadro clínico</summary>

- __[Informar quadro](https://www.runscope.com/radar/1b9gvi4cizjk/255db2c3-c953-4bf9-9452-23fe7407cfd0/overview)__
- __[Listar quadros do usuário](https://www.runscope.com/radar/1b9gvi4cizjk/d4eebe46-b339-436b-8f3f-2de19c8deb9e/overview)__
- __[Listar todos](https://www.runscope.com/radar/1b9gvi4cizjk/186a6192-79c4-4540-82ad-300eff7da481/overview)__
</details>

<details>
<summary>Recomendações médicas</summary>

- __[Listar recomendações](https://www.runscope.com/radar/1b9gvi4cizjk/382fc507-fa57-4e45-a099-00257d0e6e68/overview)__
- __[Marcar como lido](https://www.runscope.com/radar/1b9gvi4cizjk/1249150d-f5ef-42f0-bcd1-5f365178c24d/overview)__
</details>

<details>
<summary>Sintomas</summary>

- __[Histórico de sinstomas do usuário](https://www.runscope.com/radar/1b9gvi4cizjk/f0d2b741-f94f-4abd-892e-b13117bb1fe9/overview)__
- __[Informar sintoma](https://www.runscope.com/radar/1b9gvi4cizjk/0de7205e-0865-4b68-9a27-0a04c12cf9f7/overview)__
- __[Listar sintomas](https://www.runscope.com/radar/1b9gvi4cizjk/8cf22a4a-17fb-4106-91a4-eb7becc64364/overview)__
</details>

<details>
<summary>Usuários</summary>

- __[Atualizar Usuário](https://www.runscope.com/radar/1b9gvi4cizjk/77696514-2c21-4e09-9f1c-e1f67c7ee633/overview)__
- __[Criar novo usuário](https://www.runscope.com/radar/1b9gvi4cizjk/b825fc5a-40c4-481f-8f8f-4fca60f6478c/overview)__
- __[Dados do usuário](https://www.runscope.com/radar/1b9gvi4cizjk/d0ca0dea-6998-4bca-8db7-14cd4c64c50f/overview)__
</details>